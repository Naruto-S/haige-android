package com.hc.base.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.util.TypedValue;

import com.hc.base.R;


public class ATEUtil {

    public static int getThemeTextColorPrimary(Context context) {
        TypedValue textColorPrimary = new TypedValue();
        context.getTheme().resolveAttribute(android.R.attr.textColorPrimary, textColorPrimary, true);
        return context.getResources().getColor(textColorPrimary.resourceId);
    }

    public static int getThemeTextColorSecondly(Context context) {
        TypedValue textColorSecondly = new TypedValue();
        context.getTheme().resolveAttribute(android.R.attr.textColorSecondary, textColorSecondly, true);
        return context.getResources().getColor(textColorSecondly.resourceId);
    }

    public static Drawable getDefaultAlbumDrawable(Context context) {
        TypedValue defaultAlbum = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.base_default_album_drawable, defaultAlbum, true);
        return context.getResources().getDrawable(defaultAlbum.resourceId);
    }

    @DrawableRes
    public static int getDefaultAlbumResourceId(Context context) {
        TypedValue defaultAlbum = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.base_default_album_drawable, defaultAlbum, true);
        return defaultAlbum.resourceId;
    }

    public static Drawable getDefaultSingerDrawable(Context context) {
        TypedValue defaultSinger = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.base_default_singer_drawable, defaultSinger, true);
        return context.getResources().getDrawable(defaultSinger.resourceId);
    }

    public static int getThemeAlbumDefaultPaletteColor(Context context) {
        TypedValue paletteColor = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.base_album_default_palette_color, paletteColor, true);
        return context.getResources().getColor(paletteColor.resourceId);
    }

    public static int getThemePopupMenuStyle(Context context) {
        TypedValue paletteColor = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.base_ttPopupMenuStyle, paletteColor, true);
        return paletteColor.resourceId;
    }

}
