package com.hc.base.utils;

import android.os.Build;
import android.support.annotation.ColorInt;
import android.view.View;
import android.view.Window;

/**
 * ================================================
 * Created by huangcong on 2017/12/29.
 * ================================================
 */

public class TranslucentStatusBarUtils {

    /**
     * 设置全屏
     *
     * @param window
     */
    public static void setFullScreen(Window window) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            View decorView = window.getDecorView();
            int option = decorView.getSystemUiVisibility();
            option |= View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
            decorView.setSystemUiVisibility(option);
        }
    }

    /**
     * 设置全屏，并设置状态栏为透明
     *
     * @param window
     * @param statusBarColor
     */
    public static void setFullScreenStatusBarTranslucent(Window window, @ColorInt int statusBarColor) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            View decorView = window.getDecorView();
            int option = decorView.getSystemUiVisibility();
            option |= View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
            window.setStatusBarColor(statusBarColor);
            // 4.4系统可设置这个属性，本app暂时不需要在在4.4上如此适配
            // option |= WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS;
            decorView.setSystemUiVisibility(option);
        }
    }
}
