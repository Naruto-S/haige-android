package com.hc.base.mvp;

import com.hc.core.mvp.CorePresenter;
import com.hc.core.mvp.IModel;
import com.hc.core.mvp.IView;
import com.hc.core.utils.RxLifecycleUtils;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;
import me.jessyan.rxerrorhandler.handler.ErrorHandleSubscriber;

/**
 * ================================================
 * Created by huangcong on 2018/2/22.
 * ================================================
 */

public class BasePresenter<M extends IModel, V extends IView> extends CorePresenter<M, V> {

    public BasePresenter() {
        super();
    }

    public BasePresenter(V v) {
        super(v);
    }

    public BasePresenter(M m, V v) {
        super(m, v);
    }
}
