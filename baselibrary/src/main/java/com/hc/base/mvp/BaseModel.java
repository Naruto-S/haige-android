package com.hc.base.mvp;

import com.hc.core.integration.IRepositoryManager;
import com.hc.core.mvp.CoreModel;

/**
 * ================================================
 * Created by huangcong on 2018/2/22.
 * ================================================
 */

public class BaseModel extends CoreModel {

    public BaseModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }
}
