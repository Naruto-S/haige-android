package com.hc.base.widget.recycleradapterwrapper.layoutmanager;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by huangcong on 2018/2/16.
 * <p>
 * 高度自适应
 */

public class AutoAdaptHeightGridLayoutManager extends GridLayoutManager {

    RecyclerView recyclerView;
    int spacing;

    public AutoAdaptHeightGridLayoutManager(Context context, int spanCount, int spacing, RecyclerView recyclerView) {
        super(context, spanCount);
        this.recyclerView = recyclerView;
        this.spacing = spacing;
    }

    @Override
    public void onMeasure(RecyclerView.Recycler recycler, RecyclerView.State state, int widthSpec, int heightSpec) {
        super.onMeasure(recycler, state, widthSpec, heightSpec);
        try {
            int measuredWidth = recyclerView.getMeasuredWidth();
            int measuredHeight = recyclerView.getMeasuredHeight();
            int myMeasureHeight = 0;
            int count = state.getItemCount();
            for (int i = 0; i < count; i++) {
                View view = recycler.getViewForPosition(i);
                if (view != null) {
                    if (myMeasureHeight < measuredHeight && i % getSpanCount() == 0) {
                        RecyclerView.LayoutParams p = (RecyclerView.LayoutParams) view.getLayoutParams();
                        int childWidthSpec = ViewGroup.getChildMeasureSpec(widthSpec,
                                getPaddingLeft() + getPaddingRight(), p.width);
                        int childHeightSpec = ViewGroup.getChildMeasureSpec(heightSpec,
                                getPaddingTop() + getPaddingBottom(), p.height);
                        view.measure(childWidthSpec, childHeightSpec);
                        myMeasureHeight += view.getMeasuredHeight() + p.bottomMargin + p.topMargin + spacing;
                    }
                    recycler.recycleView(view);
                }
            }
            setMeasuredDimension(measuredWidth, Math.min(measuredHeight, myMeasureHeight));
        } catch (Exception e) {
        }
    }

}
