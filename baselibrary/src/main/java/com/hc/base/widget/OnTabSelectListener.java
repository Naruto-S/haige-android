package com.hc.base.widget;

public interface OnTabSelectListener {
    void onTabSelect(int position);
    void onTabReselect(int position);
}