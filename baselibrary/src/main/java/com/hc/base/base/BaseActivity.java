package com.hc.base.base;

import android.app.Activity;
import android.content.Intent;

import com.hc.core.base.CoreActivity;
import com.hc.core.mvp.IPresenter;
import com.tbruyelle.rxpermissions2.RxPermissions;

/**
 * ================================================
 * Created by huangcong on 2018/2/22.
 * ================================================
 */

public abstract class BaseActivity<P extends IPresenter> extends CoreActivity<P> {

    public void showLoading() {

    }

    public void hideLoading() {

    }

    public void showMessage(String message) {

    }

    public void launchActivity(Intent intent) {

    }

    public void killMyself() {

    }

    public Activity getActivity() {
        return this;
    }

    public RxPermissions getRxPermissions() {
        return null;
    }
}
