package com.hc.base.base;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.hc.core.mvp.IPresenter;

/**
 * ================================================
 * Created by huangcong on 2018/2/22.
 * <p>
 * 懒加载实现
 * ================================================
 */

public abstract class BaseLazyFragment<P extends IPresenter> extends BaseFragment<P> {

    //Fragment的View加载完毕的标记
    private boolean isViewCreated = false;

    //Fragment对用户可见的标记
    private boolean isUIVisible = false;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        //isVisibleToUser这个boolean值表示:该Fragment的UI 用户是否可见
        if (isVisibleToUser) {
            isUIVisible = true;
            initData(null);
        } else {
            isUIVisible = false;
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        isViewCreated = true;
        initData(savedInstanceState);
    }

    @Override
    public void initData(Bundle savedInstanceState) {
        if (isViewCreated && isUIVisible) {
            lazyInitData(savedInstanceState);
            //数据加载完毕,恢复标记,防止重复加载
            isViewCreated = false;
            isUIVisible = false;
        }
    }

    public abstract void lazyInitData(Bundle savedInstanceState);
}
