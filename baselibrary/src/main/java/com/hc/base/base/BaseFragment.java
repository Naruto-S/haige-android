package com.hc.base.base;

import android.content.Intent;

import com.hc.core.base.CoreFragment;
import com.hc.core.mvp.IPresenter;
import com.tbruyelle.rxpermissions2.RxPermissions;

/**
 * ================================================
 * Created by huangcong on 2018/2/22.
 * ================================================
 */

public abstract class BaseFragment<P extends IPresenter> extends CoreFragment<P> {

    @Override
    public void setData(Object data) {

    }

    public void showLoading() {

    }

    public void hideLoading() {

    }

    public void showMessage(String message) {

    }

    public void launchActivity(Intent intent) {

    }

    public void killMyself() {

    }

    public RxPermissions getRxPermissions() {
        return null;
    }
}
