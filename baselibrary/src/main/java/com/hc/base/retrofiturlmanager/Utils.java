package com.hc.base.retrofiturlmanager;

import okhttp3.HttpUrl;

/**
 * ================================================
 * <p>
 * ================================================
 */
class Utils {

    private Utils() {
        throw new IllegalStateException("do not instantiation me");
    }

    static HttpUrl checkUrl(String url) {
        HttpUrl parseUrl = HttpUrl.parse(url);
        if (null == parseUrl) {
            throw new InvalidUrlException(url);
        } else {
            return parseUrl;
        }
    }
}
