package com.hc.base.retrofiturlmanager;

import android.text.TextUtils;

/**
 * ================================================
 * <p>
 * ================================================
 */
public class InvalidUrlException extends RuntimeException {

    public InvalidUrlException(String url) {
        super("You've configured an invalid url : " + (TextUtils.isEmpty(url) ? "EMPTY_OR_NULL_URL" : url));
    }
}
