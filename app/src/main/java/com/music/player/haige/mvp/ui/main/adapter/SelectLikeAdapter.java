package com.music.player.haige.mvp.ui.main.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.music.player.haige.R;
import com.music.player.haige.mvp.model.entity.rank.RankCategoryBean;

import java.util.ArrayList;

public class SelectLikeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<RankCategoryBean> mData = new ArrayList<>();

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_select_like, parent, false);
        return new TagViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        TagViewHolder itemHolder = (TagViewHolder) holder;
        RankCategoryBean category = mData.get(position);
        itemHolder.tagTv.setText(category.getTag());
        itemHolder.tagTv.setSelected(category.isSelect());
        itemHolder.tagTv.setOnClickListener(v -> setSelect(position));
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setData(ArrayList<RankCategoryBean> data) {
        mData.clear();
        mData.addAll(data);
        notifyDataSetChanged();
    }

    public ArrayList<RankCategoryBean> getData() {
        return mData;
    }

    public void setSelect(int position) {
        if (position >= 0 && position < mData.size()) {
            RankCategoryBean category = mData.get(position);
            category.setSelect(!category.isSelect());
            mData.set(position, category);
            notifyItemChanged(position);
        }
    }

    class TagViewHolder extends RecyclerView.ViewHolder {

        private TextView tagTv;

        public TagViewHolder(View itemView) {
            super(itemView);

            tagTv = itemView.findViewById(R.id.tv_tag);
        }
    }
}
