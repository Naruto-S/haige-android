package com.music.player.haige.di.module;

import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.SongsModel;

import dagger.Module;
import dagger.Provides;

/**
 * ================================================
 * Created by huangcong on 2018/2/27.
 * ================================================
 */
@Module
public class SongsModule {
    private MVContract.SongsView view;

    public SongsModule(MVContract.SongsView view) {
        this.view = view;
    }

    @FragmentScope
    @Provides
    MVContract.SongsView provideView() {
        return this.view;
    }

    @FragmentScope
    @Provides
    MVContract.SongsModel provideModel(SongsModel model) {
        return model;
    }

}
