package com.music.player.haige.app.image.widget;

import com.music.player.haige.app.image.loader.listener.OnImageLoaderListener;
import com.music.player.haige.app.image.release.DisplayOptions;

/**
 * 设置图片功能抽象
 * <p>
 * Created by ZaKi on 2016/9/3.
 */
public interface ImageFetcher {

    /**
     * 最基本的加载图片：无自定义
     *
     * @param uri
     */
    void setImageURI(String uri);

    /**
     * 带加载监听的加载图片
     *
     * @param uri
     * @param onImageLoaderListener
     */
    void setImageURI(String uri, OnImageLoaderListener onImageLoaderListener);

    /**
     * 带自定义参数的加载图片
     *
     * @param uri
     * @param displayOptions
     * @param onImageLoaderListener
     */
    void setImageURI(String uri, DisplayOptions displayOptions, OnImageLoaderListener onImageLoaderListener);

    /**
     * 获取到显示的样式
     *
     * @return
     */
    DisplayOptions.Builder display();
}
