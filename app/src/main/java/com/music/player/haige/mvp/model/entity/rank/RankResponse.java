package com.music.player.haige.mvp.model.entity.rank;

import com.music.player.haige.mvp.model.entity.BaseResponse;
import com.music.player.haige.mvp.model.entity.music.Song;

import java.util.ArrayList;

/**
 * ================================================
 * Created by huangcong on 2018/3/11.
 * ================================================
 */

public class RankResponse extends BaseResponse<ArrayList<Song>> {

}
