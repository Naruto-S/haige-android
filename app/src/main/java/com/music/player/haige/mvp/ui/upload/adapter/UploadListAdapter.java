package com.music.player.haige.mvp.ui.upload.adapter;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hc.base.utils.ATEUtil;
import com.hc.base.utils.DensityUtil;
import com.hc.base.widget.fastscroller.FastScrollRecyclerView;
import com.music.player.haige.R;
import com.music.player.haige.app.base.pref.DevicePrefHelper;
import com.music.player.haige.app.utils.launcher.ActivityLauncherStart;
import com.music.player.haige.app.utils.launcher.ActivityStartBase;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.ui.music.service.MusicServiceConnection;
import com.music.player.haige.mvp.ui.music.viewholder.SongItemViewHolder;
import com.music.player.haige.mvp.ui.upload.dialog.UploadPrivacyDialog;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;
import com.music.player.haige.mvp.ui.utils.Utils;
import com.music.player.haige.mvp.ui.widget.MusicVisualizer;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


public class UploadListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements FastScrollRecyclerView.SectionedAdapter {

    private List<Song> arrayList;
    private AppCompatActivity mContext;
    private float topPlayScore;
    private OnItemClickListener onItemClickListener;

    public UploadListAdapter(AppCompatActivity context) {
        this.mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.app_item_music_upload, viewGroup, false);
        return new ItemHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ItemHolder itemHolder = (ItemHolder) holder;
        Song localItem;
        localItem = arrayList.get(position);
        itemHolder.rank.setText(String.valueOf(position + 1));
        itemHolder.title.setText(localItem.musicName);
        itemHolder.artist.setText(localItem.getArtistName(mContext));
        itemHolder.album.setText(localItem.albumName);

        if (MusicServiceConnection.getCurrentAudioId() == localItem.songId) {
            itemHolder.title.setTextColor(ResourceUtils.getColor(R.color.colorAccent));
            if (MusicServiceConnection.isPlaying()) {
                itemHolder.rank.setVisibility(View.GONE);
                itemHolder.musicVisualizer.setVisibility(View.VISIBLE);
                itemHolder.musicVisualizer.setColor(ResourceUtils.getColor(R.color.colorAccent));
            } else {
                itemHolder.rank.setVisibility(View.VISIBLE);
                itemHolder.rank.setTextColor(ResourceUtils.getColor(R.color.colorAccent));
                itemHolder.musicVisualizer.setVisibility(View.GONE);
            }
        } else {
            itemHolder.rank.setVisibility(View.VISIBLE);
            itemHolder.rank.setTextColor(ATEUtil.getThemeTextColorPrimary(mContext));
            itemHolder.title.setTextColor(ATEUtil.getThemeTextColorPrimary(mContext));
            itemHolder.musicVisualizer.setVisibility(View.GONE);
        }

        if (topPlayScore != 0) {
            itemHolder.playScore.setVisibility(View.VISIBLE);
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) itemHolder.playScore.getLayoutParams();
            int full = DensityUtil.getScreenWidth(mContext);
            layoutParams.width = (int) (full * (localItem.getPlayCountScore() / topPlayScore));
        }

        itemHolder.upload.setOnClickListener(v -> {
            if (Utils.isNotNull(onItemClickListener)) {
                onItemClickListener.onUpload(v, localItem);
            }
        });
    }

    @Override
    public int getItemCount() {
        return Utils.isNotEmptyCollection(arrayList) ? arrayList.size() : 0;
    }

    public void setSongList(List<Song> arrayList) {
        this.arrayList = arrayList;
        if (arrayList.size() != 0) {
            this.topPlayScore = arrayList.get(0).getPlayCountScore();
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public String getSectionName(int position) {
        if (arrayList == null || arrayList.size() == 0 || position == 0) {
            return "";
        }
        Character ch = arrayList.get(position).musicName.charAt(0);
        if (Character.isDigit(ch)) {
            return "#";
        } else {
            return Character.toString(ch);
        }
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public class ItemHolder extends SongItemViewHolder implements View.OnClickListener {

        public TextView rank;
        public TextView title;
        public TextView artist;
        public TextView album;
        public TextView upload;
        public View playScore;
        public MusicVisualizer musicVisualizer;

        public ItemHolder(View view) {
            super(view);
            this.upload = view.findViewById(R.id.tv_upload);
            this.rank = view.findViewById(R.id.app_item_rank);
            this.title = view.findViewById(R.id.text_item_title);
            this.artist = view.findViewById(R.id.text_item_subtitle);
            this.album = view.findViewById(R.id.text_item_subtitle_2);
            this.popupMenu = view.findViewById(R.id.popup_menu);
            this.playScore = view.findViewById(R.id.app_item_play_score);
            this.musicVisualizer = view.findViewById(R.id.app_item_music_visualizer);

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Observable.empty()
                    .delay(100, TimeUnit.MILLISECONDS)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnComplete(() -> {
                        int position = getAdapterPosition();
                        if (position >= 0 && position < arrayList.size()) {
                            MusicServiceConnection.playAll(arrayList, position, false);
                        }
                    })
                    .subscribe();
        }
    }

    public interface OnItemClickListener {
        void onUpload(View view, Song song);
    }

}


