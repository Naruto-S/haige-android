package com.music.player.haige.mvp.ui.music.fragment;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.hc.base.base.BaseFragment;
import com.hc.base.widget.SlidingTabLayout;
import com.hc.core.di.component.AppComponent;
import com.music.player.haige.R;
import com.music.player.haige.app.Constants;
import com.music.player.haige.app.EventBusTags;
import com.music.player.haige.app.ToolbarConfig;
import com.music.player.haige.app.base.AppBaseFragment;
import com.music.player.haige.di.component.DaggerCommonComponent;
import com.music.player.haige.di.module.CommonModule;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.presenter.MusicPresenter;
import com.music.player.haige.mvp.ui.widget.adapter.TitlePagerAdapter;

import org.simple.eventbus.EventBus;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * ================================================
 * Created by huangcong on 2018/2/27.
 * ================================================
 */

public class MusicFragment extends AppBaseFragment<MusicPresenter> implements ToolbarConfig, MVContract.CommonView {

    @BindView(R.id.app_layout_toolbar)
    Toolbar mToolbar;

    @BindView(R.id.app_fragment_local_music_tl)
    SlidingTabLayout mTabLayout;

    @BindView(R.id.app_fragment_local_music_search_layout)
    View mSearchView;

    @BindView(R.id.app_fragment_local_music_vp)
    ViewPager mViewPager;

    private String mAction;

    public static MusicFragment newInstance(String action) {
        Bundle args = new Bundle();
        switch (action) {
            case Constants.NAVIGATE_ALLSONG:
                args.putString(Constants.PLAYLIST_TYPE, action);
                break;
            case Constants.NAVIGATE_PLAYLIST_RECENTADD:
                args.putString(Constants.PLAYLIST_TYPE, action);
                break;
            case Constants.NAVIGATE_PLAYLIST_RECENTPLAY:
                args.putString(Constants.PLAYLIST_TYPE, action);
                break;
            case Constants.NAVIGATE_PLAYLIST_FAVORITE:
                args.putString(Constants.PLAYLIST_TYPE, action);
                break;
            default:
                throw new RuntimeException("wrong action type");
        }
        MusicFragment fragment = new MusicFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Toolbar getToolbar() {
        return mToolbar;
    }

    @Override
    public String getTitle() {
        if (getArguments() != null) {
            String action = getArguments().getString(Constants.PLAYLIST_TYPE);
            switch (action) {
                case Constants.NAVIGATE_ALLSONG:
                    return getString(R.string.app_mine_music);
                case Constants.NAVIGATE_PLAYLIST_RECENTADD:
                    return getString(R.string.app_mine_recently_added);
                case Constants.NAVIGATE_PLAYLIST_RECENTPLAY:
                    return getString(R.string.app_mine_recently_played);
                case Constants.NAVIGATE_PLAYLIST_FAVORITE:
                    return getString(R.string.app_mine_favorite);
            }
        }
        return getString(R.string.app_mine_music);
    }

    @Override
    public boolean displayHomeAsUpEnabled() {
        return true;
    }

    @Override
    public boolean displayShowTitleEnabled() {
        return true;
    }

    @Override
    public boolean showOptionsMenu() {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                EventBus.getDefault().post(EventBusTags.MAIN.BACK);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void setupFragmentComponent(AppComponent appComponent) {
        DaggerCommonComponent
                .builder()
                .appComponent(appComponent)
                .commonModule(new CommonModule(this))
                .build()
                .inject(this);
    }

    @Override
    public View initView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.app_fragment_music, container, false);
    }

    @Override
    public void initData(Bundle savedInstanceState) {
        mAction = getArguments().getString(Constants.PLAYLIST_TYPE);

        if (Constants.NAVIGATE_ALLSONG.equals(mAction)
                || Constants.NAVIGATE_PLAYLIST_RECENTPLAY.equals(mAction)
                || Constants.NAVIGATE_PLAYLIST_FAVORITE.equals(mAction)) {
            mTabLayout.setVisibility(View.GONE);
            if (Constants.NAVIGATE_PLAYLIST_FAVORITE.equals(mAction)) {
                mSearchView.setVisibility(View.INVISIBLE);
            }
            String[] titles = getResources().getStringArray(R.array.app_music_favorite);
            BaseFragment[] fragments = new BaseFragment[1];
            fragments[0] = SongsFragment.newInstance(mAction);
            TitlePagerAdapter adapter = new TitlePagerAdapter(getChildFragmentManager(), titles, fragments);
            mViewPager.setAdapter(adapter);
        } else {
            boolean isAllSong = Constants.NAVIGATE_ALLSONG.equals(mAction);
            String[] titles = getResources().getStringArray(isAllSong ? R.array.app_all_music : R.array.app_music);
            BaseFragment[] fragments = new BaseFragment[titles.length];
            fragments[0] = SongsFragment.newInstance(mAction);
            fragments[1] = ArtistFragment.newInstance(mAction);
            fragments[2] = AlbumFragment.newInstance(mAction);
            if (isAllSong) {
                fragments[3] = FoldersFragment.newInstance();
            }
            TitlePagerAdapter adapter = new TitlePagerAdapter(getChildFragmentManager(), titles, fragments);
            mViewPager.setAdapter(adapter);
            mViewPager.setOffscreenPageLimit(adapter.getCount() - 1);
            mTabLayout.setViewPager(mViewPager);
        }
    }

    @OnClick({R.id.app_fragment_local_music_search_layout})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.app_fragment_local_music_search_layout:
                if (Constants.NAVIGATE_ALLSONG.equals(mAction)) {
                    EventBus.getDefault().post(EventBusTags.MAIN.SEARCH_LOCAL);
                } else if (Constants.NAVIGATE_PLAYLIST_RECENTPLAY.equals(mAction)) {
                    EventBus.getDefault().post(EventBusTags.MAIN.SEARCH_RECENT);
                } else {
                    EventBus.getDefault().post(EventBusTags.MAIN.SEARCH);
                }
                break;
        }
    }
}
