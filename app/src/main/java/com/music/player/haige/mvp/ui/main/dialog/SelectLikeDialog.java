package com.music.player.haige.mvp.ui.main.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.music.player.haige.BuildConfig;
import com.music.player.haige.R;
import com.music.player.haige.app.EventBusTags;
import com.music.player.haige.app.base.BaseDialogFragment;
import com.music.player.haige.app.base.pref.DevicePrefHelper;
import com.music.player.haige.app.constants.TimeConstants;
import com.music.player.haige.mvp.model.entity.rank.RankCategoryBean;
import com.music.player.haige.mvp.ui.main.adapter.SelectLikeAdapter;
import com.music.player.haige.mvp.ui.setting.RateDialog;
import com.music.player.haige.mvp.ui.upload.adapter.MusicTypeAdapter;

import org.simple.eventbus.EventBus;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by john on 2018/1/17.
 */

public class SelectLikeDialog extends BaseDialogFragment {

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    private SelectLikeAdapter mAdapter;
    private ArrayList<RankCategoryBean> mData = new ArrayList<>();
    private OnSelectLikeListener mListener;

    public static SelectLikeDialog create(FragmentManager manager) {
        SelectLikeDialog dialog = new SelectLikeDialog();
        dialog.setFragmentManger(manager);
        return dialog;
    }

    public SelectLikeDialog setData(ArrayList<RankCategoryBean> data) {
        mData.clear();
        mData.addAll(data);
        return this;
    }

    public SelectLikeDialog setListener(OnSelectLikeListener listener) {
        this.mListener = listener;
        return this;
    }

    public SelectLikeDialog show() {
        show(mFragmentManager);
        return this;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mAdapter = new SelectLikeAdapter();
        mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        mRecyclerView.setAdapter(mAdapter);

        mAdapter.setData(mData);
    }

    @Override
    protected void setDialogParams() {
        Dialog dialog = getDialog();
        if (dialog != null) {
            DisplayMetrics dm = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);

            Window window = this.getDialog().getWindow();
            window.getDecorView().setPadding(0, 0, 0, 0);
            WindowManager.LayoutParams lp = window.getAttributes();
            lp.width = (int) (dm.widthPixels * 0.85);
            if (isUseRatio) {
                lp.height = (int) (lp.width * ratio);
            } else {
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            }
            window.setAttributes(lp);
            window.setBackgroundDrawable(new ColorDrawable());
        }

    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    @Override
    public int setRootView() {
        return R.layout.dialog_select_like;
    }

    @OnClick({R.id.iv_close, R.id.tv_select_complete})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_select_complete:
                if (mListener != null) {
                    mListener.onSelectList(getSelectList());
                }

                break;
            case R.id.iv_close:
                dismiss();
                break;
        }
    }

    public ArrayList<String> getSelectList() {
        ArrayList<RankCategoryBean> datas = mAdapter.getData();
        ArrayList<String> strings = new ArrayList<>();
        for (RankCategoryBean data : datas) {
            if (data.isSelect()) {
                strings.add(data.getTag());
            }
        }
        return strings;
    }

    public interface OnSelectLikeListener {
        void onSelectList(ArrayList<String> strings);
    }
}
