package com.music.player.haige.di.component;

import com.hc.core.di.component.AppComponent;
import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.di.module.LikeMusicModule;
import com.music.player.haige.mvp.ui.music.fragment.LikeMusicFragment;
import com.music.player.haige.mvp.ui.user.MeLikeMusicActivity;

import dagger.Component;

/**
 * ================================================
 * Created by huangcong on 2018/2/27.
 * ================================================
 */

@FragmentScope
@Component(modules = {LikeMusicModule.class}, dependencies = AppComponent.class)
public interface LikeMusicComponent {

    void inject(LikeMusicFragment fragment);

    void inject(MeLikeMusicActivity activity);
}
