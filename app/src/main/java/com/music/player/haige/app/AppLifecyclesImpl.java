package com.music.player.haige.app;

import android.app.Application;
import android.content.Context;

import com.hc.core.base.delegate.AppLifecycles;
import com.hc.core.utils.CoreUtils;
import com.liulishuo.filedownloader.FileDownloader;
import com.liulishuo.filedownloader.connection.FileDownloadUrlConnection;
import com.liulishuo.filedownloader.util.FileDownloadLog;
import com.music.player.haige.BuildConfig;
import com.music.player.haige.app.image.utils.ImageLoadController;
import com.music.player.haige.app.service.InitService;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;
import com.tencent.bugly.crashreport.CrashReport;
import com.umeng.analytics.MobclickAgent;
import com.umeng.commonsdk.UMConfigure;
import com.umeng.socialize.PlatformConfig;

import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * ================================================
 * 展示 {@link AppLifecycles} 的用法
 * <p>
 * ================================================
 */
public class AppLifecyclesImpl implements AppLifecycles {

    @Override
    public void attachBaseContext(Context base) {
//          MultiDex.install(base);  //这里比 onCreate 先执行,常用于 MultiDex 初始化,插件化框架的初始化
    }

    @Override
    public void onCreate(Application application) {
        if (LeakCanary.isInAnalyzerProcess(application)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        if (BuildConfig.LOG_DEBUG) {//Timber初始化
            //Timber 是一个日志框架容器,外部使用统一的Api,内部可以动态的切换成任何日志框架(打印策略)进行日志打印
            //并且支持添加多个日志框架(打印策略),做到外部调用一次 Api,内部却可以做到同时使用多个策略
            //比如添加三个策略,一个打印日志,一个将日志保存本地,一个将日志上传服务器
            Timber.plant(new Timber.DebugTree());
            // 如果你想将框架切换为 Logger 来打印日志,请使用下面的代码,如想切换为其他日志框架请根据下面的方式扩展
//                    Logger.addLogAdapter(new AndroidLogAdapter());
//                    Timber.plant(new Timber.DebugTree() {
//                        @Override
//                        protected void log(int priority, String tag, String message, Throwable t) {
//                            Logger.log(priority, tag, message, t);
//                        }
//                    });
            ButterKnife.setDebug(true);
        }

        //Fresco
        ImageLoadController.initFresco(application);

        //download配置
        FileDownloadLog.NEED_LOG = BuildConfig.LOG_DEBUG;
        /**
         * just for cache Application's Context, and ':filedownloader' progress will NOT be launched
         * by below code, so please do not worry about performance.
         * @see FileDownloader#init(Context)
         */
        FileDownloader.setupOnApplicationOnCreate(application)
                .connectionCreator(new FileDownloadUrlConnection
                        .Creator(new FileDownloadUrlConnection.Configuration()
                        .connectTimeout(15_000) // set connection timeout.
                        .readTimeout(15_000) // set read timeout.
                ))
                .commit();
        //leakCanary内存泄露检查
        CoreUtils.obtainAppComponentFromContext(application).extras().put(RefWatcher.class.getName(), BuildConfig.USE_CANARY ? LeakCanary.install(application) : RefWatcher.DISABLED);
        //扩展 AppManager 的远程遥控功能
        CoreUtils.obtainAppComponentFromContext(application).appManager().setHandleListener((appManager, message) -> {
            switch (message.what) {
                //case 0:
                //do something ...
                //   break;
            }
        });
        //Usage:
        //Message msg = new Message();
        //msg.what = 0;
        //AppManager.post(msg); like EventBus
        initUmeng(application);
        initBugly(application);
        initService(application);
    }

    @Override
    public void onTerminate(Application application) {

    }

    private void initBugly(Application application) {
        /**
         * 腾讯bugly
         */
        final String buglyKey = "2678184b49";
        CrashReport.initCrashReport(application, buglyKey, BuildConfig.USE_CANARY);
        CrashReport.setAppVersion(application, BuildConfig.VERSION_NAME);
    }

    private void initUmeng(Application application) {
        /**
         * 友盟统计
         */
        UMConfigure.init(application, UMConfigure.DEVICE_TYPE_PHONE, null);
        UMConfigure.setLogEnabled(BuildConfig.DEBUG);
        //日志加密
        UMConfigure.setEncryptEnabled(true);
        MobclickAgent.setScenarioType(application, MobclickAgent.EScenarioType.E_DUM_NORMAL);

//        //微信
        PlatformConfig.setWeixin("wxe2a9837c769dc98b", "bd7138027556812558dfa53e0dc31575");
        //QQ
        PlatformConfig.setQQZone("1106841442", "JKdaUjQge0MRc3uM");
    }

    public void initService(Application application) {
        InitService.start(application);
    }
}
