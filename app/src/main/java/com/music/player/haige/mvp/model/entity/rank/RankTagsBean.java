package com.music.player.haige.mvp.model.entity.rank;

import com.google.gson.annotations.SerializedName;
import com.music.player.haige.mvp.model.entity.music.Song;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Naruto on 2018/6/17.
 */

public class RankTagsBean implements Serializable {

    @SerializedName("new_music")
    Song newMusic;

    @SerializedName("hot_music")
    Song hotMusic;

    @SerializedName("category")
    ArrayList<RankCategoryBean> category;

    public Song getNewMusic() {
        return newMusic;
    }

    public void setNewMusic(Song newMusic) {
        this.newMusic = newMusic;
    }

    public Song getHotMusic() {
        return hotMusic;
    }

    public void setHotMusic(Song hotMusic) {
        this.hotMusic = hotMusic;
    }

    public ArrayList<RankCategoryBean> getCategory() {
        return category;
    }

    public void setCategory(ArrayList<RankCategoryBean> category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "RankTagsBean{" +
                "newMusic=" + newMusic +
                ", hotMusic=" + hotMusic +
                ", category=" + category +
                '}';
    }
}
