package com.music.player.haige.mvp.ui.setting.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;

import com.hc.core.di.component.AppComponent;
import com.hc.core.utils.CoreUtils;
import com.music.player.haige.R;
import com.music.player.haige.app.EventBusTags;
import com.music.player.haige.app.base.AppBaseFragment;
import com.music.player.haige.di.component.DaggerDownloadFinishComponent;
import com.music.player.haige.di.module.DownloadFinishModule;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.Api;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.presenter.DownloadFinishFPresenter;
import com.music.player.haige.mvp.ui.music.adapter.SongsListAdapter;
import com.music.player.haige.mvp.ui.music.service.MusicServiceConnection;
import com.music.player.haige.mvp.ui.widget.DividerItemDecoration;
import com.music.player.haige.mvp.ui.widget.StatusLayout;

import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * ================================================
 * Created by huangcong on 2018/3/20.
 * ================================================
 */

public class DownloadFinishFragment extends AppBaseFragment<DownloadFinishFPresenter> implements MVContract.DownloadFinishView, SongsListAdapter.OnSongClickListener {

    @BindView(R.id.app_layout_sl)
    StatusLayout mStatusLayout;

    @BindView(R.id.app_fragment_download_finish_rv)
    RecyclerView mDownloadFinishRv;

    private SongsListAdapter mAdapter;

    public static DownloadFinishFragment newInstance() {
        DownloadFinishFragment fragment = new DownloadFinishFragment();
        return fragment;
    }

    @Override
    public void setupFragmentComponent(AppComponent appComponent) {
        DaggerDownloadFinishComponent.builder()
                .appComponent(appComponent)
                .downloadFinishModule(new DownloadFinishModule(this))
                .build()
                .inject(this);
    }

    @Override
    public View initView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.app_fragment_download_finish, container, false);
    }

    @Override
    public void initData(Bundle savedInstanceState) {
        mStatusLayout.setOnStatusClickListener(new StatusLayout.OnStatusClickListener() {
            @Override
            public void onEmptyClick() {
                EventBus.getDefault().post(EventBusTags.UI.GO_TO_RECOMMEND);
                EventBus.getDefault().post(EventBusTags.MAIN.BACK);
            }

            @Override
            public void onReloadClick() {

            }
        });

        mAdapter = new SongsListAdapter((AppCompatActivity) getActivity(), true);
        mAdapter.setOnSongClickListener(this);
        mDownloadFinishRv.setLayoutManager(new LinearLayoutManager(getActivity()));
        mDownloadFinishRv.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST, true));
        mDownloadFinishRv.setHasFixedSize(true);
        mDownloadFinishRv.setAdapter(mAdapter);

        mPresenter.loadDownloadFinishMusic();
    }

    @Override
    public void onSuccess(Api.Action action, Object o) {
        super.onSuccess(action, o);
        mStatusLayout.showStatusView(StatusLayout.STATUS.OK);
        mDownloadFinishRv.setVisibility(View.VISIBLE);
        mAdapter.setSongList((List<Song>) o);
    }

    @Override
    public void onError(Api.Action action, String code, String message) {
        super.onError(action, code, message);
        mStatusLayout.showStatusView(StatusLayout.STATUS.EMPTY);
        mDownloadFinishRv.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showMessage(String message) {
        super.showMessage(message);
        CoreUtils.makeText(getContext(), message);
    }

    @Override
    public void onPopupMenuClick(View v, int position, Song song) {
        final PopupMenu menu = new PopupMenu(getContext(), v);
        menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Context context = getContext();
                switch (item.getItemId()) {
                    case R.id.popup_song_play_next:
                        MusicServiceConnection.playNext(song);
                        showMessage(context.getResources().getString(R.string.app_next_play));
                        break;
                    case R.id.popup_song_addto_queue:
                        MusicServiceConnection.addToQueue(song);
                        break;
                    case R.id.popup_song_delete:
                        mPresenter.deleteDownloadedMusic(song);
                        mAdapter.removeSong(position);
                        break;
                }
                return false;
            }
        });
        menu.inflate(R.menu.popup_song_download);
        menu.show();
    }

    @Subscriber
    public void onSubscriber(EventBusTags.MetaChangedEvent event) {
        mAdapter.notifyDataSetChanged();
    }

    @Subscriber
    public void onSubscriber(EventBusTags.DOWNLOAD event) {
        if (event == EventBusTags.DOWNLOAD.FINISH) {
            mPresenter.loadDownloadFinishMusic();
        }
    }

    @OnClick(R.id.app_layout_empty_btn)
    public void onEmptyClick() {
        EventBus.getDefault().post(EventBusTags.UI.GO_TO_RECOMMEND);
    }
}
