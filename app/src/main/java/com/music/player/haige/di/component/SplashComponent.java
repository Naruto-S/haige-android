package com.music.player.haige.di.component;

import com.hc.core.di.component.AppComponent;
import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.di.module.SplashModule;
import com.music.player.haige.mvp.ui.launch.SplashActivity;

import dagger.Component;

/**
 * ================================================
 * Created by huangcong on 2018/2/25.
 * ================================================
 */

@FragmentScope
@Component(modules = {SplashModule.class}, dependencies = AppComponent.class)
public interface SplashComponent {

    void inject(SplashActivity activity);
}
