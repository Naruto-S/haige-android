package com.music.player.haige.app.base;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatDialogFragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.music.player.haige.R;
import com.music.player.haige.mvp.ui.utils.DebugLogger;

import butterknife.ButterKnife;

/**
 * Created by Graceful Sun on 18/1/16.
 * E-mail itzhishuaisun@sina.com
 */

public abstract class BaseDialogFragment extends AppCompatDialogFragment {

    private static final String TAG = BaseDialogFragment.class.getSimpleName();

    protected FragmentManager mFragmentManager;

    protected boolean mIsCancelOutside = true;
    protected float ratio = 1.0f; //宽高比
    protected boolean isUseRatio = false;
    private OnDisMissListener onDisMissListener;

    public void setFragmentManger(FragmentManager manger) {
        this.mFragmentManager = manger;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.dialog_style);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().setCanceledOnTouchOutside(true);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        return View.inflate(getActivity().getApplicationContext(), setRootView(), null);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (onDisMissListener != null) {
            onDisMissListener.onDisMiss(dialog);
        }
    }

    public OnDisMissListener getOnDisMissListener() {
        return onDisMissListener;
    }

    public void setOnDisMissListener(OnDisMissListener onDisMissListener) {
        this.onDisMissListener = onDisMissListener;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setDialogParams();

        ButterKnife.bind(this, view);

        this.setCancelable(mIsCancelOutside);
    }

    protected void setDialogParams() {
        Dialog dialog = getDialog();
        if (dialog != null) {
            DisplayMetrics dm = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);

            Window window = this.getDialog().getWindow();
            window.getDecorView().setPadding(0, 0, 0, 0);
            WindowManager.LayoutParams lp = window.getAttributes();
            lp.width = (int) (dm.widthPixels * 0.75);
            if (isUseRatio) {
                lp.height = (int) (lp.width * ratio);
            } else {
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            }
            window.setAttributes(lp);
            window.setBackgroundDrawable(new ColorDrawable());
        }
    }

    public String getFragmentTag() {
        return TAG;
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        try {
            //在每个add事务前增加一个remove事务，防止连续的add
            manager.beginTransaction().remove(this).commit();
            super.show(manager, tag);
        } catch (Exception e) {
            //同一实例使用不同的tag会异常,这里捕获一下
            //e.printStackTrace();
            DebugLogger.i("TAG", "========java.lang.IllegalStateException: Fragment already added:=========>>>1:");
        }
    }

    public void show(FragmentManager fragmentManager) {
        show(fragmentManager, getFragmentTag());
    }

    public boolean isShow() {
        return getDialog() != null && getDialog().isShowing();
    }

    /**
     * 设置dialog 布局
     *
     * @return 布局资源id
     */
    public abstract int setRootView();

    public interface OnDisMissListener {
        void onDisMiss(DialogInterface dialog);
    }

}
