package com.music.player.haige.mvp.ui.share;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.hc.core.di.component.AppComponent;
import com.music.player.haige.R;
import com.music.player.haige.app.Constants;
import com.music.player.haige.app.base.AppBaseDialogFragment;
import com.music.player.haige.app.utils.statistics.CommonStatistics;
import com.music.player.haige.app.utils.statistics.StatisticsConstant;
import com.music.player.haige.di.component.DaggerCommonComponent;
import com.music.player.haige.di.module.CommonModule;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.model.utils.HaigeUtil;
import com.music.player.haige.mvp.presenter.MusicSharePresenter;
import com.music.player.haige.mvp.ui.utils.DebugLogger;
import com.music.player.haige.mvp.ui.utils.ShareUtils;
import com.music.player.haige.mvp.ui.utils.Utils;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;

import butterknife.BindView;
import butterknife.OnClick;

public class MusicShareDialog extends AppBaseDialogFragment<MusicSharePresenter> implements MVContract.CommonView {

    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;

    private Song mSong;
    private String mWebpName;
    private OnShareListener mShareListener;

    public static MusicShareDialog newInstance() {
        MusicShareDialog fragment = new MusicShareDialog();
        return fragment;
    }

    public MusicShareDialog setWebpBgName(String name) {
        mWebpName = name;
        return this;
    }

    public MusicShareDialog setMusic(Song song) {
        mSong = song;
        return this;
    }

    public MusicShareDialog setShareListener(OnShareListener listener) {
        mShareListener = listener;
        return this;
    }

    @Override
    public void onStart() {
        super.onStart();

        Dialog dialog = getDialog();
        dialog.setCanceledOnTouchOutside(true);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams params = window.getAttributes();
        params.gravity = Gravity.BOTTOM;
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(params);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    @Override
    public void setupFragmentComponent(AppComponent component) {
        DaggerCommonComponent.builder()
                .appComponent(component)
                .commonModule(new CommonModule(this))
                .build()
                .inject(this);
    }

    @Override
    public View initView(LayoutInflater inflater, ViewGroup group, Bundle bundle) {
        return inflater.inflate(R.layout.dialog_music_share, group, false);
    }

    @Override
    public void initData(Bundle bundle) {

    }

    @OnClick(R.id.layout_share_weixin)
    public void onShareWeiXin() {
        CommonStatistics.sendEvent(StatisticsConstant.SHARE_WECHAT_FRIEND_CLICK);

        if (Utils.isNotNull(mShareListener)) {
            mShareListener.onShareSuccess();
        }

        if (Utils.isNull(mSong)) return;

        String targetUrl = Constants.URL_SHARE_MUSIC + "?id=" + mSong.songId + "&gif=" + mWebpName;
        String imageUrl = mSong.isLocal ? HaigeUtil.getAlbumArtUri(mSong.getAlbumId()).toString() : mSong.getMusicCover();
        String albumName;
        if (Utils.isNotNull(mSong.getUser()) && Utils.isNotEmptyString(mSong.getUser().getNickName())) {
            albumName = mSong.getUser().getNickName();
        } else {
            albumName = "嗨友提供";
        }

        ShareUtils.shareMusic(getActivity(),
                mSong.musicPath,
                mSong.musicName,
                imageUrl,
                albumName,
                targetUrl,
                mUMShareListener,
                SHARE_MEDIA.WEIXIN);
    }

    @OnClick(R.id.layout_share_weixin_c)
    public void onShareWeiXinCircle() {
        CommonStatistics.sendEvent(StatisticsConstant.SHARE_DYNAMIC_CLICK);

        if (Utils.isNotNull(mShareListener)) {
            mShareListener.onShareSuccess();
        }

        if (Utils.isNull(mSong)) return;

        String targetUrl = Constants.URL_SHARE_MUSIC + "?id=" + mSong.songId + "&gif=" + mWebpName;
        String imageUrl = mSong.isLocal ? HaigeUtil.getAlbumArtUri(mSong.getAlbumId()).toString() : mSong.getMusicCover();
        String albumName;
        if (Utils.isNotNull(mSong.getUser()) && Utils.isNotEmptyString(mSong.getUser().getNickName())) {
            albumName = mSong.getUser().getNickName();
        } else {
            albumName = "嗨友提供";
        }

        ShareUtils.shareMusic(getActivity(),
                mSong.musicPath,
                mSong.musicName,
                imageUrl,
                albumName,
                targetUrl,
                mUMShareListener,
                SHARE_MEDIA.WEIXIN_CIRCLE);
    }

    @OnClick(R.id.layout_share_qq)
    public void onShareQQ() {
        CommonStatistics.sendEvent(StatisticsConstant.SHARE_QQ_CLICK);

        if (Utils.isNotNull(mShareListener)) {
            mShareListener.onShareSuccess();
        }

        if (Utils.isNull(mSong)) return;

        String targetUrl = Constants.URL_SHARE_MUSIC + "?id=" + mSong.songId + "&gif=" + mWebpName;
        String imageUrl = mSong.isLocal ? HaigeUtil.getAlbumArtUri(mSong.getAlbumId()).toString() : mSong.getMusicCover();
        String albumName;
        if (Utils.isNotNull(mSong.getUser()) && Utils.isNotEmptyString(mSong.getUser().getNickName())) {
            albumName = mSong.getUser().getNickName();
        } else {
            albumName = "嗨友提供";
        }

        ShareUtils.shareMusic(getActivity(),
                mSong.musicPath,
                mSong.musicName,
                imageUrl,
                albumName,
                targetUrl,
                mUMShareListener,
                SHARE_MEDIA.QQ);
    }

    @OnClick(R.id.layout_share_qone)
    public void onShareQone() {
        CommonStatistics.sendEvent(StatisticsConstant.SHARE_ZONE_CLICK);

        if (Utils.isNotNull(mShareListener)) {
            mShareListener.onShareSuccess();
        }

        if (Utils.isNull(mSong)) return;

        String targetUrl = Constants.URL_SHARE_MUSIC + "?id=" + mSong.songId + "&gif=" + mWebpName;
        String imageUrl = mSong.isLocal ? HaigeUtil.getAlbumArtUri(mSong.getAlbumId()).toString() : mSong.getMusicCover();
        String albumName;
        if (Utils.isNotNull(mSong.getUser()) && Utils.isNotEmptyString(mSong.getUser().getNickName())) {
            albumName = mSong.getUser().getNickName();
        } else {
            albumName = "嗨友提供";
        }

        ShareUtils.shareMusic(getActivity(),
                mSong.musicPath,
                mSong.musicName,
                imageUrl,
                albumName,
                targetUrl,
                mUMShareListener,
                SHARE_MEDIA.QZONE);
    }

    @OnClick(R.id.layout_share_weibo)
    public void onShareWeibo() {
        CommonStatistics.sendEvent(StatisticsConstant.SHARE_WEIBO_CLICK);

        if (Utils.isNotNull(mShareListener)) {
            mShareListener.onShareSuccess();
        }

        if (Utils.isNull(mSong)) return;

        String targetUrl = Constants.URL_SHARE_MUSIC + "?id=" + mSong.songId + "&gif=" + mWebpName;
        String imageUrl = mSong.isLocal ? HaigeUtil.getAlbumArtUri(mSong.getAlbumId()).toString() : mSong.getMusicCover();
        String albumName;
        if (Utils.isNotNull(mSong.getUser()) && Utils.isNotEmptyString(mSong.getUser().getNickName())) {
            albumName = mSong.getUser().getNickName();
        } else {
            albumName = "嗨友提供";
        }

        ShareUtils.shareMusic(getActivity(),
                mSong.musicPath,
                mSong.musicName,
                imageUrl,
                albumName,
                targetUrl,
                mUMShareListener,
                SHARE_MEDIA.SINA);
    }

    @OnClick(R.id.tv_close)
    public void onClose() {
        if (Utils.isNotNull(getDialog())) {
            getDialog().dismiss();
        }
    }

    private UMShareListener mUMShareListener = new UMShareListener() {
        @Override
        public void onStart(SHARE_MEDIA share_media) {
            DebugLogger.e(TAG, "shareMusic == onStart");
            if (Utils.isNotNull(mProgressBar)) {
                mProgressBar.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onResult(final SHARE_MEDIA share_media) {
            DebugLogger.e(TAG, "shareMusic == onResult");
            if (Utils.isNotNull(mProgressBar)) {
                mProgressBar.setVisibility(View.GONE);
            }
            onClose();
            Toast.makeText(getContext(), "分享成功", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onError(final SHARE_MEDIA share_media, final Throwable throwable) {
            DebugLogger.e(throwable);
            DebugLogger.e(TAG, "shareMusic == onError");
            if (Utils.isNotNull(mProgressBar)) {
                mProgressBar.setVisibility(View.GONE);
            }
            onClose();
//            Toast.makeText(getContext(), "分享失败", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onCancel(final SHARE_MEDIA share_media) {
            DebugLogger.e(TAG, "shareMusic == onCancel");
            if (Utils.isNotNull(mProgressBar)) {
                mProgressBar.setVisibility(View.GONE);
            }
            onClose();
//            Toast.makeText(getContext(), "分享取消", Toast.LENGTH_SHORT).show();
        }
    };

    public interface OnShareListener {

        void onShareSuccess();

    }
}
