package com.music.player.haige.mvp.ui.comment.utils;

import android.widget.TextView;

import com.hc.base.utils.DateUtils;
import com.music.player.haige.R;
import com.music.player.haige.app.constants.TimeConstants;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;
import com.music.player.haige.mvp.ui.utils.Utils;

import static com.hc.base.utils.DateUtils.MM_DD_CN;

/**
 * Created by Naruto on 2018/8/25.
 */

public class CommentUtils {

    public static void showCommentTime(TextView textView, long createdAt) {
        if (Utils.isNull(textView)) return;

        long time = System.currentTimeMillis();
        long interval = time - createdAt * 1000;
        if (interval <= TimeConstants.MIN_1) {
            textView.setText(ResourceUtils.resourceString(R.string.comment_time_min, 1));
        } else if (interval < TimeConstants.HOUR_1) {
            textView.setText(ResourceUtils.resourceString(R.string.comment_time_min, interval / TimeConstants.MIN_1));
        } else if (interval < TimeConstants.HOUR_24) {
            textView.setText(ResourceUtils.resourceString(R.string.comment_time_min, interval / TimeConstants.HOUR_1));
        } else {
            textView.setText(DateUtils.format(createdAt * 1000, MM_DD_CN));
        }
    }
}
