package com.music.player.haige.mvp.ui.share;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.imagepipeline.request.Postprocessor;
import com.hc.core.di.component.AppComponent;
import com.music.player.haige.R;
import com.music.player.haige.app.base.AppBaseDialogFragment;
import com.music.player.haige.app.base.pref.UserPrefHelper;
import com.music.player.haige.app.image.utils.ImageLoadController;
import com.music.player.haige.app.utils.statistics.CommonStatistics;
import com.music.player.haige.app.utils.statistics.StatisticsConstant;
import com.music.player.haige.di.component.DaggerCommonComponent;
import com.music.player.haige.di.module.CommonModule;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.entity.user.UserBean;
import com.music.player.haige.mvp.presenter.DownloadSharePresenter;
import com.music.player.haige.mvp.ui.utils.DebugLogger;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;
import com.music.player.haige.mvp.ui.utils.ShareUtils;
import com.music.player.haige.mvp.ui.utils.Utils;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;

import butterknife.BindView;
import butterknife.OnClick;

public class DownloadShareDialog extends AppBaseDialogFragment<DownloadSharePresenter> implements MVContract.CommonView {

    @BindView(R.id.iv_download_share_lock)
    ImageView mShareLockIv;

    @BindView(R.id.tv_download_share_title)
    TextView mShareTitleTv;

    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;

    private UserBean mUserBean;

    public static DownloadShareDialog newInstance() {
        DownloadShareDialog fragment = new DownloadShareDialog();
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();

        Dialog dialog = getDialog();
        dialog.setCanceledOnTouchOutside(true);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams params = window.getAttributes();
        params.gravity = Gravity.BOTTOM;
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(params);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    @Override
    public void setupFragmentComponent(AppComponent component) {
        DaggerCommonComponent.builder()
                .appComponent(component)
                .commonModule(new CommonModule(this))
                .build()
                .inject(this);
    }

    @Override
    public View initView(LayoutInflater inflater, ViewGroup group, Bundle bundle) {
        return inflater.inflate(R.layout.dialog_download_share, group, false);
    }

    @Override
    public void initData(Bundle bundle) {
        mUserBean = UserPrefHelper.getMeUser();

        if (UserPrefHelper.getDownloadMusicCount() == 0) {
            mShareLockIv.setImageResource(R.drawable.ic_download_share_lock);
            mShareTitleTv.setText(ResourceUtils.resourceString(R.string.share_download_tips_lock));
        } else {
            mShareLockIv.setImageResource(R.drawable.ic_download_share_ok);
            mShareTitleTv.setText(ResourceUtils.resourceString(R.string.share_download_tips_unlock, UserPrefHelper.getDownloadMusicCount()));
        }
    }

    @OnClick(R.id.layout_share_weixin)
    public void onShareWeiXin() {
        CommonStatistics.sendEvent(StatisticsConstant.DOWNLOAD_SHARE_CLICK);
        shareImage(SHARE_MEDIA.WEIXIN);
    }

    @OnClick(R.id.layout_share_weixin_c)
    public void onShareWeiXinCircle() {
        CommonStatistics.sendEvent(StatisticsConstant.DOWNLOAD_SHARE_CLICK);
        shareImage(SHARE_MEDIA.WEIXIN_CIRCLE);
    }

    @OnClick(R.id.layout_share_qq)
    public void onShareQQ() {
        CommonStatistics.sendEvent(StatisticsConstant.DOWNLOAD_SHARE_CLICK);
        shareImage(SHARE_MEDIA.QQ);
    }

    @OnClick(R.id.layout_share_qone)
    public void onShareQone() {
        CommonStatistics.sendEvent(StatisticsConstant.DOWNLOAD_SHARE_CLICK);
        shareImage(SHARE_MEDIA.QZONE);
    }

    @OnClick(R.id.layout_share_weibo)
    public void onShareWeibo() {
        CommonStatistics.sendEvent(StatisticsConstant.DOWNLOAD_SHARE_CLICK);
        shareImage(SHARE_MEDIA.SINA);
    }

    @OnClick(R.id.tv_close)
    public void onClose() {
        if (Utils.isNotNull(getDialog())) {
            getDialog().dismiss();
        }
    }

    private void shareImage(SHARE_MEDIA platform) {
        if (Utils.isNull(mUserBean)) {
            ShareUtils.shareImage(getActivity(), R.drawable.img_share, R.drawable.img_share, mUMShareListener, platform);
        } else {
            ImageLoadController.getImageFromFull(mUserBean.getAvatarUrl(), new ImageLoadController.RequestImageCallback() {
                @Override
                public void onImageResult(Bitmap bitmap, int width, int height, String uri) {
                    Bitmap finalBitmap = ShareUtils.getAvatarToImg(bitmap, ShareUtils.getUserNameToImg(mUserBean.getNickName()));
                    ShareUtils.shareImage(getActivity(), finalBitmap, finalBitmap, mUMShareListener, platform);
                }

                @Override
                public void onImageFail(String uri) {

                }

                @Override
                public Postprocessor obtainPostprocessor() {
                    return null;
                }
            });
        }
    }

    private UMShareListener mUMShareListener = new UMShareListener() {
        @Override
        public void onStart(SHARE_MEDIA share_media) {
            DebugLogger.e(TAG, "shareImage == onStart");
            if (Utils.isNotNull(mProgressBar)) {
                mProgressBar.setVisibility(View.VISIBLE);
            }
            UserPrefHelper.putDownloadMusicCount(UserPrefHelper.getDownloadMusicCount() + 1);
        }

        @Override
        public void onResult(final SHARE_MEDIA share_media) {
            DebugLogger.e(TAG, "shareImage == onResult");
            if (Utils.isNotNull(mProgressBar)) {
                mProgressBar.setVisibility(View.GONE);
            }
            onClose();
            Toast.makeText(getContext(), "分享成功", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onError(final SHARE_MEDIA share_media, final Throwable throwable) {
            DebugLogger.e(throwable);
            DebugLogger.e(TAG, "shareImage == onError");
            if (Utils.isNotNull(mProgressBar)) {
                mProgressBar.setVisibility(View.GONE);
            }
            onClose();
            Toast.makeText(getContext(), "分享失败", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onCancel(final SHARE_MEDIA share_media) {
            DebugLogger.e(TAG, "shareImage == onCancel");
            if (Utils.isNotNull(mProgressBar)) {
                mProgressBar.setVisibility(View.GONE);
            }
            onClose();
            Toast.makeText(getContext(), "分享取消", Toast.LENGTH_SHORT).show();
        }
    };
}
