package com.music.player.haige.mvp.ui.user.crop.callback;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

public interface BitmapCropCallback {

    void onBitmapCropped(@NonNull Bitmap resultBitmap, Bitmap mThumnailBitmap, int imageWidth, int
            imageHeight);

    void onCropFailure(@NonNull Throwable t);

}