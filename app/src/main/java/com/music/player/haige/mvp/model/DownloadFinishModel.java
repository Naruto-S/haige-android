package com.music.player.haige.mvp.model;

import com.hc.base.mvp.BaseModel;
import com.hc.core.integration.IRepositoryManager;
import com.music.player.haige.mvp.contract.MVContract;

import javax.inject.Inject;

/**
 * ================================================
 * Created by huangcong on 2018/3/20.
 * ================================================
 */

public class DownloadFinishModel extends BaseModel implements MVContract.DownloadFinishModel {

    @Inject
    public DownloadFinishModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }
}
