package com.music.player.haige.app.image.loader;

import android.graphics.drawable.Animatable;
import android.view.View;

import com.facebook.imagepipeline.image.ImageInfo;
import com.music.player.haige.app.image.loader.listener.OnImageLoaderListener;
import com.music.player.haige.app.image.release.DisplayOptions;
import com.music.player.haige.app.image.widget.ImageFetcher;

/**
 * Created by Naruto on 2018/5/13.
 */

public class AvatarLoader extends BaseImageLoader {

    public static void loadUserAvatar(String avatarFid, DisplayOptions.Builder displayOptions, ImageFetcher imageFetcher, OnImageLoaderListener onImageLoaderListener) {
        displaySimpleImage(avatarFid, displayOptions, imageFetcher, onImageLoaderListener);
    }

    public static void loadUserAvatar(String avatarFid, final int loadingRes, final int defaultRes, ImageFetcher imageFetcher) {
        DisplayOptions.Builder displayOptions = new DisplayOptions.Builder()
                .showImageOnLoading(loadingRes)
                .showImageOnFail(defaultRes);

        OnImageLoaderListener onImageLoaderListener = new OnImageLoaderListener() {
            @Override
            public void onImageLoadComplete(String uri, ImageInfo imageInfo, boolean isGif, Animatable animatable, View tagVview) {

            }

            @Override
            public void onImageLoadFail(String uri, Throwable throwable, View tagVview) {
                LocalImageLoader.displayResImage(defaultRes, imageFetcher);
            }
        };

        displaySimpleImage(avatarFid, displayOptions, imageFetcher, onImageLoaderListener);
    }
}
