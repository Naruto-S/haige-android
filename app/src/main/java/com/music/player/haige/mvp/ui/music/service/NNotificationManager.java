package com.music.player.haige.mvp.ui.music.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.graphics.Palette;
import android.text.TextUtils;
import android.widget.RemoteViews;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.facebook.imagepipeline.request.Postprocessor;
import com.hc.core.http.imageloader.glide.GlideCore;
import com.hc.core.http.imageloader.glide.GlideUtils;
import com.music.player.haige.R;
import com.music.player.haige.app.image.utils.ImageLoadController;
import com.music.player.haige.mvp.model.utils.CommonUtils;
import com.music.player.haige.mvp.model.utils.HaigeUtil;
import com.music.player.haige.mvp.model.utils.NavigationUtil;
import com.music.player.haige.mvp.model.utils.StringUtils;
import com.music.player.haige.mvp.ui.utils.DebugLogger;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;
import com.music.player.haige.mvp.ui.utils.ShareUtils;

import timber.log.Timber;

/**
 * ================================================
 * Created by huangcong on 2018/3/13.
 * <p>
 * 通知栏管理
 * ================================================
 */

public class NNotificationManager {

    private static final String CHANNEL_ID = "haige_channel_01";

    private static final int IMAGE_SIZE = 160;

    private static final int NOTIFY_MODE_NONE = 0;
    private static final int NOTIFY_MODE_FOREGROUND = 1;
    private static final int NOTIFY_MODE_BACKGROUND = 2;

    private NotificationManagerCompat mNotificationManager;
    private int mNotifyMode = NOTIFY_MODE_NONE;
    private int mNotificationId = 1000;
    private long mNotificationPostTime = 0;

    public NNotificationManager(Context context) {
        mNotificationManager = NotificationManagerCompat.from(context);
    }

    public void createNotificationChannel(Context context) {
        if (CommonUtils.isOreo()) {
            CharSequence name = "Haige";
            int importance = android.app.NotificationManager.IMPORTANCE_LOW;
            android.app.NotificationManager manager = (android.app.NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            manager.createNotificationChannel(mChannel);
        }
    }

    public void updateNotification(MusicService musicService) {
        final int newNotifyMode;
        if (musicService.isPlaying()) {
            newNotifyMode = NOTIFY_MODE_FOREGROUND;
        } else if (musicService.recentlyPlayed()) {
            newNotifyMode = NOTIFY_MODE_BACKGROUND;
        } else {
            newNotifyMode = NOTIFY_MODE_NONE;
        }

        // int mNotificationId = hashCode();

        if (mNotifyMode != newNotifyMode) {
            if (mNotifyMode == NOTIFY_MODE_FOREGROUND) {
                if (CommonUtils.isLollipop())
                    musicService.stopForeground(newNotifyMode == NOTIFY_MODE_NONE);
                else
                    musicService.stopForeground(newNotifyMode == NOTIFY_MODE_NONE || newNotifyMode == NOTIFY_MODE_BACKGROUND);
            } else if (newNotifyMode == NOTIFY_MODE_NONE) {
                mNotificationManager.cancel(mNotificationId);
                mNotificationPostTime = 0;
            }
        }

        if (newNotifyMode == NOTIFY_MODE_FOREGROUND) {
            musicService.startForeground(mNotificationId, buildNotification(musicService));
        } else if (newNotifyMode == NOTIFY_MODE_BACKGROUND) {
            mNotificationManager.notify(mNotificationId, buildNotification(musicService));
        }

        mNotifyMode = newNotifyMode;
    }

    public void cancelNotification(MusicService musicService) {
        musicService.stopForeground(true);
        //mNotificationManager.cancel(hashCode());
        mNotificationManager.cancel(mNotificationId);
        mNotificationPostTime = 0;
        mNotifyMode = NOTIFY_MODE_NONE;
    }

    private Notification buildNotification(MusicService musicService) {
        Intent nowPlayingIntent = NavigationUtil.getNowPlayingIntent(musicService);
        PendingIntent clickIntent = PendingIntent.getActivity(musicService, 0, nowPlayingIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        if (mNotificationPostTime == 0) {
            mNotificationPostTime = System.currentTimeMillis();
        }

        RemoteViews remoteViews = new RemoteViews(musicService.getPackageName(), R.layout.notification_music);
        remoteViews.setOnClickPendingIntent(R.id.iv_play_or_pause, retrievePlaybackAction(musicService, MusicService.TOGGLEPAUSE_ACTION));
        remoteViews.setOnClickPendingIntent(R.id.iv_next, retrievePlaybackAction(musicService, MusicService.NEXT_ACTION));
        remoteViews.setOnClickPendingIntent(R.id.iv_close, retrievePlaybackAction(musicService, MusicService.CLOSE_NOTITY_ACTION));

        final String albumName = musicService.getMusicName();
        final String artistName = musicService.getArtistName();
        final boolean isPlaying = musicService.isPlaying();
        int playButtonResId = isPlaying ? R.drawable.app_ic_pause : R.drawable.app_ic_play;

        remoteViews.setTextViewText(R.id.tv_music_name, albumName);
        remoteViews.setTextViewText(R.id.tv_music_artist, artistName);
        remoteViews.setImageViewResource(R.id.iv_play_or_pause, playButtonResId);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(musicService, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_notify_statusbar)
                .setContent(remoteViews)
                .setContentIntent(clickIntent)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setWhen(mNotificationPostTime)
                .setAutoCancel(true)
                .setUsesChronometer(false);

        ImageLoadController.getImageFromFull(musicService.getMusicCover(), new ImageLoadController.RequestImageCallback() {
            @Override
            public void onImageResult(Bitmap bitmap, int width, int height, String uri) {
                remoteViews.setImageViewBitmap(R.id.iv_music_album, bitmap);
            }

            @Override
            public void onImageFail(String uri) {

            }

            @Override
            public Postprocessor obtainPostprocessor() {
                return null;
            }
        });

        return builder.build();
    }

    private final PendingIntent retrievePlaybackAction(MusicService musicService, final String action) {
        final ComponentName serviceName = new ComponentName(musicService, MusicService.class);
        Intent intent = new Intent(action);
        intent.setComponent(serviceName);
        return PendingIntent.getService(musicService, 0, intent, 0);
    }

}
