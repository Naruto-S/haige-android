package com.music.player.haige.mvp.presenter;

import com.music.player.haige.mvp.contract.AppBasePresenter;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.Api;

import javax.inject.Inject;

/**
 * ================================================
 * Created by huangcong on 2018/2/27.
 * ================================================
 */

public class RankingFPresenter extends AppBasePresenter<MVContract.RankingModel, MVContract.RankingView> {

    @Inject
    public RankingFPresenter(MVContract.RankingModel model, MVContract.RankingView view) {
        super(model, view);
    }

    public void getRankingTags() {
        buildObservable(mModel.getRankingTags(), Api.Action.MUSIC_RANK_TAG, true);
    }
}
