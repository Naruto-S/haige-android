package com.music.player.haige.mvp.ui.setting.adapter;

import android.graphics.PorterDuff;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.hc.base.utils.ATEUtil;
import com.hc.base.utils.DensityUtil;
import com.hc.base.widget.fastscroller.FastScrollRecyclerView;
import com.hc.core.di.component.AppComponent;
import com.hc.core.http.imageloader.ImageLoader;
import com.hc.core.utils.CoreUtils;
import com.music.player.haige.R;
import com.music.player.haige.app.Constants;
import com.music.player.haige.app.EventBusTags;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.model.utils.HaigeUtil;
import com.music.player.haige.mvp.model.utils.NavigationUtil;
import com.music.player.haige.mvp.presenter.DownloadingFPresenter;
import com.music.player.haige.mvp.ui.music.service.MusicServiceConnection;
import com.music.player.haige.mvp.ui.music.viewholder.SongItemViewHolder;
import com.music.player.haige.mvp.ui.utils.DebugLogger;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;
import com.music.player.haige.mvp.ui.widget.dialog.SystemDialog;

import org.simple.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.schedulers.Schedulers;


public class DownloadingListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements FastScrollRecyclerView.SectionedAdapter {

    public int currentlyPlayingPosition;
    private List<Song> arrayList;
    private AppCompatActivity mContext;
    private boolean withHeader;
    private float topPlayScore;
    private String action;
    private AppComponent mAppComponent;
    private ImageLoader mImageLoader;//用于加载图片的管理类,默认使用 Glide,使用策略模式,可替换框架
    private DownloadingFPresenter mPresenter;

    public DownloadingListAdapter(AppCompatActivity context, List<Song> arrayList, String action, boolean withHeader, DownloadingFPresenter presenter) {
        if (arrayList == null) {
            this.arrayList = new ArrayList<>();
        } else {
            this.arrayList = arrayList;
        }
        this.mContext = context;
        this.withHeader = withHeader;
        this.action = action;
        this.mPresenter = presenter;

        //可以在任何可以拿到 Context 的地方,拿到 AppComponent,从而得到用 Dagger 管理的单例对象
        mAppComponent = CoreUtils.obtainAppComponentFromContext(mContext);
        mImageLoader = mAppComponent.imageLoader();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0 && withHeader) {
            return Type.TYPE_PLAY_SHUFFLE;
        } else {
            return Type.TYPE_SONG;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        switch (viewType) {
            case Type.TYPE_PLAY_SHUFFLE:
                View playShuffle = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.app_item_play_shuffle, viewGroup, false);
                ImageView imageView = playShuffle.findViewById(R.id.app_item_play_shuffle);
                imageView.getDrawable().setColorFilter(ResourceUtils.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
                viewHolder = new PlayShuffleViewHolder(playShuffle);
                break;
            case Type.TYPE_SONG:
                View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.app_item_download_ing, viewGroup, false);
                viewHolder = new ItemHolder(v);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int viewType = getItemViewType(position);
        switch (viewType) {
            case Type.TYPE_PLAY_SHUFFLE:
                break;
            case Type.TYPE_SONG:
                ItemHolder itemHolder = (ItemHolder) holder;
                Song localItem;
                if (withHeader) {
                    localItem = arrayList.get(position - 1);
                } else {
                    localItem = arrayList.get(position);
                }
                // 更新下载监听
                mPresenter.replaceDownloadListener(itemHolder, localItem);

                itemHolder.rank.setText(String.valueOf(position));
                itemHolder.title.setText(localItem.musicName);
                itemHolder.artist.setText(localItem.artistName);
                itemHolder.album.setText(localItem.albumName);

                if (MusicServiceConnection.getCurrentAudioId() == localItem.songId) {
                    itemHolder.title.setTextColor(ResourceUtils.getColor(R.color.colorAccent));
                    if (MusicServiceConnection.isPlaying()) {
                        itemHolder.rank.setVisibility(View.GONE);
                        itemHolder.musicVisualizer.setVisibility(View.VISIBLE);
                        itemHolder.musicVisualizer.setColor(ResourceUtils.getColor(R.color.colorAccent));
                    } else {
                        itemHolder.rank.setVisibility(View.VISIBLE);
                        itemHolder.rank.setTextColor(ResourceUtils.getColor(R.color.colorAccent));
                        itemHolder.musicVisualizer.setVisibility(View.GONE);
                    }
                } else {
                    itemHolder.rank.setVisibility(View.VISIBLE);
                    itemHolder.rank.setTextColor(ATEUtil.getThemeTextColorPrimary(mContext));
                    itemHolder.title.setTextColor(ATEUtil.getThemeTextColorPrimary(mContext));
                    itemHolder.musicVisualizer.setVisibility(View.GONE);
                }

                if (topPlayScore != 0) {
                    itemHolder.playScore.setVisibility(View.VISIBLE);
                    RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) itemHolder.playScore.getLayoutParams();
                    int full = DensityUtil.getScreenWidth(mContext);
                    layoutParams.width = (int) (full * (localItem.getPlayCountScore() / topPlayScore));
                }

                setOnPopupMenuListener(itemHolder, position);
                break;
        }
    }

    @Override
    public int getItemCount() {
        if (withHeader && arrayList.size() != 0) {
            return (null != arrayList ? arrayList.size() + 1 : 0);
        } else {
            return (null != arrayList ? arrayList.size() : 0);
        }
    }

    private void setOnPopupMenuListener(ItemHolder itemHolder, final int position) {

        final int realSongPosition;
        if (withHeader) {
            realSongPosition = position - 1;
        } else {
            realSongPosition = position;
        }

        itemHolder.popupMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final PopupMenu menu = new PopupMenu(mContext, v);
                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.popup_song_play_next:
                                MusicServiceConnection.playNext(arrayList.get(realSongPosition));
                                break;
                            case R.id.popup_song_goto_album:
                                NavigationUtil.goToAlbum(mContext, arrayList.get(realSongPosition).albumId,
                                        arrayList.get(realSongPosition).musicName);
                                break;
                            case R.id.popup_song_goto_artist:
                                NavigationUtil.goToArtist(mContext, arrayList.get(realSongPosition).artistId,
                                        arrayList.get(realSongPosition).artistName);
                                break;
                            case R.id.popup_song_addto_queue:
                                Song song = arrayList.get(realSongPosition);
                                MusicServiceConnection.addToQueue(song);
                                break;
                            case R.id.popup_song_delete:
                                try {
                                    long[] deleteIds = {arrayList.get(realSongPosition).songId};
                                    switch (action) {
                                        case Constants.NAVIGATE_PLAYLIST_FAVORITE:
                                            break;
                                        case Constants.NAVIGATE_PLAYLIST_RECENTPLAY:
                                            break;
                                        default:
                                            new SystemDialog().create(((AppCompatActivity) mContext).getSupportFragmentManager())
                                                    .setContent(mContext.getString(R.string.app_delete) + " " + arrayList.get(realSongPosition).musicName + " ?")
                                                    .setOk(ResourceUtils.resourceString(R.string.app_delete))
                                                    .setCancle(ResourceUtils.resourceString(R.string.app_cancel))
                                                    .setOnOkListener(() -> {
                                                        HaigeUtil.deleteTracks(mContext, deleteIds);
                                                        arrayList.remove(position);
                                                        notifyDataSetChanged();
                                                        EventBus.getDefault().post(new EventBusTags.MediaUpdateEvent());
                                                    })
                                                    .setOnCancelListener(() -> {
                                                    })
                                                    .show();
                                            break;
                                    }
                                } catch (Exception e) {
                                    DebugLogger.e(e);
                                }
                                break;
                        }
                        return false;
                    }
                });
                menu.inflate(R.menu.popup_song);
                menu.show();
            }
        });
    }

    public void setSongList(List<Song> arrayList) {
        this.arrayList = arrayList;
        if (arrayList.size() != 0) {
            this.topPlayScore = arrayList.get(0).getPlayCountScore();
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public String getSectionName(int position) {
        if (arrayList == null || arrayList.size() == 0 || (withHeader && position == 0))
            return "";

        if (withHeader) {
            position = position - 1;
        }
        Character ch = arrayList.get(position).musicName.charAt(0);
        if (Character.isDigit(ch)) {
            return "#";
        } else
            return Character.toString(ch);
    }

    public static class Type {
        public static final int TYPE_PLAY_SHUFFLE = 0;
        public static final int TYPE_SONG = 1;
    }

    public class ItemHolder extends SongItemViewHolder implements View.OnClickListener {

        public int downloadId;
        public Song song;

        public Button mDownloadBtn;
        public ProgressBar mDownloadPb;

        public ItemHolder(View view) {
            super(view);
            mDownloadBtn = view.findViewById(R.id.app_item_music_download_btn);
            mDownloadPb = view.findViewById(R.id.app_item_music_download_pb);
            mDownloadBtn.setOnClickListener(this);
            view.setOnClickListener(this);
        }

        public void updateDownloaded() {
            mDownloadPb.setMax(1);
            mDownloadPb.setProgress(1);

//            taskStatusTv.setText(R.string.tasks_manager_demo_status_completed);
            mDownloadBtn.setText(R.string.app_delete);
        }

        public void updateNotDownloaded(final int status, final long sofar, final long total) {
            if (sofar > 0 && total > 0) {
                final float percent = sofar
                        / (float) total;
                mDownloadPb.setMax(100);
                mDownloadPb.setProgress((int) (percent * 100));
            } else {
                mDownloadPb.setMax(1);
                mDownloadPb.setProgress(0);
            }

//            switch (status) {
//                case FileDownloadStatus.error:
//                    taskStatusTv.setText(R.string.tasks_manager_demo_status_error);
//                    break;
//                case FileDownloadStatus.paused:
//                    taskStatusTv.setText(R.string.tasks_manager_demo_status_paused);
//                    break;
//                default:
//                    taskStatusTv.setText(R.string.tasks_manager_demo_status_not_downloaded);
//                    break;
//            }
            mDownloadBtn.setText(R.string.app_start);
        }

        public void updateDownloading(final int status, final long sofar, final long total) {
            final float percent = sofar
                    / (float) total;
            mDownloadPb.setMax(100);
            mDownloadPb.setProgress((int) (percent * 100));

//            switch (status) {
//                case FileDownloadStatus.pending:
//                    mDownloadBtn.setText(R.string.tasks_manager_demo_status_pending);
//                    break;
//                case FileDownloadStatus.started:
//                    mDownloadBtn.setText(R.string.tasks_manager_demo_status_started);
//                    break;
//                case FileDownloadStatus.connected:
//                    mDownloadBtn.setText(R.string.tasks_manager_demo_status_connected);
//                    break;
//                case FileDownloadStatus.progress:
//                    mDownloadBtn.setText(R.string.tasks_manager_demo_status_progress);
//                    break;
//                default:
//                    mDownloadBtn.setText(DemoApplication.CONTEXT.getString(
//                            R.string.tasks_manager_demo_status_downloading, status));
//                    break;
//            }

            mDownloadBtn.setText(R.string.app_pause);
        }

        @Override
        public void onClick(View v) {
            int realPosition = getAdapterPosition() - 1;
            switch (v.getId()) {
                case R.id.app_item_music_download_btn:
                    mPresenter.onDownloadClick(this, this.song);
                    break;
                default:
                    Observable.empty()
                            .delay(100, TimeUnit.MILLISECONDS)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .doOnComplete(new Action() {
                                @Override
                                public void run() throws Exception {
                                    MusicServiceConnection.playAll(arrayList, realPosition, false);
                                }
                            })
                            .subscribe();
                    break;
            }
        }
    }

    public class PlayShuffleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public PlayShuffleViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    MusicServiceConnection.playAll(arrayList, -1, true);
                    Handler handler1 = new Handler();
                    handler1.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            notifyItemChanged(currentlyPlayingPosition);
                            notifyItemChanged(getAdapterPosition());
                            currentlyPlayingPosition = getAdapterPosition();
                        }
                    }, 50);
                }
            }, 100);
        }
    }

    public interface OnItemClickListener {
        void onDownloadClick(ItemHolder holder, Song song);
    }
}


