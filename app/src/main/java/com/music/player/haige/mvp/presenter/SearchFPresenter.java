package com.music.player.haige.mvp.presenter;

import com.music.player.haige.app.Constants;
import com.music.player.haige.app.HaigeApplication;
import com.music.player.haige.mvp.contract.AppBasePresenter;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.Api;
import com.music.player.haige.mvp.model.utils.PreferencesUtility;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import me.jessyan.rxerrorhandler.handler.ErrorHandleSubscriber;

/**
 * ================================================
 * Created by huangcong on 2018/3/3.
 * ================================================
 */

public class SearchFPresenter extends AppBasePresenter<MVContract.SearchModel, MVContract.SearchView> {

    @Inject
    public SearchFPresenter(MVContract.SearchModel model, MVContract.SearchView view) {
        super(model, view);
    }

    /**
     * 获取热词
     */
    public void loadHotWord() {
        buildObservable(mModel.loadHotWord(8), Api.Action.SEARCH_HOT_TAG);
    }

    /**
     * 加载历史搜索
     */
    public void loadHistoryWord(Constants.SEARCH searchType) {
        buildObservable(
                Observable.unsafeCreate((ObservableSource<ArrayList<String>>) observer -> {
                    try {
                        ArrayList<String> historyKeyWords = null;
                        if (Constants.SEARCH.NET == searchType) {
                            historyKeyWords = (ArrayList<String>) PreferencesUtility.getInstance(HaigeApplication.getInstance()).getSearchHistory();
                        } else if (Constants.SEARCH.LOCAL == searchType) {
                            historyKeyWords = (ArrayList<String>) PreferencesUtility.getInstance(HaigeApplication.getInstance()).getSearchLocalHistory();
                        } else if (Constants.SEARCH.RECENT == searchType) {
                            historyKeyWords = (ArrayList<String>) PreferencesUtility.getInstance(HaigeApplication.getInstance()).getSearchRecentHistory();
                        }
                        if (historyKeyWords == null) {
                            historyKeyWords = new ArrayList<>();
                        }
                        observer.onNext(historyKeyWords);
                    } catch (Exception e) {
                        observer.onError(e);
                    }
                    observer.onComplete();
                }), false)
                .subscribe(new ErrorHandleSubscriber<ArrayList<String>>(mErrorHandler) {
                    @Override
                    public void onNext(ArrayList<String> strings) {
                        if (strings != null) {
                            // 展示历史搜索记录
                            mRootView.onSuccess(Api.Action.SEARCH_HISTORY, strings);
                        } else {
                            mRootView.onError(Api.Action.SEARCH_HISTORY, Api.State.ERROR.getCode(), Api.State.ERROR.getMsg());
                        }
                    }
                });
    }

    public void saveHistoryWord(Constants.SEARCH searchType, final ArrayList<String> oldHistory, String newWord) {
        buildObservable(
                Observable.unsafeCreate((ObservableSource<ArrayList<String>>) observer -> {
                    ArrayList<String> historyKeyWord = oldHistory;
                    String keyWord = newWord;
                    if (historyKeyWord != null && historyKeyWord.size() > 10) {
                        //最多保存条数
                        historyKeyWord.remove(historyKeyWord.size() - 1);
                    }
                    if (historyKeyWord == null || historyKeyWord.size() == 0) {
                        historyKeyWord = new ArrayList<>();
                        historyKeyWord.add(keyWord);
                    } else {
                        if (!historyKeyWord.contains(keyWord)) {
                            historyKeyWord.add(0, keyWord);
                        }
                    }
                    if (Constants.SEARCH.NET == searchType) {
                        PreferencesUtility.getInstance(HaigeApplication.getInstance()).setSearchHistory(historyKeyWord);
                    } else if (Constants.SEARCH.LOCAL == searchType) {
                        PreferencesUtility.getInstance(HaigeApplication.getInstance()).setSearchLocalHistory(historyKeyWord);
                    } else if (Constants.SEARCH.RECENT == searchType) {
                        PreferencesUtility.getInstance(HaigeApplication.getInstance()).setSearchRecentHistory(historyKeyWord);
                    }
                    observer.onNext(historyKeyWord);
                    observer.onComplete();
                }), false)
                .subscribe(new ErrorHandleSubscriber<ArrayList<String>>(mErrorHandler) {
                    @Override
                    public void onNext(ArrayList<String> strings) {
                        if (strings != null) {
                            // 刷新历史搜索记录
                            mRootView.onSuccess(Api.Action.SEARCH_HISTORY, strings);
                        } else {
                            mRootView.onError(Api.Action.SEARCH_HISTORY, Api.State.ERROR.getCode(), Api.State.ERROR.getMsg());
                        }
                    }
                });
    }

    /**
     * 清空搜索历史记录
     */
    public void clearHistory(Constants.SEARCH searchType) {
        buildObservable(
                Observable.unsafeCreate(observer -> {
                    if (Constants.SEARCH.NET == searchType) {
                        PreferencesUtility.getInstance(HaigeApplication.getInstance()).clearSearchHistory();
                    } else if (Constants.SEARCH.LOCAL == searchType) {
                        PreferencesUtility.getInstance(HaigeApplication.getInstance()).clearSearchLocalHistory();
                    } else if (Constants.SEARCH.RECENT == searchType) {
                        PreferencesUtility.getInstance(HaigeApplication.getInstance()).clearSearchRecentHistory();
                    }
                    observer.onNext(true);
                    observer.onComplete();
                }), false)
                .subscribe(new ErrorHandleSubscriber<Object>(mErrorHandler) {
                    @Override
                    public void onNext(Object obj) {
                        // 刷新历史搜索记录
                        mRootView.onSuccess(Api.Action.SEARCH_HISTORY, null);
                    }
                });
    }

}
