package com.music.player.haige.mvp.presenter;

import com.music.player.haige.mvp.contract.AppBasePresenter;
import com.music.player.haige.mvp.contract.MVContract;

import javax.inject.Inject;

import me.jessyan.rxerrorhandler.core.RxErrorHandler;

/**
 * Created by Naruto on 2018/5/5.
 */

public class CommentFPresenter extends AppBasePresenter<MVContract.CommentModel, MVContract.CommentView> {

    public static final String TAG = CommentFPresenter.class.getSimpleName();

    @Inject
    RxErrorHandler mErrorHandler;


    @Inject
    public CommentFPresenter(MVContract.CommentModel model, MVContract.CommentView view) {
        super(model, view);
    }
}
