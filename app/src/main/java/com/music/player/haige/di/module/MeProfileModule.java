package com.music.player.haige.di.module;

import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.MeProfileModel;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Naruto on 2018/5/13.
 */
@Module
public class MeProfileModule {
    private MVContract.MeProfileView view;

    public MeProfileModule(MVContract.MeProfileView view){
        this.view = view;
    }

    @FragmentScope
    @Provides
    MVContract.MeProfileView provideView(){
        return view;
    }

    @FragmentScope
    @Provides
    MVContract.MeProfileModel provideModel(MeProfileModel model){
        return model;
    }
}
