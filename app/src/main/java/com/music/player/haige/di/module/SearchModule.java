package com.music.player.haige.di.module;

import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.SearchModel;

import dagger.Module;
import dagger.Provides;

/**
 * ================================================
 * Created by huangcong on 2018/3/3.
 * ================================================
 */
@Module
public class SearchModule  {
    private MVContract.SearchView view;

    public SearchModule(MVContract.SearchView view){
        this.view = view;
    }

    @FragmentScope
    @Provides
    MVContract.SearchView provideView(){
        return view;
    }

    @FragmentScope
    @Provides
    MVContract.SearchModel provideModel(SearchModel model){
        return model;
    }
}
