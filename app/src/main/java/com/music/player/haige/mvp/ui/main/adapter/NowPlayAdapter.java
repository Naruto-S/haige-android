package com.music.player.haige.mvp.ui.main.adapter;

import android.content.Context;
import android.support.design.widget.CheckableImageButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.music.player.haige.R;
import com.music.player.haige.app.image.loader.AvatarLoader;
import com.music.player.haige.app.image.widget.HaigeImageView;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.model.utils.CommonUtils;
import com.music.player.haige.mvp.ui.main.utils.MainViewUtils;
import com.music.player.haige.mvp.ui.main.widget.likebutton.LikeButton;
import com.music.player.haige.mvp.ui.main.widget.likebutton.OnLikeListener;
import com.music.player.haige.mvp.ui.music.service.MusicServiceConnection;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;
import com.music.player.haige.mvp.ui.utils.Utils;
import com.music.player.haige.mvp.ui.widget.LyricView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Naruto on 2018/5/12.
 */

public class NowPlayAdapter extends RecyclerView.Adapter<NowPlayAdapter.NowPlayViewHolder> {

    private Context mContext;
    private OnLikeListener mLikeListener;
    private View.OnClickListener mClickListener;
    private View.OnTouchListener mTouchListener;
    private LyricView.OnPlayerClickListener mOnPlayerClickListener;
    private ArrayList<Song> mPlaySongs = new ArrayList<>();

    public NowPlayAdapter(Context context, OnLikeListener likeListener, View.OnClickListener clickListener, View.OnTouchListener touchListener, LyricView.OnPlayerClickListener playerClickListener) {
        this.mContext = context;
        this.mLikeListener = likeListener;
        this.mClickListener = clickListener;
        this.mTouchListener = touchListener;
        this.mOnPlayerClickListener = playerClickListener;
    }

    @Override
    public NowPlayViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_fragment_now_play, parent, false);
        return new NowPlayViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(NowPlayViewHolder holder, int position) {
        holder.mMusicLyricTv.setText("");
        holder.mMusicLyricTv.setVisibility(View.GONE);

        Song song = mPlaySongs.get(position);

        if (Utils.isNotNull(song)) {
            if (MusicServiceConnection.isPlaying()) {
                holder.mPlayPauseView.setImageResource(R.drawable.ic_play_suspend);
            } else {
                holder.mPlayPauseView.setImageResource(R.drawable.ic_play_start);
            }

            MainViewUtils.updatePlayWebpBg(song.getBgImgWebp(), holder.mMusicWebpBgSdv);

            if (Utils.isNotNull(song.getUser())) {
                AvatarLoader.loadUserAvatar(song.getUser().getAvatarUrl(),
                        R.drawable.ic_main_default_avatar,
                        R.drawable.ic_main_default_avatar,
                        holder.mOperateCoverIv);
            }

            if (song.getTags() != null && song.getTags().size() > 0) {
                holder.mMusicTagTv.setVisibility(View.VISIBLE);
                holder.mMusicTagTv.setText(ResourceUtils.resourceString(R.string.app_music_tag, song.getTags().get(0)));
            } else {
                holder.mMusicTagTv.setVisibility(View.GONE);
            }
            holder.mMusicNameTv.setText(song.getMusicName());
            holder.mArtistNameTv.setText(ResourceUtils.resourceString(R.string.app_artist_name, song.getArtistName(mContext)));

            holder.mLikeCountIv.setLiked(song.isUserLiked);
            holder.mLikeCountIv.setOnLikeListener(mLikeListener);
            holder.mLikeCountTv.setText(CommonUtils.makeLikeCount(mContext, song.getLikeCount()));
            holder.mShareCountIv.setOnClickListener(mClickListener);
            holder.mTopPlayModeIv.setOnClickListener(mClickListener);
            holder.nPlayDownloadIv.setOnClickListener(mClickListener);
            holder.mInfoPreIv.setOnClickListener(mClickListener);
            holder.mPlayPauseView.setOnClickListener(mClickListener);
            holder.mInfoNextIv.setOnClickListener(mClickListener);
            holder.mOperateCoverIv.setOnClickListener(mClickListener);
            holder.mCommentIv.setOnClickListener(mClickListener);
            holder.mMusicTagTv.setOnClickListener(mClickListener);
            holder.mArtistNameTv.setOnClickListener(mClickListener);

            holder.mLikeViewGroup.setOnTouchListener(mTouchListener);

            long duration = MusicServiceConnection.duration();
            holder.mDurationSeekBar.setMax((int) duration);
            holder.mDurationTv.setText(CommonUtils.makeShortTimeString(mContext, duration));
        }
    }

    @Override
    public int getItemCount() {
        return mPlaySongs.size();
    }

    public void setPlaySongs(ArrayList<Song> playSongs) {
        mPlaySongs.clear();
        mPlaySongs.addAll(playSongs);
        notifyDataSetChanged();
    }

    public void setItem(int key, Song song) {
        mPlaySongs.set(key, song);
        notifyItemChanged(key);
    }

    class NowPlayViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_play_download)
        ImageView nPlayDownloadIv;

        @BindView(R.id.iv_play_random)
        ImageView mTopPlayModeIv;

        @BindView(R.id.view_group_like)
        ViewGroup mLikeViewGroup;

        @BindView(R.id.hiv_music_webp_bg)
        HaigeImageView mMusicWebpBgSdv;

        @BindView(R.id.app_layout_music_info_duration_cur_tv)
        TextView mDurationCurTv;

        @BindView(R.id.app_layout_music_info_duration_sb)
        SeekBar mDurationSeekBar;

        @BindView(R.id.app_layout_music_info_duration_tv)
        TextView mDurationTv;

        @BindView(R.id.app_layout_music_operate_cover_iv)
        HaigeImageView mOperateCoverIv;

        @BindView(R.id.app_layout_music_other_info_tag_tv)
        TextView mMusicTagTv;

        @BindView(R.id.app_layout_music_other_info_artist_tv)
        TextView mArtistNameTv;

        @BindView(R.id.app_layout_music_other_info_music_name_tv)
        TextView mMusicNameTv;

        @BindView(R.id.app_layout_music_operate_like_iv)
        LikeButton mLikeCountIv;

        @BindView(R.id.app_layout_music_operate_like_count_tv)
        TextView mLikeCountTv;

        @BindView(R.id.app_layout_music_operate_share_iv)
        CheckableImageButton mShareCountIv;

        @BindView(R.id.iv_play_last)
        ImageView mInfoPreIv;

        @BindView(R.id.iv_play_start)
        ImageView mPlayPauseView;

        @BindView(R.id.iv_play_next)
        ImageView mInfoNextIv;

        @BindView(R.id.app_layout_music_operate_danmu_iv)
        CheckableImageButton mCommentIv;

        @BindView(R.id.tv_music_lyric)
        TextView mMusicLyricTv;

        public NowPlayViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
