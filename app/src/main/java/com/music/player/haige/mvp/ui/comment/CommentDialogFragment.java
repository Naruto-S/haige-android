package com.music.player.haige.mvp.ui.comment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.hc.core.di.component.AppComponent;
import com.hc.core.utils.DeviceUtils;
import com.music.player.haige.R;
import com.music.player.haige.app.base.AppBaseDialogFragment;
import com.music.player.haige.di.component.DaggerCommentDialogComponent;
import com.music.player.haige.di.module.CommentDialogModule;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.entity.comment.CommentBean;
import com.music.player.haige.mvp.presenter.CommentFPresenter;
import com.music.player.haige.mvp.ui.comment.adapter.CommentAdapter;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;
import com.music.player.haige.mvp.ui.utils.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

public class CommentDialogFragment extends AppBaseDialogFragment<CommentFPresenter> implements MVContract.CommentView {

    @BindView(R.id.recycler_comment)
    RecyclerView mRecyclerView;

    @BindView(R.id.tv_comment_count)
    TextView mCommentCountTv;

    @BindView(R.id.tv_no_more)
    TextView mNoMoreTv;

    private OnCommentListener mCommentListener;
    private ArrayList<CommentBean> mCommentList = new ArrayList<>();
    private CommentAdapter mAdapter;

    public static CommentDialogFragment newInstance() {
        CommentDialogFragment fragment = new CommentDialogFragment();
        return fragment;
    }

    public CommentDialogFragment setCommentListener(OnCommentListener commentListener) {
        mCommentListener = commentListener;
        return this;
    }

    @Override
    public void onStart() {
        super.onStart();

        Dialog dialog = getDialog();
        dialog.setCanceledOnTouchOutside(true);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams params = window.getAttributes();
        params.gravity = Gravity.BOTTOM;
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = (int) (DeviceUtils.getScreenHeight(getActivity()) * 0.7f);
        window.setAttributes(params);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    @Override
    public void setupFragmentComponent(AppComponent component) {
        DaggerCommentDialogComponent
                .builder()
                .appComponent(component)
                .commentDialogModule(new CommentDialogModule(this))
                .build()
                .inject(this);
    }

    @Override
    public View initView(LayoutInflater inflater, ViewGroup group, Bundle bundle) {
        return inflater.inflate(R.layout.layout_barrage_comment_list, group, false);
    }

    @Override
    public void initData(Bundle bundle) {
        initRecyclerView();
    }

    private void initRecyclerView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(null);
        mAdapter = new CommentAdapter();
        mRecyclerView.setAdapter(mAdapter);

        refreshData();
    }

    private void refreshData() {
        if (Utils.isNotNull(mCommentCountTv)) {
            mCommentCountTv.setText(ResourceUtils.resourceString(R.string.barrage_comment_count, mCommentList.size()));
        }

        if (Utils.isNotNull(mAdapter)) {
            mAdapter.setCommentList(mCommentList);
        }

        if (Utils.isNotEmptyCollection(mCommentList)) {
            if (Utils.isNotNull(mRecyclerView) && Utils.isNotNull(mNoMoreTv)) {
                mRecyclerView.setVisibility(View.VISIBLE);
                mNoMoreTv.setVisibility(View.GONE);
            }
        } else {
            if (Utils.isNotNull(mRecyclerView) && Utils.isNotNull(mNoMoreTv)) {
                mRecyclerView.setVisibility(View.GONE);
                mNoMoreTv.setVisibility(View.VISIBLE);
            }
        }
    }

    @OnClick(R.id.tv_comment_edit)
    public void onEditComment() {
        if (Utils.isNotNull(mCommentListener)) {
            mCommentListener.onEditComment();
            onClose();
        }
    }

    @OnClick(R.id.iv_close)
    public void onClose() {
        getDialog().dismiss();
    }

    public void setCommentList(ArrayList<CommentBean> commentList) {
        mCommentList.clear();
        mCommentList.addAll(commentList);

        refreshData();
    }

    public interface OnCommentListener {

        void onEditComment();

    }
}
