package com.music.player.haige.di.component;

import com.hc.core.di.component.AppComponent;
import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.di.module.FoldersModule;
import com.music.player.haige.mvp.ui.music.fragment.FoldersFragment;

import dagger.Component;

/**
 * ================================================
 * Created by huangcong on 2018/3/2.
 * ================================================
 */
@FragmentScope
@Component(modules = {FoldersModule.class}, dependencies = AppComponent.class)
public interface FoldersComponent {

    void inject(FoldersFragment fragment);
}
