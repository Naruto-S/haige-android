package com.music.player.haige.mvp.presenter;

import android.text.TextUtils;

import com.hc.core.utils.RxLifecycleUtils;
import com.liulishuo.filedownloader.FileDownloader;
import com.liulishuo.filedownloader.model.FileDownloadStatus;
import com.liulishuo.filedownloader.util.FileDownloadUtils;
import com.music.player.haige.app.GlobalConfiguration;
import com.music.player.haige.app.HaigeApplication;
import com.music.player.haige.app.base.pref.UserPrefHelper;
import com.music.player.haige.mvp.contract.AppBasePresenter;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.model.entity.music.SongDao;
import com.music.player.haige.mvp.model.entity.recommend.RecommendResponse;
import com.music.player.haige.mvp.ui.music.utils.MusicUtils;
import com.music.player.haige.mvp.ui.utils.Utils;

import java.io.File;
import java.util.Date;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;
import me.jessyan.rxerrorhandler.handler.ErrorHandleSubscriber;

public class LikeMusicPresenter extends AppBasePresenter<MVContract.LikeMusicModel, MVContract.LikeMusicView> {

    @Inject
    RxErrorHandler mErrorHandler;

    private SongDao mDownloadSongDao; // 音乐下载记录

    @Inject
    public LikeMusicPresenter(MVContract.LikeMusicModel model, MVContract.LikeMusicView view) {
        super(model, view);

        mDownloadSongDao = GlobalConfiguration.sDownloadDaoSession.getSongDao();
    }

    public void getLikedList(String userId, int pageNo, int pageSize, boolean refresh) {
        mModel.getLikedList(userId, pageNo, pageSize)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> {
                    if (Utils.isNotNull(mRootView)) {
                        mRootView.loadRequestStarted();
                    }
                })
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .subscribe(new ErrorHandleSubscriber<RecommendResponse>(mErrorHandler) {
                    @Override
                    public void onNext(RecommendResponse response) {
                        if (Utils.isNotNull(mRootView)) {
                            if (Utils.isNotNull(response) && Utils.isNotEmptyCollection(response.getData())) {
                                if (refresh) {
                                    mRootView.showRefresh(response.getData());
                                } else {
                                    mRootView.showLoadMore(response.getData());
                                }
                            } else {
                                mRootView.loadEnded();
                            }
                        }
                        mRootView.loadRequestCompleted();
                    }

                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                        if (Utils.isNotNull(mRootView)) {
                            mRootView.loadRequestCompleted();
                            mRootView.showErrorNetwork();
                        }
                    }
                });
    }

    public boolean downloadMusic(Song song) {
        if (song.isLocal) {
            mRootView.showMessage(HaigeApplication.getInstance().getString(com.music.player.haige.R.string.app_download_local_music));
            return false;
        }
        String url = song.musicPath;
        if (TextUtils.isEmpty(url)) {
            return false;
        }
        // 记录到下载数据库
        if (mDownloadSongDao.load(song.songId) == null) {
            song.setDownloadDate(new Date());
            mDownloadSongDao.insert(song);
        }
        String path = MusicUtils.getDownloadPath(song);
        int id = FileDownloadUtils.generateId(url, path);
        int status = FileDownloader.getImpl().getStatus(id, path);
        System.out.println("===>>> id : " + id + ", status : " + status);
        if (status == FileDownloadStatus.completed || new File(path).exists()) {
            mRootView.showMessage(HaigeApplication.getInstance().getString(com.music.player.haige.R.string.app_download_finish));
            return false;
        } else if (status == FileDownloadStatus.pending || status == FileDownloadStatus.started ||
                status == FileDownloadStatus.connected || status == FileDownloadStatus.progress) {
            mRootView.showMessage(HaigeApplication.getInstance().getString(com.music.player.haige.R.string.app_download_ing));
            return false;
        } else {
            UserPrefHelper.putDownloadMusicCount(UserPrefHelper.getDownloadMusicCount() + 1);
            mRootView.showMessage(HaigeApplication.getInstance().getString(com.music.player.haige.R.string.app_download_join));
        }
        if (!FileDownloader.getImpl().isServiceConnected()) {
            // 开启下载服务
            FileDownloader.getImpl().bindService();
        }
        // 开始下载
        FileDownloader.getImpl().create(url).setPath(path).start();
        return true;
    }
}
