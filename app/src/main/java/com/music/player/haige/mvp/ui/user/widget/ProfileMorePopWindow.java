package com.music.player.haige.mvp.ui.user.widget;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.music.player.haige.R;

/**
 * Created by Qiaobangzhu on 2017/8/28 0028.
 */

public class ProfileMorePopWindow extends PopupWindow {

    private OnProfileMoreClickListener mListener;
    private Activity mActivity;

    public interface OnProfileMoreClickListener {
        void onSettingsClick();
        void onEditClick();
    }

    public ProfileMorePopWindow(Context context) {
        super(context);
        mActivity = (Activity) context;
        LayoutInflater inflater = LayoutInflater.from(context);
        View rootView = inflater.inflate(R.layout.layout_me_profile_more_pop_window, null);
        TextView mReportTv = rootView.findViewById(R.id.tv_settings);
        TextView mBlockTv = rootView.findViewById(R.id.tv_edit);
        this.setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setTouchable(true);
        this.setFocusable(true);
        this.setOutsideTouchable(true);
        this.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_me_profile_more_popwindow));
        this.setContentView(rootView);

        mReportTv.setOnClickListener(v -> {
            if (mListener != null) {
                mListener.onSettingsClick();
            }
            ProfileMorePopWindow.this.dismiss();
        });

        mBlockTv.setOnClickListener(v -> {
            if (mListener != null) {
                mListener.onEditClick();
            }
            ProfileMorePopWindow.this.dismiss();
        });
    }

    @Override
    public void dismiss() {
        super.dismiss();
        setBackgroundAlpha(1f);
    }

    public void showPopWindow(View targetView) {
        if (!this.isShowing()) {
            setBackgroundAlpha(0.5f);
            this.showAsDropDown(targetView);
        } else {
            this.dismiss();
        }
    }

    public void setProfileMoreClickListener(OnProfileMoreClickListener listener) {
        mListener = listener;
    }

    private void setBackgroundAlpha(float alpha) {
        if (mActivity != null) {
            WindowManager.LayoutParams params = mActivity.getWindow().getAttributes();
            params.alpha = alpha;
            mActivity.getWindow().setAttributes(params);
        }
    }
}
