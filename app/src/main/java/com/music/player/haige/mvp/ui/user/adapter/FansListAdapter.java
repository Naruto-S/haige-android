package com.music.player.haige.mvp.ui.user.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.music.player.haige.R;
import com.music.player.haige.app.base.recyclerview.BaseQuickAdapter;
import com.music.player.haige.app.base.recyclerview.BaseViewHolder;
import com.music.player.haige.app.image.loader.AvatarLoader;
import com.music.player.haige.app.image.widget.HaigeImageView;
import com.music.player.haige.mvp.model.entity.user.UserBean;
import com.music.player.haige.mvp.ui.user.utils.UserViewUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FansListAdapter extends BaseQuickAdapter<UserBean, FansListAdapter.FansListViewHolder> {

    private int mFollowType = 0;

    public FansListAdapter(Context context, int type) {
        super(context, R.layout.item_follow_list, null);
        mFollowType = type;
    }

    @Override
    public FansListViewHolder onCreateDefViewHolder(ViewGroup parent, int viewType) {
        FansListViewHolder viewHolder = new FansListViewHolder(mLayoutInflater.inflate(mLayoutResId, parent, false));
        viewHolder.addOnClickListener(R.id.tv_follow);
        return viewHolder;
    }

    @Override
    protected void convert(FansListViewHolder holder, UserBean item) {
        AvatarLoader.loadUserAvatar(item.getAvatarUrl(),
                R.drawable.ic_main_default_avatar,
                R.drawable.ic_main_default_avatar,
                holder.mAvatarHiv);

        holder.mNameTv.setText(item.getNickName());
        holder.mDescTv.setText(item.getDesc());

        UserViewUtils.switchFollowStatus(holder.mFollowTv, item, mFollowType);
    }

    class FansListViewHolder extends BaseViewHolder {

        @BindView(R.id.hiv_avatar)
        HaigeImageView mAvatarHiv;

        @BindView(R.id.tv_name)
        TextView mNameTv;

        @BindView(R.id.tv_desc)
        TextView mDescTv;

        @BindView(R.id.tv_follow)
        TextView mFollowTv;

        public FansListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
