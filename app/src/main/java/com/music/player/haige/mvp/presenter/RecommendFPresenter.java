package com.music.player.haige.mvp.presenter;

import android.text.TextUtils;

import com.hc.core.utils.RxLifecycleUtils;
import com.liulishuo.filedownloader.FileDownloader;
import com.liulishuo.filedownloader.model.FileDownloadStatus;
import com.liulishuo.filedownloader.util.FileDownloadUtils;
import com.music.player.haige.R;
import com.music.player.haige.app.GlobalConfiguration;
import com.music.player.haige.app.HaigeApplication;
import com.music.player.haige.app.base.pref.UserPrefHelper;
import com.music.player.haige.mvp.contract.AppBasePresenter;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.Api;
import com.music.player.haige.mvp.model.entity.action.CommentLikeRequest;
import com.music.player.haige.mvp.model.entity.comment.CommentRequest;
import com.music.player.haige.mvp.model.entity.comment.CommentResponse;
import com.music.player.haige.mvp.model.entity.comment.SendCommentResponse;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.model.entity.music.SongDao;
import com.music.player.haige.mvp.model.entity.music.WebpBgResponse;
import com.music.player.haige.mvp.model.utils.LyricUtil;
import com.music.player.haige.mvp.ui.music.service.MusicServiceConnection;
import com.music.player.haige.mvp.ui.music.utils.MusicUtils;
import com.music.player.haige.mvp.ui.utils.RxJavaUtils;
import com.music.player.haige.mvp.ui.utils.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import me.jessyan.rxerrorhandler.handler.ErrorHandleSubscriber;

/**
 * ================================================
 * Created by huangcong on 2018/2/27.
 * ================================================
 */

public class RecommendFPresenter extends AppBasePresenter<MVContract.RecommendModel, MVContract.RecommendView> {

    private static final long UPDATE_DELAY = 500;

    private SongDao mFavoriteSongDao; // 音乐收藏记录
    private SongDao mDownloadSongDao; // 音乐下载记录
    private Disposable mUpateMusicTimeDisp;

    @Inject
    public RecommendFPresenter(MVContract.RecommendModel model, MVContract.RecommendView view) {
        super(model, view);
        mFavoriteSongDao = GlobalConfiguration.sFavoriteDaoSession.getSongDao();
        mDownloadSongDao = GlobalConfiguration.sDownloadDaoSession.getSongDao();
    }

    /**
     * 加载音乐推荐数据
     */
    public void loadRecommendMusic(int pageNum, int pageSize, boolean refresh) {
        mModel.loadRecommendMusic(pageNum, pageSize)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> {
                    if (Utils.isNotNull(mRootView)) {
                        mRootView.loadRequestStarted();
                    }
                })
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .subscribe(new ErrorHandleSubscriber<ArrayList<Song>>(mErrorHandler) {
                    @Override
                    public void onNext(ArrayList<Song> response) {
                        if (Utils.isNotNull(mRootView)) {
                            if (Utils.isNotEmptyCollection(response)) {
                                if (refresh) {
                                    mRootView.showRefresh(response);
                                } else {
                                    mRootView.showLoadMore(response);
                                }
                            } else {
                                mRootView.loadEnded();
                            }
                        }
                        mRootView.loadRequestCompleted();
                    }

                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                        if (Utils.isNotNull(mRootView)) {
                            mRootView.loadRequestCompleted();
                            mRootView.showErrorNetwork();
                        }
                    }
                });
    }

    public void getLyricFile(Observable<File> observable) {
        buildObservable(observable)
                .subscribe(new ErrorHandleSubscriber<File>(mErrorHandler) {
                    @Override
                    public void onNext(File response) {
                        if (response != null) {
                            mRootView.onSuccess(Api.Action.MUSIC_LRC, response);
                        } else {
                            mRootView.onError(Api.Action.MUSIC_LRC, Api.State.NORMAL.getCode(), Api.State.NORMAL.getMsg());
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        //super.onError(t);
                        mRootView.onError(Api.Action.MUSIC_LRC, "-1", t.getMessage());
                    }
                });
    }

    public void getLyricFile(String title, String artist, long duration) {
        if (LyricUtil.isLrcFileExist(title, artist)) {
            getLyricFile(LyricUtil.getLocalLyricFile(title, artist));
        } else {
            getLyricFile(mModel.getLyricFile(title, artist, duration));
        }
    }

    /**
     * 点赞统计
     */
    public void countLike(Song song) {
        buildObservable(mModel.countLike(song.songId), Api.Action.MUSIC_COUNT_LIKE, song);
    }

    /**
     * 获取动态背景图
     */
    public void getMusicWebpBg(String type) {
        buildObservable(mModel.getMusicWebpBg(type))
                .subscribe(new ErrorHandleSubscriber<WebpBgResponse>(mErrorHandler) {
                    @Override
                    public void onNext(WebpBgResponse response) {
                        if (response != null) {
                            mRootView.onSuccess(Api.Action.MUSIC_WEBP_BG, response.getData());
                        } else {
                            mRootView.onError(Api.Action.MUSIC_WEBP_BG, Api.State.NORMAL.getCode(), Api.State.NORMAL.getMsg());
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                        mRootView.onError(Api.Action.USER_LOGIN, "-1", t.getMessage());
                    }
                });
    }

    /**
     * 添加点赞音乐记录
     */
    public void recordFavoriteSong(Song song) {
        if (!isFavoriteSong(song)) {
            mFavoriteSongDao.insert(song);
        }
    }

    /**
     * 删除点赞音乐记录
     */
    public boolean deleteFavoriteSong(Song song) {
        if (isFavoriteSong(song)) {
            mFavoriteSongDao.delete(song);
            return true;
        } else {
            return false;
        }
    }

    public boolean isFavoriteSong(Song song) {
        return mFavoriteSongDao.load(song.songId) != null;
    }

    /**
     * 分享统计
     */
    public void countShare(long musicId) {
        buildObservable(mModel.countShare(musicId), Api.Action.MUSIC_COUNT_SHARE);
    }

    /**
     * 下载统计
     */
    public void countDownload(long musicId) {
        buildObservable(mModel.countDownload(musicId), Api.Action.MUSIC_COUNT_DOWNLOAD);
    }

    public boolean downloadMusic(Song song) {
        if (song.isLocal) {
            mRootView.showMessage(HaigeApplication.getInstance().getString(R.string.app_download_local_music));
            return false;
        }
        String url = song.musicPath;
        if (TextUtils.isEmpty(url)) {
            return false;
        }
        // 记录到下载数据库
        if (mDownloadSongDao.load(song.songId) == null) {
            song.setDownloadDate(new Date());
            mDownloadSongDao.insert(song);
        }
        String path = MusicUtils.getDownloadPath(song);
        int id = FileDownloadUtils.generateId(url, path);
        int status = FileDownloader.getImpl().getStatus(id, path);
        System.out.println("===>>> id : " + id + ", status : " + status);
        if (status == FileDownloadStatus.completed || new File(path).exists()) {
            mRootView.showMessage(HaigeApplication.getInstance().getString(R.string.app_download_finish));
            return false;
        } else if (status == FileDownloadStatus.pending || status == FileDownloadStatus.started ||
                status == FileDownloadStatus.connected || status == FileDownloadStatus.progress) {
            mRootView.showMessage(HaigeApplication.getInstance().getString(R.string.app_download_ing));
            return false;
        } else {
            UserPrefHelper.putDownloadMusicCount(UserPrefHelper.getDownloadMusicCount() + 1);
            mRootView.showMessage(HaigeApplication.getInstance().getString(R.string.app_download_join));
        }
        if (!FileDownloader.getImpl().isServiceConnected()) {
            // 开启下载服务
            FileDownloader.getImpl().bindService();
        }
        // 开始下载
        FileDownloader.getImpl().create(url).setPath(path).start();
        return true;
    }

    public void startUpateMusicTime() {
        RxJavaUtils.dispose(mUpateMusicTimeDisp);

        mUpateMusicTimeDisp = Observable.interval(0, UPDATE_DELAY, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aLong -> {
                    if (Utils.isNotNull(mRootView)) {
                        mRootView.onUpateMusicTime();
                    }
                });
    }

    public void sendMusicComment(CommentRequest request) {
        buildObservable(mModel.sendMusicComment(request))
                .subscribe(new ErrorHandleSubscriber<SendCommentResponse>(mErrorHandler) {
                    @Override
                    public void onNext(SendCommentResponse response) {
                        if (response != null) {
                            response.setRequest(request);
                            mRootView.onSuccess(Api.Action.SEND_MUSIC_COMMENT, response);
                        } else {
                            mRootView.onError(Api.Action.SEND_MUSIC_COMMENT, Api.State.NORMAL.getCode(), Api.State.NORMAL.getMsg());
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                        mRootView.onError(Api.Action.SEND_MUSIC_COMMENT, "-1", t.getMessage());
                    }
                });
    }

    public void getMusicComment(long musicId, int pageSize, String lastId) {
        buildObservable(mModel.getMusicComment(musicId, pageSize, lastId))
                .subscribe(new ErrorHandleSubscriber<CommentResponse>(mErrorHandler) {
                    @Override
                    public void onNext(CommentResponse response) {
                        if (response != null && musicId == MusicServiceConnection.getCurrentAudioId()) {
                            mRootView.onSuccess(Api.Action.GET_MUSIC_COMMENT, response);
                        } else {
                            mRootView.onError(Api.Action.GET_MUSIC_COMMENT, Api.State.NORMAL.getCode(), Api.State.NORMAL.getMsg());
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                        mRootView.onError(Api.Action.GET_MUSIC_COMMENT, "-1", t.getMessage());
                    }
                });
    }

    public void getCommentSub(String commentId, int pageSize, int pageNo, int position) {
        buildObservable(mModel.getCommentSub(commentId, pageSize, pageNo))
                .subscribe(new ErrorHandleSubscriber<CommentResponse>(mErrorHandler) {
                    @Override
                    public void onNext(CommentResponse response) {
                        if (response != null) {
                            response.setPosition(position);
                            mRootView.onSuccess(Api.Action.GET_COMMENT_SUB, response);
                        } else {
                            mRootView.onError(Api.Action.GET_COMMENT_SUB, Api.State.NORMAL.getCode(), Api.State.NORMAL.getMsg());
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                        mRootView.onError(Api.Action.GET_COMMENT_SUB, "-1", t.getMessage());
                    }
                });
    }

    public void deleteComment(CommentRequest request) {
        buildObservable(mModel.deleteComment(request), Api.Action.DELETE_COMMENT);
    }

    public void commentLike(CommentLikeRequest request) {
        buildObservable(mModel.commentLike(request), Api.Action.LIKE_COMMENT);
    }

    public boolean isDisposedUpateMusicTime() {
        return RxJavaUtils.isDisposed(mUpateMusicTimeDisp);
    }

    public void onStopUpateMusicTime() {
        RxJavaUtils.dispose(mUpateMusicTimeDisp);
    }
}
