package com.music.player.haige.mvp.ui.music.utils;

import com.music.player.haige.mvp.model.entity.music.Song;

import static com.music.player.haige.mvp.ui.music.service.proxy.utils.ProxyConstants.DOWNLOAD_PATH;

public class MusicUtils {

    public static String getPathSuffix(String path) {
        return path.substring(path.lastIndexOf("."), path.length());
    }

    public static String getMusicFileName(String path) {
        return path.substring(path.lastIndexOf("/"), path.length());
    }

    public static String getDownloadPath(Song song) {
        return getDownloadPath(song, false);
    }

    public static String getDownloadPath(Song song, boolean isShort) {
        String name  = isShort ? "嗨歌音乐_" + song.getMusicName() : "嗨歌音乐_铃声_" + song.getMusicName();
        return DOWNLOAD_PATH + name + getPathSuffix(song.getMusicPath());
    }
}
