package com.music.player.haige.mvp.ui.music.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.hc.base.utils.ATEUtil;
import com.hc.base.widget.fastscroller.FastScrollRecyclerView;
import com.hc.core.http.imageloader.glide.GlideCore;
import com.music.player.haige.R;
import com.music.player.haige.app.Constants;
import com.music.player.haige.app.EventBusTags;
import com.music.player.haige.mvp.model.entity.music.Album;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.model.respository.dataloader.AlbumSongLoader;
import com.music.player.haige.mvp.model.utils.ColorUtil;
import com.music.player.haige.mvp.model.utils.CommonUtils;
import com.music.player.haige.mvp.model.utils.HaigeUtil;
import com.music.player.haige.mvp.model.utils.NavigationUtil;
import com.music.player.haige.mvp.model.utils.PreferencesUtility;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;
import com.music.player.haige.mvp.ui.widget.dialog.SystemDialog;

import org.simple.eventbus.EventBus;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;


public class AlbumAdapter extends RecyclerView.Adapter<AlbumAdapter.ItemHolder> implements FastScrollRecyclerView.SectionedAdapter {

    private List<Album> arrayList;
    private Activity mContext;
    private boolean isGrid;
    private String action;

    public AlbumAdapter(Activity context, String action) {
        this.mContext = context;
        this.isGrid = PreferencesUtility.getInstance(mContext).isAlbumsInGrid();
        this.action = action;
    }

    @Override
    public ItemHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        if (isGrid) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.app_item_list_grid_layout_item, viewGroup, false);
            return new ItemHolder(v);
        } else {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.app_item_music, viewGroup, false);
            return new ItemHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(final ItemHolder itemHolder, final int i) {
        Album localItem = arrayList.get(i);

        itemHolder.title.setText(localItem.title);
        itemHolder.artist.setText(localItem.artistName);
        itemHolder.songcount.setText(HaigeUtil.makeLabel(mContext, R.plurals.Nsongs, localItem.songCount));

        GlideCore.with(itemHolder.itemView.getContext()).asBitmap()
                .load(HaigeUtil.getAlbumArtUri(localItem.id).toString())
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .into(new SimpleTarget<Bitmap>() {

                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        if (isGrid) {
                            itemHolder.footer.setBackgroundColor(ATEUtil.getThemeAlbumDefaultPaletteColor(mContext));
                        }
                        itemHolder.albumArt.setImageDrawable(ATEUtil.getDefaultAlbumDrawable(mContext));
                        itemHolder.title.setTextColor(ATEUtil.getThemeTextColorPrimary(mContext));
                        itemHolder.artist.setTextColor(ATEUtil.getThemeTextColorSecondly(mContext));
                        itemHolder.songcount.setTextColor(ATEUtil.getThemeTextColorSecondly(mContext));
                        itemHolder.popupMenu.setColorFilter(mContext.getResources().getColor(R.color.background_floating_material_dark));
                    }

                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        if (isGrid) {
                            new Palette.Builder(resource).generate(new Palette.PaletteAsyncListener() {
                                @Override
                                public void onGenerated(Palette palette) {
                                    Palette.Swatch swatch = ColorUtil.getMostPopulousSwatch(palette);
                                    if (swatch != null) {
                                        int color = swatch.getRgb();
                                        itemHolder.footer.setBackgroundColor(color);

                                        int detailColor = swatch.getTitleTextColor();
                                        itemHolder.albumArt.setImageBitmap(resource);
                                        itemHolder.title.setTextColor(ColorUtil.getOpaqueColor(detailColor));
                                        itemHolder.artist.setTextColor(detailColor);
                                        itemHolder.songcount.setTextColor(detailColor);
                                        itemHolder.popupMenu.setColorFilter(detailColor);
                                    }
                                }
                            });
                        } else {
                            itemHolder.albumArt.setImageBitmap(resource);
                        }
                    }
                });

        if (CommonUtils.isLollipop())
            itemHolder.albumArt.setTransitionName("transition_album_art" + i);

        setOnPopupMenuListener(itemHolder, i);

    }

    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
    }

    public void setAlbumsList(List<Album> arrayList) {
        this.arrayList = arrayList;
        notifyItemRangeInserted(0, arrayList.size());
    }

    @NonNull
    @Override
    public String getSectionName(int position) {
        if (arrayList == null || arrayList.size() == 0)
            return "";
        Character ch = arrayList.get(position).title.charAt(0);
        if (Character.isDigit(ch)) {
            return "#";
        } else
            return Character.toString(ch);
    }

    private void setOnPopupMenuListener(final AlbumAdapter.ItemHolder itemHolder, final int position) {
        itemHolder.popupMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final PopupMenu menu = new PopupMenu(mContext, v);
                int adapterPosition = itemHolder.getAdapterPosition();
                final Album album = arrayList.get(adapterPosition);
                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.popup_album_addto_queue:
                                getSongListIdByAlbum(arrayList.get(position).id)
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(new Consumer<long[]>() {
                                            @Override
                                            public void accept(long[] ids) throws Exception {
//                                                MusicServiceConnection.addToQueue(mContext, ids, -1, HaigeUtil.IdType.NA);
                                            }
                                        });
                                break;
                            case R.id.popup_album_addto_playlist:
                                getSongListIdByAlbum(arrayList.get(position).id)
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(ids -> {
                                        });
                                break;
                            case R.id.popup_album_goto_artist:
                                NavigationUtil.goToArtist(mContext, arrayList.get(position).artistId,
                                        arrayList.get(position).artistName);
                                break;
                            case R.id.popup_artist_delete:
                                switch (action) {
                                    case Constants.NAVIGATE_PLAYLIST_FAVORITE:
                                        getSongListIdByAlbum(album.id)
                                                .subscribeOn(Schedulers.io())
                                                .observeOn(AndroidSchedulers.mainThread())
                                                .subscribe(ids -> {
                                                });
                                        break;
                                    case Constants.NAVIGATE_PLAYLIST_RECENTPLAY:
                                        getSongListIdByAlbum(album.id)
                                                .subscribeOn(Schedulers.io())
                                                .observeOn(AndroidSchedulers.mainThread())
                                                .subscribe(ids -> {
                                                });
                                        break;
                                    default:
                                        AlbumSongLoader.getSongsForAlbum(mContext, arrayList.get(position).id)
                                                .subscribeOn(Schedulers.io())
                                                .observeOn(AndroidSchedulers.mainThread())
                                                .subscribe(new Consumer<List<Song>>() {
                                                    @Override
                                                    public void accept(List<Song> songs) throws Exception {
                                                        long[] ids = new long[songs.size()];
                                                        int i = 0;
                                                        for (Song song : songs) {
                                                            ids[i] = song.songId;
                                                            i++;
                                                        }
                                                        if (ids.length == 1) {
                                                            new SystemDialog().create(((AppCompatActivity) mContext).getSupportFragmentManager())
                                                                    .setContent(mContext.getString(R.string.app_delete) + " " + songs.get(0).musicName + " ?")
                                                                    .setOk(ResourceUtils.resourceString(R.string.app_delete))
                                                                    .setCancle(ResourceUtils.resourceString(R.string.app_cancel))
                                                                    .setOnOkListener(() -> {
                                                                        HaigeUtil.deleteTracks(mContext, ids);
                                                                        arrayList.remove(position);
                                                                        notifyDataSetChanged();
                                                                        EventBus.getDefault().post(new EventBusTags.MediaUpdateEvent());
                                                                    })
                                                                    .setOnCancelListener(() -> {
                                                                    })
                                                                    .show();
                                                        } else {
                                                            String songCount = HaigeUtil.makeLabel(mContext,
                                                                    R.plurals.Nsongs, arrayList.get(position).songCount);
                                                            new SystemDialog().create(((AppCompatActivity) mContext).getSupportFragmentManager())
                                                                    .setContent(mContext.getString(R.string.app_delete) + " " + songCount + " ?")
                                                                    .setOk(ResourceUtils.resourceString(R.string.app_delete))
                                                                    .setCancle(ResourceUtils.resourceString(R.string.app_cancel))
                                                                    .setOnOkListener(() -> {
                                                                        HaigeUtil.deleteTracks(mContext, ids);
                                                                        arrayList.remove(position);
                                                                        notifyDataSetChanged();
                                                                        EventBus.getDefault().post(new EventBusTags.MediaUpdateEvent());
                                                                    })
                                                                    .setOnCancelListener(() -> {
                                                                    })
                                                                    .show();
                                                        }
                                                    }
                                                });
                                        break;
                                }
                                break;
                        }
                        return false;
                    }
                });
                menu.inflate(R.menu.popup_album);
                menu.show();
            }
        });
    }

    private Observable<long[]> getSongListIdByAlbum(long albumId) {
        return AlbumSongLoader.getSongsForAlbum(mContext, albumId)
                .map(new Function<List<Song>, long[]>() {

                    @Override
                    public long[] apply(List<Song> songs) throws Exception {
                        long[] ids = new long[songs.size()];
                        int i = 0;
                        for (Song song : songs) {
                            ids[i] = song.songId;
                            i++;
                        }
                        return ids;
                    }
                });
    }

    public class ItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView title;
        private TextView artist;
        private TextView songcount;
        private ImageView albumArt;
        private ImageView popupMenu;
        private View footer;

        public ItemHolder(View view) {
            super(view);
            this.title = (TextView) view.findViewById(R.id.text_item_title);
            this.artist = (TextView) view.findViewById(R.id.text_item_subtitle);
            this.songcount = (TextView) view.findViewById(R.id.text_item_subtitle_2);
            this.albumArt = (ImageView) view.findViewById(R.id.app_item_image);
            this.popupMenu = (ImageView) view.findViewById(R.id.popup_menu);
            this.footer = view.findViewById(R.id.footer);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            NavigationUtil.navigateToAlbum(mContext, arrayList.get(getAdapterPosition()).id,
                    arrayList.get(getAdapterPosition()).title,
                    new Pair<View, String>(albumArt, "transition_album_art" + getAdapterPosition()));
        }

    }


}



