package com.music.player.haige.app;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hc.base.utils.DateUtils;
import com.hc.core.http.GlobalHttpHandler;
import com.hc.core.utils.DeviceUtils;
import com.music.player.haige.BuildConfig;
import com.music.player.haige.app.base.pref.UserPrefHelper;
import com.music.player.haige.mvp.model.api.Api;
import com.music.player.haige.mvp.model.utils.CommonUtils;
import com.music.player.haige.mvp.ui.utils.NetworkUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import okhttp3.Headers;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;
import timber.log.Timber;

/**
 * ================================================
 * 展示 {@link GlobalHttpHandler} 的用法
 * <p>
 * ================================================
 */
public class GlobalHttpHandlerImpl implements GlobalHttpHandler {

    private static final String PATH = "path";
    private static final String TYPE = "type";
    private static final String EQUALS = "=";
    private static final String QUOTE = ";";
    private static final String EMPTY = "";

    public static final String EXTRA_USER_ID = "user_id";
    public static final String EXTRA_TOKEN= "token";
    public static final String EXTRA_DEVID= "_devid";
    public static final String EXTRA_LOCALE= "_locale";
    public static final String EXTRA_IMEI = "_imei";
    public static final String EXTRA_VERSION = "_andver";
    public static final String EXTRA_VER_NAME = "_ver";
    public static final String EXTRA_TS = "_ts";
    public static final String EXTRA_MODEL = "_model";
    public static final String EXTRA_NETTYPE = "_nettype";

    private boolean isEncrypt = true;
    private boolean isDecrypt = true;

    public static String KEY = "";

    static {
        KEY = "EduigvnRM!@5269yg";
    }

    private Context mContext;
    private Gson mGson;
    private ThreadLocal<HashMap<String, String>> mMapThreadLocal = new ThreadLocal<HashMap<String, String>>() {
        @Override
        protected HashMap<String, String> initialValue() {
            return new HashMap<>();
        }
    };
    private Map<String, String> headerParamsMap = new HashMap<>(); // 添加头部参数
    private List<String> headerLinesList = new ArrayList<>(); // 添加头部参数
    private ArrayList<String> excludeDecryptUrl = new ArrayList<>();

    public GlobalHttpHandlerImpl(Context context) {
        this.mContext = context;
        this.mGson = new GsonBuilder().create();
    }

    @Override
    public Response onHttpResultResponse(String httpResult, Interceptor.Chain chain, Response response) {
                /* 这里可以先客户端一步拿到每一次http请求的结果,可以解析成json,做一些操作,如检测到token过期后
                   重新请求token,并重新执行请求 */


                 /* 这里如果发现token过期,可以先请求最新的token,然后在拿新的token放入request里去重新请求
                    注意在这个回调之前已经调用过proceed,所以这里必须自己去建立网络请求,如使用okhttp使用新的request去请求
                    create a new request and modify it accordingly using the new token
                    Request newRequest = chain.request().newBuilder().header("token", newToken)
                                         .build();

                    retry the request

                    response.body().close();
                    如果使用okhttp将新的请求,请求成功后,将返回的response  return出去即可
                    如果不需要返回新的结果,则直接把response参数返回出去 */
        // 解析头部
        try {
            String url = chain.request().url().url().toString();
            Timber.e("onHttpResultResponse url:" + url);
            if (isImageRequest(url)) {
                return response;
            }
            headerParse(response);
            Response.Builder builder = response.newBuilder();
            String body = response.body().string();
            Timber.e("响应明文结果##########：" + body);
            builder.body(ResponseBody.create(MediaType.parse("application/x-www-form-urlencoded;charset=UTF-8"), body));
            response = builder.build();
        } catch (Exception e) {
            Timber.e(e);
        }
        return response;
    }

    // 这里可以在请求服务器之前可以拿到request,做一些操作比如给request统一添加token或者header以及参数加密等操作
    @Override
    public Request onHttpRequestBefore(Interceptor.Chain chain, Request request) {
        String url = request.url().url().toString();
        Timber.e("onHttpRequestBefore url:" + url);
        if (isImageRequest(url)) {
            return request;
        }
        if (isAvatarUpload(url)) {
            Request.Builder requestBuilder = request.newBuilder();
            HashMap<String, String> urlParamsMap = getQueryParams();
            HttpUrl.Builder httpUrlBuilder = request.url().newBuilder();
            if (urlParamsMap.size() > 0) {
                Iterator iterator = urlParamsMap.entrySet().iterator();
                while (iterator.hasNext()) {
                    Map.Entry entry = (Map.Entry) iterator.next();
                    httpUrlBuilder.addQueryParameter((String) entry.getKey(), (String) entry.getValue());
                }
            }
            requestBuilder.url(httpUrlBuilder.build());
            requestBuilder.post(request.body());
            request = requestBuilder.build();
            return request;
        }

        HashMap queryParamsMap = mMapThreadLocal.get();
        HashMap<String, String> params = getParams(request);

        Request.Builder requestBuilder = request.newBuilder();
        Headers.Builder headerBuilder = request.headers().newBuilder();
        addHeader(headerBuilder);
        requestBuilder.headers(headerBuilder.build());
        if (TextUtils.equals(request.method(), "POST")) { // POST
            HashMap<String, String> urlParamsMap = getQueryParams();
            HttpUrl.Builder httpUrlBuilder = request.url().newBuilder();
            if (urlParamsMap.size() > 0) {
                Iterator iterator = urlParamsMap.entrySet().iterator();
                while (iterator.hasNext()) {
                    Map.Entry entry = (Map.Entry) iterator.next();
                    httpUrlBuilder.addQueryParameter((String) entry.getKey(), (String) entry.getValue());
                }
            }

            requestBuilder.url(httpUrlBuilder.build());
            requestBuilder.post(request.body());
        } else if (TextUtils.equals(request.method(), "PUT")) {
            HashMap<String, String> urlParamsMap = getQueryParams();
            HttpUrl.Builder httpUrlBuilder = request.url().newBuilder();
            if (urlParamsMap.size() > 0) {
                Iterator iterator = urlParamsMap.entrySet().iterator();
                while (iterator.hasNext()) {
                    Map.Entry entry = (Map.Entry) iterator.next();
                    httpUrlBuilder.addQueryParameter((String) entry.getKey(), (String) entry.getValue());
                }
            }

            requestBuilder.url(httpUrlBuilder.build());
            requestBuilder.put(request.body());
        } else if (isGET(request)) { // GET
                addQueryParams(params);
                injectParamsIntoUrl(request, requestBuilder, queryParamsMap);
            }
        request = requestBuilder.build();
        return request;
    }

    private boolean isImageRequest(String url) {
        return url.endsWith(".jpg") || url.endsWith(".png");
    }

    /**
     * 加密开关
     */
    public GlobalHttpHandlerImpl isEncrypt(boolean isEncrypt) {
        this.isEncrypt = isEncrypt;
        return this;
    }

    /**
     * 解密开关
     */
    public GlobalHttpHandlerImpl isDecrypt(boolean isDecrypt) {
        this.isDecrypt = isDecrypt;
        return this;
    }

    public GlobalHttpHandlerImpl setExcludeDecryptUrl(String fileName) {
        if (!TextUtils.isEmpty(fileName)) {
            try {
                InputStreamReader inputReader = new InputStreamReader(mContext.getResources().getAssets().open(fileName));
                BufferedReader bufReader = new BufferedReader(inputReader);
                String line;
                while ((line = bufReader.readLine()) != null) {
                    if (!TextUtils.isEmpty(line) && (line.startsWith("http:") || line.startsWith("https:"))) {
                        excludeDecryptUrl.add(line);
                    }
                }
            } catch (Exception e) {
            }
        }
        return this;
    }

    private void headerParse(Response response) {
        if (response == null) {
            return;
        }
        String serverDate = response.header("Date");
        if (TextUtils.isEmpty(serverDate)) {
            return;
        }
        long mills = DateUtils.parseGMT(serverDate);
        long cur = System.currentTimeMillis();
        int delta = 5 * 60 * 1000; // 5min
        boolean notify = Math.abs(mills - cur) > delta;
        // 当服务器时间与本地时间误差超过5分钟的时候,提示用户去系统同步网络时间,这里可以更优化点。
        if (notify) {
            //notifyUI();
        }
    }

    /**
     * 目前只适配的get post 请求,后续根据需求添加 put delete head 等等
     */
    private HashMap<String, String> getParams(Request request) {
        if (TextUtils.equals(request.method(), "POST")) {
            return getBodyParams(bodyToString(request.body()));
        }
        return getOriginParams(request);
    }

    private HashMap<String, String> getBodyParams(String bodyStr) {
        HashMap<String, String> params = new HashMap<>();
        if (TextUtils.isEmpty(bodyStr)) {
            return params;
        }
        String[] values = bodyStr.split("&");
        for (String value : values) {
            String[] both = value.split("=");
            if (both.length == 2) {
                try {
                    params.put(both[0], URLDecoder.decode(both[1], "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        }
        return params;
    }

    private String bodyToString(final RequestBody request) {
        try {
            final RequestBody copy = request;
            final Buffer buffer = new Buffer();
            if (copy != null)
                copy.writeTo(buffer);
            else
                return "";
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "did not work";
        }
    }

    private HashMap<String, String> getOriginParams(Request request) {
        HashMap<String, String> params = new HashMap<>();
        HttpUrl httpUrl = request.url();
        int size = httpUrl.querySize();
        for (int i = 0; i < size; i++) {
            String name = httpUrl.queryParameterName(i);
            String value = httpUrl.queryParameterValue(i);
            params.put(name, value);
        }
        return params;
    }

    private boolean isAvatarUpload(String url) {
        return url.contains(Api.URL.UPLOAD_AVATAR);
    }

    @NonNull
    private Request.Builder uploadFile(Request request, HashMap<String, String> params) {
        String path = params.get(PATH);
        File file = new File(path);
        String type = params.get(TYPE);
        HashMap<String, String> signParams = new HashMap<>();
        signParams.put(TYPE, type);
        addQueryParams(signParams);
        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("filebytes", file.getName(), requestFile);
        String descriptionString = "uploadAvatar";
        RequestBody description = RequestBody.create(MediaType.parse("multipart/form-data"), descriptionString);
        MultipartBody.Builder mb = new MultipartBody.Builder();
        HashMap<String, String> queryParamsMap = mMapThreadLocal.get();
        Set<String> keys = queryParamsMap.keySet();
        for (String key : keys) {
            mb.addFormDataPart(key, queryParamsMap.get(key));
        }
        mb.addPart(body).addPart(description);
        Request.Builder builder = request.newBuilder();
        builder.post(mb.build());
        addHeader(request.headers().newBuilder());
        return builder;
    }

    /**
     * @param params
     */
    private void addQueryParams(HashMap<String, String> params) {
        HashMap<String, String> queryParamsMap = mMapThreadLocal.get();
        CommonUtils.clear(queryParamsMap);
//        String imei = DeviceUtils.getIMEI();
        queryParamsMap.put(EXTRA_USER_ID, UserPrefHelper.getUserId());
        queryParamsMap.put(EXTRA_TOKEN, UserPrefHelper.getUserToken());
        queryParamsMap.put(EXTRA_DEVID, "");
        queryParamsMap.put(EXTRA_LOCALE, "");
        queryParamsMap.put(EXTRA_IMEI, "");
        queryParamsMap.put(EXTRA_VERSION, String.valueOf(BuildConfig.VERSION_CODE));
        queryParamsMap.put(EXTRA_VER_NAME, String.valueOf(BuildConfig.VERSION_NAME));
        queryParamsMap.put(EXTRA_TS, String.valueOf(System.currentTimeMillis()));
        queryParamsMap.put(EXTRA_MODEL, DeviceUtils.getPhoneType());
        queryParamsMap.put(EXTRA_NETTYPE, NetworkUtils.getNetWorkType());
        queryParamsMap.putAll(params);
    }

    private HashMap<String, String> getQueryParams() {
        HashMap<String, String> params = new HashMap<>();
//        String imei = DeviceUtils.getIMEI();
        params.put(EXTRA_USER_ID, UserPrefHelper.getUserId());
        params.put(EXTRA_TOKEN, UserPrefHelper.getUserToken());
        params.put(EXTRA_DEVID, "");
        params.put(EXTRA_LOCALE, "");
        params.put(EXTRA_IMEI, "");
        params.put(EXTRA_VERSION, String.valueOf(BuildConfig.VERSION_CODE));
        params.put(EXTRA_VER_NAME, String.valueOf(BuildConfig.VERSION_NAME));
        params.put(EXTRA_TS, String.valueOf(System.currentTimeMillis()));
        params.put(EXTRA_MODEL, DeviceUtils.getPhoneType() + " " + DeviceUtils.getSystemVersion());
        params.put(EXTRA_NETTYPE, NetworkUtils.getNetWorkType());
        return params;
    }

    private void addHeader(Headers.Builder headerBuilder) {
        if (headerParamsMap.size() > 0) {
            Iterator iterator = headerParamsMap.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry entry = (Map.Entry) iterator.next();
                headerBuilder.add((String) entry.getKey(), (String) entry.getValue());
            }
        }

        if (headerLinesList.size() > 0) {
            for (String line : headerLinesList) {
                headerBuilder.add(line);
            }
        }
        headerParamsMap.clear();
        headerLinesList.clear();
    }

    private boolean isGET(Request request) {
        if (request == null) {
            return false;
        }
        if (!TextUtils.equals(request.method(), "GET")) {
            return false;
        }
        return true;
    }

    // func to inject params into url
    private void injectParamsIntoUrl(Request request, Request.Builder requestBuilder, Map<String, String> paramsMap) {
        HttpUrl.Builder httpUrlBuilder = request.url().newBuilder();
        // 添加转换之后的参数
        if (paramsMap.size() > 0) {
            Iterator iterator = paramsMap.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry entry = (Map.Entry) iterator.next();
                httpUrlBuilder.addQueryParameter((String) entry.getKey(), (String) entry.getValue());
            }
        }
        requestBuilder.url(httpUrlBuilder.build());
    }

}
