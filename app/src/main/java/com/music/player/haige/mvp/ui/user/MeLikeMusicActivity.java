package com.music.player.haige.mvp.ui.user;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.hc.core.di.component.AppComponent;
import com.music.player.haige.R;
import com.music.player.haige.app.EventBusTags;
import com.music.player.haige.app.SystemUIConfig;
import com.music.player.haige.app.base.BaseListActivity;
import com.music.player.haige.app.base.pref.UserPrefHelper;
import com.music.player.haige.app.base.recyclerview.BaseQuickAdapter;
import com.music.player.haige.app.base.recyclerview.listener.OnItemClickListener;
import com.music.player.haige.app.base.recyclerview.pullrefresh.HaigeRefreshHeader;
import com.music.player.haige.di.component.DaggerLikeMusicComponent;
import com.music.player.haige.di.module.LikeMusicModule;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.model.utils.NavigationUtil;
import com.music.player.haige.mvp.presenter.LikeMusicPresenter;
import com.music.player.haige.mvp.ui.login.LoginDialogFragment;
import com.music.player.haige.mvp.ui.main.activity.TagMusicActivity;
import com.music.player.haige.mvp.ui.main.fragment.PlayMainFragment;
import com.music.player.haige.mvp.ui.music.adapter.MusicListAdapter;
import com.music.player.haige.mvp.ui.music.service.MusicServiceConnection;
import com.music.player.haige.mvp.ui.share.DownloadShareDialog;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;
import com.music.player.haige.mvp.ui.utils.ShareUtils;
import com.music.player.haige.mvp.ui.utils.Utils;

import org.simple.eventbus.Subscriber;

import butterknife.BindView;
import butterknife.OnClick;

import static com.music.player.haige.mvp.ui.main.fragment.PlayMainFragment.PLAY_MAIN_CURRENT_PLAY;

public class MeLikeMusicActivity extends BaseListActivity<LikeMusicPresenter> implements MVContract.LikeMusicView,
        SystemUIConfig {

    @BindView(R.id.app_fragment_title_tv)
    TextView mTitleTv;

    @Override
    public boolean translucentStatusBar() {
        return true;
    }

    @Override
    public int setStatusBarColor() {
        return getResources().getColor(R.color.color_00000000);
    }

    @Override
    public boolean lightStatusBar() {
        return false;
    }

    @Override
    public boolean fitSystemWindows() {
        return true;
    }

    @Override
    public boolean fullScreen() {
        return false;
    }

    @Override
    protected RecyclerView.LayoutManager createLayoutManager() {
        return new LinearLayoutManager(getActivity());
    }

    @Override
    protected OnItemClickListener createItemClickListener() {
        return new OnItemClickListener(false) {
            @Override
            public void SimpleOnItemClick(BaseQuickAdapter adapter, View view, int position) {
                if (Utils.isFastClick()) {
                    return;
                }

                MusicServiceConnection.playAll(adapter.getData(), position, false);
                startPlayMain();
            }

            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                if (Utils.isFastClick()) {
                    return;
                }

                switch (view.getId()) {
                    case R.id.popup_menu:
                        if (UserPrefHelper.isLogin()) {
                            onPopupMenuClick(view, (Song) adapter.getItem(position));
                        } else {
                            LoginDialogFragment.newInstance().show(getSupportFragmentManager(), LoginDialogFragment.class.getName());
                        }
                        break;
                    default:
                        break;
                }
            }
        };
    }

    @Override
    protected RecyclerView.OnScrollListener addRecyclerViewScrollListener() {
        return null;
    }

    @Override
    protected RecyclerView.ItemDecoration createDecoration() {
        return null;
    }

    @Override
    protected RecyclerView.ItemAnimator createItemAnimator() {
        return null;
    }

    @Override
    protected View createRefreshHeader() {
        return new HaigeRefreshHeader(getActivity());
    }

    @Override
    protected Context createContext() {
        return getActivity();
    }

    @Override
    protected BaseQuickAdapter createAdapter() {
        return new MusicListAdapter(getActivity(), false);
    }

    @Override
    public void setupActivityComponent(AppComponent appComponent) {
        DaggerLikeMusicComponent
                .builder()
                .appComponent(appComponent)
                .likeMusicModule(new LikeMusicModule(this))
                .build()
                .inject(this);
    }

    @Override
    public int initView(Bundle savedInstanceState) {
        return R.layout.activity_like_music;
    }

    @Override
    protected void setupView() {
        super.setupView();

        mTitleTv.setText(ResourceUtils.resourceString(R.string.app_mine_favorite));
    }

    @Override
    protected void sendListReq(int pageNum, int pageSize, boolean refresh) {
        mPresenter.getLikedList(UserPrefHelper.getUserId(), pageNum, pageSize, refresh);
    }

    @Override
    protected void reLoadData() {
        fetchPusherList(mAdapter, true);
    }

    @Override
    protected void loadMoreData() {
        fetchPusherList(mAdapter, false);
    }

    @Subscriber
    public void onSubscriber(EventBusTags.MetaChangedEvent event) {
        if (getAdapter() != null) {
            getAdapter().notifyDataSetChanged();
        }
    }

    @Subscriber
    public void onSubscriber(EventBusTags.MAIN tags) {
        switch (tags) {
            case BACK:
                getSupportFragmentManager().popBackStackImmediate();
                break;
        }
    }

    @OnClick(R.id.app_fragment_back_iv)
    public void onClose() {
        onBackPressed();
    }

    public void onPopupMenuClick(View v, Song song) {
        final PopupMenu menu = new PopupMenu(getActivity(), v);
        menu.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.popup_song_play_next:
                    MusicServiceConnection.playNext(song);
                    showMessage(ResourceUtils.resourceString(R.string.app_next_play));
                    break;
                case R.id.popup_song_goto_album:
                    NavigationUtil.goToAlbum(getActivity(), song.albumId, song.musicName);
                    break;
                case R.id.popup_song_goto_artist:
                    NavigationUtil.goToArtist(getActivity(), song.artistId, song.artistName);
                    break;
                case R.id.popup_song_addto_queue:
                    MusicServiceConnection.addToQueue(song);
                    break;
                case R.id.popup_download:
                    if (ShareUtils.checkDownloadShare(UserPrefHelper.getDownloadMusicCount())) {
                        DownloadShareDialog.newInstance().show(getSupportFragmentManager(), DownloadShareDialog.class.getName());
                    } else {
                        mPresenter.downloadMusic(song);
                    }
                    break;
            }
            return false;
        });
        menu.inflate(R.menu.popup_other_music_list);
        menu.show();
    }

    private void startPlayMain() {
        Fragment fragment = PlayMainFragment.newInstance(PLAY_MAIN_CURRENT_PLAY);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_from_bottom, R.anim.slide_out_to_left, R.anim.slide_in_from_left, R.anim.slide_out_to_bottom);
        transaction.replace(R.id.app_activity_main_foreground_layout, fragment, TagMusicActivity.class.getName());
        transaction.commitAllowingStateLoss();
        transaction.addToBackStack(null);
    }
}
