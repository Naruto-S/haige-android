package com.music.player.haige.mvp.model.entity.comment;

import com.music.player.haige.mvp.model.entity.BaseResponse;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Naruto on 2018/5/13.
 */

public class CommentResponse extends BaseResponse<ArrayList<CommentBean>> implements Serializable {

    private int position;

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "CommentResponse{" +
                "position=" + position +
                '}';
    }
}
