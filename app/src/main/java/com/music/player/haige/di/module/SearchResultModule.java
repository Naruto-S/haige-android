package com.music.player.haige.di.module;

import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.SearchResultModel;

import dagger.Module;
import dagger.Provides;

/**
 * ================================================
 * Created by huangcong on 2018/3/28.
 * ================================================
 */
@Module
public class SearchResultModule  {
    private MVContract.SearchResultView view;

    public SearchResultModule( MVContract.SearchResultView view){
        this.view = view;
    }

    @FragmentScope
    @Provides
    MVContract.SearchResultView provideView(){
        return view;
    }

    @FragmentScope
    @Provides
    MVContract.SearchResultModel provideModel(SearchResultModel model){
        return model;
    }
}
