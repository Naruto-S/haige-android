package com.music.player.haige.mvp.ui.comment;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.hc.core.utils.DeviceUtils;
import com.music.player.haige.R;
import com.music.player.haige.mvp.model.entity.comment.CommentBean;
import com.music.player.haige.mvp.ui.comment.adapter.CommentAdapter;
import com.music.player.haige.mvp.ui.utils.KeyBoardUtils;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;
import com.music.player.haige.mvp.ui.utils.Utils;
import com.music.player.haige.mvp.ui.widget.emoji.EditCommentView;

import java.util.ArrayList;

public class CommentPopWindow extends PopupWindow {

    private Activity mActivity;
    private RecyclerView mRecyclerView;
    private TextView mCommentCountTv;
    private TextView mNoMoreTv;
    private TextView mEditTv;
    private ImageView mCloseIv;
    private EditCommentView mEditCommentView;
    private ArrayList<CommentBean> mCommentList = new ArrayList<>();
    private CommentAdapter mAdapter;

    public CommentPopWindow(Context context) {
        super(context);
        mActivity = (Activity) context;
        LayoutInflater inflater = LayoutInflater.from(context);
        View rootView = inflater.inflate(R.layout.layout_barrage_comment_list, null);
        mRecyclerView = rootView.findViewById(R.id.recycler_comment);
        mCommentCountTv = rootView.findViewById(R.id.tv_comment_count);
        mEditCommentView = rootView.findViewById(R.id.edit_comment_view);
        mNoMoreTv = rootView.findViewById(R.id.tv_no_more);
        mEditTv = rootView.findViewById(R.id.tv_comment_edit);
        mEditTv.setOnClickListener(v -> {
            mEditCommentView.setInputVisibility(View.VISIBLE);
            KeyBoardUtils.openKeybord(mEditCommentView.getEditText(), mActivity);
            mEditCommentView.getEditText().requestFocus();
        });
        mCloseIv = rootView.findViewById(R.id.iv_close);
        mCloseIv.setOnClickListener(v -> dismiss());

        initRecyclerView();

        this.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        this.setHeight((int) (DeviceUtils.getScreenHeight(mActivity) * 0.77));
        this.setTouchable(true);
        this.setFocusable(true);
        this.setOutsideTouchable(true);
        this.setBackgroundDrawable(ResourceUtils.getDrawable(R.drawable.bg_me_profile_more_popwindow));
        this.setAnimationStyle(R.style.DialogAnimation);
        this.setContentView(rootView);
    }

    private void initRecyclerView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(null);
        mAdapter = new CommentAdapter();
        mRecyclerView.setAdapter(mAdapter);

        refreshData();
    }

    private void refreshData() {
        if (Utils.isNotNull(mCommentCountTv)) {
            mCommentCountTv.setText(ResourceUtils.resourceString(R.string.barrage_comment_count,
                    mCommentList.size()));
        }

        if (Utils.isNotNull(mAdapter)) {
            mAdapter.setCommentList(mCommentList);
        }

        if (Utils.isNotEmptyCollection(mCommentList)) {
            if (Utils.isNotNull(mRecyclerView) && Utils.isNotNull(mNoMoreTv)) {
                mRecyclerView.setVisibility(View.VISIBLE);
                mNoMoreTv.setVisibility(View.GONE);
            }
        } else {
            if (Utils.isNotNull(mRecyclerView) && Utils.isNotNull(mNoMoreTv)) {
                mRecyclerView.setVisibility(View.GONE);
                mNoMoreTv.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void dismiss() {
        KeyBoardUtils.closeKeybord(mEditCommentView.getEditText(), mActivity);
        super.dismiss();
    }

    public EditCommentView getEditCommentView() {
        return mEditCommentView;
    }

    public void setCommentList(ArrayList<CommentBean> commentList) {
        mCommentList.clear();
        mCommentList.addAll(commentList);

        refreshData();
    }
}
