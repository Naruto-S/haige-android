package com.music.player.haige.mvp.model.entity.music;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class UploadMusicRequest implements Serializable {

    @SerializedName("music_path")
    private String musicPath;

    @SerializedName("cover_path")
    private String cover;

    @SerializedName("tags")
    private ArrayList<String> tags;

    @SerializedName("artist_name")
    private String artistName;

    @SerializedName("music_name")
    private String musicName;

    @SerializedName("bg_type")
    private int bgType;

    public String getMusicPath() {
        return musicPath;
    }

    public void setMusicPath(String musicPath) {
        this.musicPath = musicPath;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public ArrayList<String> getTags() {
        return tags;
    }

    public void setTags(ArrayList<String> tags) {
        this.tags = tags;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public String getMusicName() {
        return musicName;
    }

    public void setMusicName(String musicName) {
        this.musicName = musicName;
    }

    public int getBgType() {
        return bgType;
    }

    public void setBgType(int bgType) {
        this.bgType = bgType;
    }

    @Override
    public String toString() {
        return "UploadMusicRequest{" +
                "musicPath='" + musicPath + '\'' +
                ", cover='" + cover + '\'' +
                ", tags=" + tags +
                ", artistName='" + artistName + '\'' +
                ", musicName='" + musicName + '\'' +
                ", bgType=" + bgType +
                '}';
    }
}
