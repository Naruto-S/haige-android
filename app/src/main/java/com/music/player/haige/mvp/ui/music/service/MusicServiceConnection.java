/*
 * Copyright (C) 2012 Andrew Neal
 * Copyright (C) 2014 The CyanogenMod Project
 * Copyright (C) 2015 Naman Dwivedi
 *
 * Licensed under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law
 * or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */

package com.music.player.haige.mvp.ui.music.service;

import android.app.Activity;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.net.Uri;
import android.os.IBinder;
import android.os.RemoteException;
import android.provider.MediaStore;
import android.util.Log;

import com.music.player.haige.MediaAidlInterface;
import com.music.player.haige.mvp.model.entity.music.MusicPlaybackTrack;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.tencent.bugly.crashreport.CrashReport;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class MusicServiceConnection {

    private static final WeakHashMap<Context, ServiceBinder> mConnectionMap;
    private static final long[] sEmptyList;
    public static MediaAidlInterface mService = null;
    private static ContentValues[] mContentValuesCache = null;

    static {
        mConnectionMap = new WeakHashMap<>();
        sEmptyList = new long[0];
    }

    public static ServiceToken bindToService(final Context context,
                                             final ServiceConnection callback) {
        Activity realActivity = ((Activity) context).getParent();
        if (realActivity == null) {
            realActivity = (Activity) context;
        }
        final ContextWrapper contextWrapper = new ContextWrapper(realActivity);
        contextWrapper.startService(new Intent(contextWrapper, MusicService.class));
        final ServiceBinder binder = new ServiceBinder(callback,
                contextWrapper.getApplicationContext());
        if (contextWrapper.bindService(
                new Intent().setClass(contextWrapper, MusicService.class), binder, 0)) {
            mConnectionMap.put(contextWrapper, binder);
            return new ServiceToken(contextWrapper);
        }
        return null;
    }

    public static void unbindFromService(final ServiceToken token) {
        if (token == null) {
            return;
        }
        final ContextWrapper mContextWrapper = token.mWrappedContext;
        final ServiceBinder mBinder = mConnectionMap.remove(mContextWrapper);
        if (mBinder == null) {
            return;
        }
        mContextWrapper.unbindService(mBinder);
        if (mConnectionMap.isEmpty()) {
            mService = null;
        }
    }

    public static final boolean isPlaybackServiceConnected() {
        return mService != null;
    }

    public static void next() {
        try {
            if (mService != null) {
                mService.next();
            }
        } catch (final RemoteException ignored) {
        }
    }

    public static void initPlaybackServiceWithSettings(final Context context) {
        setShowAlbumArtOnLockscreen(true);
    }

    public static void setShowAlbumArtOnLockscreen(final boolean enabled) {
        Observable.create((ObservableOnSubscribe<Boolean>)
                emitter -> {
                    try {
                        if (mService != null) {
                            mService.setLockscreenAlbumArt(enabled);
                        }
                    } catch (final RemoteException ignored) {
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aBoolean -> {
                });
    }

    public static void asyncNext(final Context context) {
        final Intent previous = new Intent(context, MusicService.class);
        previous.setAction(MusicService.NEXT_ACTION);
        context.startService(previous);
    }

    public static void previous(final Context context, final boolean force) {
        final Intent previous = new Intent(context, MusicService.class);
        if (force) {
            previous.setAction(MusicService.PREVIOUS_FORCE_ACTION);
        } else {
            previous.setAction(MusicService.PREVIOUS_ACTION);
        }
        context.startService(previous);
    }

    public static void playOrPause() {
        Observable.create((ObservableOnSubscribe<Boolean>)
                emitter -> {
                    try {
                        if (mService != null) {
                            if (mService.isPlaying()) {
                                mService.pause();
                            } else {
                                mService.play();
                            }
                        }
                    } catch (final Exception ignored) {
                    }

                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aBoolean -> {
                });
    }


    public static boolean isTrackLocal() {
        try {
            if (mService != null) {
                return mService.isTrackLocal();
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void cycleRepeat() {
        try {
            if (mService != null) {
                if (mService.getShuffleMode() == MusicDataManager.SHUFFLE_NORMAL) {
                    mService.setShuffleMode(MusicDataManager.SHUFFLE_NONE);
                    mService.setRepeatMode(MusicDataManager.REPEAT_CURRENT);
                    return;
                } else {

                    switch (mService.getRepeatMode()) {
                        case MusicDataManager.REPEAT_CURRENT:
                            mService.setRepeatMode(MusicDataManager.REPEAT_ALL);
                            break;
                        case MusicDataManager.REPEAT_ALL:
                            mService.setShuffleMode(MusicDataManager.SHUFFLE_NORMAL);
//                            if (mService.getShuffleMode() != MusicService.SHUFFLE_NONE) {
//                                mService.setShuffleMode(MusicService.SHUFFLE_NONE);
//                            }
                            break;

                    }
                }

            }
        } catch (final RemoteException ignored) {
        }
    }

    public static void cycleShuffle() {
        try {
            if (mService != null) {
                switch (mService.getShuffleMode()) {
                    case MusicDataManager.SHUFFLE_NONE:
                        mService.setShuffleMode(MusicDataManager.SHUFFLE_NORMAL);
                        if (mService.getRepeatMode() == MusicDataManager.REPEAT_CURRENT) {
                            mService.setRepeatMode(MusicDataManager.REPEAT_ALL);
                        }
                        break;
                    case MusicDataManager.SHUFFLE_NORMAL:
                        mService.setShuffleMode(MusicDataManager.SHUFFLE_NONE);
                        break;
//                    case MusicService.SHUFFLE_AUTO:
//                        mService.setShuffleMode(MusicService.SHUFFLE_NONE);
//                        break;
                    default:
                        break;
                }
            }
        } catch (final RemoteException ignored) {
        }
    }

    public static final boolean isPlaying() {
        if (mService != null) {
            try {
                return mService.isPlaying();
            } catch (final RemoteException ignored) {
            }
        }
        return false;
    }

    public static final int getShuffleMode() {
        if (mService != null) {
            try {
                return mService.getShuffleMode();
            } catch (final RemoteException ignored) {
            }
        }
        return 0;
    }

    public static void setShuffleMode(int mode) {
        Observable.create((ObservableOnSubscribe<Boolean>)
                emitter -> {
                    try {
                        if (mService != null) {
                            mService.setShuffleMode(mode);
                        }
                    } catch (RemoteException ignored) {

                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aBoolean -> {
                });
    }

    public static void setRepeatMode(int mode) {
        Observable.create((ObservableOnSubscribe<Boolean>)
                emitter -> {
                    try {
                        if (mService != null) {
                            mService.setRepeatMode(mode);
                        }
                    } catch (RemoteException ignored) {

                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aBoolean -> {
                });
    }

    public static final int getRepeatMode() {
        if (mService != null) {
            try {
                return mService.getRepeatMode();
            } catch (final RemoteException ignored) {
            }
        }
        return 0;
    }

    public static final String getTrackName() {
        if (mService != null) {
            try {
                return mService.getTrackName();
            } catch (final RemoteException ignored) {
            }
        }
        return null;
    }

    public static final String getArtistName() {
        if (mService != null) {
            try {
                return mService.getArtistName();
            } catch (final RemoteException ignored) {
            }
        }
        return null;
    }

    public static final String getAlbumName() {
        if (mService != null) {
            try {
                return mService.getAlbumName();
            } catch (final RemoteException ignored) {
            }
        }
        return null;
    }

    public static final String getAlbumPath() {
        if (mService != null) {
            try {
                return mService.getAlbumPath();
            } catch (final RemoteException ignored) {
            }
        }
        return null;
    }

    public static final String[] getAlbumPathAll() {
        if (mService != null) {
            try {
                return mService.getAlbumPathtAll();
            } catch (final RemoteException ignored) {
            }
        }
        return null;
    }

    public static final long getCurrentAlbumId() {
        if (mService != null) {
            try {
                return mService.getAlbumId();
            } catch (final RemoteException ignored) {
            }
        }
        return -1;
    }

    public static final long getCurrentAudioId() {
        if (mService != null) {
            try {
                return mService.getAudioId();
            } catch (final RemoteException ignored) {
            }
        }
        return -1;
    }

    public static final MusicPlaybackTrack getCurrentTrack() {
        if (mService != null) {
            try {
                return mService.getCurrentTrack();
            } catch (final RemoteException ignored) {
            }
        }
        return null;
    }

    public static final MusicPlaybackTrack getTrack(int index) {
        if (mService != null) {
            try {
                return mService.getTrack(index);
            } catch (final RemoteException ignored) {
            }
        }
        return null;
    }

    public static final long getNextAudioId() {
        if (mService != null) {
            try {
                return mService.getNextAudioId();
            } catch (final RemoteException ignored) {
            }
        }
        return -1;
    }

    public static final long getPreviousAudioId() {
        if (mService != null) {
            try {
                return mService.getPreviousAudioId();
            } catch (final RemoteException ignored) {
            }
        }
        return -1;
    }

    public static final long getCurrentArtistId() {
        if (mService != null) {
            try {
                return mService.getArtistId();
            } catch (final RemoteException ignored) {
            }
        }
        return -1;
    }

    public static final int getAudioSessionId() {
        if (mService != null) {
            try {
                return mService.getAudioSessionId();
            } catch (final RemoteException ignored) {
            }
        }
        return -1;
    }

    public static final long[] getQueue() {
        try {
            if (mService != null) {
                return mService.getQueue();
            } else {
            }
        } catch (final RemoteException ignored) {
        }
        return sEmptyList;
    }

    public static final HashMap<Long, Song> getPlayinfos() {
        try {
            if (mService != null) {
                return (HashMap<Long, Song>) mService.getPlayinfos();
            } else {
            }
        } catch (final RemoteException ignored) {
        }
        return null;
    }

    public static final long getQueueItemAtPosition(int position) {
        try {
            if (mService != null) {
                return mService.getQueueItemAtPosition(position);
            } else {
            }
        } catch (final RemoteException ignored) {
        }
        return -1;
    }

    public static final int getQueueSize() {
        try {
            if (mService != null) {
                return mService.getQueueSize();
            } else {
            }
        } catch (final RemoteException ignored) {
        }
        return 0;
    }

    public static final int getQueuePosition() {
        try {
            if (mService != null) {
                return mService.getQueuePosition();
            }
        } catch (Throwable throwable) {
        }
        return 0;
    }

    public static void setQueuePosition(final int position) {
        if (mService != null) {
            try {
                mService.setQueuePosition(position);
            } catch (Throwable throwable) {
            }
        }
    }

    public static final int getQueueHistorySize() {
        if (mService != null) {
            try {
                return mService.getQueueHistorySize();
            } catch (final RemoteException ignored) {
            }
        }
        return 0;
    }

    public static final int getQueueHistoryPosition(int position) {
        if (mService != null) {
            try {
                return mService.getQueueHistoryPosition(position);
            } catch (final RemoteException ignored) {
            }
        }
        return -1;
    }

    public static final int[] getQueueHistoryList() {
        if (mService != null) {
            try {
                return mService.getQueueHistoryList();
            } catch (final RemoteException ignored) {
            }
        }
        return null;
    }

    public static void refresh() {
        try {
            if (mService != null) {
                mService.refresh();
            }
        } catch (final RemoteException ignored) {
        }
    }

    public static final int removeTrack(final long id) {
        try {
            if (mService != null) {
                return mService.removeTrack(id);
            }
        } catch (final RemoteException ingored) {
        }
        return 0;
    }

    public static final boolean removeTrackAtPosition(final long id, final int position) {
        try {
            if (mService != null) {
                return mService.removeTrackAtPosition(id, position);
            }
        } catch (final RemoteException ingored) {
        }
        return false;
    }

    public static void moveQueueItem(final int from, final int to) {
        try {
            if (mService != null) {
                mService.moveQueueItem(from, to);
            } else {
            }
        } catch (final RemoteException ignored) {
        }
    }

    public static void setMusicItem(final Song song) {
        if (mService != null) {
            try {
                HashMap<Long, Song> playinfos = (HashMap<Long, Song>) mService.getPlayinfos();
                playinfos.put(song.songId, song);
                mService.putPlayinfos(playinfos);
            } catch (final RemoteException ignored) {
            }
        }
    }

    public static void play(Song song, int position, final boolean forceShuffle, boolean isShort) {
        ArrayList<Song> songs = new ArrayList<>();
        songs.add(song);
        playAll(songs, position, forceShuffle, isShort);
    }

    public static void playAll(final List<Song> songs, int position, final boolean forceShuffle) {
        playAll(songs, position, forceShuffle, false);
    }

    public static void playAll(final List<Song> songs, final int position, final boolean forceShuffle, boolean isShort) {
        Observable.create((ObservableOnSubscribe<Boolean>)
                emitter -> {
                    Timber.e("===>>> playAll start");
                    if (songs == null || songs.size() == 0 || mService == null) {
                        return;
                    }
                    try {
                        mService.putIsShort(isShort);
                        if (forceShuffle) {
                            mService.setShuffleMode(MusicDataManager.SHUFFLE_NORMAL);
                        }
                        int songNum = songs.size();
                        long[] ret = new long[songNum];
                        for (int i = 0; i < songNum; i++) {
                            Song song = songs.get(i);
                            if (song != null) {
                                ret[i] = song.songId;
                            }
                        }
                        final long currentId = mService.getAudioId();
                        final int currentQueuePosition = getQueuePosition();
                        if (position != -1) {
                            final long[] playlist = getQueue();
                            if (Arrays.equals(ret, playlist)) {
                                if (currentQueuePosition == position && currentId == ret[position]) {
                                    Timber.e("===>>> playAll play start");
                                    mService.play();
                                    Timber.e("===>>> playAll play end");
                                    Timber.e("===>>> playAll end");
                                    return;
                                } else {
                                    Timber.e("===>>> playAll setQueuePosition start");
                                    mService.setQueuePosition(position);
                                    Timber.e("===>>> playAll setQueuePosition end");
                                    Timber.e("===>>> playAll end");
                                    return;
                                }

                            }
                        }
                        Map<Long, Song> map = new HashMap<>();
                        int i = 0;
                        for (Song info : songs) {
                            map.put(ret[i], info);
                            i++;
                        }
                        Timber.e("===>>> playAll open start");
                        mService.open(map, ret, forceShuffle ? -1 : position);
                        Timber.e("===>>> playAll open end");
                        mService.play();
                    } catch (Throwable t) {
                        CrashReport.postCatchedException(t);
                    }
                    Timber.e("===>>> playAll end");
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aBoolean -> {
                });
    }

    public static void playNext(Song song) {
        if (mService == null) {
            return;
        }
        try {
            long[] ids = new long[1];
            ids[0] = song.songId;
            HashMap<Long, Song> musicInfoHashMap = new HashMap<>();
            musicInfoHashMap.put(ids[0], song);
            mService.enqueue(ids, musicInfoHashMap, MusicService.NEXT);
        } catch (final RemoteException ignored) {
        }
    }

    public static void open(final List<Song> songs, final int position, final boolean forceShuffle, boolean isShort) {
        Observable.create((ObservableOnSubscribe<Boolean>)
                emitter -> {
                    Timber.e("===>>> playAll start");
                    if (songs == null || songs.size() == 0 || mService == null) {
                        return;
                    }
                    try {
                        mService.putIsShort(isShort);
                        if (forceShuffle) {
                            mService.setShuffleMode(MusicDataManager.SHUFFLE_NORMAL);
                        }
                        int songNum = songs.size();
                        long[] ret = new long[songNum];
                        for (int i = 0; i < songNum; i++) {
                            Song song = songs.get(i);
                            if (song != null) {
                                ret[i] = song.songId;
                            }
                        }
                        Map<Long, Song> map = new HashMap<>();
                        int i = 0;
                        for (Song info : songs) {
                            map.put(ret[i], info);
                            i++;
                        }
                        Timber.e("===>>> playAll open start");
                        mService.open(map, ret, forceShuffle ? -1 : position);
                        Timber.e("===>>> playAll open end");
                        mService.play();
                    } catch (Throwable t) {
                        CrashReport.postCatchedException(t);
                    }
                    Timber.e("===>>> playAll end");
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aBoolean -> {
                });
    }

    public static boolean isShort() {
        try {
            return mService.isShort();
        } catch (Exception e) {
            return false;
        }
    }

    public static String getPath() {
        try {
            return mService.getPath();
        } catch (Exception e) {
            return null;
        }
    }

    public static void stop() {
        Observable.create((ObservableOnSubscribe<Boolean>)
                emitter -> {
                    try {
                        mService.stop();
                    } catch (Exception e) {
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aBoolean -> {
                });
    }

    public static final int getSongCountForAlbumInt(final Context context, final long id) {
        int songCount = 0;
        if (id == -1) {
            return songCount;
        }

        Uri uri = ContentUris.withAppendedId(MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI, id);
        Cursor cursor = context.getContentResolver().query(uri,
                new String[]{MediaStore.Audio.AlbumColumns.NUMBER_OF_SONGS}, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            if (!cursor.isAfterLast()) {
                if (!cursor.isNull(0)) {
                    songCount = cursor.getInt(0);
                }
            }
            cursor.close();
            cursor = null;
        }

        return songCount;
    }

    public static final String getReleaseDateForAlbum(final Context context, final long id) {
        if (id == -1) {
            return null;
        }
        Uri uri = ContentUris.withAppendedId(MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI, id);
        Cursor cursor = context.getContentResolver().query(uri, new String[]{
                MediaStore.Audio.AlbumColumns.FIRST_YEAR
        }, null, null, null);
        String releaseDate = null;
        if (cursor != null) {
            cursor.moveToFirst();
            if (!cursor.isAfterLast()) {
                releaseDate = cursor.getString(0);
            }
            cursor.close();
            cursor = null;
        }
        return releaseDate;
    }

    public static void seek(final long position) {
        Observable.create((ObservableOnSubscribe<Boolean>)
                emitter -> {
                    try {
                        mService.seek(position);
                    } catch (Exception ignored) {
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aBoolean -> {
                });
    }

    public static void seekRelative(final long deltaInMs) {
        if (mService != null) {
            try {
                mService.seekRelative(deltaInMs);
            } catch (Exception ignored) {
            }
        }
    }

    public static final long position() {
        if (mService != null) {
            try {
                return mService.position();
            } catch (Exception ignored) {
            }
        }
        return 0;
    }

    public static final int secondPosition() {
        if (mService != null) {
            try {
                return mService.secondPosition();
            } catch (Exception ignored) {
            }
        }
        return 0;
    }

    public static long duration() {
        if (mService != null) {
            try {
                return mService.duration();
            } catch (Exception ignored) {
            }
        }
        return 0;
    }

    public static void clearQueue() {
        Observable.create((ObservableOnSubscribe<Boolean>)
                emitter -> {
                    try {
                        if (mService != null)
                            mService.removeTracks(0, Integer.MAX_VALUE);
                    } catch (final Exception ignored) {
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aBoolean -> {
                });
    }

    /**
     * 在播放列表尾部插入歌曲
     *
     * @param song song to insert
     */
    public static void addToQueue(Song song) {
        if (mService == null) {
            return;
        }
        try {
            long[] id = new long[1];
            id[0] = song.songId;
            Map<Long, Song> infos = new HashMap<>();
            infos.put(song.songId, song);
            mService.enqueue(id, infos, MusicService.LAST);
            //final String message = makeLabel(context, R.plurals.NNNtrackstoqueue, list.length);
            //Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        } catch (final RemoteException ignored) {
        }
    }

    /**
     * 删除播放队列中歌曲
     *
     * @param position song position in queue
     */
    public static void removeFromQueue(int position) {
        try {
            mService.removeTracks(position, position);
        } catch (final Exception ignored) {
        }
    }

    public static String makeLabel(final Context context, final int pluralInt,
                                   final int number) {
        return context.getResources().getQuantityString(pluralInt, number, number);
    }

    public static void addToPlaylist(final Context context, final long[] ids, final long playlistid) {
        final int size = ids.length;
        final ContentResolver resolver = context.getContentResolver();
        final String[] projection = new String[]{
                "max(" + "play_order" + ")",
        };
        final Uri uri = MediaStore.Audio.Playlists.Members.getContentUri("external", playlistid);
        Cursor cursor = null;
        int base = 0;

        try {
            cursor = resolver.query(uri, projection, null, null, null);

            if (cursor != null && cursor.moveToFirst()) {
                base = cursor.getInt(0) + 1;
            }
        } finally {
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
        }

        int numinserted = 0;
        for (int offSet = 0; offSet < size; offSet += 1000) {
            makeInsertItems(ids, offSet, 1000, base);
            numinserted += resolver.bulkInsert(uri, mContentValuesCache);
        }

    }

    public static void makeInsertItems(final long[] ids, final int offset, int len, final int base) {
        if (offset + len > ids.length) {
            len = ids.length - offset;
        }

        if (mContentValuesCache == null || mContentValuesCache.length != len) {
            mContentValuesCache = new ContentValues[len];
        }
        for (int i = 0; i < len; i++) {
            if (mContentValuesCache[i] == null) {
                mContentValuesCache[i] = new ContentValues();
            }
            mContentValuesCache[i].put(MediaStore.Audio.Playlists.Members.PLAY_ORDER, base + offset + i);
            mContentValuesCache[i].put(MediaStore.Audio.Playlists.Members.AUDIO_ID, ids[offset + i]);
        }
    }

    public static final long createPlaylist(final Context context, final String name) {
        if (name != null && name.length() > 0) {
            final ContentResolver resolver = context.getContentResolver();
            final String[] projection = new String[]{
                    MediaStore.Audio.PlaylistsColumns.NAME
            };
            final String selection = MediaStore.Audio.PlaylistsColumns.NAME + " = '" + name + "'";
            Cursor cursor = resolver.query(MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI,
                    projection, selection, null, null);
            if (cursor.getCount() <= 0) {
                final ContentValues values = new ContentValues(1);
                values.put(MediaStore.Audio.PlaylistsColumns.NAME, name);
                final Uri uri = resolver.insert(MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI,
                        values);
                return Long.parseLong(uri.getLastPathSegment());
            }
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
            return -1;
        }
        return -1;
    }

    public static void exitService() {
//        if (mService == null) {
//            return;
//        }
        try {
            mConnectionMap.clear();
            Log.e("exitmp", "Destroying service");
            mService.exit();
        } catch (Exception e) {
        }
    }

    public static void timing(int time) {
        if (mService == null) {
            return;
        }
        try {
            mService.timing(time);
        } catch (Exception e) {

        }
    }

    public static final class ServiceBinder implements ServiceConnection {
        private final ServiceConnection mCallback;
        private final Context mContext;


        public ServiceBinder(final ServiceConnection callback, final Context context) {
            mCallback = callback;
            mContext = context;
        }

        @Override
        public void onServiceConnected(final ComponentName className, final IBinder service) {
            mService = MediaAidlInterface.Stub.asInterface(service);
            if (mCallback != null) {
                mCallback.onServiceConnected(className, service);
            }
            initPlaybackServiceWithSettings(mContext);
        }

        @Override
        public void onServiceDisconnected(final ComponentName className) {
            if (mCallback != null) {
                mCallback.onServiceDisconnected(className);
            }
            mService = null;
        }
    }

    public static final class ServiceToken {
        public ContextWrapper mWrappedContext;

        public ServiceToken(final ContextWrapper context) {
            mWrappedContext = context;
        }
    }


}
