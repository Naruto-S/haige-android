package com.music.player.haige.mvp.ui.music.service;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;

import com.music.player.haige.app.HaigeApplication;
import com.music.player.haige.mvp.ui.music.service.cache.MusicLog;
import com.music.player.haige.mvp.ui.utils.Utils;
import com.tencent.bugly.crashreport.CrashReport;

import java.io.IOException;
import java.lang.ref.WeakReference;

import timber.log.Timber;

/**
 * ================================================
 * Created by huangcong on 2018/3/13.
 * <p>
 * 音乐播放器，支持在线播放
 * ================================================
 */

public class MultiPlayer implements MediaPlayer.OnErrorListener, MediaPlayer.OnInfoListener,
        MediaPlayer.OnCompletionListener {

    private static final String TAG = MultiPlayer.class.getName();

    private final WeakReference<MusicService> mService;

    private MediaPlayer mCurrentMediaPlayer = new MediaPlayer();

    private MediaPlayer mNextMediaPlayer;

    private Handler mHandler;

    private boolean mIsInitialized = false;

    private String mNextMediaPath;

    private boolean isFirstLoad = true;


    private int sencondaryPosition = 0;

    private Handler handler = new Handler();


    public MultiPlayer(final MusicService service) {
        mService = new WeakReference<MusicService>(service);
        mCurrentMediaPlayer.setWakeMode(mService.get(), PowerManager.PARTIAL_WAKE_LOCK);
    }

    public void setDataSource(final String path) {
        mIsInitialized = setDataSourceImpl(mCurrentMediaPlayer, path);
        if (mIsInitialized) {
            setNextDataSource(null);
        }
    }

    public void setNextDataSource(final String path) {
        mNextMediaPath = null;
        mIsNextInitialized = false;
        try {
            if (Utils.isNotNull(mCurrentMediaPlayer)) {
                mCurrentMediaPlayer.setNextMediaPlayer(null);
            }

            if (mNextMediaPlayer != null) {
                mNextMediaPlayer.release();
                mNextMediaPlayer = null;
            }
            if (path == null) {
                return;
            }

            mNextMediaPlayer = new MediaPlayer();
            mNextMediaPlayer.setWakeMode(mService.get(), PowerManager.PARTIAL_WAKE_LOCK);
            mNextMediaPlayer.setAudioSessionId(getAudioSessionId());

            if (setNextDataSourceImpl(mNextMediaPlayer, path)) {
                mNextMediaPath = path;
                mCurrentMediaPlayer.setNextMediaPlayer(mNextMediaPlayer);
                mHandler.post(setNextMediaPlayerIfPrepared);
            } else {
                if (mNextMediaPlayer != null) {
                    mNextMediaPlayer.release();
                    mNextMediaPlayer = null;
                }
            }
        } catch (Throwable t) {
            CrashReport.postCatchedException(t);
            return;
        }
    }

    boolean mIsTrackPrepared = false;
    boolean mIsTrackNet = false;
    boolean mIsNextTrackPrepared = false;
    boolean mIsNextInitialized = false;
    boolean mIllegalState = false;

    private boolean setDataSourceImpl(final MediaPlayer player, final String path) {
        mIsTrackNet = false;
        mIsTrackPrepared = false;
        try {
            player.reset();
            player.setAudioStreamType(AudioManager.STREAM_MUSIC);
            if (path.startsWith("content://")) {
                player.setOnPreparedListener(null);
                player.setDataSource(HaigeApplication.getInstance(), Uri.parse(path));
                player.prepare();
                mIsTrackPrepared = true;
                player.setOnInfoListener(this);
                player.setOnCompletionListener(this);
            } else {
                player.setDataSource(path);
                player.setOnPreparedListener(preparedListener);
                player.prepareAsync();
                mIsTrackNet = true;
            }
            if (mIllegalState) {
                mIllegalState = false;
            }
        } catch (final IOException todo) {
            return false;
        } catch (final IllegalArgumentException todo) {
            return false;
        } catch (final IllegalStateException todo) {
            todo.printStackTrace();
            if (!mIllegalState) {
                Timber.d(TAG + " mcurrentmediaplayer invoke IllegalState");
                mCurrentMediaPlayer = null;
                mCurrentMediaPlayer = new MediaPlayer();
                mCurrentMediaPlayer.setWakeMode(mService.get(), PowerManager.PARTIAL_WAKE_LOCK);
                mCurrentMediaPlayer.setAudioSessionId(getAudioSessionId());
                setDataSourceImpl(mCurrentMediaPlayer, path);
                mIllegalState = true;
            } else {
                Timber.d(TAG + "mcurrentmediaplayer invoke IllegalState ,and try set again failed ,setnext");
                mIllegalState = false;
                return false;
            }
        }

        player.setOnErrorListener(this);
        player.setOnBufferingUpdateListener(bufferingUpdateListener);
        return true;
    }

    private boolean setNextDataSourceImpl(final MediaPlayer player, final String path) {
        mIsNextTrackPrepared = false;
        try {
            player.reset();
            player.setAudioStreamType(AudioManager.STREAM_MUSIC);
            if (path.startsWith("content://")) {
                player.setOnPreparedListener(preparedNextListener);
                player.setDataSource(HaigeApplication.getInstance(), Uri.parse(path));
                player.prepare();
            } else {
                player.setDataSource(path);
                player.setOnPreparedListener(preparedNextListener);
                player.prepare();
                mIsNextTrackPrepared = false;
            }

        } catch (final IOException todo) {
            return false;
        } catch (final IllegalArgumentException todo) {
            return false;
        }
        player.setOnCompletionListener(this);
        player.setOnErrorListener(this);
        //  player.setOnBufferingUpdateListener(this);
        return true;
    }

    public void setHandler(final Handler handler) {
        mHandler = handler;
    }


    public boolean isInitialized() {
        return mIsInitialized;
    }

    public boolean isTrackPrepared() {
        return mIsTrackPrepared;
    }

    public void start() {
        Timber.d(TAG + " mIsTrackNet, " + mIsTrackNet);
        if (!mIsTrackNet) {
            mService.get().sendUpdateBuffer(100);
            sencondaryPosition = 100;
            if (Utils.isNotNull(mCurrentMediaPlayer)) {
                mCurrentMediaPlayer.start();
            }
        } else {
            sencondaryPosition = 0;
            mService.get().loading(true);
            handler.postDelayed(startMediaPlayerIfPrepared, 50);
        }
        mService.get().notifyChange(MusicService.MUSIC_CHANGED);
    }

    MediaPlayer.OnPreparedListener preparedListener = new MediaPlayer.OnPreparedListener() {
        @Override
        public void onPrepared(MediaPlayer mp) {
            if (isFirstLoad) {
                long seekPos = mService.get().getLastSeekPos();
                Timber.e(TAG + " seekpos = " + seekPos);
                seek(seekPos >= 0 ? seekPos : 0);
                isFirstLoad = false;
            }
            mService.get().notifyChange(MusicService.TRACK_PREPARED);
            mService.get().notifyChange(MusicService.META_CHANGED);
            mp.setOnCompletionListener(MultiPlayer.this);
            mIsTrackPrepared = true;
        }
    };

    MediaPlayer.OnPreparedListener preparedNextListener = mp -> mIsNextTrackPrepared = true;

    MediaPlayer.OnBufferingUpdateListener bufferingUpdateListener = new MediaPlayer.OnBufferingUpdateListener() {

        @Override
        public void onBufferingUpdate(MediaPlayer mp, int percent) {
            if (sencondaryPosition != 100)
                mService.get().sendUpdateBuffer(percent);
            sencondaryPosition = percent;
        }
    };

    Runnable setNextMediaPlayerIfPrepared = new Runnable() {
        int count = 0;

        @Override
        public void run() {
            try {
                if (mIsNextTrackPrepared && mIsInitialized && Utils.isNotNull(mCurrentMediaPlayer)) {
                    mCurrentMediaPlayer.setNextMediaPlayer(mNextMediaPlayer);
                } else if (count < 60) {
                    handler.postDelayed(setNextMediaPlayerIfPrepared, 100);
                }
                count++;
            } catch (Throwable t) {
                CrashReport.postCatchedException(t);
            }
        }
    };

    Runnable startMediaPlayerIfPrepared = new Runnable() {

        @Override
        public void run() {
            Timber.d(TAG + " mIsTrackPrepared, " + mIsTrackPrepared);
            if (mIsTrackPrepared && Utils.isNotNull(mCurrentMediaPlayer)) {
                mCurrentMediaPlayer.start();
                if (mService.get().shouldGotoNext()) {
                    mService.get().gotoNext(true);
                    Timber.e("play to go");
                }
                mService.get().loading(false);
            } else {
                handler.postDelayed(startMediaPlayerIfPrepared, 700);
            }
        }
    };


    public void stop() {
        handler.removeCallbacks(setNextMediaPlayerIfPrepared);
        mService.get().loading(false);
        handler.removeCallbacks(startMediaPlayerIfPrepared);
        if (Utils.isNotNull(mCurrentMediaPlayer)) {
            mCurrentMediaPlayer.reset();
        }
        mIsInitialized = false;
        mIsTrackPrepared = false;
    }


    public void release() {
        if (Utils.isNotNull(mCurrentMediaPlayer)) {
            mCurrentMediaPlayer.release();
        }
    }


    public void pause() {
        mService.get().loading(false);
        handler.removeCallbacks(startMediaPlayerIfPrepared);
        if (Utils.isNotNull(mCurrentMediaPlayer)) {
            mCurrentMediaPlayer.pause();
        }
    }


    public long duration() {
        if (mIsTrackPrepared && Utils.isNotNull(mCurrentMediaPlayer)) {
            return mCurrentMediaPlayer.getDuration();
        }
        return -1;
    }


    public long position() {
        if (mIsTrackPrepared && Utils.isNotNull(mCurrentMediaPlayer)) {
            return mCurrentMediaPlayer.getCurrentPosition();
        }
        return -1;
    }

    public int secondPosition() {
        if (mIsTrackPrepared) {
            return sencondaryPosition;
        }
        return -1;
    }

    public long seek(final long whereto) {
        if (Utils.isNotNull(mCurrentMediaPlayer)) {
            mCurrentMediaPlayer.seekTo((int) whereto);
        }
        return whereto;
    }


    public void setVolume(final float vol) {
        try {
            mCurrentMediaPlayer.setVolume(vol, vol);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getAudioSessionId() {
        try {
            return mCurrentMediaPlayer.getAudioSessionId();
        } catch (Exception e) {
            return 0;
        }
    }

    public void setAudioSessionId(final int sessionId) {
        if (Utils.isNotNull(mCurrentMediaPlayer)) {
            mCurrentMediaPlayer.setAudioSessionId(sessionId);
        }
    }

    @Override
    public boolean onError(final MediaPlayer mp, final int what, final int extra) {
        Timber.e(TAG + " Music Server Error what: " + what + " extra: " + extra);
        switch (what) {
            //case MediaPlayer.MEDIA_ERROR_UNKNOWN:
            case MediaPlayer.MEDIA_ERROR_SERVER_DIED:
                final MusicService service = mService.get();
                final MusicService.TrackErrorInfo errorInfo = new MusicService.TrackErrorInfo(service.getAudioId(),
                        service.getTrackName());

                mIsInitialized = false;
                mIsTrackPrepared = false;
                mCurrentMediaPlayer.release();
                mCurrentMediaPlayer = new MediaPlayer();
                mCurrentMediaPlayer.setWakeMode(service, PowerManager.PARTIAL_WAKE_LOCK);
                Message msg = mHandler.obtainMessage(MultiPlayerHandler.SERVER_DIED, errorInfo);
                mHandler.sendMessageDelayed(msg, 2000);
                return true;
            case MediaPlayer.MEDIA_ERROR_UNKNOWN:
                break;
            default:
                break;
        }
        return false;
    }

    @Override
    public void onCompletion(final MediaPlayer mp) {
        Timber.e(TAG + " Music Server Completion");
        if (mp == mCurrentMediaPlayer && mNextMediaPlayer != null) {
            Timber.w(TAG + " completion 1");
            mCurrentMediaPlayer.release();
            mCurrentMediaPlayer = mNextMediaPlayer;
            mNextMediaPath = null;
            mNextMediaPlayer = null;
            mHandler.sendEmptyMessage(MultiPlayerHandler.TRACK_WENT_TO_NEXT);
        } else {
            Timber.w(TAG + " completion 2");
            mService.get().mWakeLock.acquire(30000);
            mHandler.sendEmptyMessage(MultiPlayerHandler.TRACK_ENDED);
            mHandler.sendEmptyMessage(MultiPlayerHandler.RELEASE_WAKELOCK);
        }
    }

    @Override
    public boolean onInfo(MediaPlayer mp, int what, int extra) {
        MusicLog.playerD("onInfo what == " + what + "  extra == " + extra);
        return false;
    }

    /**
     * http://www.jianshu.com/p/6c938df18413
     * Do not change these values without updating their counterparts in native
     * int MEDIA_INFO_UNKNOWN = 1;//未知信息
     * int MEDIA_INFO_STARTED_AS_NEXT = 2;//播放下一条
     * int MEDIA_INFO_VIDEO_RENDERING_START = 3;//视频开始整备中
     * int MEDIA_INFO_VIDEO_TRACK_LAGGING = 700;//视频日志跟踪
     * int MEDIA_INFO_BUFFERING_START = 701;//开始缓冲中
     * int MEDIA_INFO_BUFFERING_END = 702;//缓冲结束
     * int MEDIA_INFO_NETWORK_BANDWIDTH = 703;//网络带宽，网速方面
     * int MEDIA_INFO_BAD_INTERLEAVING = 800;//
     * int MEDIA_INFO_NOT_SEEKABLE = 801;//不可设置播放位置，直播方面
     * int MEDIA_INFO_METADATA_UPDATE = 802;//
     * int MEDIA_INFO_TIMED_TEXT_ERROR = 900;
     * int MEDIA_INFO_UNSUPPORTED_SUBTITLE = 901;//不支持字幕
     * int MEDIA_INFO_SUBTITLE_TIMED_OUT = 902;//字幕超时
     * <p>
     * int MEDIA_INFO_VIDEO_INTERRUPT = -10000;//数据连接中断
     * int MEDIA_INFO_VIDEO_ROTATION_CHANGED = 10001;//视频方向改变
     * int MEDIA_INFO_AUDIO_RENDERING_START = 10002;//音频开始整备中
     * <p>
     * int MEDIA_ERROR_UNKNOWN = 1;//未知错误
     * int MEDIA_ERROR_SERVER_DIED = 100;//视频中断，一般是视频源异常或者不支持的视频类型
     * int MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK = 200;//The video is streamed and its container is not valid for progressive
     playback i.e the video's index (e.g moov atom) is not at the start
     of thefile.由于网络下载数据比较慢，而且Android里面读取网络数据的buffer缓冲
     和清空机制，如果经常需要在文件前后跳转读取时就比较低效率了，比如你现在读取文件
     末尾offset1除size1大小的数据，这个时候播放器会多读取一些存放在buffer中，如果
     这个时候又要跳到文件开始的某个offset2处读取size2大小的数据，播放器需要将先前
     存放在buffer中的数据清空，然后将offset2发给服务器，让服务器给播放器发offset2
     这个位置的数据，如果经常性这样跳转的话，就比较耗时和低效率了。此时会出现这个参数
     * int MEDIA_ERROR_IO = -1004;//IO错误
     * int MEDIA_ERROR_MALFORMED = -1007;
     * int MEDIA_ERROR_UNSUPPORTED = -1010;//数据不支持
     * int MEDIA_ERROR_TIMED_OUT = -110;//数据超时
     */
}
