package com.music.player.haige.mvp.presenter;

import com.music.player.haige.mvp.contract.AppBasePresenter;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.Api;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.ui.utils.Utils;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;
import me.jessyan.rxerrorhandler.handler.ErrorHandleSubscriber;

/**
 * ================================================
 * Created by huangcong on 2018/3/3.
 * ================================================
 */

public class PlayQueueFPresenter extends AppBasePresenter<MVContract.PlayQueueModel, MVContract.PlayQueueView> {

    private static final long UPDATE_DELAY = 200;

    @Inject
    RxErrorHandler mErrorHandler;

    private Disposable mUpateMusicTimeDisp;


    @Inject
    public PlayQueueFPresenter(MVContract.PlayQueueModel model, MVContract.PlayQueueView view) {
        super(model, view);
    }

    /**
     * 加载当前正在播放的音乐列表
     */
    public void loadCurrentMusic() {
        if (Utils.isNotNull(mModel.loadCurrentMusic())) {
            buildObservable(mModel.loadCurrentMusic())
                    .subscribe(new ErrorHandleSubscriber<ArrayList<Song>>(mErrorHandler) {
                        @Override
                        public void onNext(ArrayList<Song> response) {
                            if (Utils.isNotNull(mRootView)) {
                                if (response != null) {
                                    mRootView.onSuccess(Api.Action.MUSIC_PLAY_QUEUE, response);
                                } else {
                                    mRootView.onError(Api.Action.MUSIC_PLAY_QUEUE, Api.State.NORMAL.getCode(), Api.State.NORMAL.getMsg());
                                }
                            }
                        }

                        @Override
                        public void onError(Throwable t) {
                            super.onError(t);
                            if (Utils.isNotNull(mRootView)) {
                                mRootView.onError(Api.Action.MUSIC_PLAY_QUEUE, "-1", t.getMessage());
                            }
                        }
                    });
        }
    }

    public void startUpateMusicTime() {
        mUpateMusicTimeDisp = Observable.interval(0, UPDATE_DELAY, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aLong -> {
                    if (Utils.isNotNull(mRootView)) {
                        mRootView.onUpateMusicTime();
                    }
                });
    }
}
