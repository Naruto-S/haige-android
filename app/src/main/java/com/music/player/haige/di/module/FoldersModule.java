package com.music.player.haige.di.module;

import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.FoldersModel;

import dagger.Module;
import dagger.Provides;

/**
 * ================================================
 * Created by huangcong on 2018/3/2.
 * ================================================
 */
@Module
public class FoldersModule {
    private MVContract.FoldersView view;

    public FoldersModule(MVContract.FoldersView view){
        this.view = view;
    }

    @FragmentScope
    @Provides
    MVContract.FoldersView provideView(){
        return view;
    }

    @FragmentScope
    @Provides
    MVContract.FoldersModel provideModel(FoldersModel model){
        return model;
    }
}
