package com.music.player.haige.app.utils.launcher;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.music.player.haige.app.constants.IntentConstants;
import com.music.player.haige.app.utils.statistics.CommonStatistics;
import com.music.player.haige.app.utils.statistics.StatisticsConstant;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.model.entity.user.UserBean;
import com.music.player.haige.mvp.ui.launch.SplashActivity;
import com.music.player.haige.mvp.ui.main.activity.MainActivity;
import com.music.player.haige.mvp.ui.main.activity.MusicCompleteActivity;
import com.music.player.haige.mvp.ui.main.activity.TagMusicActivity;
import com.music.player.haige.mvp.ui.upload.MusicTypeActivity;
import com.music.player.haige.mvp.ui.upload.UploadActivity;
import com.music.player.haige.mvp.ui.user.FansListActivity;
import com.music.player.haige.mvp.ui.user.OtherUserActivity;

import java.util.ArrayList;

/**
 * Created by Naruto on 2018/1/11.
 * <p>
 * 负责Activity跳转
 * <p>
 * <p> ActivityLauncherStart.startActivity(this, WorkoutDetailsActivity.class) </p>
 */

public class ActivityLauncherStart extends ActivityStartBase {

    public static Intent getSplashIntent(Context context, int notifyType) {
        CommonStatistics.sendEvent(StatisticsConstant.NOTICE_CLICK);

        Intent intent = new Intent(context, SplashActivity.class);
        intent.putExtra(IntentConstants.EXTRA_CLICK_NOTIFY, notifyType);
        return intent;
    }

    public static Intent getSplashIntent(Context context, Song song, int notifyType) {
        CommonStatistics.sendEvent(StatisticsConstant.NOTICE_CLICK);

        Intent intent = new Intent(context, SplashActivity.class);
        intent.putExtra(IntentConstants.EXTRA_NOTIFY_MUSIC, song);
        intent.putExtra(IntentConstants.EXTRA_CLICK_NOTIFY, notifyType);
        return intent;
    }

    public static void startMain(Context context, Song song, int notifyType) {
        startActivity((Activity) context, MainActivity.class, intent -> {
            intent.putExtra(IntentConstants.EXTRA_NOTIFY_MUSIC, song);
            intent.putExtra(IntentConstants.EXTRA_CLICK_NOTIFY, notifyType);
        });
    }

    public static void startOtherUser(Activity activity, UserBean otherUser) {
        startActivity(activity, OtherUserActivity.class, intent -> intent.putExtra(IntentConstants.EXTRA_OTHER_USER, otherUser));
    }

    public static void startFollowList(Activity activity, UserBean otherUser, int type) {
        startActivity(activity, FansListActivity.class, intent -> {
            intent.putExtra(IntentConstants.EXTRA_OTHER_USER, otherUser);
            intent.putExtra(IntentConstants.EXTRA_FOLLOW_TYPE, type);
        });
    }

    public static void startTagMusic(Activity activity, String tag) {
        startActivity(activity, TagMusicActivity.class, intent -> intent.putExtra(IntentConstants.EXTRA_MUSIC_TAG, tag));
    }

    public static void startMusicComplete(Activity activity, Song song) {
        startActivity(activity, MusicCompleteActivity.class, intent -> intent.putExtra(IntentConstants.EXTRA_CURRENT_MUSIC, song));
    }

    public static void startUpload(Activity activity, Song song) {
        startActivity(activity, UploadActivity.class, intent -> intent.putExtra(IntentConstants.EXTRA_CURRENT_MUSIC, song));
    }

    public static void startMusicType(Activity activity, ArrayList<String> types) {
        startActivity(activity, MusicTypeActivity.class, intent -> intent.putExtra(IntentConstants.EXTRA_MUSIC_TYPE_LIST, types));
    }
}
