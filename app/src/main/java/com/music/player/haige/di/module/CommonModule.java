package com.music.player.haige.di.module;

import com.hc.core.di.scope.ActivityScope;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.CommonModel;

import dagger.Module;
import dagger.Provides;

/**
 * ================================================
 * Created by huangcong on 2018/2/23.
 * ================================================
 */

@Module
public class CommonModule {
    private MVContract.CommonView view;

    public CommonModule(MVContract.CommonView view) {
        this.view = view;
    }

    @ActivityScope
    @Provides
    MVContract.CommonView provideView() {
        return view;
    }

    @ActivityScope
    @Provides
    MVContract.CommonModel provideModel(CommonModel model){
        return model;
    }


}
