package com.music.player.haige.app.image.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.generic.GenericDraweeHierarchyBuilder;
import com.facebook.drawee.generic.GenericDraweeHierarchyInflater;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.music.player.haige.app.image.loader.listener.OnImageLoaderListener;
import com.music.player.haige.app.image.release.DisplayOptions;
import com.music.player.haige.app.image.release.ImageOptions;

/**
 * 大部分情况下使用该View可以满足需求
 * <p>
 * public static final class attr {
 * public static int actualImageScaleType = 0x7f01012f;
 * public static int actualImageUri = 0x7f0101d5;
 * public static int backgroundImage = 0x7f010130;
 * public static int fadeDuration = 0x7f010124;
 * public static int failureImage = 0x7f01012a;
 * public static int failureImageScaleType = 0x7f01012b;
 * public static int overlayImage = 0x7f010131;
 * public static int placeholderImage = 0x7f010126;
 * public static int placeholderImageScaleType = 0x7f010127;
 * public static int pressedStateOverlayImage = 0x7f010132;
 * public static int progressBarAutoRotateInterval = 0x7f01012e;
 * public static int progressBarImage = 0x7f01012c;
 * public static int progressBarImageScaleType = 0x7f01012d;
 * public static int retryImage = 0x7f010128;
 * public static int retryImageScaleType = 0x7f010129;
 * public static int roundAsCircle = 0x7f010133;
 * public static int roundBottomLeft = 0x7f010138;
 * public static int roundBottomRight = 0x7f010137;
 * public static int roundTopLeft = 0x7f010135;
 * public static int roundTopRight = 0x7f010136;
 * public static int roundWithOverlayColor = 0x7f010139;
 * public static int roundedCornerRadius = 0x7f010134;
 * public static int roundingBorderColor = 0x7f01013b;
 * public static int roundingBorderPadding = 0x7f01013c;
 * public static int roundingBorderWidth = 0x7f01013a;
 * public static int viewAspectRatio = 0x7f010125;
 * }
 * Created by ZaKi on 2016/9/2.
 */
public class HaigeImageView extends SimpleDraweeView implements ImageFetcher {

    protected DisplayOptions.Builder mBuilder;
    //真正的功能由FrescoWapper完成
    private FrescoWapper frescoWapper;
    private int width;
    private int height;

    private String uri;

    private OnImageLoaderListener onImageLoaderListener;

    public HaigeImageView(Context context, GenericDraweeHierarchy hierarchy) {
        super(context, hierarchy);
        init();
    }

    public HaigeImageView(Context context) {
        super(context);
        init();
    }

    public HaigeImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    //重写该方法，得到默认的DisplayOptions
    @Override
    protected void inflateHierarchy(Context context, @Nullable AttributeSet attrs) {
        GenericDraweeHierarchyBuilder builder =
                GenericDraweeHierarchyInflater.inflateBuilder(context, attrs);
        setAspectRatio(builder.getDesiredAspectRatio());
        setHierarchy(builder.build());

        mBuilder = new DisplayOptions.Builder(builder);
    }

    private void init() {
        frescoWapper = new FrescoWapper() {
            @Override
            protected DraweeController getController() {
                return HaigeImageView.this.getController();
            }

            @Override
            protected void setController(DraweeController controller) {
                HaigeImageView.this.setController(controller);
            }

            @Override
            protected GenericDraweeHierarchy getHierarchy() {
                return HaigeImageView.this.getHierarchy();
            }

            @Override
            protected void setHierarchy(GenericDraweeHierarchy hierarchy) {
                HaigeImageView.this.setHierarchy(hierarchy);
            }

            @Override
            protected View obtainView() {
                return HaigeImageView.this;
            }

            @Override
            public DisplayOptions.Builder display() {
                return mBuilder;
            }
        };
    }


    public DisplayOptions.Builder display() {
        return mBuilder;
    }

    @Override
    public void setImageURI(String uri) {
        setImageURI(uri, null);
    }

    @Override
    public void setImageURI(String uri, OnImageLoaderListener onImageLoaderListener) {
        setImageURI(uri, null, onImageLoaderListener);
    }

    @Override
    public void setImageURI(String uri, DisplayOptions displayOptions, OnImageLoaderListener
            onImageLoaderListener) {

        if (displayOptions != null) {
            mBuilder.clone(displayOptions);
        }
        this.uri = uri;
        if (this.width > 0 && this.height > 0) {
            resizeImage();
            frescoWapper.setImageURI(uri, mBuilder.build(), onImageLoaderListener);
        } else {
            this.onImageLoaderListener = onImageLoaderListener;
        }
//        frescoWapper.setImageURI(uri, mBuilder.build(), onImageLoaderListener);
    }

    /**
     * 自适应宽高
     */
    private void resizeImage() {
        ImageOptions imageOptions = mBuilder.getImageOptions();
        if (imageOptions == null) {
            imageOptions = new ImageOptions.Builder().setWidth(width)
                    .setHeight(height)
                    .build();
            mBuilder.setImageOptions(imageOptions);
        } else {
            imageOptions.setWidth(this.width);
            imageOptions.setHeight(this.height);
        }
    }


    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        this.width = w;
        this.height = h;
        if (this.uri != null && w > 0 && h > 0) {
            resizeImage();
            frescoWapper.setImageURI(uri, mBuilder.build(), onImageLoaderListener);
        }
    }
}
