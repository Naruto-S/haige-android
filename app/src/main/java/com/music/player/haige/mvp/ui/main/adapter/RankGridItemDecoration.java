package com.music.player.haige.mvp.ui.main.adapter;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Naruto on 2018/8/25.
 */

public class RankGridItemDecoration extends RecyclerView.ItemDecoration {

    private int spacing;

    public RankGridItemDecoration(int space) {
        this.spacing = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view); // item position

        if (position == 0) {
            outRect.left = spacing;
            outRect.right = spacing;
            outRect.top = 0;
            outRect.bottom = spacing;
        } else if (position % 2 == 1) {
            outRect.left = spacing;
            outRect.right = spacing / 2;
            outRect.top = 0;
            outRect.bottom = spacing;
        } else if (position % 2 == 0) {
            outRect.left = spacing / 2;
            outRect.right = spacing;
            outRect.top = 0;
            outRect.bottom = spacing;
        }
    }
}