package com.music.player.haige.app.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by Messi on 17-1-16.
 */

public class TimeTickReceiver extends Observable {

    private static final Object sLock = new Object();
    private static TimeTickReceiver sInstance;
    private RegularReminderReceiver mReceiver;
    private Context mContext;

    private TimeTickReceiver(Context context) {
        mReceiver = new RegularReminderReceiver();
        mContext = context.getApplicationContext();
    }

    public static TimeTickReceiver getInstance(Context context) {
        synchronized (sLock) {
            if (sInstance == null) {
                sInstance = new TimeTickReceiver(context);
            }
            return sInstance;
        }
    }

    @Override
    public void addObserver(Observer observer) {
        super.addObserver(observer);
        if (countObservers() > 0 && !mReceiver.isRegisted()) {
            mReceiver.register(mContext);
        }
    }

    @Override
    public synchronized void deleteObserver(Observer observer) {
        super.deleteObserver(observer);
        if (countObservers() == 0 && mReceiver.isRegisted()) {
            mReceiver.unregister(mContext);
        }
    }

    private class RegularReminderReceiver extends BroadcastReceiver {

        private boolean mRegisted = false;

        public boolean isRegisted() {
            return mRegisted;
        }

        public void unregister(Context context) {
            if (context != null) {
                context.unregisterReceiver(this);
                mRegisted = false;
            }
        }

        public void register(Context context) {
            if (context != null) {
                IntentFilter dateChangeFilter = new IntentFilter();
                dateChangeFilter.addAction(Intent.ACTION_TIME_TICK);
                context.registerReceiver(this, dateChangeFilter);
                mRegisted = true;
            }
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (Intent.ACTION_TIME_TICK.equals(action)) {
                setChanged();
                notifyObservers();
            }
        }
    }
}
