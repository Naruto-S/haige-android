package com.music.player.haige.mvp.model.entity.comment;

import com.music.player.haige.mvp.model.entity.BaseResponse;

import java.io.Serializable;

/**
 * Created by Naruto on 2018/5/13.
 */

public class SendCommentResponse extends BaseResponse<String> implements Serializable {

    private CommentRequest request;

    public CommentRequest getRequest() {
        return request;
    }

    public void setRequest(CommentRequest request) {
        this.request = request;
    }

    @Override
    public String toString() {
        return "SendCommentResponse{" +
                "request=" + request +
                '}';
    }
}
