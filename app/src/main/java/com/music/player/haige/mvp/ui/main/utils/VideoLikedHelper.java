package com.music.player.haige.mvp.ui.main.utils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.music.player.haige.R;
import com.music.player.haige.mvp.ui.utils.DebugLogger;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;
import com.music.player.haige.mvp.ui.utils.Utils;
import com.music.player.haige.mvp.ui.utils.ViewUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Naruto on 2017/7/27.
 */

public class VideoLikedHelper {

    private static final int[] DEGREES = {-30, -22, -15, 0, 15, 22, 30};
    private static final int LIKED_SIZE = ResourceUtils.dpToPX(110);

    private List<LikeAnimateView> likeAnimateAcquire = new ArrayList<>();

    private LikeAnimateView acquire(Context context, boolean isFullScreen, int pointX, int pointY) {
        LikeAnimateView iv;
        FrameLayout.LayoutParams layoutParams = null;
        if (!likeAnimateAcquire.isEmpty()) {
            iv = likeAnimateAcquire.remove(0);
            ViewUtil.removeChild(iv);
            ViewGroup.LayoutParams lp = iv.getLayoutParams();
            if (lp != null && lp instanceof FrameLayout.LayoutParams) {
                layoutParams = (FrameLayout.LayoutParams) lp;
            }
        } else {
            iv = new LikeAnimateView(context);
        }
        if (layoutParams == null) {
            layoutParams = new FrameLayout.LayoutParams(LIKED_SIZE, LIKED_SIZE);
            iv.setLayoutParams(layoutParams);
        } else {
            layoutParams.width = LIKED_SIZE;
            layoutParams.height = LIKED_SIZE;
        }
        int index = (int) (Math.random() * DEGREES.length);
        iv.setRotation(DEGREES[index]);
        final boolean isRtl = ResourceUtils.isRtl(context);
        if (isFullScreen) {
            layoutParams.topMargin = pointY - LIKED_SIZE - ResourceUtils.dpToPX(8);
        } else {
            layoutParams.topMargin = pointY - LIKED_SIZE / 2;
        }
        layoutParams.leftMargin = isRtl ? (ResourceUtils.getScreenWidth() - pointX - LIKED_SIZE / 2)
                : (pointX - LIKED_SIZE / 2);
        if (isRtl) {
            layoutParams.setMarginStart(layoutParams.leftMargin);
        }
        return iv;
    }

    public void likedTouchAt(ViewGroup container, boolean isFullScreen, int pointX, int pointY) {
        if (Utils.isNull(container)) return;

        LikeAnimateView lav = acquire(container.getContext(), isFullScreen, pointX, pointY);
        container.addView(lav);
        lav.startLikeAnimate();
    }

    //-----------我是分割线-----------
    private class LikeAnimateView extends ImageView {

        private AnimatorSet animatorSet;

        public LikeAnimateView(Context context) {
            super(context);
            setImageResource(R.drawable.ic_video_like2);
        }

        public void startLikeAnimate() {
            setScaleX(1f);
            setScaleY(1f);
            setAlpha(1f);
            setTranslationY(0);
            //in
            AnimatorSet animatorSetIn = new AnimatorSet();
            animatorSetIn.setDuration(250);
            ObjectAnimator scaleXAnimIn = ObjectAnimator.ofFloat(this, SCALE_X, 2.2f, 1f);
            scaleXAnimIn.setInterpolator(new OvershootInterpolator());
            ObjectAnimator scaleYAnimIn = ObjectAnimator.ofFloat(this, SCALE_Y, 2.2f, 1f);
            scaleYAnimIn.setInterpolator(new OvershootInterpolator());
            ObjectAnimator alphaAnimIn = ObjectAnimator.ofFloat(this, ALPHA, 0.5f, 1f);
            animatorSetIn.play(scaleXAnimIn).with(scaleYAnimIn).with(alphaAnimIn);
            //out
            AnimatorSet animatorSetOut = new AnimatorSet();
            animatorSetOut.setDuration(300);
            ObjectAnimator scaleXAnimOut = ObjectAnimator.ofFloat(this, SCALE_X, 1f, 1.5f);
            scaleXAnimOut.setInterpolator(new LinearInterpolator());
            ObjectAnimator scaleYAnimOut = ObjectAnimator.ofFloat(this, SCALE_Y, 1f, 1.5f);
            scaleYAnimOut.setInterpolator(new LinearInterpolator());
            ObjectAnimator alphaAnimOut = ObjectAnimator.ofFloat(this, ALPHA, 1f, 0.2f);
            alphaAnimOut.setInterpolator(new LinearInterpolator());
            ObjectAnimator transAnimOut = ObjectAnimator.ofFloat(this, TRANSLATION_Y, 0f, -ResourceUtils.dp2PX(100));
            transAnimOut.setInterpolator(new LinearInterpolator());
            animatorSetOut.play(scaleXAnimOut).with(scaleYAnimOut).with(alphaAnimOut).with(transAnimOut);

            animatorSet = new AnimatorSet();
            animatorSet.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    try {
                        ViewUtil.cancelAnimator(animatorSet, true);
                        animatorSet = null;

                        ViewUtil.removeChild(LikeAnimateView.this);
                        likeAnimateAcquire.add(LikeAnimateView.this);
                    } catch (Throwable e) {
                        DebugLogger.e(e);
                    }
                }
            });
            animatorSet.play(animatorSetOut).after(500).after(animatorSetIn);
            animatorSet.start();
        }

    }

}

