package com.music.player.haige.di.module;

import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.ArtistModel;

import dagger.Module;
import dagger.Provides;

/**
 * ================================================
 * Created by huangcong on 2018/3/1.
 * ================================================
 */
@Module
public class ArtistModule {
    private MVContract.ArtistView view;

    public ArtistModule(MVContract.ArtistView view) {
        this.view = view;
    }

    @FragmentScope
    @Provides
    MVContract.ArtistView provideView() {
        return view;
    }

    @FragmentScope
    @Provides
    MVContract.ArtistModel provideModel(ArtistModel model) {
        return model;
    }

}
