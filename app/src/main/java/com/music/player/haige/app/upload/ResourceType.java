package com.music.player.haige.app.upload;

/**
 * Created by liumingkong on 14-5-6.
 */
public enum ResourceType {

    UNKNOWN(0),
    IMAGE(1),
    VIDEO(2),
    AUDIO(3);

    private final int code;

    ResourceType(int code) {
        this.code = code;
    }

    public int value() {
        return code;
    }

    public static ResourceType valueOf(final int code) {
        for (ResourceType c : ResourceType.values()) {
            if (code == c.code) return c;
        }
        return UNKNOWN;
    }
}
