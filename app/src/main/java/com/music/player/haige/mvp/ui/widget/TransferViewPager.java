package com.music.player.haige.mvp.ui.widget;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.hc.core.utils.CoreUtils;

/**
 * ================================================
 * Created by huangcong on 2018/3/22.
 * <p>
 * 区域拦截
 * ================================================
 */

public class TransferViewPager extends ViewPager {

    private boolean shouldTransferInterceptTouch = false;
    private int shouldTransferMinY, shouldTransferMaxY;

    public TransferViewPager(Context context) {
        this(context, null);
    }

    public TransferViewPager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    /**
     * 是否传递触摸事件，也就是是否不拦截触摸事件
     *
     * @param shouldTransferInterceptTouch
     */
    public TransferViewPager setShouldTransferInterceptTouch(boolean shouldTransferInterceptTouch) {
        this.shouldTransferInterceptTouch = shouldTransferInterceptTouch;
        return this;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        shouldTransferMaxY = getMeasuredHeight();
        shouldTransferMinY = shouldTransferMaxY - CoreUtils.dip2px(52);
    }

    @Override
    public boolean onInterceptHoverEvent(MotionEvent event) {
        //Timber.e("===>>> onInterceptHoverEvent ");
        return super.onInterceptHoverEvent(event);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        //Timber.e("===>>> dispatchTouchEvent ");
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        boolean intercept = super.onInterceptTouchEvent(event);
        if(shouldTransferInterceptTouch(event)){
            intercept = false;
        }
        //Timber.e("===>>> onInterceptTouchEvent " + "x:" + event.getX() + ", y:" + event.getY() + ", intercept:" + intercept);
        return intercept;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        //Timber.e("===>>> onTouchEvent " + "x:" + ev.getX() + ", y:" + ev.getY() + ", isInterceptTouch:" + isInterceptTouch);
        if (shouldTransferInterceptTouch(ev)) {
            return false;
        }
        return super.onTouchEvent(ev);
    }

    private boolean shouldTransferInterceptTouch(MotionEvent event) {
        if (shouldTransferInterceptTouch) {
            float y = event.getY();
            if (y > shouldTransferMinY && y < shouldTransferMaxY) {
                return true;
            }
        }
        return false;
    }

    @Override
    protected boolean canScroll(View v, boolean checkV, int dx, int x, int y) {
        boolean canScroll = super.canScroll(v, checkV, dx, x, y);
        //Timber.e("===>>> canScroll  " + canScroll);
        return canScroll;
    }
}
