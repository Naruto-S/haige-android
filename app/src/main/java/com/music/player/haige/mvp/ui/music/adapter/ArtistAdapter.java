package com.music.player.haige.mvp.ui.music.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.gson.Gson;
import com.hc.base.utils.ATEUtil;
import com.hc.base.widget.fastscroller.FastScrollRecyclerView;
import com.hc.core.http.imageloader.glide.GlideCore;
import com.music.player.haige.R;
import com.music.player.haige.app.Constants;
import com.music.player.haige.app.EventBusTags;
import com.music.player.haige.mvp.model.entity.music.Artist;
import com.music.player.haige.mvp.model.entity.music.ArtistArt;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.model.respository.dataloader.ArtistSongLoader;
import com.music.player.haige.mvp.model.utils.ColorUtil;
import com.music.player.haige.mvp.model.utils.CommonUtils;
import com.music.player.haige.mvp.model.utils.HaigeUtil;
import com.music.player.haige.mvp.model.utils.NavigationUtil;
import com.music.player.haige.mvp.model.utils.PreferencesUtility;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;
import com.music.player.haige.mvp.ui.widget.dialog.SystemDialog;

import org.simple.eventbus.EventBus;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class ArtistAdapter extends RecyclerView.Adapter<ArtistAdapter.ItemHolder> implements FastScrollRecyclerView.SectionedAdapter {

    private List<Artist> arraylist;
    private Activity mContext;
    private boolean isGrid;
    private String action;

    public ArtistAdapter(Activity context, List<Artist> arraylist) {
        this.arraylist = arraylist;
        this.mContext = context;
        this.isGrid = PreferencesUtility.getInstance(mContext).isArtistsInGrid();
    }

    public ArtistAdapter(Activity context, String action) {
        this.mContext = context;
        this.isGrid = PreferencesUtility.getInstance(mContext).isArtistsInGrid();
        this.action = action;
    }


    public void setArtistList(List<Artist> arrayList) {
        this.arraylist = arrayList;
        notifyItemRangeInserted(0, arrayList.size());
    }

    @Override
    public ItemHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        if (isGrid) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.app_item_list_grid_layout_item, viewGroup, false);
            return new ItemHolder(v);
        } else {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.app_item_music, viewGroup, false);
            return new ItemHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(final ItemHolder itemHolder, int i) {
        final Artist localItem = arraylist.get(i);

        itemHolder.name.setText(localItem.name);
        itemHolder.albumCount.setText(HaigeUtil.makeLabel(mContext, R.plurals.Nalbums, localItem.albumCount));
        itemHolder.songCount.setText(HaigeUtil.makeLabel(mContext, R.plurals.Nsongs, localItem.songCount));

        String artistArtJson = PreferencesUtility.getInstance(mContext).getArtistArt(localItem.id);
        if (TextUtils.isEmpty(artistArtJson)) {
//            getArtistInfo.execute(new GetArtistInfo.RequestValues(localItem.name))
//                    .getArtistInfo()
//                    .subscribeOn(Schedulers.io())
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .onErrorReturn(new Function<Throwable, ArtistInfo>() {
//                        @Override
//                        public ArtistInfo apply(Throwable throwable) throws Exception {
//                            Toast.makeText(itemHolder.itemView.getContext(), R.string.load_artist_fail, Toast.LENGTH_SHORT).show();
//                            return null;
//                        }
//                    })
//                    .subscribe(new Consumer<ArtistInfo>() {
//                        @Override
//                        public void accept(ArtistInfo artistInfo) throws Exception {
//                            if (artistInfo != null && artistInfo.mArtist != null && artistInfo.mArtist.mArtwork != null) {
//                                List<Artwork> artworks = artistInfo.mArtist.mArtwork;
//                                ArtistArt artistArt = new ArtistArt(artworks.get(0).mUrl, artworks.get(1).mUrl,
//                                        artworks.get(2).mUrl, artworks.get(3).mUrl);
//                                PreferencesUtility.getInstance(mContext).setArtistArt(localItem.id, new Gson().toJson(artistArt));
//                                loadArtistArt(artistArt, itemHolder);
//                            }
//                        }
//                    });

        } else {
            ArtistArt artistArt = new Gson().fromJson(artistArtJson, ArtistArt.class);
            loadArtistArt(artistArt, itemHolder);
        }

        if (CommonUtils.isLollipop())
            itemHolder.artistImage.setTransitionName("transition_artist_art" + i);

        setOnPopupMenuListener(itemHolder, i);

    }

    private void loadArtistArt(ArtistArt artistArt, final ItemHolder itemHolder) {
        if (isGrid) {
            GlideCore.with(mContext).asBitmap()
                    .load(artistArt.getExtralarge())
                    .placeholder(ATEUtil.getDefaultSingerDrawable(mContext))
                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                    .transform(new CenterCrop())
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onLoadFailed(@Nullable Drawable errorDrawable) {
                            super.onLoadFailed(errorDrawable);
                            itemHolder.artistImage.setImageDrawable(ATEUtil.getDefaultSingerDrawable(mContext));
                            itemHolder.name.setTextColor(ATEUtil.getThemeTextColorPrimary(mContext));
                            itemHolder.albumCount.setTextColor(ATEUtil.getThemeTextColorSecondly(mContext));
                            itemHolder.songCount.setTextColor(ATEUtil.getThemeTextColorSecondly(mContext));
                            itemHolder.popupMenu.setColorFilter(mContext.getResources().getColor(R.color.background_floating_material_dark));
                            itemHolder.footer.setBackgroundColor(ATEUtil.getThemeAlbumDefaultPaletteColor(mContext));
                        }

                        @Override
                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                            new Palette.Builder(resource).generate(new Palette.PaletteAsyncListener() {
                                @Override
                                public void onGenerated(Palette palette) {
                                    Palette.Swatch swatch = ColorUtil.getMostPopulousSwatch(palette);
                                    if (swatch != null) {
                                        int color = swatch.getRgb();
                                        itemHolder.footer.setBackgroundColor(color);

                                        int detailColor = swatch.getTitleTextColor();
                                        itemHolder.artistImage.setImageBitmap(resource);
                                        itemHolder.name.setTextColor(ColorUtil.getOpaqueColor(detailColor));
                                        itemHolder.albumCount.setTextColor(detailColor);
                                        itemHolder.songCount.setTextColor(detailColor);
                                        itemHolder.popupMenu.setColorFilter(detailColor);
                                    }
                                }
                            });
                        }
                    });
        } else {
            GlideCore.with(mContext)
                    .load(artistArt.getLarge())
                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                    .placeholder(ATEUtil.getDefaultSingerDrawable(mContext))
                    .error(ATEUtil.getDefaultSingerDrawable(mContext))
                    .into(itemHolder.artistImage);
        }
    }

    @Override
    public int getItemCount() {
        return (null != arraylist ? arraylist.size() : 0);
    }

    public void updateDataSet(List<Artist> arrayList) {
        this.arraylist = arrayList;
    }

    @NonNull
    @Override
    public String getSectionName(int position) {
        if (arraylist == null || arraylist.size() == 0)
            return "";
        return Character.toString(arraylist.get(position).name.charAt(0));
    }

    private void setOnPopupMenuListener(final ArtistAdapter.ItemHolder itemHolder, final int position) {
        itemHolder.popupMenu.setOnClickListener(v -> {

            final PopupMenu menu = new PopupMenu(mContext, v);
            int adapterPosition = itemHolder.getAdapterPosition();
            final Artist artist = arraylist.get(adapterPosition);
            menu.setOnMenuItemClickListener(item -> {
                switch (item.getItemId()) {
                    case R.id.popup_artist_addto_queue:
                        getSongListIdByArtist(arraylist.get(position).id)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(ids -> {
                                });
                        break;
                    case R.id.popup_artist_addto_playlist:
                        getSongListIdByArtist(arraylist.get(position).id)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(longs -> {

                                });
                        break;
                    case R.id.popup_artist_delete:
                        switch (action) {
                            case Constants.NAVIGATE_PLAYLIST_FAVORITE:
                                getSongListIdByArtist(artist.id)
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(ids -> {});
                                break;
                            case Constants.NAVIGATE_PLAYLIST_RECENTPLAY:
                                getSongListIdByArtist(artist.id)
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(ids -> {});
                                break;
                            default:
                                ArtistSongLoader.getSongsForArtist(mContext, arraylist.get(position).id)
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(songs -> {
                                            long[] ids = new long[songs.size()];
                                            int i = 0;
                                            for (Song song : songs) {
                                                ids[i] = song.songId;
                                                i++;
                                            }
                                            if (ids.length == 1) {
                                                new SystemDialog().create(((AppCompatActivity) mContext).getSupportFragmentManager())
                                                        .setContent(mContext.getString(R.string.app_delete) + " " + songs.get(0).musicName + " ?")
                                                        .setOk(ResourceUtils.resourceString(R.string.app_delete))
                                                        .setCancle(ResourceUtils.resourceString(R.string.app_cancel))
                                                        .setOnOkListener(() -> {
                                                            HaigeUtil.deleteTracks(mContext, ids);
                                                            arraylist.remove(position);
                                                            notifyDataSetChanged();
                                                            EventBus.getDefault().post(new EventBusTags.MediaUpdateEvent());
                                                        })
                                                        .setOnCancelListener(() -> {
                                                        })
                                                        .show();
                                            } else {
                                                String songCount = HaigeUtil.makeLabel(mContext,
                                                        R.plurals.Nsongs, arraylist.get(position).songCount);
                                                new SystemDialog().create(((AppCompatActivity) mContext).getSupportFragmentManager())
                                                        .setContent(mContext.getString(R.string.app_delete) + " " + songCount + " ?")
                                                        .setOk(ResourceUtils.resourceString(R.string.app_delete))
                                                        .setCancle(ResourceUtils.resourceString(R.string.app_cancel))
                                                        .setOnOkListener(() -> {
                                                            HaigeUtil.deleteTracks(mContext, ids);
                                                            arraylist.remove(position);
                                                            notifyDataSetChanged();
                                                            EventBus.getDefault().post(new EventBusTags.MediaUpdateEvent());
                                                        })
                                                        .setOnCancelListener(() -> {
                                                        })
                                                        .show();
                                            }
                                        });
                                break;
                        }
                        break;
                }
                return false;
            });
            menu.inflate(R.menu.popup_artist);
            menu.show();
        });
    }

    private Observable<long[]> getSongListIdByArtist(long id) {
        return ArtistSongLoader.getSongsForArtist(mContext, id)
                .map(new Function<List<Song>, long[]>() {
                    @Override
                    public long[] apply(List<Song> songs) throws Exception {
                        long[] ids = new long[songs.size()];
                        int i = 0;
                        for (Song song : songs) {
                            ids[i] = song.songId;
                            i++;
                        }
                        return ids;
                    }
                });
    }

    public class ItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView name;
        private TextView albumCount;
        private TextView songCount;
        private ImageView artistImage;
        private ImageView popupMenu;
        private View footer;

        public ItemHolder(View view) {
            super(view);
            this.name = (TextView) view.findViewById(R.id.text_item_title);
            this.albumCount = (TextView) view.findViewById(R.id.text_item_subtitle);
            this.songCount = (TextView) view.findViewById(R.id.text_item_subtitle_2);
            this.artistImage = (ImageView) view.findViewById(R.id.app_item_image);
            this.popupMenu = (ImageView) view.findViewById(R.id.popup_menu);
            this.footer = view.findViewById(R.id.footer);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            NavigationUtil.navigateToArtist(mContext, arraylist.get(getAdapterPosition()).id, arraylist.get(getAdapterPosition()).name,
                    new Pair<View, String>(artistImage, "transition_artist_art" + getAdapterPosition()));
        }

    }
}




