package com.music.player.haige.mvp.model.entity.user;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class UserBean implements Parcelable {

    public static final int USER_FOLLOW_STAUS_UNFOLLOW = 0; //互相不关注
    public static final int USER_FOLLOW_STAUS_FOLLOW_OTHER = 1; //我关注他
    public static final int USER_FOLLOW_STAUS_FOLLOW_ME = 2; //他关注我
    public static final int USER_FOLLOW_STAUS_MUTUAL = 3; //互相关注

    public static final int TYPE_OFFICIAL_USER = 0; //官方用户
    public static final int TYPE_GENERAL_USER = 1; //普通用户

    /**
     * user_id : QQ-4AF314EC950013DF29A47AC9D09D6F5C
     * nickname : 鸣人
     * avatar_url : http://qzapp.qlogo.cn/qzapp/1106809517/4AF314EC950013DF29A47AC9D09D6F5C/100
     * gender : 女
     * account_type : 2
     * rtime : 1525933798
     * token : 8a32fc3c-f74e-4085-8f52-4c51bc585827
     */

    @SerializedName("user_id")
    private String userId;
    @SerializedName("nickname")
    private String nickName;
    @SerializedName("avatar_url")
    private String avatarUrl;
    @SerializedName("gender")
    private String gender;
    @SerializedName("account_type")
    private int accountType;
    @SerializedName("rtime")
    private int rtime;
    @SerializedName("token")
    private String token;
    @SerializedName("haige_id")
    private long haigeId;
    @SerializedName("age")
    private String age;
    @SerializedName("year")
    private String year;
    @SerializedName("city")
    private String city;
    @SerializedName("province")
    private String province;
    @SerializedName("birthday")
    private String birthday;
    @SerializedName("desc")
    private String desc;
    @SerializedName("following")
    private int followStatus;
    @SerializedName("following_count")
    private int followingCount;
    @SerializedName("follower_count")
    private int followerCount;
    @SerializedName("liked_count")
    private int likedCount;
    @SerializedName("upload_count")
    private int uploadCount;

    private int type = 1;

    public UserBean() {

    }

    protected UserBean(Parcel in) {
        userId = in.readString();
        nickName = in.readString();
        avatarUrl = in.readString();
        gender = in.readString();
        accountType = in.readInt();
        rtime = in.readInt();
        token = in.readString();
        haigeId = in.readLong();
        age = in.readString();
        year = in.readString();
        city = in.readString();
        province = in.readString();
        birthday = in.readString();
        desc = in.readString();
        followStatus = in.readInt();
        followingCount = in.readInt();
        followerCount = in.readInt();
        likedCount = in.readInt();
        uploadCount = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userId);
        dest.writeString(nickName);
        dest.writeString(avatarUrl);
        dest.writeString(gender);
        dest.writeInt(accountType);
        dest.writeInt(rtime);
        dest.writeString(token);
        dest.writeLong(haigeId);
        dest.writeString(age);
        dest.writeString(year);
        dest.writeString(city);
        dest.writeString(province);
        dest.writeString(birthday);
        dest.writeString(desc);
        dest.writeInt(followStatus);
        dest.writeInt(followingCount);
        dest.writeInt(followerCount);
        dest.writeInt(likedCount);
        dest.writeInt(uploadCount);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UserBean> CREATOR = new Creator<UserBean>() {
        @Override
        public UserBean createFromParcel(Parcel in) {
            return new UserBean(in);
        }

        @Override
        public UserBean[] newArray(int size) {
            return new UserBean[size];
        }
    };

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAccountType() {
        return accountType;
    }

    public void setAccountType(int accountType) {
        this.accountType = accountType;
    }

    public int getRtime() {
        return rtime;
    }

    public void setRtime(int rtime) {
        this.rtime = rtime;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public long getHaigeId() {
        return haigeId;
    }

    public void setHaigeId(long haigeId) {
        this.haigeId = haigeId;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getFollowStatus() {
        return followStatus;
    }

    public void setFollowStatus(int followStatus) {
        this.followStatus = followStatus;
    }

    public int getFollowingCount() {
        return followingCount;
    }

    public void setFollowingCount(int followingCount) {
        this.followingCount = followingCount;
    }

    public int getFollowerCount() {
        return followerCount;
    }

    public void setFollowerCount(int followerCount) {
        this.followerCount = followerCount;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getLikedCount() {
        return likedCount;
    }

    public void setLikedCount(int likedCount) {
        this.likedCount = likedCount;
    }

    public int getUploadCount() {
        return uploadCount;
    }

    public void setUploadCount(int uploadCount) {
        this.uploadCount = uploadCount;
    }

    @Override
    public String toString() {
        return "UserBean{" +
                "userId='" + userId + '\'' +
                ", nickName='" + nickName + '\'' +
                ", avatarUrl='" + avatarUrl + '\'' +
                ", gender='" + gender + '\'' +
                ", accountType=" + accountType +
                ", rtime=" + rtime +
                ", token='" + token + '\'' +
                ", haigeId=" + haigeId +
                ", age='" + age + '\'' +
                ", year='" + year + '\'' +
                ", city='" + city + '\'' +
                ", province='" + province + '\'' +
                ", birthday='" + birthday + '\'' +
                ", desc='" + desc + '\'' +
                ", followStatus=" + followStatus +
                ", followingCount=" + followingCount +
                ", followerCount=" + followerCount +
                ", likedCount=" + likedCount +
                ", uploadCount=" + uploadCount +
                ", type=" + type +
                '}';
    }
}
