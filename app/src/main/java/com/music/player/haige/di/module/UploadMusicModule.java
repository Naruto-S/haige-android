package com.music.player.haige.di.module;

import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.UploadMusicModel;

import dagger.Module;
import dagger.Provides;

/**
 * ================================================
 * Created by huangcong on 2018/2/27.
 * ================================================
 */
@Module
public class UploadMusicModule {
    private MVContract.UploadMusicView view;

    public UploadMusicModule(MVContract.UploadMusicView view) {
        this.view = view;
    }

    @FragmentScope
    @Provides
    MVContract.UploadMusicView provideView() {
        return this.view;
    }

    @FragmentScope
    @Provides
    MVContract.UploadMusicModel provideModel(UploadMusicModel model) {
        return model;
    }

}
