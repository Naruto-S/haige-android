package com.music.player.haige.app.image.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;

import com.facebook.cache.disk.DiskCacheConfig;
import com.facebook.common.executors.UiThreadImmediateExecutorService;
import com.facebook.common.memory.MemoryTrimType;
import com.facebook.common.memory.MemoryTrimmable;
import com.facebook.common.memory.MemoryTrimmableRegistry;
import com.facebook.common.references.CloseableReference;
import com.facebook.common.util.ByteConstants;
import com.facebook.datasource.BaseDataSubscriber;
import com.facebook.datasource.DataSource;
import com.facebook.datasource.DataSubscriber;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.backends.okhttp3.OkHttpImagePipelineConfigFactory;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.core.ImagePipelineConfig;
import com.facebook.imagepipeline.image.CloseableAnimatedImage;
import com.facebook.imagepipeline.image.CloseableImage;
import com.facebook.imagepipeline.image.CloseableStaticBitmap;
import com.facebook.imagepipeline.listener.RequestListener;
import com.facebook.imagepipeline.listener.RequestLoggingListener;
import com.facebook.imagepipeline.memory.PoolConfig;
import com.facebook.imagepipeline.memory.PoolFactory;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.facebook.imagepipeline.request.Postprocessor;
import com.music.player.haige.app.HaigeApplication;
import com.music.player.haige.mvp.ui.utils.DebugLogger;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import okhttp3.OkHttpClient;

/**
 * 对Fresco能力的封装,单例
 * <p>
 * Created by ZaKi on 2016/9/2.
 */
public class ImageLoadController {

    private static Map<String, CloseableReference<CloseableImage>> mRefs = new HashMap<>();

    /**
     * 初始化Fresco库
     *
     * @param appContext
     */
    public static void initFresco(Context appContext) {

        //配置磁盘缓存路径
        DiskCacheConfig.Builder diskBuilder = DiskCacheConfig.newBuilder(appContext)
                .setMaxCacheSize(50 * ByteConstants.MB)//设备在磁盘充足情况下，磁盘缓存默认为40M
                .setMaxCacheSizeOnLowDiskSpace(10 * ByteConstants.MB)//设备在磁盘在低内存情况下，磁盘缓存默认为10M
                .setMaxCacheSizeOnVeryLowDiskSpace(2 * ByteConstants.MB)//设备在磁盘极低内存情况下，磁盘缓存为2M
                .setBaseDirectoryPath(FileStore.getFrescoMainImageFile(appContext));//磁盘缓存目录
        //小缓存路径
        DiskCacheConfig.Builder minDiskBuilder = DiskCacheConfig.newBuilder(appContext)
                .setMaxCacheSize(30 * ByteConstants.MB)//设备在磁盘充足情况下，磁盘缓存默认为40M
                .setMaxCacheSizeOnLowDiskSpace(10 * ByteConstants.MB)//设备在磁盘在低内存情况下，磁盘缓存默认为10M
                .setMaxCacheSizeOnVeryLowDiskSpace(2 * ByteConstants.MB)//设备在磁盘极低内存情况下，磁盘缓存为2M
                .setBaseDirectoryPath(FileStore.getFrescoMinImageFile(appContext));

        OkHttpClient mOkHttpClient = new OkHttpClient.Builder()
                .sslSocketFactory(SSLSocketFactoryUtils.createSSLSocketFactory(), SSLSocketFactoryUtils.createTrustAllManager())
                .hostnameVerifier((s, sslSession) -> true)
                .build();

        Set<RequestListener> listeners = new HashSet<>();
        listeners.add(new RequestLoggingListener());
        ImagePipelineConfig config = OkHttpImagePipelineConfigFactory
                .newBuilder(appContext, mOkHttpClient)
                .setDownsampleEnabled(true)//默认ImageOptions只支持jpg，设置此属性可支持png／jpg/webp
                .setPoolFactory(new PoolFactory(PoolConfig.newBuilder()
                        .setFlexByteArrayPoolParams(FlexByteArrayPoolParams.get())
                        .build()))
                .setMainDiskCacheConfig(diskBuilder.build())
                .setSmallImageDiskCacheConfig(minDiskBuilder.build())
                .setMemoryTrimmableRegistry(new MemoryTrimmableRegistry() {
                    @Override
                    public void registerMemoryTrimmable(MemoryTrimmable trimmable) {
                        //当应用处于后台并且处于低内存情况下，回收资源
                        trimmable.trim(MemoryTrimType.OnSystemLowMemoryWhileAppInBackground);
                    }

                    @Override
                    public void unregisterMemoryTrimmable(MemoryTrimmable trimmable) {
                        // Fresco已经实现了，这里不实现
                    }
                })
                .setRequestListeners(listeners)
                .build();
        //先保存Fresco可编程管道
        Fresco.initialize(appContext, config);
    }

    /**
     * 获取一个图片引用，这里格外注意一定要在不使用的时候关闭这个引用
     *
     * @param imageRequest
     * @param requestImageCallback
     */
    public static void getImage(final ImageRequest imageRequest, final RequestImageCallback requestImageCallback) {
        DataSource<CloseableReference<CloseableImage>> dataSource = Fresco.getImagePipeline().fetchDecodedImage(imageRequest, null);
        DataSubscriber<CloseableReference<CloseableImage>> dataSubscriber =
                new BaseDataSubscriber<CloseableReference<CloseableImage>>() {
                    @Override
                    protected void onNewResultImpl(DataSource<CloseableReference<CloseableImage>> dataSource) {
                        if (!dataSource.isFinished()) {
                            return;
                        }

                        CloseableReference<CloseableImage> mRef = null;
                        try {
                            mRef = dataSource.getResult();
                            if (mRef != null) {
                                final CloseableImage image = mRef.get();
                                if (image != null && image instanceof CloseableStaticBitmap) {
                                    CloseableStaticBitmap closeableStaticBitmap = (CloseableStaticBitmap) image;
                                    Bitmap bitmap = closeableStaticBitmap.getUnderlyingBitmap();
                                    if (bitmap != null) {
                                        try {
                                            //拷贝一个Bitmap给应用层，当前这个Bitmap会被回收掉
                                            Bitmap bmp = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), null, true);
                                            if (bmp != null) {
                                                bitmap = bmp;
                                            }
                                        } catch (Throwable throwable) {
                                            DebugLogger.e(throwable.toString());
                                        }

                                    }
                                    if (requestImageCallback != null) {
                                        requestImageCallback.onImageResult(bitmap, image.getWidth(), image.getHeight(), imageRequest.getSourceUri().toString());
                                    }
                                } else if (image != null && image instanceof CloseableAnimatedImage) {
                                    if (requestImageCallback != null) {
                                        requestImageCallback.onImageResult(null, image.getWidth(), image.getHeight(), imageRequest.getSourceUri().toString());
                                    }
                                }
                            } else {
                                if (requestImageCallback != null) {
                                    requestImageCallback.onImageResult(null, 0, 0, imageRequest.getSourceUri().toString());
                                }
                            }
                        } finally {
                            dataSource.close();
                            CloseableReference.closeSafely(mRef);
                        }
                    }

                    @Override
                    protected void onFailureImpl(DataSource<CloseableReference<CloseableImage>> dataSource) {
                        Throwable t = dataSource.getFailureCause();
                        if (requestImageCallback != null) {
                            requestImageCallback.onImageFail(imageRequest.getSourceUri().toString());
                        }
                    }
                };
        dataSource.subscribe(dataSubscriber, UiThreadImmediateExecutorService.getInstance());
    }


    /**
     * 获取不关闭的图片引用
     */
    public static void getImage(String uri, RequestImageCallback requestImageCallback) {
        getImage(getImageRequestBuilder(uri, ImageRequest.RequestLevel.FULL_FETCH, requestImageCallback), requestImageCallback);
    }


    /**
     * 从内存缓存中获取一张图片
     *
     * @param uri
     * @param requestImageCallback
     * @return
     */
    public static void getImageFromCache(String uri, RequestImageCallback requestImageCallback) {
        getImage(getImageRequestBuilder(uri, ImageRequest.RequestLevel.ENCODED_MEMORY_CACHE, requestImageCallback), requestImageCallback);
    }

    /**
     * 从磁盘缓存中获取一张图片
     *
     * @param uri
     * @param requestImageCallback
     * @return
     */
    public static void getImageFromDisk(String uri, RequestImageCallback requestImageCallback) {
        getImage(getImageRequestBuilder(uri, ImageRequest.RequestLevel.DISK_CACHE, requestImageCallback), requestImageCallback);
    }

    /**
     * 从完全缓存中获取一张图片
     *
     * @param uri
     * @param requestImageCallback
     * @return
     */
    public static void getImageFromFull(String uri, RequestImageCallback requestImageCallback) {
        getImage(getImageRequestBuilder(uri, ImageRequest.RequestLevel.FULL_FETCH, requestImageCallback), requestImageCallback);
    }

    private static ImageRequest getImageRequestBuilder(String uri, ImageRequest.RequestLevel requestLevel, RequestImageCallback requestImageCallback) {
        ImageRequestBuilder requestBuilder = ImageRequestBuilder.newBuilderWithSource(Uri.parse(uri)).setAutoRotateEnabled(true);
        requestBuilder.setLowestPermittedRequestLevel(requestLevel);
        if (requestImageCallback != null && requestImageCallback.obtainPostprocessor() != null) {
            requestBuilder.setPostprocessor(requestImageCallback.obtainPostprocessor());
        }
        return requestBuilder.build();
    }


    /**
     * 从缓存中取一个图片引用
     *
     * @param imageRequest
     * @return
     */
    public static CloseableReference<CloseableImage> getImageFromCache(ImageRequest imageRequest) {
        DataSource<CloseableReference<CloseableImage>> dataSource =
                Fresco.getImagePipeline().fetchImageFromBitmapCache(imageRequest, null);
        CloseableReference<CloseableImage> ref = null;
        try {
            CloseableReference<CloseableImage> imageReference = dataSource.getResult();
            if (imageReference != null) {
                try {
                    ref = imageReference.clone();
                } finally {
                    CloseableReference.closeSafely(imageReference);
                }
            }
        } finally {
            dataSource.close();
        }
        return ref;
    }

    /**
     * 预加载图片到磁盘,特定场景用得上
     *
     * @param imageRequest
     */
    public static DataSource<Void> preLoadImageToDisk(ImageRequest imageRequest) {
        return Fresco.getImagePipeline().prefetchToDiskCache(imageRequest, null);
    }

    /**
     * 预加载图片到内存缓存，特定场景用得上
     *
     * @param imageRequest
     */
    public static DataSource<Void> preLoadImageToCache(ImageRequest imageRequest) {
        return Fresco.getImagePipeline().prefetchToBitmapCache(imageRequest, null);
    }

    /**
     * 取消图片加载
     */
    public static void cancelLoadImage(DataSource<Void> dataSource) {
        try {
            if (dataSource != null) {
                dataSource.close();
            }
        } catch (Throwable throwable) {

        }
    }


    /**
     * 是否在内存缓存，同步操作
     *
     * @param uri
     * @return
     */
    public static boolean isInBitmapCache(String uri) {
        boolean inMemoryCache = Fresco.getImagePipeline().isInBitmapMemoryCache(Uri.parse(uri));

        return inMemoryCache;
    }

    /**
     * 是否在磁盘缓存，异步步操作
     *
     * @param uri
     */
    public static boolean isInDiskCache(String uri) {
        return Fresco.getImagePipeline().isInDiskCache(Uri.parse(uri)).getResult();
    }

    /**
     * 清除一个内存缓存的bitmap
     *
     * @param uri
     */
    public static void clearBitmapInCache(String uri) {
        Fresco.getImagePipeline().evictFromMemoryCache(Uri.parse(uri));
    }

    /**
     * 清除一个磁盘缓存的bitmap
     *
     * @param uri
     */
    public static void clearBitmapInDisk(String uri) {
        Fresco.getImagePipeline().evictFromDiskCache(Uri.parse(uri));
    }

    /**
     * 获取Drawable
     *
     * @param resId
     */
    public static Drawable getDrawable(int resId) {
        if (resId <= 0) {
            return null;
        }
        return HaigeApplication.getInstance().getResources().getDrawable(resId);
    }

    /**
     * 清除所有缓存
     */
    public static void clearCaches() {
        Fresco.getImagePipeline().clearCaches();
    }

    public static void clearMemoryCache() {
        Fresco.getImagePipeline().clearMemoryCaches();
    }

    public static void clearDiskCache() {
        Fresco.getImagePipeline().clearDiskCaches();
    }

    public static ImageRequestBuilder buildImageRequest(String url) {
        return buildImageRequest(url, 0, 0);
    }

    public static ImageRequestBuilder buildImageRequest(String url, int targetWidth, int targetHeigh) {
        ImageRequestBuilder requestBuilder = ImageRequestBuilder.newBuilderWithSource(Uri.parse(url)).setAutoRotateEnabled(true);
        if (targetWidth > 0 && targetHeigh > 0) {
            requestBuilder.setResizeOptions(new ResizeOptions(targetWidth, targetHeigh));
        }
        return requestBuilder;
    }

    /**
     * 请求图片回调
     */
    public interface RequestImageCallback {
        void onImageResult(Bitmap bitmap, int width, int height, String uri);

        void onImageFail(String uri);

        Postprocessor obtainPostprocessor();
    }

    public static class SimpleRequestImageCallback implements RequestImageCallback {

        @Override
        public void onImageResult(Bitmap bitmap, int width, int height, String uri) {

        }

        @Override
        public void onImageFail(String uri) {

        }

        @Override
        public Postprocessor obtainPostprocessor() {
            return null;
        }
    }

}
