package com.music.player.haige.app;

import android.app.Activity;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.hc.base.utils.TranslucentStatusBarUtils;
import com.hc.base.utils.statusbar.StatusBarCompat;
import com.music.player.haige.MediaAidlInterface;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.ui.music.service.MusicService;
import com.music.player.haige.mvp.ui.music.service.MusicServiceConnection;
import com.music.player.haige.mvp.ui.utils.DebugLogger;
import com.umeng.analytics.MobclickAgent;

import org.simple.eventbus.EventBus;

import java.lang.ref.WeakReference;

import timber.log.Timber;

/**
 * ================================================
 * 展示 {@link Application.ActivityLifecycleCallbacks} 的用法
 * <p>
 * ================================================
 */
public class ActivityLifecycleCallbacksImpl implements Application.ActivityLifecycleCallbacks, ServiceConnection {

    private MusicServiceConnection.ServiceToken mToken;
    private PlaybackStatus mPlaybackStatus; //receiver 接受播放状态变化等

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        Timber.w(activity + " - onActivityCreated");
        initSystemBarTint(activity);
        if (activity instanceof BindMusicService) {
            mToken = MusicServiceConnection.bindToService(activity, this);
            registerReceiver(activity);
        }
    }

    @Override
    public void onActivityStarted(Activity activity) {
        Timber.w(activity + " - onActivityStarted");
    }

    @Override
    public void onActivityResumed(Activity activity) {
        Timber.w(activity + " - onActivityResumed");
        MobclickAgent.onResume(activity);
    }

    @Override
    public void onActivityPaused(Activity activity) {
        Timber.w(activity + " - onActivityPaused");
        MobclickAgent.onPause(activity);
    }

    @Override
    public void onActivityStopped(Activity activity) {
        Timber.w(activity + " - onActivityStopped");
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
        Timber.w(activity + " - onActivitySaveInstanceState");
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        Timber.w(activity + " - onActivityDestroyed");
        if (activity instanceof BindMusicService) {
            if (mToken != null) {
                MusicServiceConnection.unbindFromService(mToken);
                mToken = null;
            }
            try {
                activity.unregisterReceiver(mPlaybackStatus);
            } catch (Exception e) {
            }
        }
    }

    @Override
    public void onServiceConnected(final ComponentName name, final IBinder service) {
        Timber.w(this + " - onServiceConnected");
        MusicServiceConnection.mService = MediaAidlInterface.Stub.asInterface(service);
    }

    @Override
    public void onServiceDisconnected(final ComponentName name) {
        Timber.w(this + " - onServiceDisconnected");
        MusicServiceConnection.mService = null;
    }

    private void registerReceiver(Activity activity) {
        mPlaybackStatus = new PlaybackStatus(activity);

        IntentFilter f = new IntentFilter();
        f.addAction(MusicService.PLAYSTATE_CHANGED);
        f.addAction(MusicService.META_CHANGED);
        f.addAction(MusicService.QUEUE_CHANGED);
        f.addAction(MusicService.REPEATMODE_CHANGED);
        f.addAction(MusicService.SHUFFLEMODE_CHANGED);
        f.addAction(Constants.MUSIC_COUNT_CHANGED);
        f.addAction(MusicService.TRACK_PREPARED);
        f.addAction(MusicService.BUFFER_UP);
        f.addAction(Constants.EMPTY_LIST);
        f.addAction(MusicService.MUSIC_CHANGED);
        f.addAction(MusicService.LRC_UPDATED);
        f.addAction(Constants.PLAYLIST_COUNT_CHANGED);
        f.addAction(MusicService.MUSIC_LOADING);
        activity.registerReceiver(mPlaybackStatus, new IntentFilter(f));
    }

    private final static class PlaybackStatus extends BroadcastReceiver {

        private final WeakReference<Activity> mReference;

        public PlaybackStatus(final Activity activity) {
            mReference = new WeakReference<>(activity);
        }

        @Override
        public void onReceive(final Context context, final Intent intent) {
            String action = intent.getAction();
            if (TextUtils.isEmpty(action)) {
                return;
            }
            DebugLogger.e("MusicReceive == ", action);
            Activity activity = mReference.get();
            switch (action) {
                case MusicService.META_CHANGED:
                    long songId = intent.getLongExtra(Song.KEY_MUSIC_ID, -1);
                    String songName = intent.getStringExtra(Song.KEY_MUSIC_NAME);
                    String artistName = intent.getStringExtra(Song.KEY_ARTIST_NAME);
                    long albumId = intent.getLongExtra(Song.KEY_ALBUM_ID, -1);
                    boolean isLocal = intent.getBooleanExtra(Song.KEY_IS_LOCAL, true);
                    String songCover = intent.getStringExtra(Song.KEY_MUSIC_COVER);
                    int likeCount = intent.getIntExtra(Song.KEY_LIKE, 0);
                    int shareCount = intent.getIntExtra(Song.KEY_SHARE, 0);
                    int downloadCount = intent.getIntExtra(Song.KEY_DOWNLOAD, 0);
                    String songUrl = intent.getStringExtra(Song.KEY_MUSIC_URL);
                    EventBus.getDefault().post(new EventBusTags.MetaChangedEvent(
                            songId,
                            songName,
                            artistName,
                            albumId,
                            isLocal,
                            songCover,
                            likeCount,
                            shareCount,
                            downloadCount,
                            songUrl));
                    break;
                case MusicService.PLAYSTATE_CHANGED:

                    break;
                case MusicService.TRACK_PREPARED:

                    break;
                case MusicService.BUFFER_UP:

                    break;
                case MusicService.MUSIC_LOADING:
                    EventBusTags.MusicIsLoadingEvent isLoadingEvent = new EventBusTags.MusicIsLoadingEvent();
                    isLoadingEvent.setLoading(intent.getBooleanExtra(MusicService.EXTRA_LOADING, false));
                    EventBus.getDefault().post(isLoadingEvent);
                    break;
                case MusicService.REFRESH:

                    break;
                case Constants.MUSIC_COUNT_CHANGED:

                    break;
                case Constants.PLAYLIST_COUNT_CHANGED:

                    break;
                case MusicService.QUEUE_CHANGED:

                    break;
                case MusicService.REPEATMODE_CHANGED:
                case MusicService.SHUFFLEMODE_CHANGED:
                    EventBus.getDefault().post(new EventBusTags.RepeatModeChangedEvent());
                    break;
                case MusicService.TRACK_ERROR:
                    if (activity != null) {
//                        final String errorMsg = context.getString(R.string.app_exit,
//                                intent.getStringExtra(MusicService.TrackErrorExtra.TRACK_NAME));
//                        Toast.makeText(activity, errorMsg, Toast.LENGTH_SHORT).show();
                    }
                    break;
                case MusicService.MUSIC_CHANGED:

                    break;
                case MusicService.LRC_UPDATED:

                    break;
            }
        }
    }

    public interface BindMusicService {

    }

    /**
     * 设置全屏以及透明状态栏
     */
    protected void initSystemBarTint(Activity activity) {
        if (!(activity instanceof SystemUIConfig)) {
            return;
        }
        SystemUIConfig config = (SystemUIConfig) activity;
        Window window = activity.getWindow();
        // 设置全屏
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (config.fullScreen()) {
                TranslucentStatusBarUtils.setFullScreen(window);
            } else {
                if (config.translucentStatusBar()) {
                    window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                } else {
                    window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                }
                View decorView = window.getDecorView();
                int visibility = decorView.getVisibility()
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
                decorView.setSystemUiVisibility(visibility);
            }
        }

        try {
            StatusBarCompat.setStatusBarColor(activity.getWindow(), config.setStatusBarColor(), config.lightStatusBar());
            StatusBarCompat.setFitsSystemWindows(activity.getWindow(), config.fitSystemWindows());
        } catch (Exception e) {

        }
    }
}
