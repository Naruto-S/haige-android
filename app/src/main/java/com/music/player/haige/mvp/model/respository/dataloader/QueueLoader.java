package com.music.player.haige.mvp.model.respository.dataloader;

import android.content.Context;
import android.database.Cursor;

import com.music.player.haige.mvp.model.entity.music.Song;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;


public class QueueLoader {

    public static Observable<List<Song>> getQueueSongs(final Context context) {
        return Observable.create(new ObservableOnSubscribe<List<Song>>() {

            @Override
            public void subscribe(ObservableEmitter<List<Song>> emitter) throws Exception {
                final List<Song> mSongList = new ArrayList<>();
                Cursor mCursor = new NowPlayingCursor(context);

                if (mCursor.moveToFirst()) {
                    do {

                        final long id = mCursor.getLong(0);

                        final String songName = mCursor.getString(1);

                        final String artist = mCursor.getString(2);

                        final long albumId = mCursor.getLong(3);

                        final String album = mCursor.getString(4);

                        final int duration = mCursor.getInt(5);

                        final long artistid = mCursor.getInt(6);

                        final int tracknumber = mCursor.getInt(7);

                        final boolean isLocal = true;

                        final Song song = new Song(
                                id,
                                albumId,
                                artistid,
                                songName,
                                artist,
                                album,
                                duration,
                                tracknumber,
                                isLocal);

                        mSongList.add(song);
                    } while (mCursor.moveToNext());
                }
                emitter.onNext(mSongList);
                emitter.onComplete();
                mCursor.close();
                mCursor = null;
            }
        });
    }
}
