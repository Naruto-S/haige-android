package com.music.player.haige.mvp.ui.upload;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.hc.core.di.component.AppComponent;
import com.hc.core.utils.CoreUtils;
import com.music.player.haige.R;
import com.music.player.haige.app.Constants;
import com.music.player.haige.app.EventBusTags;
import com.music.player.haige.app.SystemUIConfig;
import com.music.player.haige.app.base.AppBaseActivity;
import com.music.player.haige.app.base.pref.DevicePrefHelper;
import com.music.player.haige.app.utils.launcher.ActivityLauncherStart;
import com.music.player.haige.app.web.WebViewActivity;
import com.music.player.haige.di.component.DaggerSongsComponent;
import com.music.player.haige.di.module.SongsModule;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.Api;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.presenter.SongsPresenter;
import com.music.player.haige.mvp.ui.upload.adapter.UploadListAdapter;
import com.music.player.haige.mvp.ui.upload.dialog.UploadPrivacyDialog;
import com.music.player.haige.mvp.ui.utils.Utils;
import com.music.player.haige.mvp.ui.widget.StatusLayout;

import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

import static com.music.player.haige.app.Constants.URL_PRIVACY_UPLOAD;

public class UploadListActivity extends AppBaseActivity<SongsPresenter> implements MVContract.SongsView,
        SystemUIConfig {

    @BindView(R.id.app_layout_sl)
    StatusLayout mStatusLayout;

    @BindView(R.id.app_layout_rv)
    RecyclerView mSonsRecyclerView;

    @BindView(R.id.app_layout_progress)
    ProgressBar mLoadingView;

    private UploadListAdapter mAdapter;

    @Override
    public void setupActivityComponent(AppComponent appComponent) {
        DaggerSongsComponent
                .builder()
                .appComponent(appComponent)
                .songsModule(new SongsModule(this))
                .build()
                .inject(this);
    }

    @Override
    public int initView(Bundle savedInstanceState) {
        return R.layout.activity_upload_list;
    }

    @Override
    public void initData(Bundle savedInstanceState) {
        mStatusLayout.setOnStatusClickListener(new StatusLayout.OnStatusClickListener() {
            @Override
            public void onEmptyClick() {
                EventBus.getDefault().post(EventBusTags.UI.GO_TO_RECOMMEND);
                finish();
            }

            @Override
            public void onReloadClick() {

            }
        });

        mAdapter = new UploadListAdapter((AppCompatActivity) getActivity());
        mAdapter.setOnItemClickListener((view, song) -> {
            if (!DevicePrefHelper.isAgreeUploadPrivacy()) {
                UploadPrivacyDialog.create(getSupportFragmentManager()).show();
            } else {
                ActivityLauncherStart.startUpload(this, song);
            }
        });
        mSonsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mSonsRecyclerView.setAdapter(mAdapter);

        mPresenter.loadSongs(Constants.NAVIGATE_ALLSONG);

    }

    @Override
    public void showLoading() {
        super.showLoading();
        if (Utils.isNotNull(mLoadingView)) {
            mLoadingView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
        if (Utils.isNotNull(mLoadingView)) {
            mLoadingView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onSuccess(Api.Action action, Object o) {
        super.onSuccess(action, o);
        if (action == Api.Action.MUSIC_LIST) {
            mStatusLayout.showStatusView(StatusLayout.STATUS.OK);
            mSonsRecyclerView.setVisibility(View.VISIBLE);
            mAdapter.setSongList((List<Song>) o);
        }
    }

    @Override
    public void onError(Api.Action action, String code, String message) {
        super.onError(action, code, message);
        if (action == Api.Action.MUSIC_LIST) {
            mStatusLayout.showStatusView(StatusLayout.STATUS.EMPTY);
            mSonsRecyclerView.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void showMessage(String message) {
        super.showMessage(message);
        CoreUtils.makeText(this, message);
    }

    @Override
    public boolean translucentStatusBar() {
        return false;
    }

    @Override
    public int setStatusBarColor() {
        return getResources().getColor(R.color.color_00000000);
    }

    @Override
    public boolean lightStatusBar() {
        return false;
    }

    @Override
    public boolean fitSystemWindows() {
        return true;
    }

    @Override
    public boolean fullScreen() {
        return false;
    }

    @Subscriber
    public void onSubscriber(EventBusTags.MetaChangedEvent event) {
        mAdapter.notifyDataSetChanged();
    }

    @OnClick({R.id.app_fragment_back_iv, R.id.iv_upload_tips})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.app_fragment_back_iv:
                finish();
                break;
            case R.id.iv_upload_tips:
                WebViewActivity.start(this, URL_PRIVACY_UPLOAD);
                break;
        }
    }
}
