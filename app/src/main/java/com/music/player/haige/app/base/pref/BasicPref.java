package com.music.player.haige.app.base.pref;

import android.content.Context;
import android.content.SharedPreferences;

import com.music.player.haige.app.HaigeApplication;
import com.music.player.haige.mvp.ui.utils.Utils;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by liumingkong on 2017/6/13.
 */

public class BasicPref {

    private static final String SEP = "-";

    protected static String genKey(String prefix, String key) {
        return prefix + SEP + key;
    }

    private static SharedPreferences getPreference(String prefTag) {
        return HaigeApplication.getInstance().getSharedPreferences(prefTag, Context.MODE_PRIVATE);
    }

    protected static void clear(String prefTag) {
        SharedPreferences.Editor editor = getPreference(prefTag).edit();
        editor.clear();
        editor.apply();
    }

    protected static void remove(String prefTag, String key) {
        SharedPreferences.Editor editor = getPreference(prefTag).edit();
        editor.remove(key);
        editor.apply();
    }

    protected static String getString(String prefTag, String tag, String defaultValue) {
        return getPreference(prefTag).getString(tag, defaultValue);
    }

    protected static void saveString(String prefTag, String tag, String value) {
        if (Utils.isNull(value)) {
            value = "";
        }
        SharedPreferences.Editor editor = getPreference(prefTag).edit();
        editor.putString(tag, value);
        editor.apply();
    }

    protected static int getInt(String prefTag, String tag, int defaultValue) {
        return getPreference(prefTag).getInt(tag, defaultValue);
    }

    protected static void saveInt(String prefTag, String tag, int value) {
        SharedPreferences.Editor editor = getPreference(prefTag).edit();
        editor.putInt(tag, value);
        editor.apply();
    }

    protected static long getLong(String prefTag, String tag, long defaultValue) {
        return getPreference(prefTag).getLong(tag, defaultValue);
    }

    protected static void saveLong(String prefTag, String tag, long value) {
        SharedPreferences.Editor editor = getPreference(prefTag).edit();
        editor.putLong(tag, value);
        editor.apply();
    }

    protected static boolean getBoolean(String prefTag, String tag, boolean defaultValue) {
        return getPreference(prefTag).getBoolean(tag, defaultValue);
    }

    protected static void saveBoolean(String prefTag, String tag, boolean value) {
        SharedPreferences.Editor editor = getPreference(prefTag).edit();
        editor.putBoolean(tag, value);
        editor.apply();
    }

    protected static Set<String> getStringSet(String prefTag, String tag) {
        return getPreference(prefTag).getStringSet(tag, new HashSet<String>());
    }

    protected static void saveStringSet(String prefTag, String tag, Set<String> value) {
        SharedPreferences.Editor editor = getPreference(prefTag).edit();
        editor.putStringSet(tag, value);
        editor.apply();
    }
}
