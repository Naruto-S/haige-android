package com.music.player.haige.mvp.ui.main.activity;

import android.graphics.Point;
import android.graphics.drawable.Animatable;
import android.os.Bundle;
import android.support.design.widget.CheckableImageButton;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.hc.core.di.component.AppComponent;
import com.hc.core.utils.CoreUtils;
import com.music.player.haige.R;
import com.music.player.haige.app.EventBusTags;
import com.music.player.haige.app.SystemUIConfig;
import com.music.player.haige.app.base.AppBaseActivity;
import com.music.player.haige.app.base.pref.UserPrefHelper;
import com.music.player.haige.app.image.loader.AvatarLoader;
import com.music.player.haige.app.image.widget.HaigeImageView;
import com.music.player.haige.app.utils.launcher.ActivityLauncherStart;
import com.music.player.haige.app.utils.statistics.CommonStatistics;
import com.music.player.haige.app.utils.statistics.StatisticsConstant;
import com.music.player.haige.di.component.DaggerNowPlayComponent;
import com.music.player.haige.di.module.NowPlayModule;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.Api;
import com.music.player.haige.mvp.model.entity.action.CommentLikeRequest;
import com.music.player.haige.mvp.model.entity.comment.CommentBean;
import com.music.player.haige.mvp.model.entity.comment.CommentRequest;
import com.music.player.haige.mvp.model.entity.comment.CommentResponse;
import com.music.player.haige.mvp.model.entity.comment.SendCommentResponse;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.model.entity.user.UserBean;
import com.music.player.haige.mvp.model.utils.CommonUtils;
import com.music.player.haige.mvp.model.utils.LyricUtil;
import com.music.player.haige.mvp.presenter.NowPlayPresenter;
import com.music.player.haige.mvp.ui.comment.CommentBottomSheetDialog;
import com.music.player.haige.mvp.ui.comment.adapter.CommentAdapter;
import com.music.player.haige.mvp.ui.login.LoginDialogFragment;
import com.music.player.haige.mvp.ui.main.utils.MainViewUtils;
import com.music.player.haige.mvp.ui.main.utils.MusicLyricManager;
import com.music.player.haige.mvp.ui.main.utils.VideoLikedHelper;
import com.music.player.haige.mvp.ui.main.widget.PlayMorePopWindow;
import com.music.player.haige.mvp.ui.main.widget.barrage.BarrageView;
import com.music.player.haige.mvp.ui.main.widget.likebutton.LikeButton;
import com.music.player.haige.mvp.ui.main.widget.likebutton.OnLikeListener;
import com.music.player.haige.mvp.ui.music.service.MusicDataManager;
import com.music.player.haige.mvp.ui.music.service.MusicServiceConnection;
import com.music.player.haige.mvp.ui.share.DownloadShareDialog;
import com.music.player.haige.mvp.ui.share.MusicShareDialog;
import com.music.player.haige.mvp.ui.utils.DebugLogger;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;
import com.music.player.haige.mvp.ui.utils.ShareUtils;
import com.music.player.haige.mvp.ui.utils.Utils;
import com.music.player.haige.mvp.ui.utils.ViewVisibleUtils;
import com.music.player.haige.mvp.ui.widget.LyricView;
import com.music.player.haige.mvp.ui.widget.NoDoubleClickListener;
import com.music.player.haige.mvp.ui.widget.emoji.EditCommentView;
import com.music.player.haige.mvp.ui.widget.loadingdrawable.render.LoadingDrawable;
import com.music.player.haige.mvp.ui.widget.loadingdrawable.render.circle.rotate.MaterialLoadingRenderer2;

import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

import static com.music.player.haige.app.Constants.DEFAULT_REQUEST_COMMENT_SIZE;
import static com.music.player.haige.app.constants.IntentConstants.EXTRA_CURRENT_MUSIC;
import static com.music.player.haige.mvp.model.entity.user.UserBean.TYPE_OFFICIAL_USER;

/**
 * Created by Naruto on 2018/5/16.
 */

public class MusicCompleteActivity extends AppBaseActivity<NowPlayPresenter> implements MVContract.NowPlayView,
        View.OnClickListener, View.OnTouchListener, LyricView.OnPlayerClickListener, SystemUIConfig,
        PlayMorePopWindow.OnPlayMoreClickListener, EditCommentView.OnEditCommentListener, OnLikeListener {

    private static final int MAX_BARRAGE_TEXT_COUNT = 20;

    @BindView(R.id.app_layout_music_operate_more_iv)
    ImageView mOperateMoreIv;

    @BindView(R.id.iv_play_download)
    ImageView nPlayDownloadIv;

    @BindView(R.id.iv_play_random)
    ImageView mTopPlayModeIv;

    @BindView(R.id.view_group_like)
    ViewGroup mLikeViewGroup;

    @BindView(R.id.hiv_music_webp_bg)
    HaigeImageView mMusicWebpBgSdv;

    @BindView(R.id.app_layout_music_info_duration_cur_tv)
    TextView mDurationCurTv;

    @BindView(R.id.app_layout_music_info_duration_sb)
    SeekBar mDurationSeekBar;

    @BindView(R.id.app_layout_music_info_duration_tv)
    TextView mDurationTv;

    @BindView(R.id.app_layout_music_operate_cover_iv)
    HaigeImageView mOperateCoverIv;

    @BindView(R.id.app_layout_music_other_info_tag_tv)
    TextView mMusicTagTv;

    @BindView(R.id.app_layout_music_other_info_artist_tv)
    TextView mArtistNameTv;

    @BindView(R.id.app_layout_music_other_info_music_name_tv)
    TextView mMusicNameTv;

    @BindView(R.id.app_layout_music_operate_like_iv)
    LikeButton mLikeCountIv;

    @BindView(R.id.app_layout_music_operate_like_count_tv)
    TextView mLikeCountTv;

    @BindView(R.id.app_layout_music_operate_share_iv)
    CheckableImageButton mShareCountIv;

    @BindView(R.id.iv_play_last)
    ImageView mInfoPreIv;

    @BindView(R.id.iv_play_start)
    ImageView mPlayPauseView;

    @BindView(R.id.iv_play_next)
    ImageView mInfoNextIv;

    @BindView(R.id.app_layout_music_operate_danmu_iv)
    CheckableImageButton mCommentIv;

    @BindView(R.id.tv_music_lyric)
    TextView mMusicLyricTv;

    @BindView(R.id.app_layout_music_operate_danmu_count_tv)
    TextView mCommentCountTv;

    @BindView(R.id.danmakuHolder)
    BarrageView mBarrageView;

    private Animatable mAnimatable;

    private long mCurrentSongId = 0;
    private long mLastCommentPostAt = 0;
    private boolean isStartTrackingTouch = false;

    private ArrayList<CommentBean> mCommentList = new ArrayList<>();
    private HashMap<Long, CommentBean> mCommentMap = new HashMap<>();
    private PlayMorePopWindow mPlayMorePopWindow;
    private CommentBottomSheetDialog mCommentSheetDialog;
    private UserBean mUserBean;
    private Point likeFirstPoint;
    private GestureDetector mGestureDetector;
    private VideoLikedHelper videoLikedHelper = new VideoLikedHelper();
    private PlayMode mPlayMode = PlayMode.SHUFFLE;
    private Song mSong;

    public static MusicCompleteActivity newInstance() {
        MusicCompleteActivity fragment = new MusicCompleteActivity();
        return fragment;
    }

    @Override
    public void setupActivityComponent(AppComponent appComponent) {
        DaggerNowPlayComponent
                .builder()
                .appComponent(appComponent)
                .nowPlayModule(new NowPlayModule(this))
                .build()
                .inject(this);
    }

    @Override
    public int initView(Bundle savedInstanceState) {
        return R.layout.activity_music_complete;
    }

    @Override
    public void initData(Bundle bundle) {

        if (Utils.isNotNull(getIntent())) {
            mSong = getIntent().getParcelableExtra(EXTRA_CURRENT_MUSIC);
        }

        initGestureDetector();

        mCommentSheetDialog = new CommentBottomSheetDialog(getActivity());
        mCommentSheetDialog.setCommentActionListener(new CommentAdapter.onCommentActionListener() {
            @Override
            public void onDelete(CommentRequest request) {
                mPresenter.deleteComment(request);
            }

            @Override
            public void onLike(CommentLikeRequest request) {
                mPresenter.commentLike(request);
            }

            @Override
            public void onClickAllSub(String commentId, int position) {
                mPresenter.getCommentSub(commentId, 1, 100, position);
            }
        });

        if (Utils.isNotNull(mCommentSheetDialog.getEditCommentView())) {
            mCommentSheetDialog.getEditCommentView().setEditCommentListener(this);
        }

        mPlayMorePopWindow = new PlayMorePopWindow(getActivity());
        mPlayMorePopWindow.setPlayMoreClickListener(this);

        ViewVisibleUtils.setVisibleGone(mBarrageView, mPlayMorePopWindow.isOpenBarrage());
        mCommentCountTv.setText(String.valueOf(mCommentList.size()));
        mDurationSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                isStartTrackingTouch = true;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                MusicServiceConnection.seek((long) seekBar.getProgress());
                isStartTrackingTouch = false;
            }
        });

        startMusicTop();
    }

    private void initGestureDetector() {
        mGestureDetector = new GestureDetector(getActivity(), new GestureDetector.SimpleOnGestureListener() {

            @Override
            public boolean onDoubleTap(MotionEvent event) {
                if (Utils.isNotNull(mLikeCountIv) && !mLikeCountIv.isLiked()) {
                    mLikeCountIv.performClick();
                }
                startLikeAnimator((int) event.getX(), (int) event.getY());
                return true;
            }

            @Override
            public boolean onSingleTapConfirmed(MotionEvent e) {
                playPause();
                return false;
            }
        });
    }

    public void onBindViewHolder() {
        mMusicLyricTv.setText("");
        mMusicLyricTv.setVisibility(View.GONE);

        if (Utils.isNotNull(mSong)) {
            if (MusicServiceConnection.isPlaying()) {
                mPlayPauseView.setImageResource(R.drawable.ic_play_suspend);
            } else {
                mPlayPauseView.setImageResource(R.drawable.ic_play_start);
            }

            MainViewUtils.updatePlayWebpBg(mSong.getBgImgWebp(), mMusicWebpBgSdv);

            if (Utils.isNotNull(mSong.getUser())) {
                AvatarLoader.loadUserAvatar(mSong.getUser().getAvatarUrl(),
                        R.drawable.ic_main_default_avatar,
                        R.drawable.ic_main_default_avatar,
                        mOperateCoverIv);
            }

            if (mSong.getTags() != null && mSong.getTags().size() > 0) {
                mMusicTagTv.setVisibility(View.VISIBLE);
                mMusicTagTv.setText(ResourceUtils.resourceString(R.string.app_music_tag, mSong.getTags().get(0)));
            } else {
                mMusicTagTv.setVisibility(View.GONE);
            }
            mMusicNameTv.setText(mSong.getMusicName());
            mArtistNameTv.setText(ResourceUtils.resourceString(R.string.app_artist_name, mSong.getArtistName(this)));

            mLikeCountIv.setLiked(mSong.isUserLiked);
            mLikeCountIv.setOnLikeListener(this);
            mLikeCountTv.setText(CommonUtils.makeLikeCount(this, mSong.getLikeCount()));
            mShareCountIv.setOnClickListener(this);
            mTopPlayModeIv.setOnClickListener(this);
            nPlayDownloadIv.setOnClickListener(this);
            mInfoPreIv.setOnClickListener(this);
            mPlayPauseView.setOnClickListener(this);
            mInfoNextIv.setOnClickListener(this);
            mOperateCoverIv.setOnClickListener(this);
            mCommentIv.setOnClickListener(this);
            mMusicTagTv.setOnClickListener(this);
            mArtistNameTv.setOnClickListener(this);

            mLikeViewGroup.setOnTouchListener(this);

            long duration = MusicServiceConnection.duration();
            mDurationSeekBar.setMax((int) duration);
            mDurationTv.setText(CommonUtils.makeShortTimeString(this, duration));
        }
    }

    public void startLikeAnimator(int pointX, int pointY) {
        if (likeFirstPoint != null) {
            pointX = likeFirstPoint.x;
            pointY = likeFirstPoint.y;
            likeFirstPoint = null;
        }
        videoLikedHelper.likedTouchAt(mLikeViewGroup, true, pointX, pointY);
    }

    @Override
    public void onDestroy() {
        onClearBarrage();
        mPresenter.onDispose();
        super.onDestroy();
    }

    @Override
    public void finish() {
        MusicServiceConnection.clearQueue();
        super.finish();
    }

    @Override
    public boolean translucentStatusBar() {
        return true;
    }

    @Override
    public int setStatusBarColor() {
        return getResources().getColor(R.color.color_00000000);
    }

    @Override
    public boolean lightStatusBar() {
        return false;
    }

    @Override
    public boolean fitSystemWindows() {
        return true;
    }

    @Override
    public boolean fullScreen() {
        return false;
    }

    @Override
    public void onSuccess(Api.Action action, Object o) {
        super.onSuccess(action, o);
        switch (action) {
            case GET_MUSIC_COMMENT:
                CommentResponse response = (CommentResponse) o;
                if (Utils.isNotNull(response) && Utils.isNotEmptyCollection(response.getData())) {
                    if (Utils.isNotNull(mBarrageView)) {
                        mBarrageView.enqueueBarrage(getFirstBarrage(response.getData().size()));
                    }
                    mCommentList.clear();
                    mCommentList.addAll(response.getData());
                    for (CommentBean comment : mCommentList) {
                        mCommentMap.put(comment.getPostAt(), comment);
                    }
                    mCommentSheetDialog.setCommentList(mCommentList);
                } else {
                    if (Utils.isNotNull(mBarrageView)) {
                        mBarrageView.enqueueBarrage(getFirstBarrage(0));
                    }
                    mCommentList.clear();
                    mCommentSheetDialog.setCommentList(mCommentList);
                }
                break;
            case GET_COMMENT_SUB:
                if (Utils.isNotNull(mCommentSheetDialog)) {
                    CommentResponse subResponse = (CommentResponse) o;
                    int position = subResponse.getPosition();
                    CommentBean oldComment = mCommentSheetDialog.getCommentItem(position);
                    oldComment.setSubComments(subResponse.getData());

                    mCommentList.set(position, oldComment);
                    mCommentSheetDialog.setCommentList(mCommentList);
                }
                break;
            case MUSIC_LRC:
                MusicLyricManager.getInstance().clear();
                MusicLyricManager.getInstance().setLyricFile((File) o, "UTF-8");
                if (Utils.isNotNull(mPlayMorePopWindow)) {
                    mPlayMorePopWindow.showCloseLyricTv();
                }
                break;
            case SEND_MUSIC_COMMENT:
                try {
                    SendCommentResponse sendCommentResponse = (SendCommentResponse) o;
                    CommentRequest request = sendCommentResponse.getRequest();
                    request.setCommentId(sendCommentResponse.getData());
                    insertComment(sendCommentResponse.getRequest());
                    Toast.makeText(getActivity(), R.string.comment_send_success, Toast.LENGTH_SHORT).show();
                } catch (Exception e) {

                }
                break;
            case DELETE_COMMENT:
                if (Utils.isNotNull(mCommentSheetDialog) && Utils.isNotNull(mCommentCountTv)) {
                    mCommentList.clear();
                    mCommentList.addAll(mCommentSheetDialog.getCommentList());
                    mCommentCountTv.setText(String.valueOf(mCommentList.size()));
                    mCommentSheetDialog.setCommentCount();
                }
                Toast.makeText(getActivity(), R.string.comment_delete_success, Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void onError(Api.Action action, String code, String message) {
        super.onError(action, code, message);
        switch (action) {
            case MUSIC_LRC:
                MusicLyricManager.getInstance().clear();
                hideMusicLyricView();
                if (Utils.isNotNull(mPlayMorePopWindow)) {
                    mPlayMorePopWindow.hideCloseLyricTv();
                }
                break;
        }
    }

    @Override
    public void showMessage(String message) {
        super.showMessage(message);
        CoreUtils.makeText(this, message);
    }

    @Override
    public void onPlayerClicked(long progress, String content) {
        playSeek(progress);
    }

    @Subscriber
    public void onMetaChangedEvent(EventBusTags.MetaChangedEvent event) {
        Timber.e("===>>> onMetaChangedEvent " + event);

        try {
            if (mCurrentSongId != event.getSongId()) {
                DebugLogger.e("onMetaChangedEvent", "切歌 拉取评论 == ： " + event);
                onBindViewHolder();

                mCurrentSongId = event.getSongId();

                onClearBarrage();
                mPresenter.getMusicComment(mSong.getSongId(), DEFAULT_REQUEST_COMMENT_SIZE, "");

                MusicLyricManager.getInstance().clear();
                DebugLogger.e("onMetaChangedEvent", "歌词: " + mSong.musicName + "   *** realName *** ：" + mSong.realName);
                String musicName = Utils.isNotEmptyString(mSong.realName) ? mSong.realName : mSong.musicName;
                String musicAuthors = Utils.isNotEmptyCollection(mSong.realAuthors) ? mSong.realAuthors.get(0) : mSong.artistName;
                DebugLogger.e("onMetaChangedEvent", "歌词: name -- " + musicName + "   authors -- " + musicAuthors);
                if (Utils.isNotEmptyString(mSong.getLrc())) {
                    Observable.create((ObservableOnSubscribe<File>)
                            emitter -> {
                                File file = null;
                                try {
                                    String rawLyric = LyricUtil.decryptBASE64(mSong.getLrc());
                                    file = LyricUtil.writeLrcToLoc(musicName, musicAuthors, rawLyric);
                                    if (Utils.isNull(file)) {
                                        file = new File("嗨歌歌词");
                                    }
                                } catch (Throwable throwable) {
                                    DebugLogger.e(throwable);
                                    if (Utils.isNull(file)) {
                                        file = new File("嗨歌歌词");
                                    }
                                }
                                emitter.onNext(file);
                                emitter.onComplete();
                            })
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(file -> {
                                if (Utils.isNotNull(file) && file.exists()){
                                    onSuccess(Api.Action.MUSIC_LRC, file);
                                } else {
                                    mPresenter.getLyricFile(musicName, musicAuthors, MusicServiceConnection.duration());
                                }
                            });
                } else {
                    mPresenter.getLyricFile(musicName, musicAuthors, MusicServiceConnection.duration());
                }
            }

            repeatModeChanged();
        } catch (Exception e) {
            DebugLogger.e(e);
        }
    }

    @Subscriber
    public void onMusicIsLoadingEvent(EventBusTags.MusicIsLoadingEvent event) {
        Timber.e("===>>> onMusicIsLoadingEvent. isLoading:" + event.isLoading());
        try {
            if (event.isLoading()) {
                LoadingDrawable loadingDrawable = new LoadingDrawable(new
                        MaterialLoadingRenderer2.Builder(this)
                        .setWidth(CoreUtils.dip2px(10))
                        .setHeight(CoreUtils.dip2px(10))
                        .setCenterRadius(CoreUtils.dip2px(5))
                        .setStrokeWidth(CoreUtils.dip2px(1))
                        .build());
                loadingDrawable.setBounds(0, 0,
                        CoreUtils.dip2px(10),
                        CoreUtils.dip2px(10));
                mDurationSeekBar.setThumb(loadingDrawable);
                loadingDrawable.start();
            } else {
                mDurationSeekBar.setThumb(getResources().getDrawable(R.drawable.app_selector_music_info_thumb));
            }
        } catch (Exception e) {
            DebugLogger.e(e);
        }
    }

    @Subscriber
    public void onRepeatModeChangedEvent(EventBusTags.RepeatModeChangedEvent event) {
        repeatModeChanged();
    }

    @Subscriber
    public void onSubscriber(EventBusTags.UI tags) {
        switch (tags) {
            case UPDATE_USER_UI:
                mUserBean = UserPrefHelper.getMeUser();
                break;
        }
    }

    @OnClick(R.id.app_layout_music_info_top_back_iv)
    public void onBack() {
        finish();
    }

    @OnClick(R.id.app_layout_music_operate_more_iv)
    public void onMenuMore() {
        CommonStatistics.sendEvent(StatisticsConstant.MORE_CLICK);

        if (UserPrefHelper.isLogin()) {
            if (mPlayMorePopWindow == null) {
                mPlayMorePopWindow = new PlayMorePopWindow(getActivity());
                mPlayMorePopWindow.setPlayMoreClickListener(this);
            }
            if (Utils.isNotNull(mOperateMoreIv)) {
                mPlayMorePopWindow.showPopWindow(mOperateMoreIv);
            }
        } else {
            LoginDialogFragment.newInstance().show(getSupportFragmentManager(), LoginDialogFragment.class.getName());
        }
    }

    @Override
    public void onClick(View v) {
        if (NoDoubleClickListener.isFastClick()) {
            return;
        }

        switch (v.getId()) {
            case R.id.iv_play_download:
                onDownloadMusic();
                break;
            case R.id.iv_play_random:
                onClickPlayMode();
                break;
            case R.id.app_layout_music_operate_cover_iv:
                if (mSong != null && mSong.getUser() != null) {
                    CommonStatistics.sendEvent(StatisticsConstant.HEAD_CLICK);
                    ActivityLauncherStart.startOtherUser(this, mSong.getUser());
                }
                break;
            case R.id.app_layout_music_operate_share_iv:
                CommonStatistics.sendEvent(StatisticsConstant.SHARE_CLICK);

                try {
                    MusicShareDialog.newInstance()
                            .setWebpBgName(mSong.getBgImgWebp())
                            .setMusic(mSong)
                            .setShareListener(() -> {
                                mPresenter.countShare(mSong.songId);
                                mSong.setShareCount(mSong.getShareCount() + 1);
                            })
                            .show(getSupportFragmentManager(), MusicShareDialog.class.getName());
                } catch (Exception e) {
                    Timber.e(e);
                }
                break;
            case R.id.iv_play_last:
                playPre();
                break;
            case R.id.iv_play_next:
                playNext();
                break;
            case R.id.iv_play_start:
                playPause();
                break;
            case R.id.app_layout_music_operate_danmu_iv:
                CommonStatistics.sendEvent(StatisticsConstant.COMMENT_CLICK);

                if (UserPrefHelper.isLogin()) {
                    mCommentSheetDialog.show();
                }  else {
                    LoginDialogFragment.newInstance().show(getSupportFragmentManager(), LoginDialogFragment.class.getName());
                }
                break;
            case R.id.app_layout_music_other_info_tag_tv:
                if (Utils.isNotNull(mSong) && Utils.isNotEmptyCollection(mSong.getTags())) {
                    CommonStatistics.sendEvent(StatisticsConstant.LABEL_CLICK);
                    String tag = mSong.getTags().get(0);
                    ActivityLauncherStart.startTagMusic(getActivity(), tag);
                }
                break;
            case R.id.app_layout_music_other_info_artist_tv:
                if (mSong != null && mSong.getUser() != null) {
                    ActivityLauncherStart.startOtherUser(this, mSong.getUser());
                }
                break;
        }
    }

    public void onClickPlayMode() {
        if (Utils.isNull(mTopPlayModeIv)) return;

        if (mPlayMode == MusicCompleteActivity.PlayMode.REPEAT_ALL) {
            mTopPlayModeIv.setImageResource(R.drawable.ic_play_single);
            MusicServiceConnection.setShuffleMode(MusicDataManager.SHUFFLE_NONE);
            MusicServiceConnection.setRepeatMode(MusicDataManager.REPEAT_CURRENT);
            Toast.makeText(this, R.string.app_repeat_current, Toast.LENGTH_SHORT).show();
            mPlayMode = MusicCompleteActivity.PlayMode.CURRENT;
        } else if (mPlayMode == MusicCompleteActivity.PlayMode.CURRENT) {
            mTopPlayModeIv.setImageResource(R.drawable.ic_play_random);
            MusicServiceConnection.setShuffleMode(MusicDataManager.SHUFFLE_NORMAL);
            MusicServiceConnection.setRepeatMode(MusicDataManager.REPEAT_ALL);
            Toast.makeText(this, R.string.app_shuffle_all, Toast.LENGTH_SHORT).show();
            mPlayMode = MusicCompleteActivity.PlayMode.SHUFFLE;
        } else if (mPlayMode == MusicCompleteActivity.PlayMode.SHUFFLE) {
            mTopPlayModeIv.setImageResource(R.drawable.ic_play_sequential);
            MusicServiceConnection.setShuffleMode(MusicDataManager.SHUFFLE_NONE);
            Toast.makeText(this, R.string.app_repeat_all, Toast.LENGTH_SHORT).show();
            mPlayMode = MusicCompleteActivity.PlayMode.REPEAT_ALL;
        }
    }

    private void repeatModeChanged() {
        if (Utils.isNull(mTopPlayModeIv)) return;

        int shuffleMode = MusicServiceConnection.getShuffleMode();
        int repeatMode = MusicServiceConnection.getRepeatMode();
        if (shuffleMode == MusicDataManager.SHUFFLE_NONE) {
            if (repeatMode == MusicDataManager.REPEAT_CURRENT) {
                //单曲播放模式
                mTopPlayModeIv.setImageDrawable(getResources().getDrawable(R.drawable.ic_play_single));
                mPlayMode = PlayMode.CURRENT;
            } else {
                //顺序播放模式
                mTopPlayModeIv.setImageDrawable(getResources().getDrawable(R.drawable.ic_play_sequential));
                mPlayMode = PlayMode.REPEAT_ALL;
            }
        } else if (shuffleMode == MusicDataManager.SHUFFLE_NORMAL || shuffleMode == MusicDataManager.SHUFFLE_AUTO) {
            //随机播放模式
            mTopPlayModeIv.setImageDrawable(getResources().getDrawable(R.drawable.ic_play_random));
            mPlayMode = PlayMode.SHUFFLE;
        }
    }

    private void startMusicTop() {
        playMusic();

        mPresenter.startUpateMusicTime();
    }

    public void startWebpAnim() {
        if (mAnimatable != null && !mAnimatable.isRunning()) {
            mAnimatable.start();
        }

        if (Utils.isNotNull(mBarrageView)) {
            mBarrageView.restartBarrage();
        }

        if (Utils.isNotNull(mPlayPauseView)) {
            mPlayPauseView.setImageResource(R.drawable.ic_play_suspend);
        }
    }

    public void stopWebpAnim() {
        if (mAnimatable != null && mAnimatable.isRunning()) {
            mAnimatable.stop();
        }

        if (Utils.isNotNull(mBarrageView)) {
            mBarrageView.pauseBarrage();
        }

        if (Utils.isNotNull(mPlayPauseView)) {
            mPlayPauseView.setImageResource(R.drawable.ic_play_start);
        }
    }

    /**
     * 播放音乐
     */
    private void playMusic() {
        if (Utils.isNotNull(mSong)) {
            ArrayList<Song> songs = new ArrayList<>();
            songs.add(mSong);

            startWebpAnim();
            if (MusicServiceConnection.isShort()) {
                MusicServiceConnection.open(songs, 0, false, false);
            } else {
                MusicServiceConnection.playAll(songs, 0, false, false);
            }
        }
    }

    private void playNext() {
        playSeek(0);
    }

    private void playPre() {
        playSeek(0);
    }

    private void playPause() {
        if (MusicServiceConnection.isPlaying()) {
            MusicServiceConnection.playOrPause();
            stopWebpAnim();
        } else {
            playMusic();
            startWebpAnim();
        }
    }

    private void playSeek(long progress) {
        playMusic();
        MusicServiceConnection.seek(progress);
    }

    @Override
    public void onUpateMusicTime() {
        try {
            long position = MusicServiceConnection.position();
            if (mDurationSeekBar.getMax() <= 0) {
                long duration = MusicServiceConnection.duration();
                Timber.e("===>>> mUpdateProgress duration:" + duration);
                mDurationSeekBar.setMax((int) MusicServiceConnection.duration());
                mDurationTv.setText(CommonUtils.makeShortTimeString(this, duration));
            }

            if (!isStartTrackingTouch) {
                mDurationSeekBar.setProgress((int) position);
            }
            mDurationCurTv.setText(CommonUtils.makeShortTimeString(this, position));

            if (Utils.isNotNull(mMusicLyricTv) && position > 1000
                    && Utils.isNotNull(mPlayMorePopWindow) && mPlayMorePopWindow.isOpenLyric()) {
                String content = MusicLyricManager.getInstance().getMusicLyricHint(position);
                mMusicLyricTv.setText(content);
                ViewVisibleUtils.setVisibleGone(mMusicLyricTv, Utils.isNotEmptyString(content));
            } else {
                hideMusicLyricView();
            }

            long postAt = MusicServiceConnection.position() / 1000;
            if (mCommentMap.containsKey(postAt) && mLastCommentPostAt != postAt) {
                CommentBean commentBean = mCommentMap.get(postAt);
                if (Utils.isNotNull(commentBean) && Utils.isNotNull(commentBean.getUserBean())
                        && commentBean.getContext().length() <= MAX_BARRAGE_TEXT_COUNT) {
                    mLastCommentPostAt = postAt;
                    if (Utils.isNotNull(mBarrageView)) {
                        mBarrageView.enqueueBarrage(commentBean);
                    }
                }
            }

            if (MusicServiceConnection.isPlaying() && Utils.isNotNull(mMusicWebpBgSdv.getController())) {
                mAnimatable = mMusicWebpBgSdv.getController().getAnimatable();
                startWebpAnim();
            } else if (Utils.isNotNull(mMusicWebpBgSdv.getController())) {
                mAnimatable = mMusicWebpBgSdv.getController().getAnimatable();
                stopWebpAnim();
            }

            if (Utils.isNotNull(mPlayMorePopWindow) && Utils.isNotNull(mBarrageView)) {
                ViewVisibleUtils.setVisibleGone(mBarrageView, mPlayMorePopWindow.isOpenBarrage());
            }

            if (Utils.isNotNull(mCommentCountTv)) {
                mCommentCountTv.setText(String.valueOf(mCommentList.size()));
            }
        } catch (Exception e) {
            DebugLogger.e(TAG, "mUpdateProgress：" + e);
        }
    }

    @Override
    public void liked(LikeButton likeButton) {
        CommonStatistics.sendEvent(StatisticsConstant.LIKE_CLICK);

        try {
            mPresenter.countLike(mSong);
            mPresenter.recordFavoriteSong(mSong);
            mSong.setIsUserLiked(true);
            mSong.setLikeCount(mSong.getLikeCount() + 1);

            EventBus.getDefault().post(mSong);
        } catch (Exception e) {
            Timber.e(e);
        }
    }

    @Override
    public void unLiked(LikeButton likeButton) {
        try {
            mPresenter.countLike(mSong);
            mPresenter.deleteFavoriteSong(mSong);
            mSong.setIsUserLiked(false);
            mSong.setLikeCount(mSong.getLikeCount() - 1);

            EventBus.getDefault().post(mSong);
        } catch (Exception e) {
            Timber.e(e);
        }
    }

    @Override
    public void onShowLoginDialog() {
        LoginDialogFragment.newInstance().show(getSupportFragmentManager(), LoginDialogFragment.class.getName());
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        int id = v.getId();
        if (id == R.id.view_group_like) {
            if (mGestureDetector != null) {
                mGestureDetector.onTouchEvent(event);
            }
        }
        return true;
    }

    @Override
    public void onCloseBarrage(boolean isOpenBarrage) {
        CommonStatistics.sendEvent(StatisticsConstant.MORE_CLOSE_COMMENT_CLICK);

        if (Utils.isNotNull(mBarrageView)) {
            ViewVisibleUtils.setVisibleGone(mBarrageView, true);
        }
    }

    @Override
    public void onCloseLyric(boolean isOpenLyric) {
        if (Utils.isNotNull(mMusicLyricTv)) {
            ViewVisibleUtils.setVisibleGone(mMusicLyricTv, true);
        }
    }

    public void onDownloadMusic() {
        CommonStatistics.sendEvent(StatisticsConstant.MORE_DOWNLOAD_CLICK);

        try {
            if (UserPrefHelper.isLogin()) {
                if (ShareUtils.checkDownloadShare(UserPrefHelper.getDownloadMusicCount())) {
                    DownloadShareDialog.newInstance().show(getSupportFragmentManager(), DownloadShareDialog.class.getName());
                } else if (mPresenter.downloadMusic(mSong)) {
                    mPresenter.countDownload(mSong.songId);

                    mSong.setDownloadCount(mSong.getDownloadCount() + 1);
                }
            }  else {
                LoginDialogFragment.newInstance().show(getSupportFragmentManager(), LoginDialogFragment.class.getName());
            }
        } catch (Exception e) {
            Timber.e(e);
        }
    }

    @Override
    public void onSend(CommentRequest request) {
        DebugLogger.e(TAG, "评论：" + request.getContext() + "  " + MusicServiceConnection.position());

        request.setMusicId(MusicServiceConnection.getCurrentAudioId());
        request.setPostAt((MusicServiceConnection.position() / 1000) + 1);
        mPresenter.sendMusicComment(request);
    }

    private CommentBean getFirstBarrage(int count) {
        CommentBean commentBean = new CommentBean();
        UserBean userBean = new UserBean();
        userBean.setType(TYPE_OFFICIAL_USER);
        commentBean.setUserBean(userBean);
        if (Utils.isZero(count)) {
            commentBean.setContext(ResourceUtils.resourceString(R.string.barrage_start_no_exist_content));
        } else {
            commentBean.setContext(ResourceUtils.resourceString(R.string.barrage_start_exist_content, count));
        }
        return commentBean;
    }

    private void insertComment(CommentRequest request) {
        CommentBean commentBean = new CommentBean();
        commentBean.setId(request.getCommentId());
        commentBean.setUserBean(mUserBean);
        if (request.isSub()) {
            try {
                if (Utils.isNotNull(mCommentSheetDialog)) {
                    int position = request.getPosition();
                    commentBean.setContext(request.getContext());
                    commentBean.setMusicId(request.getMusicId());
                    commentBean.setPostAt(request.getPostAt());
                    commentBean.setCreatedAt(System.currentTimeMillis());

                    CommentBean oldBean = mCommentSheetDialog.getCommentItem(position);
                    ArrayList<CommentBean> subComments = oldBean.getSubComments();
                    if (Utils.isEmptyCollection(subComments)) {
                        subComments = new ArrayList<>();
                    }
                    subComments.add(commentBean);
                    oldBean.setSubCount(subComments.size());
                    oldBean.setSubComments(subComments);

                    mCommentList.set(position, oldBean);
                    mCommentSheetDialog.setCommentList(mCommentList);
                }
            } catch (Exception e) {
                DebugLogger.e(e);
            }
        } else {
            if (Utils.isNotNull(mCommentSheetDialog)) {
                commentBean.setContext(request.getContext());
                commentBean.setMusicId(request.getMusicId());
                commentBean.setPostAt(request.getPostAt());
                commentBean.setCreatedAt(System.currentTimeMillis());

                mCommentList.add(0, commentBean);
                mCommentMap.put(commentBean.getPostAt(), commentBean);

                mCommentSheetDialog.setCommentList(mCommentList);
            }
        }
    }

    private void hideMusicLyricView() {
        if (Utils.isNotNull(mMusicLyricTv)) {
            mMusicLyricTv.setVisibility(View.GONE);
            mMusicLyricTv.setText("");
        }
    }

    private void onClearBarrage() {
        mLastCommentPostAt = 0;

        if (Utils.isNotNull(mBarrageView)) {
            mBarrageView.clearBarrage();
        }

        if (Utils.isNotEmptyCollection(mCommentList)) {
            mCommentList.clear();
            mCommentMap.clear();
        }
    }

    public enum PlayMode {
        REPEAT_ALL,
        CURRENT,
        SHUFFLE
    }
}
