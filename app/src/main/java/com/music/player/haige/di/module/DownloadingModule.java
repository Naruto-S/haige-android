package com.music.player.haige.di.module;

import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.DownloadingModel;

import dagger.Module;
import dagger.Provides;

/**
 * ================================================
 * Created by huangcong on 2018/3/20.
 * ================================================
 */
@Module
public class DownloadingModule {
    private MVContract.DownloadingView view;

    public DownloadingModule(MVContract.DownloadingView view) {
        this.view = view;
    }

    @FragmentScope
    @Provides
    MVContract.DownloadingView provideView() {
        return this.view;
    }

    @FragmentScope
    @Provides
    MVContract.DownloadingModel provideModel(DownloadingModel model) {
        return model;
    }

}
