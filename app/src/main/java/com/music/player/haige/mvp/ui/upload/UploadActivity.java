package com.music.player.haige.mvp.ui.upload;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.sdk.android.oss.ClientConfiguration;
import com.alibaba.sdk.android.oss.OSS;
import com.alibaba.sdk.android.oss.OSSClient;
import com.alibaba.sdk.android.oss.common.OSSLog;
import com.alibaba.sdk.android.oss.common.auth.OSSCredentialProvider;
import com.alibaba.sdk.android.oss.common.auth.OSSPlainTextAKSKCredentialProvider;
import com.hc.core.di.component.AppComponent;
import com.music.player.haige.R;
import com.music.player.haige.app.SystemUIConfig;
import com.music.player.haige.app.base.AppBaseActivity;
import com.music.player.haige.app.base.pref.UserPrefHelper;
import com.music.player.haige.app.constants.IntentConstants;
import com.music.player.haige.app.image.loader.HaigeImageLoader;
import com.music.player.haige.app.image.loader.LocalImageLoader;
import com.music.player.haige.app.image.utils.MD5;
import com.music.player.haige.app.image.widget.HaigeImageView;
import com.music.player.haige.app.oss.app.Config;
import com.music.player.haige.app.oss.customprovider.OssService;
import com.music.player.haige.app.utils.launcher.ActivityLauncherStart;
import com.music.player.haige.di.component.DaggerUpdateComponent;
import com.music.player.haige.di.module.UpdateModule;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.Api;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.model.entity.music.UploadMusicRequest;
import com.music.player.haige.mvp.presenter.UpdatePresenter;
import com.music.player.haige.mvp.ui.music.utils.MusicUtils;
import com.music.player.haige.mvp.ui.upload.dialog.ProgressDialog;
import com.music.player.haige.mvp.ui.user.MeUploadActivity;
import com.music.player.haige.mvp.ui.user.utils.AvatarUtils;
import com.music.player.haige.mvp.ui.utils.DebugLogger;
import com.music.player.haige.mvp.ui.utils.FileUtils;
import com.music.player.haige.mvp.ui.utils.Utils;
import com.umeng.socialize.UMShareAPI;

import org.simple.eventbus.Subscriber;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

import static com.music.player.haige.app.oss.customprovider.OssService.UPLOAD_TYPE_IMAGE;
import static com.music.player.haige.app.oss.customprovider.OssService.UPLOAD_TYPE_MUSIC;

public class UploadActivity extends AppBaseActivity<UpdatePresenter> implements MVContract.UpdateView,
        SystemUIConfig, OssService.UploadCallBack {

    private static final String BG_TYPE_RANDOM = "随机动图";
    private static final String BG_TYPE_COVER = "封面图";

    @BindView(R.id.hiv_music_cover)
    HaigeImageView mMusicCoverHiv;

    @BindView(R.id.edit_music_name)
    EditText mMusicNameTv;

    @BindView(R.id.edit_music_author)
    EditText mMusicAuthorTv;

    @BindView(R.id.tv_music_type)
    TextView mMusicTypeTv;

    @BindView(R.id.edit_music_bg_type)
    TextView mMusicBgTypeTv;

    //OSS的上传下载
    private OssService mOssService;
    private Song mUploadMusic;
    private String mCoverPath;
    private ProgressDialog mProgressDialog;
    private ArrayList<String> mMusicTypes = new ArrayList<>();

    private String mResultCover;
    private String mResultMusic;

    @Override
    public void setupActivityComponent(AppComponent appComponent) {
        DaggerUpdateComponent
                .builder()
                .appComponent(appComponent)
                .updateModule(new UpdateModule(this))
                .build()
                .inject(this);
    }

    @Override
    public int initView(Bundle savedInstanceState) {
        return R.layout.activity_upload;
    }

    @Override
    public void initData(Bundle savedInstanceState) {
        if (Utils.isNotNull(getIntent())) {
            mUploadMusic = getIntent().getParcelableExtra(IntentConstants.EXTRA_CURRENT_MUSIC);
        }

        if (Utils.isNull(mUploadMusic)) {
            finish();
        }

        mProgressDialog = ProgressDialog.create(getSupportFragmentManager()).setCancelOutside(false);

        mMusicNameTv.setText(mUploadMusic.getMusicName());
        mMusicAuthorTv.setText(mUploadMusic.getArtistName());
        HaigeImageLoader.loadTagCover(mUploadMusic.getMusicCover(),
                R.drawable.bg_rank_default,
                R.drawable.bg_rank_default,
                mMusicCoverHiv);

        mOssService = initOSS(Config.endpoint, Config.bucket);
    }

    @Override
    public boolean translucentStatusBar() {
        return false;
    }

    @Override
    public int setStatusBarColor() {
        return getResources().getColor(R.color.color_00000000);
    }

    @Override
    public boolean lightStatusBar() {
        return false;
    }

    @Override
    public boolean fitSystemWindows() {
        return true;
    }

    @Override
    public boolean fullScreen() {
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);

        if (requestCode == AvatarUtils.REQUEST_GALLERY && resultCode == RESULT_OK) {
            mCoverPath = FileUtils.getPath(this, data.getData());
            LocalImageLoader.displayLocalImage(mCoverPath, mMusicCoverHiv);
        }
    }

    @Subscriber
    public void onSubscriber(ArrayList<String> types) {
        if (Utils.isNotEmptyCollection(types)) {
            mMusicTypes.clear();
            mMusicTypes.addAll(types);

            StringBuffer stringBuffer = new StringBuffer();
            for (String type : types) {
                stringBuffer.append(type);
                stringBuffer.append("   ");
            }
            mMusicTypeTv.setText(stringBuffer.toString());
        }
    }

    @OnClick({R.id.tv_upload, R.id.tv_music_type, R.id.layout_camera, R.id.hiv_music_cover,
            R.id.layout_music_bg_type, R.id.app_fragment_back_iv})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.app_fragment_back_iv:
                finish();
                break;
            case R.id.tv_upload:
                uploadMusic();
                break;
            case R.id.tv_music_type:
                ActivityLauncherStart.startMusicType(this, mMusicTypes);
                break;
            case R.id.layout_camera:
            case R.id.hiv_music_cover:
                AvatarUtils.startGalleryIntent(getActivity());
                break;
            case R.id.layout_music_bg_type:
                showSelectBgDialog();
                break;
        }
    }

    private void uploadMusic() {
        if (Utils.isEmptyString(mCoverPath)) {
            Toast.makeText(this, "请先选择音乐封面", Toast.LENGTH_SHORT).show();
            return;
        }

        mProgressDialog.show();
        mOssService.asyncPutFile(getUploadMusicCover(mCoverPath), mCoverPath, UPLOAD_TYPE_IMAGE);
    }

    private void showSelectBgDialog() {
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_bottom_upload_music_bg, null);
        TextView choose = view.findViewById(R.id.tv_profile_avatar_choose);
        TextView take = view.findViewById(R.id.tv_profile_avatar_take);

        final Dialog mBottomSheetDialog = new Dialog(getActivity(), R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.show();

        if (mMusicBgTypeTv.getText().toString().equals(BG_TYPE_RANDOM)) {
            choose.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_upload_select, 0);
            take.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        } else if (mMusicBgTypeTv.getText().toString().equals(BG_TYPE_COVER)) {
            choose.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            take.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_upload_select, 0);
        }

        choose.setOnClickListener(v -> {
            choose.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_upload_select, 0);
            take.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            mMusicBgTypeTv.setText(BG_TYPE_RANDOM);
            mBottomSheetDialog.dismiss();
        });

        take.setOnClickListener(v -> {
            choose.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            take.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_upload_select, 0);
            mMusicBgTypeTv.setText(BG_TYPE_COVER);
            mBottomSheetDialog.dismiss();
        });
    }

    private int getBgType() {
        int bgType = 0;
        if (mMusicBgTypeTv.getText().toString().equals(BG_TYPE_RANDOM)) {
            bgType = 0;
        } else if (mMusicBgTypeTv.getText().toString().equals(BG_TYPE_COVER)) {
            bgType = 1;
        }
        return bgType;
    }

    private String getUploadMusicName(String path) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("user");
        buffer.append(File.separator);
        buffer.append(MD5.getMD5(UserPrefHelper.getUserId()));
        buffer.append(File.separator);
        buffer.append(System.currentTimeMillis());
        buffer.append(MusicUtils.getPathSuffix(path));
        return buffer.toString();
    }

    private String getUploadMusicCover(String path) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("user");
        buffer.append(File.separator);
        buffer.append(MD5.getMD5(UserPrefHelper.getUserId()));
        buffer.append(File.separator);
        buffer.append(System.currentTimeMillis());
        buffer.append(MusicUtils.getPathSuffix(path));
        return buffer.toString();
    }

    public OssService initOSS(String endpoint, String bucket) {
//        移动端是不安全环境，不建议直接使用阿里云主账号ak，sk的方式。建议使用STS方式。具体参
//        https://help.aliyun.com/document_detail/31920.html
//        注意：SDK 提供的 PlainTextAKSKCredentialProvider 只建议在测试环境或者用户可以保证阿里云主账号AK，SK安全的前提下使用。具体使用如下
//        主账户使用方式
//        String AK = "******";
//        String SK = "******";
//        credentialProvider = new PlainTextAKSKCredentialProvider(AK,SK)
//        以下是使用STS Sever方式。
//        如果用STS鉴权模式，推荐使用OSSAuthCredentialProvider方式直接访问鉴权应用服务器，token过期后可以自动更新。
//        详见：https://help.aliyun.com/document_detail/31920.html
//        OSSClient的生命周期和应用程序的生命周期保持一致即可。在应用程序启动时创建一个ossClient，在应用程序结束时销毁即可。

        OSSCredentialProvider credentialProvider;
        String AK = "LTAIUGhJvmFGPqGs";
        String SK = "lcgUr3j49bDm1qUHd5gn2F3MEcgSFL";
        credentialProvider = new OSSPlainTextAKSKCredentialProvider(AK, SK);

        ClientConfiguration conf = new ClientConfiguration();
        conf.setConnectionTimeout(15 * 1000); // 连接超时，默认15秒
        conf.setSocketTimeout(15 * 1000); // socket超时，默认15秒
        conf.setMaxConcurrentRequest(5); // 最大并发请求书，默认5个
        conf.setMaxErrorRetry(2); // 失败后最大重试次数，默认2次
        OSS oss = new OSSClient(getApplicationContext(), endpoint, credentialProvider, conf);
        OSSLog.enableLog();
        return new OssService(oss, bucket, this);
    }

    @Override
    public void onUploadProgress(int type, int progress) {
        DebugLogger.e(TAG, type + " == progress == " + progress);
        if (type == UPLOAD_TYPE_IMAGE) {
            mProgressDialog.setProgress(progress / 5);
        } else if (type == UPLOAD_TYPE_MUSIC) {
            if (progress == 100) {
                mProgressDialog.setProgress(98);
            } else if (progress >= 20) {
                mProgressDialog.setProgress(progress);
            } else {
                mProgressDialog.setProgress(20);
            }
        }
    }

    @Override
    public void onUploadComplete(int type, String url) {
        DebugLogger.e(TAG, type + " == onUploadComplete url == " + url);
        if (type == UPLOAD_TYPE_IMAGE) {
            mResultCover = url;

            mOssService.asyncPutFile(getUploadMusicName(mUploadMusic.musicPath), mUploadMusic.musicPath, UPLOAD_TYPE_MUSIC);
        } else if (type == UPLOAD_TYPE_MUSIC) {
            mResultMusic = url;

            UploadMusicRequest request = new UploadMusicRequest();
            request.setMusicName(mMusicNameTv.getText().toString());
            request.setArtistName(mMusicAuthorTv.getText().toString());
            request.setTags(mMusicTypes);
            request.setCover(mResultCover);
            request.setMusicPath(mResultMusic);
            request.setBgType(getBgType());
            mPresenter.uploadMusic(request);
        }
    }

    @Override
    public void onUploadFail(int type, String info) {
        DebugLogger.e(TAG, type + " == onUploadFail == ");
        if (type == UPLOAD_TYPE_IMAGE) {
            mOssService.asyncPutFile(getUploadMusicName(mUploadMusic.musicPath), mUploadMusic.musicPath, UPLOAD_TYPE_MUSIC);
        } else if (type == UPLOAD_TYPE_MUSIC) {
            mProgressDialog.dismiss();
            Toast.makeText(this, "上传失败", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onSuccess(Api.Action action, Object o) {
        super.onSuccess(action, o);

        switch (action) {
            case UPLOAD_MUSIC:
                mProgressDialog.setProgress(100);
                mProgressDialog.dismiss();
                Toast.makeText(this, "上传成功", Toast.LENGTH_SHORT).show();
                ActivityLauncherStart.startActivity(getActivity(), MeUploadActivity.class);
                finish();
                break;
        }
    }

    @Override
    public void onError(Api.Action action, String code, String message) {
        super.onError(action, code, message);

        switch (action) {
            case UPLOAD_MUSIC:
                mProgressDialog.dismiss();
                Toast.makeText(this, "上传失败：" + message, Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
