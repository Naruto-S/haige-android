package com.music.player.haige.di.module;

import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.LoginModel;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Naruto on 2018/5/5.
 */
@Module
public class LoginDialogModule {
    private MVContract.LoginView view;

    public LoginDialogModule(MVContract.LoginView view){
        this.view = view;
    }

    @FragmentScope
    @Provides
    MVContract.LoginView provideView(){
        return view;
    }

    @FragmentScope
    @Provides
    MVContract.LoginModel provideModel(LoginModel model){
        return model;
    }
}
