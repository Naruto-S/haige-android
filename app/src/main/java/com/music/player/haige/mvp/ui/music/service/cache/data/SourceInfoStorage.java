package com.music.player.haige.mvp.ui.music.service.cache.data;

import com.music.player.haige.mvp.ui.music.service.cache.SourceInfo;

/**
 * Storage for {@link SourceInfo}.
 *
 * @author Alexey Danilov (danikula@gmail.com).
 */
public interface SourceInfoStorage {

    SourceInfo get(String url);

    void put(String url, SourceInfo sourceInfo);

    void release();
}
