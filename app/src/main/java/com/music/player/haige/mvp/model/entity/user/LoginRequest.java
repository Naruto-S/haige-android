package com.music.player.haige.mvp.model.entity.user;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LoginRequest implements Serializable {

    public static final String TYPE_LOGIN_QQ = "QQ";
    public static final String TYPE_LOGIN_WEIXIN = "WX";
    public static final String TYPE_LOGIN_SINA = "SINA";

    @SerializedName("type")
    private String type;

    @SerializedName("qq_info")
    private LoginQQBean qqInfo;

    @SerializedName("wx_info")
    private LoginWXBean wxInfo;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public LoginQQBean getQqInfo() {
        return qqInfo;
    }

    public void setQqInfo(LoginQQBean qqInfo) {
        this.qqInfo = qqInfo;
    }

    public LoginWXBean getWxInfo() {
        return wxInfo;
    }

    public void setWxInfo(LoginWXBean wxInfo) {
        this.wxInfo = wxInfo;
    }

    @Override
    public String toString() {
        return "LoginRequest{" +
                "type='" + type + '\'' +
                ", qqInfo=" + qqInfo +
                ", wxInfo=" + wxInfo +
                '}';
    }
}
