package com.music.player.haige.mvp.model;

import com.hc.base.mvp.BaseModel;
import com.hc.core.integration.IRepositoryManager;
import com.music.player.haige.app.GlobalConfiguration;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.usecase.GetAlbums;

import javax.inject.Inject;

/**
 * ================================================
 * Created by huangcong on 2018/3/2.
 * ================================================
 */

public class AlbumModel extends BaseModel implements MVContract.AlbumModel {

    private GetAlbums mGetAlbumsCase;

    @Inject
    public AlbumModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
        mGetAlbumsCase = new GetAlbums(GlobalConfiguration.sRepository);
    }

    @Override
    public GetAlbums.ResponseValue loadAlbum(String action) {
        return mGetAlbumsCase.execute(new GetAlbums.RequestValues(action));
    }
}
