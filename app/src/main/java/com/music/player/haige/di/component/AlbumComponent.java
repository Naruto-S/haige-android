package com.music.player.haige.di.component;

import com.hc.core.di.component.AppComponent;
import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.di.module.AlbumModule;
import com.music.player.haige.mvp.ui.music.fragment.AlbumFragment;

import dagger.Component;

/**
 * ================================================
 * Created by huangcong on 2018/3/2.
 * ================================================
 */
@FragmentScope
@Component(modules = {AlbumModule.class}, dependencies = AppComponent.class)
public interface AlbumComponent {

    void inject(AlbumFragment fragment);
}
