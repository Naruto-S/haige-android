package com.music.player.haige.mvp.usecase;

import com.hc.core.mvp.UseCase;
import com.music.player.haige.app.Constants;
import com.music.player.haige.mvp.model.entity.music.Artist;
import com.music.player.haige.mvp.model.respository.interfaces.Repository;

import java.util.List;

import io.reactivex.Observable;


public class GetArtists extends UseCase<GetArtists.RequestValues, GetArtists.ResponseValue> {

    private final Repository mRepository;

    public GetArtists(Repository repository) {
        mRepository = repository;
    }

    @Override
    public ResponseValue execute(RequestValues requestValues) {
        String action = requestValues.getAction();
        switch (action) {
            case Constants.NAVIGATE_ALLSONG:
                return new ResponseValue(mRepository.getAllArtists());
            case Constants.NAVIGATE_PLAYLIST_RECENTADD:
                return new ResponseValue(mRepository.getRecentlyAddedArtists());
            case Constants.NAVIGATE_PLAYLIST_RECENTPLAY:
                return new ResponseValue(mRepository.getRecentlyPlayedArtist());
            case Constants.NAVIGATE_PLAYLIST_FAVORITE:
                return new ResponseValue(mRepository.getFavoriteArtist());
            default:
                throw new RuntimeException("wrong action type");
        }
    }


    public static final class RequestValues implements UseCase.RequestValues {

        private String action;

        public RequestValues(String action) {
            this.action = action;
        }

        public String getAction() {
            return action;
        }
    }

    public static final class ResponseValue implements UseCase.ResponseValue {

        private final Observable<List<Artist>> mListObservable;

        public ResponseValue(Observable<List<Artist>> listObservable) {
            mListObservable = listObservable;
        }

        public Observable<List<Artist>> getArtistList() {
            return mListObservable;
        }
    }
}
