package com.music.player.haige.di.module;

import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.DownloadModel;

import dagger.Module;
import dagger.Provides;

/**
 * ================================================
 * Created by huangcong on 2018/3/20.
 * ================================================
 */
@Module
public class DownloadModule {
    private MVContract.DownloadView view;

    public DownloadModule(MVContract.DownloadView view) {
        this.view = view;
    }

    @FragmentScope
    @Provides
    MVContract.DownloadView provideView(){
        return this.view;
    }

    @FragmentScope
    @Provides
    MVContract.DownloadModel provideModel(DownloadModel model){
        return model;
    }

}
