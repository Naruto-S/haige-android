package com.music.player.haige.app;

/**
 * ================================================
 * Created by huangcong on 2018/2/28.
 * ================================================
 */

public class Constants {

    public static final String URL_PRIVACY_POLICY = "https://haige.ipaizhao.cn/html/law";
    public static final String URL_PRIVACY_UPLOAD = "https://haige.ipaizhao.cn/html/law/upload";

    //https://haige.ipaizhao.cn/share?id=asdasdkasdjaksjd&gif=asdasda.gif
    public static final String URL_SHARE_MUSIC = "https://haige.ipaizhao.cn/share/share";

    public static final String BASE_API_URL_KUGOU = "http://lyrics.kugou.com/";

    public static final String MUSIC_COUNT_CHANGED = "com.music.player.haige.musiccountchanged";
    public static final String PLAYLIST_ITEM_MOVED = "com.music.player.haige.mmoved";
    public static final String PLAYLIST_COUNT_CHANGED = "com.music.player.haige.playlistcountchanged";
    public static final String CHANGE_THEME = "com.music.player.haige.themechange";
    public static final String EMPTY_LIST = "com.music.player.haige.emptyplaylist";
    public static final String PACKAGE = "com.music.player.haige";

    public static final String NAVIGATE_LIBRARY = "navigate_library";
    public static final String NAVIGATE_QUEUE = "navigate_queue";
    public static final String NAVIGATE_ALBUM = "navigate_album";
    public static final String NAVIGATE_ARTIST = "navigate_artist";

    public static final String NAVIGATE_ALLSONG = "navigate_all_song";
    public static final String NAVIGATE_PLAYLIST_RECENTPLAY = "navigate_playlist_recentplay";
    public static final String NAVIGATE_PLAYLIST_RECENTADD = "navigate_playlist_recentadd";
    public static final String NAVIGATE_PLAYLIST_TOPPLAYED = "navigate_playlist_topplayed";
    public static final String NAVIGATE_PLAYLIST_FAVORITE = "navigate_playlist_favourate";
    public static final String NAVIGATE_PLAYLIST_CURRENT = "navigate_playlist_current";
    public static final String NAVIGATE_PLAYLIST_RECOMMEND = "navigate_playlist_recommend";

    public static final String PLAYLIST_TYPE = "playlist_type";

    public static final String PLAY_MAIN_CURRENT_ITEM = "PLAY_MAIN_CURRENT_ITEM";
    public static final String PLAY_MAIN_IS_FROM_RECOMMEND = "PLAY_MAIN_IS_FROM_RECOMMEND";

    public static final String MUSIC_DOWNLOAD_DB_NAME = "music_download_db";
    public static final String MUSIC_FAVORITE_DB_NAME = "music_favorite_db";
    public static final String MUSIC_RECENT_DB_NAME = "music_recent_db";

    public static final String RANK_TYPE = "rank_type";

    public static final int DEFAULT_REQUEST_START = 0;
    public static final int DEFAULT_REQUEST_SIZE = 20;
    public static final int DEFAULT_REQUEST_COMMENT_SIZE = 100;
    public static final int DEFAULT_REQUEST_RECOMMEND_SIZE = 10;

    public static final String KEY_WORD = "key_word";

    public static final String SEARCH_TYPE = "search_type";

    public enum SEARCH {
        NET,
        LOCAL,
        RECENT
    }
}
