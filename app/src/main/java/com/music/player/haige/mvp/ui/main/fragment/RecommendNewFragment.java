package com.music.player.haige.mvp.ui.main.fragment;

import android.app.Activity;
import android.graphics.drawable.Animatable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.hc.core.di.component.AppComponent;
import com.hc.core.utils.CoreUtils;
import com.hc.core.utils.DeviceUtils;
import com.music.player.haige.R;
import com.music.player.haige.app.EventBusTags;
import com.music.player.haige.app.base.AppBaseFragment;
import com.music.player.haige.app.base.pref.DevicePrefHelper;
import com.music.player.haige.app.base.pref.UserPrefHelper;
import com.music.player.haige.app.base.recyclerview.view.MultiStateView;
import com.music.player.haige.app.base.recyclerview.view.NoNetworkView;
import com.music.player.haige.app.utils.launcher.ActivityLauncherStart;
import com.music.player.haige.app.utils.statistics.CommonStatistics;
import com.music.player.haige.app.utils.statistics.StatisticsConstant;
import com.music.player.haige.di.component.DaggerRecommendComponent;
import com.music.player.haige.di.module.RecommendModule;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.Api;
import com.music.player.haige.mvp.model.entity.action.CommentLikeRequest;
import com.music.player.haige.mvp.model.entity.comment.CommentBean;
import com.music.player.haige.mvp.model.entity.comment.CommentRequest;
import com.music.player.haige.mvp.model.entity.comment.CommentResponse;
import com.music.player.haige.mvp.model.entity.comment.SendCommentResponse;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.model.utils.LyricUtil;
import com.music.player.haige.mvp.model.utils.PreferencesUtility;
import com.music.player.haige.mvp.presenter.RecommendFPresenter;
import com.music.player.haige.mvp.ui.comment.CommentBottomSheetDialog;
import com.music.player.haige.mvp.ui.comment.adapter.CommentAdapter;
import com.music.player.haige.mvp.ui.login.LoginDialogFragment;
import com.music.player.haige.mvp.ui.main.adapter.RecommendNewAdapter;
import com.music.player.haige.mvp.ui.main.holder.RecommendHolder;
import com.music.player.haige.mvp.ui.main.utils.MusicLyricManager;
import com.music.player.haige.mvp.ui.main.widget.swiple.SwipeFlingView;
import com.music.player.haige.mvp.ui.music.service.MusicDataManager;
import com.music.player.haige.mvp.ui.music.service.MusicServiceConnection;
import com.music.player.haige.mvp.ui.setting.RateDialog;
import com.music.player.haige.mvp.ui.utils.DebugLogger;
import com.music.player.haige.mvp.ui.utils.NetworkUtils;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;
import com.music.player.haige.mvp.ui.utils.Utils;
import com.music.player.haige.mvp.ui.widget.NoDoubleClickListener;
import com.music.player.haige.mvp.ui.widget.dialog.SystemDialog;
import com.music.player.haige.mvp.ui.widget.emoji.EditCommentView;

import org.simple.eventbus.Subscriber;

import java.io.File;
import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

import static com.music.player.haige.app.Constants.DEFAULT_REQUEST_COMMENT_SIZE;
import static com.music.player.haige.app.utils.statistics.StatisticsConstant.COMPLETE_CLICk;
import static com.music.player.haige.app.utils.statistics.StatisticsConstant.NO_LIKE_CLICK;
import static com.music.player.haige.app.utils.statistics.StatisticsConstant.RECOMMEND_LIKE_CLICK;
import static com.music.player.haige.app.utils.statistics.StatisticsConstant.REFRESH_CLICK;

/**
 * ================================================
 * Created by huangcong on 2018/2/27.
 * <p>
 * 推荐页
 * ================================================
 */
public class RecommendNewFragment extends AppBaseFragment<RecommendFPresenter> implements MVContract.RecommendView,
        View.OnClickListener, SwipeFlingView.OnSwipeFlingListener, SwipeFlingView.OnItemClickListener,
        EditCommentView.OnEditCommentListener {

    @BindView(R.id.multiStateView)
    MultiStateView mMultiStateView;

    @BindView(R.id.swipe_fling_view)
    SwipeFlingView mSwipeFlingView;

    @BindView(R.id.layout_recommend_refresh)
    View mRefreshLayout;

    @BindView(R.id.layout_recommend_dislike)
    View mDislikeLayout;

    @BindView(R.id.layout_recommend_like)
    View mLikeLayout;

    @BindView(R.id.layout_recommend_all)
    View mAllSongLayout;

    private int currentPage;
    private long mCurrentSongId = 0;
    private boolean isLikeSwipe = false;

    private CommentBottomSheetDialog mCommentSheetDialog;
    private RecommendNewAdapter mAdapter;

    private ArrayList<CommentBean> mCommentList = new ArrayList<>();

    public static RecommendNewFragment newInstance() {
        RecommendNewFragment fragment = new RecommendNewFragment();
        return fragment;
    }

    @Override
    public void setupFragmentComponent(AppComponent appComponent) {
        DaggerRecommendComponent
                .builder()
                .appComponent(appComponent)
                .recommendModule(new RecommendModule(this))
                .build()
                .inject(this);
    }

    @Override
    public View initView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.app_fragment_recommend_new, container, false);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        Timber.w("===>>> setUserVisibleHint isVisibleToUser:" + isVisibleToUser);
    }

    @Override
    public void initData(Bundle savedInstanceState) {
        initMultiStateView();

        mRefreshLayout.setOnClickListener(this);
        mDislikeLayout.setOnClickListener(this);
        mLikeLayout.setOnClickListener(this);
        mAllSongLayout.setOnClickListener(this);

        mSwipeFlingView.setOnSwipeFlingListener(this);
        mSwipeFlingView.setOnItemClickListener(this);

        mAdapter = new RecommendNewAdapter();
        mAdapter.setViewClickListener(this);
        mSwipeFlingView.setAdapter(mAdapter);

        mCommentSheetDialog = new CommentBottomSheetDialog(getActivity());
        mCommentSheetDialog.setCommentActionListener(new CommentAdapter.onCommentActionListener() {
            @Override
            public void onDelete(CommentRequest request) {
                mPresenter.deleteComment(request);
            }

            @Override
            public void onLike(CommentLikeRequest request) {
                mPresenter.commentLike(request);
            }

            @Override
            public void onClickAllSub(String commentId, int position) {
                mPresenter.getCommentSub(commentId, 1, 100, position);
            }
        });

        if (Utils.isNotNull(mCommentSheetDialog.getEditCommentView())) {
            mCommentSheetDialog.getEditCommentView().setEditCommentListener(this);
        }

        reLoadData();
    }

    private void initMultiStateView() {
        ((NoNetworkView) mMultiStateView.getView(MultiStateView.VIEW_STATE_ERROR))
                .setOnRefreshNetwork(() -> {
                    if (NetworkUtils.isConnected(getContext())) {
                        reLoadData();
                    } else {
                        Toast.makeText(getContext(), R.string.app_net_no_connect_tip, Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public synchronized void fetchPusherList(boolean isRefresh) {
        if (!NetworkUtils.isConnected(getContext())) {
            loadRequestCompleted();
            showErrorNetwork();
            return;
        }
        loadRequestStarted();

        if (isRefresh || mAdapter.isEmpty()) {
            currentPage = 1;
        } else {
            currentPage++;
        }
        DebugLogger.d("fetchPusherList === ", "Fetch LiveRoomList[" + currentPage + "]...");
        mPresenter.loadRecommendMusic(currentPage, 10, isRefresh);
    }

    private void reLoadData() {
        fetchPusherList(true);
    }

    private void loadMoreData() {
        fetchPusherList(false);
    }

    @Override
    public void onDestroy() {
        if (Utils.isNotNull(mAdapter)) {
            mAdapter.clear();
        }
        MusicLyricManager.getInstance().clear();
        mPresenter.onStopUpateMusicTime();
        super.onDestroy();
    }

    @Override
    public void loadRequestStarted() {
        if (mAdapter.getCount() <= 0) {
            mMultiStateView.setViewState(MultiStateView.VIEW_STATE_LOADING);
        } else {
            mMultiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);
        }

    }

    @Override
    public void showErrorNetwork() {
        if (mAdapter.getCount() > 0) {
            mMultiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);
            Toast.makeText(getContext(), R.string.app_net_no_connect_tip, Toast.LENGTH_SHORT).show();
        } else {
            mMultiStateView.setViewState(MultiStateView.VIEW_STATE_ERROR);
        }
    }

    @Override
    public void showLoadingError() {
        if (mAdapter.getCount() <= 0) {
            mMultiStateView.setViewState(MultiStateView.VIEW_STATE_EMPTY);
        } else {
            mMultiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);
        }

    }

    @Override
    public void showNoMoreView() {

    }

    @Override
    public void hideLoadingView() {

    }

    @Override
    public void loadRequestCompleted() {
        if (mAdapter.getCount() > 0) {
            mMultiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);
        } else {
            mMultiStateView.setViewState(MultiStateView.VIEW_STATE_EMPTY);
        }

    }

    @Override
    public void loadEnded() {

    }

    @Override
    public void showRefresh(ArrayList arrayList) {
        mAdapter.setSongs(arrayList);

        startMusicTop();
    }

    @Override
    public void showLoadMore(ArrayList arrayList) {
        mAdapter.addSongs(arrayList);
    }

    @Override
    public void onItemClicked(int itemPosition, Object dataObject) {
        gotoMusicComplete();
    }

    @Override
    public void onStartDragCard() {
    }

    @Override
    public boolean canLeftCardExit() {
        return true;
    }

    @Override
    public boolean canRightCardExit() {
        return true;
    }

    @Override
    public void onPreCardExit() {
    }

    @Override
    public void onLeftCardExit(View view, Object dataObject, boolean triggerByTouchMove) {
        mAdapter.remove(0);

        MusicLyricManager.getInstance().clear();
        playNext();
    }

    @Override
    public void onRightCardExit(View view, Object dataObject, boolean triggerByTouchMove) {
        if (isLikeSwipe) {
            isLikeSwipe = false;

            mAdapter.remove(0);

            MusicLyricManager.getInstance().clear();
        } else {
            liked(mAdapter.getItem(0));
            mAdapter.remove(0);

            MusicLyricManager.getInstance().clear();
            playNext();
        }
    }

    @Override
    public void onSuperLike(View view, Object dataObject, boolean triggerByTouchMove) {
    }

    @Override
    public void onTopCardViewFinish() {
    }

    @Override
    public void onAdapterAboutToEmpty(int itemsInAdapter) {
        loadMoreData();
    }

    @Override
    public void onAdapterEmpty() {
    }

    @Override
    public void onScroll(View selectedView, float scrollXProgress) {
        if (scrollXProgress <= 0 && Utils.isNotNull(getAnimDislikeIv())) {
            getAnimDislikeIv().setAlpha(Math.abs(scrollXProgress));
        }

        if (scrollXProgress >= 0 && Utils.isNotNull(getAnimlikeIv())) {
            getAnimlikeIv().setAlpha(Math.abs(scrollXProgress));
        }
    }

    @Override
    public void onEndDragCard() {

    }

    @Override
    public void onSuccess(Api.Action action, Object o) {
        super.onSuccess(action, o);
        switch (action) {
            case GET_MUSIC_COMMENT:
                CommentResponse response = (CommentResponse) o;
                if (Utils.isNotNull(response) && Utils.isNotEmptyCollection(response.getData())) {
                    mCommentList.clear();
                    mCommentList.addAll(response.getData());
                    mCommentSheetDialog.setCommentList(mCommentList);
                } else {
                    mCommentList.clear();
                    mCommentSheetDialog.setCommentList(mCommentList);
                }
                break;
            case GET_COMMENT_SUB:
                if (Utils.isNotNull(mCommentSheetDialog)) {
                    CommentResponse subResponse = (CommentResponse) o;
                    int position = subResponse.getPosition();
                    CommentBean oldComment = mCommentSheetDialog.getCommentItem(position);
                    oldComment.setSubComments(subResponse.getData());

                    mCommentList.set(position, oldComment);
                    mCommentSheetDialog.setCommentList(mCommentList);
                }
            case MUSIC_LRC:
                MusicLyricManager.getInstance().clear();
                MusicLyricManager.getInstance().setLyricFile((File) o, "UTF-8");
                DebugLogger.e("有歌词");
                break;
            case SEND_MUSIC_COMMENT:
                try {
                    SendCommentResponse sendCommentResponse = (SendCommentResponse) o;
                    CommentRequest request = sendCommentResponse.getRequest();
                    request.setCommentId(sendCommentResponse.getData());
                    Toast.makeText(getActivity(), R.string.comment_send_success, Toast.LENGTH_SHORT).show();
                } catch (Exception e) {

                }
                break;
            case DELETE_COMMENT:
                if (Utils.isNotNull(mCommentSheetDialog)) {
                    mCommentList.clear();
                    mCommentList.addAll(mCommentSheetDialog.getCommentList());
                    mCommentSheetDialog.setCommentCount();
                    if (Utils.isNotNull(getCommentCountTv())) {
                        getCommentCountTv().setText(ResourceUtils.resourceString(R.string.recommend_comment, mCommentList.size()));
                    }
                }
                Toast.makeText(getActivity(), R.string.comment_delete_success, Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void onError(Api.Action action, String code, String message) {
        super.onError(action, code, message);

        switch (action) {
            case MUSIC_LRC:
                DebugLogger.e("歌词拉取失败：" + code + " -- " + message);
                break;
        }
    }

    @Override
    public void showMessage(String message) {
        super.showMessage(message);
        CoreUtils.makeText(getContext(), message);
    }

    private RecommendHolder getTopHolder() {
        if (Utils.isNull(mSwipeFlingView)) return null;

        View view = mSwipeFlingView.getSelectedView();
        if (Utils.isNotNull(view)) {
            return (RecommendHolder) view.getTag();
        } else {
            return null;
        }
    }

    private TextView getMusicLyricTv() {
        RecommendHolder holder = getTopHolder();
        if (Utils.isNotNull(holder)) {
            return holder.mMusicLyricTv;
        } else {
            return null;
        }
    }

    private TextView getCommentCountTv() {
        RecommendHolder holder = getTopHolder();
        if (Utils.isNotNull(holder)) {
            return holder.mRecommendCommentTv;
        } else {
            return null;
        }
    }

    private ProgressBar getPlayLoading() {
        RecommendHolder holder = getTopHolder();
        if (Utils.isNotNull(holder)) {
            return holder.mPlayLoading;
        } else {
            return null;
        }
    }

    private ImageView getPlayIv() {
        RecommendHolder holder = getTopHolder();
        if (Utils.isNotNull(holder)) {
            return holder.mPlayIv;
        } else {
            return null;
        }
    }

    private ImageView getAnimDislikeIv() {
        RecommendHolder holder = getTopHolder();
        if (Utils.isNotNull(holder)) {
            return holder.mAnimDislikeIv;
        } else {
            return null;
        }
    }

    private ImageView getAnimlikeIv() {
        RecommendHolder holder = getTopHolder();
        if (Utils.isNotNull(holder)) {
            return holder.mAnimlikeIv;
        } else {
            return null;
        }
    }

    private void updatePlayState(int position){
        RecommendHolder holder = getTopHolder();
        if (Utils.isNotNull(holder)) {
            if (Utils.isNotNull(holder.mMusicWebpBgSdv.getController())) {
                Animatable animatable = holder.mMusicWebpBgSdv.getController().getAnimatable();
                if (MusicServiceConnection.isShort() && MusicServiceConnection.isPlaying() && MusicServiceConnection.getCurrentAudioId() == mAdapter.getItem(position).songId) {
                    holder.mPlayIv.setImageResource(R.drawable.ic_recommend_suspend);
                    if (animatable != null && !animatable.isRunning()) {
                        animatable.start();
                    }
                } else {
                    holder.mPlayIv.setImageResource(R.drawable.ic_recommend_play);
                    if (animatable != null && animatable.isRunning()) {
                        animatable.stop();
                    }
                }
            }
        }
    }

    private void updateProgress(int position) {
        try {
            RecommendHolder holder = getTopHolder();
            if (Utils.isNotNull(holder)) {
                updatePlayState(position);

                if (MusicServiceConnection.isShort() && MusicServiceConnection.getCurrentAudioId() == mAdapter.getItem(position).getSongId()) {
                    long progress = MusicServiceConnection.position();
                    if (holder.mProgressBar.getMax() <= 0) {
                        long duration = MusicServiceConnection.duration();
                        Timber.e("===>>> mUpdateProgress duration:" + duration);
                        holder.mProgressBar.setMax((int) MusicServiceConnection.duration());
                    }

                    holder.mProgressBar.setProgress((int) progress);
                    Song song = mAdapter.getItem(0);
                    long curr = song.startAt * 1000 + progress - 600;
                    String content = MusicLyricManager.getInstance().getMusicLyricHint(curr);
                    holder.mMusicLyricTv.setText(content);
                }
            }
        } catch (Exception e) {
            DebugLogger.e(TAG, "mUpdateProgress：" + e);
        }
    }

    @Override
    public void onUpateMusicTime() {
        updateProgress(0);
    }

    @Subscriber
    public void onMetaChangedEvent(EventBusTags.MetaChangedEvent event) {
        Timber.e("===>>> onMetaChangedEvent " + event);

        try {
            if (event.getSongId() == mAdapter.getItem(0).getSongId()) {
                updatePlayState(0);

                Song song = mAdapter.getItem(0);
                if (mCurrentSongId != event.getSongId()) {
                    mCurrentSongId = event.getSongId();

                    DebugLogger.e("onMetaChangedEvent", "切歌 拉取评论 == ： " + event);

                    mPresenter.getMusicComment(song.getSongId(), DEFAULT_REQUEST_COMMENT_SIZE, "");

                    MusicLyricManager.getInstance().clear();
                    getMusicLyricTv().setText("");
                    DebugLogger.e("onMetaChangedEvent", "歌词: " + song.musicName + "   *** realName *** ：" + song.realName);
                    String musicName = Utils.isNotEmptyString(song.realName) ? song.realName : song.musicName;
                    String musicAuthors = Utils.isNotEmptyCollection(song.realAuthors) ? song.realAuthors.get(0) : song.artistName;
                    DebugLogger.e("onMetaChangedEvent", "歌词: name -- " + musicName + "   authors -- " + musicAuthors);
                    if (Utils.isNotEmptyString(song.getLrc())) {
                        Observable.create((ObservableOnSubscribe<File>)
                                emitter -> {
                                    File file = null;
                                    try {
                                        String rawLyric = LyricUtil.decryptBASE64(song.getLrc());
                                        file = LyricUtil.writeLrcToLoc(musicName, musicAuthors, rawLyric);
                                        if (Utils.isNull(file)) {
                                            file = new File("嗨歌歌词");
                                        }
                                    } catch (Throwable throwable) {
                                        DebugLogger.e(throwable);
                                        if (Utils.isNull(file)) {
                                            file = new File("嗨歌歌词");
                                        }
                                    }
                                    emitter.onNext(file);
                                    emitter.onComplete();
                                })
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(file -> {
                                    if (Utils.isNotNull(file) && file.exists()){
                                        onSuccess(Api.Action.MUSIC_LRC, file);
                                    } else {
                                        mPresenter.getLyricFile(musicName, musicAuthors, MusicServiceConnection.duration());
                                    }
                                });
                    } else {
                        mPresenter.getLyricFile(musicName, musicAuthors, MusicServiceConnection.duration());
                    }
                }
                repeatModeChanged();
            } else {
                updatePlayState(0);
            }
        } catch (Exception e) {
            DebugLogger.e(e);
        }
    }

    @Subscriber
    public void onMusicIsLoadingEvent(EventBusTags.MusicIsLoadingEvent event) {
        Timber.e("===>>> onMusicIsLoadingEvent isLoading:" + event.isLoading());

        if (event.isLoading()) {
            Objects.requireNonNull(getPlayLoading()).setVisibility(View.VISIBLE);
        } else {
            Objects.requireNonNull(getPlayLoading()).setVisibility(View.INVISIBLE);
        }
    }

    @Subscriber
    public void onRepeatModeChangedEvent(EventBusTags.RepeatModeChangedEvent event) {
        repeatModeChanged();
    }

    @Subscriber
    public void onSubscriber(Song song) {
        if (Utils.isNotNull(mSwipeFlingView)) {
            isLikeSwipe = true;
            mSwipeFlingView.selectRight(false);
        }
    }

    private void repeatModeChanged() {

    }

    private void startMusicTop() {
        if (PreferencesUtility.getInstance(getContext()).is4GAutoPlay()
                || DeviceUtils.getNetworkType(getContext()) == DeviceUtils.NETTYPE_WIFI) {
            playMusic();
        } else {
            new SystemDialog().create(getChildFragmentManager())
                    .setContent(ResourceUtils.resourceString(R.string.app_4G_tip))
                    .setOk(ResourceUtils.resourceString(R.string.app_4G_tip_go_on))
                    .setCancle(ResourceUtils.resourceString(R.string.app_4G_tip_pause))
                    .setOnOkListener(() -> {
                        playMusic();

                        MusicServiceConnection.setShuffleMode(MusicDataManager.SHUFFLE_NONE);
                        MusicServiceConnection.setRepeatMode(MusicDataManager.REPEAT_CURRENT);
                    })
                    .setOnCancelListener(() -> {})
                    .show();
        }

        mPresenter.startUpateMusicTime();
    }

    /**
     * 播放音乐
     */
    private void playMusic() {
        if (Utils.isNotEmptyCollection(mAdapter.getSongs())) {
            if (MusicServiceConnection.isShort()) {
                MusicServiceConnection.play(mAdapter.getItem(0), 0, false, true);
            } else {
                MusicServiceConnection.open(mAdapter.getSongs(), 0, false, true);
            }
        }

        MusicServiceConnection.setShuffleMode(MusicDataManager.SHUFFLE_NONE);
        MusicServiceConnection.setRepeatMode(MusicDataManager.REPEAT_CURRENT);
    }

    private void playNext() {
        playMusic();
    }

    private void playPause() {
        try {
            long songId = mAdapter.getItem(0).getSongId();
            if (MusicServiceConnection.getCurrentAudioId() == songId && MusicServiceConnection.isPlaying()) {
                MusicServiceConnection.playOrPause();
            } else {
                playMusic();
            }
        } catch (Exception e) {
            DebugLogger.e(e);
        }
    }

    @Override
    public void onClick(View v) {
        if (NoDoubleClickListener.isFastClick()) {
            return;
        }

        Song song = mAdapter.getItem(0);

        switch (v.getId()) {
            case R.id.iv_recommend_play:
                playPause();
                break;
            case R.id.layout_recommend_refresh:
                CommonStatistics.sendEvent(REFRESH_CLICK);
                mAdapter.clear();
                reLoadData();
                break;
            case R.id.layout_recommend_dislike:
                mSwipeFlingView.selectLeft(false);
                CommonStatistics.sendEvent(NO_LIKE_CLICK);
                break;
            case R.id.layout_recommend_like:
                if (UserPrefHelper.isLogin()) {
                    mSwipeFlingView.selectRight(false);
                }  else {
                    LoginDialogFragment.newInstance().show(getChildFragmentManager(), LoginDialogFragment.class.getName());
                }
                CommonStatistics.sendEvent(RECOMMEND_LIKE_CLICK);
                break;
            case R.id.layout_recommend_all:
                CommonStatistics.sendEvent(COMPLETE_CLICk);
                gotoMusicComplete();
                break;
            case R.id.app_layout_music_operate_cover_iv:
                if (song != null && song.getUser() != null) {
                    CommonStatistics.sendEvent(StatisticsConstant.HEAD_CLICK);
                    ActivityLauncherStart.startOtherUser((Activity) getContext(), song.getUser());
                }
                break;
            case R.id.tv_recommend_comment:
                CommonStatistics.sendEvent(StatisticsConstant.COMMENT_CLICK);

                if (UserPrefHelper.isLogin()) {
                    mCommentSheetDialog.show();
                }  else {
                    LoginDialogFragment.newInstance().show(getChildFragmentManager(), LoginDialogFragment.class.getName());
                }
                break;
            case R.id.tv_recommend_tag:
                if (Utils.isNotNull(song) && Utils.isNotEmptyCollection(song.getTags())) {
                    CommonStatistics.sendEvent(StatisticsConstant.LABEL_CLICK);
                    String tag = song.getTags().get(0);
                    ActivityLauncherStart.startTagMusic(getActivity(), tag);
                }
                break;
        }
    }

    @Override
    public void onSend(CommentRequest request) {
        DebugLogger.e(TAG, "评论：" + request.getContext() + "  " + MusicServiceConnection.position());

        request.setMusicId(MusicServiceConnection.getCurrentAudioId());
        request.setPostAt((MusicServiceConnection.position() / 1000) + 1);
        mPresenter.sendMusicComment(request);
    }

    private void gotoMusicComplete() {
        ActivityLauncherStart.startMusicComplete(getActivity(), mAdapter.getItem(0));
    }

    public void liked(Song song) {
        CommonStatistics.sendEvent(StatisticsConstant.LIKE_CLICK);

        try {
            mPresenter.countLike(song);
            mPresenter.recordFavoriteSong(song);
        } catch (Exception e) {
            Timber.e(e);
        }

        showRateDialog();
    }

    private void showRateDialog() {
        long time = System.currentTimeMillis();
        boolean isShowRate = time >= DevicePrefHelper.getNextShowRateTime()
                && DevicePrefHelper.getNextShowRateTime() != 0
                && DevicePrefHelper.isShowRateDialog();

        if (isShowRate) {
            RateDialog.create(getChildFragmentManager()).show();
        }
    }
}
