package com.music.player.haige.mvp.presenter;

import com.liulishuo.filedownloader.FileDownloader;
import com.liulishuo.filedownloader.model.FileDownloadStatus;
import com.liulishuo.filedownloader.util.FileDownloadUtils;
import com.music.player.haige.app.GlobalConfiguration;
import com.music.player.haige.mvp.contract.AppBasePresenter;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.Api;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.model.entity.music.SongDao;
import com.music.player.haige.mvp.ui.music.utils.MusicUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import me.jessyan.rxerrorhandler.handler.ErrorHandleSubscriber;

/**
 * ================================================
 * Created by huangcong on 2018/3/20.
 * ================================================
 */

public class DownloadFinishFPresenter extends AppBasePresenter<MVContract.DownloadFinishModel, MVContract.DownloadFinishView> {

    private SongDao mDownloadSongDao;              // 音乐下载记录数据库

    @Inject
    public DownloadFinishFPresenter(MVContract.DownloadFinishModel model, MVContract.DownloadFinishView view) {
        super(model, view);
        mDownloadSongDao = GlobalConfiguration.sDownloadDaoSession.getSongDao();
    }

    public void loadDownloadFinishMusic() {
        buildObservable(Observable.just(mDownloadSongDao.queryBuilder().orderDesc(SongDao.Properties.DownloadDate).list()))
                .subscribe(new ErrorHandleSubscriber<List<Song>>(mErrorHandler) {
                    @Override
                    public void onNext(List<Song> songs) {
                        ArrayList<Song> downloadSongs = new ArrayList<>();
                        for (Song song : songs) {
                            String url = song.musicPath;
                            String path = MusicUtils.getDownloadPath(song);
                            int id = FileDownloadUtils.generateId(url, path);
                            int status = FileDownloader.getImpl().getStatus(id, path);
                            if (status == FileDownloadStatus.completed
                                    || new File(path).exists()) {
                                downloadSongs.add(song);
                            }
                        }
                        if (downloadSongs.size() > 0) {
                            mRootView.onSuccess(Api.Action.DOWNLOAD_FINISH, downloadSongs);
                        } else {
                            mRootView.onError(Api.Action.DOWNLOAD_FINISH, Api.State.NORMAL.getCode(), Api.State.NORMAL.getMsg());
                        }
                    }
                });
    }

    public void deleteDownloadedMusic(Song song){
        mDownloadSongDao.delete(song);
    }
}
