package com.music.player.haige.di.module;

import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.UpdateModel;

import dagger.Module;
import dagger.Provides;

/**
 * ================================================
 * Created by huangcong on 2018/3/3.
 * ================================================
 */
@Module
public class UpdateModule {
    private MVContract.UpdateView view;

    public UpdateModule(MVContract.UpdateView view){
        this.view = view;
    }

    @FragmentScope
    @Provides
    MVContract.UpdateView provideView(){
        return view;
    }

    @FragmentScope
    @Provides
    MVContract.UpdateModel provideModel(UpdateModel model){
        return model;
    }

}
