package com.music.player.haige.mvp.ui.main.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.music.player.haige.R;
import com.music.player.haige.app.image.widget.HaigeImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecommendHolder {

    @BindView(R.id.hiv_music_webp_bg)
    public HaigeImageView mMusicWebpBgSdv;

    @BindView(R.id.iv_recommend_play)
    public ImageView mPlayIv;

    @BindView(R.id.tv_recommend_like)
    public TextView mRecommendLikeTv;

    @BindView(R.id.tv_recommend_comment)
    public TextView mRecommendCommentTv;

    @BindView(R.id.tv_recommend_tag)
    public TextView mRecommendTagTv;

    @BindView(R.id.hiv_recommend_avatar)
    public HaigeImageView mAvatarTv;

    @BindView(R.id.tv_music_name)
    public TextView mMusicNameTv;

    @BindView(R.id.tv_music_author)
    public TextView mMusicAuthorTv;

    @BindView(R.id.progressBar)
    public ProgressBar mProgressBar;

    @BindView(R.id.tv_recommend_lyric)
    public TextView mMusicLyricTv;

    @BindView(R.id.iv_anim_dislike)
    public ImageView mAnimDislikeIv;

    @BindView(R.id.iv_anim_like)
    public ImageView mAnimlikeIv;

    @BindView(R.id.play_loading)
    public ProgressBar mPlayLoading;

    public RecommendHolder(View itemView) {
        ButterKnife.bind(this, itemView);
    }
}
