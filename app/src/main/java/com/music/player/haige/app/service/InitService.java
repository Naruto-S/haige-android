package com.music.player.haige.app.service;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;

import com.music.player.haige.BuildConfig;
import com.music.player.haige.app.HaigeApplication;
import com.music.player.haige.app.utils.statistics.CommonStatistics;
import com.music.player.haige.app.utils.statistics.StatisticsConstant;
import com.music.player.haige.mvp.ui.utils.DebugLogger;

import java.util.concurrent.TimeUnit;


/**
 * Created by john on 2018/3/7.
 */

public class InitService extends IntentService {
    public static final String ACTION_ADD_SHORTCUT = "com.android.launcher.action.INSTALL_SHORTCUT";
    private static final String TAG = InitService.class.getSimpleName();

    private static final String ACTION_INIT_WHEN_APP_CREATE = BuildConfig.APPLICATION_ID + ".service.action.INIT";

    public InitService() {
        super("InitializeService");
    }

    public static void start(Context context) {
        try {
            Intent intent = new Intent(context, InitService.class);
            intent.setAction(ACTION_INIT_WHEN_APP_CREATE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(intent);
            }
            context.startService(intent);
        } catch (Exception e) {
            //BugFix:java.lang.IllegalStateException: Not allowed to start service Intent
        }
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_INIT_WHEN_APP_CREATE.equals(action)) {
                performInit();
            }
        }
    }

    private void performInit() {
        initLocal();
        //埋点
        sentOpenEvent();
        initService();
    }

    private void initLocal() {
    }

    private void initService() {
        LocalService.start(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            doService();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void doService() {
        try {
            JobScheduler jobScheduler = (JobScheduler) HaigeApplication.getInstance().getSystemService(JOB_SCHEDULER_SERVICE);
            @SuppressLint("JobSchedulerService")
            JobInfo.Builder builder = new JobInfo.Builder(1, new ComponentName(HaigeApplication.getInstance(), AlarmJobService.class));  //指定哪个JobService执行操作
            builder.setMinimumLatency(TimeUnit.MILLISECONDS.toMillis(60 * 1000)); //执行的最小延迟时间
            builder.setOverrideDeadline(TimeUnit.MILLISECONDS.toMillis(5 * 60 * 1000));  //执行的最长延时时间
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_NOT_ROAMING);  //非漫游网络状态
            }
            builder.setBackoffCriteria(TimeUnit.MINUTES.toMillis(10), JobInfo.BACKOFF_POLICY_LINEAR);  //线性重试方案
            if (jobScheduler != null) {
                jobScheduler.schedule(builder.build());
            }
        } catch (Exception e) {
            DebugLogger.e(e);
        }
    }

    private void sentOpenEvent() {
        CommonStatistics.sendEvent(StatisticsConstant.OPEN_APP);
    }

}
