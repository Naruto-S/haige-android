package com.music.player.haige.app.image.utils;

import com.facebook.drawee.drawable.ScalingUtils;
import com.music.player.haige.app.image.release.DisplayOptions;

/**
 * Created by liumingkong on 2017/6/19.
 */

public class ImageOptionsUtils {

    public static DisplayOptions.Builder buildDisplay(int resLoading, int resFail) {
        return buildDisplay(resLoading, resFail, ScalingUtils.ScaleType.CENTER_CROP);
    }

    public static DisplayOptions.Builder buildDisplay(int resLoading, int resFail, ScalingUtils.ScaleType scaleType) {
        return buildDisplay(resLoading, resFail, scaleType, false);
    }

    public static DisplayOptions.Builder buildDisplay(int resLoading, int resFail, ScalingUtils.ScaleType scaleType
            , boolean asCircle) {
        return buildDisplay(resLoading, resFail, scaleType, asCircle, 0);
    }

    public static DisplayOptions.Builder buildDisplay(int resLoading, int resFail, ScalingUtils.ScaleType scaleType
            , boolean asCircle, int borderWidth) {
        return buildDisplay(resLoading, resFail, scaleType, asCircle, borderWidth, 0);
    }

    public static DisplayOptions.Builder buildDisplay(int resLoading, int resFail, ScalingUtils.ScaleType scaleType
            , boolean asCircle, int borderWidth, int borderColor) {
        return buildDisplay(resLoading, resFail, scaleType, asCircle, borderWidth, borderColor, null);
    }

    public static DisplayOptions.Builder buildDisplay(int resLoading, int resFail, ScalingUtils.ScaleType scaleType
            , boolean asCircle, int borderWidth, int borderColor, int[] roundRadii) {
        return new DisplayOptions.Builder()
                .showImageOnLoading(resLoading)
                .showCircleRadius(asCircle)
                .showImageScaleType(scaleType)
                .showImageOnFail(resFail)
                .showRoundRadii(roundRadii)
                .showBorderWidth(borderWidth)
                .showBorderColor(borderColor);
    }
}
