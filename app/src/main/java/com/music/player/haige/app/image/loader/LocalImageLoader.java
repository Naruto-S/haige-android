package com.music.player.haige.app.image.loader;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.widget.ImageView;

import com.music.player.haige.app.HaigeApplication;
import com.music.player.haige.app.image.loader.listener.OnImageLoaderListener;
import com.music.player.haige.app.image.release.DisplayOptions;
import com.music.player.haige.app.image.widget.ImageFetcher;
import com.music.player.haige.mvp.ui.utils.DebugLogger;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;
import com.music.player.haige.mvp.ui.utils.Utils;

/**
 * Created by liumingkong on 15/7/13.
 */
public class LocalImageLoader extends BaseImageLoader {

    /**
     * 给view设置新背景，并回收旧的背景图片<br>
     * <font color=red>注意：需要确定以前的背景不被使用</font>
     *
     * @param v
     */
    @SuppressWarnings("deprecation")
    public static void setAndRecycleBackground(View v, int resID) {
        try {
            // 获得ImageView当前显示的图片
            Bitmap bitmap1 = null;
            if (v.getBackground() != null) {
                try {
                    //若是可转成bitmap的背景，手动回收
                    bitmap1 = ((BitmapDrawable) v.getBackground()).getBitmap();
                } catch (ClassCastException e) {
                    //若无法转成bitmap，则解除引用，确保能被系统GC回收
                    v.getBackground().setCallback(null);
                }
            }
            // 根据原始位图和Matrix创建新的图片
            v.setBackgroundResource(resID);
            // bitmap1确认即将不再使用，强制回收，这也是我们经常忽略的地方
            if (bitmap1 != null && !bitmap1.isRecycled()) {
                bitmap1.recycle();
            }
        } catch (Throwable throwable) {

        }
    }

    //加载资源文件，避免oom
    public static Bitmap decodeResource(int resId) {
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 1;
            options.inJustDecodeBounds = true;
            final Resources resources = ResourceUtils.getResources();
            BitmapFactory.decodeResource(resources, resId, options);
            int bitmapWidth = options.outWidth;
            int bitmapHeight = options.outHeight;

            //内存检测
            Runtime.getRuntime().gc();
            long totalMemory = Runtime.getRuntime().maxMemory();
            long allowedMemoryToUse = totalMemory / 8;
            int maximumAreaPossibleAccordingToAvailableMemory = (int) (allowedMemoryToUse / 4);

            //可用内存
            final int maxArea = Math.min(bitmapWidth * bitmapHeight, maximumAreaPossibleAccordingToAvailableMemory);
            //需要内存
            int resultArea = bitmapWidth * bitmapHeight;
            final int rawBW = bitmapWidth;
            final int rawBH = bitmapHeight;
            int sizeW = bitmapWidth, sizeH = bitmapHeight;
            while (resultArea >= maxArea) {
                options.inSampleSize *= 2;

                sizeW = rawBW / options.inSampleSize;
                sizeH = rawBH / options.inSampleSize;

                resultArea = sizeW * sizeH;
            }
            options.inJustDecodeBounds = false;
            return BitmapFactory.decodeResource(resources, resId, options);
        } catch (Throwable e) {
            DebugLogger.e("LocalImageLoader:decodeResource-", e);
        }
        return null;
    }

    //fresco 加载资源文件
    public static void displayResImage(int resId, ImageFetcher imageFetcher) {
        try {
            if (Utils.ensureNotNull(imageFetcher)) {
                imageFetcher.setImageURI("res:///" + resId);
            }
        } catch (Throwable throwable) {
            DebugLogger.e(throwable);
        }
    }

    public static void displayResImage(int resId, DisplayOptions.Builder displayOptions, ImageFetcher imageFetcher, OnImageLoaderListener onImageLoaderListener) {
        try {
            if (Utils.ensureNotNull(imageFetcher)) {
                imageFetcher.setImageURI("res:///" + resId, displayOptions.build(), onImageLoaderListener);
            }
        } catch (Throwable throwable) {
            DebugLogger.e(throwable);
        }
    }

    public static void displayAssetImage(String uri, ImageFetcher imageFetcher) {
        try {
            if (Utils.ensureNotNull(imageFetcher)) {
                imageFetcher.setImageURI("asset:///" + uri);
            }
        } catch (Throwable throwable) {
            DebugLogger.e(throwable);
        }
    }

    public static void displayLocalImage(String uri, ImageFetcher imageFetcher) {
        try {
            if (Utils.ensureNotNull(imageFetcher)) {
                imageFetcher.setImageURI("file://" + uri);
            }
        } catch (Throwable throwable) {
            DebugLogger.e(throwable);
        }
    }

    // 安全记在res的图片
    public static void safeSetImageViewRes(ImageView imageView, int resId) {
        try {
            if (!Utils.isNull(imageView)) {
                imageView.setImageResource(resId);
            }
        } catch (Throwable e) {
            DebugLogger.e(e);
        }
    }

    public static void safeSetViewBackgroundRes(View view, int resId) {
        try {
            if (!Utils.isNull(view)) {
                view.setBackgroundResource(resId);
            }
        } catch (Throwable e) {
            DebugLogger.e(e);
        }
    }

    public static void safeSetViewBackgroundResColor(View view, int color) {
        try {
            if (!Utils.isNull(view)) {
                view.setBackgroundColor(color);
            }
        } catch (Throwable e) {
            DebugLogger.e(e);
        }
    }

    // 安全加载 资源图片并放入ImageView
    public static Bitmap safeSetResImage(ImageView imageView, int res) {
        Bitmap bgBitmap = safeResBitmap(res);
        safeSetImageView(imageView, bgBitmap);
        return bgBitmap;
    }

    // 安全加载 资源的 bitmap 图片
    public static Bitmap safeResBitmap(int res) {
        Bitmap bgBitmap = null;
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = false; // 设置成了true,不占用内存，只获取bitmap宽高
            options.inPurgeable = true;
            options.inInputShareable = true;
            options.inDither = false;
            options.inPurgeable = true;
            options.inSampleSize = 1;
            bgBitmap = BitmapFactory.decodeResource(HaigeApplication.getInstance().getResources(), res, options);
        } catch (Exception e) {
            DebugLogger.e(e);
        } catch (OutOfMemoryError t) {
            System.gc();
        }
        return bgBitmap;
    }

    // 安全将图片放到imageView中
    public static void safeSetImageView(ImageView imageView, Bitmap bitmap) {
        try {
            if (!Utils.isNull(bitmap) && !bitmap.isRecycled()) {
                imageView.setImageBitmap(bitmap);
            }
        } catch (Exception e) {
            DebugLogger.e(e);
        } catch (OutOfMemoryError t) {
            System.gc();
        }
    }

    // 安全创建扩大的图片
    public static Bitmap safeCreateScaledBitmap(Bitmap src, int dstWidth, int dstHeight, boolean filter) {
        Bitmap bgBitmap = null;
        try {
            bgBitmap = Bitmap.createScaledBitmap(src, dstWidth, dstHeight, filter);
        } catch (Exception e) {
            DebugLogger.e(e);
        } catch (OutOfMemoryError t) {
            System.gc();
        }
        return bgBitmap;
    }

    // 释放图片的处理方法
    public static void releaseImageView(Bitmap bitmap, ImageView imageView) {
        releaseBitmap(bitmap);
        releaseImageView(imageView);
    }

    public static void releaseBitmap(Bitmap bitmap) {
        try {
            if (!Utils.isNull(bitmap) && !bitmap.isRecycled()) {
                bitmap.recycle();
            }
        } catch (Throwable e) {
            DebugLogger.e(e);
        }
    }

    public static void releaseImageView(ImageView imageView) {
        try {
            if (!Utils.isNull(imageView)) {
                imageView.setImageResource(0);
            }
        } catch (Throwable e) {
            DebugLogger.e(e);
        }
    }

    public static void releaseImageView(ImageView... imageViews) {
        for (ImageView iv : imageViews) {
            releaseImageView(iv);
        }
    }

    public static void releaseBitmap(Bitmap... bitmaps) {
        for (Bitmap b : bitmaps) {
            releaseBitmap(b);
        }
    }

}
