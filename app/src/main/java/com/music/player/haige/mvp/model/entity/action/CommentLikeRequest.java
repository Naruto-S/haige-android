package com.music.player.haige.mvp.model.entity.action;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Naruto on 2018/5/12.
 */

public class CommentLikeRequest implements Serializable {

    public static final String ACTION_LIKE = "like";
    public static final String ACTION_UNLIKE = "unlike";

    @SerializedName("action")
    private String action;

    @SerializedName("comment_id")
    private String commentId;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    @Override
    public String toString() {
        return "CommentLikeRequest{" +
                "action='" + action + '\'' +
                ", commentId='" + commentId + '\'' +
                '}';
    }
}
