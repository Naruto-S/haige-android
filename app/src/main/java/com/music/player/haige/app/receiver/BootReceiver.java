package com.music.player.haige.app.receiver;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.music.player.haige.app.service.InitService;

/**
 * Created by Graceful Sun on 18/2/27.
 * E-mail itzhishuaisun@sina.com
 */

public class BootReceiver extends BroadcastReceiver {

    @SuppressLint("UnsafeProtectedBroadcastReceiver")
    @Override
    public void onReceive(Context context, Intent intent) {
        initService(context);
    }

    private void initService(Context context) {
        InitService.start(context);
    }

}
