package com.music.player.haige.mvp.presenter;

import com.music.player.haige.mvp.contract.AppBasePresenter;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.Api;
import com.music.player.haige.mvp.model.entity.comment.CommentRequest;
import com.music.player.haige.mvp.model.entity.music.UploadMusicRequest;

import javax.inject.Inject;

public class UpdatePresenter extends AppBasePresenter<MVContract.UpdateModel, MVContract.UpdateView> {

    @Inject
    public UpdatePresenter(MVContract.UpdateModel model, MVContract.UpdateView view) {
        super(model, view);
    }

    public void uploadMusic(UploadMusicRequest request) {
        buildObservable(mModel.uploadMusic(request), Api.Action.UPLOAD_MUSIC);
    }
}
