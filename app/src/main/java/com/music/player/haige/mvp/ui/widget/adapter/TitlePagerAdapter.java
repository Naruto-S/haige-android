package com.music.player.haige.mvp.ui.widget.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.hc.base.base.BaseFragment;

/**
 * ================================================
 * Created by huangcong on 2018/2/25.
 * ================================================
 */

public class TitlePagerAdapter extends FragmentStatePagerAdapter {

    private String[] mTitles;
    private BaseFragment[] mFragments;

    public TitlePagerAdapter(FragmentManager fm, String[] titles, BaseFragment[] fragments) {
        super(fm);
        mTitles = titles;
        mFragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments[position];
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTitles[position];
    }

    @Override
    public int getCount() {
        if (mTitles == null) return 0;
        return mTitles.length;
    }
}
