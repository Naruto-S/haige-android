package com.music.player.haige.app.image.utils;

import android.os.Environment;
import android.os.StatFs;

import com.music.player.haige.mvp.ui.utils.DebugLogger;

import java.io.File;

/**
 * @author WeJoy Group
 */
public class SDCardUtil {

    public static final String TAG = SDCardUtil.class.getSimpleName();

    public static final int MB = 1024 * 1024;
    public static final int MINIMUM_SDCARD_SPACE = 5; // 5MB

    public static int freeSpaceOnSd() {
        int sdFreeMB = 0;
        try {
            if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
                sdFreeMB = (int) (((double) stat.getAvailableBlocks() * (double) stat.getBlockSize()) / MB);
            }
        } catch (Throwable throwable) {
            DebugLogger.e(TAG, throwable);
        }
        return sdFreeMB;
    }

    public static boolean isSDCardWritable() {
        return freeSpaceOnSd() >= MINIMUM_SDCARD_SPACE;
    }

    public static File createFolder(String folder) {
        File file = new File(folder);

        if (!file.exists()) {
            String[] subfolder = folder.split(File.separator);
            StringBuilder sb = new StringBuilder();

            for (String subfolder0 : subfolder) {
                sb.append(subfolder0).append(File.separator);
                file = new File(sb.toString());

                if (!file.exists()) {
                    file.mkdir();
                }
            }
        }

        return file;
    }
}
