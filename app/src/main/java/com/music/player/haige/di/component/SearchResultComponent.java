package com.music.player.haige.di.component;

import com.hc.core.di.component.AppComponent;
import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.di.module.SearchResultModule;
import com.music.player.haige.mvp.ui.main.fragment.SearchResultFragment;

import dagger.Component;

/**
 * ================================================
 * Created by huangcong on 2018/3/29.
 * ================================================
 */
@FragmentScope
@Component(modules = {SearchResultModule.class}, dependencies = AppComponent.class)
public interface SearchResultComponent {

    void inject(SearchResultFragment fragment);
}
