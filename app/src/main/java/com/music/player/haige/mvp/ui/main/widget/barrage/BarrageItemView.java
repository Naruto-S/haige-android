package com.music.player.haige.mvp.ui.main.widget.barrage;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.music.player.haige.R;
import com.music.player.haige.app.image.loader.AvatarLoader;
import com.music.player.haige.app.image.loader.LocalImageLoader;
import com.music.player.haige.app.image.widget.HaigeImageView;
import com.music.player.haige.app.utils.launcher.ActivityLauncherStart;
import com.music.player.haige.mvp.model.entity.comment.CommentBean;
import com.music.player.haige.mvp.ui.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.music.player.haige.mvp.model.entity.user.UserBean.TYPE_OFFICIAL_USER;

/**
 * 通用弹幕
 * <p>
 * Created by mg on 09/11/2017.
 */

public class BarrageItemView extends LinearLayout {

    @BindView(R.id.hiv_danmuku_avatar)
    HaigeImageView mAvatarHiv;

    @BindView(R.id.tv_danmuku_content)
    TextView mContentTv;

    private float mTextWidth;
    private CommentBean mCommentBean;

    public BarrageItemView(Context context, CommentBean commentBean) {
        super(context);
        mCommentBean = commentBean;
    }

    public BarrageItemView(Context context, AttributeSet attrs, CommentBean commentBean) {
        super(context, attrs);
        mCommentBean = commentBean;
    }

    public BarrageItemView(Context context, AttributeSet attrs, int defStyleAttr, CommentBean commentBean) {
        super(context, attrs, defStyleAttr);
        mCommentBean = commentBean;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // 父容器传过来的宽度的值
        int width = MeasureSpec.getSize(widthMeasureSpec) - getPaddingLeft() - getPaddingRight();

        width = (int) (width * 2.0f);
        if (width < mTextWidth * 2) {
            width = (int) mTextWidth * 2;
        }

        widthMeasureSpec = MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(getResourceId(), this, true);
        ButterKnife.bind(this, this);
        setCommentView();
    }

    protected int getResourceId() {
        return R.layout.layout_barrage_item;
    }

    public void setCommentView() {
        if (mCommentBean == null || mCommentBean.getUserBean() == null) {
            return;
        }

        if (mCommentBean.getUserBean().getType() == TYPE_OFFICIAL_USER) {
            LocalImageLoader.displayResImage(R.drawable.ic_avatar_default_official, mAvatarHiv);
        } else {
            AvatarLoader.loadUserAvatar(mCommentBean.getUserBean().getAvatarUrl(),
                    R.drawable.ic_avatar_default_official,
                    R.drawable.ic_avatar_default_official,
                    mAvatarHiv);
        }

        mContentTv.setText(mCommentBean.getContext());
    }

    public TextView getContentTv() {
        return mContentTv;
    }

    public float getTextWidth() {
        return mTextWidth;
    }

    public void setTextWidth(float textWidth) {
        mTextWidth = textWidth;
    }

    @OnClick(R.id.layout_barrage_item)
    public void gotoUserProfile() {
        if (Utils.isNotNull(mCommentBean) && Utils.isNotNull(mCommentBean.getUserBean()) && Utils.isNotEmptyString(mCommentBean.getUserBean().getUserId())) {
            ActivityLauncherStart.startOtherUser((Activity) getContext(), mCommentBean.getUserBean());
        }
    }
}
