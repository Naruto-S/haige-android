package com.music.player.haige.mvp.model.entity.rank;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Naruto on 2018/8/25.
 */

public class RankCategoryBean implements Serializable {

    @SerializedName("id")
    String id;

    @SerializedName("tag")
    String tag;

    @SerializedName("title")
    String title;

    @SerializedName("img")
    String img;

    @SerializedName("description")
    String description;

    @SerializedName("count")
    int count;

    private boolean isSelect;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }

    @Override
    public String toString() {
        return "RankCategoryBean{" +
                "id='" + id + '\'' +
                ", tag='" + tag + '\'' +
                ", title='" + title + '\'' +
                ", img='" + img + '\'' +
                ", description='" + description + '\'' +
                ", count=" + count +
                ", isSelect=" + isSelect +
                '}';
    }
}
