package com.music.player.haige.mvp.ui.widget.emoji;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Rect;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.music.player.haige.R;
import com.music.player.haige.mvp.model.entity.comment.CommentRequest;
import com.music.player.haige.mvp.ui.utils.KeyBoardUtils;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;
import com.music.player.haige.mvp.ui.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Geek4IT on 2/12/16.
 */
public class EditCommentView extends LinearLayout {

    private static final String TAG = EditCommentView.class.getSimpleName();

    @BindView(R.id.messageEditText)
    EmojiEditText mEditText;

    @BindView(R.id.tv_comment_send)
    TextView mSentTv;

    private int mKeyBoardHeight;
    private boolean isSoftInputOpen;
    private CommentRequest mCommentRequest;
    private OnEditCommentListener mEditCommentListener;

    public EditCommentView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
        initView();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        View editLayout = getChildAt(0);
        View placeView = getChildAt(1);
        int[] location = new int[2];
        editLayout.getLocationInWindow(location);
        int editBottom = location[1] + editLayout.getHeight();
        Rect rect = new Rect();
        editLayout.getWindowVisibleDisplayFrame(rect);
        if (editBottom > rect.bottom) {
            mKeyBoardHeight = KeyBoardUtils.getKeyboardHeight(scanForActivity(getContext()));
            ViewGroup.LayoutParams lp = placeView.getLayoutParams();
            if (lp.height != mKeyBoardHeight) {
                lp.height = mKeyBoardHeight;
                placeView.setLayoutParams(lp);
                isSoftInputOpen = true;
            }
        } else if (editBottom < rect.bottom && isSoftInputOpen) {

            int keyboardHeight = KeyBoardUtils.getKeyboardHeight(scanForActivity(getContext()));
            if (keyboardHeight != mKeyBoardHeight
                    && keyboardHeight != getDipDimension(KeyBoardUtils.DEFAULT_SOFT_KEYBOARD_HEIGHT)) {
                ViewGroup.LayoutParams lp = placeView.getLayoutParams();
                if (lp.height != keyboardHeight) {
                    lp.height = keyboardHeight;
                    placeView.setLayoutParams(lp);
                }
            } else {
                ViewGroup.LayoutParams lp = placeView.getLayoutParams();
                lp.height = 0;
                placeView.setLayoutParams(lp);

                isSoftInputOpen = false;
                setInputVisibility(GONE);
            }
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mKeyBoardHeight = KeyBoardUtils.getKeyboardHeight(scanForActivity(getContext()));
    }

    public int getDipDimension(float size) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, size, getResources()
                .getDisplayMetrics());
    }

    private void initView() {
        mEditText.addTextChangedListener(editTextWatcher);
        //软键盘完成键捕捉
        mEditText.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEND) {
                sendMessage();
                return true;
            }
            return false;
        });
        mSentTv.setOnClickListener(v -> sendMessage());

        // 监听删除按键
        mEditText.setOnDelKeyListener(() -> false);
    }

    /**
     * 发送消息
     */
    private void sendMessage() {
        String message = mEditText.getText().toString().trim();
        if (Utils.isNotNull(mEditCommentListener)) {
            mCommentRequest.setContext(message);
            mEditCommentListener.onSend(mCommentRequest);
        }
        mEditText.setText("");

        KeyBoardUtils.closeKeybord(mEditText, scanForActivity(getContext()));
    }

    public void setInputVisibility(int visibility) {
        if (visibility == View.VISIBLE) {
            if (Utils.isNotNull(mCommentRequest) && Utils.isNotNull(mCommentRequest.getUser())) {
                String hint = mCommentRequest.getUser().getNickName();
                mEditText.setHint(ResourceUtils.resourceString(R.string.comment_reply_edit_hint, hint));
            }
            display();
        } else {
            disappear();
        }
    }

    public void setCommentRequest(CommentRequest commentRequest) {
        mCommentRequest = commentRequest;
    }

    public EditText getEditText() {
        return mEditText;
    }

    /**
     * 渐变消失
     */
    public void disappear() {
        Animator anim = ObjectAnimator.ofFloat(this, View.ALPHA, new float[]{1f, 0f});
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                setVisibility(GONE);
            }
        });
        anim.start();
    }

    /**
     * 渐变显示
     */
    public void display() {
        Animator anim = ObjectAnimator.ofFloat(this, View.ALPHA, new float[]{0f, 1f});
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                setVisibility(VISIBLE);
            }
        });
        anim.start();
    }

    private final TextWatcher editTextWatcher = new TextWatcher() {
        private CharSequence temp;

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            temp = s;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (temp.toString().trim().length() <= 0) {
                mSentTv.setEnabled(false);
            } else {
                mSentTv.setEnabled(true);
            }
        }
    };

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }

    private Activity scanForActivity(Context cont) {
        if (cont == null)
            return null;
        else if (cont instanceof Activity)
            return (Activity) cont;
        else if (cont instanceof ContextWrapper)
            return scanForActivity(((ContextWrapper) cont).getBaseContext());
        return null;
    }

    public void setEditCommentListener(OnEditCommentListener editCommentListener) {
        mEditCommentListener = editCommentListener;
    }

    public interface OnEditCommentListener {

        void onSend(CommentRequest request);

    }

}
