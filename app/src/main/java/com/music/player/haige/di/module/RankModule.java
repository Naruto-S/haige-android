package com.music.player.haige.di.module;

import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.RankModel;

import dagger.Module;
import dagger.Provides;

/**
 * ================================================
 * Created by huangcong on 2018/3/3.
 * ================================================
 */
@Module
public class RankModule {
    private MVContract.RankView view;

    public RankModule(MVContract.RankView view){
        this.view = view;
    }

    @FragmentScope
    @Provides
    MVContract.RankView provideView(){
        return view;
    }

    @FragmentScope
    @Provides
    MVContract.RankModel provideModel(RankModel model){
        return model;
    }

}
