package com.music.player.haige.di.component;

import com.hc.core.di.component.AppComponent;
import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.di.module.MeProfileModule;
import com.music.player.haige.mvp.ui.user.MeProfileFragment;

import dagger.Component;

/**
 * Created by Naruto on 2018/5/13.
 */
@FragmentScope
@Component(modules = {MeProfileModule.class}, dependencies = AppComponent.class)
public interface MeProfileComponent {

    void inject(MeProfileFragment fragment);
}
