package com.music.player.haige.mvp.presenter;

import android.app.Activity;
import android.widget.Toast;

import com.music.player.haige.app.base.pref.UserPrefHelper;
import com.music.player.haige.mvp.contract.AppBasePresenter;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.Api;
import com.music.player.haige.mvp.model.entity.user.LoginQQBean;
import com.music.player.haige.mvp.model.entity.user.LoginRequest;
import com.music.player.haige.mvp.model.entity.user.LoginWXBean;
import com.music.player.haige.mvp.model.entity.user.UserResponse;
import com.music.player.haige.mvp.ui.utils.Utils;
import com.umeng.socialize.UMAuthListener;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.bean.SHARE_MEDIA;

import java.util.Map;

import javax.inject.Inject;

import me.jessyan.rxerrorhandler.core.RxErrorHandler;
import me.jessyan.rxerrorhandler.handler.ErrorHandleSubscriber;
import timber.log.Timber;

import static com.music.player.haige.mvp.model.entity.user.LoginRequest.TYPE_LOGIN_QQ;
import static com.music.player.haige.mvp.model.entity.user.LoginRequest.TYPE_LOGIN_SINA;
import static com.music.player.haige.mvp.model.entity.user.LoginRequest.TYPE_LOGIN_WEIXIN;

/**
 * Created by Naruto on 2018/5/5.
 */

public class LoginFPresenter extends AppBasePresenter<MVContract.LoginModel, MVContract.LoginView> {

    public static final String TAG = LoginFPresenter.class.getSimpleName();

    @Inject
    RxErrorHandler mErrorHandler;


    @Inject
    public LoginFPresenter(MVContract.LoginModel model, MVContract.LoginView view) {
        super(model, view);
    }

    public void onQQLogin(Activity activity) {
        authorization(activity, TYPE_LOGIN_QQ);
    }

    public void onWeiXinLogin(Activity activity) {
        authorization(activity, TYPE_LOGIN_WEIXIN);
    }

    public void onSinaLogin(Activity activity) {
        authorization(activity, TYPE_LOGIN_SINA);
    }

    //授权
    public void authorization(Activity activity, String type) {
        mRootView.showProgressBar();

        SHARE_MEDIA share_media;
        switch (type) {
            case TYPE_LOGIN_QQ:
                share_media = SHARE_MEDIA.QQ;
                break;

            case TYPE_LOGIN_WEIXIN:
                share_media = SHARE_MEDIA.WEIXIN;
                break;

            case TYPE_LOGIN_SINA:
                share_media = SHARE_MEDIA.SINA;
                break;
            default:
                share_media = SHARE_MEDIA.QQ;
                break;
        }

        UMShareAPI.get(activity).getPlatformInfo(activity, share_media, new UMAuthListener() {
            @Override
            public void onStart(SHARE_MEDIA share_media) {
                Timber.tag(TAG).e("onStart " + "授权开始");
            }

            @Override
            public void onComplete(SHARE_MEDIA share_media, int i, Map<String, String> map) {
                Toast.makeText(activity, "授权完成", Toast.LENGTH_SHORT).show();

                //sdk是6.4.4的,但是获取值的时候用的是6.2以前的(access_token)才能获取到值,未知原因
                String uid = map.get("uid");
                String openid = map.get("openid");//微博没有
                String unionid = map.get("unionid");//微博没有
                String access_token = map.get("access_token");
                String refresh_token = map.get("refresh_token");//微信,qq,微博都没有获取到
                String expires_in = map.get("expires_in");
                String name = map.get("name");
                String gender = map.get("gender");
                String iconurl = map.get("iconurl");
                String pay_token = map.get("pay_token");

                Timber.tag(TAG).e("uid == " + uid +
                        "\nopenid == " + openid +
                        "\naccess_token == " + access_token +
                        "\nname == " + name +
                        "\ngender == " + gender +
                        "\niconurl == " + iconurl);

                //拿到信息去请求登录接口。。。
                LoginRequest request = new LoginRequest();
                request.setType(type);
                switch (type) {
                    case TYPE_LOGIN_QQ:
                        LoginQQBean qqBean = new LoginQQBean();
                        qqBean.setOpenid(openid);
                        qqBean.setAccessToken(access_token);
                        qqBean.setPayToken(pay_token);
                        qqBean.setExpiresIn(expires_in);
                        request.setQqInfo(qqBean);
                        break;

                    case TYPE_LOGIN_WEIXIN:
                        LoginWXBean wxBean = new LoginWXBean();
                        wxBean.setOpenid(openid);
                        wxBean.setToken(access_token);
                        request.setWxInfo(wxBean);
                        break;

                    case TYPE_LOGIN_SINA:
                        break;
                }

                userLoginOrRegistrer(request);

                UserPrefHelper.putUserLoginType(type);
            }

            @Override
            public void onError(SHARE_MEDIA share_media, int i, Throwable throwable) {
                Toast.makeText(activity, "授权失败", Toast.LENGTH_SHORT).show();
                if (Utils.isNotNull(mRootView)) {
                    mRootView.hideProgressBar();
                }
            }

            @Override
            public void onCancel(SHARE_MEDIA share_media, int i) {
                Toast.makeText(activity, "授权取消", Toast.LENGTH_SHORT).show();
                if (Utils.isNotNull(mRootView)) {
                    mRootView.hideProgressBar();
                }
            }
        });
    }

    public void userLoginOrRegistrer(LoginRequest request) {
        if (Utils.isNull(request) || Utils.isNull(mModel)) return;

        if (Utils.isNotNull(mRootView)) {
            mRootView.showProgressBar();
        }

        buildObservable(mModel.userLoginOrRegistrer(request))
                .subscribe(new ErrorHandleSubscriber<UserResponse>(mErrorHandler) {
                    @Override
                    public void onNext(UserResponse response) {
                        if (Utils.isNotNull(mRootView)) {
                            if (response != null) {
                                mRootView.onSuccess(Api.Action.USER_LOGIN, response.getData());
                            } else {
                                mRootView.onError(Api.Action.USER_LOGIN, Api.State.NORMAL.getCode(), Api.State.NORMAL.getMsg());
                            }
                            mRootView.hideProgressBar();
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                        if (Utils.isNotNull(mRootView)) {
                            mRootView.onError(Api.Action.USER_LOGIN, "-1", t.getMessage());
                            mRootView.hideProgressBar();
                        }
                    }
                });
    }

    public void getUserProfile() {
        buildObservable(mModel.getUserProfile())
                .subscribe(new ErrorHandleSubscriber<UserResponse>(mErrorHandler) {
                    @Override
                    public void onNext(UserResponse response) {
                        if (response != null) {
                            mRootView.onSuccess(Api.Action.USER_PROFILE, response.getData());
                        } else {
                            mRootView.onError(Api.Action.USER_PROFILE, Api.State.NORMAL.getCode(), Api.State.NORMAL.getMsg());
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                        mRootView.onError(Api.Action.USER_PROFILE, "-1", t.getMessage());
                    }
                });
    }
}
