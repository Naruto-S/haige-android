package com.music.player.haige.app.base.recyclerview.pullrefresh;

public class DragDistanceConverter implements IDragDistanceConverter {

    @Override
    public float convert(float scrollDistance, float refreshDistance) {
        return scrollDistance * 0.5f;
    }
}