package com.music.player.haige.mvp.ui.widget.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.music.player.haige.R;
import com.music.player.haige.app.base.BaseDialogFragment;

import butterknife.BindView;

/**
 * Created by Naruto on 2018/1/19.
 */

public class SystemDialog extends BaseDialogFragment {

    @BindView(R.id.personal_dialog_msg)
    TextView mContentTv;
    @BindView(R.id.personal_dialog_ok)
    TextView mOkTv;
    @BindView(R.id.personal_dialog_cancel)
    TextView mCancelTv;

    private String mContent;
    private String mOk;
    private String mCancel;

    private OnClickListener mOkListener;
    private OnClickListener mCancelListener;

    public SystemDialog create(FragmentManager manager) {
        SystemDialog dialog = new SystemDialog();
        dialog.setFragmentManger(manager);
        return dialog;
    }

    public SystemDialog setCancelOutside(boolean cancelOutside) {
        this.mIsCancelOutside = cancelOutside;
        return this;
    }

    public SystemDialog setContent(String content) {
        this.mContent = content;
        return this;
    }

    public SystemDialog setOk(String ok) {
        this.mOk = ok;
        return this;
    }

    public SystemDialog setCancle(String cancel) {
        this.mCancel = cancel;
        return this;
    }

    public SystemDialog setOnOkListener(OnClickListener listener) {
        this.mOkListener = listener;
        return this;
    }

    public SystemDialog setOnCancelListener(OnClickListener listener) {
        this.mCancelListener = listener;
        return this;
    }

    public SystemDialog show() {
        show(mFragmentManager);
        return this;
    }

    @Override
    public int setRootView() {
        return R.layout.dialog_content_system;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (!TextUtils.isEmpty(mContent)) {
            mContentTv.setText(mContent);
        } else {
            mContentTv.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(mOk)) {
            mOkTv.setText(mOk);
        } else {
            mOkTv.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(mCancel)) {
            mCancelTv.setText(mCancel);
        } else {
            mCancelTv.setVisibility(View.GONE);
        }

        mOkTv.setOnClickListener(v -> onOK());
        mCancelTv.setOnClickListener(v -> onCancel());
    }

    public void onOK() {
        try {
            if (mOkListener != null) {
                mOkListener.onClick();
            }
            dismiss();
        } catch (Exception e) {

        }
    }

    public void onCancel() {
        try {
            if (mCancelListener != null) {
                mCancelListener.onClick();
            }
            dismiss();
        } catch (Exception e) {

        }
    }

    public interface OnClickListener {

        void onClick();
    }

}
