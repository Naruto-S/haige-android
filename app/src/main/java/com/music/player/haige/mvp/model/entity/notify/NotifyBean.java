package com.music.player.haige.mvp.model.entity.notify;

import com.google.gson.annotations.SerializedName;
import com.music.player.haige.mvp.model.entity.music.Song;

import java.io.Serializable;

/**
 * Created by Naruto on 2018/6/30.
 */

public class NotifyBean implements Serializable {

    @SerializedName("create_time")
    private long createTime;
    @SerializedName("start_time")
    private int startTime;
    @SerializedName("title")
    private String title;
    @SerializedName("content")
    private String content;
    @SerializedName("show_type")
    private int showType;
    @SerializedName("notify_type")
    private int notifyType;
    @SerializedName("music")
    private Song song;

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public int getStartTime() {
        return startTime;
    }

    public void setStartTime(int startTime) {
        this.startTime = startTime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getShowType() {
        return showType;
    }

    public void setShowType(int showType) {
        this.showType = showType;
    }

    public int getNotifyType() {
        return notifyType;
    }

    public void setNotifyType(int notifyType) {
        this.notifyType = notifyType;
    }

    public Song getSong() {
        return song;
    }

    public void setSong(Song song) {
        this.song = song;
    }

    @Override
    public String toString() {
        return "NotifyBean{" +
                "createTime=" + createTime +
                ", startTime=" + startTime +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", showType=" + showType +
                ", notifyType=" + notifyType +
                ", song=" + song +
                '}';
    }
}
