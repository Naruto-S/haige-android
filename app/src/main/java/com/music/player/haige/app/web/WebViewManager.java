package com.music.player.haige.app.web;

import android.net.http.SslError;
import android.os.Build;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.music.player.haige.app.HaigeApplication;
import com.music.player.haige.mvp.ui.utils.NetworkUtils;

/**
 * Created by liumingkong on 16/4/5.
 */
public class WebViewManager {


    public static void initWebviewSettings(WebView webView) {
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);   //设置支持Javascript
        webSettings.setSupportZoom(true);
        webSettings.setBuiltInZoomControls(true);   //页面添加缩放按钮
        webSettings.setDisplayZoomControls(false);
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setDefaultTextEncodingName("UTF-8"); // 字体及文字编码设置
        webSettings.setDomStorageEnabled(true);
        try {
            // 缓存设置
            webSettings.setAllowFileAccess(true); // 可以读取文件缓存(manifest生效)
            webSettings.setAppCacheEnabled(true); // H5缓存 设置H5的缓存是否打开，默认关闭。
            String appCachePath = HaigeApplication.getInstance().getCacheDir().getAbsolutePath();
            webSettings.setAppCachePath(appCachePath); // 提供的路径，在H5使用缓存过程中生成的缓存文件。
            // 通过setAppCacheMaxSize(long appCacheMaxSize)设置缓存最大容量。
            webSettings.setAppCacheMaxSize(8 * 1024 * 1024);
        } catch (Throwable e) {
        }
        beforeLoadWebUrl(webView);
    }

    // ************************************************************


    // 因为4.4以上系统在onPageFinished时再恢复图片加载时,如果存在多张图片引用的是相同的src时，会只有一个image标签得到加载，因而对于这样的系统我们就先直接加载。
    public static void onPageFinish(WebView webView) {
        try {
            if (!webView.getSettings().getLoadsImagesAutomatically()) {
                webView.getSettings().setLoadsImagesAutomatically(true);
            }
        } catch (Throwable e) {
        }
    }

    // ************************************************************
    public static void onReceivedError(WebView loadWebview) {
        // 加载网页失败时处理  如：
    }

    public static void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
        handler.proceed();  // 接受信任所有网站的证书
        // handler.cancel();   // 默认操作 不处理
        // handler.handleMessage(null);  // 可做其他处理
    }

    // *********************************************************************
    private static void webviewSettingsData(WebSettings webSettings) {
        /**
         * 用WebView显示图片，可使用这个参数 设置网页布局类型：
         * 1、LayoutAlgorithm.NARROW_COLUMNS ：适应内容大小
         * 2、LayoutAlgorithm.SINGLE_COLUMN : 适应屏幕，内容将自动缩放
         */
        webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webSettings.setUseWideViewPort(true);
        webSettings.setDatabaseEnabled(true); //开启 database storage API 功能 是否允许使用数据库api
    }

    public static void beforeLoadWebUrl(WebView webView) {
        try {
            // LOAD_DEFAULT，根据cache-control决定是否从网络上取数据
            // LOAD_CACHE_ELSE_NETWORK，只要本地有，无论是否过期，或者no-cache，都使用缓存中的数据
            if (NetworkUtils.isConnected(HaigeApplication.getInstance())) {
                webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
            } else {
                webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
            }
        } catch (Throwable e) {
        }
        try {
            // 默认情况html代码下载到WebView后，webkit开始解析网页各个节点，发现有外部样式文件或者外部脚本文件时，会异步发起网络请求下载文件，但如果在这之前也有解析到image节点，那势必也会发起网络请求下载相应的图片。
            // 在网络情况较差的情况下，过多的网络请求就会造成带宽紧张，影响到css或js文件加载完成的时间，造成页面空白loading过久。
            // 解决的方法就是告诉WebView先不要自动加载图片，等页面finish后再发起图片加载。
            if (Build.VERSION.SDK_INT >= 19) {
                webView.getSettings().setLoadsImagesAutomatically(true);
            } else {
                webView.getSettings().setLoadsImagesAutomatically(false);
            }
        } catch (Throwable e) {
        }
    }
}
