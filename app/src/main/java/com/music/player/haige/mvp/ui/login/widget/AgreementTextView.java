package com.music.player.haige.mvp.ui.login.widget;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.AppCompatTextView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.AttributeSet;
import android.view.View;

import com.music.player.haige.R;
import com.music.player.haige.app.web.WebViewActivity;

import static com.music.player.haige.app.Constants.URL_PRIVACY_POLICY;

/**
 * Created by Geek4IT on 3/14/16.
 */
public class AgreementTextView extends AppCompatTextView {

    public AgreementTextView(Context context) {
        super(context);
    }

    public AgreementTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        String privacy = getResources().getString(R.string.privacy_policy);
        String agreement = getResources().getString(R.string.login_agreement, privacy);
        int privacyStartIndex = agreement.lastIndexOf(privacy);
        int privacyEndIndex = agreement.lastIndexOf(privacy) + privacy.length();

        SpannableStringBuilder content = new SpannableStringBuilder(agreement);
        content.setSpan(new StyleSpan(android.graphics.Typeface.BOLD_ITALIC), privacyStartIndex, privacyEndIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        content.setSpan(new ForegroundColorSpan(Color.parseColor("#ed6c4c")), privacyStartIndex,
                privacyEndIndex, 0);
        content.setSpan(new PrivacyUrlSpan(), privacyStartIndex, privacyEndIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        setText(content);
        setMovementMethod(LinkMovementMethod.getInstance());
    }

    private class TermsUrlSpan extends ClickableSpan {

        @Override
        public void updateDrawState(TextPaint ds) {

        }

        @Override
        public void onClick(View widget) {
            WebViewActivity.start(getContext(), URL_PRIVACY_POLICY);
        }
    }

    private class PrivacyUrlSpan extends ClickableSpan {

        @Override
        public void updateDrawState(TextPaint ds) {

        }

        @Override
        public void onClick(View widget) {
            WebViewActivity.start(getContext(), URL_PRIVACY_POLICY);
        }
    }
}
