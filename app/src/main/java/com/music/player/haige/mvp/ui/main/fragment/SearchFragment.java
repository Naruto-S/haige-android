package com.music.player.haige.mvp.ui.main.fragment;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.hc.base.widget.flowlayout.FlowLayout;
import com.hc.base.widget.flowlayout.TagAdapter;
import com.hc.base.widget.flowlayout.TagFlowLayout;
import com.hc.core.di.component.AppComponent;
import com.hc.core.utils.CoreUtils;
import com.hc.core.utils.DeviceUtils;
import com.music.player.haige.R;
import com.music.player.haige.app.Constants;
import com.music.player.haige.app.EventBusTags;
import com.music.player.haige.app.HaigeApplication;
import com.music.player.haige.app.base.AppBaseFragment;
import com.music.player.haige.di.component.DaggerSearchComponent;
import com.music.player.haige.di.module.SearchModule;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.Api;
import com.music.player.haige.mvp.model.entity.search.SearchHotWordResponse;
import com.music.player.haige.mvp.model.utils.StringUtils;
import com.music.player.haige.mvp.presenter.SearchFPresenter;
import com.music.player.haige.mvp.ui.main.adapter.SearchHistoryAdapter;

import org.simple.eventbus.EventBus;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * ================================================
 * Created by huangcong on 2018/3/3.
 * <p>
 * 搜索页面
 * ================================================
 */

public class SearchFragment extends AppBaseFragment<SearchFPresenter> implements MVContract.SearchView, SearchHistoryAdapter.OnHistoryItemClickListener {

    @BindView(R.id.app_layout_search_keyword_et)
    EditText mKeyWordEt;

    @BindView(R.id.app_layout_search_keyword_clear_iv)
    ImageView mKeyWordClearIv;

    @BindView(R.id.app_fragment_search_hot_layout)
    View mHotLayout;

    @BindView(R.id.app_fragment_search_hot_flow_layout)
    TagFlowLayout mHotFlowLayout;

    @BindView(R.id.app_fragment_search_history_layout)
    View mHistoryLayout;

    @BindView(R.id.app_fragment_search_history_list_rv)
    RecyclerView mHistoryRv;

    @BindView(R.id.app_fragment_search_result_layout)
    View mSearchResultLayout;

    private LayoutInflater mLayoutInflater;
    private SearchHistoryAdapter mHistoryAdapter;
    private ArrayList<String> mSearchHotWords;
    private Constants.SEARCH mSearchType;

    public static SearchFragment newInstance(Constants.SEARCH searchType) {
        SearchFragment fragment = new SearchFragment();
        Bundle args = new Bundle();
        args.putSerializable(Constants.SEARCH_TYPE, searchType);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void setupFragmentComponent(AppComponent appComponent) {
        DaggerSearchComponent
                .builder()
                .appComponent(appComponent)
                .searchModule(new SearchModule(this))
                .build()
                .inject(this);
    }

    @Override
    public View initView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mLayoutInflater = inflater;
        return inflater.inflate(R.layout.app_fragment_search, container, false);
    }

    @Override
    public void initData(Bundle savedInstanceState) {
        mSearchType = (Constants.SEARCH) getArguments().getSerializable(Constants.SEARCH_TYPE);

        if (Constants.SEARCH.NET == mSearchType) {
            mPresenter.loadHotWord();
        } else if (Constants.SEARCH.LOCAL == mSearchType) {
            mKeyWordEt.setHint(R.string.app_search_hint_local);
        } else if (Constants.SEARCH.RECENT == mSearchType) {
            mKeyWordEt.setHint(R.string.app_search_hint_recent);
        }
        mPresenter.loadHistoryWord(mSearchType);

        mKeyWordEt.requestFocus();
        mKeyWordEt.postDelayed(() -> DeviceUtils.showSoftKeyboard(getContext(), mKeyWordEt), 100);
        mKeyWordEt.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH
                    || actionId == EditorInfo.IME_ACTION_UNSPECIFIED) {
                String text = v.getText().toString().trim();
                searchKeyWord(text);
                DeviceUtils.hideSoftKeyboard(getContext(), mKeyWordEt);
                return true;
            }
            return false;
        });
        mKeyWordEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = s.toString();
                if (StringUtils.isEmpty(text)) {
                    removeSearchResult();
                } else {
                    mKeyWordClearIv.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public void onSuccess(Api.Action action, Object o) {
        super.onSuccess(action, o);
        if (action == Api.Action.SEARCH_HOT_TAG) {
            mHotLayout.setVisibility(View.VISIBLE);
            SearchHotWordResponse hotWordResult = (SearchHotWordResponse) o;
            mSearchHotWords = hotWordResult.getData();
            mHotFlowLayout.setAdapter(new TagAdapter<String>(mSearchHotWords) {
                @Override
                public View getView(FlowLayout parent, int position, String hotWord) {
                    View view = mLayoutInflater.inflate(R.layout.app_item_search_hot_word, null);
                    TextView searchTv = view.findViewById(R.id.app_item_music_tag_tv);
                    searchTv.setText(hotWord);
                    return view;
                }
            });
            mHotFlowLayout.setOnTagClickListener((view, position, parent) -> {
                clickToSearch(mSearchHotWords.get(position));
                return true;
            });
        } else if (action == Api.Action.SEARCH_HISTORY) {
            ArrayList<String> historyWords = (ArrayList<String>) o;
            if (historyWords == null || historyWords.size() <= 0) {
                mHistoryLayout.setVisibility(View.GONE);
                if (mHistoryAdapter != null) {
                    mHistoryAdapter.clearData();
                }
                return;
            }
            mHistoryLayout.setVisibility(View.VISIBLE);
            if (mHistoryAdapter == null) {
                mHistoryAdapter = new SearchHistoryAdapter(getContext(), this);
                mHistoryRv.setLayoutManager(new LinearLayoutManager(getContext()));
                mHistoryRv.setHasFixedSize(true);
                mHistoryRv.setAdapter(mHistoryAdapter);
            }
            mHistoryAdapter.refreshData(historyWords);
        }
    }

    @Override
    public void onError(Api.Action action, String code, String message) {
        super.onError(action, code, message);
    }

    @Override
    public void showMessage(String message) {
        super.showMessage(message);
        CoreUtils.makeText(getContext(), message);
    }

    @Override
    public void onHistoryItemClick(String historyKeyWord) {
        clickToSearch(historyKeyWord);
    }

    @OnClick({R.id.app_fragment_search_cancel_tv,
            R.id.app_layout_search_keyword_iv,
            R.id.app_layout_search_keyword_clear_iv,
            R.id.app_fragment_search_history_clear_tv})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.app_fragment_search_cancel_tv:
                DeviceUtils.hideSoftKeyboard(getContext(), mKeyWordEt);
                EventBus.getDefault().post(EventBusTags.MAIN.BACK);
                break;
            case R.id.app_layout_search_keyword_iv:
                String text = mKeyWordEt.getText().toString().trim();
                searchKeyWord(text);
                break;
            case R.id.app_layout_search_keyword_clear_iv:
                mKeyWordEt.setText("");
                mKeyWordClearIv.setVisibility(View.GONE);
                break;
            case R.id.app_fragment_search_history_clear_tv:
                mPresenter.clearHistory(mSearchType);
                break;
        }
    }

    private void clickToSearch(String keyWord) {
        mKeyWordEt.setText(keyWord);
        mKeyWordEt.setSelection(keyWord.length());
        searchKeyWord(keyWord);
        DeviceUtils.hideSoftKeyboard(getContext(), mKeyWordEt);
    }

    private void searchKeyWord(String keyWord) {
        if (StringUtils.isEmpty(keyWord)) {
            showMessage(HaigeApplication.getInstance().getString(R.string.app_search_empty_tip));
        } else {
            // 记录到历史搜索
            mPresenter.saveHistoryWord(mSearchType, mHistoryAdapter != null ? mHistoryAdapter.getData() : null, keyWord);

            // 显示搜索结果
            mSearchResultLayout.setVisibility(View.VISIBLE);
            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            transaction.replace(
                    R.id.app_fragment_search_result_layout,
                    SearchResultFragment.newInstance(mSearchType, keyWord),
                    SearchResultFragment.class.getName());
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            transaction.commitAllowingStateLoss();
        }
    }

    /**
     *
     */
    private void removeSearchResult() {
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        SearchResultFragment fragment = (SearchResultFragment) getChildFragmentManager().findFragmentByTag(SearchResultFragment.class.getName());
        if (fragment != null) {
            if (fragment.isAdded()) {
                transaction.remove(fragment);
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                transaction.commitAllowingStateLoss();
            }
        }
    }

}
