package com.music.player.haige.mvp.model;

import com.hc.base.mvp.BaseModel;
import com.hc.core.integration.IRepositoryManager;
import com.music.player.haige.app.GlobalConfiguration;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.usecase.GetArtists;

import javax.inject.Inject;

/**
 * ================================================
 * Created by huangcong on 2018/3/1.
 * ================================================
 */

public class ArtistModel extends BaseModel implements MVContract.ArtistModel {

    GetArtists mGetArtistsCase;

    @Inject
    public ArtistModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
        mGetArtistsCase = new GetArtists(GlobalConfiguration.sRepository);
    }

    @Override
    public GetArtists.ResponseValue loadArtists(String action) {
        return mGetArtistsCase.execute(new GetArtists.RequestValues(action));
    }
}
