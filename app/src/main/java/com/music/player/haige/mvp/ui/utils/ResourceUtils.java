package com.music.player.haige.mvp.ui.utils;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.View;

import com.music.player.haige.R;
import com.music.player.haige.app.HaigeApplication;

import java.util.Locale;


/**
 * Created by ben on 8/15/17.
 */

public class ResourceUtils {

    private static float mDensity;
    private static int densityDpi;
    private static int mScreenWidth;
    private static int mScreenHeight;

    public static String[] getStringArray(int arrayId) {
        return getResources().getStringArray(arrayId);
    }

    public static String resourceString(int strId) {
        return HaigeApplication.getInstance().getString(strId);
    }

    public static Resources getResources() {
        return HaigeApplication.getInstance().getResources();
    }

    public static Drawable getDrawable(int resId) {
        return getResources().getDrawable(resId);
    }

    public static float getDensity() {
        if (mDensity <= 0) {
            mDensity = getResources().getDisplayMetrics().density;
        }
        return mDensity;
    }

    public static int getDensityDpi() {
        if (densityDpi <= 0) {
            densityDpi = getResources().getDisplayMetrics().densityDpi;
        }
        return densityDpi;
    }

    public static float dp2PX(float dp) {
        return getDensity() * dp;
    }

    public static int getScreenWidth() {
        if (mScreenWidth <= 0) {
            mScreenWidth = getResources().getDisplayMetrics().widthPixels;
        }
        return mScreenWidth;
    }

    //小心使用，某些机型上可能？
    public static int getScreenHeight() {
        if (mScreenHeight <= 0) {
            mScreenHeight = getResources().getDisplayMetrics().heightPixels;
        }
        return mScreenHeight;
    }

    public static int getColor(int colorId) {
        return getResources().getColor(colorId);
    }

    public static ColorStateList getColorStateList(int colorId) {
        return getResources().getColorStateList(colorId);
    }

    //屏幕是否低于720
    public static boolean isLow720Screen() {
        return getDensity() < 2.0;
    }

    public static int dpToPX(float dp) {
        return Math.round(getDensity() * dp);
    }

    public static float getDimen(int resId) {
        return getResources().getDimension(resId);
    }

    //======================string format相关======================
    public static String resourceString(int resId, Object... formatArgs) {
        return getResources().getString(resId, formatArgs);
    }

    public static String resourceString(String raw, Object... formatArgs) {
        return String.format(raw, formatArgs);
    }

    public static String resourceString(Locale locale, int resId, Object... formatArgs) {
        String raw = resourceString(resId);
        return String.format(locale, raw, formatArgs);
    }

    //屏幕是否低于720
    public static boolean isLowAndEqual720Screen() {
        return getDensity() <= 2.0;
    }



    public static String getString(Context context, String stringName) {
        Resources resources = context.getResources();
        int stringId = getStringId(context, stringName);
        if (stringId != 0) {
            return resources.getString(stringId);
        }
        return context.getString(R.string.app_name);
    }

    public static String getString(Context context, String stringName, Object... formatArgs) throws Resources.NotFoundException {
        Resources resources = context.getResources();
        String string = resources.getString(getStringId(context, stringName), formatArgs);
        return string;
    }

    public static int getStringId(Context context, String stringName) {
        Resources resources = context.getResources();
        return resources.getIdentifier(stringName, "string", context.getPackageName());
    }

    public static int getStringArrayId(String itemName) {
        Context context = HaigeApplication.getInstance();
        Resources resources = context.getResources();
        return resources.getIdentifier(itemName, "array", context.getPackageName());
    }

    public static int getIconId(Context context, String iconName) {
        Resources resources = context.getResources();
        return resources.getIdentifier(iconName, "mipmap", context.getPackageName());
    }

    public static boolean isRtl(Context context) {
        if (context != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return context.getResources().getConfiguration().getLayoutDirection() == View.LAYOUT_DIRECTION_RTL;
        }
        return false;
    }

    public static void forceRtl(Context context, View view) {
        if (isRtl(context)) {
            view.setRotationY(180);
        }
    }
}
