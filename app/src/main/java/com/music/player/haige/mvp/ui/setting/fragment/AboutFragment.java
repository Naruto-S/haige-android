package com.music.player.haige.mvp.ui.setting.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hc.core.di.component.AppComponent;
import com.hc.core.utils.DeviceUtils;
import com.music.player.haige.R;
import com.music.player.haige.app.EventBusTags;
import com.music.player.haige.app.base.AppBaseFragment;
import com.music.player.haige.di.component.DaggerCommonComponent;
import com.music.player.haige.di.module.CommonModule;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.presenter.AboutFPresenter;

import org.simple.eventbus.EventBus;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * ================================================
 * Created by huangcong on 2018/3/31.
 * ================================================
 */

public class AboutFragment extends AppBaseFragment<AboutFPresenter> implements MVContract.CommonView {

    @BindView(R.id.app_fragment_about_version_tv)
    TextView mVersionTv;

    public static AboutFragment newInstance() {
        AboutFragment fragment = new AboutFragment();
        return fragment;
    }

    @Override
    public void setupFragmentComponent(AppComponent appComponent) {
        DaggerCommonComponent.builder()
                .appComponent(appComponent)
                .commonModule(new CommonModule(this))
                .build()
                .inject(this);
    }

    @Override
    public View initView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.app_fragment_about, container, false);
    }

    @Override
    public void initData(Bundle savedInstanceState) {
        mVersionTv.setText(getString(R.string.app_about_text_version, DeviceUtils.getVersionName(getContext())));
    }

    @OnClick({R.id.app_fragment_about_back_iv, R.id.app_fragment_about_score_layout,
            R.id.app_fragment_about_privacy_layout, R.id.app_fragment_about_copyright_layout})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.app_fragment_about_copyright_layout:
                EventBus.getDefault().post(EventBusTags.MAIN.SETTING_COPYRIGHT);
                break;
            case R.id.app_fragment_about_privacy_layout:
                EventBus.getDefault().post(EventBusTags.MAIN.SETTING_POLICY);
                break;
            case R.id.app_fragment_about_back_iv:
                EventBus.getDefault().post(EventBusTags.MAIN.BACK);
                break;
        }
    }
}
