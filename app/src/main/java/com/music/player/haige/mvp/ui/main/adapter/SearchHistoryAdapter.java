package com.music.player.haige.mvp.ui.main.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.music.player.haige.R;

import java.util.ArrayList;

/**
 * =============================
 * Created by huangcong on 2018/3/10.
 * =============================
 */

public class SearchHistoryAdapter extends RecyclerView.Adapter<SearchHistoryAdapter.SearchHistoryViewHolder> {

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private OnHistoryItemClickListener mOnItemClickListener;
    private ArrayList<String> mHistoryKeyWords;

    public SearchHistoryAdapter(Context context, OnHistoryItemClickListener listener) {
        this.mContext = context;
        this.mLayoutInflater = LayoutInflater.from(context);
        this.mOnItemClickListener = listener;
    }

    public void refreshData(ArrayList<String> historyKeyWords) {
        this.mHistoryKeyWords = historyKeyWords;
        notifyDataSetChanged();
    }

    public void clearData() {
        if (this.mHistoryKeyWords != null) {
            this.mHistoryKeyWords.clear();
            this.mHistoryKeyWords = null;
        }
        notifyDataSetChanged();
    }

    public ArrayList<String> getData() {
        return this.mHistoryKeyWords;
    }

    @Override
    public int getItemCount() {
        return mHistoryKeyWords == null ? 0 : mHistoryKeyWords.size();
    }

    @Override
    public SearchHistoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mLayoutInflater.inflate(R.layout.app_item_search_history, parent, false);
        return new SearchHistoryViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SearchHistoryViewHolder holder, int position) {
        holder.textView.setText(mHistoryKeyWords.get(position));
    }

    class SearchHistoryViewHolder extends RecyclerView.ViewHolder {

        TextView textView;

        public SearchHistoryViewHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.app_item_text_tv);
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnItemClickListener != null) {
                        int position = getAdapterPosition();
                        // BugFix：清空历史按钮和历史item一起点击，可能造成空指针或者下标越界
                        if (mHistoryKeyWords != null
                                && position >= 0 && position < mHistoryKeyWords.size()) {
                            mOnItemClickListener.onHistoryItemClick(mHistoryKeyWords.get(position));
                        }
                    }
                }
            });
        }
    }

    public interface OnHistoryItemClickListener {
        void onHistoryItemClick(String historyKeyWord);
    }
}
