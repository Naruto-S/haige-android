package com.music.player.haige.mvp.model.entity.user;

import com.google.gson.annotations.SerializedName;

public class ModifyUserRequest {

    @SerializedName("user_id")
    private String userId;
    @SerializedName("nickname")
    private String nickName;
    @SerializedName("avatar_url")
    private String avatarUrl;
    @SerializedName("gender")
    private String gender;
    @SerializedName("age")
    private String age;
    @SerializedName("year")
    private String year;
    @SerializedName("city")
    private String city;
    @SerializedName("province")
    private String province;
    @SerializedName("birthday")
    private String birthday;
    @SerializedName("desc")
    private String desc;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return "ModifyUserRequest{" +
                "userId='" + userId + '\'' +
                ", nickName='" + nickName + '\'' +
                ", avatarUrl='" + avatarUrl + '\'' +
                ", gender='" + gender + '\'' +
                ", age='" + age + '\'' +
                ", year='" + year + '\'' +
                ", city='" + city + '\'' +
                ", province='" + province + '\'' +
                ", birthday='" + birthday + '\'' +
                ", desc='" + desc + '\'' +
                '}';
    }
}
