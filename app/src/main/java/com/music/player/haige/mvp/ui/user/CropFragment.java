package com.music.player.haige.mvp.ui.user;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.widget.Toast;

import com.hc.core.di.component.AppComponent;
import com.music.player.haige.R;
import com.music.player.haige.app.EventBusTags;
import com.music.player.haige.app.base.AppBaseFragment;
import com.music.player.haige.app.base.pref.UserPrefHelper;
import com.music.player.haige.app.constants.IntentConstants;
import com.music.player.haige.di.component.DaggerCropComponent;
import com.music.player.haige.di.module.CropModule;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.Api;
import com.music.player.haige.mvp.model.entity.user.UserBean;
import com.music.player.haige.mvp.presenter.CropPresenter;
import com.music.player.haige.mvp.ui.user.crop.callback.BitmapCropCallback;
import com.music.player.haige.mvp.ui.user.crop.view.GestureCropImageView;
import com.music.player.haige.mvp.ui.user.crop.view.OverlayView;
import com.music.player.haige.mvp.ui.user.crop.view.TransformImageView;
import com.music.player.haige.mvp.ui.user.crop.view.UCropView;
import com.music.player.haige.mvp.ui.user.utils.AvatarUtils;
import com.music.player.haige.mvp.ui.utils.Utils;

import org.simple.eventbus.EventBus;

import butterknife.BindView;
import butterknife.OnClick;

public class CropFragment extends AppBaseFragment<CropPresenter> implements MVContract.CropView {

    @BindView(R.id.view_crop)
    UCropView mCropView;

    private Uri mInputUri;
    private Uri mOutputUri;
    private GestureCropImageView mGestureCropImageView;
    private OverlayView mOverlayView;
    private ProgressDialog mProgressDialog;

    public static CropFragment newInstance(Uri uri) {
        CropFragment fragment = new CropFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(IntentConstants.EXTRA_CHOOSE_AVATAR_PATH, uri);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void setupFragmentComponent(AppComponent component) {
        DaggerCropComponent
                .builder()
                .appComponent(component)
                .cropModule(new CropModule(this))
                .build()
                .inject(this);
    }

    @Override
    public View initView(LayoutInflater inflater, ViewGroup group, Bundle bundle) {
        return inflater.inflate(R.layout.app_fragment_crop, group, false);
    }

    @Override
    public void initData(Bundle bundle) {

        if (Utils.isNotNull(getArguments())) {
            mInputUri = getArguments().getParcelable(IntentConstants.EXTRA_CHOOSE_AVATAR_PATH);
        }
        mOutputUri = Uri.parse(AvatarUtils.getAvatarImageFilePath());

        mGestureCropImageView = mCropView.getCropImageView();
        mOverlayView = mCropView.getOverlayView();
        mGestureCropImageView.setTargetAspectRatio(1f);          // 图片显示的比例
        mGestureCropImageView.setRotateEnabled(false);
        mGestureCropImageView.setMaxScaleMultiplier(3f);         // 最大放大倍数
        mGestureCropImageView.setMaxResultImageSizeX(AvatarUtils.AVATAR_MAX_SIZE);
        mGestureCropImageView.setMaxResultImageSizeY(AvatarUtils.AVATAR_MAX_SIZE);
        mGestureCropImageView.setTransformImageListener(new TransformImageView.TransformImageListener() {
            @Override
            public void onLoadComplete() {
                mCropView.animate().alpha(1).setDuration(300).setInterpolator(new AccelerateInterpolator());
            }

            @Override
            public void onLoadFailure(@NonNull Exception e) {

            }

            @Override
            public void onRotate(float currentAngle) {

            }

            @Override
            public void onScale(float currentScale) {

            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();

        if (mInputUri != null && mOutputUri != null) {
            if (!AvatarUtils.checkAvatarLegal(getContext(), mInputUri)) {
                Toast.makeText(getContext(), R.string.user_avatar_choose_error, Toast.LENGTH_SHORT).show();
                EventBus.getDefault().post(EventBusTags.MAIN.BACK);
            }

            try {
                Uri resultUri = Uri.parse(AvatarUtils.getCropAvatarImageFilePath());
                mGestureCropImageView.setImageUri(mInputUri, resultUri);
            } catch (Exception e) {
                EventBus.getDefault().post(EventBusTags.MAIN.BACK);
            }
        } else {
            EventBus.getDefault().post(EventBusTags.MAIN.BACK);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void showProgress() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getContext());
        }
        mProgressDialog.setMessage(getResources().getString(R.string.processing));
        mProgressDialog.setCancelable(false);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    @Override
    public void hideProgress() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void onSuccess(Api.Action action, Object o) {
        super.onSuccess(action, o);
        switch (action) {
            case UPLOAD_AVATAR:
                String url = (String) o;
                UserBean userBean = UserPrefHelper.getMeUser();
                if (Utils.isNotNull(userBean)) {
                    userBean.setAvatarUrl(url);
                    UserPrefHelper.putMeUser(userBean);
                    EventBus.getDefault().post(EventBusTags.UI.EDIT_UPDATE_AVATAR);
                    EventBus.getDefault().post(EventBusTags.MAIN.BACK);
                }
                break;
        }
        hideProgress();
    }

    @Override
    public void onError(Api.Action action, String code, String message) {
        super.onError(action, code, message);
        hideProgress();
    }

    @OnClick({R.id.app_fragment_back_iv, R.id.app_fragment_save_tv})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.app_fragment_back_iv:
                EventBus.getDefault().post(EventBusTags.MAIN.BACK);
                break;
            case R.id.app_fragment_save_tv:
                cropAndSaveImage();
                break;
        }
    }

    private void cropAndSaveImage() {
        mGestureCropImageView.cropAndSaveImage(new BitmapCropCallback() {
            @Override
            public void onBitmapCropped(@NonNull Bitmap resultBitmap, Bitmap mThumnailBitmap, int imageWidth, int imageHeight) {
                mPresenter.saveAndUploadImage(mOutputUri, resultBitmap);
            }

            @Override
            public void onCropFailure(@NonNull Throwable t) {
                hideProgress();
            }
        });
    }
}
