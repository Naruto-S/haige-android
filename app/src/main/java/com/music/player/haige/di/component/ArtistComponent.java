package com.music.player.haige.di.component;

import com.hc.core.di.component.AppComponent;
import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.di.module.ArtistModule;
import com.music.player.haige.mvp.ui.music.fragment.ArtistFragment;

import dagger.Component;

/**
 * ================================================
 * Created by huangcong on 2018/3/1.
 * ================================================
 */
@FragmentScope
@Component(modules = {ArtistModule.class}, dependencies = AppComponent.class)
public interface ArtistComponent {

    void inject(ArtistFragment fragment);
}
