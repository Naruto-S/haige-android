package com.music.player.haige.app.upload;

import com.music.player.haige.app.image.utils.FileConstants;
import com.music.player.haige.mvp.ui.utils.DebugLogger;
import com.music.player.haige.mvp.ui.utils.Utils;

import java.io.File;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by liumingkong on 2017/4/18.
 */

public class FileBaseService {

    public static final String CONTENT_TYPE_IMAGE_PNG = "image/png";
    private static final String CONTENT_TYPE_IMAGE_JPG = "image/jpeg";
    private static final String CONTENT_TYPE_IMAGE_GIF = "image/gif";
    private static final String CONTENT_TYPE_VIDEO_MP4 = "video/mpeg4";
    private static final String CONTENT_TYPE_AUDIO_MP3 = "audio/mp3";

    public static boolean buildFileRequestBody(MultipartBody.Builder builder,
                                               HashMap<String, String> urlParams,
                                               String paramName, String filePath,
                                               FileUploadHandler apiBaseHandler) {
        try {
            ResourceType resourceType = ResourceType.UNKNOWN;
            File file = new File(filePath);
            if (file.exists()) {
                String filename = file.getName().toLowerCase();
                String strContentType = "";
                if (filename.endsWith(".jpeg") || filename.endsWith(".jpg")) {
                    strContentType = CONTENT_TYPE_IMAGE_JPG;
                    resourceType = ResourceType.IMAGE;
                } else if (filename.endsWith(".png")) {
                    strContentType = CONTENT_TYPE_IMAGE_PNG;
                    resourceType = ResourceType.IMAGE;
                } else if (filename.endsWith(".mp4")) {
                    strContentType = CONTENT_TYPE_VIDEO_MP4;
                    resourceType = ResourceType.VIDEO;
                } else if(filename.endsWith(".amr")){
                    strContentType = CONTENT_TYPE_AUDIO_MP3;
                    resourceType = ResourceType.AUDIO;
                } else if (filename.endsWith(".gif")) {
                    strContentType = CONTENT_TYPE_IMAGE_GIF;
                    resourceType = ResourceType.IMAGE;
                }

                if (Utils.ensureNotNull(urlParams)) {
                    urlParams.put(FileConstants.RESOURCE, String.valueOf(resourceType.value()));
                }
                RequestBody requestBody = RequestBody.create(MediaType.parse(strContentType), file);
                FileRequestBody apiFileRequestBody = new FileRequestBody(requestBody, apiBaseHandler);
                builder.addFormDataPart(paramName, filename, apiFileRequestBody);
                return true;
            }
        } catch (Throwable e) {
            DebugLogger.e(e);
        }
        return false;
    }


    public static MultipartBody.Builder getBasicMultipartBodyBuilder() {
        MultipartBody.Builder builder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM);
        return builder;
    }
}
