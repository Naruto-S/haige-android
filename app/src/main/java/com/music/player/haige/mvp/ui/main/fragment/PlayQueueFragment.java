package com.music.player.haige.mvp.ui.main.fragment;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.hc.core.di.component.AppComponent;
import com.hc.core.utils.CoreUtils;
import com.music.player.haige.R;
import com.music.player.haige.app.EventBusTags;
import com.music.player.haige.app.base.AppBaseFragment;
import com.music.player.haige.app.image.loader.BaseImageLoader;
import com.music.player.haige.app.image.release.DisplayOptions;
import com.music.player.haige.app.image.widget.HaigeImageView;
import com.music.player.haige.di.component.DaggerPlayQueueComponent;
import com.music.player.haige.di.module.PlayQueueModule;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.Api;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.model.utils.CommonUtils;
import com.music.player.haige.mvp.presenter.PlayQueueFPresenter;
import com.music.player.haige.mvp.ui.music.adapter.PlayQueueSongsAdapter;
import com.music.player.haige.mvp.ui.music.service.MusicDataManager;
import com.music.player.haige.mvp.ui.music.service.MusicServiceConnection;
import com.music.player.haige.mvp.ui.utils.DebugLogger;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;
import com.music.player.haige.mvp.ui.utils.Utils;
import com.music.player.haige.mvp.ui.widget.StatusLayout;
import com.music.player.haige.mvp.ui.widget.dialog.SystemDialog;
import com.music.player.haige.mvp.ui.widget.loadingdrawable.render.LoadingDrawable;
import com.music.player.haige.mvp.ui.widget.loadingdrawable.render.circle.rotate.MaterialLoadingRenderer2;

import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import timber.log.Timber;

public class PlayQueueFragment extends AppBaseFragment<PlayQueueFPresenter> implements MVContract.PlayQueueView,
        PlayQueueSongsAdapter.ItemClickListener {

    @BindView(R.id.clear_all)
    ImageView clearAll;

    @BindView(R.id.recycler_view_songs)
    RecyclerView mRecyclerView;

    @BindView(R.id.app_layout_sl)
    StatusLayout mStatusLayout;

    @BindView(R.id.hiv_music_cover)
    HaigeImageView mMusicCoverHiv;

    @BindView(R.id.iv_pause)
    ImageView mPauseIv;

    @BindView(R.id.iv_play_mode_repeat)
    ImageView mPlayModeIv;

    @BindView(R.id.app_layout_music_info_duration_cur_tv)
    TextView mDurationCurTv;

    @BindView(R.id.app_layout_music_info_duration_sb)
    SeekBar mDurationSeekBar;

    @BindView(R.id.app_layout_music_info_duration_tv)
    TextView mDurationTv;

    private PlayQueueSongsAdapter mAdapter;
    private PlayMode mPlayMode;
    private ArrayList<Song> mPlaySongs;

    private int mCurrentPosition = 0;
    private boolean isStartTrackingTouch = false;

    public enum PlayMode {
        REPEAT_ALL,
        CURRENT,
        SHUFFLE
    }

    public static PlayQueueFragment newInstance() {
        PlayQueueFragment fragment = new PlayQueueFragment();
        return fragment;
    }

    @Override
    public void setupFragmentComponent(AppComponent appComponent) {
        DaggerPlayQueueComponent
                .builder()
                .appComponent(appComponent)
                .playQueueModule(new PlayQueueModule(this))
                .build()
                .inject(this);
    }

    @Override
    public View initView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.app_dialog_playqueue, container, false);
    }

    @Override
    public void initData(Bundle savedInstanceState) {
        mStatusLayout.setOnStatusClickListener(new StatusLayout.OnStatusClickListener() {
            @Override
            public void onEmptyClick() {
                EventBus.getDefault().post(EventBusTags.UI.GO_TO_RECOMMEND);
                EventBus.getDefault().post(EventBusTags.MAIN.BACK);
            }

            @Override
            public void onReloadClick() {

            }
        });
        mDurationSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                isStartTrackingTouch = true;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                MusicServiceConnection.seek((long) seekBar.getProgress());
                isStartTrackingTouch = false;
            }
        });

        long duration = MusicServiceConnection.duration();
        mDurationSeekBar.setMax((int) duration);
        mDurationTv.setText(CommonUtils.makeShortTimeString(getContext(), duration));


        mAdapter = new PlayQueueSongsAdapter((AppCompatActivity) getActivity());
        mAdapter.setOnItemClickListener(this);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(mAdapter);

        repeatModeChanged();

        mAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeRemoved(int positionStart, int itemCount) {
                if (mAdapter.getItemCount() == 0) {
                    EventBus.getDefault().post(EventBusTags.MAIN.BACK);
                }
            }
        });

        mPresenter.loadCurrentMusic();
        mPresenter.startUpateMusicTime();
    }

    private void repeatModeChanged() {
        int shuffleMode = MusicServiceConnection.getShuffleMode();
        int repeatMode = MusicServiceConnection.getRepeatMode();
        if (shuffleMode == MusicDataManager.SHUFFLE_NONE) {
            if (repeatMode == MusicDataManager.REPEAT_CURRENT) {
                //单曲播放模式
                mPlayModeIv.setImageDrawable(getResources().getDrawable(R.drawable.ic_play_single));
                mPlayMode = PlayMode.CURRENT;
            } else {
                //顺序播放模式
                mPlayModeIv.setImageDrawable(getResources().getDrawable(R.drawable.ic_play_sequential));
                mPlayMode = PlayMode.REPEAT_ALL;
            }
        } else if (shuffleMode == MusicDataManager.SHUFFLE_NORMAL || shuffleMode == MusicDataManager.SHUFFLE_AUTO) {
            //随机播放模式
            mPlayModeIv.setImageDrawable(getResources().getDrawable(R.drawable.ic_play_random));
            mPlayMode = PlayMode.SHUFFLE;
        }
    }

    @Override
    public void onSuccess(Api.Action action, Object o) {
        super.onSuccess(action, o);
        if (action == Api.Action.MUSIC_PLAY_QUEUE) {
            ArrayList<Song> songs = (ArrayList<Song>) o;
            if (Utils.isNotEmptyCollection(songs)) {
                if (MusicServiceConnection.isShort()) {
                    mPlaySongs = new ArrayList<>();
                    mPlaySongs.add(songs.get(0));
                } else {
                    mPlaySongs = songs;
                }

                mAdapter.setSongList(mPlaySongs);
                startMusicTop();

                for (int i = 0; i < (mPlaySongs).size(); i++) {
                    Song song = mPlaySongs.get(i);
                    if (Utils.isNotNull(song) && MusicServiceConnection.getCurrentAudioId() == song.songId) {
                        mRecyclerView.scrollToPosition(i);
                        break;
                    }
                }

                mRecyclerView.setVisibility(View.VISIBLE);
                mStatusLayout.showStatusView(StatusLayout.STATUS.OK);
            } else {
                mRecyclerView.setVisibility(View.GONE);
                mStatusLayout.showStatusView(StatusLayout.STATUS.EMPTY);
            }
        }
    }

    @Override
    public void onError(Api.Action action, String code, String message) {
        super.onError(action, code, message);
    }

    @Subscriber
    public void onMetaChangedEvent(EventBusTags.MetaChangedEvent event) {
        Timber.e("===>>> onMetaChangedEvent " + event);

        try {
            int position = MusicServiceConnection.getQueuePosition();
            if (position >= 0 && mPlaySongs != null && mPlaySongs.size() > position
                    && event.getSongId() == mPlaySongs.get(position).getSongId()) {
                mCurrentPosition = position;

                mAdapter.notifyItemChanged(mCurrentPosition);
                mRecyclerView.scrollToPosition(mCurrentPosition);

                if (MusicServiceConnection.isPlaying()) {
                    mPauseIv.setImageResource(R.drawable.ic_play_suspend);
                } else {
                    mPauseIv.setImageResource(R.drawable.ic_play_start);
                }

                // 背景图
                BaseImageLoader.displaySimpleImage(
                        event.getSongCover(),
                        new DisplayOptions.Builder()
                                .showImageOnLoading(R.drawable.app_ic_album_default)
                                .showImageOnFail(R.drawable.app_ic_album_default),
                        mMusicCoverHiv);

                repeatModeChanged();

                long duration = MusicServiceConnection.duration();
                mDurationSeekBar.setMax((int) duration);
                mDurationTv.setText(CommonUtils.makeShortTimeString(getContext(), duration));
            }
        } catch (Exception e) {
            DebugLogger.e(e);
        }
    }

    @Subscriber
    public void onMusicIsLoadingEvent(EventBusTags.MusicIsLoadingEvent event) {
        Timber.e("===>>> onMusicIsLoadingEvent. isLoading:" + event.isLoading());
        try {
            if (event.isLoading()) {
                LoadingDrawable loadingDrawable = new LoadingDrawable(new
                        MaterialLoadingRenderer2.Builder(getContext())
                        .setWidth(CoreUtils.dip2px(10))
                        .setHeight(CoreUtils.dip2px(10))
                        .setCenterRadius(CoreUtils.dip2px(5))
                        .setStrokeWidth(CoreUtils.dip2px(1))
                        .build());
                loadingDrawable.setBounds(0, 0,
                        CoreUtils.dip2px(10),
                        CoreUtils.dip2px(10));
                mDurationSeekBar.setThumb(loadingDrawable);
                loadingDrawable.start();
            } else {
                mDurationSeekBar.setThumb(getResources().getDrawable(R.drawable.app_selector_music_info_thumb));
            }
        } catch (Exception e) {
            DebugLogger.e(e);
        }
    }

    @Subscriber
    public void onRepeatModeChangedEvent(EventBusTags.RepeatModeChangedEvent event) {
        repeatModeChanged();
    }

    @OnClick({R.id.app_dialog_play_queue_back_iv, R.id.iv_play_mode, R.id.clear_all, R.id.app_layout_empty_btn,
            R.id.iv_previous, R.id.iv_pause, R.id.iv_next, R.id.iv_play_mode_repeat, R.id.hiv_music_cover})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.hiv_music_cover:
                EventBus.getDefault().post(EventBusTags.PlayMain.CURRENT_PLAY);
                break;
            case R.id.app_dialog_play_queue_back_iv:
                EventBus.getDefault().post(EventBusTags.MAIN.BACK);
                break;
            case R.id.iv_play_mode_repeat:
                if (mPlayMode == PlayMode.REPEAT_ALL) {
                    mPlayModeIv.setImageDrawable(getResources().getDrawable(R.drawable.ic_play_single));
                    MusicServiceConnection.setShuffleMode(MusicDataManager.SHUFFLE_NONE);
                    MusicServiceConnection.setRepeatMode(MusicDataManager.REPEAT_CURRENT);
                    Toast.makeText(getContext(), R.string.app_repeat_current, Toast.LENGTH_SHORT).show();
                    mPlayMode = PlayMode.CURRENT;
                } else if (mPlayMode == PlayMode.CURRENT) {
                    mPlayModeIv.setImageDrawable(getResources().getDrawable(R.drawable.ic_play_random));
                    MusicServiceConnection.setShuffleMode(MusicDataManager.SHUFFLE_NORMAL);
                    MusicServiceConnection.setRepeatMode(MusicDataManager.REPEAT_ALL);
                    Toast.makeText(getContext(), R.string.app_shuffle_all, Toast.LENGTH_SHORT).show();
                    mPlayMode = PlayMode.SHUFFLE;
                } else if (mPlayMode == PlayMode.SHUFFLE) {
                    mPlayModeIv.setImageDrawable(getResources().getDrawable(R.drawable.ic_play_sequential));
                    MusicServiceConnection.setShuffleMode(MusicDataManager.SHUFFLE_NONE);
                    Toast.makeText(getContext(), R.string.app_repeat_all, Toast.LENGTH_SHORT).show();
                    mPlayMode = PlayMode.REPEAT_ALL;
                }
                break;
            case R.id.clear_all:
                new SystemDialog().create(getChildFragmentManager())
                        .setContent(ResourceUtils.resourceString(R.string.app_clear_song_queue))
                        .setOk(ResourceUtils.resourceString(R.string.app_confirm))
                        .setCancle(ResourceUtils.resourceString(R.string.app_cancel))
                        .setOnOkListener(() -> {
                            MusicServiceConnection.clearQueue();
                            mAdapter.clear();
                            mRecyclerView.setVisibility(View.GONE);
                            mStatusLayout.showStatusView(StatusLayout.STATUS.EMPTY);
                        })
                        .setOnCancelListener(() -> {
                        })
                        .show();
                break;

            case R.id.iv_previous:
                playPre();
                break;

            case R.id.iv_pause:
                if (MusicServiceConnection.isPlaying()) {
                    mPauseIv.setImageResource(R.drawable.ic_play_start);
                } else {
                    mPauseIv.setImageResource(R.drawable.ic_play_suspend);
                }
                playPause();
                break;

            case R.id.iv_next:
                playNext();
                break;
        }
    }

    @Override
    public void onItemClick(int position, View view) {
        MusicServiceConnection.playAll(mAdapter.getSongs(), position, false);
        EventBus.getDefault().post(EventBusTags.PlayMain.CURRENT_PLAY);
    }

    private void startMusicTop() {
        mCurrentPosition = MusicServiceConnection.getQueuePosition();
        if (MusicServiceConnection.isPlaying()) {
            playMusic();
        } else {
            Song song;
            if (mCurrentPosition >= 0 && mCurrentPosition < mPlaySongs.size()) {
                song = mPlaySongs.get(mCurrentPosition);
                if (song != null) {
                    EventBusTags.MetaChangedEvent event = new EventBusTags.MetaChangedEvent(
                            song.songId,
                            song.musicName,
                            song.artistName,
                            song.albumId,
                            song.isLocal,
                            song.musicCover,
                            song.likeCount,
                            song.shareCount,
                            song.downloadCount,
                            song.musicPath
                    );
                    onMetaChangedEvent(event);
                }
            }
        }
    }

    /**
     * 播放音乐
     */
    private void playMusic() {
        if (MusicServiceConnection.isShort()) {
            MusicServiceConnection.playAll(mPlaySongs, mCurrentPosition, false, true);
        } else {
            MusicServiceConnection.playAll(mPlaySongs, mCurrentPosition, false);
        }
    }

    private void playNext() {
        if (Utils.isEmptyCollection(mPlaySongs)) return;

        mCurrentPosition++;
        if (mCurrentPosition < mPlaySongs.size()) {
            playMusic();
        } else {
            mCurrentPosition = mPlaySongs.size() - 1;
        }
    }

    private void playPre() {
        if (Utils.isEmptyCollection(mPlaySongs)) return;

        mCurrentPosition--;
        if (mCurrentPosition >= 0) {
            playMusic();
        } else {
            mCurrentPosition = 0;
        }
    }

    private void playPause() {
        MusicServiceConnection.playOrPause();
    }

    @Subscriber
    public void onSubscriber(EventBusTags.MetaChangedEvent event) {
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onUpateMusicTime() {
        try {
            if (MusicServiceConnection.getCurrentAudioId() == mPlaySongs.get(mCurrentPosition).getSongId()) {
                long position = MusicServiceConnection.position();
                if (mDurationSeekBar.getMax() <= 0) {
                    long duration = MusicServiceConnection.duration();
                    Timber.e("===>>> mUpdateProgress duration:" + duration);
                    mDurationSeekBar.setMax((int) MusicServiceConnection.duration());
                    mDurationTv.setText(CommonUtils.makeShortTimeString(getContext(), duration));
                }

                if (!isStartTrackingTouch) {
                    mDurationSeekBar.setProgress((int) position);
                }
                mDurationCurTv.setText(CommonUtils.makeShortTimeString(getContext(), position));
            }
        } catch (Exception e) {
            DebugLogger.e(TAG, "mUpdateProgress：" + e);
        }
    }
}
