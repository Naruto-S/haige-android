package com.music.player.haige.mvp.model.entity.user;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LoginWXBean implements Serializable {

    @SerializedName("token")
    private String token;
    @SerializedName("openid")
    private String openid;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }
}
