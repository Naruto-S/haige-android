package com.music.player.haige.app.event;

public class UpdateEditInfoEvent {

    private int type;
    private String cotent;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getCotent() {
        return cotent;
    }

    public void setCotent(String cotent) {
        this.cotent = cotent;
    }
}
