package com.music.player.haige.app.base.pref;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.music.player.haige.mvp.model.entity.user.UserBean;
import com.music.player.haige.mvp.ui.utils.DebugLogger;
import com.music.player.haige.mvp.ui.utils.Utils;

/**
 * Created by Naruto on 2017/10/1.
 */

public class UserPrefHelper extends Preferences {

    public static final String TAG = UserPrefHelper.class.getSimpleName();

    private static final String PREF_KEY_USER_ID = "PREF_KEY_USER_ID";

    private static final String PREF_KEY_USER_TOKEN = "PREF_KEY_USER_TOKEN";

    private static final String PREF_KEY_IS_LOGIN = "PREF_KEY_IS_LOGIN";

    private static final String PREF_KEY_USER_BEAN = "PREF_KEY_USER_BEAN";

    private static final String PREF_KEY_USER_LOGIN_TYPE = "PREF_KEY_USER_LOGIN_TYPE";    //登录类型：QQ、微信

    private static final String PREF_KEY_DOWNLOAD_MUSIC_COUNT = "PREF_KEY_DOWNLOAD_MUSIC_COUNT";    //下载音乐数量

    private static final String PREF_KEY_IS_SHOW_MINE_TOP_TIPS = "PREF_KEY_IS_SHOW_MINE_TOP_TIPS";    //是否显示我的页面顶部小蓝条

    public static void putMeUser(UserBean user) {
        if (Utils.isNotNull(user)) {
            putUserId(user.getUserId());
            putUserToken(user.getToken());

            Gson gson = new GsonBuilder().serializeNulls().create();
            saveString(PREF_KEY_USER_BEAN, gson.toJson(user));
        }
    }

    public static UserBean getMeUser() {
        Gson gson = new GsonBuilder().serializeNulls().create();
        return gson.fromJson(getString(PREF_KEY_USER_BEAN, ""), UserBean.class);
    }

    public static boolean isMe(UserBean user) {
        UserBean meUser = getMeUser();
        if (Utils.isNotNull(meUser)
                && Utils.isNotNull(user)
                && user.getUserId().equals(meUser.getUserId())) {
            return true;
        } else {
            return false;
        }
    }

    public static void putUserId(String flags) {
        saveString(PREF_KEY_USER_ID, flags);
    }

    public static String getUserId() {
        return getString(PREF_KEY_USER_ID, "");
    }

    public static void putUserToken(String flags) {
        saveString(PREF_KEY_USER_TOKEN, flags);
    }

    public static String getUserToken() {
        return getString(PREF_KEY_USER_TOKEN, "");
    }

    public static void putUserLoginType(String flags) {
        saveString(PREF_KEY_USER_LOGIN_TYPE, flags);
    }

    public static String getUserLoginType() {
        return getString(PREF_KEY_USER_LOGIN_TYPE, "");
    }

    public static void putIsLogin(boolean flags) {
        saveBoolean(PREF_KEY_IS_LOGIN, flags);
    }

    public static boolean isLogin() {
        return getBoolean(PREF_KEY_IS_LOGIN, false);
    }


    public static void putDownloadMusicCount(int flags) {
        DebugLogger.e(TAG, "下载了几首：" + flags);
        saveInt(PREF_KEY_DOWNLOAD_MUSIC_COUNT, flags);
    }

    public static int getDownloadMusicCount() {
        return getInt(PREF_KEY_DOWNLOAD_MUSIC_COUNT, 0);
    }

    public static void putIsShowMineTips(boolean flags) {
        saveBoolean(PREF_KEY_IS_SHOW_MINE_TOP_TIPS, flags);
    }

    public static boolean isShowMineTips() {
        return getBoolean(PREF_KEY_IS_SHOW_MINE_TOP_TIPS, true);
    }
}
