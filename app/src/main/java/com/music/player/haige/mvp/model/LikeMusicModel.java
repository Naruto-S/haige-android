package com.music.player.haige.mvp.model;

import com.hc.base.mvp.BaseModel;
import com.hc.core.integration.IRepositoryManager;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.service.ApiService;
import com.music.player.haige.mvp.model.entity.recommend.RecommendResponse;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * ================================================
 * Created by huangcong on 2018/2/27.
 * ================================================
 */

public class LikeMusicModel extends BaseModel implements MVContract.LikeMusicModel {

    @Inject
    public LikeMusicModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public Observable<RecommendResponse> getLikedList(String userId, int pageNo, int pageSize) {
        return Observable.just(mRepositoryManager.obtainRetrofitService(ApiService.class).getLikedList(userId, pageNo, pageSize))
                .flatMap(observable -> observable);
    }
}
