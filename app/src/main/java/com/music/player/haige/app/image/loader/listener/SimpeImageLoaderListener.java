package com.music.player.haige.app.image.loader.listener;

import android.graphics.drawable.Animatable;
import android.view.View;

import com.facebook.imagepipeline.image.ImageInfo;

/**
 * 分发成不同的回调
 * <p>
 * Created by ZaKi on 2016/10/14.
 */
public class SimpeImageLoaderListener extends OnImageLoaderListener {

    public void onLoadComplete() {

    }

    public void onLoadComplete(String uri) {

    }

    public void onLoadComplete(String uri, View view) {

    }

    public void onLoadComplete(String uri, int width, int height, View view) {

    }

    public void onLoadComplete(String uri, int width, int height, boolean isGif, View view) {

    }

    public void onLoadComplete(String uri, ImageInfo imageInfo, boolean isGif, Animatable animatable, View tagVview) {

    }

    public void onLoadFail() {

    }

    public void onLoadFail(String uri) {

    }

    public void onLoadFail(String uri, Throwable throwable) {

    }

    public void onLoadFail(String uri, Throwable throwable, View tagVview) {

    }

    //不能重写
    @Override
    public final void onImageLoadComplete(String uri, ImageInfo imageInfo, boolean isGif, Animatable animatable, View tagVview) {
        onLoadComplete();
        onLoadComplete(uri);
        onLoadComplete(uri, tagVview);
        if (imageInfo != null) {
            onLoadComplete(uri, imageInfo.getWidth(), imageInfo.getHeight(), tagVview);
            onLoadComplete(uri, imageInfo.getWidth(), imageInfo.getHeight(), isGif, tagVview);
        } else {
            onLoadComplete(uri, 0, 0, tagVview);
            onLoadComplete(uri, 0, 0, isGif, tagVview);
        }
        onLoadComplete(uri, imageInfo, isGif, animatable, tagVview);
    }

    //不能重写
    @Override
    public final void onImageLoadFail(String uri, Throwable throwable, View tagVview) {
        onLoadFail();
        onLoadFail(uri);
        onLoadFail(uri, throwable);
        onLoadFail(uri, throwable, tagVview);
    }
}
