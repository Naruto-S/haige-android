package com.music.player.haige.di.component;

import com.hc.core.di.component.AppComponent;
import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.di.module.SongsModule;
import com.music.player.haige.mvp.ui.upload.UploadListActivity;
import com.music.player.haige.mvp.ui.music.fragment.SongsFragment;

import dagger.Component;

/**
 * ================================================
 * Created by huangcong on 2018/2/27.
 * ================================================
 */

@FragmentScope
@Component(modules = {SongsModule.class}, dependencies = AppComponent.class)
public interface SongsComponent {

    void inject(SongsFragment fragment);

    void inject(UploadListActivity activity);
}
