package com.music.player.haige.di.module;

import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.CommentModel;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Naruto on 2018/5/5.
 */
@Module
public class CommentDialogModule {
    private MVContract.CommentView view;

    public CommentDialogModule(MVContract.CommentView view){
        this.view = view;
    }

    @FragmentScope
    @Provides
    MVContract.CommentView provideView(){
        return view;
    }

    @FragmentScope
    @Provides
    MVContract.CommentModel provideModel(CommentModel model){
        return model;
    }
}
