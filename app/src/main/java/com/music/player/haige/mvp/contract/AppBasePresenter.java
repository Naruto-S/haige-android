package com.music.player.haige.mvp.contract;

import com.hc.base.mvp.BasePresenter;
import com.hc.core.mvp.IModel;
import com.hc.core.utils.DeviceUtils;
import com.hc.core.utils.RxLifecycleUtils;
import com.music.player.haige.app.ErrorHandleSubscriberImpl;
import com.music.player.haige.app.HaigeApplication;
import com.music.player.haige.mvp.model.api.Api;
import com.music.player.haige.mvp.model.entity.BaseResponse;
import com.music.player.haige.mvp.ui.utils.DebugLogger;
import com.music.player.haige.mvp.ui.utils.Utils;
import com.trello.rxlifecycle2.LifecycleTransformer;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;
import me.jessyan.rxerrorhandler.handler.ErrorHandleSubscriber;

/**
 * ================================================
 * Created by huangcong on 2018/3/19.
 * ================================================
 */

public class AppBasePresenter<M extends IModel, V extends AppIView> extends BasePresenter<M, V> {

    @Inject
    protected RxErrorHandler mErrorHandler;

    public AppBasePresenter() {
        super();
    }

    public AppBasePresenter(V v) {
        super(v);
    }

    public AppBasePresenter(M m, V v) {
        super(m, v);
    }

    protected <T> Observable<T> buildObservable(Observable<T> observable) {
        return buildObservable(observable, true);
    }

    protected <T> Observable<T> buildObservable(Observable<T> observable, boolean showLoading) {
        LifecycleTransformer<T> transformer = RxLifecycleUtils.bindToLifecycle(mRootView);
        if (Utils.isNotNull(transformer)) {
            return observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(disposable -> {
                        if (showLoading && Utils.isNotNull(mRootView)) {
                            mRootView.showLoading();
                        }
                    })
                    .doFinally(() -> {
                        if (showLoading && Utils.isNotNull(mRootView)) {
                            mRootView.hideLoading();
                        }
                    })
                    .compose(transformer);
        } else {
            return observable.onErrorReturn(throwable -> {
                DebugLogger.e("AppBasePresenter", throwable);
                return null;
            });
        }
    }

    protected <T extends BaseResponse> void buildObservable(Observable<T> observable, Api.Action action) {
        buildObservable(observable, action, false);
    }

    protected <T extends BaseResponse> void buildObservable(Observable<T> observable, Api.Action action, boolean checkNetWork) {
        buildObservable(observable, action, checkNetWork, true);
    }

    protected <T extends BaseResponse> void buildObservable(Observable<T> observable, Api.Action action, boolean checkNetWork, boolean showLoading) {
        if (checkNetWork) {
            if (!DeviceUtils.netIsConnected(HaigeApplication.getInstance())) {
                mRootView.onError(Api.Action.NET_NO_CONNECT, Api.State.NET_NO_CONNECT.getCode(), Api.State.NET_NO_CONNECT.getMsg());
                return;
            }
        }
        buildObservable(observable, showLoading)
                .subscribe(new ErrorHandleSubscriber<T>(mErrorHandler) {
                    @Override
                    public void onNext(T response) {
                        if (response != null && response.getCode() == 200) {
                            mRootView.onSuccess(action, response);
                        } else {
                            mRootView.onError(action, String.valueOf(response.getCode()), response.getMsg());
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                        mRootView.onError(action, Api.State.ERROR.getCode(), Api.State.ERROR.getMsg() + " > " + t.getMessage());
                    }
                });
    }

    protected <T extends BaseResponse> void buildObservable(Observable<T> observable, Api.Action action, Object tag) {
        buildObservable(observable, action, tag, false);
    }

    protected <T extends BaseResponse> void buildObservable(Observable<T> observable, Api.Action action, Object tag, boolean checkNetWork) {
        if (checkNetWork) {
            if (!DeviceUtils.netIsConnected(HaigeApplication.getInstance())) {
                mRootView.onError(Api.Action.NET_NO_CONNECT, Api.State.NET_NO_CONNECT.getCode(), Api.State.NET_NO_CONNECT.getMsg());
                return;
            }
        }
        buildObservable(observable)
                .subscribe(new ErrorHandleSubscriberImpl<T>(mErrorHandler, tag) {
                    @Override
                    public void onNext(T response) {
                        super.onNext(response);
                        if (response != null && response.getCode() == 200) {
                            mRootView.onSuccess(action, response);
                        } else {
                            mRootView.onError(action, String.valueOf(response.getCode()), response.getMsg());
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                        mRootView.onError(action, "-1", t.getMessage());
                    }
                });
    }
}
