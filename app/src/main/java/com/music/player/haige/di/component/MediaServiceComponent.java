package com.music.player.haige.di.component;

import com.hc.core.di.component.AppComponent;
import com.hc.core.di.scope.ServiceScope;
import com.music.player.haige.di.module.MediaServiceModule;
import com.music.player.haige.mvp.ui.music.service.MusicService;

import dagger.Component;

/**
 * ================================================
 * Created by huangcong on 2018/3/3.
 * ================================================
 */
@ServiceScope
@Component(modules = {MediaServiceModule.class}, dependencies = AppComponent.class)
public interface MediaServiceComponent {

    void inject(MusicService service);
}
