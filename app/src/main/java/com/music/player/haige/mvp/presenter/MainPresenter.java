package com.music.player.haige.mvp.presenter;

import android.Manifest;

import com.hc.base.mvp.BasePresenter;
import com.hc.core.utils.PermissionUtil;
import com.music.player.haige.app.EventBusTags;
import com.music.player.haige.mvp.contract.MVContract;

import org.simple.eventbus.EventBus;

import java.util.List;

import javax.inject.Inject;

import me.jessyan.rxerrorhandler.core.RxErrorHandler;

/**
 * ================================================
 * Created by huangcong on 2018/2/22.
 * <p>
 * ================================================
 */

public class MainPresenter extends BasePresenter<MVContract.CommonModel, MVContract.CommonView> implements PermissionUtil.RequestPermission {

    @Inject
    RxErrorHandler mErrorHandler;

    @Inject
    public MainPresenter(MVContract.CommonModel model, MVContract.CommonView view) {
        super(model, view);
    }


    @Override
    public void onRequestPermissionSuccess() {
        EventBus.getDefault().post(EventBusTags.UI.REQUEST_CAMERA_PERMISSION_SUCCESS);
    }

    @Override
    public void onRequestPermissionFailure(List<String> list) {
        // 再次请求权限
        requestPermission();
    }

    @Override
    public void onRequestPermissionFailureWithAskNeverAgain(List<String> list) {
        // 打开设置
        PermissionUtil.showRequestPermissionDialog(mRootView.getActivity());
    }

    public void requestPermission() {
        PermissionUtil.requestPermission(this,
                mRootView.getRxPermissions(), mErrorHandler,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA);
    }
}
