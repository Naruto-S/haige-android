package com.music.player.haige.mvp.model;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.OnLifecycleEvent;

import com.hc.base.mvp.BaseModel;
import com.hc.core.integration.IRepositoryManager;
import com.music.player.haige.mvp.contract.MVContract;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * ================================================
 * Created by huangcong on 2018/2/22.
 * ================================================
 */

public class CommonModel extends BaseModel implements MVContract.CommonModel {

    @Inject
    public CommonModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    void onPause() {
        Timber.d("Release Resource");
    }

}
