package com.music.player.haige.mvp.model;

import com.hc.base.mvp.BaseModel;
import com.hc.core.integration.IRepositoryManager;
import com.music.player.haige.app.GlobalConfiguration;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.usecase.GetSongs;

import javax.inject.Inject;

/**
 * ================================================
 * Created by huangcong on 2018/2/27.
 * ================================================
 */

public class SongsModel extends BaseModel implements MVContract.SongsModel {

    private GetSongs mGetSongsCase;

    @Inject
    public SongsModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
        mGetSongsCase = new GetSongs(GlobalConfiguration.sRepository);
    }

    @Override
    public GetSongs.ResponseValue loadSongs(String action) {
        return mGetSongsCase.execute(new GetSongs.RequestValues(action));
    }
}
