package com.music.player.haige.app.image.loader;

import android.graphics.drawable.Animatable;
import android.view.View;
import android.widget.ProgressBar;

import com.facebook.imagepipeline.image.ImageInfo;
import com.music.player.haige.app.image.loader.listener.OnImageLoaderListener;
import com.music.player.haige.app.image.loader.listener.SimpeImageLoaderListener;
import com.music.player.haige.app.image.release.DisplayOptions;
import com.music.player.haige.app.image.release.ImageOptions;
import com.music.player.haige.app.image.utils.ImageLoadController;
import com.music.player.haige.app.image.widget.FrescoWapper;
import com.music.player.haige.app.image.widget.ImageFetcher;
import com.music.player.haige.mvp.ui.utils.DebugLogger;
import com.music.player.haige.mvp.ui.utils.Utils;

/**
 * Created by liumingkong on 15/8/14.
 */
public class BaseImageLoader {

    public static void displaySimpleImage(String uri, ImageFetcher imageFetcher) {
        imageFetcher.setImageURI(uri);
    }

    public static void displaySimpleImage(String uri, DisplayOptions.Builder displayOptions, ImageFetcher imageFetcher) {
        displaySimpleImage(uri, displayOptions, imageFetcher, new OnImageLoaderListener() {
            @Override
            public void onImageLoadComplete(String uri, ImageInfo imageInfo, boolean isGif, Animatable animatable, View tagVview) {

            }

            @Override
            public void onImageLoadFail(String uri, Throwable throwable, View tagVview) {
                DebugLogger.e(uri + "_" + throwable.getLocalizedMessage());
            }
        });
    }

    public static void displaySimpleImage(String uri, DisplayOptions.Builder displayOptions, ImageFetcher imageFetcher, OnImageLoaderListener onImageLoaderListener) {
        if (Utils.isNotEmptyString(uri) && Utils.isNotNull(imageFetcher)) {
            if (displayOptions != null) {
                imageFetcher.setImageURI(uri, displayOptions.build(), onImageLoaderListener);
            } else {
                imageFetcher.setImageURI(uri, null, onImageLoaderListener);
            }
        } else {
            DebugLogger.e("image uri null?");
        }
    }

    protected static void displaySimpleImage(String uri, ImageOptions imageOptions, ImageLoadController.RequestImageCallback requestImageCallback) {
        ImageLoadController.getImage(FrescoWapper.obtainRequest(uri, imageOptions), requestImageCallback);
    }

    /**
     * 最高请求级别到磁盘缓存
     *
     * @param uri
     */
    public static void loadImageFromCache(String uri, ImageLoadController.RequestImageCallback requestImageCallback) {
        try {
            ImageLoadController.getImageFromCache(uri, requestImageCallback);
        } catch (Throwable throwable) {
            DebugLogger.e(throwable);
        }
    }

    /**
     * 最高请求级别到磁盘缓存
     *
     * @param uri
     */
    public static void loadImageFromDisk(String uri, ImageLoadController.RequestImageCallback requestImageCallback) {
        try {
            ImageLoadController.getImageFromDisk(uri, requestImageCallback);
        } catch (Throwable throwable) {
            DebugLogger.e(throwable);
        }
    }

    /**
     * 最高请求级别到完全
     *
     * @param uri
     */
    public static void loadImageFromFull(String uri, ImageLoadController.RequestImageCallback requestImageCallback) {
        try {
            ImageLoadController.getImageFromFull(uri, requestImageCallback);
        } catch (Throwable throwable) {
            DebugLogger.e(throwable);
        }
    }

    // *********************************** listener *******************************************************
    static class BaseLoaderWithProgressBarListener extends SimpeImageLoaderListener {
        ProgressBar midpage_discovery_list_loading;

        public BaseLoaderWithProgressBarListener(ProgressBar item_image_loading) {
            this.midpage_discovery_list_loading = item_image_loading;
            if (item_image_loading != null) {
                item_image_loading.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onLoadComplete() {
            if (this.midpage_discovery_list_loading != null) {
                midpage_discovery_list_loading.setVisibility(View.GONE);
            }
        }

        @Override
        public void onLoadFail() {
            if (this.midpage_discovery_list_loading != null) {
                midpage_discovery_list_loading.setVisibility(View.GONE);
            }
        }
    }
}
