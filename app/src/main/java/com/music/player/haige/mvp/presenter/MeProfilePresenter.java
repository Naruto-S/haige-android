package com.music.player.haige.mvp.presenter;

import android.text.TextUtils;

import com.liulishuo.filedownloader.FileDownloader;
import com.liulishuo.filedownloader.model.FileDownloadStatus;
import com.liulishuo.filedownloader.util.FileDownloadUtils;
import com.music.player.haige.app.Constants;
import com.music.player.haige.app.GlobalConfiguration;
import com.music.player.haige.app.HaigeApplication;
import com.music.player.haige.app.base.pref.UserPrefHelper;
import com.music.player.haige.mvp.contract.AppBasePresenter;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.Api;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.model.entity.music.SongDao;
import com.music.player.haige.mvp.model.entity.user.UserResponse;
import com.music.player.haige.mvp.ui.music.utils.MusicUtils;
import com.music.player.haige.mvp.ui.utils.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;
import me.jessyan.rxerrorhandler.handler.ErrorHandleSubscriber;

/**
 * Created by Naruto on 2018/5/13.
 */

public class MeProfilePresenter extends AppBasePresenter<MVContract.MeProfileModel, MVContract.MeProfileView> {

    public static final String TAG = MeProfilePresenter.class.getSimpleName();

    @Inject
    RxErrorHandler mErrorHandler;

    private SongDao mDownloadSongDao; // 音乐下载记录
    private SongDao mRecentSongDao;   // 最近播放记录

    @Inject
    public MeProfilePresenter(MVContract.MeProfileModel model, MVContract.MeProfileView view) {
        super(model, view);

        mDownloadSongDao = GlobalConfiguration.sDownloadDaoSession.getSongDao();
        mRecentSongDao = GlobalConfiguration.sRecentDaoSession.getSongDao();
    }

    public void getUserProfile() {
        buildObservable(mModel.getUserProfile())
                .subscribe(new ErrorHandleSubscriber<UserResponse>(mErrorHandler) {
                    @Override
                    public void onNext(UserResponse response) {
                        if (response != null) {
                            mRootView.onSuccess(Api.Action.USER_PROFILE, response.getData());
                        } else {
                            mRootView.onError(Api.Action.USER_PROFILE, Api.State.NORMAL.getCode(), Api.State.NORMAL.getMsg());
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                        mRootView.onError(Api.Action.USER_PROFILE, "-1", t.getMessage());
                    }
                });
    }

    public boolean downloadMusic(Song song) {
        if (song.isLocal) {
            mRootView.showMessage(HaigeApplication.getInstance().getString(com.music.player.haige.R.string.app_download_local_music));
            return false;
        }
        String url = song.musicPath;
        if (TextUtils.isEmpty(url)) {
            return false;
        }
        // 记录到下载数据库
        if (mDownloadSongDao.load(song.songId) == null) {
            song.setDownloadDate(new Date());
            mDownloadSongDao.insert(song);
        }
        String path = MusicUtils.getDownloadPath(song);
        int id = FileDownloadUtils.generateId(url, path);
        int status = FileDownloader.getImpl().getStatus(id, path);
        System.out.println("===>>> id : " + id + ", status : " + status);
        if (status == FileDownloadStatus.completed || new File(path).exists()) {
            mRootView.showMessage(HaigeApplication.getInstance().getString(com.music.player.haige.R.string.app_download_finish));
            return false;
        } else if (status == FileDownloadStatus.pending || status == FileDownloadStatus.started ||
                status == FileDownloadStatus.connected || status == FileDownloadStatus.progress) {
            mRootView.showMessage(HaigeApplication.getInstance().getString(com.music.player.haige.R.string.app_download_ing));
            return false;
        } else {
            UserPrefHelper.putDownloadMusicCount(UserPrefHelper.getDownloadMusicCount() + 1);
            mRootView.showMessage(HaigeApplication.getInstance().getString(com.music.player.haige.R.string.app_download_join));
        }
        if (!FileDownloader.getImpl().isServiceConnected()) {
            // 开启下载服务
            FileDownloader.getImpl().bindService();
        }
        // 开始下载
        FileDownloader.getImpl().create(url).setPath(path).start();
        return true;
    }

    public void loadAllSongs() {
        try {
            buildObservable(mModel.loadAllSongs(Constants.NAVIGATE_ALLSONG).getSongList())
                    .subscribe(songs -> {
                        if (songs != null && songs.size() > 0) {
                            if (Utils.isNotNull(mRootView))  {
                                mRootView.setAllSongsCount(songs.size());
                            }
                        } else {
                            if (Utils.isNotNull(mRootView))  {
                                mRootView.setAllSongsCount(0);
                            }
                        }
                    });
        } catch (Exception e)  {
            if (Utils.isNotNull(mRootView))  {
                mRootView.setAllSongsCount(0);
            }
        }
    }

    public void loadRecentPlaySongs() {
        Observable<List<Song>> observable = Observable.just(mRecentSongDao.loadAll());
        buildObservable(observable)
                .subscribe(songs -> {
                    if (songs != null && songs.size() > 0) {
                        mRootView.setRecentPlaySongsCount(songs.size());
                    } else {
                        mRootView.setRecentPlaySongsCount(0);
                    }
                });
    }

    public void loadDownloadFinishMusic() {
        buildObservable(Observable.just(mDownloadSongDao.queryBuilder().orderDesc(SongDao.Properties.DownloadDate).list()))
                .subscribe(new ErrorHandleSubscriber<List<Song>>(mErrorHandler) {
                    @Override
                    public void onNext(List<Song> songs) {
                        ArrayList<Song> downloadSongs = new ArrayList<>();
                        for (Song song : songs) {
                            String url = song.musicPath;
                            String path = MusicUtils.getDownloadPath(song);
                            int id = FileDownloadUtils.generateId(url, path);
                            int status = FileDownloader.getImpl().getStatus(id, path);
                            if (status == FileDownloadStatus.completed
                                    || new File(path).exists()) {
                                downloadSongs.add(song);
                            }
                        }

                        mRootView.setDownloadSongsCount(downloadSongs.size());
                    }
                });
    }
}
