package com.music.player.haige.mvp.ui.music.listener;

public interface PaletteColorChangeListener {

    void onPaletteColorChange(int paletteColor, int blackWhiteColor);

}
