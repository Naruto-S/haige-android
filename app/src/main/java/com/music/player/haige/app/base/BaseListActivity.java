package com.music.player.haige.app.base;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.hc.core.mvp.IPresenter;
import com.music.player.haige.R;
import com.music.player.haige.app.Constants;
import com.music.player.haige.app.base.recyclerview.BaseQuickAdapter;
import com.music.player.haige.app.base.recyclerview.listener.OnItemClickListener;
import com.music.player.haige.app.base.recyclerview.pullrefresh.DragDistanceConverter;
import com.music.player.haige.app.base.recyclerview.pullrefresh.RecyclerRefreshLayout;
import com.music.player.haige.app.base.recyclerview.view.EmptyView;
import com.music.player.haige.app.base.recyclerview.view.MultiStateView;
import com.music.player.haige.app.base.recyclerview.view.NoNetworkView;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.ui.utils.DebugLogger;
import com.music.player.haige.mvp.ui.utils.NetworkUtils;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;
import com.music.player.haige.mvp.ui.utils.Utils;

import java.util.ArrayList;

import butterknife.BindView;

import static com.music.player.haige.mvp.ui.utils.Utils.isFastClick;

/**
 * Created by kince on 16-11-22.
 * base activity for recyclerview
 */
public abstract class BaseListActivity<P extends IPresenter> extends AppBaseActivity<P>
        implements MVContract.CommonListView, BaseQuickAdapter.RequestLoadMoreListener, RecyclerRefreshLayout.OnRefreshListener {

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    @BindView(R.id.refresh_layout)
    RecyclerRefreshLayout mRefreshLayout;

    @BindView(R.id.multiStateView)
    MultiStateView mMultiStateView;

    protected int currentPage;

    protected int mLastFirstVisible = 0;

    protected BaseQuickAdapter mAdapter;

    protected RecyclerView.OnScrollListener mOnScrollListener;

    protected RecyclerView.ItemDecoration mItemDecoration;

    protected RecyclerView.LayoutManager mLayoutManager;

    protected abstract void reLoadData();

    protected abstract void loadMoreData();

    protected abstract RecyclerView.LayoutManager createLayoutManager();

    protected abstract OnItemClickListener createItemClickListener();

    protected abstract RecyclerView.OnScrollListener addRecyclerViewScrollListener();

    protected abstract RecyclerView.ItemDecoration createDecoration();

    protected abstract RecyclerView.ItemAnimator createItemAnimator();

    protected abstract View createRefreshHeader();

    protected abstract Context createContext();

    protected abstract BaseQuickAdapter createAdapter();

    protected boolean shouldLoadMoreData() {
        return true;
    }

    @Override
    public void initData(Bundle savedInstanceState) {
        setupView();
        reLoadData();
    }

    protected void setupView() {
        setupRecycleView();
        setupRefreshLayout();
        setupMultiStateView();
    }

    @Override
    public void onRefreshStart() {

    }

    @Override
    public void onRefresh() {
        reLoadData();
    }

    @Override
    public void onRefreshEnd() {

    }

    @Override
    public void onLoadMoreRequested() {
        loadMoreData();
    }

    @Override
    public void showLoadingError() {
        if (mAdapter.getItemCount() <= 0) {
            mMultiStateView.setViewState(MultiStateView.VIEW_STATE_EMPTY);
        } else {
            mMultiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);
        }
    }

    @Override
    public void showErrorNetwork() {
        if (mAdapter.getItemCount() > 0) {
            mMultiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);
            Toast.makeText(createContext(), R.string.app_net_no_connect_tip, Toast.LENGTH_SHORT).show();
        } else {
            mMultiStateView.setViewState(MultiStateView.VIEW_STATE_ERROR);
        }
    }

    @Override
    public void showNoMoreView() {
        mAdapter.showLoadNoMoreView();
    }

    @Override
    public void hideLoadingView() {
        mAdapter.hideLoadingView();
    }

    @Override
    public void loadEnded() {
        mAdapter.loadComplete();
    }

    @Override
    public void loadRequestStarted() {
        if (!mRefreshLayout.isRefreshing() && mAdapter.getItemCount() <= 0) {
            mMultiStateView.setViewState(MultiStateView.VIEW_STATE_LOADING);
        } else {
            mMultiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);
        }
    }

    @Override
    public void loadRequestCompleted() {
        mRefreshLayout.setRefreshing(false);

        if (mAdapter.getItemCount() > 0) {
            mMultiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);
        } else {
            mMultiStateView.setViewState(MultiStateView.VIEW_STATE_EMPTY);
        }
    }

    @Override
    public void showRefresh(ArrayList arrayList) {
        mRefreshLayout.setRefreshing(false);

        mAdapter.setNewData(arrayList);
        mAdapter.removeAllFooterView();
    }

    @Override
    public void showLoadMore(ArrayList arrayList) {
        mAdapter.addData(arrayList);
    }

    public ArrayList<Object> getCurrentData() {
        return mAdapter != null ? (ArrayList<Object>) mAdapter.getData() : null;
    }

    private void setupRecycleView() {
        mLayoutManager = createLayoutManager();
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(createItemAnimator());
        mRecyclerView.setHorizontalFadingEdgeEnabled(false);
        mOnScrollListener = addRecyclerViewScrollListener();
        if (Utils.isNotNull(mOnScrollListener)) {
            mRecyclerView.addOnScrollListener(addRecyclerViewScrollListener());
        }
        mItemDecoration = createDecoration();
        if (Utils.isNotNull(mItemDecoration)) {
            mRecyclerView.addItemDecoration(mItemDecoration);
        }
        mRecyclerView.addOnItemTouchListener(createItemClickListener());
        mAdapter = createAdapter();
        mAdapter.setAutoLoadMoreSize(3, mLayoutManager);
        if (shouldLoadMoreData()) {
            mAdapter.setOnLoadMoreListener(this);
        }
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                switch (newState) {
                    case RecyclerView.SCROLL_STATE_IDLE:
                        if (mLastFirstVisible != findFirstVisibleItemPosition()) {
                            mLastFirstVisible = findFirstVisibleItemPosition();
                            onFindLastVisibleItem(findLastVisibleItemPosition());
                        }
                        break;

                    default:
                        break;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });

    }

    private void setupRefreshLayout() {
        View kittyRefreshHeader = createRefreshHeader();
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(
                ResourceUtils.dpToPX(24), getRefreshHeight());
        kittyRefreshHeader.setPadding(0, getRefreshPadding(), 0, 0);
        mRefreshLayout.setRefreshView(kittyRefreshHeader, layoutParams);
        mRefreshLayout.setDragDistanceConverter(new DragDistanceConverter());
        mRefreshLayout.setNestedScrollingEnabled(true);
        mRefreshLayout.setOnRefreshListener(this);
    }

    private void setupMultiStateView() {
        EmptyView emptyView = ((EmptyView) mMultiStateView.getView(MultiStateView.VIEW_STATE_EMPTY));
        emptyView.setEmptyType(getEmptyType());
        emptyView.setOnEmptyListener(() -> {
            if (NetworkUtils.isConnected(createContext())) {
                reLoadData();
            } else {
                Toast.makeText(createContext(), R.string.app_net_no_connect_tip, Toast.LENGTH_SHORT).show();
            }
        });
        ((NoNetworkView) mMultiStateView.getView(MultiStateView.VIEW_STATE_ERROR))
                .setOnRefreshNetwork(() -> {
                    if (NetworkUtils.isConnected(createContext())) {
                        reLoadData();
                    } else {
                        Toast.makeText(createContext(), R.string.app_net_no_connect_tip, Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public void setSwipeRefreshEnable(boolean enable) {
        enable &= mMultiStateView.getViewState() != MultiStateView.VIEW_STATE_ERROR;
    }

    public void setStateToRefresh() {
        if (!isFastClick() && !mRefreshLayout.isRefreshing()) {
            if (mAdapter.getItemCount() > 0) {
                mRecyclerView.smoothScrollToPosition(0);
                mRefreshLayout.setRefreshing(true);
            }
            reLoadData();
        }
    }

    public int findFirstVisibleItemPosition() {
        if (mLayoutManager instanceof GridLayoutManager) {
            return ((GridLayoutManager) mLayoutManager).findFirstVisibleItemPosition();
        } else if (mLayoutManager instanceof LinearLayoutManager) {
            return ((LinearLayoutManager) mLayoutManager).findFirstVisibleItemPosition();
        } else {
            return ((GridLayoutManager) mLayoutManager).findFirstVisibleItemPosition();
        }
    }

    public int findLastVisibleItemPosition() {
        if (mLayoutManager instanceof GridLayoutManager) {
            return ((GridLayoutManager) mLayoutManager).findLastVisibleItemPosition();
        } else if (mLayoutManager instanceof LinearLayoutManager) {
            return ((LinearLayoutManager) mLayoutManager).findLastVisibleItemPosition();
        } else {
            return ((GridLayoutManager) mLayoutManager).findLastVisibleItemPosition();
        }
    }

    public void onFindLastVisibleItem(int position) {

    }

    public synchronized void fetchPusherList(BaseQuickAdapter adapter, boolean isRefresh) {
        if (!NetworkUtils.isConnected(createContext())) {
            loadRequestCompleted();
            showErrorNetwork();
            return;
        }
        loadRequestStarted();

        if (isRefresh || adapter.isEmptyData()) {
            currentPage = 1;
        } else {
            currentPage++;
        }
        DebugLogger.d("fetchPusherList === ", "Fetch LiveRoomList[" + currentPage + "]...");
        sendListReq(currentPage, Constants.DEFAULT_REQUEST_SIZE, isRefresh);
    }

    protected void sendListReq(int pageNum, int pageSize, boolean refresh) {

    }

    public int getRefreshHeight() {
        return ResourceUtils.dpToPX(72);
    }

    public int getRefreshPadding() {
        return ResourceUtils.dpToPX(48);
    }

    public int getEmptyType() {
        return EmptyView.EMPTY_NO_MUSIC;
    }

    public RecyclerView getRecyclerView() {
        return mRecyclerView;
    }

    public RecyclerRefreshLayout getRefreshLayout() {
        return mRefreshLayout;
    }

    public MultiStateView getMultiStateView() {
        return mMultiStateView;
    }

    public BaseQuickAdapter getAdapter() {
        return mAdapter;
    }

    public RecyclerView.ItemDecoration getItemDecoration() {
        return mItemDecoration;
    }

    public void disablePullRefresh() {
        mRefreshLayout.setEnabled(false);
    }

}
