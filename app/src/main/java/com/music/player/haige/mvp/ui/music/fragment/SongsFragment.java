package com.music.player.haige.mvp.ui.music.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.ProgressBar;

import com.hc.core.di.component.AppComponent;
import com.hc.core.utils.CoreUtils;
import com.music.player.haige.R;
import com.music.player.haige.app.Constants;
import com.music.player.haige.app.EventBusTags;
import com.music.player.haige.app.base.AppBaseLazyFragment;
import com.music.player.haige.app.base.pref.UserPrefHelper;
import com.music.player.haige.di.component.DaggerSongsComponent;
import com.music.player.haige.di.module.SongsModule;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.Api;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.model.utils.NavigationUtil;
import com.music.player.haige.mvp.presenter.SongsPresenter;
import com.music.player.haige.mvp.ui.music.adapter.SongsListAdapter;
import com.music.player.haige.mvp.ui.music.service.MusicServiceConnection;
import com.music.player.haige.mvp.ui.share.DownloadShareDialog;
import com.music.player.haige.mvp.ui.utils.ShareUtils;
import com.music.player.haige.mvp.ui.utils.Utils;
import com.music.player.haige.mvp.ui.widget.StatusLayout;

import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;

import java.util.List;

import butterknife.BindView;

/**
 * ================================================
 * Created by huangcong on 2018/2/27.
 * <p>
 * 本地歌曲列表
 * ================================================
 */

public class SongsFragment extends AppBaseLazyFragment<SongsPresenter> implements MVContract.SongsView, SongsListAdapter.OnSongClickListener {

    @BindView(R.id.app_layout_sl)
    StatusLayout mStatusLayout;

    @BindView(R.id.app_layout_rv)
    RecyclerView mSonsRecyclerView;

    @BindView(R.id.app_layout_progress)
    ProgressBar mLoadingView;

    private String mAction;
    private SongsListAdapter mAdapter;

    public static SongsFragment newInstance(String action) {
        Bundle args = new Bundle();
        switch (action) {
            case Constants.NAVIGATE_ALLSONG:
                args.putString(Constants.PLAYLIST_TYPE, action);
                break;
            case Constants.NAVIGATE_PLAYLIST_RECENTADD:
                args.putString(Constants.PLAYLIST_TYPE, action);
                break;
            case Constants.NAVIGATE_PLAYLIST_RECENTPLAY:
                args.putString(Constants.PLAYLIST_TYPE, action);
                break;
            case Constants.NAVIGATE_PLAYLIST_FAVORITE:
                args.putString(Constants.PLAYLIST_TYPE, action);
                break;
            default:
                throw new RuntimeException("wrong action type");
        }
        SongsFragment fragment = new SongsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void setupFragmentComponent(AppComponent appComponent) {
        DaggerSongsComponent
                .builder()
                .appComponent(appComponent)
                .songsModule(new SongsModule(this))
                .build()
                .inject(this);
    }

    @Override
    public View initView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.app_layout_recyclerview, container, false);
    }

    @Override
    public void lazyInitData(Bundle savedInstanceState) {
        mAction = getArguments().getString(Constants.PLAYLIST_TYPE);

        mStatusLayout.setOnStatusClickListener(new StatusLayout.OnStatusClickListener() {
            @Override
            public void onEmptyClick() {
                EventBus.getDefault().post(EventBusTags.UI.GO_TO_RECOMMEND);
                EventBus.getDefault().post(EventBusTags.MAIN.BACK);
            }

            @Override
            public void onReloadClick() {

            }
        });

        mAdapter = new SongsListAdapter((AppCompatActivity) getActivity(), true);
        mAdapter.setOnSongClickListener(this);
        mSonsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mSonsRecyclerView.setAdapter(mAdapter);

        mPresenter.loadSongs(mAction);
    }

    @Override
    public void setData(Object data) {

    }

    @Override
    public void showLoading() {
        super.showLoading();
        if (Utils.isNotNull(mLoadingView)) {
            mLoadingView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
        if (Utils.isNotNull(mLoadingView)) {
            mLoadingView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onSuccess(Api.Action action, Object o) {
        super.onSuccess(action, o);
        if (action == Api.Action.MUSIC_LIST) {
            mStatusLayout.showStatusView(StatusLayout.STATUS.OK);
            mSonsRecyclerView.setVisibility(View.VISIBLE);
            mAdapter.setSongList((List<Song>) o);
        }
    }

    @Override
    public void onError(Api.Action action, String code, String message) {
        super.onError(action, code, message);
        if (action == Api.Action.MUSIC_LIST) {
            mStatusLayout.showStatusView(StatusLayout.STATUS.EMPTY);
            mSonsRecyclerView.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void showMessage(String message) {
        super.showMessage(message);
        CoreUtils.makeText(getContext(), message);
    }

    @Override
    public void onPopupMenuClick(View v, int position, Song song) {
        final PopupMenu menu = new PopupMenu(getContext(), v);
        menu.setOnMenuItemClickListener(item -> {
            Context context = getContext();
            switch (item.getItemId()) {
                case R.id.popup_song_play_next:
                    MusicServiceConnection.playNext(song);
                    showMessage(context.getResources().getString(R.string.app_next_play));
                    break;
                case R.id.popup_song_goto_album:
                    NavigationUtil.goToAlbum(context, song.albumId, song.musicName);
                    break;
                case R.id.popup_song_goto_artist:
                    NavigationUtil.goToArtist(context, song.artistId, song.artistName);
                    break;
                case R.id.popup_song_addto_queue:
                    MusicServiceConnection.addToQueue(song);
                    break;
                case R.id.popup_download:
                    if (ShareUtils.checkDownloadShare(UserPrefHelper.getDownloadMusicCount())) {
                        DownloadShareDialog.newInstance().show(getChildFragmentManager(), DownloadShareDialog.class.getName());
                    } else {
                        mPresenter.downloadMusic(song);
                    }
                    break;
                case R.id.popup_song_delete:
                    long[] deleteIds = {song.songId};
                    switch (mAction) {
                        case Constants.NAVIGATE_PLAYLIST_FAVORITE:
                            //HaigeUtil.showDeleteFromFavourate(context, deleteIds);
                            mPresenter.deleteFavoriteSong(song);
                            mAdapter.removeSong(position);
                            break;
                        case Constants.NAVIGATE_PLAYLIST_RECENTPLAY:
                            mPresenter.deleteRecentSong(song);
                            mAdapter.removeSong(position);
                            //HaigeUtil.showDeleteFromRecentlyPlay(context, deleteIds);
                            break;
                        default:
                            mAdapter.removeSong(position);
//                                HaigeUtil.showDeleteDialog(context, song.musicName, deleteIds,
//                                        new MaterialDialog.SingleButtonCallback() {
//                                            @Override
//                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                                                mAdapter.removeSong(position);
//                                            }
//                                        });
                            break;
                    }
                    break;
            }
            return false;
        });
        if (Constants.NAVIGATE_PLAYLIST_FAVORITE.equals(mAction)) {
            menu.inflate(R.menu.popup_song_favorite);
        } else if (Constants.NAVIGATE_ALLSONG.equals(mAction)) {
            menu.inflate(R.menu.popup_song_local);
        } else if (Constants.NAVIGATE_PLAYLIST_RECENTPLAY.equals(mAction)) {
            menu.inflate(R.menu.popup_song_recent);
        } else {
            menu.inflate(R.menu.popup_song);
        }
        menu.show();
    }

    @Subscriber
    public void onSubscriber(EventBusTags.FavoriteSongEvent event) {
        if (!Constants.NAVIGATE_PLAYLIST_FAVORITE.equals(mAction)) {
            return;
        }
        mPresenter.loadSongs(mAction);
    }

    @Subscriber
    public void onSubscriber(EventBusTags.RecentlyPlayEvent event) {
        if (!Constants.NAVIGATE_PLAYLIST_RECENTPLAY.equals(mAction)) {
            return;
        }
        mPresenter.loadSongs(mAction);
    }

    @Subscriber
    public void onSubscriber(EventBusTags.MediaUpdateEvent event) {
        if (Constants.NAVIGATE_PLAYLIST_RECENTPLAY.equals(mAction)
                || Constants.NAVIGATE_PLAYLIST_RECENTPLAY.equals(mAction)) {
            return;
        }
        mPresenter.loadSongs(mAction);
    }

    @Subscriber
    public void onSubscriber(EventBusTags.MetaChangedEvent event) {
        mAdapter.notifyDataSetChanged();
    }
}
