package com.music.player.haige.app.base.pref;

import java.util.Set;

/**
 * Created by zhoumingjun on 1/7/14.
 *
 * preference的存储文件：MIMICONFIG
 *
 *
 */
public class Preferences extends BasicPref {

    protected static final String MIMI_PREFERENCE = "MIMICONFIG";

    public static void clear() {
        clear(MIMI_PREFERENCE);
    }

    public static void remove(String key) {
        remove(MIMI_PREFERENCE, key);
    }

    protected static int getInt(String tag, int defaultValue) {
        return getInt(MIMI_PREFERENCE, tag, defaultValue);
    }

    protected static void saveInt(String tag, int value) {
        saveInt(MIMI_PREFERENCE, tag, value);
    }

    protected static long getLong(String tag, long defaultValue) {
        return getLong(MIMI_PREFERENCE, tag, defaultValue);
    }

    protected static void saveLong(String tag, long value) {
        saveLong(MIMI_PREFERENCE, tag, value);
    }

    protected static boolean getBoolean(String tag, boolean defaultValue) {
        return getBoolean(MIMI_PREFERENCE, tag, defaultValue);
    }

    protected static void saveBoolean(String tag, boolean value) {
        saveBoolean(MIMI_PREFERENCE, tag, value);
    }

    protected static String getString(String tag, String defaultValue) {
        return getString(MIMI_PREFERENCE, tag, defaultValue);
    }

    protected static void saveString(String tag, String value) {
        saveString(MIMI_PREFERENCE, tag, value);
    }

    protected static Set<String> getStringSet(String tag) {
        return getStringSet(MIMI_PREFERENCE, tag);
    }

    protected static void saveStringSet(String tag, Set<String> value) {
        saveStringSet(MIMI_PREFERENCE, tag, value);
    }
}
