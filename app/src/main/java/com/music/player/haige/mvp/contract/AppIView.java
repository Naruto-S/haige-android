package com.music.player.haige.mvp.contract;

import com.hc.core.mvp.IView;
import com.music.player.haige.mvp.model.api.Api;

/**
 * ================================================
 * Created by huangcong on 2018/3/19.
 * ================================================
 */

public interface AppIView extends IView<Api.Action> {
}
