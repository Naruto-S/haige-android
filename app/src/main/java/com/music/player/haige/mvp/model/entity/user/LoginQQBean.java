package com.music.player.haige.mvp.model.entity.user;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LoginQQBean implements Serializable {

    @SerializedName("openid")
    private String openid;
    @SerializedName("access_token")
    private String accessToken;
    @SerializedName("ret")
    private String ret;
    @SerializedName("pay_token")
    private String payToken;
    @SerializedName("pf")
    private String pf;
    @SerializedName("expires_in")
    private String expiresIn;
    @SerializedName("pfkey")
    private String pfkey;
    @SerializedName("msg")
    private String msg;

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRet() {
        return ret;
    }

    public void setRet(String ret) {
        this.ret = ret;
    }

    public String getPayToken() {
        return payToken;
    }

    public void setPayToken(String payToken) {
        this.payToken = payToken;
    }

    public String getPf() {
        return pf;
    }

    public void setPf(String pf) {
        this.pf = pf;
    }

    public String getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(String expiresIn) {
        this.expiresIn = expiresIn;
    }

    public String getPfkey() {
        return pfkey;
    }

    public void setPfkey(String pfkey) {
        this.pfkey = pfkey;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }


}
