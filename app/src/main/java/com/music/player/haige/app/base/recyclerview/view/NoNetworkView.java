package com.music.player.haige.app.base.recyclerview.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.music.player.haige.R;

/**
 * Created by kince on 2016/3/18.
 */
public class NoNetworkView extends LinearLayout {

    private View mRefreshButton;

    private OnNetworkErrorListener mNetworkErrorListener;

    public NoNetworkView(Context context) {
        super(context);
    }

    public NoNetworkView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public NoNetworkView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater.from(context).inflate(R.layout.layout_network_error, this, true);
        mRefreshButton = findViewById(R.id.app_layout_net_no_connect_btn);
        mRefreshButton.setOnClickListener(v -> {
            if (mNetworkErrorListener != null) {
                mNetworkErrorListener.onRefreshNetwork();
            }
        });
    }

    public NoNetworkView setOnRefreshNetwork(final OnNetworkErrorListener networkErrorListener) {
        this.mNetworkErrorListener = networkErrorListener;
        return this;
    }

    public interface OnNetworkErrorListener {
        void onRefreshNetwork();
    }

}
