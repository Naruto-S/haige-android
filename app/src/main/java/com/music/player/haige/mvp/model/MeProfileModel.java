package com.music.player.haige.mvp.model;

import com.hc.base.mvp.BaseModel;
import com.hc.core.integration.IRepositoryManager;
import com.music.player.haige.app.GlobalConfiguration;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.service.ApiService;
import com.music.player.haige.mvp.model.entity.user.UserResponse;
import com.music.player.haige.mvp.usecase.GetSongs;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by Naruto on 2018/5/13.
 */

public class MeProfileModel extends BaseModel implements MVContract.MeProfileModel {

    private GetSongs mGetSongsCase;

    @Inject
    public MeProfileModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
        mGetSongsCase = new GetSongs(GlobalConfiguration.sRepository);
    }

    @Override
    public Observable<UserResponse> getUserProfile() {
        return Observable.just(mRepositoryManager.obtainRetrofitService(ApiService.class).getUserProfile())
                .flatMap(observable -> observable);
    }

    @Override
    public GetSongs.ResponseValue loadAllSongs(String action) {
        try {
            return mGetSongsCase.execute(new GetSongs.RequestValues(action));
        } catch (Exception e)  {
            return null;
        }
    }
}
