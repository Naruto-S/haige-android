package com.music.player.haige.mvp.model;

import com.hc.base.mvp.BaseModel;
import com.hc.core.integration.IRepositoryManager;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.service.ApiService;
import com.music.player.haige.mvp.model.entity.BaseResponse;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import okhttp3.MultipartBody;

/**
 * Created by Naruto on 2018/5/13.
 */

public class CropModel extends BaseModel implements MVContract.CropModel {

    @Inject
    public CropModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public Observable<BaseResponse> uploadAvatar(List<MultipartBody.Part> partList) {
        return Observable.just(mRepositoryManager.obtainRetrofitService(ApiService.class).uploadAvatar(partList))
                .flatMap(observable -> observable);
    }
}
