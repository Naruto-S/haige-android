package com.music.player.haige.mvp.ui.music.service;

import com.music.player.haige.mvp.ui.music.service.cache.file.FileNameGenerator;
import com.music.player.haige.mvp.ui.utils.Utils;

public class ProxyFileNameGenerator implements FileNameGenerator {

    private boolean mIsShort;

    @Override
    public void putIsShort(boolean aShort) {
        mIsShort = aShort;
    }

    @Override
    public String generate(String url) {
        String name = "";
        if (!Utils.isEmptyString(url)) {
            try {
                String[] strs = url.split("/");
                if (!Utils.isEmptyArray(strs)) {
                    name = strs[strs.length - 1];
                }
            } catch (Throwable e) {

            }
        } else {
            name = "tmp.acc";
        }
        if (mIsShort) {
            return "short_" + name;
        } else {
            return name;
        }
    }
}
