package com.music.player.haige.app.utils.statistics;

public class StatisticsConstant {

    public static final String USER_DEVICE_ID = "USER_DEVICE_ID"; //设备id
    public static final String OPEN_APP = "Open_App"; //启动app 打点

    public static final String RANKING_CLICK = "Ranking_Click" ;//1，点击或者切换到排行页面的次数。（Ranking_Click）
    public static final String SWITCH_CLICK = "Switch_Click";//2，推荐页滑动切换歌曲的次数。（Switch_Click）
    public static final String HEAD_CLICK = "Head_Click";//3，点击推荐页用户头像的次数。（Head_Click）
    public static final String LIKE_CLICK = "Like_Click";//4，点击点赞的次数。（Like_Click）
    public static final String COMMENT_CLICK = "Comment_Click";//5，点击弹幕的次数。（Comment_Click）
    public static final String SHARE_CLICK = "Share_Click";//6，点击分享的次数。（Share_Click）
    public static final String MORE_CLICK = "More_Click";//7，点击更多的次数。（More_Click）
    public static final String MORE_CLOSE_COMMENT_CLICK = "More_Close_Comment_Click";//8，点击更多至关闭弹幕的次数。（More_Close_Comment_Click）
    public static final String MORE_DOWNLOAD_CLICK = "More_Download_Click";//9，点击更多至下载的次数。（More_Download_Click）
    public static final String SEARCH_CLICK = "Search_Click";//10，点击搜索入口的次数。（Search_Click）
    public static final String TABLE_PLAY_CLICK = "Table_Play_Click";//11，点击底部table 播放入口的次数。（Table_Play_Click）
    public static final String TABLE_MY_CLICK = "Table_My_Click";//12，点击table我的入口次数。（Table_My_Click）
    public static final String LOGIN_WINDOW_APPEAR = "Login_Window_Appear";//13，统计登录弹窗出现的次数。（Login_Window_Appear）
    public static final String LOGIN_WECHAT_CLICK = "Login_Wechat_Click";//14，统计点击微信授权登录的次数。（Login_Wechat_Click）
    public static final String LOGIN_QQ_CLICK = "Login_QQ_Click";//15，统计点击QQ授权登录的次数。（Login_QQ_Click）
    public static final String SHARE_WECHAT_FRIEND_CLICK = "Share_Wechat_Friend_Click";//16，分开统计点击微信好友、朋友圈、QQ、微博、QQ空间分享的次数 （Share_Wechat_Friend_Click , Share_Dynamic_Click , Share_QQ_Click , Share_Weibo_Click , Share_Zone_Click）
    public static final String SHARE_DYNAMIC_CLICK = "Share_Dynamic_Click";
    public static final String SHARE_QQ_CLICK = "Share_QQ_Click";
    public static final String SHARE_WEIBO_CLICK = "Share_Weibo_Click";
    public static final String SHARE_ZONE_CLICK = "Share_Zone_Click";
    public static final String DOWNLOAD_SHARE_CLICK = "Download_Share_Click";//17，点击下载弹窗分享的次数（不管点那个入口分享都统计为一次）。（Download_Share_Click）
    public static final String LABEL_CLICK = "Label_Click";//18，点击推荐页标签的次数。（Label_Click）
    public static final String NOTICE_CLICK = "Notice_Click";
    public static final String NOTICE_ARRIVE = "Notice_Arrive";

    public static final String NO_LIKE_CLICK = "NO_Like_Click"; //点击不喜欢的次数
    public static final String RECOMMEND_LIKE_CLICK = "Recommend_Like_Click";    //点击喜欢的次数
    public static final String REFRESH_CLICK = "Refresh_Click"; //点击换一批的次数
    public static final String COMPLETE_CLICk = "Complete_Click";    //点击听整首的次数
    public static final String LEFT_SLIP_CLICK = "Left_slip_Click";   //向左滑动卡片的次数
    public static final String RIGHT_SLIP_CLICK = "Right_slip_Click";  //向右滑动卡片的次数

}
