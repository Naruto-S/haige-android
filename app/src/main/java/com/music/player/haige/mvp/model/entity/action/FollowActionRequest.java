package com.music.player.haige.mvp.model.entity.action;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FollowActionRequest implements Serializable {

    @SerializedName("user_id")
    private String userId;

    @SerializedName("be_followed_id")
    private String beFollowedId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getBeFollowedId() {
        return beFollowedId;
    }

    public void setBeFollowedId(String beFollowedId) {
        this.beFollowedId = beFollowedId;
    }

    @Override
    public String toString() {
        return "FollowActionRequest{" +
                "userId='" + userId + '\'' +
                ", beFollowedId='" + beFollowedId + '\'' +
                '}';
    }
}
