package com.music.player.haige.mvp.ui.main.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.music.player.haige.R;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Naruto on 2018/3/7.
 */

public class TagLoopHeader extends FrameLayout {

    @BindView(R.id.tv_music_count)
    TextView mMusicCountTv;

    private LayoutInflater mLayoutInflater;

    public TagLoopHeader(@NonNull Context context) {
        super(context);
        init();
    }

    public TagLoopHeader(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TagLoopHeader(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mLayoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mLayoutInflater.inflate(R.layout.app_item_tag_music_list_top_loop, this, true);
        ButterKnife.bind(this, this);

        mMusicCountTv.setText(ResourceUtils.resourceString(R.string.rank_tag_music_count, 0));
    }

    public void setupView(int count) {
        mMusicCountTv.setText(ResourceUtils.resourceString(R.string.rank_tag_music_count, count));
    }
}
