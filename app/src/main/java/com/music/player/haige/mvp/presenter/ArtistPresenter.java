package com.music.player.haige.mvp.presenter;

import com.hc.base.mvp.BasePresenter;
import com.hc.core.utils.RxLifecycleUtils;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.entity.music.Artist;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;
import me.jessyan.rxerrorhandler.handler.ErrorHandleSubscriber;

/**
 * ================================================
 * Created by huangcong on 2018/3/1.
 * ================================================
 */

public class ArtistPresenter extends BasePresenter<MVContract.ArtistModel, MVContract.ArtistView> {

    @Inject
    RxErrorHandler mErrorHandler;

    @Inject
    public ArtistPresenter(MVContract.ArtistModel model, MVContract.ArtistView view) {
        super(model, view);
    }

    public void loadArtists(String action) {
        mModel.loadArtists(action)
                .getArtistList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(Disposable disposable) throws Exception {
                        mRootView.showLoading();
                    }
                })
                .doFinally(new Action() {
                    @Override
                    public void run() throws Exception {
                        mRootView.hideLoading();
                    }
                })
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))//使用 Rxlifecycle,使 Disposable 和 Activity 一起销毁
                .subscribe(new ErrorHandleSubscriber<List<Artist>>(mErrorHandler) {
                    @Override
                    public void onNext(List<Artist> artist) {
                        if (artist == null || artist.size() == 0) {
                            mRootView.showEmptyView();
                        } else {
                            mRootView.showArtists(artist);
                        }
                    }
                });
    }

}
