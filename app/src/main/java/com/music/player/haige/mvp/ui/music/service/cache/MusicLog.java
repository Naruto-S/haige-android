package com.music.player.haige.mvp.ui.music.service.cache;

import com.music.player.haige.mvp.ui.utils.DebugLogger;

/**
 * Created by liumingkong on 2017/5/4.
 */

public class MusicLog {

    private static final String MUSICLOGO = "MUSICLOGO";

    private static void d(String event) {
        DebugLogger.d(MUSICLOGO, event);
    }

    private static void e(String info, Throwable e) {
        DebugLogger.e(MUSICLOGO, info, e);
    }

    public static void preloadD(String event) {
        d("[Preload]-" + event);
    }

    public static void preloadE(String info, Throwable e) {
        e("[Preload]-" + info, e);
    }

    public static void playerD(String event) {
        d("[Player]-" + event);
    }

    public static void playerE(String info, Throwable e) {
        e("[Player]-" + info, e);
    }

    public static void proxyD(String event) {
        d("[Proxy]-" + event);
    }

    public static void proxyE(String info, Throwable e) {
//        proxyD(info);
        e("[Proxy]-" + info, e);
    }
}
