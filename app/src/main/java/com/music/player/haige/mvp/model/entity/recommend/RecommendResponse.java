package com.music.player.haige.mvp.model.entity.recommend;

import com.music.player.haige.mvp.model.entity.BaseResponse;
import com.music.player.haige.mvp.model.entity.music.Song;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * ================================================
 * Created by huangcong on 2018/3/16.
 * ================================================
 */

public class RecommendResponse extends BaseResponse<ArrayList<Song>> implements Serializable {

}
