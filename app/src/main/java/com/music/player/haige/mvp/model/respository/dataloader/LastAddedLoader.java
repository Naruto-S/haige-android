package com.music.player.haige.mvp.model.respository.dataloader;

import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;

import com.music.player.haige.mvp.model.entity.music.Album;
import com.music.player.haige.mvp.model.entity.music.Artist;
import com.music.player.haige.mvp.model.entity.music.Song;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.functions.Function;


public class LastAddedLoader {

    public static Observable<List<Song>> getLastAddedSongs(final Context context) {
        return Observable.create(new ObservableOnSubscribe<List<Song>>() {
            @Override
            public void subscribe(ObservableEmitter<List<Song>> emitter) throws Exception {
                List<Song> mSongList = new ArrayList<>();
                Cursor mCursor = makeLastAddedCursor(context);

                if (mCursor != null && mCursor.moveToFirst()) {
                    do {
                        long id = mCursor.getLong(0);
                        String title = mCursor.getString(1);
                        String artist = mCursor.getString(2);
                        String album = mCursor.getString(3);
                        int duration = mCursor.getInt(4);
                        int trackNumber = mCursor.getInt(5);
                        long artistId = mCursor.getInt(6);
                        long albumId = mCursor.getLong(7);
                        boolean isLocal = true;

                        final Song song = new Song(
                                id,
                                albumId,
                                artistId,
                                title,
                                artist,
                                album,
                                duration,
                                trackNumber,
                                isLocal);

                        mSongList.add(song);
                    } while (mCursor.moveToNext());
                }
                if (mCursor != null) {
                    mCursor.close();
                    mCursor = null;
                }
                emitter.onNext(mSongList);
                emitter.onComplete();
            }
        });
    }

    public static Observable<List<Album>> getLastAddedAlbums(final Context context) {
        return getLastAddedSongs(context)
                .flatMap(new Function<List<Song>, Observable<Song>>() {
                    @Override
                    public Observable<Song> apply(List<Song> songs) throws Exception {
                        return Observable.fromIterable(songs);
                    }
                }).distinct(new Function<Song, Long>() {
                    @Override
                    public Long apply(Song song) throws Exception {
                        return song.albumId;
                    }
                }).flatMap(new Function<Song, Observable<Album>>() {
                    @Override
                    public Observable<Album> apply(Song song) throws Exception {
                        return AlbumLoader.getAlbum(context, song.albumId);
                    }
                }).toList().toObservable();
    }

    public static Observable<List<Artist>> getLastAddedArtist(final Context context) {
        return getLastAddedSongs(context)
                .flatMap(new Function<List<Song>, Observable<Song>>() {
                    @Override
                    public Observable<Song> apply(List<Song> songs) throws Exception {
                        return Observable.fromIterable(songs);
                    }
                }).distinct(new Function<Song, Long>() {
                    @Override
                    public Long apply(Song song) throws Exception {
                        return song.artistId;
                    }
                }).flatMap(new Function<Song, Observable<Artist>>() {
                    @Override
                    public Observable<Artist> apply(Song song) throws Exception {
                        return ArtistLoader.getArtist(context, song.artistId);
                    }
                }).toList().toObservable();
    }

    private static Cursor makeLastAddedCursor(final Context context) {
        //four weeks ago
        long fourWeeksAgo = (System.currentTimeMillis() / 1000) - (4 * 3600 * 24 * 7);
        long cutoff = 0L;
        // use the most recent of the two timestamps
        if (cutoff < fourWeeksAgo) {
            cutoff = fourWeeksAgo;
        }

        final StringBuilder selection = new StringBuilder();
        selection.append(MediaStore.Audio.AudioColumns.IS_MUSIC + "=1");
        selection.append(" AND " + MediaStore.Audio.AudioColumns.TITLE + " != ''");
        selection.append(" AND " + MediaStore.Audio.Media.DATE_ADDED + ">");
        selection.append(cutoff);

        return context.getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                new String[]{"_id", "title", "artistName", "album", "duration", "track", "artist_id", "album_id"}, selection.toString(), null, MediaStore.Audio.Media.DATE_ADDED + " DESC");
    }

}
