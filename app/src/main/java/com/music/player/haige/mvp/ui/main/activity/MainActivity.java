package com.music.player.haige.mvp.ui.main.activity;

import android.app.PendingIntent;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.hc.base.utils.DateUtils;
import com.hc.core.di.component.AppComponent;
import com.hc.core.utils.CoreUtils;
import com.music.player.haige.BuildConfig;
import com.music.player.haige.R;
import com.music.player.haige.app.ActivityLifecycleCallbacksImpl;
import com.music.player.haige.app.Constants;
import com.music.player.haige.app.EventBusTags;
import com.music.player.haige.app.SystemUIConfig;
import com.music.player.haige.app.base.AppBaseActivity;
import com.music.player.haige.app.base.pref.DevicePrefHelper;
import com.music.player.haige.app.constants.IntentConstants;
import com.music.player.haige.app.event.EditUserInfoEvent;
import com.music.player.haige.app.utils.NotificationUtils;
import com.music.player.haige.app.web.WebViewActivity;
import com.music.player.haige.di.component.DaggerCommonComponent;
import com.music.player.haige.di.module.CommonModule;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.model.entity.update.UpdateBean;
import com.music.player.haige.mvp.presenter.MainPresenter;
import com.music.player.haige.mvp.ui.main.fragment.PlayMainFragment;
import com.music.player.haige.mvp.ui.main.fragment.SearchFragment;
import com.music.player.haige.mvp.ui.music.fragment.MusicFragment;
import com.music.player.haige.mvp.ui.music.service.MusicServiceConnection;
import com.music.player.haige.mvp.ui.setting.fragment.AboutFragment;
import com.music.player.haige.mvp.ui.setting.fragment.CopyrightFragment;
import com.music.player.haige.mvp.ui.setting.fragment.DownloadFragment;
import com.music.player.haige.mvp.ui.setting.fragment.FeedbackFragment;
import com.music.player.haige.mvp.ui.setting.fragment.PolicyFragment;
import com.music.player.haige.mvp.ui.setting.fragment.SettingFragment;
import com.music.player.haige.mvp.ui.user.CropFragment;
import com.music.player.haige.mvp.ui.user.EditFragment;
import com.music.player.haige.mvp.ui.user.EditProfileFragment;
import com.music.player.haige.mvp.ui.user.utils.AvatarUtils;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;
import com.music.player.haige.mvp.ui.utils.Utils;
import com.music.player.haige.mvp.ui.widget.dialog.SystemDialog;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.umeng.socialize.UMShareAPI;

import org.simple.eventbus.Subscriber;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

import static com.music.player.haige.app.Constants.URL_PRIVACY_POLICY;
import static com.music.player.haige.app.utils.NotificationUtils.NOTIFY_ID_UPDATE;
import static com.music.player.haige.app.utils.NotificationUtils.NOTIFY_TYPE_COMMON;

public class MainActivity extends AppBaseActivity<MainPresenter> implements MVContract.CommonView, SystemUIConfig, ActivityLifecycleCallbacksImpl.BindMusicService {

    @BindView(R.id.app_activity_main_container_layout)
    FrameLayout mContainer;

    private long mLastExitTime; // 记录上次点击退出按钮的事件
    private int mEditType = 0;
    private Uri mChooseAvatarPath;

    private Runnable mainLibrary = () -> {
        Fragment fragment = MainFragment.newInstance();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.app_activity_main_container_layout, fragment, MainFragment.class.getName());
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.commitAllowingStateLoss();
        //transaction.addToBackStack(null);
    };

    private Runnable musicListLibrary = () -> {
        Fragment fragment = PlayMainFragment.newInstance(PlayMainFragment.PLAY_MAIN_CURRENT_LIST);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_from_bottom, R.anim.slide_out_to_left, R.anim.slide_in_from_left, R.anim.slide_out_to_bottom);
        transaction.replace(R.id.app_activity_main_foreground_layout, fragment, PlayMainFragment.class.getName());
        transaction.commitAllowingStateLoss();
        transaction.addToBackStack(null);
    };

    private Runnable musicPlayLibrary = () -> {
        Fragment fragment = PlayMainFragment.newInstance(PlayMainFragment.PLAY_MAIN_CURRENT_PLAY);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_from_bottom, R.anim.slide_out_to_left, R.anim.slide_in_from_left, R.anim.slide_out_to_bottom);
        transaction.replace(R.id.app_activity_main_foreground_layout, fragment, PlayMainFragment.class.getName());
        transaction.commitAllowingStateLoss();
        transaction.addToBackStack(null);
    };

    // 本地音乐
    private Runnable localLibrary = () -> {
        Fragment fragment = MusicFragment.newInstance(Constants.NAVIGATE_ALLSONG);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_from_right, R.anim.slide_out_to_left, R.anim.slide_in_from_left, R.anim.slide_out_to_right);
        transaction.add(R.id.app_activity_main_container_layout, fragment, MusicFragment.class.getName() + Constants.NAVIGATE_ALLSONG);
        Fragment curFragment = getSupportFragmentManager().findFragmentById(R.id.app_activity_main_container_layout);
        if (curFragment != null && curFragment.isVisible()) {
            transaction.hide(curFragment);
        }
        transaction.commitAllowingStateLoss();
        transaction.addToBackStack(null);
    };

    // 最近播放
    private Runnable recentPlayLibrary = () -> {
        Fragment fragment = MusicFragment.newInstance(Constants.NAVIGATE_PLAYLIST_RECENTPLAY);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_from_right, R.anim.slide_out_to_left, R.anim.slide_in_from_left, R.anim.slide_out_to_right);
        transaction.add(R.id.app_activity_main_container_layout, fragment, MusicFragment.class.getName() + Constants.NAVIGATE_PLAYLIST_RECENTPLAY);
        Fragment curFragment = getSupportFragmentManager().findFragmentById(R.id.app_activity_main_container_layout);
        if (curFragment != null && curFragment.isVisible()) {
            transaction.hide(curFragment);
        }
        transaction.commitAllowingStateLoss();
        transaction.addToBackStack(null);
    };

    // 喜欢的音乐
    private Runnable favoriteLibrary = () -> {
        Fragment fragment = MusicFragment.newInstance(Constants.NAVIGATE_PLAYLIST_FAVORITE);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_from_right, R.anim.slide_out_to_left, R.anim.slide_in_from_left, R.anim.slide_out_to_right);
        transaction.add(R.id.app_activity_main_container_layout, fragment, MusicFragment.class.getName() + Constants.NAVIGATE_PLAYLIST_FAVORITE);
        Fragment curFragment = getSupportFragmentManager().findFragmentById(R.id.app_activity_main_container_layout);
        if (curFragment != null && curFragment.isVisible()) {
            transaction.hide(curFragment);
        }
        transaction.commitAllowingStateLoss();
        transaction.addToBackStack(null);
    };

    // 设置
    private Runnable settingLibrary = () -> {
        Fragment fragment = SettingFragment.newInstance();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_from_right, R.anim.slide_out_to_left, R.anim.slide_in_from_left, R.anim.slide_out_to_right);
        transaction.add(R.id.app_activity_main_container_layout, fragment, SettingFragment.class.getName());
        Fragment curFragment = getSupportFragmentManager().findFragmentById(R.id.app_activity_main_container_layout);
        if (curFragment != null && curFragment.isVisible()) {
            transaction.hide(curFragment);
        }
        transaction.commitAllowingStateLoss();
        transaction.addToBackStack(null);
    };

    // 搜索
    private Runnable searchLibrary = () -> {
        Fragment fragment = SearchFragment.newInstance(Constants.SEARCH.NET);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_from_right, R.anim.slide_out_to_left, R.anim.slide_in_from_left, R.anim.slide_out_to_right);
        transaction.add(R.id.app_activity_main_container_layout, fragment, SearchFragment.class.getName() + Constants.SEARCH.NET);
        Fragment curFragment = getSupportFragmentManager().findFragmentById(R.id.app_activity_main_container_layout);
        if (curFragment != null && curFragment.isVisible()) {
            transaction.hide(curFragment);
        }
        transaction.commitAllowingStateLoss();
        transaction.addToBackStack(null);
    };

    // 本地搜索
    private Runnable searchLocalLibrary = () -> {
        Fragment fragment = SearchFragment.newInstance(Constants.SEARCH.LOCAL);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_from_right, R.anim.slide_out_to_left, R.anim.slide_in_from_left, R.anim.slide_out_to_right);
        transaction.add(R.id.app_activity_main_container_layout, fragment, SearchFragment.class.getName() + Constants.SEARCH.LOCAL);
        Fragment curFragment = getSupportFragmentManager().findFragmentById(R.id.app_activity_main_container_layout);
        if (curFragment != null && curFragment.isVisible()) {
            transaction.hide(curFragment);
        }
        transaction.commitAllowingStateLoss();
        transaction.addToBackStack(null);
    };

    // 最近播放搜索
    private Runnable searchRecentLibrary = () -> {
        Fragment fragment = SearchFragment.newInstance(Constants.SEARCH.RECENT);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_from_right, R.anim.slide_out_to_left, R.anim.slide_in_from_left, R.anim.slide_out_to_right);
        transaction.add(R.id.app_activity_main_container_layout, fragment, SearchFragment.class.getName() + Constants.SEARCH.RECENT);
        Fragment curFragment = getSupportFragmentManager().findFragmentById(R.id.app_activity_main_container_layout);
        if (curFragment != null && curFragment.isVisible()) {
            transaction.hide(curFragment);
        }
        transaction.commitAllowingStateLoss();
        transaction.addToBackStack(null);
    };

    // 下载
    private Runnable downloadLibrary = () -> {
        Fragment fragment = DownloadFragment.newInstance();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_from_right, R.anim.slide_out_to_left, R.anim.slide_in_from_left, R.anim.slide_out_to_right);
        transaction.add(R.id.app_activity_main_container_layout, fragment, DownloadFragment.class.getName());
        Fragment curFragment = getSupportFragmentManager().findFragmentById(R.id.app_activity_main_container_layout);
        if (curFragment != null && curFragment.isVisible()) {
            transaction.hide(curFragment);
        }
        transaction.commitAllowingStateLoss();
        transaction.addToBackStack(null);
    };

    // 问题反馈
    private Runnable feedbackLibrary = () -> {
        Fragment fragment = FeedbackFragment.newInstance();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_from_right, R.anim.slide_out_to_left, R.anim.slide_in_from_left, R.anim.slide_out_to_right);
        transaction.add(R.id.app_activity_main_container_layout, fragment, FeedbackFragment.class.getName());
        Fragment curFragment = getSupportFragmentManager().findFragmentById(R.id.app_activity_main_container_layout);
        if (curFragment != null && curFragment.isVisible()) {
            transaction.hide(curFragment);
        }
        transaction.commitAllowingStateLoss();
        transaction.addToBackStack(null);
    };

    // 隐私政策
    private Runnable policyLibrary = () -> {
        Fragment fragment = PolicyFragment.newInstance();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_from_right, R.anim.slide_out_to_left, R.anim.slide_in_from_left, R.anim.slide_out_to_right);
        transaction.add(R.id.app_activity_main_container_layout, fragment, PolicyFragment.class.getName());
        Fragment curFragment = getSupportFragmentManager().findFragmentById(R.id.app_activity_main_container_layout);
        if (curFragment != null && curFragment.isVisible()) {
            transaction.hide(curFragment);
        }
        transaction.commitAllowingStateLoss();
        transaction.addToBackStack(null);
    };

    // 版权声明
    private Runnable copyrightLibrary = () -> {
        Fragment fragment = CopyrightFragment.newInstance();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_from_right, R.anim.slide_out_to_left, R.anim.slide_in_from_left, R.anim.slide_out_to_right);
        transaction.add(R.id.app_activity_main_container_layout, fragment, CopyrightFragment.class.getName());
        Fragment curFragment = getSupportFragmentManager().findFragmentById(R.id.app_activity_main_container_layout);
        if (curFragment != null && curFragment.isVisible()) {
            transaction.hide(curFragment);
        }
        transaction.commitAllowingStateLoss();
        transaction.addToBackStack(null);
    };
    // 关于
    private Runnable aboutLibrary = () -> {
        Fragment fragment = AboutFragment.newInstance();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_from_right, R.anim.slide_out_to_left, R.anim.slide_in_from_left, R.anim.slide_out_to_right);
        transaction.add(R.id.app_activity_main_container_layout, fragment, AboutFragment.class.getName());
        Fragment curFragment = getSupportFragmentManager().findFragmentById(R.id.app_activity_main_container_layout);
        if (curFragment != null && curFragment.isVisible()) {
            transaction.hide(curFragment);
        }
        transaction.commitAllowingStateLoss();
        transaction.addToBackStack(null);
    };

    private Runnable editProfileLibrary = () -> {
        Fragment fragment = EditProfileFragment.newInstance();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_from_right, R.anim.slide_out_to_left, R.anim.slide_in_from_left, R.anim.slide_out_to_right);
        transaction.add(R.id.app_activity_main_container_layout, fragment, AboutFragment.class.getName());
        Fragment curFragment = getSupportFragmentManager().findFragmentById(R.id.app_activity_main_container_layout);
        if (curFragment != null && curFragment.isVisible()) {
            transaction.hide(curFragment);
        }
        transaction.commitAllowingStateLoss();
        transaction.addToBackStack(null);
    };

    private Runnable editLibrary = () -> {
        Fragment fragment = EditFragment.newInstance(mEditType);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_from_right, R.anim.slide_out_to_left, R.anim.slide_in_from_left, R.anim.slide_out_to_right);
        transaction.add(R.id.app_activity_main_container_layout, fragment, AboutFragment.class.getName());
        Fragment curFragment = getSupportFragmentManager().findFragmentById(R.id.app_activity_main_container_layout);
        if (curFragment != null && curFragment.isVisible()) {
            transaction.hide(curFragment);
        }
        transaction.commitAllowingStateLoss();
        transaction.addToBackStack(null);
    };

    private Runnable cropLibrary = () -> {
        Fragment fragment = CropFragment.newInstance(mChooseAvatarPath);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_from_right, R.anim.slide_out_to_left, R.anim.slide_in_from_left, R.anim.slide_out_to_right);
        transaction.add(R.id.app_activity_main_container_layout, fragment, AboutFragment.class.getName());
        Fragment curFragment = getSupportFragmentManager().findFragmentById(R.id.app_activity_main_container_layout);
        if (curFragment != null && curFragment.isVisible()) {
            transaction.hide(curFragment);
        }
        transaction.commitAllowingStateLoss();
        transaction.addToBackStack(null);
    };

    @Override
    public void onBackPressed() {
        if(Utils.isNotNull(getVisibleFragment())) {
            if (getVisibleFragment() instanceof EditProfileFragment) {
                EditProfileFragment fragment = (EditProfileFragment) getVisibleFragment();
                fragment.onBackPressed();
            } else {
                super.onBackPressed();
            }
        } else {
            super.onBackPressed();
        }
    }

    public Fragment getVisibleFragment(){
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        for(Fragment fragment : fragments){
            if(fragment != null && fragment.isVisible())
                return fragment;
        }
        return null;
    }

    @Override
    public void finish() {
        // 两秒内点击退出按钮才真正退出
        long currentTime = System.currentTimeMillis();
        if (currentTime - mLastExitTime > 2000) {
            CoreUtils.makeText(this, R.string.app_twice_exit);
            mLastExitTime = currentTime;
        } else {
            super.finish();
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
    }

    @Override
    public void setupActivityComponent(AppComponent appComponent) {
        DaggerCommonComponent
                .builder()
                .appComponent(appComponent)
                .commonModule(new CommonModule(this))
                .build()
                .inject(this);
    }

    @Override
    public int initView(Bundle savedInstanceState) {
        return R.layout.app_activity_main;
    }

    @Override
    public void initData(Bundle savedInstanceState) {
        UMShareAPI.get(this);//初始化sdk
        mainLibrary.run();
        if (Utils.isNotNull(getIntent())) {
            int notifyType = getIntent().getIntExtra(IntentConstants.EXTRA_CLICK_NOTIFY, 0);
            Song song = getIntent().getParcelableExtra(IntentConstants.EXTRA_NOTIFY_MUSIC);
            if (notifyType == NotificationUtils.NOTIFY_TYPE_MUSIC) {
                ArrayList<Song> songs = new ArrayList<>();
                songs.add(song);
                mContainer.postDelayed(() -> {
                    MusicServiceConnection.playAll(songs, 0, false);
                }, 200);
            }
        }
    }

    @Override
    public RxPermissions getRxPermissions() {
        return new RxPermissions(this);
    }

    @Override
    public boolean translucentStatusBar() {
        return true;
    }

    @Override
    public int setStatusBarColor() {
        return getResources().getColor(R.color.color_00000000);
    }

    @Override
    public boolean lightStatusBar() {
        return false;
    }

    @Override
    public boolean fitSystemWindows() {
        return false;
    }

    @Override
    public boolean fullScreen() {
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);

        if (requestCode == AvatarUtils.REQUEST_CAMERA && resultCode == RESULT_OK) {
            String path = "file:" + AvatarUtils.getCameraImageFilePath();
            if (!TextUtils.isEmpty(path)) {
                mChooseAvatarPath = Uri.parse(path);
                cropLibrary.run();
            }
        } else if (requestCode == AvatarUtils.REQUEST_GALLERY && resultCode == RESULT_OK) {
            mChooseAvatarPath = data.getData();
            cropLibrary.run();
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        checkUpdate();
    }

    @Subscriber
    public void onSubscriber(EventBusTags.MAIN tags) {
        switch (tags) {
            case BACK:
                getSupportFragmentManager().popBackStackImmediate();
                break;
            case MUSIC:
                localLibrary.run();
                break;
            case MUSIC_PLAY_LIST:
                musicListLibrary.run();
                break;
            case MUSIC_PLAY:
                musicPlayLibrary.run();
                break;
            case RECENT_PLAY:
                recentPlayLibrary.run();
                break;
            case FAVORITE:
                favoriteLibrary.run();
                break;
            case SETTING:
                settingLibrary.run();
                break;
            case SEARCH:
                searchLibrary.run();
                break;
            case SEARCH_LOCAL:
                searchLocalLibrary.run();
                break;
            case SEARCH_RECENT:
                searchRecentLibrary.run();
                break;
            case DOWNLOAD:
                downloadLibrary.run();
                break;
            case PLAY_HIDE:
                break;
            case PLAY_SHOW:
                break;
            case SETTING_FEEDBACK:
                feedbackLibrary.run();
                break;
            case SETTING_POLICY:
                WebViewActivity.start(this, URL_PRIVACY_POLICY);
                break;
            case SETTING_COPYRIGHT:
                copyrightLibrary.run();
                break;
            case SETTING_ABOUT:
                aboutLibrary.run();
                break;
            case USER_PROFILE_EDIT:
                editProfileLibrary.run();
                break;
            case REQUEST_CAMERA_PERMISSION:
                mPresenter.requestPermission();
                break;
            case UPDATE_DIALOG:
                checkUpdate();
                break;
        }
    }

    @Subscriber
    public void onSubscriber(EditUserInfoEvent event) {
        mEditType = event.getType();
        editLibrary.run();
    }

    @Subscriber
    public void onSubscriber(EventBusTags.MetaChangedEvent event) {

    }

    private void checkUpdate() {
        UpdateBean updateBean = DevicePrefHelper.getUpdateConfig();
        if (BuildConfig.VERSION_CODE < updateBean.getLatestVersion()) {
            boolean isForceUpdate = false;
            if (BuildConfig.VERSION_CODE < updateBean.getForceVersion()) {
                isForceUpdate = true;
            }

            if (isForceUpdate) {
                showUpdateDialog(updateBean, isForceUpdate);
                showUpdateNotification(updateBean);
            } else if (!DateUtils.isSameDay(DevicePrefHelper.getShowUpdateTime(), System.currentTimeMillis())) {
                showUpdateDialog(updateBean, isForceUpdate);
                showUpdateNotification(updateBean);

                DevicePrefHelper.putShowUpdateTime(System.currentTimeMillis());
            }
        }
    }

    private void showUpdateDialog(UpdateBean updateBean, boolean isForceUpdate) {
        new SystemDialog().create(getSupportFragmentManager())
                .setCancelOutside(!isForceUpdate)
                .setContent(updateBean.getContent())
                .setOk(ResourceUtils.resourceString(R.string.app_update))
                .setCancle(isForceUpdate ? "" : ResourceUtils.resourceString(R.string.app_cancel))
                .setOnOkListener(() -> {
                    try {
                        Uri uri = Uri.parse(updateBean.getPath());
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    } catch (Exception e) {
                        Toast.makeText(this, R.string.update_not_installed_appstore, Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                })
                .setOnCancelListener(() -> {
                })
                .show();
    }

    private void showUpdateNotification(UpdateBean updateBean) {
        Uri uri = Uri.parse(updateBean.getPath());
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
        NotificationUtils.showCommonNotification(this,
                pendingIntent,
                ResourceUtils.resourceString(R.string.update_notify_title),
                ResourceUtils.resourceString(R.string.update_notify_content),
                NOTIFY_TYPE_COMMON,
                NOTIFY_ID_UPDATE);
    }
}
