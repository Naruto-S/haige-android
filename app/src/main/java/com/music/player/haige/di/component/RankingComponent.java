package com.music.player.haige.di.component;

import com.hc.core.di.component.AppComponent;
import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.di.module.RankingModule;
import com.music.player.haige.mvp.ui.main.fragment.RankingNewFragment;
import com.music.player.haige.mvp.ui.upload.MusicTypeActivity;

import dagger.Component;

/**
 * ================================================
 * Created by huangcong on 2018/3/3.
 * ================================================
 */
@FragmentScope
@Component(modules = {RankingModule.class}, dependencies = AppComponent.class)
public interface RankingComponent {

    void inject(RankingNewFragment fragment);

    void inject(MusicTypeActivity activity);

}
