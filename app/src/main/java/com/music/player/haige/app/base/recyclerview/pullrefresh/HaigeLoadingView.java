package com.music.player.haige.app.base.recyclerview.pullrefresh;

import android.content.Context;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.music.player.haige.BuildConfig;
import com.music.player.haige.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Naruto on 2016/9/13.
 */
public class HaigeLoadingView extends FrameLayout {

    @BindView(R.id.sdv_loading)
    SimpleDraweeView mLoadingSDV;

    public HaigeLoadingView(Context context) {
        super(context);
    }

    public HaigeLoadingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public HaigeLoadingView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context
                .LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.layout_haige_loading, this, true);
        ButterKnife.bind(this, this);

        Uri uri = Uri.parse("res://" + BuildConfig.APPLICATION_ID + "/" + R.drawable.ic_webp_loading);

        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setUri(uri)
                .setAutoPlayAnimations(true)
                .build();
        mLoadingSDV.setController(controller);
    }
}
