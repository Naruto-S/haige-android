package com.music.player.haige.mvp.ui.utils;

import io.reactivex.disposables.Disposable;

/**
 * Created by john on 2018/3/20.
 */

public class RxJavaUtils {

    public static void dispose(Disposable disposable) {
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
    }

    public static boolean isDisposed(Disposable disposable) {
        if (disposable != null) {
            return disposable.isDisposed();
        } else {
            return true;
        }
    }
}
