package com.music.player.haige.mvp.model;

import com.hc.base.mvp.BaseModel;
import com.hc.core.integration.IRepositoryManager;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.service.ApiService;
import com.music.player.haige.mvp.model.entity.user.LoginRequest;
import com.music.player.haige.mvp.model.entity.user.UserResponse;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by Naruto on 2018/5/5.
 */

public class LoginModel extends BaseModel implements MVContract.LoginModel {

    @Inject
    public LoginModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public Observable<UserResponse> userLoginOrRegistrer(LoginRequest request) {
        return Observable.just(mRepositoryManager.obtainRetrofitService(ApiService.class).userLoginOrRegistrer(request))
                .flatMap(observable -> observable);
    }

    @Override
    public Observable<UserResponse> getUserProfile() {
        return Observable.just(mRepositoryManager.obtainRetrofitService(ApiService.class).getUserProfile())
                .flatMap(observable -> observable);
    }
}
