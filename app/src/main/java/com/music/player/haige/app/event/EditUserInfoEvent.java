package com.music.player.haige.app.event;

public class EditUserInfoEvent {

    private int type;

    public EditUserInfoEvent(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
