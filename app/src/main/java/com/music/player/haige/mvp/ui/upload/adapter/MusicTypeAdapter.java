package com.music.player.haige.mvp.ui.upload.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.music.player.haige.R;
import com.music.player.haige.mvp.model.entity.rank.RankCategoryBean;

import java.util.ArrayList;

public class MusicTypeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<RankCategoryBean> mData = new ArrayList<>();
    private OnItemClickListener mClickListener;

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_music_type, viewGroup, false);
        return new ItemHolder(v, mClickListener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ItemHolder itemHolder = (ItemHolder) holder;
        RankCategoryBean category = mData.get(position);
        itemHolder.type.setText(category.getTag());
        itemHolder.select.setVisibility(category.isSelect() ? View.VISIBLE : View.GONE);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setData(ArrayList<RankCategoryBean> data) {
        mData.clear();
        mData.addAll(data);
        notifyDataSetChanged();
    }

    public ArrayList<RankCategoryBean> getData() {
        return mData;
    }

    public void setSelect(int position) {
        if (position >= 0 && position < mData.size()) {
            RankCategoryBean category = mData.get(position);
            category.setSelect(!category.isSelect());
            mData.set(position, category);
            notifyItemChanged(position);
        }
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mClickListener = listener;
    }

    public class ItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView type;
        public ImageView select;
        private OnItemClickListener mListener;

        public ItemHolder(View view, OnItemClickListener listener) {
            super(view);
            this.type = view.findViewById(R.id.tv_music_type);
            this.select = view.findViewById(R.id.iv_select);
            view.setOnClickListener(this);
            mListener = listener;
        }

        @Override
        public void onClick(View v) {
            mListener.onItemClick(v, getAdapterPosition());
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int postion);
    }
}
