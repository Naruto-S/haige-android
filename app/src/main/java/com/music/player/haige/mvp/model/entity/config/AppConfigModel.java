package com.music.player.haige.mvp.model.entity.config;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AppConfigModel implements Serializable {

    /**
     * show_feedback_bar : true
     */

    @SerializedName("show_feedback_bar")
    private boolean isShowFeedbackBar;

    public boolean isIsShowFeedbackBar() {
        return isShowFeedbackBar;
    }

    public void setIsShowFeedbackBar(boolean isShowFeedbackBar) {
        this.isShowFeedbackBar = isShowFeedbackBar;
    }

    @Override
    public String toString() {
        return "AppConfigModel{" +
                "isShowFeedbackBar=" + isShowFeedbackBar +
                '}';
    }
}
