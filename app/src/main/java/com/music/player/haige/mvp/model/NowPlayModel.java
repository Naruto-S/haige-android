package com.music.player.haige.mvp.model;

import com.hc.base.mvp.BaseModel;
import com.hc.core.integration.IRepositoryManager;
import com.hc.core.utils.RxNotNullOptional;
import com.music.player.haige.app.GlobalConfiguration;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.service.ApiService;
import com.music.player.haige.mvp.model.api.service.KuGouApiService;
import com.music.player.haige.mvp.model.entity.BaseResponse;
import com.music.player.haige.mvp.model.entity.action.ActionRequest;
import com.music.player.haige.mvp.model.entity.action.CommentLikeRequest;
import com.music.player.haige.mvp.model.entity.comment.CommentRequest;
import com.music.player.haige.mvp.model.entity.comment.CommentResponse;
import com.music.player.haige.mvp.model.entity.comment.SendCommentResponse;
import com.music.player.haige.mvp.model.entity.music.KuGouRawLyric;
import com.music.player.haige.mvp.model.entity.music.KuGouSearchLyricResult;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.model.entity.music.WebpBgResponse;
import com.music.player.haige.mvp.model.utils.LyricUtil;
import com.music.player.haige.mvp.model.utils.StringUtils;
import com.music.player.haige.mvp.ui.music.service.MusicServiceConnection;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * ================================================
 * Created by huangcong on 2018/2/27.
 * ================================================
 */

public class NowPlayModel extends BaseModel implements MVContract.NowPlayModel {

    @Inject
    public NowPlayModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public Observable<ArrayList<Song>> loadCurrentMusic() {
        return Observable.just(new RxNotNullOptional(MusicServiceConnection.getPlayinfos()))
                .flatMap(longSongOptional -> {
                    long[] songIds = MusicServiceConnection.getQueue();
                    ArrayList<Song> songs = new ArrayList<>();
                    if (!longSongOptional.isEmpty()) {
                        HashMap<Long, Song> longSongHashMap = (HashMap<Long, Song>) longSongOptional.get();
                        for (long songId : songIds) {
                            songs.add(longSongHashMap.get(songId));
                        }
                    }
                    return Observable.just(songs);
                });
    }

    @Override
    public Observable<WebpBgResponse> getMusicWebpBg(String type) {
        return Observable.just(mRepositoryManager.obtainRetrofitService(ApiService.class).getMusicWebpBg(type))
                .flatMap(observable -> observable);
    }

    @Override
    public Observable<SendCommentResponse> sendMusicComment(CommentRequest request) {
        return Observable.just(mRepositoryManager.obtainRetrofitService(ApiService.class).sendMusicComment(request))
                .flatMap(observable -> observable);
    }

    @Override
    public Observable<CommentResponse> getMusicComment(long musicId, int pageSize, String lastId) {
        return Observable.just(mRepositoryManager.obtainRetrofitService(ApiService.class).getMusicComment(musicId, pageSize, lastId))
                .flatMap(observable -> observable);

    }

    @Override
    public Observable<File> getLyricFile(String title, String artist, long duration) {
        KuGouApiService mKuGouApiService = GlobalConfiguration.sKugouRetrofit.create(KuGouApiService.class);
        return mKuGouApiService.searchLyric(title, String.valueOf(duration))
                .flatMap(kuGouSearchLyricResult -> {
                    if (kuGouSearchLyricResult.status == 200
                            && kuGouSearchLyricResult.candidates != null
                            && kuGouSearchLyricResult.candidates.size() != 0) {
                        KuGouSearchLyricResult.Candidates candidates = kuGouSearchLyricResult.candidates.get(0);
                        return mKuGouApiService.getRawLyric(candidates.id, candidates.accesskey);
                    } else {
                        return Observable.just(new KuGouRawLyric());
                    }
                })
                .map(kuGouRawLyric -> {
                    if (kuGouRawLyric == null) {
                        return null;
                    }
                    String rawLyric = LyricUtil.decryptBASE64(kuGouRawLyric.content);
                    if (StringUtils.isEmpty(rawLyric)) {
                        return null;
                    }
                    return LyricUtil.writeLrcToLoc(title, artist, rawLyric);
                });
    }

    @Override
    public Observable<BaseResponse> countLike(long musicId) {
        ActionRequest request = new ActionRequest();
        request.setMusicId(musicId);

        return Observable.just(mRepositoryManager.obtainRetrofitService(ApiService.class).countLike(request))
                .flatMap(observable -> observable);
    }

    @Override
    public Observable<BaseResponse> countShare(long musicId) {
        ActionRequest request = new ActionRequest();
        request.setMusicId(musicId);

        return Observable.just(mRepositoryManager.obtainRetrofitService(ApiService.class).countShare(request))
                .flatMap(observable -> observable);
    }

    @Override
    public Observable<BaseResponse> countDownload(long musicId) {
        ActionRequest request = new ActionRequest();
        request.setMusicId(musicId);

        return Observable.just(mRepositoryManager.obtainRetrofitService(ApiService.class).countDownload(request))
                .flatMap(observable -> observable);
    }

    @Override
    public Observable<CommentResponse> getCommentSub(String commentId, int pageSize, int pageNo) {
        return Observable.just(mRepositoryManager.obtainRetrofitService(ApiService.class).getCommentSub(commentId, pageSize, pageNo))
                .flatMap(observable -> observable);
    }

    @Override
    public Observable<BaseResponse> deleteComment(CommentRequest request) {
        return Observable.just(mRepositoryManager.obtainRetrofitService(ApiService.class).deleteComment(request.getCommentId(), request.isSub()))
                .flatMap(observable -> observable);
    }

    @Override
    public Observable<BaseResponse> commentLike(CommentLikeRequest request) {
        return Observable.just(mRepositoryManager.obtainRetrofitService(ApiService.class).commentLike(request))
                .flatMap(observable -> observable);
    }
}

