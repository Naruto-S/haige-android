package com.music.player.haige.mvp.model.entity.search;

import com.music.player.haige.mvp.model.entity.BaseResponse;
import com.music.player.haige.mvp.model.entity.music.Song;

import java.util.ArrayList;

/**
 * =============================
 * Created by huangcong on 2018/3/10.
 * =============================
 */

public class SearchKeyWordResponse extends BaseResponse<ArrayList<Song>> {

}