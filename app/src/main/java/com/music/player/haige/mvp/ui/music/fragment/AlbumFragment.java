package com.music.player.haige.mvp.ui.music.fragment;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.hc.core.di.component.AppComponent;
import com.music.player.haige.R;
import com.music.player.haige.app.Constants;
import com.music.player.haige.app.EventBusTags;
import com.music.player.haige.app.base.AppBaseLazyFragment;
import com.music.player.haige.di.component.DaggerAlbumComponent;
import com.music.player.haige.di.module.AlbumModule;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.entity.music.Album;
import com.music.player.haige.mvp.presenter.AlbumPresenter;
import com.music.player.haige.mvp.ui.music.adapter.AlbumAdapter;
import com.music.player.haige.mvp.ui.widget.SpacesItemDecoration;
import com.music.player.haige.mvp.ui.widget.StatusLayout;

import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;

import java.util.List;

import butterknife.BindView;

/**
 * ================================================
 * Created by huangcong on 2018/3/2.
 * <p>
 * 专辑
 * ================================================
 */

public class AlbumFragment extends AppBaseLazyFragment<AlbumPresenter> implements MVContract.AlbumView {

    @BindView(R.id.app_layout_sl)
    StatusLayout mStatusLayout;

    @BindView(R.id.app_layout_rv)
    RecyclerView mAlbumRecyclerView;

    @BindView(R.id.app_layout_progress)
    ProgressBar mLoadingView;

    private String mAction;
    private AlbumAdapter mAlbumAdapter;

    public static AlbumFragment newInstance(String action) {
        Bundle args = new Bundle();
        switch (action) {
            case Constants.NAVIGATE_ALLSONG:
                args.putString(Constants.PLAYLIST_TYPE, action);
                break;
            case Constants.NAVIGATE_PLAYLIST_RECENTADD:
                args.putString(Constants.PLAYLIST_TYPE, action);
                break;
            case Constants.NAVIGATE_PLAYLIST_RECENTPLAY:
                args.putString(Constants.PLAYLIST_TYPE, action);
                break;
            case Constants.NAVIGATE_PLAYLIST_FAVORITE:
                args.putString(Constants.PLAYLIST_TYPE, action);
                break;
            default:
                throw new RuntimeException("wrong action type");
        }
        AlbumFragment fragment = new AlbumFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void setupFragmentComponent(AppComponent appComponent) {
        DaggerAlbumComponent
                .builder()
                .appComponent(appComponent)
                .albumModule(new AlbumModule(this))
                .build()
                .inject(this);
    }

    @Override
    public View initView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.app_layout_recyclerview, container, false);
    }

    @Override
    public void lazyInitData(Bundle savedInstanceState) {
        mAction = getArguments().getString(Constants.PLAYLIST_TYPE);

        mStatusLayout.setOnStatusClickListener(new StatusLayout.OnStatusClickListener() {
            @Override
            public void onEmptyClick() {
                EventBus.getDefault().post(EventBusTags.UI.GO_TO_RECOMMEND);
                EventBus.getDefault().post(EventBusTags.MAIN.BACK);
            }

            @Override
            public void onReloadClick() {

            }
        });

        mAlbumAdapter = new AlbumAdapter(getActivity(), mAction);
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
        int spacingInPixels = getActivity().getResources().getDimensionPixelSize(R.dimen.dp_4);
        SpacesItemDecoration itemDecoration = new SpacesItemDecoration(spacingInPixels);
        mAlbumRecyclerView.setLayoutManager(layoutManager);
        mAlbumRecyclerView.setHasFixedSize(true);
        mAlbumRecyclerView.addItemDecoration(itemDecoration);
        mAlbumRecyclerView.setAdapter(mAlbumAdapter);


        mPresenter.loadAlbums(mAction);
    }

    @Override
    public void showLoading() {
        super.showLoading();
        mLoadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
        mLoadingView.setVisibility(View.GONE);
    }

    @Override
    public void showAlbum(List<Album> artists) {
        mStatusLayout.showStatusView(StatusLayout.STATUS.OK);
        mAlbumRecyclerView.setVisibility(View.VISIBLE);
        mAlbumAdapter.setAlbumsList(artists);
    }

    @Override
    public void showEmptyView() {
        mStatusLayout.showStatusView(StatusLayout.STATUS.EMPTY);
        mAlbumRecyclerView.setVisibility(View.INVISIBLE);
    }

    @Subscriber
    public void onSubscriber(EventBusTags.FavoriteSongEvent event) {
        if (!Constants.NAVIGATE_PLAYLIST_FAVORITE.equals(mAction)) {
            return;
        }
        mPresenter.loadAlbums(mAction);
    }

    @Subscriber
    public void onSubscriber(EventBusTags.RecentlyPlayEvent event) {
        if (!Constants.NAVIGATE_PLAYLIST_RECENTPLAY.equals(mAction)) {
            return;
        }
        mPresenter.loadAlbums(mAction);
    }

    @Subscriber
    public void onSubscriber(EventBusTags.MediaUpdateEvent event) {
        if (Constants.NAVIGATE_PLAYLIST_RECENTPLAY.equals(mAction)
                || Constants.NAVIGATE_PLAYLIST_RECENTPLAY.equals(mAction)) {
            return;
        }
        mPresenter.loadAlbums(mAction);
    }
}
