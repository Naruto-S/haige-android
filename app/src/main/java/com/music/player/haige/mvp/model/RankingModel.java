package com.music.player.haige.mvp.model;

import com.hc.base.mvp.BaseModel;
import com.hc.core.integration.IRepositoryManager;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.service.ApiService;
import com.music.player.haige.mvp.model.entity.rank.NewRankTagsResponse;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * ================================================
 * Created by huangcong on 2018/3/3.
 * ================================================
 */

public class RankingModel extends BaseModel implements MVContract.RankingModel {

    @Inject
    public RankingModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public Observable<NewRankTagsResponse> getRankingTags() {
        return Observable.just(mRepositoryManager.obtainRetrofitService(ApiService.class).getNewRankTag())
                .flatMap(observable -> observable);
    }
}
