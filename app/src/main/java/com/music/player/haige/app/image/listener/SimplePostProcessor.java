package com.music.player.haige.app.image.listener;

import android.graphics.Bitmap;

/**
 * 简单的图片后处理这
 * <p>
 * Created by ZaKi on 2016/9/2.
 */
public class SimplePostProcessor extends OnPostProcessListener {
    //是否使用缓存，使用缓存只能使用同一个PostProcessor.
    public SimplePostProcessor(String uri, String tag) {
        super(uri, tag);
    }

    @Override
    public boolean hasCache() {
        return true;
    }

    @Override
    protected void post(Bitmap bitmap) {
    }
}
