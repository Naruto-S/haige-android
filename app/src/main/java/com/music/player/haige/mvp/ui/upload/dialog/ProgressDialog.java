package com.music.player.haige.mvp.ui.upload.dialog;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.music.player.haige.BuildConfig;
import com.music.player.haige.R;
import com.music.player.haige.app.EventBusTags;
import com.music.player.haige.app.base.BaseDialogFragment;
import com.music.player.haige.app.base.pref.DevicePrefHelper;
import com.music.player.haige.app.constants.TimeConstants;
import com.music.player.haige.mvp.ui.upload.widget.RoundProgressBarWidthNumber;

import org.simple.eventbus.EventBus;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by john on 2018/1/17.
 */

public class ProgressDialog extends BaseDialogFragment {

    @BindView(R.id.roundProgressBar)
    RoundProgressBarWidthNumber mProgressBar;

    @BindView(R.id.tv_progressbar)
    TextView mProgressTv;

    public static ProgressDialog create(FragmentManager manager) {
        ProgressDialog dialog = new ProgressDialog();
        dialog.setFragmentManger(manager);
        return dialog;
    }

    public ProgressDialog setCancelOutside(boolean cancelOutside) {
        this.mIsCancelOutside = cancelOutside;
        return this;
    }

    public ProgressDialog show() {
        show(mFragmentManager);
        return this;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    @Override
    public int setRootView() {
        return R.layout.dialog_upload_music_progress;
    }

    public void setProgress(int progress) {
        mProgressBar.setProgress(progress);
    }
}
