package com.music.player.haige.mvp.presenter;

import com.music.player.haige.mvp.contract.AppBasePresenter;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.Api;
import com.music.player.haige.mvp.model.entity.action.SendFeedbackRequest;

import javax.inject.Inject;

/**
 * ================================================
 * Created by huangcong on 2018/3/31.
 * ================================================
 */

public class FeedbackFPresenter extends AppBasePresenter<MVContract.FeedbackModel, MVContract.FeedbackView> {

    @Inject
    public FeedbackFPresenter(MVContract.FeedbackModel model, MVContract.FeedbackView view) {
        super(model, view);
    }

    public void sendFeedback(SendFeedbackRequest request) {
        buildObservable(mModel.sendFeedback(request), Api.Action.FEEDBACK);
    }
}
