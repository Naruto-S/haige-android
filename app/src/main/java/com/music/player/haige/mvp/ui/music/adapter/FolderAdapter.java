package com.music.player.haige.mvp.ui.music.adapter;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.hc.base.utils.DensityUtil;
import com.hc.base.widget.fastscroller.FastScrollRecyclerView;
import com.music.player.haige.R;
import com.music.player.haige.app.EventBusTags;
import com.music.player.haige.mvp.model.entity.music.FolderInfo;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.model.respository.dataloader.SongLoader;
import com.music.player.haige.mvp.model.utils.HaigeUtil;
import com.music.player.haige.mvp.model.utils.NavigationUtil;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;
import com.music.player.haige.mvp.ui.widget.dialog.SystemDialog;

import org.simple.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;


public class FolderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements FastScrollRecyclerView.SectionedAdapter {

    private List<FolderInfo> arrayList;
    private AppCompatActivity mContext;

    public FolderAdapter(AppCompatActivity context, List<FolderInfo> arrayList) {
        if (arrayList == null) {
            this.arrayList = new ArrayList<>();
        } else {
            this.arrayList = arrayList;
        }
        this.mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.app_item_folder, viewGroup, false);
        return new ItemHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ItemHolder itemHolder = (ItemHolder) holder;
        FolderInfo localItem = arrayList.get(position);
        Drawable image = mContext.getResources().getDrawable(R.drawable.ic_folder_black_48dp);
        image.setColorFilter(mContext.getResources().getColor(R.color.colorFolderTint), PorterDuff.Mode.SRC_IN);
        itemHolder.image.setImageDrawable(image);
        itemHolder.folderName.setText(localItem.folderName);
        itemHolder.songCount.setText(HaigeUtil.makeLabel(mContext, R.plurals.Nsongs, localItem.songCount));
        itemHolder.folderPath.setText(localItem.folderPath);
        itemHolder.folderPath.setMaxWidth(DensityUtil.dip2px(mContext, 240));
        setOnPopupMenuListener(itemHolder, position);
    }


    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
    }

    private void setOnPopupMenuListener(final ItemHolder itemHolder, final int position) {
        itemHolder.popupMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final PopupMenu menu = new PopupMenu(mContext, v);
                int adapterPosition = itemHolder.getAdapterPosition();
                final FolderInfo folderInfo = arrayList.get(adapterPosition);
                menu.setOnMenuItemClickListener(item -> {
                    switch (item.getItemId()) {
                        case R.id.popup_folder_addto_queue:
                            getSongListIdByFolder(folderInfo.folderPath)
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(ids -> {
                                    });
                            break;
                        case R.id.popup_folder_addto_playlist:
                            getSongListIdByFolder(folderInfo.folderPath)
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(ids -> {
                                    });
                            break;
                        case R.id.popup_folder_delete:
                            new SystemDialog().create(mContext.getSupportFragmentManager())
                                    .setContent("删除文件夹下" + folderInfo.songCount + "首歌曲?")
                                    .setOk(ResourceUtils.resourceString(R.string.app_confirm))
                                    .setCancle(ResourceUtils.resourceString(R.string.app_cancel))
                                    .setOnOkListener(() -> {
                                        getSongListIdByFolder(folderInfo.folderPath)
                                                .subscribeOn(Schedulers.io())
                                                .observeOn(AndroidSchedulers.mainThread())
                                                .subscribe(ids -> {
                                                    HaigeUtil.deleteTracks(mContext, ids);
                                                    EventBus.getDefault().post(new EventBusTags.MediaUpdateEvent());
                                                });
                                    })
                                    .setOnCancelListener(() -> {
                                    })
                                    .show();
                            break;
                    }
                    return false;
                });
                menu.inflate(R.menu.popup_folder);
                menu.show();
            }
        });
    }

    public void setFolderList(List<FolderInfo> arrayList) {
        this.arrayList = arrayList;
        notifyItemRangeInserted(0, arrayList.size());
    }

    @NonNull
    @Override
    public String getSectionName(int position) {
        Character ch = arrayList.get(position).folderName.charAt(0);
        if (Character.isDigit(ch)) {
            return "#";
        } else
            return Character.toString(ch);
    }

    private Observable<long[]> getSongListIdByFolder(String path) {
        return SongLoader.getSongListInFolder(mContext, path)
                .map(new Function<List<Song>, long[]>() {
                    @Override
                    public long[] apply(List<Song> songs) throws Exception {
                        long[] ids = new long[songs.size()];
                        int i = 0;
                        for (Song song : songs) {
                            ids[i] = song.songId;
                            i++;
                        }
                        return ids;
                    }
                });
    }

    public class ItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView image;
        private TextView folderName;
        private TextView songCount;
        private TextView folderPath;
        private ImageView popupMenu;

        public ItemHolder(View view) {
            super(view);
            this.image = view.findViewById(R.id.app_item_image);
            this.folderName = view.findViewById(R.id.text_item_title);
            this.songCount = view.findViewById(R.id.text_item_subtitle);
            this.folderPath = view.findViewById(R.id.text_item_subtitle_2);
            this.popupMenu = view.findViewById(R.id.popup_menu);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            NavigationUtil.navigateToFolderSongs(mContext, arrayList.get(getAdapterPosition()).folderPath);
        }
    }
}


