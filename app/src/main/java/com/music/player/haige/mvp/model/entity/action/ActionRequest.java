package com.music.player.haige.mvp.model.entity.action;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Naruto on 2018/5/12.
 */

public class ActionRequest implements Serializable {

    @SerializedName("music_id")
    private long musicId;

    public long getMusicId() {
        return musicId;
    }

    public void setMusicId(long musicId) {
        this.musicId = musicId;
    }

    @Override
    public String toString() {
        return "ActionRequest{" +
                "musicId='" + musicId + '\'' +
                '}';
    }
}
