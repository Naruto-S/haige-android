package com.music.player.haige.mvp.presenter;

import android.text.TextUtils;

import com.liulishuo.filedownloader.FileDownloader;
import com.liulishuo.filedownloader.model.FileDownloadStatus;
import com.liulishuo.filedownloader.util.FileDownloadUtils;
import com.music.player.haige.app.Constants;
import com.music.player.haige.app.GlobalConfiguration;
import com.music.player.haige.app.HaigeApplication;
import com.music.player.haige.app.base.pref.UserPrefHelper;
import com.music.player.haige.mvp.contract.AppBasePresenter;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.Api;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.model.entity.music.SongDao;
import com.music.player.haige.mvp.ui.music.utils.MusicUtils;

import java.io.File;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.functions.Consumer;

/**
 * ================================================
 * Created by huangcong on 2018/2/27.
 * ================================================
 */

public class SongsPresenter extends AppBasePresenter<MVContract.SongsModel, MVContract.SongsView> {

    private SongDao mFavoriteSongDao;
    private SongDao mDownloadSongDao; // 音乐下载记录
    private SongDao mRecentSongDao;   // 最近播放记录

    @Inject
    public SongsPresenter(MVContract.SongsModel model, MVContract.SongsView view) {
        super(model, view);
        mFavoriteSongDao = GlobalConfiguration.sFavoriteDaoSession.getSongDao();
        mDownloadSongDao = GlobalConfiguration.sDownloadDaoSession.getSongDao();
        mRecentSongDao = GlobalConfiguration.sRecentDaoSession.getSongDao();
    }

    public void loadSongs(String action) {
        Observable<List<Song>> observable;
        if (Constants.NAVIGATE_PLAYLIST_FAVORITE.equals(action)) {
            observable = Observable.mergeArray(
                    mModel.loadSongs(action).getSongList(),
                    Observable.just(mFavoriteSongDao.loadAll()));
        } else if (Constants.NAVIGATE_PLAYLIST_RECENTPLAY.equals(action)) {
            observable = Observable.just(mRecentSongDao.loadAll());
        } else {
            observable = mModel.loadSongs(action).getSongList();
        }
        buildObservable(observable)
                .subscribe(new Consumer<List<Song>>() {
                    @Override
                    public void accept(List<Song> songs) throws Exception {
                        if (songs != null && songs.size() > 0) {
                            mRootView.onSuccess(Api.Action.MUSIC_LIST, songs);
                        } else {
                            mRootView.onError(Api.Action.MUSIC_LIST, Api.State.NORMAL.getCode(), Api.State.NORMAL.getMsg());
                        }
                    }
                });
    }

    /**
     * 添加喜欢的音乐记录
     */
    public void recordFavoriteSong(Song song) {
        if (!isFavoriteSong(song)) {
            mFavoriteSongDao.insert(song);
            mRootView.showMessage(HaigeApplication.getInstance().getString(com.music.player.haige.R.string.app_add_favorite_success));
        }
    }

    /**
     * 删除喜欢的音乐记录
     */
    public boolean deleteFavoriteSong(Song song) {
        if (isFavoriteSong(song)) {
            mFavoriteSongDao.delete(song);
            return true;
        } else {
            return false;
        }
    }

    private boolean isFavoriteSong(Song song) {
        return mFavoriteSongDao.load(song.songId) != null;
    }

    /**
     * 删除最近播放的音乐记录
     */
    public boolean deleteRecentSong(Song song) {
        mRecentSongDao.delete(song);
        return true;
    }

    public boolean downloadMusic(Song song) {
        if (song.isLocal) {
            mRootView.showMessage(HaigeApplication.getInstance().getString(com.music.player.haige.R.string.app_download_local_music));
            return false;
        }
        String url = song.musicPath;
        if (TextUtils.isEmpty(url)) {
            return false;
        }
        // 记录到下载数据库
        if (mDownloadSongDao.load(song.songId) == null) {
            song.setDownloadDate(new Date());
            mDownloadSongDao.insert(song);
        }
        String path = MusicUtils.getDownloadPath(song);
        int id = FileDownloadUtils.generateId(url, path);
        int status = FileDownloader.getImpl().getStatus(id, path);
        System.out.println("===>>> id : " + id + ", status : " + status);
        if (status == FileDownloadStatus.completed || new File(path).exists()) {
            mRootView.showMessage(HaigeApplication.getInstance().getString(com.music.player.haige.R.string.app_download_finish));
            return false;
        } else if (status == FileDownloadStatus.pending || status == FileDownloadStatus.started ||
                status == FileDownloadStatus.connected || status == FileDownloadStatus.progress) {
            mRootView.showMessage(HaigeApplication.getInstance().getString(com.music.player.haige.R.string.app_download_ing));
            return false;
        } else {
            UserPrefHelper.putDownloadMusicCount(UserPrefHelper.getDownloadMusicCount() + 1);
            mRootView.showMessage(HaigeApplication.getInstance().getString(com.music.player.haige.R.string.app_download_join));
        }
        if (!FileDownloader.getImpl().isServiceConnected()) {
            // 开启下载服务
            FileDownloader.getImpl().bindService();
        }
        // 开始下载
        FileDownloader.getImpl().create(url).setPath(path).start();
        return true;
    }

}
