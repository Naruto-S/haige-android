package com.music.player.haige.mvp.ui.launch;

import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.widget.Toast;

import com.hc.core.di.component.AppComponent;
import com.music.player.haige.R;
import com.music.player.haige.app.SystemUIConfig;
import com.music.player.haige.app.base.AppBaseActivity;
import com.music.player.haige.di.component.DaggerCommonComponent;
import com.music.player.haige.di.module.CommonModule;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.presenter.MainPresenter;
import com.music.player.haige.mvp.ui.main.activity.MainActivity;

public class LauncherActivity extends AppBaseActivity<MainPresenter> implements MVContract.CommonView, SystemUIConfig {

    @Override
    public void setupActivityComponent(AppComponent appComponent) {
        DaggerCommonComponent
                .builder()
                .appComponent(appComponent)
                .commonModule(new CommonModule(this))
                .build()
                .inject(this);
    }

    @Override
    public int initView(Bundle savedIstanceState) {
        return R.layout.activity_launcher;
    }

    @Override
    public void initData(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            Toast.makeText(this, getString(R.string.android_version_low), Toast.LENGTH_SHORT).show();
            finish();
        }

        // startActivities()在4.4.*版本的手机会出现空指针(偶现)
        // http://stackoverflow.com/questions/28754754/taskstackbuilderstartactivities-nullpointerexception
        Intent launcherIntent = new Intent(this, SplashActivity.class);
        Intent mainIntent = new Intent(this, MainActivity.class);
        try {
            PendingIntent.getActivities(this, 0,
                    new Intent[]{mainIntent, launcherIntent},
                    PendingIntent.FLAG_ONE_SHOT).send();
        } catch (Exception e) {
            startActivities(new Intent[]{mainIntent, launcherIntent});
        }
        finish();
    }

    @Override
    public boolean translucentStatusBar() {
        return true;
    }

    @Override
    public int setStatusBarColor() {
        return getResources().getColor(R.color.color_00000000);
    }

    @Override
    public boolean lightStatusBar() {
        return false;
    }

    @Override
    public boolean fitSystemWindows() {
        return true;
    }

    @Override
    public boolean fullScreen() {
        return true;
    }
}
