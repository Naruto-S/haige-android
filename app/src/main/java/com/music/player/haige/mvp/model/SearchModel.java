package com.music.player.haige.mvp.model;

import com.hc.base.mvp.BaseModel;
import com.hc.core.integration.IRepositoryManager;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.cache.ApiCache;
import com.music.player.haige.mvp.model.api.service.ApiService;
import com.music.player.haige.mvp.model.entity.search.SearchHotWordResponse;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.rx_cache2.DynamicKey;
import io.rx_cache2.EvictDynamicKey;

/**
 * ================================================
 * Created by huangcong on 2018/3/3.
 * ================================================
 */

public class SearchModel extends BaseModel implements MVContract.SearchModel {

    @Inject
    public SearchModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public Observable<SearchHotWordResponse> loadHotWord(int size) {
        return Observable.just(mRepositoryManager.obtainRetrofitService(ApiService.class).getHotWord(size))
                .flatMap(baseResponseObservable -> mRepositoryManager.obtainCacheService(ApiCache.class)
                        .getHotWord(baseResponseObservable, new DynamicKey(1), new EvictDynamicKey(false)));
    }

}
