package com.music.player.haige.di.module;

import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.SplashModel;

import dagger.Module;
import dagger.Provides;

/**
 * ================================================
 * Created by huangcong on 2018/2/27.
 * ================================================
 */
@Module
public class SplashModule {
    private MVContract.SplashView view;

    public SplashModule(MVContract.SplashView view) {
        this.view = view;
    }

    @FragmentScope
    @Provides
    MVContract.SplashView provideView() {
        return this.view;
    }

    @FragmentScope
    @Provides
    MVContract.SplashModel provideModel(SplashModel model) {
        return model;
    }

}
