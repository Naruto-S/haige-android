package com.music.player.haige.mvp.ui.widget;

import android.view.View;

/**
 * Created by huangcong on 2018/2/10.
 * <p>
 * 防止快速点击
 */

public abstract class NoDoubleClickListener implements View.OnClickListener {

    private static final int MIN_DELAY_TIME = 400;  // 两次点击间隔

    private static long lastClickTime;

    /**
     * 判断是否是快速点击
     *
     * @return
     */
    public static boolean isFastClick() {
        boolean flag = true;
        long currentClickTime = System.currentTimeMillis();
        if ((currentClickTime - lastClickTime) >= MIN_DELAY_TIME) {
            flag = false;
        }
        lastClickTime = currentClickTime;
        return flag;
    }

    @Override
    public void onClick(View v) {
        if (isFastClick()) {
            return;
        }
        onNoDoubleClick(v);
    }

    public abstract void onNoDoubleClick(View view);
}
