package com.music.player.haige.app.oss.customprovider;

import com.alibaba.sdk.android.oss.ClientException;
import com.alibaba.sdk.android.oss.OSS;
import com.alibaba.sdk.android.oss.ServiceException;
import com.alibaba.sdk.android.oss.callback.OSSCompletedCallback;
import com.alibaba.sdk.android.oss.common.OSSLog;
import com.alibaba.sdk.android.oss.internal.OSSAsyncTask;
import com.alibaba.sdk.android.oss.model.OSSRequest;
import com.alibaba.sdk.android.oss.model.PutObjectRequest;
import com.alibaba.sdk.android.oss.model.PutObjectResult;
import com.music.player.haige.mvp.ui.utils.DebugLogger;
import com.music.player.haige.mvp.ui.utils.Utils;

import java.io.File;

/**
 * Created by mOss on 2015/12/7 0007.
 * 支持普通上传，普通下载
 */
public class OssService {

    public static final int UPLOAD_TYPE_IMAGE = 1;
    public static final int UPLOAD_TYPE_MUSIC = 2;

    public OSS mOss;
    private String mBucket;
    private UploadCallBack uploadCallBack;

    public OssService(OSS oss, String bucket, UploadCallBack uploadCallBack) {
        this.mOss = oss;
        this.mBucket = bucket;
        this.uploadCallBack = uploadCallBack;
    }

    public void asyncPutFile(String object, String localFile, int type) {
        final long upload_start = System.currentTimeMillis();

        if (object.equals("")) {
            DebugLogger.w("AsyncPutImage", "ObjectNull");
            return;
        }

        File file = new File(localFile);
        if (!file.exists()) {
            DebugLogger.w("AsyncPutImage", "FileNotExist");
            DebugLogger.w("LocalFile", localFile);
            return;
        }

        // 构造上传请求
        PutObjectRequest put = new PutObjectRequest(mBucket, object, localFile);
        put.setCRC64(OSSRequest.CRC64Config.YES);

        // 异步上传时可以设置进度回调
        put.setProgressCallback((request, currentSize, totalSize) -> {
            DebugLogger.d("PutObject", "currentSize: " + currentSize + " totalSize: " + totalSize);
            int progress = (int) (100 * currentSize / totalSize);
            if (Utils.isNotNull(uploadCallBack)) {
                uploadCallBack.onUploadProgress(type, progress);
            }
        });

        OSSAsyncTask task = mOss.asyncPutObject(put, new OSSCompletedCallback<PutObjectRequest, PutObjectResult>() {
            @Override
            public void onSuccess(PutObjectRequest request, PutObjectResult result) {
                DebugLogger.d("PutObject", "UploadSuccess");

                long upload_end = System.currentTimeMillis();
                OSSLog.logDebug("upload cost: " + (upload_end - upload_start) / 1000f);
                if (Utils.isNotNull(uploadCallBack)) {
                    uploadCallBack.onUploadComplete(type, object);
                }
            }

            @Override
            public void onFailure(PutObjectRequest request, ClientException clientExcepion, ServiceException serviceException) {
                String info = "";
                // 请求异常
                if (clientExcepion != null) {
                    // 本地异常如网络异常等
                    clientExcepion.printStackTrace();
                    info = clientExcepion.toString();
                }
                if (serviceException != null) {
                    // 服务异常
                    DebugLogger.e("ErrorCode", serviceException.getErrorCode());
                    DebugLogger.e("RequestId", serviceException.getRequestId());
                    DebugLogger.e("HostId", serviceException.getHostId());
                    DebugLogger.e("RawMessage", serviceException.getRawMessage());
                    info = serviceException.toString();
                }
                if (Utils.isNotNull(uploadCallBack)) {
                    uploadCallBack.onUploadFail(type, info);
                }
            }
        });
    }

    public interface UploadCallBack {
        void onUploadProgress(int type, int progress);
        void onUploadComplete(int type, String url);
        void onUploadFail(int type, String info);
    }
}
