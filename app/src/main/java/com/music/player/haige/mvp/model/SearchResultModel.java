package com.music.player.haige.mvp.model;

import com.hc.base.mvp.BaseModel;
import com.hc.core.integration.IRepositoryManager;
import com.music.player.haige.app.HaigeApplication;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.service.ApiService;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.model.entity.search.SearchKeyWordResponse;
import com.music.player.haige.mvp.model.respository.dataloader.SongLoader;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * ================================================
 * Created by huangcong on 2018/3/28.
 * ================================================
 */

public class SearchResultModel extends BaseModel implements MVContract.SearchResultModel {

    @Inject
    public SearchResultModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public Observable<SearchKeyWordResponse> searchKeyWord(int tagId, int pageNum, int pageSize, String searchWord) {
        return Observable.just(mRepositoryManager.obtainRetrofitService(ApiService.class).getSearchResult(tagId, pageNum, pageSize, searchWord))
                .flatMap(observable -> observable);
    }

    @Override
    public Observable<List<Song>> searchLocalKeyWord(String searchWord) {
        return SongLoader.searchSongs(HaigeApplication.getInstance(), searchWord);
    }

    @Override
    public Observable<List<Song>> searchRecentKeyWord(String searchWord) {
        return SongLoader.searchSongs(HaigeApplication.getInstance(), searchWord);
    }
}
