/**
 * Copyright (lrc_arrow) www.longdw.com
 */
package com.music.player.haige.mvp.model.entity.music;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.music.player.haige.R;
import com.music.player.haige.mvp.model.entity.user.UserBean;
import com.music.player.haige.mvp.model.utils.StringConverter;
import com.music.player.haige.mvp.model.utils.StringUtils;
import com.music.player.haige.mvp.model.utils.UserConverter;

import org.greenrobot.greendao.annotation.Convert;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

import java.util.ArrayList;
import java.util.Date;

@Entity
public class Song implements Parcelable {

    public static final String KEY_MUSIC_ID = "music_id";
    public static final String KEY_MUSIC_NAME = "music_name";
    public static final String KEY_MUSIC_COVER = "music_cover";
    public static final String KEY_REAL_NAME = "real_name";
    public static final String KEY_REAL_AUTHORS = "real_authors";
    public static final String KEY_MUSIC_DURATION = "music_duration";
    public static final String KEY_MUSIC_URL = "music_url";
    public static final String KEY_MUSIC_LRC = "lrc";

    public static final String KEY_ALBUM_ID = "album_id";
    public static final String KEY_ALBUM_NAME = "album_name";
    public static final String KEY_ALBUM_DATA = "album_data";

    public static final String KEY_ARTIST_NAME = "artist_name";
    public static final String KEY_ARTIST_ID = "artist_id";

    public static final String KEY_LIKE = "like";
    public static final String KEY_SHARE = "share";
    public static final String KEY_DOWNLOAD = "download";

    public static final String KEY_IS_LOCAL = "is_local";
    public static final String KEY_SORT = "sort";
    public static final String KEY_TRACK_NUMBER = "track_number";

    public static final String KEY_IS_PLAYING = "is_playing";

    public static final String KEY_DOWNLOAD_DATE = "download_date";

    public static final String KEY_TAG = "tag";

    public static final String KEY_USER_LIKED = "user_liked";

    public static final String KEY_USER = "user";

    public static final String KEY_BG_IMG_WEBP = "bg_img_webp";

    public static final String KEY_START_AT = "start_at";

    public static final String KEY_SHORT_URL = "short_url";

    public static final String KEY_IS_SHORT = "is_short";

    public static final String KEY_COMMENT_COUNT = "comment_count";

    public static final String KEY_CHECK_STATUS = "check_status";
    public static final String KEY_CHECK_STATUS_DISPLAY = "check_status_display";

    /**
     * 数据库中的_id
     */
    @Id
    @Property(nameInDb = KEY_MUSIC_ID)
    @SerializedName(KEY_MUSIC_ID)
    public long songId = -1;

    @Property(nameInDb = KEY_ALBUM_ID)
    @SerializedName(KEY_ALBUM_ID)
    public long albumId = -1;

    @Property(nameInDb = KEY_ALBUM_NAME)
    @SerializedName(KEY_ALBUM_NAME)
    public String albumName;

    public String albumData;

    @Property(nameInDb = KEY_MUSIC_DURATION)
    @SerializedName(KEY_MUSIC_DURATION)
    public int duration;

    @Property(nameInDb = KEY_MUSIC_NAME)
    @SerializedName(KEY_MUSIC_NAME)
    public String musicName;

    @Property(nameInDb = KEY_MUSIC_COVER)
    @SerializedName(KEY_MUSIC_COVER)
    public String musicCover;

    @Property(nameInDb = KEY_REAL_NAME)
    @SerializedName(KEY_REAL_NAME)
    public String realName;

    @Property(nameInDb = KEY_REAL_AUTHORS)
    @Convert(columnType = String.class, converter = StringConverter.class)
    @SerializedName(KEY_REAL_AUTHORS)
    public ArrayList<String> realAuthors;

    @Property(nameInDb = KEY_ARTIST_NAME)
    @SerializedName(KEY_ARTIST_NAME)
    public String artistName;

    @Property(nameInDb = KEY_ARTIST_ID)
    @SerializedName(KEY_ARTIST_ID)
    public long artistId;

    @Property(nameInDb = KEY_START_AT)
    @SerializedName(KEY_START_AT)
    public long startAt;

    @Property(nameInDb = KEY_IS_SHORT)
    @SerializedName(KEY_IS_SHORT)
    public int isShort;

    @Property(nameInDb = KEY_SHORT_URL)
    @SerializedName(KEY_SHORT_URL)
    public String shortUrl;

    @Property(nameInDb = KEY_MUSIC_URL)
    @SerializedName(KEY_MUSIC_URL)
    public String musicPath;

    @Property(nameInDb = KEY_MUSIC_LRC)
    @SerializedName(KEY_MUSIC_LRC)
    public String lrc;

    @Property(nameInDb = KEY_BG_IMG_WEBP)
    @SerializedName(KEY_BG_IMG_WEBP)
    public String bgImgWebp;

    @Property(nameInDb = KEY_LIKE)
    @SerializedName(KEY_LIKE)
    public int likeCount;

    @Property(nameInDb = KEY_SHARE)
    @SerializedName(KEY_SHARE)
    public int shareCount;

    @Property(nameInDb = KEY_DOWNLOAD)
    @SerializedName(KEY_DOWNLOAD)
    public int downloadCount;

    @Property(nameInDb = KEY_USER_LIKED)
    @SerializedName(KEY_USER_LIKED)
    public boolean isUserLiked;

    @Property(nameInDb = KEY_DOWNLOAD_DATE)
    public Date downloadDate;

    @Property(nameInDb = KEY_TAG)
    @Convert(columnType = String.class, converter = StringConverter.class)
    @SerializedName(KEY_TAG)
    public ArrayList<String> tags;

    @Property(nameInDb = KEY_USER)
    @Convert(columnType = String.class, converter = UserConverter.class)
    @SerializedName(KEY_USER)
    public UserBean user;

    @Property(nameInDb = KEY_COMMENT_COUNT)
    @SerializedName(KEY_COMMENT_COUNT)
    private int commentCount;

    @Property(nameInDb = KEY_CHECK_STATUS)
    @SerializedName(KEY_CHECK_STATUS)
    private int uploadStatus;

    @Property(nameInDb = KEY_CHECK_STATUS_DISPLAY)
    @SerializedName(KEY_CHECK_STATUS_DISPLAY)
    private String uploadStatusDisplay;

    public int downloadStatus;      // 下载状态
    public boolean isLocal = false; // 默认为非本地音乐
    public String sort;
    public int trackNumber;

    /**
     * 0表示没有收藏 1表示收藏
     */
    public int favorite = 0;

    public float playCountScore;

    public Song() {

    }

    public Song(long songId,
                long albumId,
                long artistId,
                String musicName,
                String artistName,
                String albumName,
                int duration,
                int trackNumber,
                boolean isLocal) {
        this.songId = songId;
        this.albumId = albumId;
        this.artistId = artistId;
        this.musicName = musicName;
        this.artistName = artistName;
        this.albumName = albumName;
        this.duration = duration;
        this.trackNumber = trackNumber;
        this.musicPath = "";
        this.isLocal = isLocal;
    }

    public Song(long songId,
                long albumId,
                long artistId,
                String musicName,
                String artistName,
                String albumName,
                int duration,
                int trackNumber,
                String path,
                boolean isLocal) {
        this.songId = songId;
        this.albumId = albumId;
        this.artistId = artistId;
        this.musicName = musicName;
        this.artistName = artistName;
        this.albumName = albumName;
        this.duration = duration;
        this.trackNumber = trackNumber;
        this.musicPath = path;
        this.isLocal = isLocal;
    }

    protected Song(Parcel in) {
        songId = in.readLong();
        albumId = in.readLong();
        albumName = in.readString();
        albumData = in.readString();
        duration = in.readInt();
        musicName = in.readString();
        musicCover = in.readString();
        realName = in.readString();
        realAuthors = in.createStringArrayList();
        artistName = in.readString();
        artistId = in.readLong();
        musicPath = in.readString();
        lrc = in.readString();
        bgImgWebp = in.readString();
        likeCount = in.readInt();
        shareCount = in.readInt();
        downloadCount = in.readInt();
        isUserLiked = in.readByte() != 0;
        tags = in.createStringArrayList();
        user = in.readParcelable(UserBean.class.getClassLoader());
        downloadStatus = in.readInt();
        isLocal = in.readByte() != 0;
        sort = in.readString();
        trackNumber = in.readInt();
        favorite = in.readInt();
        playCountScore = in.readFloat();
        startAt = in.readLong();
        isShort = in.readInt();
        shortUrl = in.readString();
        commentCount = in.readInt();
        uploadStatus = in.readInt();
        uploadStatusDisplay = in.readString();
    }

    @Generated(hash = 439297474)
    public Song(long songId, long albumId, String albumName, String albumData, int duration, String musicName, String musicCover,
            String realName, ArrayList<String> realAuthors, String artistName, long artistId, long startAt, int isShort,
            String shortUrl, String musicPath, String lrc, String bgImgWebp, int likeCount, int shareCount, int downloadCount,
            boolean isUserLiked, Date downloadDate, ArrayList<String> tags, UserBean user, int commentCount, int uploadStatus,
            String uploadStatusDisplay, int downloadStatus, boolean isLocal, String sort, int trackNumber, int favorite,
            float playCountScore) {
        this.songId = songId;
        this.albumId = albumId;
        this.albumName = albumName;
        this.albumData = albumData;
        this.duration = duration;
        this.musicName = musicName;
        this.musicCover = musicCover;
        this.realName = realName;
        this.realAuthors = realAuthors;
        this.artistName = artistName;
        this.artistId = artistId;
        this.startAt = startAt;
        this.isShort = isShort;
        this.shortUrl = shortUrl;
        this.musicPath = musicPath;
        this.lrc = lrc;
        this.bgImgWebp = bgImgWebp;
        this.likeCount = likeCount;
        this.shareCount = shareCount;
        this.downloadCount = downloadCount;
        this.isUserLiked = isUserLiked;
        this.downloadDate = downloadDate;
        this.tags = tags;
        this.user = user;
        this.commentCount = commentCount;
        this.uploadStatus = uploadStatus;
        this.uploadStatusDisplay = uploadStatusDisplay;
        this.downloadStatus = downloadStatus;
        this.isLocal = isLocal;
        this.sort = sort;
        this.trackNumber = trackNumber;
        this.favorite = favorite;
        this.playCountScore = playCountScore;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(songId);
        dest.writeLong(albumId);
        dest.writeString(albumName);
        dest.writeString(albumData);
        dest.writeInt(duration);
        dest.writeString(musicName);
        dest.writeString(musicCover);
        dest.writeString(realName);
        dest.writeStringList(realAuthors);
        dest.writeString(artistName);
        dest.writeLong(artistId);
        dest.writeString(musicPath);
        dest.writeString(lrc);
        dest.writeString(bgImgWebp);
        dest.writeInt(likeCount);
        dest.writeInt(shareCount);
        dest.writeInt(downloadCount);
        dest.writeByte((byte) (isUserLiked ? 1 : 0));
        dest.writeStringList(tags);
        dest.writeParcelable(user, flags);
        dest.writeInt(downloadStatus);
        dest.writeByte((byte) (isLocal ? 1 : 0));
        dest.writeString(sort);
        dest.writeInt(trackNumber);
        dest.writeInt(favorite);
        dest.writeFloat(playCountScore);
        dest.writeLong(startAt);
        dest.writeInt(isShort);
        dest.writeString(shortUrl);
        dest.writeInt(commentCount);
        dest.writeInt(uploadStatus);
        dest.writeString(uploadStatusDisplay);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Song> CREATOR = new Creator<Song>() {
        @Override
        public Song createFromParcel(Parcel in) {
            return new Song(in);
        }

        @Override
        public Song[] newArray(int size) {
            return new Song[size];
        }
    };

    public int getFavorite() {
        return favorite;
    }

    public void setFavorite(int favorite) {
        this.favorite = favorite;
    }

    public boolean isFavorite() {
        return this.favorite == 1;
    }

    public void setPlayCountScore(float playCountScore) {
        this.playCountScore = playCountScore;
    }

    public float getPlayCountScore() {
        return playCountScore;
    }

    public long getSongId() {
        return this.songId;
    }

    public void setSongId(long songId) {
        this.songId = songId;
    }

    public long getAlbumId() {
        return this.albumId;
    }

    public void setAlbumId(long albumId) {
        this.albumId = albumId;
    }

    public String getAlbumName() {
        return this.albumName;
    }

    public void setAlbumName(String albumName) {
        this.albumName = albumName;
    }

    public String getAlbumData() {
        return this.albumData;
    }

    public void setAlbumData(String albumData) {
        this.albumData = albumData;
    }

    public int getDuration() {
        return this.duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getMusicName() {
        return this.musicName;
    }

    public void setMusicName(String musicName) {
        this.musicName = musicName;
    }

    public String getMusicCover() {
        return this.musicCover;
    }

    public void setMusicCover(String musicCover) {
        this.musicCover = musicCover;
    }

    public String getArtistName() {
        return this.artistName;
    }

    public String getArtistName(Context context) {
        return StringUtils.isEmpty(this.artistName) ? context.getResources().getString(R.string.app_haige_provide) : this.artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public long getArtistId() {
        return this.artistId;
    }

    public void setArtistId(long artistId) {
        this.artistId = artistId;
    }

    public String getMusicPath() {
        return this.musicPath;
    }

    public void setMusicPath(String musicPath) {
        this.musicPath = musicPath;
    }

    public String getLrc() {
        return this.lrc;
    }

    public void setLrc(String lrc) {
        this.lrc = lrc;
    }

    public int getLikeCount() {
        return this.likeCount;
    }

    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
    }

    public int getShareCount() {
        return this.shareCount;
    }

    public void setShareCount(int shareCount) {
        this.shareCount = shareCount;
    }

    public int getDownloadCount() {
        return this.downloadCount;
    }

    public void setDownloadCount(int downloadCount) {
        this.downloadCount = downloadCount;
    }

    public Date getDownloadDate() {
        return downloadDate;
    }

    public void setDownloadDate(Date downloadDate) {
        this.downloadDate = downloadDate;
    }

    public boolean getIsLocal() {
        return this.isLocal;
    }

    public void setIsLocal(boolean isLocal) {
        this.isLocal = isLocal;
    }

    public String getSort() {
        return this.sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public int getTrackNumber() {
        return this.trackNumber;
    }

    public void setTrackNumber(int trackNumber) {
        this.trackNumber = trackNumber;
    }

    public int getDownloadStatus() {
        return this.downloadStatus;
    }

    public void setDownloadStatus(int downloadStatus) {
        this.downloadStatus = downloadStatus;
    }

    public ArrayList<String> getTags() {
        return tags;
    }

    public void setTags(ArrayList<String> tags) {
        this.tags = tags;
    }

    public boolean getIsUserLiked() {
        return this.isUserLiked;
    }

    public void setIsUserLiked(boolean isUserLiked) {
        this.isUserLiked = isUserLiked;
    }

    public UserBean getUser() {
        return this.user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public String getBgImgWebp() {
        return this.bgImgWebp;
    }

    public void setBgImgWebp(String bgImgWebp) {
        this.bgImgWebp = bgImgWebp;
    }

    public String getRealName() {
        return this.realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public ArrayList<String> getRealAuthors() {
        return realAuthors;
    }

    public void setRealAuthors(ArrayList<String> realAuthors) {
        this.realAuthors = realAuthors;
    }

    public long getStartAt() {
        return this.startAt;
    }

    public void setStartAt(long startAt) {
        this.startAt = startAt;
    }

    public int getIsShort() {
        return this.isShort;
    }

    public void setIsShort(int isShort) {
        this.isShort = isShort;
    }

    public String getShortUrl() {
        return this.shortUrl;
    }

    public void setShortUrl(String shortUrl) {
        this.shortUrl = shortUrl;
    }

    public int getUploadStatus() {
        return this.uploadStatus;
    }

    public void setUploadStatus(int uploadStatus) {
        this.uploadStatus = uploadStatus;
    }

    public String getUploadStatusDisplay() {
        return this.uploadStatusDisplay;
    }

    public void setUploadStatusDisplay(String uploadStatusDisplay) {
        this.uploadStatusDisplay = uploadStatusDisplay;
    }

    public int getCommentCount() {
        return this.commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    @Override
    public String toString() {
        return "Song{" +
                "songId=" + songId +
                ", albumId=" + albumId +
                ", albumName='" + albumName + '\'' +
                ", albumData='" + albumData + '\'' +
                ", duration=" + duration +
                ", musicName='" + musicName + '\'' +
                ", musicCover='" + musicCover + '\'' +
                ", realName='" + realName + '\'' +
                ", realAuthors=" + realAuthors +
                ", artistName='" + artistName + '\'' +
                ", artistId=" + artistId +
                ", startAt=" + startAt +
                ", isShort=" + isShort +
                ", shortUrl='" + shortUrl + '\'' +
                ", musicPath='" + musicPath + '\'' +
                ", lrc='" + lrc + '\'' +
                ", bgImgWebp='" + bgImgWebp + '\'' +
                ", likeCount=" + likeCount +
                ", shareCount=" + shareCount +
                ", downloadCount=" + downloadCount +
                ", isUserLiked=" + isUserLiked +
                ", downloadDate=" + downloadDate +
                ", tags=" + tags +
                ", user=" + user +
                ", commentCount=" + commentCount +
                ", uploadStatus=" + uploadStatus +
                ", uploadStatusDisplay='" + uploadStatusDisplay + '\'' +
                ", downloadStatus=" + downloadStatus +
                ", isLocal=" + isLocal +
                ", sort='" + sort + '\'' +
                ", trackNumber=" + trackNumber +
                ", favorite=" + favorite +
                ", playCountScore=" + playCountScore +
                '}';
    }
}