package com.music.player.haige.mvp.model;

import com.hc.base.mvp.BaseModel;
import com.hc.core.integration.IRepositoryManager;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.service.ApiService;
import com.music.player.haige.mvp.model.entity.BaseResponse;
import com.music.player.haige.mvp.model.entity.action.FollowActionRequest;
import com.music.player.haige.mvp.model.entity.user.FollowUserResponse;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by Naruto on 2018/5/13.
 */

public class FansListModel extends BaseModel implements MVContract.FansListModel {

    @Inject
    public FansListModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public Observable<FollowUserResponse> getFollowing(String userId, int pageNo, int pageSize) {
        return Observable.just(mRepositoryManager.obtainRetrofitService(ApiService.class).getFollowing(userId, pageNo, pageSize))
                .flatMap(observable -> observable);
    }

    @Override
    public Observable<FollowUserResponse> getFollower(String userId, int pageNo, int pageSize) {
        return Observable.just(mRepositoryManager.obtainRetrofitService(ApiService.class).getFollower(userId, pageNo, pageSize))
                .flatMap(observable -> observable);
    }

    @Override
    public Observable<BaseResponse> follow(FollowActionRequest request) {
        return Observable.just(mRepositoryManager.obtainRetrofitService(ApiService.class).follow(request))
                .flatMap(observable -> observable);
    }

    @Override
    public Observable<BaseResponse> unfollow(FollowActionRequest request) {
        return Observable.just(mRepositoryManager.obtainRetrofitService(ApiService.class).unfollow(request))
                .flatMap(observable -> observable);
    }
}
