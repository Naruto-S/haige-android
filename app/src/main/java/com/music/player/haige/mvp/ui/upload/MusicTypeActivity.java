package com.music.player.haige.mvp.ui.upload;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.hc.core.di.component.AppComponent;
import com.hc.core.utils.CoreUtils;
import com.music.player.haige.R;
import com.music.player.haige.app.SystemUIConfig;
import com.music.player.haige.app.base.AppBaseActivity;
import com.music.player.haige.app.constants.IntentConstants;
import com.music.player.haige.di.component.DaggerRankingComponent;
import com.music.player.haige.di.module.RankingModule;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.Api;
import com.music.player.haige.mvp.model.entity.rank.NewRankTagsResponse;
import com.music.player.haige.mvp.model.entity.rank.RankCategoryBean;
import com.music.player.haige.mvp.model.entity.rank.RankTagsBean;
import com.music.player.haige.mvp.presenter.RankingFPresenter;
import com.music.player.haige.mvp.ui.upload.adapter.MusicTypeAdapter;
import com.music.player.haige.mvp.ui.utils.DebugLogger;
import com.music.player.haige.mvp.ui.utils.Utils;
import com.music.player.haige.mvp.ui.widget.StatusLayout;

import org.simple.eventbus.EventBus;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

public class MusicTypeActivity extends AppBaseActivity<RankingFPresenter> implements MVContract.RankingView,
        SystemUIConfig {

    @BindView(R.id.app_layout_sl)
    StatusLayout mStatusLayout;

    @BindView(R.id.app_layout_rv)
    RecyclerView mSonsRecyclerView;

    @BindView(R.id.app_layout_progress)
    ProgressBar mLoadingView;

    private MusicTypeAdapter mAdapter;
    private ArrayList<String> mMusicTypes;

    @Override
    public void setupActivityComponent(AppComponent appComponent) {
        DaggerRankingComponent
                .builder()
                .appComponent(appComponent)
                .rankingModule(new RankingModule(this))
                .build()
                .inject(this);

    }

    @Override
    public int initView(Bundle savedInstanceState) {
        return R.layout.activity_music_type;
    }

    @Override
    public void initData(Bundle savedInstanceState) {
        if (Utils.isNotNull(getIntent())) {
            mMusicTypes = getIntent().getStringArrayListExtra(IntentConstants.EXTRA_MUSIC_TYPE_LIST);
        }

        mAdapter = new MusicTypeAdapter();
        mAdapter.setOnItemClickListener((view, postion) -> {
            mAdapter.setSelect(postion);
        });
        mSonsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mSonsRecyclerView.setAdapter(mAdapter);

        mPresenter.getRankingTags();
    }

    @Override
    public boolean translucentStatusBar() {
        return false;
    }

    @Override
    public int setStatusBarColor() {
        return getResources().getColor(R.color.color_00000000);
    }

    @Override
    public boolean lightStatusBar() {
        return false;
    }

    @Override
    public boolean fitSystemWindows() {
        return true;
    }

    @Override
    public boolean fullScreen() {
        return false;
    }

    @Override
    public void showLoading() {
        super.showLoading();
        if (Utils.isNotNull(mLoadingView)) {
            mLoadingView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
        if (Utils.isNotNull(mLoadingView)) {
            mLoadingView.setVisibility(View.GONE);
        }
    }


    @Override
    public void onSuccess(Api.Action action, Object o) {
        super.onSuccess(action, o);
        if (action == Api.Action.MUSIC_RANK_TAG) {
            try {
                mStatusLayout.showStatusView(StatusLayout.STATUS.OK);
                mSonsRecyclerView.setVisibility(View.VISIBLE);

                RankTagsBean rankTags =  ((NewRankTagsResponse) o).getData();
                ArrayList<RankCategoryBean> datas = rankTags.getCategory();
                if (Utils.isNotEmptyCollection(mMusicTypes)) {
                    for (int i = 0; i < datas.size(); i++) {
                        RankCategoryBean category = datas.get(i);
                        for (String type: mMusicTypes) {
                            if (category.getTitle().equals(type)) {
                                category.setSelect(true);
                                datas.set(i, category);
                            }
                        }
                    }
                }
                mAdapter.setData(datas);
            } catch (Exception e) {
                DebugLogger.e(e);
            }
        }
    }

    @Override
    public void onError(Api.Action action, String code, String message) {
        super.onError(action, code, message);
        if (action == Api.Action.MUSIC_RANK_TAG) {
            mStatusLayout.showStatusView(StatusLayout.STATUS.EMPTY);
            mSonsRecyclerView.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void showMessage(String message) {
        super.showMessage(message);
        CoreUtils.makeText(this, message);
    }

    @OnClick({R.id.app_fragment_back_iv})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.app_fragment_back_iv:
                finish();
                break;
        }
    }

    @Override
    public void finish() {
        postSelectType();
        super.finish();
    }

    private void postSelectType() {
        if (Utils.isNotNull(mAdapter)) {
            ArrayList<String> types = new ArrayList<>();
            for (RankCategoryBean category : mAdapter.getData()) {
                if (category.isSelect()) {
                    types.add(category.getTitle());
                }
            }
            EventBus.getDefault().post(types);
        }
    }
}
