package com.music.player.haige.mvp.ui.setting;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.Toast;

import com.music.player.haige.BuildConfig;
import com.music.player.haige.R;
import com.music.player.haige.app.EventBusTags;
import com.music.player.haige.app.base.BaseDialogFragment;
import com.music.player.haige.app.base.pref.DevicePrefHelper;
import com.music.player.haige.app.constants.TimeConstants;

import org.simple.eventbus.EventBus;

import butterknife.OnClick;

/**
 * Created by john on 2018/1/17.
 */

public class RateDialog extends BaseDialogFragment {

    private boolean isClickFeedback = false;
    private boolean isClickRate = false;
    private View.OnClickListener mListener;

    public static RateDialog create(FragmentManager manager) {
        RateDialog dialog = new RateDialog();
        dialog.setFragmentManger(manager);
        return dialog;
    }

    public RateDialog setListener(View.OnClickListener listener) {
        this.mListener = listener;
        return this;
    }

    public RateDialog show() {
        show(mFragmentManager);
        return this;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        if (!isClickFeedback && !isClickRate) {
            DevicePrefHelper.putNextShowRateTime(System.currentTimeMillis() + TimeConstants.DAY7 * 2);
            DevicePrefHelper.putShowRateDialog(true);
        }
        super.onDismiss(dialog);
    }

    @Override
    public int setRootView() {
        return R.layout.dialog_rate;
    }

    @OnClick({R.id.tv_rate_unlike})
    public void onClickFeedback(View view) {
        isClickFeedback = true;

        EventBus.getDefault().post(EventBusTags.MAIN.SETTING_FEEDBACK);

        DevicePrefHelper.putNextShowRateTime(System.currentTimeMillis() + TimeConstants.DAY7 * 2);
        DevicePrefHelper.putShowRateDialog(true);

        if (mListener != null) {
            mListener.onClick(view);
        }
        dismiss();
    }

    @OnClick({R.id.tv_rate_like})
    public void onClickRate(View view) {
        isClickRate = true;

        try {
            Uri uri = Uri.parse("market://details?id=" + BuildConfig.APPLICATION_ID);
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } catch (Exception e) {
            Toast.makeText(getContext(), R.string.update_not_installed_appstore, Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

        DevicePrefHelper.putShowRateDialog(false);

        if (mListener != null) {
            mListener.onClick(view);
        }
        dismiss();
    }

}
