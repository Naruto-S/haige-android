package com.music.player.haige.di.component;

import com.hc.core.di.component.AppComponent;
import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.di.module.RankModule;
import com.music.player.haige.mvp.ui.main.activity.TagMusicActivity;
import com.music.player.haige.mvp.ui.main.fragment.RankFragment;

import dagger.Component;

/**
 * ================================================
 * Created by huangcong on 2018/3/3.
 * ================================================
 */
@FragmentScope
@Component(modules = {RankModule.class}, dependencies = AppComponent.class)
public interface RankComponent {

    void inject(RankFragment fragment);

    void inject(TagMusicActivity activity);
}
