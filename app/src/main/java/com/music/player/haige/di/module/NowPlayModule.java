package com.music.player.haige.di.module;

import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.NowPlayModel;

import dagger.Module;
import dagger.Provides;

/**
 * ================================================
 * Created by huangcong on 2018/2/27.
 * ================================================
 */
@Module
public class NowPlayModule {
    private MVContract.NowPlayView view;

    public NowPlayModule(MVContract.NowPlayView view) {
        this.view = view;
    }

    @FragmentScope
    @Provides
    MVContract.NowPlayView provideView() {
        return this.view;
    }

    @FragmentScope
    @Provides
    MVContract.NowPlayModel provideModel(NowPlayModel model) {
        return model;
    }

}
