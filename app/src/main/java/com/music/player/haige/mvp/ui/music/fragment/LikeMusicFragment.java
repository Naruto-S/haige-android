package com.music.player.haige.mvp.ui.music.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;

import com.hc.core.di.component.AppComponent;
import com.music.player.haige.R;
import com.music.player.haige.app.EventBusTags;
import com.music.player.haige.app.base.BaseListFragment;
import com.music.player.haige.app.base.pref.UserPrefHelper;
import com.music.player.haige.app.base.recyclerview.BaseQuickAdapter;
import com.music.player.haige.app.base.recyclerview.listener.OnItemClickListener;
import com.music.player.haige.app.base.recyclerview.pullrefresh.HaigeRefreshHeader;
import com.music.player.haige.app.constants.IntentConstants;
import com.music.player.haige.di.component.DaggerLikeMusicComponent;
import com.music.player.haige.di.module.LikeMusicModule;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.model.utils.NavigationUtil;
import com.music.player.haige.mvp.presenter.LikeMusicPresenter;
import com.music.player.haige.mvp.ui.login.LoginDialogFragment;
import com.music.player.haige.mvp.ui.music.adapter.MusicListAdapter;
import com.music.player.haige.mvp.ui.music.service.MusicServiceConnection;
import com.music.player.haige.mvp.ui.share.DownloadShareDialog;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;
import com.music.player.haige.mvp.ui.utils.ShareUtils;
import com.music.player.haige.mvp.ui.utils.Utils;

import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;

public class LikeMusicFragment extends BaseListFragment<LikeMusicPresenter> implements MVContract.LikeMusicView {

    private String mUserId;

    public static LikeMusicFragment newInstance(String userId) {
        Bundle args = new Bundle();
        args.putString(IntentConstants.EXTRA_OTHER_USER, userId);
        LikeMusicFragment fragment = new LikeMusicFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected RecyclerView.LayoutManager createLayoutManager() {
        return new LinearLayoutManager(getActivity());
    }

    @Override
    protected OnItemClickListener createItemClickListener() {
        return new OnItemClickListener(false) {
            @Override
            public void SimpleOnItemClick(BaseQuickAdapter adapter, View view, int position) {
                if (Utils.isFastClick()) {
                    return;
                }

                MusicServiceConnection.playAll(adapter.getData(), position, false);
                EventBus.getDefault().post(EventBusTags.MAIN.MUSIC_PLAY);
            }

            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                if (Utils.isFastClick()) {
                    return;
                }

                switch (view.getId()) {
                    case R.id.popup_menu:
                        if (UserPrefHelper.isLogin()) {
                            onPopupMenuClick(view, (Song) adapter.getItem(position));
                        } else {
                            LoginDialogFragment.newInstance().show(getChildFragmentManager(), LoginDialogFragment.class.getName());
                        }
                        break;
                    default:
                        break;
                }
            }
        };
    }

    @Override
    protected RecyclerView.OnScrollListener addRecyclerViewScrollListener() {
        return null;
    }

    @Override
    protected RecyclerView.ItemDecoration createDecoration() {
        return null;
    }

    @Override
    protected RecyclerView.ItemAnimator createItemAnimator() {
        return null;
    }

    @Override
    protected View createRefreshHeader() {
        return new HaigeRefreshHeader(getActivity());
    }

    @Override
    protected Context createContext() {
        return getActivity();
    }

    @Override
    protected BaseQuickAdapter createAdapter() {
        return new MusicListAdapter(getActivity(), false);
    }

    @Override
    public void setupFragmentComponent(AppComponent appComponent) {
        DaggerLikeMusicComponent
                .builder()
                .appComponent(appComponent)
                .likeMusicModule(new LikeMusicModule(this))
                .build()
                .inject(this);
    }

    @Override
    public View initView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_music_list, container, false);
    }

    @Override
    public void initData(Bundle bundle) {
        super.initData(bundle);

        mUserId = getArguments().getString(IntentConstants.EXTRA_OTHER_USER);
    }

    @Override
    protected void sendListReq(int pageNum, int pageSize, boolean refresh) {
        mPresenter.getLikedList(mUserId, pageNum, pageSize, refresh);
    }

    @Override
    protected void reLoadData() {
        fetchPusherList(mAdapter, true);
    }

    @Override
    protected void loadMoreData() {
        fetchPusherList(mAdapter, false);
    }

    @Subscriber
    public void onSubscriber(EventBusTags.MetaChangedEvent event) {
        if (getAdapter() != null) {
            getAdapter().notifyDataSetChanged();
        }
    }

    public void onPopupMenuClick(View v, Song song) {
        final PopupMenu menu = new PopupMenu(getActivity(), v);
        menu.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.popup_song_play_next:
                    MusicServiceConnection.playNext(song);
                    showMessage(ResourceUtils.resourceString(R.string.app_next_play));
                    break;
                case R.id.popup_song_goto_album:
                    NavigationUtil.goToAlbum(getActivity(), song.albumId, song.musicName);
                    break;
                case R.id.popup_song_goto_artist:
                    NavigationUtil.goToArtist(getActivity(), song.artistId, song.artistName);
                    break;
                case R.id.popup_song_addto_queue:
                    MusicServiceConnection.addToQueue(song);
                    break;
                case R.id.popup_download:
                    if (ShareUtils.checkDownloadShare(UserPrefHelper.getDownloadMusicCount())) {
                        DownloadShareDialog.newInstance().show(getFragmentManager(), DownloadShareDialog.class.getName());
                    } else {
                        mPresenter.downloadMusic(song);
                    }
                    break;
            }
            return false;
        });
        menu.inflate(R.menu.popup_other_music_list);
        menu.show();
    }
}
