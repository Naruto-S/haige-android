package com.music.player.haige.mvp.model.entity.update;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UpdateBean implements Serializable {

    @SerializedName("content")
    private String content; //更新内容

    @SerializedName("force_version")
    private int forceVersion;//强制更新最高版本号

    @SerializedName("version")
    private int latestVersion;//最新版本版本号

    @SerializedName("path")
    private String path;//缺省时按照产品包名跳转

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getForceVersion() {
        return forceVersion;
    }

    public void setForceVersion(int forceVersion) {
        this.forceVersion = forceVersion;
    }

    public int getLatestVersion() {
        return latestVersion;
    }

    public void setLatestVersion(int latestVersion) {
        this.latestVersion = latestVersion;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return "UpdateBean{" +
                "content='" + content + '\'' +
                ", forceVersion='" + forceVersion + '\'' +
                ", latestVersion='" + latestVersion + '\'' +
                ", path='" + path + '\'' +
                '}';
    }
}

