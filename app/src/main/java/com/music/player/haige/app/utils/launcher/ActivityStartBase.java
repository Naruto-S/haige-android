package com.music.player.haige.app.utils.launcher;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;

import com.music.player.haige.mvp.ui.utils.Utils;

/**
 * Created by liumingkong on 2016/11/15.
 */

public class ActivityStartBase {

    protected static void startActivity(Activity activity, Class<?> cls, IntentWrapper intentWrapper) {
        startActivity(activity, cls, intentWrapper, 0);
    }

    public static void startActivity(Activity activity, Class<?> cls) {
        startActivity(activity, cls, null, 0);
    }

    public static void startActivity(Activity activity, Class<?> cls, int requestCode) {
        startActivity(activity, cls, null, requestCode);
    }

    protected static void startActivity(Activity activity, Class<?> cls, IntentWrapper intentWrapper, int requestCode) {
        try {
            Intent intent = new Intent(activity, cls);
            if (Utils.ensureNotNull(intentWrapper)) {
                intentWrapper.setIntent(intent);
            }
            if (requestCode == 0) {
                activity.startActivity(intent);
            } else {
                activity.startActivityForResult(intent, requestCode);
            }
        } catch (Throwable e) {
        }
    }

    public static Activity scanForActivity(Context cont) {
        if (cont == null)
            return null;
        else if (cont instanceof Activity)
            return (Activity) cont;
        else if (cont instanceof ContextWrapper)
            return scanForActivity(((ContextWrapper) cont).getBaseContext());

        return null;
    }

    public interface IntentWrapper {
        void setIntent(Intent intent);
    }
}
