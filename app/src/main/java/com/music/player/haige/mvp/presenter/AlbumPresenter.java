package com.music.player.haige.mvp.presenter;

import com.hc.base.mvp.BasePresenter;
import com.hc.core.utils.RxLifecycleUtils;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.entity.music.Album;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;
import me.jessyan.rxerrorhandler.handler.ErrorHandleSubscriber;

/**
 * ================================================
 * Created by huangcong on 2018/3/2.
 * ================================================
 */

public class AlbumPresenter extends BasePresenter<MVContract.AlbumModel, MVContract.AlbumView> {

    @Inject
    RxErrorHandler mErrorHandler;

    @Inject
    public AlbumPresenter(MVContract.AlbumModel model, MVContract.AlbumView view) {
        super(model, view);
    }

    public void loadAlbums(String action) {
        mModel.loadAlbum(action)
                .getAlbumList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(Disposable disposable) throws Exception {
                        mRootView.showLoading();
                    }
                })
                .doFinally(new Action() {
                    @Override
                    public void run() throws Exception {
                        mRootView.hideLoading();
                    }
                })
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .subscribe(new ErrorHandleSubscriber<List<Album>>(mErrorHandler) {
                    @Override
                    public void onNext(List<Album> albums) {
                        if (albums == null || albums.size() == 0) {
                            mRootView.showEmptyView();
                        } else {
                            mRootView.showAlbum(albums);
                        }
                    }
                });
    }
}
