package com.music.player.haige.di.module;

import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.PlayQueueModel;

import dagger.Module;
import dagger.Provides;

/**
 * ================================================
 * Created by huangcong on 2018/3/3.
 * ================================================
 */
@Module
public class PlayQueueModule {
    private MVContract.PlayQueueView view;

    public PlayQueueModule(MVContract.PlayQueueView view){
        this.view = view;
    }

    @FragmentScope
    @Provides
    MVContract.PlayQueueView provideView(){
        return view;
    }

    @FragmentScope
    @Provides
    MVContract.PlayQueueModel provideModel(PlayQueueModel model){
        return model;
    }
}
