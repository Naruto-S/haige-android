package com.music.player.haige.mvp.ui.setting.fragment;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hc.core.di.component.AppComponent;
import com.music.player.haige.R;
import com.music.player.haige.app.Constants;
import com.music.player.haige.app.EventBusTags;
import com.music.player.haige.app.base.AppBaseFragment;
import com.music.player.haige.di.component.DaggerDownloadingComponent;
import com.music.player.haige.di.module.DownloadingModule;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.Api;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.presenter.DownloadingFPresenter;
import com.music.player.haige.mvp.ui.setting.adapter.DownloadingListAdapter;
import com.music.player.haige.mvp.ui.widget.DividerItemDecoration;
import com.music.player.haige.mvp.ui.widget.StatusLayout;

import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;

import java.util.List;

import butterknife.BindView;

/**
 * ================================================
 * Created by huangcong on 2018/3/20.
 * ================================================
 */

public class DownloadingFragment extends AppBaseFragment<DownloadingFPresenter> implements MVContract.DownloadingView {


    @BindView(R.id.app_layout_sl)
    StatusLayout mStatusLayout;

    @BindView(R.id.app_fragment_download_finish_rv)
    RecyclerView mDownloadFinishRv;

    private DownloadingListAdapter mAdapter;

    public static DownloadingFragment newInstance() {
        DownloadingFragment fragment = new DownloadingFragment();
        return fragment;
    }

    @Override
    public void setupFragmentComponent(AppComponent appComponent) {
        DaggerDownloadingComponent.builder()
                .appComponent(appComponent)
                .downloadingModule(new DownloadingModule(this))
                .build()
                .inject(this);
    }

    @Override
    public View initView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.app_fragment_download_ing, container, false);
    }

    @Override
    public void initData(Bundle savedInstanceState) {
        mStatusLayout.setOnStatusClickListener(new StatusLayout.OnStatusClickListener() {
            @Override
            public void onEmptyClick() {
                EventBus.getDefault().post(EventBusTags.UI.GO_TO_RECOMMEND);
                EventBus.getDefault().post(EventBusTags.MAIN.BACK);
            }

            @Override
            public void onReloadClick() {

            }
        });

        mAdapter = new DownloadingListAdapter((AppCompatActivity) getActivity(), null, Constants.NAVIGATE_ALLSONG, true, mPresenter);
        mDownloadFinishRv.setLayoutManager(new LinearLayoutManager(getActivity()));
        mDownloadFinishRv.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST, true));
        mDownloadFinishRv.setHasFixedSize(true);
        mDownloadFinishRv.setAdapter(mAdapter);

        mPresenter.loadDownloadingMusic();
    }

    @Override
    public void onSuccess(Api.Action action, Object o) {
        super.onSuccess(action, o);
        mStatusLayout.showStatusView(StatusLayout.STATUS.OK);
        mDownloadFinishRv.setVisibility(View.VISIBLE);
        mAdapter.setSongList((List<Song>) o);
    }

    @Override
    public void onError(Api.Action action, String code, String message) {
        super.onError(action, code, message);
        mStatusLayout.showStatusView(StatusLayout.STATUS.EMPTY);
        mDownloadFinishRv.setVisibility(View.INVISIBLE);
    }

    @Subscriber
    public void onSubscriber(EventBusTags.MetaChangedEvent event) {
        mAdapter.notifyDataSetChanged();
    }
}
