package com.music.player.haige.mvp.ui.music.service;

import android.media.AudioManager;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import java.lang.ref.WeakReference;

import timber.log.Timber;

/**
 * ================================================
 * Created by huangcong on 2018/3/13.
 * ================================================
 */

public class MultiPlayerHandler extends Handler {

    private static final String TAG = MultiPlayerHandler.class.getName();

    public static final int TRACK_ENDED = 1;
    public static final int TRACK_WENT_TO_NEXT = 2;
    public static final int RELEASE_WAKELOCK = 3;
    public static final int SERVER_DIED = 4;
    public static final int FOCUSCHANGE = 5;
    public static final int FADEDOWN = 6;
    public static final int FADEUP = 7;

    private final WeakReference<MusicService> mService;
    private float mCurrentVolume = 1.0f;

    public MultiPlayerHandler(final MusicService service, final Looper looper) {
        super(looper);
        mService = new WeakReference<>(service);
    }

    @Override
    public void handleMessage(final Message msg) {
        final MusicService service = mService.get();
        if (service == null) {
            return;
        }
        synchronized (service) {
            switch (msg.what) {
                case FADEDOWN:
                    mCurrentVolume -= .05f;
                    if (mCurrentVolume > .2f) {
                        sendEmptyMessageDelayed(FADEDOWN, 10);
                    } else {
                        mCurrentVolume = .2f;
                    }
                    service.mPlayer.setVolume(mCurrentVolume);
                    break;
                case FADEUP:
                    mCurrentVolume += .01f;
                    if (mCurrentVolume < 1.0f) {
                        sendEmptyMessageDelayed(FADEUP, 10);
                    } else {
                        mCurrentVolume = 1.0f;
                    }
                    service.mPlayer.setVolume(mCurrentVolume);
                    break;
                case SERVER_DIED:
                    if (service.isPlaying()) {
                        final MusicService.TrackErrorInfo info = (MusicService.TrackErrorInfo) msg.obj;
                        service.sendErrorMessage(info.mTrackName);
                        service.removeTrack(info.mId);
                    } else {
                        service.openCurrentAndNext();
                    }
                    break;
                case TRACK_WENT_TO_NEXT:
                    Timber.e(TAG + " TRACK_WENT_TO_NEXT, start . mNextPlayPos: " + service.getNextPlayPos());
                    service.setAndRecordPlayPos(service.getNextPlayPos());
                    service.setNextTrack();
                    Timber.e(TAG + " TRACK_WENT_TO_NEXT, end . mNextPlayPos: " + service.getNextPlayPos());
                    service.closeCursor();
                    service.updateCursor();
                    service.notifyChange(MusicService.META_CHANGED);
                    service.notifyChange(MusicService.MUSIC_CHANGED);
                    service.updateNotification();
                    break;
                case TRACK_ENDED:
                    if (service.getRepeatMode() == MusicDataManager.REPEAT_CURRENT) {
                        service.seek(0);
                        service.play();
                    } else {
                        Timber.d(TAG + " Going to  of track");
                        service.gotoNext(true);
                    }
                    break;
                case RELEASE_WAKELOCK:
                    service.mWakeLock.release();
                    break;
                case FOCUSCHANGE:
                    Timber.d(TAG + " Received audio focus change event " + msg.arg1);
                    switch (msg.arg1) {
                        case AudioManager.AUDIOFOCUS_LOSS:
                        case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                            if (service.isPlaying()) {
                                service.mPausedByTransientLossOfFocus =
                                        msg.arg1 == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT;
                            }
                            service.pause();
                            break;
                        case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                            removeMessages(FADEUP);
                            sendEmptyMessage(FADEDOWN);
                            break;
                        case AudioManager.AUDIOFOCUS_GAIN:
                            if (!service.isPlaying()
                                    && service.mPausedByTransientLossOfFocus) {
                                service.mPausedByTransientLossOfFocus = false;
                                mCurrentVolume = 0f;
                                service.mPlayer.setVolume(mCurrentVolume);
                                service.play();
                            } else {
                                removeMessages(FADEDOWN);
                                sendEmptyMessage(FADEUP);
                            }
                            break;
                        default:
                    }
                    break;
                case MusicService.LRC_DOWNLOADED:
                    service.notifyChange(MusicService.LRC_UPDATED);
                default:
                    break;
            }
        }
    }
}
