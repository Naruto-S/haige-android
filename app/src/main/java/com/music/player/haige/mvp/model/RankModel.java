package com.music.player.haige.mvp.model;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.OnLifecycleEvent;

import com.hc.base.mvp.BaseModel;
import com.hc.core.integration.IRepositoryManager;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.service.ApiService;
import com.music.player.haige.mvp.model.entity.rank.RankResponse;

import javax.inject.Inject;

import io.reactivex.Observable;
import timber.log.Timber;

/**
 * ================================================
 * Created by huangcong on 2018/3/3.
 * ================================================
 */

public class RankModel extends BaseModel implements MVContract.RankModel {

    @Inject
    public RankModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public Observable<RankResponse> loadMusicRank(String tag, int pageNo, int pageSize) {
        return Observable.just(mRepositoryManager.obtainRetrofitService(ApiService.class).getMusicTagList(tag, pageNo, pageSize))
                .flatMap(observable -> observable);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    void onPause() {
        Timber.d("Release Resource");
    }

}
