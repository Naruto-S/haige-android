package com.music.player.haige.mvp.ui.main.widget.likebutton;

public enum IconType {
    Heart,
    Thumb,
    Star
}
