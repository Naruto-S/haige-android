package com.music.player.haige.app.utils;

import android.app.ActivityManager;
import android.content.Context;

import com.music.player.haige.BuildConfig;

import java.util.List;


/**
 * Created by Messi on 16-8-15.
 */
public class ServiceUtils {

    private static final String TAG = ServiceUtils.class.getSimpleName();

    public static boolean isServiceRunning(Context context, Class<?> clazz) {
        boolean isRunning = false;
        ActivityManager activityManager = (ActivityManager)
                context.getSystemService(Context.ACTIVITY_SERVICE);
        if (activityManager == null) {
            return false;
        }
        List<ActivityManager.RunningServiceInfo> services
                = activityManager.getRunningServices(Integer.MAX_VALUE);

        if (services == null || services.isEmpty()) {
            return false;
        }

        int pid = android.os.Process.myPid();
        for (int i = 0; i < services.size(); i++) {
            ActivityManager.RunningServiceInfo service = services.get(i);
            if (service.service.getClassName().equals(clazz.getName())
                    && BuildConfig.APPLICATION_ID.equals(service.service.getPackageName())) {
                isRunning = true;
                break;
            }
        }
        return isRunning;
    }
}
