package com.music.player.haige.mvp.ui.main.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.hc.base.base.BaseFragment;
import com.hc.base.widget.OnTabSelectListener;
import com.hc.base.widget.SlidingTabLayout;
import com.hc.core.di.component.AppComponent;
import com.hc.core.http.imageloader.ImageLoader;
import com.music.player.haige.R;
import com.music.player.haige.app.EventBusTags;
import com.music.player.haige.app.base.AppBaseFragment;
import com.music.player.haige.app.base.pref.UserPrefHelper;
import com.music.player.haige.app.utils.launcher.ActivityLauncherStart;
import com.music.player.haige.app.utils.statistics.CommonStatistics;
import com.music.player.haige.app.utils.statistics.StatisticsConstant;
import com.music.player.haige.di.component.DaggerCommonComponent;
import com.music.player.haige.di.module.CommonModule;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.presenter.FindFPresenter;
import com.music.player.haige.mvp.ui.login.LoginDialogFragment;
import com.music.player.haige.mvp.ui.main.widget.MainMorePopWindow;
import com.music.player.haige.mvp.ui.upload.UploadListActivity;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;
import com.music.player.haige.mvp.ui.utils.Utils;
import com.music.player.haige.mvp.ui.widget.NoScrollViewPager;
import com.music.player.haige.mvp.ui.widget.adapter.TitlePagerAdapter;

import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * ================================================
 * Created by huangcong on 2018/2/27.
 * ================================================
 */

public class FindFragment extends AppBaseFragment<FindFPresenter> implements MVContract.CommonView {

    @BindView(R.id.container)
    ConstraintLayout mContainer;

    @BindView(R.id.tab_container)
    ConstraintLayout mTabContainer;

    @BindView(R.id.app_fragment_find_tl)
    SlidingTabLayout mTabLayout;

    @BindView(R.id.app_fragment_find_vp)
    NoScrollViewPager mViewPager;

    @BindView(R.id.app_fragment_find_more)
    ImageView mFindMoreIv;

    @Inject
    ImageLoader mImageLoader;

    private boolean isFirstTimeCreate = true;

    private BaseFragment[] fragments;

    public static FindFragment newInstance() {
        FindFragment fragment = new FindFragment();
        return fragment;
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        Animation anim;
        if(!isFirstTimeCreate) {
            if (enter) {
                anim = AnimationUtils.loadAnimation(getActivity(),
                        R.anim.slide_in_from_left);
            } else {
                anim = AnimationUtils.loadAnimation(getActivity(),
                        R.anim.slide_out_to_left);
            }
            return anim;
        }
        isFirstTimeCreate = false;
        return null;
    }

    @Override
    public void setupFragmentComponent(AppComponent appComponent) {
        DaggerCommonComponent
                .builder()
                .appComponent(appComponent)
                .commonModule(new CommonModule(this))
                .build()
                .inject(this);
    }

    @Override
    public View initView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.app_fragment_find, container, false);
    }

    @Override
    public void initData(Bundle savedInstanceState) {
        String[] titles = getResources().getStringArray(R.array.app_main_find);
        fragments = new BaseFragment[titles.length];
        fragments[0] = RecommendNewFragment.newInstance();
        fragments[1] = RankingNewFragment.newInstance();

        mContainer.setPadding(0, 0, 0, 0);
        mTabContainer.setPadding(0, (int) ResourceUtils.getDimen(R.dimen.statusbar_padding_top), 0, 0);
        mTabContainer.setBackgroundColor(Color.TRANSPARENT);

        TitlePagerAdapter adapter = new TitlePagerAdapter(getChildFragmentManager(), titles, fragments);
        mViewPager.setAdapter(adapter);
        mViewPager.setOffscreenPageLimit(adapter.getCount() - 1);
        mTabLayout.setViewPager(mViewPager);
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    mContainer.setPadding(0, 0, 0, 0);
                    mTabContainer.setPadding(0, (int) ResourceUtils.getDimen(R.dimen.statusbar_padding_top), 0, 0);
                    mTabContainer.setBackgroundColor(Color.TRANSPARENT);
                    EventBus.getDefault().post(EventBusTags.UI.FIND_RECOMMEND);
                } else if (position == 1) {
                    mContainer.setPadding(0, (int) ResourceUtils.getDimen(R.dimen.statusbar_padding_top), 0, 0);
                    mTabContainer.setPadding(0, 0, 0, 0);
                    mTabContainer.setBackgroundColor(ResourceUtils.getColor(R.color.colorWindowBackground));
                    CommonStatistics.sendEvent(StatisticsConstant.RANKING_CLICK);
                    EventBus.getDefault().post(EventBusTags.UI.FIND_RANKING);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mTabLayout.setOnTabSelectListener(new OnTabSelectListener() {

            @Override
            public void onTabSelect(int position) {
            }

            @Override
            public void onTabReselect(int position) {
                if (position == 0) {
                    RecommendNewFragment f = (RecommendNewFragment) fragments[0];
//                    f.refresh();
                } else if (position == 1) {
                    RankingNewFragment f = (RankingNewFragment) fragments[1];
                    f.refresh();
                }
            }
        });
    }

    @Override
    public void setData(Object data) {

    }

    @Subscriber
    public void onSubscriber(EventBusTags.UI tags) {
        switch (tags) {
            case GO_TO_RECOMMEND:
                if (Utils.isNotNull(mViewPager)) {
                    mViewPager.setCurrentItem(0);
                }
                break;
        }
    }

    @OnClick({R.id.app_fragment_find_search, R.id.app_fragment_find_more})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.app_fragment_find_search:
                CommonStatistics.sendEvent(StatisticsConstant.SEARCH_CLICK);
                EventBus.getDefault().post(EventBusTags.MAIN.SEARCH);
                break;
            case R.id.app_fragment_find_more:
                MainMorePopWindow popWindow = new MainMorePopWindow(getContext());
                popWindow.setOnMainMoreClickListener(() -> {
                    if (UserPrefHelper.isLogin()) {
                        ActivityLauncherStart.startActivity(getActivity(), UploadListActivity.class);
                    }  else {
                        LoginDialogFragment.newInstance().show(getChildFragmentManager(), LoginDialogFragment.class.getName());
                    }
                });
                popWindow.showPopWindow(mFindMoreIv);
                break;
        }
    }

    public ViewPager getViewPager() {
        return mViewPager;
    }

    public int getCurrentItem() {
        return mViewPager.getCurrentItem();
    }
}
