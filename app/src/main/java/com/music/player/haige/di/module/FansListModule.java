package com.music.player.haige.di.module;

import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.FansListModel;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Naruto on 2018/5/13.
 */
@Module
public class FansListModule {
    private MVContract.FansListView view;

    public FansListModule(MVContract.FansListView view){
        this.view = view;
    }

    @FragmentScope
    @Provides
    MVContract.FansListView provideView(){
        return view;
    }

    @FragmentScope
    @Provides
    MVContract.FansListModel provideModel(FansListModel model){
        return model;
    }
}
