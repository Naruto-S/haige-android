package com.music.player.haige.mvp.model;

import com.hc.base.mvp.BaseModel;
import com.hc.core.integration.IRepositoryManager;
import com.music.player.haige.mvp.contract.MVContract;

import javax.inject.Inject;

/**
 * Created by Naruto on 2018/5/5.
 */

public class CommentModel extends BaseModel implements MVContract.CommentModel {

    @Inject
    public CommentModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

}
