package com.music.player.haige.mvp.ui.user;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.hc.core.di.component.AppComponent;
import com.music.player.haige.R;
import com.music.player.haige.app.EventBusTags;
import com.music.player.haige.app.SystemUIConfig;
import com.music.player.haige.app.base.BaseListActivity;
import com.music.player.haige.app.base.pref.UserPrefHelper;
import com.music.player.haige.app.base.recyclerview.BaseQuickAdapter;
import com.music.player.haige.app.base.recyclerview.listener.OnItemClickListener;
import com.music.player.haige.app.base.recyclerview.pullrefresh.HaigeRefreshHeader;
import com.music.player.haige.app.base.recyclerview.view.EmptyView;
import com.music.player.haige.di.component.DaggerUploadMusicComponent;
import com.music.player.haige.di.module.UploadMusicModule;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.entity.action.ActionRequest;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.model.utils.NavigationUtil;
import com.music.player.haige.mvp.presenter.UploadMusicPresenter;
import com.music.player.haige.mvp.ui.login.LoginDialogFragment;
import com.music.player.haige.mvp.ui.main.activity.TagMusicActivity;
import com.music.player.haige.mvp.ui.main.fragment.PlayMainFragment;
import com.music.player.haige.mvp.ui.music.service.MusicServiceConnection;
import com.music.player.haige.mvp.ui.share.DownloadShareDialog;
import com.music.player.haige.mvp.ui.upload.adapter.UploadStatusListAdapter;
import com.music.player.haige.mvp.ui.upload.dialog.UploadPrivacyDialog;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;
import com.music.player.haige.mvp.ui.utils.ShareUtils;
import com.music.player.haige.mvp.ui.utils.Utils;

import org.simple.eventbus.Subscriber;

import butterknife.BindView;
import butterknife.OnClick;

import static com.music.player.haige.mvp.ui.main.fragment.PlayMainFragment.PLAY_MAIN_CURRENT_PLAY;

public class MeUploadActivity extends BaseListActivity<UploadMusicPresenter> implements MVContract.UploadMusicView,
        SystemUIConfig {

    @BindView(R.id.app_fragment_title_tv)
    TextView mTitleTv;

    @Override
    public boolean translucentStatusBar() {
        return false;
    }

    @Override
    public int setStatusBarColor() {
        return Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP
                ? getResources().getColor(R.color.color_33000000)
                : getResources().getColor(R.color.color_00000000);
    }

    @Override
    public boolean lightStatusBar() {
        return false;
    }

    @Override
    public boolean fitSystemWindows() {
        return true;
    }

    @Override
    public boolean fullScreen() {
        return false;
    }

    @Override
    protected RecyclerView.LayoutManager createLayoutManager() {
        return new LinearLayoutManager(this);
    }

    @Override
    protected OnItemClickListener createItemClickListener() {
        return new OnItemClickListener(false) {
            @Override
            public void SimpleOnItemClick(BaseQuickAdapter adapter, View view, int position) {
                if (Utils.isFastClick()) {
                    return;
                }

                Song song = (Song) adapter.getItem(position);
                if (Utils.isNotNull(song)) {
                    if (song.getUploadStatus() == 1) {
                        MusicServiceConnection.play(song, 0, false, false);
                        startPlayMain();
                    } else if (song.getUploadStatus() == 0) {
                        Toast.makeText(MeUploadActivity.this, R.string.update_music_review_toast, Toast.LENGTH_SHORT).show();
                    } else if (song.getUploadStatus() == 2) {
                        UploadPrivacyDialog.create(getSupportFragmentManager())
                                .setRefuseDialog(song)
                                .show();
                    }
                }
            }

            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                if (Utils.isFastClick()) {
                    return;
                }

                switch (view.getId()) {
                    case R.id.popup_menu:
                        if (UserPrefHelper.isLogin()) {
                            onPopupMenuClick(view, (Song) adapter.getItem(position), position);
                        } else {
                            LoginDialogFragment.newInstance().show(getSupportFragmentManager(), LoginDialogFragment.class.getName());
                        }
                        break;
                    default:
                        break;
                }
            }
        };
    }

    @Override
    protected RecyclerView.OnScrollListener addRecyclerViewScrollListener() {
        return null;
    }

    @Override
    protected RecyclerView.ItemDecoration createDecoration() {
        return null;
    }

    @Override
    protected RecyclerView.ItemAnimator createItemAnimator() {
        return null;
    }

    @Override
    protected View createRefreshHeader() {
        return new HaigeRefreshHeader(this);
    }

    @Override
    protected Context createContext() {
        return this;
    }

    @Override
    protected BaseQuickAdapter createAdapter() {
        return new UploadStatusListAdapter(this);
    }

    @Override
    public int getEmptyType() {
        return EmptyView.EMPTY_NO_FOLLOW;
    }

    @Override
    public void setupActivityComponent(AppComponent appComponent) {
        DaggerUploadMusicComponent
                .builder()
                .appComponent(appComponent)
                .uploadMusicModule(new UploadMusicModule(this))
                .build()
                .inject(this);
    }

    @Override
    public int initView(Bundle savedInstanceState) {
        return R.layout.activity_upload_music;
    }

    @Override
    protected void setupView() {
        super.setupView();

        mTitleTv.setText(ResourceUtils.resourceString(R.string.app_mine_upload));
    }

    @Override
    protected void sendListReq(int pageNum, int pageSize, boolean refresh) {
        mPresenter.getUploadMusicStatus(pageNum, pageSize, refresh);
    }

    @Override
    protected void reLoadData() {
        fetchPusherList(mAdapter, true);
    }

    @Override
    protected void loadMoreData() {
        fetchPusherList(mAdapter, false);
    }

    @Subscriber
    public void onSubscriber(EventBusTags.MetaChangedEvent event) {
        if (getAdapter() != null) {
            getAdapter().notifyDataSetChanged();
        }
    }

    @Subscriber
    public void onSubscriber(EventBusTags.MAIN tags) {
        switch (tags) {
            case BACK:
                getSupportFragmentManager().popBackStackImmediate();
                break;
        }
    }

    @OnClick({R.id.app_fragment_back_iv})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.app_fragment_back_iv:
                finish();
                break;
        }
    }

    public void onPopupMenuClick(View v, Song song, int position) {
        final PopupMenu menu = new PopupMenu(this, v);
        menu.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.popup_song_play_next:
                    MusicServiceConnection.playNext(song);
                    showMessage(ResourceUtils.resourceString(R.string.app_next_play));
                    break;
                case R.id.popup_song_goto_album:
                    NavigationUtil.goToAlbum(this, song.albumId, song.musicName);
                    break;
                case R.id.popup_song_goto_artist:
                    NavigationUtil.goToArtist(this, song.artistId, song.artistName);
                    break;
                case R.id.popup_song_addto_queue:
                    MusicServiceConnection.addToQueue(song);
                    break;
                case R.id.popup_download:
                    if (ShareUtils.checkDownloadShare(UserPrefHelper.getDownloadMusicCount())) {
                        DownloadShareDialog.newInstance().show(getSupportFragmentManager(), DownloadShareDialog.class.getName());
                    } else {
                        mPresenter.downloadMusic(song);
                    }
                    break;
                case R.id.popup_delete:
                    mAdapter.remove(position);

                    ActionRequest request = new ActionRequest();
                    request.setMusicId(song.getSongId());
                    mPresenter.deleteMusic(request);
                    break;
            }
            return false;
        });
        menu.inflate(R.menu.popup_upload_music_list);
        menu.show();
    }

    private void startPlayMain() {
        Fragment fragment = PlayMainFragment.newInstance(PLAY_MAIN_CURRENT_PLAY);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_from_bottom, R.anim.slide_out_to_left, R.anim.slide_in_from_left, R.anim.slide_out_to_bottom);
        transaction.replace(R.id.app_activity_main_foreground_layout, fragment, TagMusicActivity.class.getName());
        transaction.commitAllowingStateLoss();
        transaction.addToBackStack(null);
    }
}
