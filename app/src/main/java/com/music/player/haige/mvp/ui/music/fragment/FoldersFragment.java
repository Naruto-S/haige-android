package com.music.player.haige.mvp.ui.music.fragment;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.hc.core.di.component.AppComponent;
import com.music.player.haige.R;
import com.music.player.haige.app.EventBusTags;
import com.music.player.haige.app.base.AppBaseLazyFragment;
import com.music.player.haige.di.component.DaggerFoldersComponent;
import com.music.player.haige.di.module.FoldersModule;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.entity.music.FolderInfo;
import com.music.player.haige.mvp.presenter.FoldersPresenter;
import com.music.player.haige.mvp.ui.music.adapter.FolderAdapter;
import com.music.player.haige.mvp.ui.widget.DividerItemDecoration;
import com.music.player.haige.mvp.ui.widget.StatusLayout;

import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;

import java.util.List;

import butterknife.BindView;

/**
 * ================================================
 * Created by huangcong on 2018/3/2.¬
 * ================================================
 */

public class FoldersFragment extends AppBaseLazyFragment<FoldersPresenter> implements MVContract.FoldersView {

    @BindView(R.id.app_layout_sl)
    StatusLayout mStatusLayout;

    @BindView(R.id.app_layout_rv)
    RecyclerView mAlbumRecyclerView;

    @BindView(R.id.app_layout_progress)
    ProgressBar mProgressBar;

    private FolderAdapter mFolderAdapter;

    public static FoldersFragment newInstance() {
        FoldersFragment fragment = new FoldersFragment();
        return fragment;
    }

    @Override
    public void setupFragmentComponent(AppComponent appComponent) {
        DaggerFoldersComponent
                .builder()
                .appComponent(appComponent)
                .foldersModule(new FoldersModule(this))
                .build()
                .inject(this);
    }

    @Override
    public View initView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.app_layout_recyclerview, container, false);
    }

    @Override
    public void lazyInitData(Bundle savedInstanceState) {
        mStatusLayout.setOnStatusClickListener(new StatusLayout.OnStatusClickListener() {
            @Override
            public void onEmptyClick() {
                EventBus.getDefault().post(EventBusTags.UI.GO_TO_RECOMMEND);
                EventBus.getDefault().post(EventBusTags.MAIN.BACK);
            }

            @Override
            public void onReloadClick() {

            }
        });

        mFolderAdapter = new FolderAdapter((AppCompatActivity) getActivity(), null);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        DividerItemDecoration itemDecoration = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST, false);
        mAlbumRecyclerView.setLayoutManager(layoutManager);
        mAlbumRecyclerView.setHasFixedSize(true);
        mAlbumRecyclerView.addItemDecoration(itemDecoration);
        mAlbumRecyclerView.setAdapter(mFolderAdapter);

        mPresenter.loadFolders();
    }

    @Override
    public void showLoading() {
        super.showLoading();
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void showFolders(List<FolderInfo> folders) {
        mStatusLayout.showStatusView(StatusLayout.STATUS.OK);
        mAlbumRecyclerView.setVisibility(View.VISIBLE);
        mFolderAdapter.setFolderList(folders);
    }

    @Override
    public void showEmptyView() {
        mStatusLayout.showStatusView(StatusLayout.STATUS.EMPTY);
        mAlbumRecyclerView.setVisibility(View.INVISIBLE);
    }

    @Subscriber
    public void onSubscriber(EventBusTags.MetaChangedEvent event) {
        if (mFolderAdapter != null) {
            mFolderAdapter.notifyDataSetChanged();
        }
    }
}
