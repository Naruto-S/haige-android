package com.music.player.haige.app.image.utils;

import android.content.Context;
import android.os.Environment;

import com.music.player.haige.mvp.ui.utils.DebugLogger;
import com.music.player.haige.mvp.ui.utils.Utils;

import java.io.File;

/**
 * Created by liumingkong on 14-3-31.
 */
public class FileStore {

    public static final String TAG = FileStore.class.getSimpleName();

    public static final String SUFFIX_JPG = ".jpg";
    public static final String SUFFIX_PNG = ".png";
    public static final String SUFFIX_NOMEDIA = ".nomedia";
    public static final String SUFFIX_WEBP = ".webp";
    public static final String SUFFIX_9_PNG = ".9.png";
    public static final String SUFFIX_GIF = ".gif";

    private static final String sep = "-";
    private static final String FRESCO_TAG = "fresco";
    private static final String MAIN_TAG = "main";
    private static final String MIN_TAG = "min";
    private static String appRootTag;

    private static final String TAG_SYSTEM_IMAGE = "image";

    // 文件存储的根地址
    public static String getRootPath() {
        if (Utils.isEmptyString(appRootTag)) {
            appRootTag = "Haige";
        }
        return Environment.getExternalStorageDirectory() + File.separator + appRootTag + File.separator;
    }

    public static String getSystemImagePath() {
        return getRootPath() + TAG_SYSTEM_IMAGE + File.separator;
    }

    public static void createDirectory(String dirPath) {
        if (SDCardUtil.isSDCardWritable()) {
            SDCardUtil.createFolder(dirPath);
        }
    }

    // imageloader disc cache dir
    public static File getFrescoMainImageFile(Context context) {
        String fullPath = context.getFilesDir().getAbsolutePath() + File.separator + FRESCO_TAG + File.separator + MAIN_TAG;
        createDirectory(fullPath);
        return SDCardUtil.createFolder(fullPath);
    }

    // imageloader disc cache dir
    public static File getFrescoMinImageFile(Context context) {
        String fullPath = context.getFilesDir().getAbsolutePath() + File.separator + FRESCO_TAG + File.separator + MIN_TAG;
        createDirectory(fullPath);
        return SDCardUtil.createFolder(fullPath);
    }

    // 文件ID的结构，前缀-MD5(当前时间).后缀
    protected static String generateLocalFidWithTimestamp(String prefix, String suffix) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(prefix).append(sep).append(MD5.getMD5(String.valueOf(System
                .currentTimeMillis()))).append(suffix);
        return stringBuilder.toString();
    }

    public static void removeFile(String filePath) {
        try {
            File file = new File(filePath);
            file.delete();
        } catch (Exception e) {

        }
    }

    protected static void delFolder(String folderPath) {
        try {
            delAllFile(folderPath);
            String filePath = folderPath;
            filePath = filePath.toString();
            java.io.File myFilePath = new java.io.File(filePath);
            myFilePath.delete();
        } catch (Exception e) {
            DebugLogger.e(TAG, e);
        }
    }

    public static boolean delAllFile(String path) {
        boolean flag = false;
        File file = new File(path);
        if (!file.exists()) {
            return flag;
        }
        if (!file.isDirectory()) {
            return flag;
        }
        String[] tempList = file.list();
        if (Utils.isNotNull(tempList)) {
            File temp = null;
            for (int i = 0; i < tempList.length; i++) {
                if (path.endsWith(File.separator)) {
                    temp = new File(path + tempList[i]);
                } else {
                    temp = new File(path + File.separator + tempList[i]);
                }
                if (temp.isFile()) {
                    temp.delete();
                }
                if (temp.isDirectory()) {
                    delAllFile(path + File.separator + tempList[i]);
                    delFolder(path + File.separator + tempList[i]);
                    flag = true;
                }
            }
        }
        return flag;
    }
}
