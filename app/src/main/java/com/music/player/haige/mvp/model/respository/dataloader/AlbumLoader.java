package com.music.player.haige.mvp.model.respository.dataloader;

import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;

import com.music.player.haige.mvp.model.entity.music.Album;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.model.utils.PreferencesUtility;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.functions.Function;

public class AlbumLoader {

    private static Observable<Album> getAlbum(final Cursor cursor) {
        return Observable.create(new ObservableOnSubscribe<Album>() {
            @Override
            public void subscribe(ObservableEmitter<Album> emitter) throws Exception {
                Album album = new Album();
                if (cursor != null) {
                    if (cursor.moveToFirst())
                        album = new Album(cursor.getLong(0), cursor.getString(1), cursor.getString(2), cursor.getLong(3), cursor.getInt(4), cursor.getInt(5));
                }
                if (cursor != null) {
                    cursor.close();
                }
                emitter.onNext(album);
                emitter.onComplete();
            }
        });
    }

    public static Observable<List<Album>> getFavoriteAlbums(final Context context) {
        return SongLoader.getFavoriteSong(context)
                .flatMap(new Function<List<Song>, Observable<Song>>() {
                    @Override
                    public Observable<Song> apply(List<Song> songList) throws Exception {
                        return Observable.fromIterable(songList);
                    }
                }).distinct(new Function<Song, Long>() {
                    @Override
                    public Long apply(Song song) throws Exception {
                        return song.albumId;
                    }
                }).flatMap(new Function<Song, Observable<Album>>() {
                    @Override
                    public Observable<Album> apply(Song song) throws Exception {
                        return AlbumLoader.getAlbum(context, song.albumId);
                    }
                }).toList().toObservable();
    }

    public static Observable<List<Album>> getRecentlyPlayedAlbums(final Context context) {
        return TopTracksLoader.getTopRecentSongs(context)
                .flatMap(new Function<List<Song>, Observable<Song>>() {
                    @Override
                    public Observable<Song> apply(List<Song> songs) throws Exception {
                        return Observable.fromIterable(songs);
                    }
                }).distinct(new Function<Song, Long>() {
                    @Override
                    public Long apply(Song song) throws Exception {
                        return song.albumId;
                    }
                }).flatMap(new Function<Song, Observable<Album>>() {

                    @Override
                    public Observable<Album> apply(Song song) throws Exception {
                        return AlbumLoader.getAlbum(context, song.albumId);
                    }
                }).toList().toObservable();
    }

    private static Observable<List<Album>> getAlbumsForCursor(final Cursor cursor) {
        return Observable.create(new ObservableOnSubscribe<List<Album>>() {

            @Override
            public void subscribe(ObservableEmitter<List<Album>> emitter) throws Exception {
                List<Album> arrayList = new ArrayList<Album>();
                if ((cursor != null) && (cursor.moveToFirst()))
                    do {
                        arrayList.add(new Album(cursor.getLong(0), cursor.getString(1), cursor.getString(2), cursor.getLong(3), cursor.getInt(4), cursor.getInt(5)));
                    }
                    while (cursor.moveToNext());
                if (cursor != null) {
                    cursor.close();
                }
                emitter.onNext(arrayList);
                emitter.onComplete();
            }
        });
    }

    public static Observable<List<Album>> getAllAlbums(Context context) {
        return getAlbumsForCursor(makeAlbumCursor(context, null, null));
    }

    public static Observable<Album> getAlbum(Context context, long id) {
        return getAlbum(makeAlbumCursor(context, "_id=?", new String[]{String.valueOf(id)}));
    }

    public static Observable<List<Album>> getAlbums(Context context, String paramString) {
        return getAlbumsForCursor(makeAlbumCursor(context, "album LIKE ? or artist LIKE ? ",
                new String[]{"%" + paramString + "%", "%" + paramString + "%"}));
    }

    private static Cursor makeAlbumCursor(Context context, String selection, String[] paramArrayOfString) {
        final String albumSortOrder = PreferencesUtility.getInstance(context).getAlbumSortOrder();
        Cursor cursor = context.getContentResolver().query(MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI, new String[]{"_id", "album", "artist", "artist_id", "numsongs", "minyear"}, selection, paramArrayOfString, albumSortOrder);

        return cursor;
    }
}
