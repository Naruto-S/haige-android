package com.music.player.haige.mvp.ui.user;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hc.core.di.component.AppComponent;
import com.hc.core.utils.CoreUtils;
import com.music.player.haige.R;
import com.music.player.haige.app.EventBusTags;
import com.music.player.haige.app.base.AppBaseFragment;
import com.music.player.haige.app.base.pref.DevicePrefHelper;
import com.music.player.haige.app.base.pref.UserPrefHelper;
import com.music.player.haige.app.image.loader.AvatarLoader;
import com.music.player.haige.app.image.loader.LocalImageLoader;
import com.music.player.haige.app.image.widget.HaigeImageView;
import com.music.player.haige.app.utils.launcher.ActivityLauncherStart;
import com.music.player.haige.di.component.DaggerMeProfileComponent;
import com.music.player.haige.di.module.MeProfileModule;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.Api;
import com.music.player.haige.mvp.model.entity.user.UserBean;
import com.music.player.haige.mvp.presenter.MeProfilePresenter;
import com.music.player.haige.mvp.ui.user.utils.UserViewUtils;
import com.music.player.haige.mvp.ui.user.widget.ProfileMorePopWindow;
import com.music.player.haige.mvp.ui.utils.DebugLogger;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;
import com.music.player.haige.mvp.ui.utils.Utils;
import com.music.player.haige.mvp.ui.utils.ViewVisibleUtils;

import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;

import butterknife.BindView;
import butterknife.OnClick;

import static com.music.player.haige.mvp.ui.user.FansListActivity.TYPE_FOLLOWER;
import static com.music.player.haige.mvp.ui.user.FansListActivity.TYPE_FOLLOWING;

/**
 * Created by Naruto on 2018/5/13.
 */

public class MeProfileFragment extends AppBaseFragment<MeProfilePresenter> implements MVContract.MeProfileView,
        ProfileMorePopWindow.OnProfileMoreClickListener {

    @BindView(R.id.hiv_avatar)
    HaigeImageView mAvatarHiv;
    @BindView(R.id.tv_name)
    TextView mNameTv;
    @BindView(R.id.tv_hai_id)
    TextView mIdTv;
    @BindView(R.id.tv_age)
    TextView mAgeTv;
    @BindView(R.id.tv_desc)
    TextView mDescTv;
    @BindView(R.id.tv_province)
    TextView mProvinceTv;
    @BindView(R.id.tv_sign)
    TextView mSignTv;
    @BindView(R.id.app_fragment_more_iv)
    ImageView mMoreIv;
    @BindView(R.id.tv_follow_count)
    TextView mFollowCountTv;
    @BindView(R.id.tv_fans_count)
    TextView mFansCountTv;
    @BindView(R.id.tv_music_time)
    TextView mMusicTimeTv;
    @BindView(R.id.tv_local_music_count)
    TextView mLocalCountTv;
    @BindView(R.id.tv_mine_download_count)
    TextView mDownloadCountTv;
    @BindView(R.id.tv_recently_played_count)
    TextView mRecentlyCountTv;
    @BindView(R.id.tv_mine_favorite_count)
    TextView mLikeCountTv;
    @BindView(R.id.tv_upload_music_count)
    TextView mUploadCountTv;
    @BindView(R.id.layout_top_tips)
    View mTopTips;

    private UserBean mUser;
    private ProfileMorePopWindow mProfileMorePopWindow;

    public static MeProfileFragment newInstance() {
        MeProfileFragment fragment = new MeProfileFragment();
        return fragment;
    }

    @Override
    public void setupFragmentComponent(AppComponent component) {
        DaggerMeProfileComponent
                .builder()
                .appComponent(component)
                .meProfileModule(new MeProfileModule(this))
                .build()
                .inject(this);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        DebugLogger.e(TAG, "setUserVisibleHint == " + isVisibleToUser);
        if (isVisibleToUser) {
            EventBus.getDefault().post(EventBusTags.UI.UPDATE_MINE_UI);
        }
    }

    @Override
    public View initView(LayoutInflater inflater, ViewGroup group, Bundle bundle) {
        return inflater.inflate(R.layout.app_fragment_me, group, false);
    }

    @Override
    public void initData(Bundle bundle) {
        mUser = UserPrefHelper.getMeUser();
        setUserView();

        boolean isShowMineTips = DevicePrefHelper.getUserConfig().isIsShowFeedbackBar() && UserPrefHelper.isShowMineTips();
        ViewVisibleUtils.setVisibleGone(mTopTips, isShowMineTips);
    }

    private void setUserView() {
        try {
            if (UserPrefHelper.isLogin() && Utils.isNotNull(mUser)) {
                AvatarLoader.loadUserAvatar(mUser.getAvatarUrl(),
                        R.drawable.ic_main_default_avatar,
                        R.drawable.ic_main_default_avatar,
                        mAvatarHiv);
                UserViewUtils.setUserName(mUser, mNameTv);
                UserViewUtils.setUserHaigeId(mUser, mIdTv);
                UserViewUtils.setUserGenderAndAge(mUser, mAgeTv);
                UserViewUtils.setUserProvince(mUser, mProvinceTv);
                UserViewUtils.setUserDesc(mUser, mDescTv);
                mFollowCountTv.setText(String.valueOf(mUser.getFollowingCount()));
                mFansCountTv.setText(String.valueOf(mUser.getFollowerCount()));
                setFavoriteSongsCount(mUser.getLikedCount());
                mUploadCountTv.setText(ResourceUtils.resourceString(R.string.app_count_no_song, mUser.getUploadCount()));
            }
        } catch (Exception e) {

        }
    }

    private void startLoad() {
        mPresenter.getUserProfile();
        mPresenter.loadAllSongs();
        mPresenter.loadRecentPlaySongs();
        mPresenter.loadDownloadFinishMusic();
    }

    @Override
    public void onStart() {
        super.onStart();

        startLoad();
    }

    @Override
    public void onSuccess(Api.Action action, Object o) {
        super.onSuccess(action, o);

        switch (action) {
            case USER_PROFILE:
                mUser = (UserBean) o;
                UserPrefHelper.putMeUser(mUser);

                setUserView();
                break;
        }
    }

    @Override
    public void setAllSongsCount(int count) {
        if (Utils.isNotNull(mLocalCountTv)) {
            mLocalCountTv.setText(getResources().getString(R.string.app_count_no_song, count));
        }
    }

    @Override
    public void setFavoriteSongsCount(int count) {
        if (Utils.isNotNull(mLikeCountTv)) {
            mLikeCountTv.setText(getResources().getString(R.string.app_count_no_song, count));
        }
    }

    @Override
    public void setRecentPlaySongsCount(int count) {
        if (Utils.isNotNull(mRecentlyCountTv)) {
            mRecentlyCountTv.setText(getResources().getString(R.string.app_count_no_song, count));
        }
    }

    @Override
    public void setDownloadSongsCount(int count) {
        if (Utils.isNotNull(mDownloadCountTv)) {
            mDownloadCountTv.setText(getResources().getString(R.string.app_count_no_song, count));
        }
    }

    @Override
    public void onError(Api.Action action, String code, String message) {
        super.onError(action, code, message);
    }

    @Override
    public void showMessage(String message) {
        super.showMessage(message);
        CoreUtils.makeText(getContext(), message);
    }

    @OnClick({R.id.app_fragment_back_iv, R.id.app_fragment_more_iv, R.id.layout_follow, R.id.layout_fans,
            R.id.layout_local_music, R.id.layout_mine_download, R.id.layout_recently_played,
            R.id.layout_mine_favorite, R.id.layout_upload_music, R.id.layout_user_info,
            R.id.layout_top_tips})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.app_fragment_back_iv:
                EventBus.getDefault().post(EventBusTags.MAIN.BACK);
                break;
            case R.id.app_fragment_more_iv:
                if (mProfileMorePopWindow == null) {
                    mProfileMorePopWindow = new ProfileMorePopWindow(getActivity());
                    mProfileMorePopWindow.setProfileMoreClickListener(this);
                }
                mProfileMorePopWindow.showPopWindow(mMoreIv);
                break;
            case R.id.layout_follow:
                ActivityLauncherStart.startFollowList(getActivity(), mUser, TYPE_FOLLOWING);
                break;
            case R.id.layout_fans:
                ActivityLauncherStart.startFollowList(getActivity(), mUser, TYPE_FOLLOWER);
                break;
            case R.id.layout_local_music:
                EventBus.getDefault().post(EventBusTags.MAIN.MUSIC);
                break;
            case R.id.layout_mine_download:
                EventBus.getDefault().post(EventBusTags.MAIN.DOWNLOAD);
                break;
            case R.id.layout_recently_played:
                EventBus.getDefault().post(EventBusTags.MAIN.RECENT_PLAY);
                break;
            case R.id.layout_mine_favorite:
                ActivityLauncherStart.startActivity(getActivity(), MeLikeMusicActivity.class);
                break;
            case R.id.layout_upload_music:
                ActivityLauncherStart.startActivity(getActivity(), MeUploadActivity.class);
                break;
            case R.id.layout_user_info:
                onEditClick();
                break;
            case R.id.layout_top_tips:
                UserPrefHelper.putIsShowMineTips(false);
                ViewVisibleUtils.setVisibleGone(mTopTips, false);
                EventBus.getDefault().post(EventBusTags.MAIN.SETTING_FEEDBACK);
                break;
        }
    }

    @Subscriber
    public void onSubscriber(EventBusTags.UI tags) {
        switch (tags) {
            case EDIT_UPDATE_USER:
                mPresenter.getUserProfile();
                break;
            case UPDATE_MINE_UI:
                startLoad();
                break;
            case UPDATE_USER_UI:
                if (UserPrefHelper.isLogin()) {
                    startLoad();
                } else {
                    mUser = null;

                    LocalImageLoader.displayResImage(R.drawable.ic_main_default_avatar, mAvatarHiv);
                    mNameTv.setText("");
                    mIdTv.setText(ResourceUtils.resourceString(R.string.user_profile_haige_id, ""));
                    mAgeTv.setText("0岁");
                    mAgeTv.setBackgroundResource(R.drawable.bg_user_age_boy);
                    mProvinceTv.setVisibility(View.GONE);
                    mDescTv.setText(ResourceUtils.resourceString(R.string.user_desc_default));
                    mFollowCountTv.setText("0");
                    mFansCountTv.setText("0");
                }
                break;
        }
    }

    @Override
    public void onSettingsClick() {
        EventBus.getDefault().post(EventBusTags.MAIN.SETTING);
    }

    @Override
    public void onEditClick() {
        if (UserPrefHelper.isLogin()) {
            EventBus.getDefault().post(EventBusTags.MAIN.USER_PROFILE_EDIT);
        }
    }
}
