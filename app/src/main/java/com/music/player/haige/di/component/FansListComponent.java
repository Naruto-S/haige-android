package com.music.player.haige.di.component;

import com.hc.core.di.component.AppComponent;
import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.di.module.FansListModule;
import com.music.player.haige.mvp.ui.user.FansListActivity;

import dagger.Component;

/**
 * Created by Naruto on 2018/5/13.
 */
@FragmentScope
@Component(modules = {FansListModule.class}, dependencies = AppComponent.class)
public interface FansListComponent {

    void inject(FansListActivity activity);
}
