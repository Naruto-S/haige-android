package com.music.player.haige.mvp.model.utils;

import org.greenrobot.greendao.converter.PropertyConverter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * GreenDao数据转换
 */
public class StringConverter implements PropertyConverter<ArrayList<String>, String> {

    //将数据库中的值，转化为实体Bean类对象(比如List<String>)
    @Override
    public ArrayList<String> convertToEntityProperty(String databaseValue) {
        if (databaseValue == null) {
            return null;
        } else {
            ArrayList<String> list = new ArrayList<>();
            list.addAll(Arrays.asList(databaseValue.split(",")));
            return list;
        }
    }

    //将实体Bean类(比如List<String>)转化为数据库中的值(比如String)
    @Override
    public String convertToDatabaseValue(ArrayList<String> entityProperty) {
        if (entityProperty == null) {
            return null;
        } else {
            StringBuilder sb = new StringBuilder();
            for (String link : entityProperty) {
                sb.append(link);
                sb.append(",");
            }
            return sb.toString();
        }
    }
}  