package com.music.player.haige.mvp.ui.comment.adapter;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.music.player.haige.R;
import com.music.player.haige.app.base.pref.UserPrefHelper;
import com.music.player.haige.app.image.loader.AvatarLoader;
import com.music.player.haige.app.image.widget.HaigeImageView;
import com.music.player.haige.app.utils.launcher.ActivityLauncherStart;
import com.music.player.haige.app.utils.launcher.ActivityStartBase;
import com.music.player.haige.mvp.model.entity.action.CommentLikeRequest;
import com.music.player.haige.mvp.model.entity.comment.CommentBean;
import com.music.player.haige.mvp.model.entity.comment.CommentRequest;
import com.music.player.haige.mvp.model.entity.user.UserBean;
import com.music.player.haige.mvp.ui.comment.utils.CommentUtils;
import com.music.player.haige.mvp.ui.utils.DebugLogger;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;
import com.music.player.haige.mvp.ui.utils.Utils;
import com.music.player.haige.mvp.ui.widget.dialog.SystemDialog;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.music.player.haige.mvp.model.entity.action.CommentLikeRequest.ACTION_LIKE;
import static com.music.player.haige.mvp.model.entity.action.CommentLikeRequest.ACTION_UNLIKE;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.CommentViewHolder> {

    private Context mContext;
    private onCommentActionListener mActionListener;
    private onCommentReplyListener mReplyListener;
    private ArrayList<CommentBean> mCommentList = new ArrayList<>();

    @Override
    public CommentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_comment, parent, false);
        return new CommentViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CommentViewHolder holder, int position) {
        CommentBean comment = mCommentList.get(position);
        UserBean user = comment.getUserBean();

        if (Utils.isNotNull(comment) && Utils.isNotNull(user)) {
            AvatarLoader.loadUserAvatar(user.getAvatarUrl(),
                    R.drawable.ic_main_default_avatar,
                    R.drawable.ic_main_default_avatar,
                    holder.commentAvatarHiv);
            holder.commentNameTv.setOnClickListener(v -> ActivityLauncherStart.startOtherUser(ActivityStartBase.scanForActivity(mContext), user));
            holder.commentAvatarHiv.setOnClickListener(v -> ActivityLauncherStart.startOtherUser(ActivityStartBase.scanForActivity(mContext), user));

            holder.commentNameTv.setText(user.getNickName());
            holder.commentContentTv.setText(comment.getContext());
            holder.commentContentTv.setOnClickListener(v -> {
                if (UserPrefHelper.isMe(user)) {
                    showDeleteDiglog(position, comment);
                } else if (Utils.isNotNull(mReplyListener)) {
                    CommentRequest request = new CommentRequest();
                    request.setCommentId(comment.getId());
                    request.setUser(comment.getUserBean());
                    request.setPosition(position);
                    mReplyListener.onReply(request);
                }
            });
            CommentUtils.showCommentTime(holder.commentTimeTv, comment.getCreatedAt());

            if (comment.isLiked()) {
                holder.commentLikeIv.setImageResource(R.drawable.app_ic_like_checked);
                holder.commentLikeCountTv.setTextColor(ResourceUtils.getColor(R.color.colorAccent));
            } else {
                holder.commentLikeIv.setImageResource(R.drawable.app_ic_like);
                holder.commentLikeCountTv.setTextColor(ResourceUtils.getColor(R.color.comment_item_title_color));
            }
            holder.commentLikeIv.setOnClickListener(v -> {
                if (Utils.isNotNull(mActionListener)) {
                    CommentLikeRequest request = new CommentLikeRequest();
                    if (comment.isLiked()) {
                        request.setAction(ACTION_UNLIKE);
                        comment.setLiked(false);

                        int count = comment.getLiked() - 1;
                        comment.setLiked(count < 0 ? 0 : count);
                    } else {
                        request.setAction(ACTION_LIKE);
                        comment.setLiked(true);

                        int count = comment.getLiked() + 1;
                        comment.setLiked(count < 0 ? 0 : count);
                        comment.setLiked(count);
                    }
                    request.setCommentId(comment.getId());
                    mActionListener.onLike(request);

                    mCommentList.set(position, comment);
                    notifyItemChanged(position);
                }
            });
            holder.commentLikeCountTv.setText(String.valueOf(comment.getLiked()));

            if (UserPrefHelper.isMe(user)) {
                holder.deleteTv.setText(ResourceUtils.resourceString(R.string.comment_action_delete));
                holder.deleteTv.setBackgroundResource(0);
                holder.deleteTv.setOnClickListener(v -> {
                    if (Utils.isNotNull(mActionListener)) {
                        showDeleteDiglog(position, comment);
                    }
                });
            } else if (!Utils.isZero(comment.getSubCount())) {
                holder.deleteTv.setText(ResourceUtils.resourceString(R.string.comment_reply_num, comment.getSubCount()));
                holder.deleteTv.setBackgroundResource(R.drawable.bg_comment_reply_num);

                onClickReply(holder, comment, position);
            } else {
                holder.deleteTv.setText(ResourceUtils.resourceString(R.string.comment_action_reply));
                holder.deleteTv.setBackgroundResource(0);

                onClickReply(holder, comment, position);
            }

            holder.replyRecycler.setLayoutManager(new LinearLayoutManager(mContext));
            holder.replyRecycler.setHasFixedSize(true);
            holder.replyRecycler.setItemAnimator(null);
            SubCommentAdapter subAdapter = new SubCommentAdapter();
            subAdapter.setActionListener(mActionListener);
            subAdapter.setDeleteReplyListener(request -> {
                showReplyDeleteDiglog(position, request);
            });
            holder.replyRecycler.setAdapter(subAdapter);
            subAdapter.setCommentId(comment.getId());
            subAdapter.setPosition(position);
            subAdapter.setReplyCount(comment.getSubCount());
            subAdapter.setCommentList(comment.getSubComments());
        }
    }

    private void showReplyDeleteDiglog(int position, CommentRequest request) {
        new SystemDialog().create(((AppCompatActivity) ActivityStartBase.scanForActivity(mContext)).getSupportFragmentManager())
                .setContent(ResourceUtils.resourceString(R.string.comment_dialog_delete_content))
                .setOk(ResourceUtils.resourceString(R.string.app_confirm))
                .setCancle(ResourceUtils.resourceString(R.string.app_cancel))
                .setOnOkListener(() -> {
                    try {
                        request.setSub(true);
                        mActionListener.onDelete(request);

                        int subPosition = request.getPosition();
                        CommentBean oldComment = mCommentList.get(position);
                        ArrayList<CommentBean> comments = oldComment.getSubComments();
                        comments.remove(subPosition);
                        oldComment.setSubComments(comments);
                        oldComment.setSubCount(comments.size());
                        mCommentList.set(position, oldComment);
                        notifyItemChanged(position);
                    } catch (Exception e) {
                        DebugLogger.e(e);
                    }
                })
                .setOnCancelListener(() -> {
                })
                .show();
    }

    private void showDeleteDiglog(int position, CommentBean comment) {
        new SystemDialog().create(((AppCompatActivity) ActivityStartBase.scanForActivity(mContext)).getSupportFragmentManager())
                .setContent(ResourceUtils.resourceString(R.string.comment_dialog_delete_content))
                .setOk(ResourceUtils.resourceString(R.string.app_confirm))
                .setCancle(ResourceUtils.resourceString(R.string.app_cancel))
                .setOnOkListener(() -> {
                    CommentRequest request = new CommentRequest();
                    request.setCommentId(comment.getId());
                    request.setSub(false);
                    mActionListener.onDelete(request);

                    mCommentList.remove(position);
                    notifyItemRemoved(position);
                })
                .setOnCancelListener(() -> {
                })
                .show();
    }

    private void onClickReply(CommentViewHolder holder, CommentBean comment, int position) {
        holder.deleteTv.setOnClickListener(v -> {
            if (Utils.isNotNull(mReplyListener)) {
                CommentRequest request = new CommentRequest();
                request.setCommentId(comment.getId());
                request.setUser(comment.getUserBean());
                request.setPosition(position);
                mReplyListener.onReply(request);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mCommentList.size();
    }

    public void setCommentList(ArrayList<CommentBean> commentList) {
        mCommentList.clear();
        mCommentList.addAll(commentList);
        notifyDataSetChanged();
    }

    public ArrayList<CommentBean> getCommentList() {
        return mCommentList;
    }

    public CommentBean getCommentItem(int position) {
        return mCommentList.get(position);
    }

    public void setCommentActionListener(onCommentActionListener actionListener) {
        mActionListener = actionListener;
    }

    public void setCommentReplyListener(onCommentReplyListener replyListener) {
        mReplyListener = replyListener;
    }

    class CommentViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.hiv_comment_avatar)
        HaigeImageView commentAvatarHiv;

        @BindView(R.id.tv_comment_name)
        TextView commentNameTv;

        @BindView(R.id.tv_comment_content)
        TextView commentContentTv;

        @BindView(R.id.iv_comment_like)
        ImageView commentLikeIv;

        @BindView(R.id.tv_comment_like_count)
        TextView commentLikeCountTv;

        @BindView(R.id.tv_comment_time)
        TextView commentTimeTv;

        @BindView(R.id.tv_delete)
        TextView deleteTv;

        @BindView(R.id.recycler_reply)
        RecyclerView replyRecycler;

        public CommentViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface onCommentActionListener {
        void onDelete(CommentRequest request);

        void onLike(CommentLikeRequest request);

        void onClickAllSub(String commentId, int position);
    }

    public interface onCommentReplyListener {
        void onReply(CommentRequest request);
    }
}
