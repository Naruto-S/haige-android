package com.music.player.haige.di.module;

import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.FeedbackModel;

import dagger.Module;
import dagger.Provides;

/**
 * ================================================
 * Created by huangcong on 2018/2/27.
 * ================================================
 */
@Module
public class FeedbackModule {
    private MVContract.FeedbackView view;

    public FeedbackModule(MVContract.FeedbackView view) {
        this.view = view;
    }

    @FragmentScope
    @Provides
    MVContract.FeedbackView provideView() {
        return this.view;
    }

    @FragmentScope
    @Provides
    MVContract.FeedbackModel provideModel(FeedbackModel model) {
        return model;
    }

}
