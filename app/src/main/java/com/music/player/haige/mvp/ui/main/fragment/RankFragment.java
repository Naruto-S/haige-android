package com.music.player.haige.mvp.ui.main.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;

import com.hc.core.di.component.AppComponent;
import com.hc.core.utils.CoreUtils;
import com.music.player.haige.R;
import com.music.player.haige.app.Constants;
import com.music.player.haige.app.EventBusTags;
import com.music.player.haige.app.base.BaseListFragment;
import com.music.player.haige.app.base.pref.UserPrefHelper;
import com.music.player.haige.app.base.recyclerview.BaseQuickAdapter;
import com.music.player.haige.app.base.recyclerview.listener.OnItemClickListener;
import com.music.player.haige.app.base.recyclerview.pullrefresh.HaigeRefreshHeader;
import com.music.player.haige.di.component.DaggerRankComponent;
import com.music.player.haige.di.module.RankModule;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.presenter.RankFPresenter;
import com.music.player.haige.mvp.ui.login.LoginDialogFragment;
import com.music.player.haige.mvp.ui.music.adapter.MusicListAdapter;
import com.music.player.haige.mvp.ui.music.service.MusicServiceConnection;
import com.music.player.haige.mvp.ui.share.DownloadShareDialog;
import com.music.player.haige.mvp.ui.utils.ShareUtils;
import com.music.player.haige.mvp.ui.utils.Utils;

import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;

/**
 * ================================================
 * Created by huangcong on 2018/3/3.
 * ================================================
 */

public class RankFragment extends BaseListFragment<RankFPresenter> implements MVContract.RankView {

    private String mRankType;

    public static RankFragment newInstance(String rank) {
        RankFragment fragment = new RankFragment();
        Bundle args = new Bundle();
        args.putString(Constants.RANK_TYPE, rank);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void setupFragmentComponent(AppComponent appComponent) {
        DaggerRankComponent
                .builder()
                .appComponent(appComponent)
                .rankModule(new RankModule(this))
                .build()
                .inject(this);
    }

    @Override
    public View initView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_music_list, container, false);
    }

    @Override
    public void initData(Bundle bundle) {
        super.initData(bundle);

        mRankType = getArguments().getString(Constants.RANK_TYPE);
    }

    @Override
    protected void sendListReq(int pageNum, int pageSize, boolean refresh) {
        mPresenter.loadMusicRank(mRankType, pageNum, pageSize, refresh);
    }

    @Override
    protected void reLoadData() {
        fetchPusherList(mAdapter, true);
    }

    @Override
    protected void loadMoreData() {
        fetchPusherList(mAdapter, false);
    }

    @Override
    public void showMessage(String message) {
        super.showMessage(message);
        CoreUtils.makeText(getContext(), message);
    }

    @Override
    protected RecyclerView.LayoutManager createLayoutManager() {
        return new LinearLayoutManager(getActivity());
    }

    @Override
    protected OnItemClickListener createItemClickListener() {
        return new OnItemClickListener(false) {
            @Override
            public void SimpleOnItemClick(BaseQuickAdapter adapter, View view, int position) {
                if (Utils.isFastClick()) {
                    return;
                }

                MusicServiceConnection.playAll(adapter.getData(), position, false);
                EventBus.getDefault().post(EventBusTags.MAIN.MUSIC_PLAY);
            }

            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                if (Utils.isFastClick()) {
                    return;
                }

                switch (view.getId()) {
                    case R.id.popup_menu:
                        if (UserPrefHelper.isLogin()) {
                            onPopupMenuClick(view, (Song) adapter.getItem(position));
                        } else {
                            LoginDialogFragment.newInstance().show(getChildFragmentManager(), LoginDialogFragment.class.getName());
                        }
                        break;
                    default:
                        break;
                }
            }
        };
    }

    @Override
    protected RecyclerView.OnScrollListener addRecyclerViewScrollListener() {
        return null;
    }

    @Override
    protected RecyclerView.ItemDecoration createDecoration() {
        return null;
    }

    @Override
    protected RecyclerView.ItemAnimator createItemAnimator() {
        return null;
    }

    @Override
    protected View createRefreshHeader() {
        return new HaigeRefreshHeader(getActivity());
    }

    @Override
    protected Context createContext() {
        return getActivity();
    }

    @Override
    protected BaseQuickAdapter createAdapter() {
        return new MusicListAdapter(getActivity(), true);
    }

    @Subscriber
    public void onSubscriber(EventBusTags.MetaChangedEvent event) {
        if (getAdapter() != null) {
            getAdapter().notifyDataSetChanged();
        }
    }

    public void onPopupMenuClick(View v, Song song) {
        final PopupMenu menu = new PopupMenu(getContext(), v);
        menu.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.popup_song_add_to_queue:
                    MusicServiceConnection.addToQueue(song);
                    break;
                case R.id.popup_song_add_to_favorite:
                    mPresenter.recordFavoriteSong(song);
                    break;
                case R.id.popup_download:
                    if (ShareUtils.checkDownloadShare(UserPrefHelper.getDownloadMusicCount())) {
                        DownloadShareDialog.newInstance().show(getChildFragmentManager(), DownloadShareDialog.class.getName());
                    } else {
                        mPresenter.downloadMusic(song);
                    }
                    break;
            }
            return false;
        });
        menu.inflate(R.menu.popup_rank);
        menu.show();
    }

    public void refresh() {
        setStateToRefresh();
    }
}
