package com.music.player.haige.app.image.listener;

import android.graphics.Bitmap;
import android.text.TextUtils;

import com.facebook.cache.common.CacheKey;
import com.facebook.cache.common.SimpleCacheKey;
import com.facebook.imagepipeline.request.BasePostprocessor;

/**
 * 当Fresco理完之后，传递Bitmap调用者Bitmap
 * 这个Bitmap是对原图的一个拷贝，不会影响原图。
 * 可以选择性的进入缓存
 * <p>
 * Created by ZaKi on 2016/9/2.
 */
public abstract class OnPostProcessListener extends BasePostprocessor {
    protected final String uri;
    //一个Postprocessor对应一个CacheKey才能进入缓存
    protected final String tag;

    public OnPostProcessListener(String uri, String tag) {
        this.uri = uri;
        this.tag = tag;
    }

    @Override
    public String getName() {
        return "mico_PostProcesser";
    }

    @Override
    public final void process(Bitmap bitmap) {
        //传给子类，不能重写防止递归死循环
        post(bitmap);
    }

    @Override
    public CacheKey getPostprocessorCacheKey() {
        if (!hasCache()) {
            return null;
        }
        if (!TextUtils.isEmpty(tag)) {
            return new SimpleCacheKey("mico_cache_key_" + uri + "_" + tag);
        } else {
            return new SimpleCacheKey("mico_cache_key_" + uri + "_null");
        }
    }

    //是否要进入缓存
    public abstract boolean hasCache();

    //子类可以得到
    protected abstract void post(Bitmap bitmap);
}
