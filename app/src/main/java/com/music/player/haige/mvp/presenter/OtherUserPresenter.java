package com.music.player.haige.mvp.presenter;

import com.music.player.haige.mvp.contract.AppBasePresenter;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.Api;
import com.music.player.haige.mvp.model.entity.action.FollowActionRequest;
import com.music.player.haige.mvp.model.entity.user.UserBean;
import com.music.player.haige.mvp.model.entity.user.UserResponse;
import com.music.player.haige.mvp.ui.utils.Utils;

import javax.inject.Inject;

import me.jessyan.rxerrorhandler.core.RxErrorHandler;
import me.jessyan.rxerrorhandler.handler.ErrorHandleSubscriber;

public class OtherUserPresenter extends AppBasePresenter<MVContract.OtherProfileModel, MVContract.OtherProfileView> {

    @Inject
    RxErrorHandler mErrorHandler;

    @Inject
    public OtherUserPresenter(MVContract.OtherProfileModel model, MVContract.OtherProfileView view) {
        super(model, view);
    }

    public void getUserProfile(UserBean user) {
        if (Utils.isNull(user)) return;

        buildObservable(mModel.getOtherUser(user.getUserId()))
                .subscribe(new ErrorHandleSubscriber<UserResponse>(mErrorHandler) {
                    @Override
                    public void onNext(UserResponse response) {
                        if (response != null) {
                            mRootView.onSuccess(Api.Action.USER_PROFILE_OTHER, response.getData());
                        } else {
                            mRootView.onError(Api.Action.USER_PROFILE_OTHER, Api.State.NORMAL.getCode(), Api.State.NORMAL.getMsg());
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                        mRootView.onError(Api.Action.USER_PROFILE_OTHER, "-1", t.getMessage());
                    }
                });
    }

    public void follow(FollowActionRequest request) {
        buildObservable(mModel.follow(request), Api.Action.USER_FOLLOW);
    }

    public void unfollow(FollowActionRequest request) {
        buildObservable(mModel.unfollow(request), Api.Action.USER_UNFOLLOW);
    }
}
