package com.music.player.haige.mvp.ui.comment.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.music.player.haige.R;
import com.music.player.haige.app.base.pref.UserPrefHelper;
import com.music.player.haige.app.utils.launcher.ActivityLauncherStart;
import com.music.player.haige.app.utils.launcher.ActivityStartBase;
import com.music.player.haige.mvp.model.entity.comment.CommentBean;
import com.music.player.haige.mvp.model.entity.comment.CommentRequest;
import com.music.player.haige.mvp.model.entity.user.UserBean;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;
import com.music.player.haige.mvp.ui.utils.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SubCommentAdapter extends RecyclerView.Adapter<SubCommentAdapter.CommentViewHolder> {

    private Context mContext;
    private ArrayList<CommentBean> mCommentList = new ArrayList<>();
    private CommentAdapter.onCommentActionListener mActionListener;
    private onDeleteReplyListener mDeleteReplyListener;
    private String mCommentId;
    private int position = -1;
    private int replyCount = 0;
    private boolean isExpand = false;

    @Override
    public CommentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_sub_comment, parent, false);
        return new CommentViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CommentViewHolder holder, int position) {
        if (isExpand) {
            if (position == mCommentList.size()) {
                setCommentBottomView(holder, position);
            } else {
                setCommentView(holder, position);
            }
        } else {
            if (replyCount > 4 && position == 4) {
                setCommentBottomView(holder, position);
            } else {
                setCommentView(holder, position);
            }
        }
    }

    private void setCommentBottomView(CommentViewHolder holder, int position) {
        String content = isExpand ? ResourceUtils.resourceString(R.string.comment_reply_collapse_hint)
                : ResourceUtils.resourceString(R.string.comment_reply_all_hint, replyCount);
        SpannableString spannable = new SpannableString(content);
        spannable.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                if (Utils.isNotNull(mActionListener)) {
                    if (isExpand) {
                        isExpand = false;
                        notifyDataSetChanged();
                    } else if (mCommentList.size() > 4) {
                        isExpand = true;
                        notifyDataSetChanged();
                    } else {
                        isExpand = true;
                        mActionListener.onClickAllSub(mCommentId, position);
                    }
                }
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(ResourceUtils.getColor(R.color.color_5B92DC));
                ds.setUnderlineText(false); //去掉下划线
            }
        }, 0, content.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        holder.contentTv.setText(spannable);
        holder.contentTv.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private void setCommentView(CommentViewHolder holder, int position) {
        CommentBean comment = mCommentList.get(position);
        UserBean user = comment.getUserBean();

        if (Utils.isNotNull(comment) && Utils.isNotNull(user)) {
            String nickName = user.getNickName() + "：";
            String content = comment.getContext();
            SpannableString spannable = new SpannableString(nickName + content);
            spannable.setSpan(new ClickableSpan() {
                @Override
                public void onClick(View widget) {
                    if (!UserPrefHelper.isMe(user)) {
                        ActivityLauncherStart.startOtherUser(ActivityStartBase.scanForActivity(mContext), user);
                    }
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setColor(ResourceUtils.getColor(R.color.color_5B92DC));
                    ds.setUnderlineText(false); //去掉下划线
                }
            }, 0, nickName.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            holder.contentTv.setText(spannable);
            holder.contentTv.setOnClickListener(v -> {
                if (UserPrefHelper.isMe(user) && Utils.isNotNull(mDeleteReplyListener)) {
                    CommentRequest request = new CommentRequest();
                    request.setPosition(position);
                    request.setCommentId(comment.getId());
                    mDeleteReplyListener.onDelete(request);
                }
            });
            holder.contentTv.setMovementMethod(LinkMovementMethod.getInstance());
        }
    }

    @Override
    public int getItemCount() {
        if (isExpand) {
             return mCommentList.size() + 1;
        } else {
            return replyCount > 4 ? 5 : mCommentList.size();
        }
    }

    public void setCommentId(String commentId) {
        mCommentId = commentId;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setReplyCount(int replyCount) {
        this.replyCount = replyCount;
    }

    public void setCommentList(ArrayList<CommentBean> commentList) {
        if (Utils.isNotEmptyCollection(commentList)) {
            mCommentList.clear();
            mCommentList.addAll(commentList);
            notifyDataSetChanged();
        }
    }

    public void setActionListener(CommentAdapter.onCommentActionListener actionListener) {
        mActionListener = actionListener;
    }

    public void setDeleteReplyListener(onDeleteReplyListener deleteReplyListener) {
        mDeleteReplyListener = deleteReplyListener;
    }

    class CommentViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_reply)
        TextView contentTv;

        public CommentViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface onDeleteReplyListener {
        void onDelete(CommentRequest request);
    }
}
