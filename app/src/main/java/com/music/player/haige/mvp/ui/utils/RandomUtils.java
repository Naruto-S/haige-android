package com.music.player.haige.mvp.ui.utils;

import java.util.Random;

/**
 * Created by Naruto on 2018/7/19.
 */

public class RandomUtils {

    public static int getRangeInt(int min, int max) {
        Random random = new Random();
        return random.nextInt(max) % (max - min + 1) + min;
    }
}
