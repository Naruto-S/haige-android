package com.music.player.haige.app.base.pref;

import android.content.Context;
import android.content.SharedPreferences;

import com.music.player.haige.app.HaigeApplication;
import com.music.player.haige.mvp.ui.utils.Utils;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by liumingkong on 15/5/26.
 */
public class DevicePref {

    private static final String ACCOUNT_PREFERENCE = "ACCOUNT_PREFERENCE";

    private static SharedPreferences getPreference() {
        return HaigeApplication.getInstance().getSharedPreferences(ACCOUNT_PREFERENCE, Context.MODE_PRIVATE);
    }
//   TODO 跟设备的信息永不删除
//    public static void clearAll(Context context) {
//        SharedPreferences.Editor editor = getPreference().edit();
//        editor.clear();
//        editor.apply();
//    }

    protected static boolean contains(String key) {
        if (Utils.isEmptyString(key)) {
            return false;
        }
        return getPreference().contains(key);
    }

    public static void remove(String key) {
        SharedPreferences.Editor editor = getPreference().edit();
        editor.remove(key);
        editor.apply();
    }

    protected static int getInt(String tag, int defaultValue) {
        return getPreference().getInt(tag, defaultValue);
    }

    protected static void saveInt(String tag, int value) {
        SharedPreferences.Editor editor = getPreference().edit();
        editor.putInt(tag, value);
        editor.apply();
    }

    protected static long getLong(String tag, long defaultValue) {
        return getPreference().getLong(tag, defaultValue);
    }

    protected static void saveLong(String tag, long value) {
        SharedPreferences.Editor editor = getPreference().edit();
        editor.putLong(tag, value);
        editor.apply();
    }

    protected static float getFloat(String tag, float defaultValue) {
        return getPreference().getFloat(tag, defaultValue);
    }

    protected static void saveFloat(String tag, float value) {
        SharedPreferences.Editor editor = getPreference().edit();
        editor.putFloat(tag, value);
        editor.apply();
    }

    protected static boolean getBoolean(String tag, boolean defaultValue) {
        return getPreference().getBoolean(tag, defaultValue);
    }

    protected static void saveBoolean(String tag, boolean value) {
        SharedPreferences.Editor editor = getPreference().edit();
        editor.putBoolean(tag, value);
        editor.apply();
    }

    protected static String getString(String tag, String defaultValue) {
        return getPreference().getString(tag, defaultValue);
    }

    protected static void saveString(String tag, String value) {
        SharedPreferences.Editor editor = getPreference().edit();
        editor.putString(tag, value);
        editor.apply();
    }

    protected static Set<String> getStringSet(String tag) {
        return getPreference().getStringSet(tag, new HashSet<String>());
    }

    protected static void saveStringSet(String tag, Set<String> value) {
        SharedPreferences.Editor editor = getPreference().edit();
        editor.putStringSet(tag, value);
        editor.apply();
    }
}
