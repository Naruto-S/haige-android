package com.music.player.haige.mvp.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import com.music.player.haige.R;

/**
 * ================================================
 * Created by huangcong on 2018/3/27.
 * ================================================
 * <p>
 * 状态显示布局：无数据、服务器错误、无网络、正常
 */
public class StatusLayout extends FrameLayout implements View.OnClickListener{

    private View mEmptyView;
    private View mNetErrorView;
    private View mNetNoConnectView;

    private OnStatusClickListener mOnStatusClickListener;

    public enum STATUS {
        EMPTY,
        NET_ERROR,
        NET_NO_CONNECT,
        OK,
    }

    public StatusLayout(Context context) {
        this(context, null);
    }

    public StatusLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public StatusLayout(Context context, AttributeSet attributeSet, int defStyleAttr) {
        super(context, attributeSet, defStyleAttr);
        LayoutInflater.from(context).inflate(R.layout.app_layout_status, this, true);
        mEmptyView = findViewById(R.id.app_layout_empty_view);
        mNetErrorView = findViewById(R.id.app_layout_net_error_view);
        mNetNoConnectView = findViewById(R.id.app_layout_net_no_connect_view);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.app_layout_empty_btn:
                if (mOnStatusClickListener != null) {
                    mOnStatusClickListener.onEmptyClick();
                }
                break;
            case R.id.app_layout_net_error_btn:
            case R.id.app_layout_net_no_connect_btn:
                if (mOnStatusClickListener != null) {
                    mOnStatusClickListener.onReloadClick();
                }
                break;
        }
    }

    public void setOnStatusClickListener(OnStatusClickListener listener){
        mOnStatusClickListener = listener;
    }

    /**
     * 显示不同的状态
     *
     * @param status
     */
    public void showStatusView(STATUS status) {
        switch (status) {
            case EMPTY:
                mEmptyView.setVisibility(View.VISIBLE);
                mNetErrorView.setVisibility(View.GONE);
                mNetNoConnectView.setVisibility(View.GONE);
                findViewById(R.id.app_layout_empty_btn).setOnClickListener(this);
                break;
            case NET_ERROR:
                mEmptyView.setVisibility(View.GONE);
                mNetErrorView.setVisibility(View.VISIBLE);
                findViewById(R.id.app_layout_net_error_btn).setOnClickListener(this);
                mNetNoConnectView.setVisibility(View.GONE);
                break;
            case NET_NO_CONNECT:
                mEmptyView.setVisibility(View.GONE);
                mNetErrorView.setVisibility(View.GONE);
                mNetNoConnectView.setVisibility(View.VISIBLE);
                findViewById(R.id.app_layout_net_no_connect_btn).setOnClickListener(this);
                break;
            case OK:
                mEmptyView.setVisibility(View.GONE);
                mNetErrorView.setVisibility(View.GONE);
                mNetNoConnectView.setVisibility(View.GONE);
                break;
        }
    }

    public interface OnStatusClickListener{
        void onEmptyClick();
        void onReloadClick();
    }

}
