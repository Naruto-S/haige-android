package com.music.player.haige.mvp.ui.main.adapter;

import android.content.Context;
import android.support.design.widget.CheckableImageButton;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.music.player.haige.R;
import com.music.player.haige.app.base.recyclerview.BaseQuickAdapter;
import com.music.player.haige.app.base.recyclerview.BaseViewHolder;
import com.music.player.haige.app.image.loader.AvatarLoader;
import com.music.player.haige.app.image.widget.HaigeImageView;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.model.utils.CommonUtils;
import com.music.player.haige.mvp.ui.main.utils.MainViewUtils;
import com.music.player.haige.mvp.ui.main.widget.RecommendPlayView;
import com.music.player.haige.mvp.ui.main.widget.likebutton.LikeButton;
import com.music.player.haige.mvp.ui.main.widget.likebutton.OnLikeListener;
import com.music.player.haige.mvp.ui.music.service.MusicServiceConnection;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;
import com.music.player.haige.mvp.ui.utils.Utils;
import com.music.player.haige.mvp.ui.widget.playpause.PlayPauseView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Naruto on 2018/5/12.
 */

public class RecommendAdapter extends BaseQuickAdapter<Song, RecommendAdapter.RecommendViewHolder> {

    private View.OnClickListener mClickListener;
    private View.OnTouchListener mTouchListener;
    private OnLikeListener mLikeListener;

    public RecommendAdapter(Context context) {
        super(context, R.layout.item_fragment_recommend, null);
    }

    @Override
    public RecommendViewHolder onCreateDefViewHolder(ViewGroup parent, int viewType) {
        return new RecommendViewHolder(mLayoutInflater.inflate(mLayoutResId, parent, false));
    }

    @Override
    protected void convert(RecommendViewHolder holder, Song song) {
        holder.mMusicLyricTv.setText("");
        holder.mMusicLyricTv.setVisibility(View.GONE);

        int position = holder.getPosition() - getHeaderLayoutCount();

        if (Utils.isNotNull(song)) {
            if (MusicServiceConnection.isPlaying()) {
                holder.mRecommendPlayView.play();
                holder.mRecommendPlayView.setVisibility(View.GONE);
            } else {
                holder.mRecommendPlayView.pause();
                holder.mRecommendPlayView.setVisibility(View.VISIBLE);
            }

            MainViewUtils.updatePlayWebpBg(song.getBgImgWebp(), holder.mMusicWebpBgSdv);

            if (Utils.isNotNull(song.getUser())) {
                AvatarLoader.loadUserAvatar(song.getUser().getAvatarUrl(),
                        R.drawable.ic_main_default_avatar,
                        R.drawable.ic_main_default_avatar,
                        holder.mOperateCoverIv);
            }

            if (song.getTags() != null && song.getTags().size() > 0) {
                holder.mMusicTagTv.setVisibility(View.VISIBLE);
                holder.mMusicTagTv.setText(ResourceUtils.resourceString(R.string.app_music_tag, song.getTags().get(0)));
            } else {
                holder.mMusicTagTv.setVisibility(View.GONE);
            }
            holder.mMusicNameTv.setText(song.getMusicName());
            holder.mArtistNameTv.setText(ResourceUtils.resourceString(R.string.app_artist_name, song.getArtistName(mContext)));

            holder.mLikeCountIv.setLiked(song.isUserLiked);
            holder.mLikeCountIv.setOnLikeListener(mLikeListener);
            holder.mLikeCountTv.setText(CommonUtils.makeLikeCount(mContext, song.getLikeCount()));
            holder.mShareCountIv.setOnClickListener(mClickListener);
            holder.mInfoPreIv.setOnClickListener(mClickListener);
            holder.mPlayPauseView.setOnClickListener(mClickListener);
            holder.mInfoNextIv.setOnClickListener(mClickListener);
            holder.mOperateCoverIv.setOnClickListener(mClickListener);
            holder.mCommentIv.setOnClickListener(mClickListener);
            holder.mMusicTagTv.setOnClickListener(mClickListener);
            holder.mArtistNameTv.setOnClickListener(mClickListener);

            holder.mLikeViewGroup.setOnTouchListener(mTouchListener);

            long duration = MusicServiceConnection.duration();
            holder.mDurationSeekBar.setMax((int) duration);
            holder.mDurationTv.setText(CommonUtils.makeShortTimeString(mContext, duration));
        }
    }

    public void setLikeListener(OnLikeListener likeListener) {
        mLikeListener = likeListener;
    }

    public void setViewClickListener(View.OnClickListener clickListener) {
        mClickListener = clickListener;
    }

    public void setViewTouchListener(View.OnTouchListener touchListener) {
        mTouchListener = touchListener;
    }

    class RecommendViewHolder extends BaseViewHolder {

        @BindView(R.id.view_group_like)
        ViewGroup mLikeViewGroup;

        @BindView(R.id.hiv_music_webp_bg)
        HaigeImageView mMusicWebpBgSdv;

        @BindView(R.id.app_fragment_recommend_music_info_vp)
        RecommendPlayView mRecommendPlayView;

        @BindView(R.id.app_layout_music_info_duration_cur_tv)
        TextView mDurationCurTv;

        @BindView(R.id.app_layout_music_info_duration_sb)
        SeekBar mDurationSeekBar;

        @BindView(R.id.app_layout_music_info_duration_tv)
        TextView mDurationTv;

        @BindView(R.id.app_layout_music_operate_cover_iv)
        HaigeImageView mOperateCoverIv;

        @BindView(R.id.app_layout_music_other_info_tag_tv)
        TextView mMusicTagTv;

        @BindView(R.id.app_layout_music_other_info_artist_tv)
        TextView mArtistNameTv;

        @BindView(R.id.app_layout_music_other_info_music_name_tv)
        TextView mMusicNameTv;

        @BindView(R.id.app_layout_music_operate_like_iv)
        LikeButton mLikeCountIv;

        @BindView(R.id.app_layout_music_operate_like_count_tv)
        TextView mLikeCountTv;

        @BindView(R.id.app_layout_music_operate_share_iv)
        CheckableImageButton mShareCountIv;

        @BindView(R.id.app_layout_music_info_previous_view)
        ImageView mInfoPreIv;

        @BindView(R.id.app_layout_music_info_play_pause_view)
        PlayPauseView mPlayPauseView;

        @BindView(R.id.app_layout_music_info_next_view)
        ImageView mInfoNextIv;

        @BindView(R.id.app_layout_music_operate_danmu_iv)
        CheckableImageButton mCommentIv;

        @BindView(R.id.tv_music_lyric)
        TextView mMusicLyricTv;

        public RecommendViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
