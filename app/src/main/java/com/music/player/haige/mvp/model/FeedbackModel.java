package com.music.player.haige.mvp.model;

import com.hc.base.mvp.BaseModel;
import com.hc.core.integration.IRepositoryManager;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.service.ApiService;
import com.music.player.haige.mvp.model.entity.BaseResponse;
import com.music.player.haige.mvp.model.entity.action.SendFeedbackRequest;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * ================================================
 * Created by huangcong on 2018/2/27.
 * ================================================
 */

public class FeedbackModel extends BaseModel implements MVContract.FeedbackModel {

    @Inject
    public FeedbackModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }


    @Override
    public Observable<BaseResponse> sendFeedback(SendFeedbackRequest request) {
        return Observable.just(mRepositoryManager.obtainRetrofitService(ApiService.class).sendFeedback(request))
                .flatMap(observable -> observable);
    }
}
