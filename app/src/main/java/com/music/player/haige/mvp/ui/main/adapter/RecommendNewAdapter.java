package com.music.player.haige.mvp.ui.main.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.music.player.haige.R;
import com.music.player.haige.app.image.loader.AvatarLoader;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.ui.main.holder.RecommendHolder;
import com.music.player.haige.mvp.ui.main.utils.MainViewUtils;
import com.music.player.haige.mvp.ui.music.service.MusicServiceConnection;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;
import com.music.player.haige.mvp.ui.utils.Utils;

import java.util.ArrayList;

public class RecommendNewAdapter extends BaseAdapter {

    private View.OnClickListener mClickListener;

    private ArrayList<Song> mSongs = new ArrayList<>();

    @Override
    public int getCount() {
        return mSongs.size();
    }

    @Override
    public Song getItem(int position) {
        if (mSongs == null || mSongs.size() == 0) return null;
        return mSongs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        RecommendHolder holder;
        Song song = getItem(position);
        if (Utils.isNotNull(song)) {
            if (convertView == null) {
                convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recommend_new, parent, false);
                holder = new RecommendHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (RecommendHolder) convertView.getTag();
            }

            holder.mAnimlikeIv.setAlpha(0f);
            holder.mAnimDislikeIv.setAlpha(0f);
            holder.mProgressBar.setProgress(0);
            holder.mMusicLyricTv.setText("");

            MainViewUtils.updatePlayWebpBg(song.getBgImgWebp(), holder.mMusicWebpBgSdv);

            if (MusicServiceConnection.isPlaying() && MusicServiceConnection.getCurrentAudioId() == song.songId) {
                holder.mPlayIv.setImageResource(R.drawable.ic_recommend_suspend);
            } else {
                holder.mPlayIv.setImageResource(R.drawable.ic_recommend_play);
            }

            if (Utils.isNotNull(song.getUser())) {
                AvatarLoader.loadUserAvatar(song.getUser().getAvatarUrl(),
                        R.drawable.ic_main_default_avatar,
                        R.drawable.ic_main_default_avatar,
                        holder.mAvatarTv);
            }

            if (song.getTags() != null && song.getTags().size() > 0) {
                holder.mRecommendTagTv.setVisibility(View.VISIBLE);
                holder.mRecommendTagTv.setText(ResourceUtils.resourceString(R.string.app_music_tag, song.getTags().get(0)));
            } else {
                holder.mRecommendTagTv.setVisibility(View.GONE);
            }


            holder.mMusicNameTv.setText(song.getMusicName());
            holder.mMusicAuthorTv.setText(ResourceUtils.resourceString(R.string.app_artist_name, song.getArtistName(parent.getContext())));

            holder.mRecommendLikeTv.setText(ResourceUtils.resourceString(R.string.recommend_like, song.getLikeCount()));
            holder.mRecommendCommentTv.setText(ResourceUtils.resourceString(R.string.recommend_comment, song.getCommentCount()));

            long duration = MusicServiceConnection.duration();
            holder.mProgressBar.setMax((int) duration);

            holder.mPlayIv.setOnClickListener(mClickListener);
        }

        return convertView;
    }

    public void setViewClickListener(View.OnClickListener clickListener) {
        mClickListener = clickListener;
    }

    public void setSongs(ArrayList<Song> songs) {
        mSongs.clear();
        mSongs.addAll(songs);
        notifyDataSetChanged();
    }

    public void addSongs(ArrayList<Song> songs) {
        mSongs.addAll(songs);
        notifyDataSetChanged();
    }

    public ArrayList<Song> getSongs() {
        return mSongs;
    }

    public void clear() {
        mSongs.clear();
        notifyDataSetInvalidated();
    }

    public void remove(int index) {
        if (index > -1) {
            if (index < mSongs.size()) {
                mSongs.remove(index);
            }

            notifyDataSetChanged();
        }
    }

    public boolean isEmpty() {
        return Utils.isEmptyCollection(mSongs);
    }
}
