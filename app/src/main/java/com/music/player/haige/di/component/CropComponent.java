package com.music.player.haige.di.component;

import com.hc.core.di.component.AppComponent;
import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.di.module.CropModule;
import com.music.player.haige.mvp.ui.user.CropFragment;

import dagger.Component;

/**
 * Created by Naruto on 2018/5/13.
 */
@FragmentScope
@Component(modules = {CropModule.class}, dependencies = AppComponent.class)
public interface CropComponent {

    void inject(CropFragment fragment);
}
