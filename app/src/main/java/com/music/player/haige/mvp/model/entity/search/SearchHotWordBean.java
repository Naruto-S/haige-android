package com.music.player.haige.mvp.model.entity.search;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * ================================
 * Created by huangcong on 2018/3/10.
 * ================================
 */

public class SearchHotWordBean implements Serializable {

    @SerializedName("tag_id")
    int tagId;

    @SerializedName("tag_name")
    String tagName;

    public int getTagId() {
        return tagId;
    }

    public void setTagId(int tagId) {
        this.tagId = tagId;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }
}
