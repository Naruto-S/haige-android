package com.music.player.haige.di.component;

import com.hc.core.di.component.AppComponent;
import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.di.module.PlayQueueModule;
import com.music.player.haige.mvp.ui.main.fragment.PlayQueueFragment;

import dagger.Component;

/**
 * ================================================
 * Created by huangcong on 2018/3/3.
 * ================================================
 */
@FragmentScope
@Component(modules = {PlayQueueModule.class}, dependencies = AppComponent.class)
public interface PlayQueueComponent {

    void inject(PlayQueueFragment fragment);
}
