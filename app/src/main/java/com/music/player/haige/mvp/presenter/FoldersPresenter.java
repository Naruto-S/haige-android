package com.music.player.haige.mvp.presenter;

import com.hc.base.mvp.BasePresenter;
import com.hc.core.utils.RxLifecycleUtils;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.entity.music.FolderInfo;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;
import me.jessyan.rxerrorhandler.handler.ErrorHandleSubscriber;

/**
 * ================================================
 * Created by huangcong on 2018/3/2.
 * ================================================
 */

public class FoldersPresenter extends BasePresenter<MVContract.FoldersModel, MVContract.FoldersView> {

    @Inject
    RxErrorHandler mErrorHandler;

    @Inject
    public FoldersPresenter(MVContract.FoldersModel model, MVContract.FoldersView view) {
        super(model, view);
    }

    public void loadFolders() {
        mModel.loadFolders()
                .getFolderList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(Disposable disposable) throws Exception {
                        mRootView.showLoading();
                    }
                })
                .doFinally(new Action() {
                    @Override
                    public void run() throws Exception {
                        mRootView.hideLoading();
                    }
                })
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .subscribe(new ErrorHandleSubscriber<List<FolderInfo>>(mErrorHandler) {
                    @Override
                    public void onNext(List<FolderInfo> folderInfos) {
                        if (folderInfos == null || folderInfos.size() == 0) {
                            mRootView.showEmptyView();
                        } else {
                            mRootView.showFolders(folderInfos);
                        }
                    }
                });
    }
}
