package com.music.player.haige.di.module;

import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.OtherProfileModel;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Naruto on 2018/5/13.
 */
@Module
public class OtherProfileModule {
    private MVContract.OtherProfileView view;

    public OtherProfileModule(MVContract.OtherProfileView view){
        this.view = view;
    }

    @FragmentScope
    @Provides
    MVContract.OtherProfileView provideView(){
        return view;
    }

    @FragmentScope
    @Provides
    MVContract.OtherProfileModel provideModel(OtherProfileModel model){
        return model;
    }
}
