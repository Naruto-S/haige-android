package com.music.player.haige.mvp.ui.login;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.hc.core.di.component.AppComponent;
import com.music.player.haige.R;
import com.music.player.haige.app.EventBusTags;
import com.music.player.haige.app.base.AppBaseDialogFragment;
import com.music.player.haige.app.base.pref.UserPrefHelper;
import com.music.player.haige.app.utils.statistics.CommonStatistics;
import com.music.player.haige.app.utils.statistics.StatisticsConstant;
import com.music.player.haige.di.component.DaggerLoginDialogComponent;
import com.music.player.haige.di.module.LoginDialogModule;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.Api;
import com.music.player.haige.mvp.model.entity.user.UserBean;
import com.music.player.haige.mvp.presenter.LoginFPresenter;
import com.music.player.haige.mvp.ui.utils.DebugLogger;
import com.music.player.haige.mvp.ui.utils.Utils;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.bean.SHARE_MEDIA;

import org.simple.eventbus.EventBus;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Naruto on 2018/5/5.
 */

public class LoginDialogFragment extends AppBaseDialogFragment<LoginFPresenter> implements MVContract.LoginView {

    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;

    private OnLoginListener mLoginListener;

    public static LoginDialogFragment newInstance() {
        LoginDialogFragment fragment = new LoginDialogFragment();
        return fragment;
    }

    public LoginDialogFragment setLoginListener(OnLoginListener listener) {
        mLoginListener = listener;
        return this;
    }

    @Override
    public void onStart() {
        super.onStart();

        Dialog dialog = getDialog();
        dialog.setCanceledOnTouchOutside(true);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams params = window.getAttributes();
        params.gravity = Gravity.BOTTOM;
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(params);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }


    @Override
    public void setupFragmentComponent(AppComponent component) {
        DaggerLoginDialogComponent
                .builder()
                .appComponent(component)
                .loginDialogModule(new LoginDialogModule(this))
                .build()
                .inject(this);
    }

    @Override
    public View initView(LayoutInflater inflater, ViewGroup group, Bundle bundle) {
        return inflater.inflate(R.layout.app_dialog_login, group, false);
    }

    @Override
    public void initData(Bundle bundle) {
        CommonStatistics.sendEvent(StatisticsConstant.LOGIN_WINDOW_APPEAR);
    }

    @Override
    public void onSuccess(Api.Action action, Object o) {
        super.onSuccess(action, o);
        switch (action) {
            case USER_LOGIN:
                UserBean user = (UserBean) o;
                UserPrefHelper.putMeUser(user);
                UserPrefHelper.putIsLogin(true);

                EventBus.getDefault().post(EventBusTags.UI.UPDATE_USER_UI);
                DebugLogger.e(TAG, "Login onSuccess：" + user);
                onClose();
                break;
            case USER_PROFILE:
                break;
        }
    }

    @Override
    public void showProgressBar() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        if (Utils.isNotNull(mLoginListener)) {
            mLoginListener.onDismiss();
        }
        super.onDismiss(dialog);
    }

    @OnClick(R.id.layout_qq_login)
    public void onQQLogin() {
        if (UMShareAPI.get(getContext()).isInstall(getActivity(), SHARE_MEDIA.QQ)) {
            CommonStatistics.sendEvent(StatisticsConstant.LOGIN_QQ_CLICK);
            mPresenter.onQQLogin(getActivity());
        } else {
            Toast.makeText(getContext(), "未安装QQ", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.layout_weixin_login)
    public void onWeiXinLogin() {
        if (UMShareAPI.get(getContext()).isInstall(getActivity(), SHARE_MEDIA.WEIXIN)) {
            CommonStatistics.sendEvent(StatisticsConstant.LOGIN_WECHAT_CLICK);
            mPresenter.onWeiXinLogin(getActivity());
        } else {
            Toast.makeText(getContext(), "未安装微信", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.layout_sina_login)
    public void onSinaLogin() {
        if (UMShareAPI.get(getContext()).isInstall(getActivity(), SHARE_MEDIA.SINA)) {
            mPresenter.onSinaLogin(getActivity());
        } else {
            Toast.makeText(getContext(), "未安装新浪微博", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.tv_close)
    public void onClose() {
        if (Utils.isNotNull(getDialog())) {
            getDialog().dismiss();
        }
    }

    public interface OnLoginListener {

        void onDismiss();

    }
}
