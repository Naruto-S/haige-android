package com.music.player.haige.app.utils.statistics;

import android.content.Context;

import java.util.Map;

public abstract class CommonStatistics {

    public static void sendEvent(String eventName) {
        getStatistics().onEvent(eventName);
        getAnalyticsStatistics().onEvent(eventName);
    }

    public static void sendEvent(String eventName, Map<String, Object> value) {
        getStatistics().onEvent(eventName, value);
        getAnalyticsStatistics().onEvent(eventName, value);
    }

    public static CommonStatistics getStatistics() {
        return AppsflyerUtils.getInstance();
    }

    public static CommonStatistics getAnalyticsStatistics() {
        return AnalyticsUtils.getInstance();
    }

    public abstract void onEvent(String eventName);

    public abstract void onEvent(String eventName, String value);

    public abstract void onEvent(String eventName, Map<String, Object> value);

    public abstract void onResume(Context context);

    public abstract void onPause(Context context);

    public abstract void onPageStart(String value);

    public abstract void onPageEnd(String value);


}
