package com.music.player.haige.mvp.model;

import com.hc.base.mvp.BaseModel;
import com.hc.core.integration.IRepositoryManager;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.service.ApiService;
import com.music.player.haige.mvp.model.entity.BaseResponse;
import com.music.player.haige.mvp.model.entity.user.ModifyUserRequest;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by Naruto on 2018/5/13.
 */

public class EditProfileModel extends BaseModel implements MVContract.EditProfileModel {

    @Inject
    public EditProfileModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public Observable<BaseResponse> modifyUserProfile(ModifyUserRequest request) {
        return Observable.just(mRepositoryManager.obtainRetrofitService(ApiService.class).modifyUserProfile(request))
                .flatMap(observable -> observable);
    }
}
