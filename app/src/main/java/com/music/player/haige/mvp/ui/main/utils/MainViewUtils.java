package com.music.player.haige.mvp.ui.main.utils;

import android.graphics.drawable.Animatable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.imagepipeline.image.ImageInfo;
import com.music.player.haige.R;
import com.music.player.haige.app.base.pref.DevicePrefHelper;
import com.music.player.haige.app.image.loader.HaigeImageLoader;
import com.music.player.haige.app.image.loader.LocalImageLoader;
import com.music.player.haige.app.image.loader.listener.OnImageLoaderListener;
import com.music.player.haige.app.image.release.DisplayOptions;
import com.music.player.haige.app.image.widget.HaigeImageView;
import com.music.player.haige.mvp.ui.utils.DebugLogger;
import com.music.player.haige.mvp.ui.utils.RandomUtils;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;
import com.music.player.haige.mvp.ui.utils.Utils;

import java.util.ArrayList;

/**
 * Created by Naruto on 2018/5/13.
 */

public class MainViewUtils {

    public static String getPlayWebpBgName(ArrayList<String> list, int position) {
        try {
            return list.get(position);
        } catch (Exception e){
            return "";
        }
    }

    public static void updatePlayWebpBg(String url, HaigeImageView imageView) {
        if (Utils.isNull(imageView) || (Utils.isNotNull(imageView.getTag()) && imageView.getTag().equals(url))) {
            return;
        }

        DisplayOptions.Builder displayOptions = new DisplayOptions.Builder()
                .setAutoPlayAnimations(false)
                .showImageOnLoading(R.drawable.bg_recommend_default)
                .showImageOnFail(R.drawable.bg_recommend_default);

        DebugLogger.e("MainViewUtils", "Webp -- Song url : " + url);

        OnImageLoaderListener onImageLoaderListener = new OnImageLoaderListener() {
            @Override
            public void onImageLoadComplete(String uri, ImageInfo imageInfo, boolean isGif, Animatable animatable, View tagVview) {
                imageView.setTag(url);
            }

            @Override
            public void onImageLoadFail(String uri, Throwable throwable, View view) {
                setPlayWebpDefault(imageView, displayOptions);
            }
        };

        if (Utils.isEmptyString(url)) {
            ArrayList<String> urls = DevicePrefHelper.getMusicBg();
            if (Utils.isNotEmptyCollection(urls)) {
                String randomUrl = urls.get(RandomUtils.getRangeInt(0, urls.size() - 1));
                if (Utils.isEmptyString(randomUrl)) {
                    setPlayWebpDefault(imageView, displayOptions);
                } else {
                    DebugLogger.e("MainViewUtils", "Webp -- Random url : " + randomUrl);
                    HaigeImageLoader.loadMusicCover(randomUrl, displayOptions, imageView, onImageLoaderListener);
                }
            } else {
                setPlayWebpDefault(imageView, displayOptions);
            }
        } else {
            HaigeImageLoader.loadMusicCover(url, displayOptions, imageView, onImageLoaderListener);
        }
    }

    public static void setPlayWebpDefault(HaigeImageView imageView, DisplayOptions.Builder displayOptions) {
        DebugLogger.e("MainViewUtils", "Webp -- Default");

        OnImageLoaderListener onImageLoaderListener = new OnImageLoaderListener() {
            @Override
            public void onImageLoadComplete(String uri, ImageInfo imageInfo, boolean isGif, Animatable animatable, View tagVview) {

            }

            @Override
            public void onImageLoadFail(String uri, Throwable throwable, View view) {
            }
        };

        LocalImageLoader.displayResImage(R.drawable.default_webp, displayOptions, imageView, onImageLoaderListener);
    }

    public static void startWebpAnim(Animatable animatable) {
        if (animatable != null && !animatable.isRunning()) {
            animatable.start();
        }
    }

    public static void stopWebpAnim(Animatable animatable) {
        if (animatable != null && !animatable.isRunning()) {
            animatable.stop();
        }
    }

    public static void showSwitchGuide(View view, ImageView imageView, TextView textView) {
        imageView.setImageResource(R.drawable.img_guide_switch);
        textView.setText(ResourceUtils.resourceString(R.string.guide_switch_text));
        view.setVisibility(View.VISIBLE);
        view.setOnClickListener(v -> {
            view.setVisibility(View.GONE);
        });
    }

    public static void showLikeGuide(View view, ImageView imageView, TextView textView) {
        imageView.setImageResource(R.drawable.img_guide_like);
        textView.setText(ResourceUtils.resourceString(R.string.guide_like_text));
        view.setVisibility(View.VISIBLE);
        view.setOnClickListener(v -> {
            view.setVisibility(View.GONE);
        });
    }
}
