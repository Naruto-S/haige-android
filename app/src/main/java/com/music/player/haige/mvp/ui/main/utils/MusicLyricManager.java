package com.music.player.haige.mvp.ui.main.utils;

import com.music.player.haige.mvp.ui.utils.Utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MusicLyricManager {

    public static final String TAG = MusicLyricManager.class.getSimpleName();

    private static MusicLyricManager mHelper;

    private LyricInfo mLyricInfo;
    private String mDefaultHint = "暂无歌词";

    public static MusicLyricManager getInstance() {
        if (mHelper == null) {
            synchronized (MusicLyricManager.class) {
                if (mHelper == null) {
                    mHelper = new MusicLyricManager();
                }
            }
        }
        return mHelper;
    }

    /**
     * 设置歌词文件
     *
     * @param file        歌词文件
     * @param charsetName 解析字符集
     */
    public void setLyricFile(File file, String charsetName) {
        if (file != null && file.exists()) {
            try {
                setupLyricResource(new FileInputStream(file), charsetName);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        } else {
            mDefaultHint = "暂无歌词";
        }
    }

    /**
     * 初始化歌词信息
     *
     * @param inputStream 歌词文件的流信息
     */
    private void setupLyricResource(InputStream inputStream, String charsetName) {
        if (inputStream != null) {
            try {
                LyricInfo lyricInfo = new LyricInfo();
                lyricInfo.songLines = new ArrayList<>();
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream, charsetName);
                BufferedReader reader = new BufferedReader(inputStreamReader);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    analyzeLyric(lyricInfo, line);
                }
                //歌词排序
                Collections.sort(lyricInfo.songLines, new sort());
                reader.close();
                inputStream.close();
                inputStreamReader.close();

                mLyricInfo = lyricInfo;
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            mDefaultHint = "暂无歌词";
        }
    }

    /**
     * 逐行解析歌词内容
     */
    private void analyzeLyric(LyricInfo lyricInfo, String line) {
        int index = line.indexOf("]");
        if (line != null && line.startsWith("[offset:")) {
            // 时间偏移量
            String string = line.substring(8, index).trim();
            lyricInfo.songOffset = Long.parseLong(string);
            return;
        }
        if (line != null && line.startsWith("[ti:")) {
            // title 标题
            String string = line.substring(4, index).trim();
            lyricInfo.songTitle = string;
            return;
        }
        if (line != null && line.startsWith("[ar:")) {
            // artistName 作者
            String string = line.substring(4, index).trim();
            lyricInfo.songArtist = string;
            return;
        }
        if (line != null && line.startsWith("[al:")) {
            // album 所属专辑
            String string = line.substring(4, index).trim();
            lyricInfo.songAlbum = string;
            return;
        }
        if (line != null && line.startsWith("[by:")) {
            return;
        }
        if (line != null && index == 9 && line.trim().length() > 10) {
            // 歌词内容,需要考虑一行歌词有多个时间戳的情况
            int lastIndexOfRightBracket = line.lastIndexOf("]");
            String content = line.substring(lastIndexOfRightBracket + 1, line.length());

            String times = line.substring(0, lastIndexOfRightBracket + 1).replace("[", "-").replace("]", "-");
            String arrTimes[] = times.split("-");
            for (String temp : arrTimes) {
                if (temp.trim().length() == 0) {
                    continue;
                }
                /** [02:34.14][01:07.00]当你我不小心又想起她
                 *
                 上面的歌词的就可以拆分为下面两句歌词了
                 [02:34.14]当你我不小心又想起她
                 [01:07.00]当你我不小心又想起她
                 */
                LineInfo lineInfo = new LineInfo();
                lineInfo.content = content;
                lineInfo.start = measureStartTimeMillis(temp);
                lyricInfo.songLines.add(lineInfo);
            }
        }
    }

    /**
     * 将解析得到的表示时间的字符转化为Long型
     */
    private static long measureStartTimeMillis(String timeString) {
        //因为给如的字符串的时间格式为XX:XX.XX,返回的long要求是以毫秒为单位
        //将字符串 XX:XX.XX 转换为 XX:XX:XX
        timeString = timeString.replace('.', ':');
        //将字符串 XX:XX:XX 拆分
        String[] times = timeString.split(":");
        // mm:ss:SS
        return Integer.valueOf(times[0]) * 60 * 1000 +//分
                Integer.valueOf(times[1]) * 1000 +//秒
                Integer.valueOf(times[2]);//毫秒
    }

    public String getMusicLyricHint(long time) {
        if (Utils.isNotNull(mLyricInfo) && Utils.isNotEmptyCollection(mLyricInfo.songLines)) {
            int position = 0;
            for (int i = 0; i < mLyricInfo.songLines.size(); i++) {
                LineInfo lineInfo = mLyricInfo.songLines.get(i);
                if (Utils.isNotNull(lineInfo) && lineInfo.start > time) {
                    position = i - 1;
                    position = position < 0 ? 0 : position;
                    break;
                } else if (i ==  mLyricInfo.songLines.size() - 1 && time > lineInfo.start) {
                    position = i;
                    break;
                }
            }
            return mLyricInfo.songLines.get(position).content;
        } else {
            return "";
        }
    }

    /**
     * 重置歌词内容
     */
    public void clear() {
        if (mLyricInfo != null) {
            if (mLyricInfo.songLines != null) {
                mLyricInfo.songLines.clear();
                mLyricInfo.songLines = null;
            }
            mLyricInfo = null;
        }
    }


    class LyricInfo {
        List<LineInfo> songLines;

        String songArtist;  // 歌手
        String songTitle;  // 标题
        String songAlbum;  // 专辑

        long songOffset;  // 偏移量

        @Override
        public String toString() {
            return "LyricInfo{" +
                    "songLines=" + songLines +
                    ", songArtist='" + songArtist + '\'' +
                    ", songTitle='" + songTitle + '\'' +
                    ", songAlbum='" + songAlbum + '\'' +
                    ", songOffset=" + songOffset +
                    '}';
        }
    }

    class LineInfo {
        String content;  // 歌词内容
        long start;  // 开始时间

        @Override
        public String toString() {
            return "LineInfo{" +
                    "content='" + content + '\'' +
                    ", start=" + start +
                    '}';
        }
    }

    class sort implements Comparator<LineInfo> {

        @Override
        public int compare(LineInfo lrc, LineInfo lrc2) {
            if (lrc.start < lrc2.start) {
                return -1;
            } else if ((lrc.start > lrc2.start)) {
                return 1;
            } else {
                return 0;
            }
        }
    }
}
