package com.music.player.haige.app.web;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;

import com.music.player.haige.mvp.ui.utils.Utils;

import java.util.HashSet;

/**
 * Created by liumingkong on 16/9/9.
 */
public class WebLinkUtils {

    private static HashSet<String> hideMenus = new HashSet<>();

    static boolean isHideMenus(String url) {
        return !Utils.isEmptyString(url) && hideMenus.contains(url);
    }

    public static void saveHideMenu(String url) {
        if (!Utils.isEmptyString(url)) {
            hideMenus.add(url);
        }
    }

    // 判断渠道, 只有uc和91才弹
    // Google Play, UC, 91
    public static String getChannel(Context context) {
        ApplicationInfo appInfo = null;
        try {
            appInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
            return appInfo.metaData.getString("UMENG_CHANNEL_N");
        } catch (PackageManager.NameNotFoundException e) {
        }
        return "Google Play";
    }
}
