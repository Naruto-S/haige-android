package com.music.player.haige.mvp.model.entity.comment;

import com.google.gson.annotations.SerializedName;
import com.music.player.haige.mvp.model.entity.user.UserBean;

import java.io.Serializable;
import java.util.ArrayList;

public class CommentBean implements Serializable {

    /**
     * id : 5b100ab22e3a010c0e037837
     * music_id : 288384
     * post_at : 8
     * context : Hchcgh
     * created_at : 1527777970
     * status : 1
     */

    @SerializedName("id")
    private String id;
    @SerializedName("music_id")
    private long musicId;
    @SerializedName("post_at")
    private long postAt;
    @SerializedName("context")
    private String context;
    @SerializedName("created_at")
    private long createdAt;
    @SerializedName("status")
    private int status;
    @SerializedName("user")
    UserBean mUserBean;
    @SerializedName("is_liked")
    private boolean isLiked;
    @SerializedName("liked")
    private int liked;
    @SerializedName("sub_count")
    private int subCount;
    @SerializedName("sub_comment")
    private ArrayList<CommentBean> subComments;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getMusicId() {
        return musicId;
    }

    public void setMusicId(long musicId) {
        this.musicId = musicId;
    }

    public long getPostAt() {
        return postAt;
    }

    public void setPostAt(long postAt) {
        this.postAt = postAt;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public UserBean getUserBean() {
        return mUserBean;
    }

    public void setUserBean(UserBean userBean) {
        mUserBean = userBean;
    }

    public boolean isLiked() {
        return isLiked;
    }

    public void setLiked(boolean liked) {
        isLiked = liked;
    }

    public int getLiked() {
        return liked;
    }

    public void setLiked(int liked) {
        this.liked = liked;
    }

    public int getSubCount() {
        return subCount;
    }

    public void setSubCount(int subCount) {
        this.subCount = subCount;
    }

    public ArrayList<CommentBean> getSubComments() {
        return subComments;
    }

    public void setSubComments(ArrayList<CommentBean> subComments) {
        this.subComments = subComments;
    }

    @Override
    public String toString() {
        return "CommentBean{" +
                "id='" + id + '\'' +
                ", musicId=" + musicId +
                ", postAt=" + postAt +
                ", context='" + context + '\'' +
                ", createdAt=" + createdAt +
                ", status=" + status +
                ", mUserBean=" + mUserBean +
                ", isLiked=" + isLiked +
                ", liked=" + liked +
                ", subCount=" + subCount +
                ", subComments=" + subComments +
                '}';
    }
}
