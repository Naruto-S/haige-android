package com.music.player.haige.mvp.model.api.service;

import com.music.player.haige.mvp.model.api.Api;
import com.music.player.haige.mvp.model.entity.BaseResponse;
import com.music.player.haige.mvp.model.entity.action.ActionRequest;
import com.music.player.haige.mvp.model.entity.action.CommentLikeRequest;
import com.music.player.haige.mvp.model.entity.action.FollowActionRequest;
import com.music.player.haige.mvp.model.entity.action.SendFeedbackRequest;
import com.music.player.haige.mvp.model.entity.comment.CommentRequest;
import com.music.player.haige.mvp.model.entity.comment.CommentResponse;
import com.music.player.haige.mvp.model.entity.comment.SendCommentResponse;
import com.music.player.haige.mvp.model.entity.config.AppConfigResponse;
import com.music.player.haige.mvp.model.entity.music.UploadMusicRequest;
import com.music.player.haige.mvp.model.entity.music.UploadMusicResponse;
import com.music.player.haige.mvp.model.entity.music.WebpBgResponse;
import com.music.player.haige.mvp.model.entity.notify.NotifyResponse;
import com.music.player.haige.mvp.model.entity.rank.NewRankTagsResponse;
import com.music.player.haige.mvp.model.entity.rank.RankResponse;
import com.music.player.haige.mvp.model.entity.rank.RankTagsResponse;
import com.music.player.haige.mvp.model.entity.recommend.RecommendResponse;
import com.music.player.haige.mvp.model.entity.search.SearchHotWordResponse;
import com.music.player.haige.mvp.model.entity.search.SearchKeyWordResponse;
import com.music.player.haige.mvp.model.entity.update.UpdateResponse;
import com.music.player.haige.mvp.model.entity.user.FollowUserResponse;
import com.music.player.haige.mvp.model.entity.user.LoginRequest;
import com.music.player.haige.mvp.model.entity.user.ModifyUserRequest;
import com.music.player.haige.mvp.model.entity.user.UserResponse;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * ================================================
 * Created by huangcong on 2018/3/11.
 * ================================================
 */

public interface ApiService {

    /**
     * 应用配置
     *
     * @return
     */
    @GET(Api.URL.GET_APP_CONFIG)
    Observable<AppConfigResponse> getAppConfig();

    /**
     * 音乐推荐
     *
     * @return
     */
    @GET(Api.URL.MUSIC_RECOMMEND)
    Observable<RecommendResponse> getRecommendMusic(@Query("page_num") int pageNum, @Query("page_size") int pageSize);

    /**
     * 音乐最热排行榜
     *
     * @return
     */
    @GET(Api.URL.MUSIC_RANK_HOT)
    Observable<RankResponse> getMusicHotRank();

    /**
     * 音乐最新排行榜
     *
     * @return
     */
    @GET(Api.URL.MUSIC_RANK_NEW)
    Observable<RankResponse> getMusicNewRank();

    /**
     * 搜索热词
     *
     * @return
     */
    @GET(Api.URL.SEARCH_HOT_TAG)
    Observable<SearchHotWordResponse> getHotWord(@Query("size") int size);

    /**
     * 关键字搜索
     *
     * @return
     */
    @GET(Api.URL.SEARCH_KEY_WORD)
    Observable<SearchKeyWordResponse> getSearchResult(@Query("tag_id") int tagId, @Query("page_num") int pageNum, @Query("page_size") int pageSize, @Query("keyword") String searchWord);

    /**
     * 点赞统计
     *
     * @return
     */
    @POST(Api.URL.MUSIC_COUNT_LIKE)
    Observable<BaseResponse> countLike(@Body ActionRequest request);

    /**
     * 分享统计
     *
     * @return
     */
    @POST(Api.URL.MUSIC_COUNT_SHARE)
    Observable<BaseResponse> countShare(@Body ActionRequest request);

    /**
     * 下载统计
     *
     * @return
     */
    @POST(Api.URL.MUSIC_COUNT_DOWNLOAD)
    Observable<BaseResponse> countDownload(@Body ActionRequest request);

    /**
     * 发送评论
     *
     * @return
     */
    @POST(Api.URL.SEND_MUSIC_COMMENT)
    Observable<SendCommentResponse> sendMusicComment(@Body CommentRequest request);

    /**
     * 获取评论
     *
     * @return
     */
    @GET(Api.URL.SEND_MUSIC_COMMENT)
    Observable<CommentResponse> getMusicComment(@Query("music_id") long musicId, @Query("page_size") int pageSize, @Query("last_id") String lastId);

    /**
     * 用户登录注册
     *
     * @return
     */
    @POST(Api.URL.API_USER_LOGIN_OR_REGISTRER)
    Observable<UserResponse> userLoginOrRegistrer(@Body LoginRequest request);

    /**
     * 获取用户信息
     *
     * @return
     */
    @GET(Api.URL.API_USER_PROFILE)
    Observable<UserResponse> getUserProfile();

    /**
     * 获取他人用户信息
     *
     * @return
     */
    @GET(Api.URL.OTHER_USER_PROFILE)
    Observable<UserResponse> getOtherUser(@Query("pub_user_id") String userId);

    /**
     * 获取用户信息
     *
     * @return
     */
    @GET(Api.URL.GET_MUSIC_WEBP_BG)
    Observable<WebpBgResponse> getMusicWebpBg(@Query("type") String type);

    /**
     * 获取用户信息
     *
     * @return
     */
    @PUT(Api.URL.API_USER_PROFILE)
    Observable<BaseResponse> modifyUserProfile(@Body ModifyUserRequest request);

    @Multipart
    @POST(Api.URL.UPLOAD_AVATAR)
    Observable<BaseResponse> uploadAvatar(@Part List<MultipartBody.Part> partList);

    /**
     * 关注
     *
     * @return
     */
    @POST(Api.URL.USER_FOLLOW)
    Observable<BaseResponse> follow(@Body FollowActionRequest request);

    /**
     * 取关
     *
     * @return
     */
    @POST(Api.URL.USER_UNFOLLOW)
    Observable<BaseResponse> unfollow(@Body FollowActionRequest request);

    /**
     * 获取关注列表
     *
     * @return
     */
    @GET(Api.URL.GET_USER_FOLLOWING)
    Observable<FollowUserResponse> getFollowing(@Query("pub_user_id") String userId,
                                                @Query("page_no") int pageNo,
                                                @Query("page_size") int pageSize);

    /**
     * 获取粉丝列表
     *
     * @return
     */
    @GET(Api.URL.GET_USER_FOLLOWER)
    Observable<FollowUserResponse> getFollower(@Query("pub_user_id") String userId,
                                               @Query("page_no") int pageNo,
                                               @Query("page_size") int pageSize);

    /**
     * 获取点赞音乐列表
     *
     * @return
     */
    @GET(Api.URL.GET_USER_LIKED)
    Observable<RecommendResponse> getLikedList(@Query("pub_user_id") String userId,
                                               @Query("page_no") int pageNo,
                                               @Query("page_size") int pageSize);

    @GET(Api.URL.GET_MUSIC_TAG)
    Observable<RankTagsResponse> getMusicTags();

    @GET(Api.URL.GET_MUSIC_TAG_LIST)
    Observable<RankResponse> getMusicTagList(@Query("tag") String tag,
                                             @Query("page_no") int pageNo,
                                             @Query("page_size") int pageSize);

    @GET(Api.URL.GET_USER_UPLOAD_MUSIC)
    Observable<RecommendResponse> getUploadMusic(@Query("pub_user_id") String userId,
                                            @Query("page_no") int pageNo,
                                            @Query("page_size") int pageSize);

    @GET(Api.URL.GET_PUSH_NOTIFY)
    Observable<NotifyResponse> getPushNotify();

    @GET(Api.URL.UPDATE_CONFIG)
    Observable<UpdateResponse> getUpdateConfig();

    /**
     * 反馈
     *
     * @return
     */
    @POST(Api.URL.SEND_FEEDBACK)
    Observable<BaseResponse> sendFeedback(@Body SendFeedbackRequest request);

    @GET(Api.URL.GET_NEW_RANK_TAG)
    Observable<NewRankTagsResponse> getNewRankTag();

    @GET(Api.URL.GET_COMMENT_SUB)
    Observable<CommentResponse> getCommentSub(@Query("comment_id") String commentId,
                                              @Query("page_no") int pageNo,
                                              @Query("page_size") int pageSize);

    @POST(Api.URL.DELETE_COMMENT)
    Observable<BaseResponse> deleteComment(@Query("comment_id") String commentId, @Query("sub") boolean sub);

    @POST(Api.URL.COMMENT_LIKE)
    Observable<BaseResponse> commentLike(@Body CommentLikeRequest request);

    @POST(Api.URL.UPLOAD_MUSIC)
    Observable<BaseResponse> uploadMusic(@Body UploadMusicRequest request);

    @GET(Api.URL.GET_UPLOAD_MUSIC)
    Observable<UploadMusicResponse> getUploadMusicStatus(@Query("page_no") int pageNo,
                                                         @Query("page_size") int pageSize);

    @POST(Api.URL.DELETE_UPLOAD_MUSIC)
    Observable<BaseResponse> deleteMusic(@Body ActionRequest request);
}
