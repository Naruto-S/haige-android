package com.music.player.haige.mvp.model.api;

/**
 * ================================================
 * 存放一些与 API 有关的东西,如请求地址,请求码等
 * <p>
 * ================================================
 */
public class Api {

    public static String APP_DOMAIN = "https://haige.ipaizhao.cn/api/";
    public static String TEST_APP_DOMAIN = "http://test.ipaizhao.cn/api/";

    static {

    }

    public interface URL {
        String MUSIC_RANK_HOT = "music/rank/hot";       // 音乐最热排行
        String MUSIC_RANK_NEW = "music/rank/new";       // 音乐最新排行
        String MUSIC_RECOMMEND = "music/recommend";     // 音乐推荐
        String SEARCH_HOT_TAG = "search/tags";       // 搜索热词列表
        String SEARCH_KEY_WORD = "search/key_word";     // 关键字搜索
        String MUSIC_COUNT_LIKE = "count/like";         // 点赞统计
        String MUSIC_COUNT_SHARE = "count/share";       // 分享统计
        String MUSIC_COUNT_DOWNLOAD = "count/download"; // 下载统计
        String API_USER_LOGIN_OR_REGISTRER = "/api/user/login-or-register"; // 登录注册
        String API_USER_PROFILE = "/api/user/profile"; // GET获取用户信息  PUT修改用户信息
        String OTHER_USER_PROFILE = "/api/user/profile/public"; // 他人用户信息
        String UPLOAD_AVATAR = "/api/user/profile/avatar_url";//上传头像
        String GET_MUSIC_WEBP_BG = "/api/music/rand/bg"; // 获取音乐动态背景
        String SEND_MUSIC_COMMENT = "/api/comment"; // 发送评论
        String USER_FOLLOW = "/api/user/follow";//关注
        String USER_UNFOLLOW = "/api/user/unfollow";//取关
        String GET_USER_FOLLOWING = "/api/user/following";//关注列表
        String GET_USER_FOLLOWER = "/api/user/follower";//粉丝列表
        String GET_USER_LIKED = "/api/user/liked";//点赞列表
        String GET_MUSIC_TAG = "/api/tag";//分类标签
        String GET_MUSIC_TAG_LIST = "/api/music/tag";//分类音乐列表
        String GET_USER_UPLOAD_MUSIC = "/api/user/music";//用户上传音乐列表
        String GET_PUSH_NOTIFY = "/api/push";//拉取通知
        String UPDATE_CONFIG = "/api/upgrade";//更新
        String SEND_FEEDBACK = "/api/feedback";//反馈
        String GET_NEW_RANK_TAG = "/api/ranking";//新排行榜
        String DELETE_COMMENT = "/api/comment/remove";//删除评论
        String COMMENT_LIKE = "/api/comment/like/action";//评论点赞和取消
        String GET_COMMENT_SUB = "/api/comment/sub";//获取子评论
        String UPLOAD_MUSIC = "/api/music";//音乐上传
        String GET_UPLOAD_MUSIC = "/api/music/uploaded";
        String DELETE_UPLOAD_MUSIC = "/api/music/del";

        String GET_APP_CONFIG = "/api/launch/config";//拉取配置接口

    }

    public enum Action {
        NET_NO_CONNECT,
        MUSIC_RANK,
        MUSIC_RANK_TAG,
        MUSIC_PLAYER,
        MUSIC_LRC,
        SEARCH_HOT_TAG,
        SEARCH_HISTORY,
        SEARCH_KEY_WORD,
        SEARCH_KEY_WORD_LOCAL,
        SEARCH_KEY_WORD_RECENT,
        MUSIC_COUNT_LIKE,
        MUSIC_COUNT_SHARE,
        MUSIC_COUNT_DOWNLOAD,
        DOWNLOAD_FINISH,
        DOWNLOAD_ING,
        MUSIC_LIST,
        MUSIC_PLAY_QUEUE,
        MUSIC_WEBP_BG,
        USER_LOGIN,
        USER_PROFILE,
        USER_PROFILE_OTHER,
        MODIFY_USER_PROFILE,
        UPLOAD_AVATAR,
        LOGIN_LOGOUT,
        SEND_MUSIC_COMMENT,
        GET_MUSIC_COMMENT,
        GET_COMMENT_SUB,
        DELETE_COMMENT,
        LIKE_COMMENT,
        USER_FOLLOW,
        USER_UNFOLLOW,
        FEEDBACK,
        UPLOAD_MUSIC,
        DELETE_UPLOAD_MUSIC,
    }


    public enum State {

        OK("200", "OK"),
        NORMAL("0", "Nothing"),
        ERROR("-1", "Error"),
        NEED_INVITE("-2", "Invite First"),
        NETWORK_ERROR("-3", "Cache Error"),
        CACHE_ERROR("-4", "Cache Error"),
        NET_NO_CONNECT("-5", "NetWork No Connect"),
        LOGIN_FAILURE("100004", "登录失效");

        private String mCode;
        private String mMsg;

        State(String code, String msg) {
            mCode = code;
            mMsg = msg;
        }

        public String getCode() {
            return mCode;
        }

        public String getMsg() {
            return mMsg;
        }

        public boolean equalsCode(String code) {
            return mCode.equals(code);
        }

    }

}
