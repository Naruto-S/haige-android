package com.music.player.haige.mvp.ui.widget.emoji;

import android.support.annotation.NonNull;

import java.io.Serializable;

/**
 * Created by congwiny on 2016/6/2.
 */
public class Emoji implements Serializable {

    public static final int FROM_CODEPOINT = 1;
    public static final int FROM_CHAR = 2;
    public static final int FROM_CHARS = 3;
    public static final int FROM_RECENT = 4;

    private int fromWhat;
    private int codePoint;
    private char codePointChar;
    private int[] codePointArray;

    private static final long serialVersionUID = 1L;

    public static Emoji fromCodePoint(final int codePoint) {
        if (Character.isDefined(codePoint)) {
            Emoji emoji = new Emoji(new String(Character.toChars(codePoint)), FROM_CODEPOINT);
            emoji.codePoint = codePoint;
            return emoji;
        } else {
            return null;
        }
    }

    public static Emoji fromChar(final char ch) {
        if (Character.isDefined(ch)) {
            Emoji emoji = new Emoji(Character.toString(ch), FROM_CHAR);
            emoji.codePointChar = ch;
            return emoji;
        } else {
            return null;
        }

    }

    public static Emoji fromChars(int... codePoints) {
        if (Character.isDefined(codePoints[0]) && Character.isDefined(codePoints[1])) {
            StringBuilder sub = new StringBuilder();
            sub.append(Character.toChars(codePoints[0])).append(Character.toChars(codePoints[1]));
            Emoji emoji = new Emoji(sub.toString(), FROM_CHARS);
            emoji.codePointArray = codePoints;
            return emoji;
        } else {
            return null;
        }
    }

    @NonNull
    private final String emoji;

    public Emoji(@NonNull final String emoji, int fromWhat) {
        this.emoji = emoji;
        this.fromWhat = fromWhat;
    }

    @NonNull
    public String getEmoji() {
        return emoji;
    }

    public int getCodePoint(){
        return codePoint;
    }

    public int getCodeChar(){
        return (int)codePointChar;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final Emoji e = (Emoji) o;
        return emoji.equals(e.emoji);
    }

    @Override
    public int hashCode() {
        return emoji.hashCode();
    }

    public boolean isDefined() {
        switch (fromWhat) {
            case FROM_CHAR:
                return Character.isDefined(this.codePointChar);
            case FROM_CHARS:
                return Character.isDefined(this.codePointArray[0])
                        && Character.isDefined(this.codePointArray[1]);
            case FROM_CODEPOINT:
                return Character.isDefined(this.codePoint);
            case FROM_RECENT:
                return true;
        }
        return false;
    }
}
