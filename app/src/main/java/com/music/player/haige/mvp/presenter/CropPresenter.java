package com.music.player.haige.mvp.presenter;

import android.graphics.Bitmap;
import android.net.Uri;

import com.music.player.haige.app.upload.FileBaseService;
import com.music.player.haige.mvp.contract.AppBasePresenter;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.Api;
import com.music.player.haige.mvp.model.entity.BaseResponse;
import com.music.player.haige.mvp.ui.utils.FileUtils;

import java.util.List;

import javax.inject.Inject;

import me.jessyan.rxerrorhandler.core.RxErrorHandler;
import me.jessyan.rxerrorhandler.handler.ErrorHandleSubscriber;
import okhttp3.MultipartBody;

/**
 * Created by Naruto on 2018/5/13.
 */

public class CropPresenter extends AppBasePresenter<MVContract.CropModel, MVContract.CropView> {

    public static final String TAG = CropPresenter.class.getSimpleName();

    @Inject
    RxErrorHandler mErrorHandler;


    @Inject
    public CropPresenter(MVContract.CropModel model, MVContract.CropView view) {
        super(model, view);
    }

    public void saveAndUploadImage(final Uri outputUri, final Bitmap bitmap) {
        mRootView.showProgress();
        String filePath = outputUri.getPath();
        FileUtils.saveImageFile(filePath, bitmap, 100, true)
                .subscribe(new ErrorHandleSubscriber<Boolean>(mErrorHandler) {
                    @Override
                    public void onNext(Boolean aBoolean) {
                        if (aBoolean) {
                            uploadAvatar(filePath);
                        } else {
                            mRootView.onError(Api.Action.UPLOAD_AVATAR, "-1", "");
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                        mRootView.onError(Api.Action.UPLOAD_AVATAR, "-1", t.getMessage());
                    }
                });
    }

    public void uploadAvatar(String filePath) {
        MultipartBody.Builder builder = FileBaseService.getBasicMultipartBodyBuilder();
        FileBaseService.buildFileRequestBody(builder, null, "file", filePath, null);
        List<MultipartBody.Part> parts = builder.build().parts();

        buildObservable(mModel.uploadAvatar(parts))
                .subscribe(new ErrorHandleSubscriber<BaseResponse>(mErrorHandler) {
                    @Override
                    public void onNext(BaseResponse response) {
                        if (response != null) {
                            mRootView.onSuccess(Api.Action.UPLOAD_AVATAR, response.getData());
                        } else {
                            mRootView.onError(Api.Action.UPLOAD_AVATAR, Api.State.NORMAL.getCode(), Api.State.NORMAL.getMsg());
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                        mRootView.onError(Api.Action.UPLOAD_AVATAR, "-1", t.getMessage());
                    }
                });
    }

}
