package com.music.player.haige.mvp.ui.main.fragment;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.hc.core.di.component.AppComponent;
import com.music.player.haige.R;
import com.music.player.haige.app.base.AppBaseFragment;
import com.music.player.haige.app.base.recyclerview.view.MultiStateView;
import com.music.player.haige.app.base.recyclerview.view.NoNetworkView;
import com.music.player.haige.di.component.DaggerRankingComponent;
import com.music.player.haige.di.module.RankingModule;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.Api;
import com.music.player.haige.mvp.model.entity.rank.NewRankTagsResponse;
import com.music.player.haige.mvp.model.entity.rank.RankTagsBean;
import com.music.player.haige.mvp.presenter.RankingFPresenter;
import com.music.player.haige.mvp.ui.main.adapter.RankGridItemDecoration;
import com.music.player.haige.mvp.ui.main.adapter.RankingNewAdapter;
import com.music.player.haige.mvp.ui.main.dialog.SelectLikeDialog;
import com.music.player.haige.mvp.ui.utils.DebugLogger;
import com.music.player.haige.mvp.ui.utils.NetworkUtils;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;
import com.music.player.haige.mvp.ui.utils.Utils;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * ================================================
 * Created by huangcong on 2018/2/27.
 * ================================================
 */

public class RankingNewFragment extends AppBaseFragment<RankingFPresenter> implements MVContract.RankingView {

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    @BindView(R.id.multiStateView)
    MultiStateView mMultiStateView;

    private boolean isFirstTimeCreate = true;
    private RankingNewAdapter mAdapter;

    public static RankingNewFragment newInstance() {
        RankingNewFragment fragment = new RankingNewFragment();
        return fragment;
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        Animation anim;
        if(!isFirstTimeCreate) {
            if (enter) {
                anim = AnimationUtils.loadAnimation(getActivity(),
                        R.anim.slide_in_from_left);
            } else {
                anim = AnimationUtils.loadAnimation(getActivity(),
                        R.anim.slide_out_to_left);
            }
            return anim;
        }
        isFirstTimeCreate = false;
        return null;
    }

    @Override
    public void setupFragmentComponent(AppComponent appComponent) {
        DaggerRankingComponent
                .builder()
                .appComponent(appComponent)
                .rankingModule(new RankingModule(this))
                .build()
                .inject(this);
    }

    @Override
    public View initView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.app_fragment_ranking_new, container, false);
    }

    @Override
    public void initData(Bundle savedInstanceState) {
        initMultiStateView();
        initRecycler();

        if (NetworkUtils.isConnected(getContext())) {
            mPresenter.getRankingTags();
        } else {
            mMultiStateView.setViewState(MultiStateView.VIEW_STATE_ERROR);
        }
    }

    private void initMultiStateView() {
        ((NoNetworkView) mMultiStateView.getView(MultiStateView.VIEW_STATE_ERROR))
                .setOnRefreshNetwork(() -> {
                    if (NetworkUtils.isConnected(getContext())) {
                        mPresenter.getRankingTags();
                    } else {
                        Toast.makeText(getContext(), R.string.app_net_no_connect_tip, Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void initRecycler() {
        mAdapter = new RankingNewAdapter();
        mRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        mRecyclerView.addItemDecoration(new RankGridItemDecoration(ResourceUtils.dpToPX(16)));
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void setData(Object data) {

    }

    @Override
    public void showLoading() {
        super.showLoading();
        if (Utils.isNotNull(mMultiStateView)) {
            mMultiStateView.setViewState(MultiStateView.VIEW_STATE_LOADING);
        }
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
        if (Utils.isNotNull(mMultiStateView)) {
            mMultiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);
        }
    }

    @Override
    public void onSuccess(Api.Action action, Object o) {
        super.onSuccess(action, o);
        if (action == Api.Action.MUSIC_RANK_TAG) {
            try {
                RankTagsBean rankTags =  ((NewRankTagsResponse) o).getData();
                mAdapter.setNewData(rankTags);

                SelectLikeDialog.create(getChildFragmentManager())
                        .setData(rankTags.getCategory())
                        .setListener(strings -> {

                        })
                        .show();
            } catch (Exception e) {
                DebugLogger.e(e);
            }
        }
    }

    @Override
    public void onError(Api.Action action, String code, String message) {
        super.onError(action, code, message);
    }

    public void refresh() {
        mPresenter.getRankingTags();
    }
}
