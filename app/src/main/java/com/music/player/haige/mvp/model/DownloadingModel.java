package com.music.player.haige.mvp.model;

import com.hc.base.mvp.BaseModel;
import com.hc.core.integration.IRepositoryManager;
import com.music.player.haige.mvp.contract.MVContract;

import javax.inject.Inject;

/**
 * ================================================
 * Created by huangcong on 2018/3/20.
 * ================================================
 */

public class DownloadingModel extends BaseModel implements MVContract.DownloadingModel {

    @Inject
    public DownloadingModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }
}
