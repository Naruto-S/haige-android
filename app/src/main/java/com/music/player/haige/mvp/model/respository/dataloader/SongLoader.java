package com.music.player.haige.mvp.model.respository.dataloader;

import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;
import android.text.TextUtils;

import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.model.respository.provider.FavoriteSong;
import com.music.player.haige.mvp.model.respository.provider.SongPlayCount;
import com.music.player.haige.mvp.model.utils.PreferencesUtility;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;


public class SongLoader {

    public static Observable<List<Song>> getSongsForCursor(final Cursor cursor) {
        return Observable.create(new ObservableOnSubscribe<List<Song>>() {
            @Override
            public void subscribe(ObservableEmitter<List<Song>> emitter) throws Exception {
                List<Song> arrayList = new ArrayList<Song>();
                if ((cursor != null) && (cursor.moveToFirst()))
                    do {
                        long id = cursor.getLong(0);
                        String title = cursor.getString(1);
                        String artist = cursor.getString(2);
                        String album = cursor.getString(3);
                        int duration = cursor.getInt(4);
                        int trackNumber = cursor.getInt(5);
                        long artistId = cursor.getInt(6);
                        long albumId = cursor.getLong(7);
                        String path = cursor.getString(8);
                        boolean isLocal = true;

                        arrayList.add(new Song(id,
                                albumId,
                                artistId,
                                title,
                                artist,
                                album,
                                duration,
                                trackNumber,
                                path,
                                isLocal));
                    } while (cursor.moveToNext());
                if (cursor != null) {
                    cursor.close();
                }
                emitter.onNext(arrayList);
                emitter.onComplete();
            }
        });
    }

    public static Observable<List<Song>> getFavoriteSong(final Context context) {
        Cursor cursor = FavoriteSong.getInstance(context).getFavoriteSong();
        SortedCursor retCursor = TopTracksLoader.makeSortedCursor(context, cursor, 0);
        return SongLoader.getSongsForCursor(retCursor);
    }

    public static Observable<List<Song>> getSongsWithScoreForCursor(final Cursor cursor, final Cursor scoreCursor) {
        return Observable.create(new ObservableOnSubscribe<List<Song>>() {

            @Override
            public void subscribe(ObservableEmitter<List<Song>> emitter) throws Exception {
                List<Song> arrayList = new ArrayList<Song>();
                if ((cursor != null && scoreCursor != null) && (cursor.moveToFirst() && scoreCursor.moveToFirst()))
                    do {
                        long id = cursor.getLong(0);
                        String title = cursor.getString(1);
                        String artist = cursor.getString(2);
                        String album = cursor.getString(3);
                        int duration = cursor.getInt(4);
                        int trackNumber = cursor.getInt(5);
                        long artistId = cursor.getInt(6);
                        long albumId = cursor.getLong(7);
                        String path = cursor.getString(8);
                        boolean isLocal = true;

                        Song song = new Song(id,
                                albumId,
                                artistId,
                                title,
                                artist,
                                album,
                                duration,
                                trackNumber,
                                path,
                                isLocal);
                        song.setPlayCountScore(scoreCursor.getFloat(scoreCursor.getColumnIndex(SongPlayCount.SongPlayCountColumns.PLAYCOUNTSCORE)));
                        arrayList.add(song);
                    }
                    while (cursor.moveToNext() && scoreCursor.moveToNext());
                if (cursor != null) {
                    cursor.close();
                }
                if (scoreCursor != null) {
                    scoreCursor.close();
                }
                emitter.onNext(arrayList);
                emitter.onComplete();
            }
        });
    }

    public static Observable<List<Song>> getAllSongs(Context context) {
        return getSongsForCursor(makeSongCursor(context, null, null));
    }

    public static Observable<List<Song>> searchSongs(Context context, String searchString) {
        return getSongsForCursor(makeSongCursor(context, "title LIKE ? or artist LIKE ? or album LIKE ? ",
                new String[]{"%" + searchString + "%", "%" + searchString + "%", "%" + searchString + "%"}));
    }

    public static Observable<List<Song>> getSongListInFolder(Context context, String path) {
        String[] whereArgs = new String[]{path + "%"};
        return getSongsForCursor(makeSongCursor(context, MediaStore.Audio.Media.DATA + " LIKE ?", whereArgs, null));
    }


    public static Cursor makeSongCursor(Context context, String selection, String[] paramArrayOfString) {
        final String songSortOrder = PreferencesUtility.getInstance(context).getSongSortOrder();
        return makeSongCursor(context, selection, paramArrayOfString, songSortOrder);
    }

    private static Cursor makeSongCursor(Context context, String selection, String[] paramArrayOfString, String sortOrder) {
        String selectionStatement = "is_music=1 AND title != ''";

        if (!TextUtils.isEmpty(selection)) {
            selectionStatement = selectionStatement + " AND " + selection;
        }
        return context.getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                new String[]{"_id", "title", "artist", "album", "duration", "track", "artist_id", "album_id", MediaStore.Audio.Media.DATA},
                selectionStatement, paramArrayOfString, sortOrder);

    }
}
