package com.music.player.haige.mvp.usecase;

import com.hc.core.mvp.UseCase;
import com.music.player.haige.mvp.model.entity.music.FolderInfo;
import com.music.player.haige.mvp.model.respository.interfaces.Repository;

import java.util.List;

import io.reactivex.Observable;

public class GetFolders extends UseCase<GetFolders.RequestValues, GetFolders.ResponseValue> {

    private final Repository mRepository;

    public GetFolders(Repository repository) {
        this.mRepository = repository;
    }

    @Override
    public ResponseValue execute(RequestValues requestValues) {
        return new ResponseValue(mRepository.getFoldersWithSong());
    }

    public static final class RequestValues implements UseCase.RequestValues {
    }

    public static final class ResponseValue implements UseCase.ResponseValue {

        private final Observable<List<FolderInfo>> mListObservable;

        public ResponseValue(Observable<List<FolderInfo>> listObservable) {
            mListObservable = listObservable;
        }

        public Observable<List<FolderInfo>> getFolderList() {
            return mListObservable;
        }
    }
}
