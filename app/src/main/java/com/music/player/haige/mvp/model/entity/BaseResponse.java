package com.music.player.haige.mvp.model.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * ================================================
 * Created by huangcong on 2018/3/11.
 * ================================================
 */

public class BaseResponse<T> implements Serializable{

    @SerializedName("code")
    int code;

    @SerializedName("msg")
    String msg;

    @SerializedName("uid")
    String uid;

    @SerializedName("data")
    T data;

    Object tag;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Object getTag() {
        return tag;
    }

    public void setTag(Object tag) {
        this.tag = tag;
    }
}
