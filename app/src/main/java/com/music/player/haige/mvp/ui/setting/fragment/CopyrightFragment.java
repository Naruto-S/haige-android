package com.music.player.haige.mvp.ui.setting.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.OnClick;
import com.hc.core.di.component.AppComponent;
import com.music.player.haige.R;
import com.music.player.haige.app.EventBusTags;
import com.music.player.haige.app.base.AppBaseFragment;
import com.music.player.haige.di.component.DaggerCommonComponent;
import com.music.player.haige.di.module.CommonModule;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.presenter.CopyrightFPresenter;
import org.simple.eventbus.EventBus;

/**
 * ================================================
 * Created by huangcong on 2018/3/31.
 * ================================================
 */

public class CopyrightFragment extends AppBaseFragment<CopyrightFPresenter> implements MVContract.CommonView {

    public static CopyrightFragment newInstance(){
        CopyrightFragment fragment = new CopyrightFragment();
        return fragment;
    }

    @Override
    public void setupFragmentComponent(AppComponent appComponent) {
        DaggerCommonComponent.builder()
                .appComponent(appComponent)
                .commonModule(new CommonModule(this))
                .build()
                .inject(this);
    }

    @Override
    public View initView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.app_fragment_copyright, container, false);
    }

    @Override
    public void initData(Bundle savedInstanceState) {

    }

    @OnClick({R.id.app_fragment_copyright_back_iv})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.app_fragment_copyright_back_iv:
                EventBus.getDefault().post(EventBusTags.MAIN.BACK);
                break;
        }
    }
}
