package com.music.player.haige.mvp.ui.user;

import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;

import com.hc.base.base.BaseFragment;
import com.hc.core.di.component.AppComponent;
import com.hc.core.utils.CoreUtils;
import com.music.player.haige.R;
import com.music.player.haige.app.EventBusTags;
import com.music.player.haige.app.SystemUIConfig;
import com.music.player.haige.app.base.AppBaseActivity;
import com.music.player.haige.app.base.pref.UserPrefHelper;
import com.music.player.haige.app.constants.IntentConstants;
import com.music.player.haige.app.image.loader.AvatarLoader;
import com.music.player.haige.app.image.widget.HaigeImageView;
import com.music.player.haige.app.utils.launcher.ActivityLauncherStart;
import com.music.player.haige.di.component.DaggerOtherProfileComponent;
import com.music.player.haige.di.module.OtherProfileModule;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.Api;
import com.music.player.haige.mvp.model.entity.action.FollowActionRequest;
import com.music.player.haige.mvp.model.entity.user.UserBean;
import com.music.player.haige.mvp.presenter.OtherUserPresenter;
import com.music.player.haige.mvp.ui.login.LoginDialogFragment;
import com.music.player.haige.mvp.ui.music.fragment.LikeMusicFragment;
import com.music.player.haige.mvp.ui.music.fragment.UploadMusicFragment;
import com.music.player.haige.mvp.ui.user.utils.UserViewUtils;
import com.music.player.haige.mvp.ui.utils.DebugLogger;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;
import com.music.player.haige.mvp.ui.utils.Utils;
import com.music.player.haige.mvp.ui.widget.adapter.TitlePagerAdapter;

import org.simple.eventbus.EventBus;

import butterknife.BindView;
import butterknife.OnClick;

import static com.music.player.haige.mvp.model.entity.user.UserBean.USER_FOLLOW_STAUS_FOLLOW_ME;
import static com.music.player.haige.mvp.model.entity.user.UserBean.USER_FOLLOW_STAUS_FOLLOW_OTHER;
import static com.music.player.haige.mvp.model.entity.user.UserBean.USER_FOLLOW_STAUS_MUTUAL;
import static com.music.player.haige.mvp.model.entity.user.UserBean.USER_FOLLOW_STAUS_UNFOLLOW;
import static com.music.player.haige.mvp.ui.user.FansListActivity.TYPE_FOLLOWER;
import static com.music.player.haige.mvp.ui.user.FansListActivity.TYPE_FOLLOWING;
import static com.music.player.haige.mvp.ui.utils.Utils.isFastClick;

public class OtherUserActivity extends AppBaseActivity<OtherUserPresenter> implements MVContract.OtherProfileView,
        SystemUIConfig, AppBarLayout.OnOffsetChangedListener {

    @BindView(R.id.hiv_avatar)
    HaigeImageView mAvatarHiv;
    @BindView(R.id.tv_name)
    TextView mNameTv;
    @BindView(R.id.tv_hai_id)
    TextView mIdTv;
    @BindView(R.id.tv_age)
    TextView mAgeTv;
    @BindView(R.id.tv_desc)
    TextView mDescTv;
    @BindView(R.id.tv_province)
    TextView mProvinceTv;
    @BindView(R.id.tv_sign)
    TextView mSignTv;
    @BindView(R.id.tv_follow)
    TextView mFollowTv;
    @BindView(R.id.tv_follow_count)
    TextView mFollowCountTv;
    @BindView(R.id.tv_fans_count)
    TextView mFansCountTv;
    @BindView(R.id.tab)
    TabLayout mTabLayout;
    @BindView(R.id.viewpager)
    ViewPager mViewPager;
    @BindView(R.id.app_fragment_title_tv)
    TextView mTitleTv;
    @BindView(R.id.app_bar)
    AppBarLayout mAppBarLayout;

    private UserBean mOtherUser;

    @Override
    public void setupActivityComponent(AppComponent appComponent) {
        DaggerOtherProfileComponent
                .builder()
                .appComponent(appComponent)
                .otherProfileModule(new OtherProfileModule(this))
                .build()
                .inject(this);
    }

    @Override
    public int initView(Bundle savedInstanceState) {
        return R.layout.activity_other_user;
    }

    @Override
    public void initData(Bundle savedInstanceState) {
        if (Utils.isNotNull(getIntent())) {
            mOtherUser = getIntent().getParcelableExtra(IntentConstants.EXTRA_OTHER_USER);
        } else {
            finish();
        }

        setUserView();

        if (Utils.isNotNull(mOtherUser)) {
            DebugLogger.e(TAG, mOtherUser);

            String[] titles = getResources().getStringArray(R.array.app_other_music);
            BaseFragment[] fragments = new BaseFragment[titles.length];
            fragments[0] = UploadMusicFragment.newInstance(mOtherUser.getUserId());
            fragments[1] = LikeMusicFragment.newInstance(mOtherUser.getUserId());

            TitlePagerAdapter adapter = new TitlePagerAdapter(getSupportFragmentManager(), titles, fragments);
            mViewPager.setAdapter(adapter);
            mTabLayout.setupWithViewPager(mViewPager);

            for (int i = 0; i < titles.length; i++) {
                TabLayout.Tab tab = mTabLayout.getTabAt(i);
                tab.setText(titles[i]);
            }

            mPresenter.getUserProfile(mOtherUser);
        }
    }

    private void setUserView() {
        try {
            AvatarLoader.loadUserAvatar(mOtherUser.getAvatarUrl(),
                    R.drawable.ic_main_default_avatar,
                    R.drawable.ic_main_default_avatar,
                    mAvatarHiv);
            UserViewUtils.setUserName(mOtherUser, mNameTv);
            UserViewUtils.setUserName(mOtherUser, mTitleTv);
            UserViewUtils.setUserHaigeId(mOtherUser, mIdTv);
            UserViewUtils.setUserGenderAndAge(mOtherUser, mAgeTv);
            UserViewUtils.setUserProvince(mOtherUser, mProvinceTv);
            UserViewUtils.setUserDesc(mOtherUser, mDescTv);
            UserViewUtils.switchFollowStatus(mFollowTv, mOtherUser, TYPE_FOLLOWING);
            mFollowCountTv.setText(String.valueOf(mOtherUser.getFollowingCount()));
            mFansCountTv.setText(String.valueOf(mOtherUser.getFollowerCount()));
        } catch (Exception e) {
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        mPresenter.getUserProfile(mOtherUser);
    }

    @Override
    protected void onResume() {
        super.onResume();

        mAppBarLayout.addOnOffsetChangedListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mAppBarLayout.removeOnOffsetChangedListener(this);
    }

    @Override
    public boolean translucentStatusBar() {
        return false;
    }

    @Override
    public int setStatusBarColor() {
        return Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP
                ? getResources().getColor(R.color.color_33000000)
                : getResources().getColor(R.color.color_00000000);
    }

    @Override
    public boolean lightStatusBar() {
        return false;
    }

    @Override
    public boolean fitSystemWindows() {
        return true;
    }

    @Override
    public boolean fullScreen() {
        return false;
    }

    @Override
    public void showLoading() {
        super.showLoading();
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
    }

    @Override
    public void onSuccess(Api.Action action, Object o) {
        super.onSuccess(action, o);

        switch (action) {
            case USER_PROFILE_OTHER:
                mOtherUser = (UserBean) o;
                setUserView();
                break;
        }
    }

    @Override
    public void onError(Api.Action action, String code, String message) {
        super.onError(action, code, message);
    }

    @Override
    public void showMessage(String message) {
        super.showMessage(message);
        CoreUtils.makeText(this, message);
    }

    @OnClick({R.id.app_fragment_back_iv, R.id.tv_follow, R.id.layout_follow, R.id.layout_fans})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.app_fragment_back_iv:
                finish();
                break;
            case R.id.tv_follow:
                if (Utils.isNull(mOtherUser)) return;

                if (UserPrefHelper.isLogin()) {
                    FollowActionRequest request = new FollowActionRequest();
                    request.setUserId(UserPrefHelper.getUserId());
                    request.setBeFollowedId(mOtherUser.getUserId());

                    switch (mOtherUser.getFollowStatus()) {
                        case USER_FOLLOW_STAUS_UNFOLLOW:
                            mOtherUser.setFollowStatus(USER_FOLLOW_STAUS_FOLLOW_OTHER);
                            mPresenter.follow(request);
                            break;
                        case USER_FOLLOW_STAUS_FOLLOW_ME:
                            mOtherUser.setFollowStatus(USER_FOLLOW_STAUS_MUTUAL);
                            mPresenter.follow(request);
                            break;
                        case USER_FOLLOW_STAUS_FOLLOW_OTHER:
                            mOtherUser.setFollowStatus(USER_FOLLOW_STAUS_UNFOLLOW);
                            mPresenter.unfollow(request);
                            break;
                        case USER_FOLLOW_STAUS_MUTUAL:
                            mOtherUser.setFollowStatus(USER_FOLLOW_STAUS_FOLLOW_ME);
                            mPresenter.unfollow(request);
                            break;
                    }
                    setUserView();
                    mPresenter.getUserProfile(mOtherUser);
                    EventBus.getDefault().post(new EventBusTags.FollowItemStatusEvent(mOtherUser.getFollowStatus()));
                } else {
                    LoginDialogFragment.newInstance().show(getSupportFragmentManager(), LoginDialogFragment.class.getName());
                }
                break;
            case R.id.layout_follow:
                ActivityLauncherStart.startFollowList(this, mOtherUser, TYPE_FOLLOWING);
                break;
            case R.id.layout_fans:
                ActivityLauncherStart.startFollowList(this, mOtherUser, TYPE_FOLLOWER);
                break;
        }
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        float offsetProportion = 1f - Math.abs((float) verticalOffset / ResourceUtils.dpToPX(200));
        offsetProportion = offsetProportion > 1f ? 1f : offsetProportion;
        mTitleTv.setAlpha(1 - offsetProportion);

        if (verticalOffset == 0) {  //展开状态，状态栏图标白色
            if (isFastClick()) {
                return;
            }
            mTitleTv.setVisibility(View.GONE);
        } else if (Math.abs(verticalOffset) >= mAppBarLayout.getTotalScrollRange()) {     //折叠状态，状态栏图标黑色
            mTitleTv.setVisibility(View.VISIBLE);
        }
    }
}
