package com.music.player.haige.mvp.model;

import com.hc.base.mvp.BaseModel;
import com.hc.core.integration.IRepositoryManager;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.service.ApiService;
import com.music.player.haige.mvp.model.entity.music.WebpBgResponse;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * ================================================
 * Created by huangcong on 2018/2/27.
 * ================================================
 */

public class SplashModel extends BaseModel implements MVContract.SplashModel {

    @Inject
    public SplashModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public Observable<WebpBgResponse> getMusicWebpBg(String type) {
        return Observable.just(mRepositoryManager.obtainRetrofitService(ApiService.class).getMusicWebpBg(type))
                .flatMap(observable -> observable);
    }
}
