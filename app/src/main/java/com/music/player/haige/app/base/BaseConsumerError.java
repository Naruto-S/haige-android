package com.music.player.haige.app.base;

import com.tencent.bugly.crashreport.CrashReport;

import io.reactivex.functions.Consumer;

/**
 * Created by john on 2018/4/19.
 */

public class BaseConsumerError implements Consumer<Throwable> {

    //onError事件处理
    @Override
    public void accept(Throwable throwable) {
        CrashReport.postCatchedException(throwable);
    }

}
