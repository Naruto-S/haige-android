package com.music.player.haige.di.component;

import com.hc.core.di.component.AppComponent;
import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.di.module.RecommendModule;
import com.music.player.haige.mvp.ui.main.fragment.RecommendNewFragment;

import dagger.Component;

/**
 * ================================================
 * Created by huangcong on 2018/2/25.
 * ================================================
 */

@FragmentScope
@Component(modules = {RecommendModule.class}, dependencies = AppComponent.class)
public interface RecommendComponent {

    void inject(RecommendNewFragment fragment);

}
