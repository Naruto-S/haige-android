package com.music.player.haige.app.constants;

public class IntentConstants {

    public static final String EXTRA_WEB_URL = "EXTRA_WEB_URL";
    public static final String EXTRA_EDIT_INFO_TYPE = "EXTRA_EDIT_INFO_TYPE";
    public static final String EXTRA_CHOOSE_AVATAR_PATH = "EXTRA_CHOOSE_AVATAR_PATH";

    public static final String EXTRA_OTHER_USER = "EXTRA_OTHER_USER";

    public static final String EXTRA_FOLLOW_TYPE = "EXTRA_FOLLOW_TYPE";

    public static final String EXTRA_MUSIC_TAG = "EXTRA_MUSIC_TAG";

    public static final String EXTRA_CLICK_NOTIFY = "EXTRA_CLICK_NOTIFY";//点击消息推送;
    public static final String EXTRA_NOTIFY_MUSIC = "EXTRA_NOTIFY_MUSIC";

    public static final String EXTRA_CURRENT_MUSIC = "EXTRA_CURRENT_MUSIC";

    public static final String EXTRA_MUSIC_TYPE_LIST = "EXTRA_MUSIC_TYPE_LIST";
}
