package com.music.player.haige.mvp.ui.main.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.music.player.haige.R;
import com.music.player.haige.app.image.loader.HaigeImageLoader;
import com.music.player.haige.app.image.widget.HaigeImageView;
import com.music.player.haige.app.utils.launcher.ActivityLauncherStart;
import com.music.player.haige.app.utils.launcher.ActivityStartBase;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.model.entity.rank.RankCategoryBean;
import com.music.player.haige.mvp.model.entity.rank.RankTagsBean;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;
import com.music.player.haige.mvp.ui.utils.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Naruto on 2018/8/25.
 */

public class RankingNewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int  TYPE_TOP = 0;
    private static final int  TYPE_TAG = 1;

    private Context mContext;
    private RankTagsBean mRankTagsBean;
    private ArrayList<RankCategoryBean> mCategory = new ArrayList<>();

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);

        RecyclerView.LayoutManager manager = recyclerView.getLayoutManager();
        if(manager instanceof GridLayoutManager){   // 布局是GridLayoutManager所管理
            final GridLayoutManager gridLayoutManager = (GridLayoutManager) manager;
            gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    // 如果是Header、Footer的对象则占据spanCount的位置，否则就只占用1个位置
                    return (position == 0) ? gridLayoutManager.getSpanCount() : 1;
                }
            });
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        if (viewType == TYPE_TOP) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ranking_new_top, parent, false);
            return new TopViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ranking_new_tag, parent, false);
            return new CategoryViewHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder.getItemViewType() == TYPE_TOP) {
            TopViewHolder topViewHolder = (TopViewHolder) holder;
            if (Utils.isNotNull(mRankTagsBean)) {
                Song hotSong = mRankTagsBean.getHotMusic();
                Song newSong = mRankTagsBean.getNewMusic();
                topViewHolder.hotMusicTv.setText(hotSong.getMusicName());
                topViewHolder.hotView.setOnClickListener(v -> ActivityLauncherStart.startTagMusic((Activity) mContext, "最热"));
                topViewHolder.newMusicTv.setText(newSong.getMusicName());
                topViewHolder.newView.setOnClickListener(v -> ActivityLauncherStart.startTagMusic((Activity) mContext, "最新"));
            }
        } else {
            CategoryViewHolder categoryViewHolder = (CategoryViewHolder) holder;
            RankCategoryBean category = mCategory.get(position - 1);
            if (Utils.isNotNull(category)) {
                HaigeImageLoader.loadTagCover(category.getImg(),
                        R.drawable.bg_rank_default,
                        R.drawable.bg_rank_default,
                        categoryViewHolder.coverIv);

                categoryViewHolder.tagTitleTv.setText(category.getTitle());
                categoryViewHolder.tagDescriptionTv.setText(category.getDescription());
                categoryViewHolder.tagCountTv.setText(ResourceUtils.resourceString(R.string.rank_tag_listen_num, category.getCount()));

                holder.itemView.setOnClickListener(v -> {
                    ActivityLauncherStart.startTagMusic(ActivityStartBase.scanForActivity(mContext), category.getTag());
                });
            }

        }
    }

    @Override
    public int getItemCount() {
        return mCategory.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_TOP;
        } else {
            return TYPE_TAG;
        }
    }

    public void setNewData(RankTagsBean rankTagsBean) {
        if (Utils.isNotNull(rankTagsBean)) {
            mRankTagsBean = rankTagsBean;
            mCategory.clear();
            mCategory.addAll(rankTagsBean.getCategory());
            notifyDataSetChanged();
        }
    }

    class TopViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.tv_new_music)
        TextView newMusicTv;

        @BindView(R.id.tv_hot_music)
        TextView hotMusicTv;

        @BindView(R.id.layout_ranking_hot)
        View hotView;

        @BindView(R.id.layout_ranking_new)
        View newView;

        public TopViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


    class CategoryViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_cover)
        HaigeImageView coverIv;

        @BindView(R.id.tv_tag_count)
        TextView tagCountTv;

        @BindView(R.id.tv_tag_title)
        TextView tagTitleTv;

        @BindView(R.id.tv_tag_description)
        TextView tagDescriptionTv;

        public CategoryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
