package com.music.player.haige.app.image.utils;

/**
 * Created by liumingkong on 14-5-5.
 */
public class FileConstants {

    public static final String RESOURCE = "resource";
    public static final String IMAGE_FILE_PREFIX = "file://";
    public static final String IMAGE_RES_PREFIX = "res://";

    public static String resUri(String uri){
        return uri + FileStore.SUFFIX_PNG;
    }
}
