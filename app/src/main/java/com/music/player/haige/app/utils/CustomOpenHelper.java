package com.music.player.haige.app.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.music.player.haige.mvp.model.entity.music.DaoMaster;
import com.music.player.haige.mvp.model.entity.music.SongDao;

import org.greenrobot.greendao.database.Database;

/**
 * Created by xiaochongzi on 17-7-12
 */

public class CustomOpenHelper extends DaoMaster.OpenHelper {

    public CustomOpenHelper(Context context, String name) {
        super(context, name);
    }

    public CustomOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory) {
        super(context, name, factory);
    }

    @Override
    public void onCreate(Database db) {
        super.onCreate(db);
    }

    @Override
    public void onUpgrade(Database db, int oldVersion, int newVersion) {
        if (oldVersion < newVersion) {
            MigrationHelper.getInstance().migrate(db, SongDao.class);
        }
    }
}
