package com.music.player.haige.mvp.model.entity.music;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ArtistInfo {

    private static final String ARTIST = "artistName";

    @Expose
    @SerializedName(ARTIST)
    public LastfmArtist mArtist;
}
