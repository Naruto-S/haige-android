package com.music.player.haige.app.image.release;

import com.facebook.imagepipeline.common.ImageDecodeOptions;
import com.music.player.haige.app.image.loader.listener.OnPostProcessListener;

/**
 * 图片加载选项
 * <p>
 * Created by ZaKi on 2016/9/2.
 */
public class ImageOptions {

    //目标图片宽高
    private int width;
    private int height;
    //是否使用磁盘缓存
    private boolean usedDiskCache;
    //是否使用小缓存
    private boolean usedSmallDiskCache;
    //图片处理监听
    private OnPostProcessListener onPostProcessListener;
    //图片解码配置
    private ImageDecodeOptions imageDecodeOptions;
    //　low请求uri
    private String lowUri;

    public ImageOptions(Builder builder) {

        this.width = builder.width;
        this.height = builder.height;
        this.usedDiskCache = builder.usedDiskCache;
        this.usedSmallDiskCache = builder.usedSmallDiskCache;
        this.onPostProcessListener = builder.onPostProcessListener;
        this.imageDecodeOptions = builder.imageDecodeOptions;
        this.lowUri = builder.lowUri;
    }

    public ImageDecodeOptions getImageDecodeOptions() {
        return imageDecodeOptions;
    }

    public boolean isUsedDiskCache() {
        return usedDiskCache;
    }

    public boolean isUsedSmallDiskCache() {
        return usedSmallDiskCache;
    }

    public String getLowUri() {
        return lowUri;
    }

    public OnPostProcessListener getOnPostProcessListener() {
        return onPostProcessListener;
    }

    public int getWidth() {

        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public static class Builder {
        private int width = 0;
        private int height = 0;
        private boolean usedDiskCache = true;
        private boolean usedSmallDiskCache = false;
        private OnPostProcessListener onPostProcessListener;
        private ImageDecodeOptions imageDecodeOptions;
        private String lowUri;//二级请求uri

        public Builder setWidth(int width) {
            this.width = width;
            return this;
        }

        public Builder setHeight(int height) {
            this.height = height;
            return this;
        }

        public Builder usedDiskCache(boolean usedDiskCache) {
            this.usedDiskCache = usedDiskCache;
            return this;
        }

        public Builder usedSmallDiskCache(boolean usedSmallDiskCache) {
            this.usedSmallDiskCache = usedSmallDiskCache;
            return this;
        }

        public Builder setImageDecodeOptions(ImageDecodeOptions imageDecodeOptions) {
            this.imageDecodeOptions = imageDecodeOptions;
            return this;
        }

        public Builder setLowUri(String uri) {
            this.lowUri = uri;
            return this;
        }

        public ImageOptions build() {
            return new ImageOptions(this);
        }

        public Builder setOnPostProcessListener(OnPostProcessListener onPostProcessListener) {
            this.onPostProcessListener = onPostProcessListener;
            return this;
        }
    }
}
