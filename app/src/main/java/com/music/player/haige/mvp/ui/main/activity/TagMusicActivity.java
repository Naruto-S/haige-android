package com.music.player.haige.mvp.ui.main.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.hc.core.di.component.AppComponent;
import com.music.player.haige.R;
import com.music.player.haige.app.EventBusTags;
import com.music.player.haige.app.SystemUIConfig;
import com.music.player.haige.app.base.BaseListActivity;
import com.music.player.haige.app.base.pref.UserPrefHelper;
import com.music.player.haige.app.base.recyclerview.BaseQuickAdapter;
import com.music.player.haige.app.base.recyclerview.listener.OnItemClickListener;
import com.music.player.haige.app.base.recyclerview.pullrefresh.HaigeRefreshHeader;
import com.music.player.haige.app.constants.IntentConstants;
import com.music.player.haige.di.component.DaggerRankComponent;
import com.music.player.haige.di.module.RankModule;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.presenter.RankFPresenter;
import com.music.player.haige.mvp.ui.login.LoginDialogFragment;
import com.music.player.haige.mvp.ui.main.fragment.PlayMainFragment;
import com.music.player.haige.mvp.ui.main.widget.TagLoopHeader;
import com.music.player.haige.mvp.ui.music.adapter.MusicListAdapter;
import com.music.player.haige.mvp.ui.music.service.MusicServiceConnection;
import com.music.player.haige.mvp.ui.share.DownloadShareDialog;
import com.music.player.haige.mvp.ui.utils.ShareUtils;
import com.music.player.haige.mvp.ui.utils.Utils;

import org.simple.eventbus.Subscriber;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

import static com.music.player.haige.mvp.ui.main.fragment.PlayMainFragment.PLAY_MAIN_CURRENT_PLAY;

public class TagMusicActivity extends BaseListActivity<RankFPresenter> implements MVContract.RankView,
        SystemUIConfig {

    @BindView(R.id.app_fragment_title_tv)
    TextView mTitleTv;

    private String mMusicTag;
    private TagLoopHeader mTagLoopHeader;

    @Override
    public boolean translucentStatusBar() {
        return true;
    }

    @Override
    public int setStatusBarColor() {
        return getResources().getColor(R.color.color_00000000);
    }

    @Override
    public boolean lightStatusBar() {
        return false;
    }

    @Override
    public boolean fitSystemWindows() {
        return true;
    }

    @Override
    public boolean fullScreen() {
        return false;
    }

    @Override
    protected RecyclerView.LayoutManager createLayoutManager() {
        return new LinearLayoutManager(this);
    }

    @Override
    protected OnItemClickListener createItemClickListener() {
        return new OnItemClickListener(false) {
            @Override
            public void SimpleOnItemClick(BaseQuickAdapter adapter, View view, int position) {
                if (Utils.isFastClick()) {
                    return;
                }

                MusicServiceConnection.playAll(adapter.getData(), position, false);
                startPlayMain();
            }

            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                if (Utils.isFastClick()) {
                    return;
                }

                switch (view.getId()) {
                    case R.id.popup_menu:
                        if (UserPrefHelper.isLogin()) {
                            onPopupMenuClick(view, (Song) adapter.getItem(position));
                        } else {
                            LoginDialogFragment.newInstance().show(getSupportFragmentManager(), LoginDialogFragment.class.getName());
                        }
                        break;
                    default:
                        break;
                }
            }
        };
    }

    @Override
    protected RecyclerView.OnScrollListener addRecyclerViewScrollListener() {
        return null;
    }

    @Override
    protected RecyclerView.ItemDecoration createDecoration() {
        return null;
    }

    @Override
    protected RecyclerView.ItemAnimator createItemAnimator() {
        return null;
    }

    @Override
    protected View createRefreshHeader() {
        return new HaigeRefreshHeader(this);
    }

    @Override
    protected Context createContext() {
        return this;
    }

    @Override
    protected BaseQuickAdapter createAdapter() {
        return new MusicListAdapter(this, true);
    }

    @Override
    public void setupActivityComponent(AppComponent appComponent) {
        DaggerRankComponent
                .builder()
                .appComponent(appComponent)
                .rankModule(new RankModule(this))
                .build()
                .inject(this);
    }

    @Override
    public int initView(Bundle savedInstanceState) {
        return R.layout.activity_tag_music;
    }

    @Override
    protected void setupView() {
        super.setupView();

        if (Utils.isNotNull(getIntent())) {
            mMusicTag = getIntent().getStringExtra(IntentConstants.EXTRA_MUSIC_TAG);
        }

        mTitleTv.setText(mMusicTag);
    }

    private void addTagLoopHeader() {
        if (mTagLoopHeader == null) {
            mTagLoopHeader = new TagLoopHeader(this);
            mTagLoopHeader.setOnClickListener(v -> {
                MusicServiceConnection.playAll(mAdapter.getData(), 0, false);
            });
            mAdapter.addHeaderView(mTagLoopHeader, 0);
        }
    }

    @Override
    protected void sendListReq(int pageNum, int pageSize, boolean refresh) {
        mPresenter.loadMusicRank(mMusicTag, pageNum, pageSize, refresh);
    }

    @Override
    protected void reLoadData() {
        fetchPusherList(mAdapter, true);
    }

    @Override
    protected void loadMoreData() {
        fetchPusherList(mAdapter, false);
    }

    @Override
    public void showRefresh(ArrayList arrayList) {
        addTagLoopHeader();

        super.showRefresh(arrayList);

        if (Utils.isNotNull(mTagLoopHeader) && Utils.isNotEmptyCollection(mAdapter.getData())) {
            mTagLoopHeader.setupView(mAdapter.getData().size());
        }
    }

    @Override
    public void showLoadMore(ArrayList arrayList) {
        super.showLoadMore(arrayList);

        if (Utils.isNotNull(mTagLoopHeader) && Utils.isNotEmptyCollection(mAdapter.getData())) {
            mTagLoopHeader.setupView(mAdapter.getData().size());
        }
    }

    @Subscriber
    public void onSubscriber(EventBusTags.MetaChangedEvent event) {
        if (getAdapter() != null) {
            getAdapter().notifyDataSetChanged();
        }
    }

    @Subscriber
    public void onSubscriber(EventBusTags.MAIN tags) {
        switch (tags) {
            case BACK:
                getSupportFragmentManager().popBackStackImmediate();
                break;
        }
    }

    @OnClick({R.id.app_fragment_back_iv})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.app_fragment_back_iv:
                finish();
                break;
        }
    }

    private void onPopupMenuClick(View v, Song song) {
        final PopupMenu menu = new PopupMenu(this, v);
        menu.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.popup_song_add_to_queue:
                    MusicServiceConnection.addToQueue(song);
                    break;
                case R.id.popup_song_add_to_favorite:
                    mPresenter.recordFavoriteSong(song);
                    break;
                case R.id.popup_download:
                    if (ShareUtils.checkDownloadShare(UserPrefHelper.getDownloadMusicCount())) {
                        DownloadShareDialog.newInstance().show(getSupportFragmentManager(), DownloadShareDialog.class.getName());
                    } else {
                        mPresenter.downloadMusic(song);
                    }
                    break;
            }
            return false;
        });
        menu.inflate(R.menu.popup_rank);
        menu.show();
    }

    private void startPlayMain() {
        Fragment fragment = PlayMainFragment.newInstance(PLAY_MAIN_CURRENT_PLAY);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_from_bottom, R.anim.slide_out_to_left, R.anim.slide_in_from_left, R.anim.slide_out_to_bottom);
        transaction.replace(R.id.app_activity_main_foreground_layout, fragment, TagMusicActivity.class.getName());
        transaction.commitAllowingStateLoss();
        transaction.addToBackStack(null);
    }
}
