package com.music.player.haige.mvp.presenter;

import android.text.TextUtils;

import com.hc.core.utils.RxLifecycleUtils;
import com.liulishuo.filedownloader.FileDownloader;
import com.liulishuo.filedownloader.model.FileDownloadStatus;
import com.liulishuo.filedownloader.util.FileDownloadUtils;
import com.music.player.haige.R;
import com.music.player.haige.app.GlobalConfiguration;
import com.music.player.haige.app.HaigeApplication;
import com.music.player.haige.app.base.pref.UserPrefHelper;
import com.music.player.haige.mvp.contract.AppBasePresenter;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.model.entity.music.SongDao;
import com.music.player.haige.mvp.model.entity.rank.RankResponse;
import com.music.player.haige.mvp.ui.music.utils.MusicUtils;
import com.music.player.haige.mvp.ui.utils.Utils;

import java.io.File;
import java.util.Date;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import me.jessyan.rxerrorhandler.handler.ErrorHandleSubscriber;

/**
 * ================================================
 * Created by huangcong on 2018/3/3.
 * ================================================
 */

public class RankFPresenter extends AppBasePresenter<MVContract.RankModel, MVContract.RankView> {

    private SongDao mFavoriteSongDao; // 音乐收藏记录
    private SongDao mDownloadSongDao; // 音乐下载记录

    @Inject
    public RankFPresenter(MVContract.RankModel model, MVContract.RankView view) {
        super(model, view);
        mFavoriteSongDao = GlobalConfiguration.sFavoriteDaoSession.getSongDao();
        mDownloadSongDao = GlobalConfiguration.sDownloadDaoSession.getSongDao();
    }

    public void loadMusicRank(String rankType, int pageNo, int pageSize, boolean refresh) {
        mModel.loadMusicRank(rankType, pageNo, pageSize)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> {
                    if (Utils.isNotNull(mRootView)) {
                        mRootView.loadRequestStarted();
                    }
                })
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .subscribe(new ErrorHandleSubscriber<RankResponse>(mErrorHandler) {
                    @Override
                    public void onNext(RankResponse response) {
                        if (Utils.isNotNull(mRootView)) {
                            if (Utils.isNotNull(response) && Utils.isNotEmptyCollection(response.getData())) {
                                if (refresh) {
                                    mRootView.showRefresh(response.getData());
                                } else {
                                    mRootView.showLoadMore(response.getData());
                                }
                            } else {
                                mRootView.loadEnded();
                            }
                        }
                        mRootView.loadRequestCompleted();
                    }

                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                        if (Utils.isNotNull(mRootView)) {
                            mRootView.loadRequestCompleted();
                            mRootView.showErrorNetwork();
                        }
                    }
                });
    }

    /**
     * 添加喜欢的音乐记录
     */
    public void recordFavoriteSong(Song song) {
        if (!isFavoriteSong(song)) {
            mFavoriteSongDao.insert(song);
            mRootView.showMessage(HaigeApplication.getInstance().getString(R.string.app_add_favorite_success));
        }
    }

    /**
     * 删除喜欢的音乐记录
     */
    public boolean deleteFavoriteSong(Song song) {
        if (isFavoriteSong(song)) {
            mFavoriteSongDao.delete(song);
            return true;
        } else {
            return false;
        }
    }

    public boolean isFavoriteSong(Song song) {
        return mFavoriteSongDao.load(song.songId) != null;
    }

    public boolean downloadMusic(Song song) {
        if (song.isLocal) {
            mRootView.showMessage(HaigeApplication.getInstance().getString(R.string.app_download_local_music));
            return false;
        }
        String url = song.musicPath;
        if (TextUtils.isEmpty(url)) {
            return false;
        }
        // 记录到下载数据库
        if (mDownloadSongDao.load(song.songId) == null) {
            song.setDownloadDate(new Date());
            mDownloadSongDao.insert(song);
        }
        String path = MusicUtils.getDownloadPath(song);
        int id = FileDownloadUtils.generateId(url, path);
        int status = FileDownloader.getImpl().getStatus(id, path);
        System.out.println("===>>> id : " + id + ", status : " + status);
        if (status == FileDownloadStatus.completed || new File(path).exists()) {
            mRootView.showMessage(HaigeApplication.getInstance().getString(R.string.app_download_finish));
            return false;
        } else if (status == FileDownloadStatus.pending || status == FileDownloadStatus.started ||
                status == FileDownloadStatus.connected || status == FileDownloadStatus.progress) {
            mRootView.showMessage(HaigeApplication.getInstance().getString(R.string.app_download_ing));
            return false;
        } else {
            UserPrefHelper.putDownloadMusicCount(UserPrefHelper.getDownloadMusicCount() + 1);
            mRootView.showMessage(HaigeApplication.getInstance().getString(R.string.app_download_join));
        }
        if (!FileDownloader.getImpl().isServiceConnected()) {
            // 开启下载服务
            FileDownloader.getImpl().bindService();
        }
        // 开始下载
        FileDownloader.getImpl().create(url).setPath(path).start();
        return true;
    }

}
