package com.music.player.haige.mvp.model.respository.dataloader;

import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;

import com.music.player.haige.mvp.model.entity.music.Artist;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.model.utils.PreferencesUtility;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.functions.Function;


public class ArtistLoader {

    private static Observable<Artist> getArtist(final Cursor cursor) {
        return Observable.create(new ObservableOnSubscribe<Artist>() {

            @Override
            public void subscribe(ObservableEmitter<Artist> emitter) throws Exception {
                Artist artist = new Artist();
                if (cursor != null) {
                    if (cursor.moveToFirst())
                        artist = new Artist(cursor.getLong(0), cursor.getString(1), cursor.getInt(2), cursor.getInt(3));
                }
                if (cursor != null) {
                    cursor.close();
                }
                emitter.onNext(artist);
                emitter.onComplete();
            }
        });

    }

    private static Observable<List<Artist>> getArtistsForCursor(final Cursor cursor) {
        return Observable.create(new ObservableOnSubscribe<List<Artist>>() {

            @Override
            public void subscribe(ObservableEmitter<List<Artist>> emitter) throws Exception {
                List<Artist> arrayList = new ArrayList<Artist>();
                if ((cursor != null) && (cursor.moveToFirst()))
                    do {
                        arrayList.add(new Artist(cursor.getLong(0), cursor.getString(1), cursor.getInt(2), cursor.getInt(3)));
                    }
                    while (cursor.moveToNext());
                if (cursor != null) {
                    cursor.close();
                }
                emitter.onNext(arrayList);
                emitter.onComplete();
            }
        });
    }

    public static Observable<List<Artist>> getFavouriteArtists(final Context context) {
        return SongLoader.getFavoriteSong(context)
                .flatMap(new Function<List<Song>, Observable<Song>>() {
                    @Override
                    public Observable<Song> apply(List<Song> songs) throws Exception {
                        return Observable.fromIterable(songs);
                    }
                }).distinct(new Function<Song, Long>() {
                    @Override
                    public Long apply(Song song) throws Exception {
                        return song.artistId;
                    }
                }).flatMap(new Function<Song, Observable<Artist>>() {
                    @Override
                    public Observable<Artist> apply(Song song) throws Exception {
                        return ArtistLoader.getArtist(context, song.artistId);
                    }
                }).toList().toObservable();
    }

    public static Observable<List<Artist>> getRecentlyPlayedArtist(final Context context) {
        return TopTracksLoader.getTopRecentSongs(context)
                .flatMap(new Function<List<Song>, Observable<Song>>() {
                    @Override
                    public Observable<Song> apply(List<Song> songs) throws Exception {
                        return Observable.fromIterable(songs);
                    }
                }).distinct(new Function<Song, Long>() {
                    @Override
                    public Long apply(Song song) throws Exception {
                        return song.artistId;
                    }
                }).flatMap(new Function<Song, Observable<Artist>>() {
                    @Override
                    public Observable<Artist> apply(Song song) throws Exception {
                        return ArtistLoader.getArtist(context, song.artistId);
                    }
                }).toList().toObservable();
    }

    public static Observable<List<Artist>> getAllArtists(Context context) {
        return getArtistsForCursor(makeArtistCursor(context, null, null));
    }

    public static Observable<Artist> getArtist(Context context, long id) {
        return getArtist(makeArtistCursor(context, "_id=?", new String[]{String.valueOf(id)}));
    }

    public static Observable<List<Artist>> getArtists(Context context, String paramString) {
        return getArtistsForCursor(makeArtistCursor(context, "artist LIKE ?", new String[]{"%" + paramString + "%"}));
    }

    private static Cursor makeArtistCursor(Context context, String selection, String[] paramArrayOfString) {
        final String artistSortOrder = PreferencesUtility.getInstance(context).getArtistSortOrder();
        Cursor cursor = context.getContentResolver().query(MediaStore.Audio.Artists.EXTERNAL_CONTENT_URI, new String[]{"_id", "artist", "number_of_albums", "number_of_tracks"}, selection, paramArrayOfString, artistSortOrder);
        return cursor;
    }
}
