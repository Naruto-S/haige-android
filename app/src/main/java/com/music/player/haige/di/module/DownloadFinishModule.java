package com.music.player.haige.di.module;

import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.DownloadFinishModel;

import dagger.Module;
import dagger.Provides;

/**
 * ================================================
 * Created by huangcong on 2018/3/20.
 * ================================================
 */
@Module
public class DownloadFinishModule {
    private MVContract.DownloadFinishView view;

    public DownloadFinishModule(MVContract.DownloadFinishView view) {
        this.view = view;
    }

    @FragmentScope
    @Provides
    MVContract.DownloadFinishView provideView() {
        return this.view;
    }

    @FragmentScope
    @Provides
    MVContract.DownloadFinishModel provideModel(DownloadFinishModel model) {
        return model;
    }

}
