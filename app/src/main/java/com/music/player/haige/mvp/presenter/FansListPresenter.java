package com.music.player.haige.mvp.presenter;

import com.hc.core.utils.RxLifecycleUtils;
import com.music.player.haige.mvp.contract.AppBasePresenter;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.Api;
import com.music.player.haige.mvp.model.entity.action.FollowActionRequest;
import com.music.player.haige.mvp.model.entity.user.FollowUserResponse;
import com.music.player.haige.mvp.ui.utils.Utils;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;
import me.jessyan.rxerrorhandler.handler.ErrorHandleSubscriber;

public class FansListPresenter extends AppBasePresenter<MVContract.FansListModel, MVContract.FansListView> {

    @Inject
    RxErrorHandler mErrorHandler;

    @Inject
    public FansListPresenter(MVContract.FansListModel model, MVContract.FansListView view) {
        super(model, view);
    }

    public void getFollowing(String userId, int pageNo, int pageSize, boolean refresh) {
        mModel.getFollowing(userId, pageNo, pageSize)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> {
                    if (Utils.isNotNull(mRootView)) {
                        mRootView.loadRequestStarted();
                    }
                })
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .subscribe(new ErrorHandleSubscriber<FollowUserResponse>(mErrorHandler) {
                    @Override
                    public void onNext(FollowUserResponse response) {
                        if (Utils.isNotNull(mRootView)) {
                            if (Utils.isNotNull(response) && Utils.isNotEmptyCollection(response.getData())) {
                                if (refresh) {
                                    mRootView.showRefresh(response.getData());
                                } else {
                                    mRootView.showLoadMore(response.getData());
                                }
                            } else {
                                mRootView.loadEnded();
                            }
                        }
                        mRootView.loadRequestCompleted();
                    }

                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                        if (Utils.isNotNull(mRootView)) {
                            mRootView.loadRequestCompleted();
                            mRootView.showErrorNetwork();
                        }
                    }
                });
    }

    public void getFollower(String userId, int pageNo, int pageSize, boolean refresh) {
        mModel.getFollower(userId, pageNo, pageSize)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> {
                    if (Utils.isNotNull(mRootView)) {
                        mRootView.loadRequestStarted();
                    }
                })
                .compose(RxLifecycleUtils.bindToLifecycle(mRootView))
                .subscribe(new ErrorHandleSubscriber<FollowUserResponse>(mErrorHandler) {
                    @Override
                    public void onNext(FollowUserResponse response) {
                        if (Utils.isNotNull(mRootView)) {
                            if (Utils.isNotNull(response) && Utils.isNotEmptyCollection(response.getData())) {
                                if (refresh) {
                                    mRootView.showRefresh(response.getData());
                                } else {
                                    mRootView.showLoadMore(response.getData());
                                }
                            } else {
                                mRootView.loadEnded();
                            }
                        }
                        mRootView.loadRequestCompleted();
                    }

                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                        if (Utils.isNotNull(mRootView)) {
                            mRootView.loadRequestCompleted();
                            mRootView.showErrorNetwork();
                        }
                    }
                });
    }

    public void follow(FollowActionRequest request) {
        buildObservable(mModel.follow(request), Api.Action.USER_FOLLOW);
    }

    public void unfollow(FollowActionRequest request) {
        buildObservable(mModel.unfollow(request), Api.Action.USER_UNFOLLOW);
    }
}
