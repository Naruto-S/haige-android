package com.music.player.haige.mvp.ui.user;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hc.core.di.component.AppComponent;
import com.music.player.haige.R;
import com.music.player.haige.app.EventBusTags;
import com.music.player.haige.app.base.AppBaseFragment;
import com.music.player.haige.app.base.pref.UserPrefHelper;
import com.music.player.haige.app.event.EditUserInfoEvent;
import com.music.player.haige.app.event.UpdateEditInfoEvent;
import com.music.player.haige.app.image.loader.AvatarLoader;
import com.music.player.haige.app.image.widget.HaigeImageView;
import com.music.player.haige.di.component.DaggerEditProfileComponent;
import com.music.player.haige.di.module.EditProfileModule;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.Api;
import com.music.player.haige.mvp.model.entity.BaseResponse;
import com.music.player.haige.mvp.model.entity.user.ModifyUserRequest;
import com.music.player.haige.mvp.model.entity.user.UserBean;
import com.music.player.haige.mvp.presenter.EditProfilePresenter;
import com.music.player.haige.mvp.ui.user.utils.AvatarUtils;
import com.music.player.haige.mvp.ui.user.utils.UserViewUtils;
import com.music.player.haige.mvp.ui.utils.DebugLogger;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;
import com.music.player.haige.mvp.ui.utils.Utils;
import com.music.player.haige.mvp.ui.widget.dialog.SystemDialog;

import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;

import static com.music.player.haige.mvp.ui.user.EditFragment.TYPE_EDIT_DESC;
import static com.music.player.haige.mvp.ui.user.EditFragment.TYPE_EDIT_NAME;
import static com.music.player.haige.mvp.ui.user.utils.UserViewUtils.GENDAR_FEMALE;
import static com.music.player.haige.mvp.ui.user.utils.UserViewUtils.GENDAR_MALE;
import static com.music.player.haige.mvp.ui.user.utils.UserViewUtils.setUserBirthday;

/**
 * Created by Naruto on 2018/5/13.
 */

public class EditProfileFragment extends AppBaseFragment<EditProfilePresenter> implements MVContract.EditProfileView {

    @BindView(R.id.hiv_avatar)
    HaigeImageView mAvatarHiv;

    @BindView(R.id.tv_name)
    TextView mNameTv;

    @BindView(R.id.tv_id)
    TextView mIdTv;

    @BindView(R.id.tv_gender)
    TextView mGenderTv;

    @BindView(R.id.tv_birthday)
    TextView mBirthdayTv;

    @BindView(R.id.tv_desc)
    TextView mDescTv;

    @BindView(R.id.app_fragment_save_tv)
    TextView mSaveTv;

    @Inject
    RxErrorHandler mErrorHandler;

    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private String mCurrentBirthday;
    private UserBean mUser;

    public static EditProfileFragment newInstance() {
        EditProfileFragment fragment = new EditProfileFragment();
        return fragment;
    }

    @Override
    public void setupFragmentComponent(AppComponent component) {
        DaggerEditProfileComponent
                .builder()
                .appComponent(component)
                .editProfileModule(new EditProfileModule(this))
                .build()
                .inject(this);
    }

    @Override
    public View initView(LayoutInflater inflater, ViewGroup group, Bundle bundle) {
        return inflater.inflate(R.layout.app_fragment_edit_profile, group, false);
    }

    @Override
    public void initData(Bundle bundle) {
        mUser = UserPrefHelper.getMeUser();
        setUserView();
    }

    private void setUserView() {
        try {
            if (Utils.isNotNull(mUser)) {
                AvatarLoader.loadUserAvatar(mUser.getAvatarUrl(),
                        R.drawable.ic_main_default_avatar,
                        R.drawable.ic_main_default_avatar,
                        mAvatarHiv);
                mIdTv.setText(String.valueOf(mUser.getHaigeId()));
                mNameTv.setText(mUser.getNickName());
                mGenderTv.setText(mUser.getGender());
                UserViewUtils.setUserDesc(mUser, mDescTv);
                mCurrentBirthday = setUserBirthday(mUser, mBirthdayTv);
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void onSuccess(Api.Action action, Object o) {
        super.onSuccess(action, o);

        switch (action) {
            case MODIFY_USER_PROFILE:
                BaseResponse response = (BaseResponse) o;
                String code = String.valueOf(response.getCode());
                String msg = response.getMsg();

                if (Api.State.LOGIN_FAILURE.equalsCode(code)) {
                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                } else if (Api.State.OK.equalsCode(code)) {
                    Toast.makeText(getActivity(), R.string.common_edit_success, Toast.LENGTH_SHORT).show();
                    EventBus.getDefault().post(EventBusTags.UI.EDIT_UPDATE_USER);
                    EventBus.getDefault().post(EventBusTags.MAIN.BACK);
                }
                break;
        }
    }

    @Override
    public void onError(Api.Action action, String code, String message) {
        super.onError(action, code, message);

        switch (action) {
            case MODIFY_USER_PROFILE:
                Toast.makeText(getActivity(), R.string.common_edit_error, Toast.LENGTH_SHORT).show();
                break;
        }
    }

    public void onBackPressed() {
        if (mSaveTv.isEnabled()) {
            new SystemDialog().create(getChildFragmentManager())
                    .setContent(ResourceUtils.resourceString(R.string.user_edit_profile_exit_dialog_title))
                    .setOk(ResourceUtils.resourceString(R.string.app_save))
                    .setCancle(ResourceUtils.resourceString(R.string.app_exit))
                    .setOnOkListener(() -> {
                        modifyUserProfile();
                    })
                    .setOnCancelListener(() -> {
                        EventBus.getDefault().post(EventBusTags.MAIN.BACK);
                    })
                    .show();
        } else {
            EventBus.getDefault().post(EventBusTags.MAIN.BACK);
        }
    }

    @Subscriber
    public void onSubscriber(UpdateEditInfoEvent event) {
        switch (event.getType()) {
            case TYPE_EDIT_NAME:
                mNameTv.setText(event.getCotent());
                break;
            case TYPE_EDIT_DESC:
                mDescTv.setText(event.getCotent());
                break;
        }
        mSaveTv.setEnabled(true);
    }

    @Subscriber
    public void onSubscriber(EventBusTags.UI tags) {
        switch (tags) {
            case REQUEST_CAMERA_PERMISSION_SUCCESS:
                AvatarUtils.dispatchTakePictureIntent(getActivity());
                break;
            case EDIT_UPDATE_AVATAR:
                mUser = UserPrefHelper.getMeUser();
                setUserView();
                mSaveTv.setEnabled(true);
                break;
        }
    }

    @OnClick({R.id.app_fragment_back_iv, R.id.iv_avatar_camera, R.id.app_fragment_save_tv,
            R.id.layout_name, R.id.layout_gender, R.id.layout_birthday, R.id.layout_desc})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.app_fragment_back_iv:
                getActivity().onBackPressed();
                break;
            case R.id.app_fragment_save_tv:
                modifyUserProfile();
                break;
            case R.id.iv_avatar_camera:
                showAvatarDialog();
                break;
            case R.id.layout_name:
                EventBus.getDefault().post(new EditUserInfoEvent(TYPE_EDIT_NAME));
                break;
            case R.id.layout_gender:
                showGenderDialog();
                break;
            case R.id.layout_birthday:
                gotoEditBirthday();
                break;
            case R.id.layout_desc:
                EventBus.getDefault().post(new EditUserInfoEvent(TYPE_EDIT_DESC));
                break;
        }
    }

    private void modifyUserProfile() {
        ModifyUserRequest request = new ModifyUserRequest();
        request.setUserId(mUser.getUserId());
        request.setNickName(mNameTv.getText().toString());
        request.setGender(mGenderTv.getText().toString());
        request.setBirthday(mBirthdayTv.getText().toString());
        request.setDesc(mDescTv.getText().toString());
        request.setYear(UserViewUtils.getBirthdayYear(mBirthdayTv.getText().toString()));
        request.setProvince(mUser.getProvince());
        request.setCity(mUser.getCity());

        mPresenter.modifyUserProfile(request);
    }

    private void showAvatarDialog() {
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_bottom_avatar, null);
        TextView choose = view.findViewById(R.id.tv_profile_avatar_choose);
        TextView take = view.findViewById(R.id.tv_profile_avatar_take);

        final Dialog mBottomSheetDialog = new Dialog(getActivity(), R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.show();

        choose.setOnClickListener(v -> {
            AvatarUtils.startGalleryIntent(getActivity());
            mBottomSheetDialog.dismiss();
        });

        take.setOnClickListener(v -> {
            EventBus.getDefault().post(EventBusTags.MAIN.REQUEST_CAMERA_PERMISSION);
            mBottomSheetDialog.dismiss();
        });
    }

    private void gotoEditBirthday() {
        try {
            // 默认时间为之前用户选择的时间
            final Calendar c = Calendar.getInstance();
            c.setTimeInMillis(simpleDateFormat.parse(mCurrentBirthday).getTime());
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            // 当前时间
            c.setTimeInMillis(System.currentTimeMillis());
            final int currentYear = c.get(Calendar.YEAR);
            if ((year == currentYear - UserViewUtils.USER_MIN_AGE || year == currentYear - UserViewUtils.USER_MAX_AGE) && month == 0 && day == 1) {
                day += 1;
            }
            DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                    Calendar c = Calendar.getInstance();
                    c.set(year, month, day);
                    mCurrentBirthday = simpleDateFormat.format(c.getTime());
                    mBirthdayTv.setText(mCurrentBirthday);
                    mSaveTv.setEnabled(true);
                }

            }, year, month, day);
            DatePicker datePicker = datePickerDialog.getDatePicker();
            datePicker.setMaxDate(simpleDateFormat.parse(currentYear + "-01-01").getTime()); //最小为0岁
            datePicker.setMinDate(simpleDateFormat.parse((currentYear - UserViewUtils.USER_MAX_AGE) + "-01-01").getTime()); // 最大为100岁
            datePickerDialog.show();
        } catch (Exception e) {
            DebugLogger.e(e);
            mBirthdayTv.setText(mCurrentBirthday);
        }
    }

    private void showGenderDialog() {
        View view = getLayoutInflater().inflate(R.layout.dialog_bottom_gender, null);
        FrameLayout maleFl = view.findViewById(R.id.fl_profile_gender_male);
        FrameLayout femaleFl = view.findViewById(R.id.fl_profile_gender_female);
        TextView male = view.findViewById(R.id.tv_profile_gender_male);
        TextView female = view.findViewById(R.id.tv_profile_gender_female);
        ImageView maleIv = view.findViewById(R.id.iv_profile_gender_male);
        ImageView femaleIv = view.findViewById(R.id.iv_profile_gender_female);

        male.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_edit_male, 0, 0, 0);
        female.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_edit_female, 0, 0, 0);

        if (mUser.getGender().equals(GENDAR_MALE)) {
            maleIv.setVisibility(View.VISIBLE);
        } else if (mUser.getGender().equals(GENDAR_FEMALE)) {
            femaleIv.setVisibility(View.VISIBLE);
        }

        final Dialog mBottomSheetDialog = new Dialog(getActivity(), R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.show();

        maleFl.setOnClickListener(v -> {
            mSaveTv.setEnabled(true);
            mGenderTv.setText(GENDAR_MALE);
            mBottomSheetDialog.dismiss();
        });

        femaleFl.setOnClickListener(v -> {
            mSaveTv.setEnabled(true);
            mGenderTv.setText(GENDAR_FEMALE);
            mBottomSheetDialog.dismiss();
        });

    }
}
