package com.music.player.haige.mvp.model.utils;

import com.google.gson.Gson;
import com.music.player.haige.mvp.model.entity.user.UserBean;
import com.music.player.haige.mvp.ui.utils.Utils;

import org.greenrobot.greendao.converter.PropertyConverter;

/**
 * GreenDao数据转换
 */
public class UserConverter implements PropertyConverter<UserBean, String> {

    //将数据库中的值，转化为实体Bean类对象(比如List<String>)
    @Override
    public UserBean convertToEntityProperty(String databaseValue) {
        if (Utils.isEmptyString(databaseValue)) {
            return null;
        } else {
            Gson gson = new Gson();
            return gson.fromJson(databaseValue, UserBean.class);
        }
    }

    //将实体Bean类(比如List<String>)转化为数据库中的值(比如String)
    @Override
    public String convertToDatabaseValue(UserBean entityProperty) {
        if (Utils.isNull(entityProperty)) {
            return "";
        } else {
            Gson gson = new Gson();
            return gson.toJson(entityProperty);
        }
    }
}  