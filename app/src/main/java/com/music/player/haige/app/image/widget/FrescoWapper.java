package com.music.player.haige.app.image.widget;

import android.graphics.drawable.Animatable;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.AbstractDraweeControllerBuilder;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.imagepipeline.common.ImageDecodeOptions;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.image.ImageInfo;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.music.player.haige.app.image.loader.listener.OnImageLoaderListener;
import com.music.player.haige.app.image.release.DisplayOptions;
import com.music.player.haige.app.image.release.ImageOptions;
import com.music.player.haige.app.image.utils.FileConstants;
import com.music.player.haige.mvp.ui.utils.DebugLogger;

import java.io.File;

/**
 * 继承ImageView和SimpleDraweeView的控件功能抽象
 * <p>
 * Created by ZaKi on 2016/9/3.
 */
public abstract class FrescoWapper implements ImageFetcher {

    public static final String TAG = FrescoWapper.class.getSimpleName();

    public FrescoWapper() {
        //每个FrescoWapper对应一个DisplayOptions
    }

    public static ImageRequest obtainRequest(String uri, ImageOptions imageOptions) {
        Uri endUri = null;
        if (!TextUtils.isEmpty(uri) && uri.startsWith(FileConstants.IMAGE_FILE_PREFIX)) {
            //有的路径会有问题，分别处理以防万一
            endUri = Uri.fromFile(new File(uri.replace(FileConstants.IMAGE_FILE_PREFIX, "")));
        } else {
            endUri = Uri.parse(uri);
        }
        ImageRequestBuilder requestBuilder = ImageRequestBuilder.newBuilderWithSource(endUri).setAutoRotateEnabled(true);//Fresco默认不保存gif到缓存，此设置先显示第一帧
        if (imageOptions != null) {
            if (imageOptions.getImageDecodeOptions() != null) {
                requestBuilder.setImageDecodeOptions(imageOptions.getImageDecodeOptions());
            }
            if (!imageOptions.isUsedDiskCache()) {
                //不使用磁盘缓存
                requestBuilder.disableDiskCache();
            }
            if (imageOptions.getWidth() > 0 && imageOptions.getHeight() > 0) {
                //目标图片大小
                requestBuilder.setResizeOptions(new ResizeOptions(imageOptions.getWidth(), imageOptions.getHeight()));
            }
            if (imageOptions.isUsedSmallDiskCache()) {
                //使用小的磁盘缓存，前提是已经配置了小磁盘缓存
                requestBuilder.setCacheChoice(ImageRequest.CacheChoice.SMALL);
            }
            if (imageOptions.getOnPostProcessListener() != null) {
                //图片后处理
                requestBuilder.setPostprocessor(imageOptions.getOnPostProcessListener());
            }
        } else {
            requestBuilder.setImageDecodeOptions(ImageDecodeOptions.newBuilder()
                    .setForceStaticImage(false)
                    .setDecodePreviewFrame(true)
                    .setUseLastFrameForPreview(true)
                    .build());

        }
        return requestBuilder.build();
    }

    public static ImageRequestBuilder obtainRequestBuilder(String uri) {
        if (TextUtils.isEmpty(uri)) {
            return null;
        }
        ImageRequestBuilder requestBuilder = ImageRequestBuilder.newBuilderWithSource(Uri.parse(uri)).setAutoRotateEnabled(true);//Fresco默认不保存gif到缓存，此设置先显示第一帧
        return requestBuilder;
    }

    @Override
    public void setImageURI(String uri) {
        //nothing to do
    }

    @Override
    public void setImageURI(String uri, OnImageLoaderListener onImageLoaderListener) {
        //nothing to do
    }

    @Override
    public void setImageURI(final String uri, DisplayOptions displayOptions, final OnImageLoaderListener onImageLoaderListener) {
        if (TextUtils.isEmpty(uri)) {
            DebugLogger.d(TAG, "image uri is null");
            return;
        }

        AbstractDraweeControllerBuilder controllerBuilder = obtainController(displayOptions);
        filterController(uri, controllerBuilder, displayOptions);
        if (onImageLoaderListener != null) {
            controllerBuilder.setControllerListener(new BaseControllerListener<ImageInfo>() {
                @Override
                public void onFinalImageSet(final String id, final ImageInfo imageInfo, Animatable animatable) {
                    if (imageInfo == null) {
                        return;
                    }
                    if (animatable == null) {
                        //静态图片
                        onImageLoaderListener.onImageLoadComplete(uri, imageInfo, false, animatable, obtainView());
                    } else {
                        //动图
                        onImageLoaderListener.onImageLoadComplete(uri, imageInfo, true, animatable, obtainView());
                    }
                }

                @Override
                public void onFailure(final String id, final Throwable throwable) {
                    onImageLoaderListener.onImageLoadFail(uri, throwable, obtainView());
                }
            });
        }
        //匹配图片样式
        DebugLogger.d(TAG, "setPlaceholderImage image uri:" + uri);
        setHierarchy(applyOptions(displayOptions));
        setController(controllerBuilder.build());
    }

    private AbstractDraweeControllerBuilder obtainController(DisplayOptions displayOptions) {
        return Fresco.newDraweeControllerBuilder()
                .setAutoPlayAnimations(displayOptions.isAutoPlayAnimations())
                .setOldController(getController());
    }

    private void filterController(String uri, AbstractDraweeControllerBuilder controllerBuilder, DisplayOptions displayOptions) {
        if (controllerBuilder != null) {
            ImageOptions imageOptions = displayOptions.getImageOptions();
            ImageRequest imageRequest = obtainRequest(uri, imageOptions);
            //一级请求
            controllerBuilder.setImageRequest(imageRequest);
            ImageOptions lowImageOptions = displayOptions.getLowImageOptions();
            if (lowImageOptions != null) {
                //二级请求
                String lowUri = lowImageOptions.getLowUri();
                if (TextUtils.isEmpty(lowUri)) {
                    lowUri = uri;
                }
                controllerBuilder.setLowResImageRequest(obtainRequest(lowUri, lowImageOptions));
            }
        }
    }

    //子类返回各自的DraweeController
    protected abstract DraweeController getController();

    //
    protected abstract void setController(DraweeController controller);

    protected abstract View obtainView();

    //
    protected abstract GenericDraweeHierarchy getHierarchy();

    //
    protected abstract void setHierarchy(GenericDraweeHierarchy hierarchy);

    /**
     * 子类调用这个方法可以应用配置的Options
     *
     * @param options
     * @return
     */
    protected GenericDraweeHierarchy applyOptions(DisplayOptions options) {
        if (options != null) {
            return buildParams(options);
        } else {
            return getHierarchy();
        }
    }

    private GenericDraweeHierarchy buildParams(DisplayOptions options) {
        GenericDraweeHierarchy hierarchy = getHierarchy();
        if (options.getImageResOnLoading() > 0) {
            try {
                hierarchy.setPlaceholderImage(options.getImageResOnLoading());
            } catch (Throwable throwable) {
                DebugLogger.d(TAG, "setPlaceholderImage oom:" + options.toString());
                DebugLogger.e(TAG, throwable);
            }
        } else if (options.getImageResOnFail() > 0) {
            hierarchy.setFailureImage(options.getImageResOnFail());
        } else if (options.getImageRoundRadii() != null && options.getImageRoundRadii().length == 4) {
            int[] radii = options.getImageRoundRadii();
            RoundingParams roundingParams = RoundingParams.fromCornersRadii(radii[0], radii[1], radii[2], radii[3]);
            if (options.getImageBorderWidth() > 0) {
                roundingParams.setBorderWidth(options.getImageBorderWidth());
            }
            if (options.getImageBorderColor() > 0) {
                roundingParams.setBorderWidth(options.getImageBorderColor());
            }
            hierarchy.setRoundingParams(roundingParams);
        } else if (options.getAsCircle()) {
            RoundingParams roundingParams = RoundingParams.asCircle();
            hierarchy.setRoundingParams(roundingParams);
        }
        hierarchy.setActualImageScaleType(options.getImageScaleType());
        return hierarchy;
    }
}
