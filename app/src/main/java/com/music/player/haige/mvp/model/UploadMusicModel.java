package com.music.player.haige.mvp.model;

import com.hc.base.mvp.BaseModel;
import com.hc.core.integration.IRepositoryManager;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.service.ApiService;
import com.music.player.haige.mvp.model.entity.BaseResponse;
import com.music.player.haige.mvp.model.entity.action.ActionRequest;
import com.music.player.haige.mvp.model.entity.music.UploadMusicResponse;
import com.music.player.haige.mvp.model.entity.recommend.RecommendResponse;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * ================================================
 * Created by huangcong on 2018/2/27.
 * ================================================
 */

public class UploadMusicModel extends BaseModel implements MVContract.UploadMusicModel {


    @Inject
    public UploadMusicModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public Observable<RecommendResponse> getUploadMusic(String userId, int pageNo, int pageSize) {
        return Observable.just(mRepositoryManager.obtainRetrofitService(ApiService.class).getUploadMusic(userId, pageNo, pageSize))
                .flatMap(observable -> observable);
    }

    @Override
    public Observable<UploadMusicResponse> getUploadMusicStatus(int pageNo, int pageSize) {
        return Observable.just(mRepositoryManager.obtainRetrofitService(ApiService.class).getUploadMusicStatus(pageNo, pageSize))
                .flatMap(observable -> observable);
    }

    @Override
    public Observable<BaseResponse> deleteMusic(ActionRequest request) {
        return Observable.just(mRepositoryManager.obtainRetrofitService(ApiService.class).deleteMusic(request))
                .flatMap(observable -> observable);
    }
}
