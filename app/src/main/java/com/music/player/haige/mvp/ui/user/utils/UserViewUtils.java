package com.music.player.haige.mvp.ui.user.utils;

import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.view.View;
import android.widget.TextView;

import com.music.player.haige.R;
import com.music.player.haige.app.base.pref.UserPrefHelper;
import com.music.player.haige.mvp.model.entity.user.UserBean;
import com.music.player.haige.mvp.ui.utils.DebugLogger;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;
import com.music.player.haige.mvp.ui.utils.Utils;
import com.music.player.haige.mvp.ui.utils.ViewVisibleUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static com.hc.base.utils.DateUtils.YMD;
import static com.music.player.haige.mvp.model.entity.user.UserBean.USER_FOLLOW_STAUS_FOLLOW_ME;
import static com.music.player.haige.mvp.model.entity.user.UserBean.USER_FOLLOW_STAUS_FOLLOW_OTHER;
import static com.music.player.haige.mvp.model.entity.user.UserBean.USER_FOLLOW_STAUS_MUTUAL;
import static com.music.player.haige.mvp.model.entity.user.UserBean.USER_FOLLOW_STAUS_UNFOLLOW;
import static com.music.player.haige.mvp.ui.user.FansListActivity.TYPE_FOLLOWER;
import static com.music.player.haige.mvp.ui.user.FansListActivity.TYPE_FOLLOWING;

/**
 * Created by Naruto on 2018/5/13.
 */

public class UserViewUtils {

    public static final String GENDAR_FEMALE = "女";
    public static final String GENDAR_MALE = "男";
    public static final String GENDAR_UNKNOWN = "未知";

    public static int USER_MIN_AGE = 17;
    public static int USER_MAX_AGE = 80;

    public static void setGenderDrawables(@DrawableRes int resid, TextView textView) {
        Drawable drawable = ResourceUtils.getDrawable(resid);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        textView.setCompoundDrawables(drawable, null, null, null);
    }

    public static void setUserGenderAndAge(UserBean user, TextView textView) {
        if (Utils.isNull(user) || Utils.isNull(textView)) return;

        if (Utils.isNotEmptyString(user.getAge())) {
            textView.setText(user.getAge());
        }

        if (Utils.isNotEmptyString(user.getGender())) {
            if (user.getGender().equals(GENDAR_MALE)) {
                textView.setBackgroundResource(R.drawable.bg_user_age_boy);
                setGenderDrawables(R.drawable.ic_gender_boy, textView);
            } else if (user.getGender().equals(GENDAR_FEMALE)) {
                textView.setBackgroundResource(R.drawable.bg_user_age_girl);
                setGenderDrawables(R.drawable.ic_gender_girl, textView);
            } else {
                textView.setBackgroundResource(R.drawable.bg_user_age_boy);
            }
        }
    }

    public static void setUserProvince(UserBean user, TextView textView) {
        if (Utils.isNull(user) || Utils.isNull(textView)) return;

        if (Utils.isEmptyString(user.getProvince())) {
            textView.setVisibility(View.GONE);
        } else {
            textView.setText(user.getProvince());
            textView.setVisibility(View.VISIBLE);
        }
    }

    public static void setUserDesc(UserBean user, TextView textView) {
        if (Utils.isNull(user) || Utils.isNull(textView)) return;

        if (Utils.isEmptyString(user.getDesc())) {
            textView.setText(ResourceUtils.resourceString(R.string.user_desc_default));
        } else {
            textView.setText(user.getDesc());
        }
    }

    public static String setUserBirthday(UserBean user, TextView textView) {
        String birthday = user.getBirthday();
        if (Utils.isEmptyString(birthday)) {
            birthday  = getDefaultBirthday(user);
            textView.setText(birthday);
        } else {
            textView.setText(birthday);
        }
        return birthday;
    }

    public static String getDefaultBirthday(UserBean user) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        int userYear = Utils.isNotEmptyString(user.getYear()) ? Integer.parseInt(user.getYear()) : calendar.get(Calendar.YEAR);
        calendar.set(userYear, 0, 1);
        String birthday = simpleDateFormat.format(calendar.getTime());

        DebugLogger.e("getDefaultBirthday", userYear + " " + birthday);
        return birthday;
    }

    public static void setUserName(UserBean user, TextView textView) {
        if (Utils.isNull(user) || Utils.isNull(textView)) return;

        textView.setText(user.getNickName());
    }

    public static void setUserHaigeId(UserBean user, TextView textView) {
        if (Utils.isNull(user) || Utils.isNull(textView)) return;

        textView.setText(ResourceUtils.resourceString(R.string.user_profile_haige_id, String.valueOf(user.getHaigeId())));
    }

    /**
     * 把字符串转为日期
     * @param strDate
     * @return
     */
    public static String getBirthdayYear(String strDate) {
        try {
            DateFormat df = new SimpleDateFormat(YMD);
            Date date = df.parse(strDate);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            return String.valueOf(calendar.get(Calendar.YEAR));
        } catch (Exception e) {
            return "";
        }
    }

    public static void switchFollowStatus(TextView textView, UserBean user, int type) {
        if (Utils.isNull(user) || Utils.isNull(textView)) return;

        ViewVisibleUtils.setVisibleGone(textView, !UserPrefHelper.isMe(user));

        if (type == TYPE_FOLLOWING) {
            switch (user.getFollowStatus()) {
                case USER_FOLLOW_STAUS_UNFOLLOW:
                case USER_FOLLOW_STAUS_FOLLOW_ME:
                    textView.setText(ResourceUtils.resourceString(R.string.common_follow));
                    textView.setBackgroundResource(R.drawable.bg_follow_btn);
                    break;
                case USER_FOLLOW_STAUS_FOLLOW_OTHER:
                    textView.setText(ResourceUtils.resourceString(R.string.common_followed));
                    textView.setBackgroundResource(R.drawable.bg_followed_btn);
                    break;
                case USER_FOLLOW_STAUS_MUTUAL:
                    textView.setText(ResourceUtils.resourceString(R.string.common_mutual_follow));
                    textView.setBackgroundResource(R.drawable.bg_followed_btn);
                    break;
            }
        } else if (type == TYPE_FOLLOWER) {
            switch (user.getFollowStatus()) {
                case USER_FOLLOW_STAUS_UNFOLLOW:
                case USER_FOLLOW_STAUS_FOLLOW_OTHER:
                    break;
                case USER_FOLLOW_STAUS_FOLLOW_ME:
                    textView.setText(ResourceUtils.resourceString(R.string.common_followed));
                    textView.setBackgroundResource(R.drawable.bg_followed_btn);
                    break;
                case USER_FOLLOW_STAUS_MUTUAL:
                    textView.setText(ResourceUtils.resourceString(R.string.common_mutual_follow));
                    textView.setBackgroundResource(R.drawable.bg_followed_btn);
                    break;
            }
        }
    }
}
