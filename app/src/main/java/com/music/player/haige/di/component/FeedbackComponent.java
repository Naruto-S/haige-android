package com.music.player.haige.di.component;

import com.hc.core.di.component.AppComponent;
import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.di.module.FeedbackModule;
import com.music.player.haige.mvp.ui.setting.fragment.FeedbackFragment;

import dagger.Component;

/**
 * ================================================
 * Created by huangcong on 2018/2/25.
 * ================================================
 */

@FragmentScope
@Component(modules = {FeedbackModule.class}, dependencies = AppComponent.class)
public interface FeedbackComponent {

    void inject(FeedbackFragment fragment);
}
