package com.music.player.haige.app;

import android.content.Context;

import com.music.player.haige.R;
import com.music.player.haige.mvp.model.utils.StringUtils;

import org.simple.eventbus.EventBus;

/**
 * ================================================
 * 放置 {@link EventBus} 的 Tag ,便于检索
 * <p>
 * ================================================
 */
public interface EventBusTags {

    enum MAIN {
        BACK,
        MUSIC,
        MUSIC_PLAY,
        MUSIC_PLAY_LIST,
        RECENT_PLAY,
        FAVORITE,
        SETTING,
        SEARCH,
        SEARCH_LOCAL,
        SEARCH_RECENT,
        DOWNLOAD,
        PLAY_HIDE,
        PLAY_SHOW,
        SETTING_FEEDBACK,
        SETTING_ABOUT,
        SETTING_POLICY,
        SETTING_COPYRIGHT,
        USER_PROFILE,
        USER_PROFILE_EDIT,
        REQUEST_CAMERA_PERMISSION,
        UPDATE_DIALOG,
    }

    enum DOWNLOAD{
        FINISH,
    }

    enum UI {
        FIND_RECOMMEND,
        FIND_RANKING,
        UPDATE_USER_UI,
        LOGIN_LOGOUT,
        REQUEST_CAMERA_PERMISSION_SUCCESS,
        UPDATE_MINE_UI,
        EDIT_UPDATE_USER,
        EDIT_UPDATE_AVATAR,
        GO_TO_RECOMMEND,
    }

    enum PlayMain {
        BACK,
        CURRENT_LIST,
        CURRENT_PLAY,
    }

    class FavoriteSongEvent {
    }

    class MetaChangedEvent {

        private long songId;
        private String songName;
        private String artistName;
        private long albumId;
        private boolean isLocal;
        private String songCover;
        private int likeCount;
        private int shareCount;
        private int downloadCount;
        private String songUrl;

        public MetaChangedEvent(long songId,
                                String songName,
                                String artistName,
                                long albumId,
                                boolean isLocal,
                                String songCover,
                                int likeCount,
                                int shareCount,
                                int downloadCount,
                                String songUrl) {
            this.songId = songId;
            this.songName = songName;
            this.artistName = artistName;
            this.albumId = albumId;
            this.isLocal = isLocal;
            this.songCover = songCover;
            this.likeCount = likeCount;
            this.shareCount = shareCount;
            this.downloadCount = downloadCount;
            this.songUrl = songUrl;
        }

        public long getSongId() {
            return songId;
        }

        public String getSongName() {
            return songName;
        }

        public String getArtistName() {
            return artistName;
        }

        public String getArtistName(Context context) {
            return StringUtils.isEmpty(this.artistName) ? context.getResources().getString(R.string.app_haige_provide) : this.artistName;
        }

        public void setSongId(long songId) {
            this.songId = songId;
        }

        public void setSongName(String songName) {
            this.songName = songName;
        }

        public void setArtistName(String artistName) {
            this.artistName = artistName;
        }

        public long getAlbumId() {
            return albumId;
        }

        public void setAlbumId(long albumId) {
            this.albumId = albumId;
        }

        public boolean isLocal() {
            return isLocal;
        }

        public void setLocal(boolean local) {
            isLocal = local;
        }

        public String getSongCover() {
            return songCover;
        }

        public void setSongCover(String songCover) {
            this.songCover = songCover;
        }

        public int getLikeCount() {
            return likeCount;
        }

        public void setLikeCount(int likeCount) {
            this.likeCount = likeCount;
        }

        public int getShareCount() {
            return shareCount;
        }

        public void setShareCount(int shareCount) {
            this.shareCount = shareCount;
        }

        public int getDownloadCount() {
            return downloadCount;
        }

        public void setDownloadCount(int downloadCount) {
            this.downloadCount = downloadCount;
        }

        public String getSongUrl() {
            return songUrl;
        }

        public void setSongUrl(String songUrl) {
            this.songUrl = songUrl;
        }

        @Override
        public String toString() {
            return "MetaChangedEvent{" +
                    "songId=" + songId +
                    ", songName='" + songName + '\'' +
                    ", artistName='" + artistName + '\'' +
                    ", albumId=" + albumId +
                    ", isLocal=" + isLocal +
                    ", songCover='" + songCover + '\'' +
                    ", likeCount=" + likeCount +
                    ", shareCount=" + shareCount +
                    ", downloadCount=" + downloadCount +
                    ", songUrl='" + songUrl + '\'' +
                    '}';
        }

        @Override
        public boolean equals(Object obj) {
            return obj != null && obj instanceof MetaChangedEvent && ((MetaChangedEvent) obj).songId == songId;
        }
    }

    class PlaylistUpdateEvent {

    }

    class MediaUpdateEvent {

    }

    class RecentlyPlayEvent {

    }

    class RepeatModeChangedEvent {

    }

    class MusicPreparedEvent {

    }

    /**
     * 音乐资源加载中
     */
    class MusicIsLoadingEvent {
        boolean isLoading;

        public boolean isLoading() {
            return isLoading;
        }

        public void setLoading(boolean loading) {
            isLoading = loading;
        }
    }

    class FollowItemStatusEvent {

        int status;

        public FollowItemStatusEvent(int status) {
            this.status = status;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }
    }
}
