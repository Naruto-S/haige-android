package com.music.player.haige.mvp.presenter;

import com.hc.base.mvp.BasePresenter;
import com.music.player.haige.mvp.contract.MVContract;

import javax.inject.Inject;

/**
 * ================================================
 * Created by huangcong on 2018/2/27.
 * ================================================
 */

public class FindFPresenter extends BasePresenter<MVContract.CommonModel, MVContract.CommonView> {

    @Inject
    public FindFPresenter(MVContract.CommonModel model, MVContract.CommonView view) {
        super(model, view);
    }
}
