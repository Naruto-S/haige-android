package com.music.player.haige.mvp.presenter;

import android.app.Activity;

import com.hc.base.mvp.BasePresenter;
import com.music.player.haige.app.base.pref.UserPrefHelper;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.Api;
import com.music.player.haige.mvp.ui.utils.DebugLogger;
import com.umeng.socialize.UMAuthListener;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.bean.SHARE_MEDIA;

import java.util.Map;

import javax.inject.Inject;

import static com.music.player.haige.mvp.model.entity.user.LoginRequest.TYPE_LOGIN_QQ;
import static com.music.player.haige.mvp.model.entity.user.LoginRequest.TYPE_LOGIN_SINA;
import static com.music.player.haige.mvp.model.entity.user.LoginRequest.TYPE_LOGIN_WEIXIN;

/**
 * ================================================
 * Created by huangcong on 2018/3/2.
 * ================================================
 */

public class SettingFPresenter extends BasePresenter<MVContract.CommonModel, MVContract.CommonView> {

    @Inject
    public SettingFPresenter(MVContract.CommonModel model, MVContract.CommonView view) {
        super(model, view);
    }

    public void signout(Activity activity) {
        SHARE_MEDIA share_media;
        switch (UserPrefHelper.getUserLoginType()) {
            case TYPE_LOGIN_QQ:
                share_media = SHARE_MEDIA.QQ;
                break;

            case TYPE_LOGIN_WEIXIN:
                share_media = SHARE_MEDIA.WEIXIN;
                break;

            case TYPE_LOGIN_SINA:
                share_media = SHARE_MEDIA.SINA;
                break;
            default:
                share_media = SHARE_MEDIA.QQ;
                break;
        }

        UserPrefHelper.clear();

        UMShareAPI.get(activity).deleteOauth(activity, share_media, new UMAuthListener() {

            @Override
            public void onStart(SHARE_MEDIA media) {
            }

            @Override
            public void onComplete(SHARE_MEDIA media, int i, Map<String, String> map) {
                mRootView.onSuccess(Api.Action.LOGIN_LOGOUT, null);
            }

            @Override
            public void onError(SHARE_MEDIA media, int i, Throwable throwable) {
                mRootView.onError(Api.Action.LOGIN_LOGOUT, "-1", throwable.getMessage());
            }

            @Override
            public void onCancel(SHARE_MEDIA media, int i) {
            }
        });
    }
}
