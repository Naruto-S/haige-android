package com.music.player.haige.app.image.release;

import com.facebook.drawee.drawable.ScalingUtils;
import com.facebook.drawee.generic.GenericDraweeHierarchyBuilder;

/**
 * Created by ZaKi on 2016/9/1.
 */
public class DisplayOptions implements Cloneable {

    private final int imageResOnLoading;//占位符
    private final int imageResOnFail;//失败图
    private final ScalingUtils.ScaleType imageScaleType;//缩放模式，默认是CENTER_CROP
    private final ImageOptions imageOptions;//请求配置
    private final ImageOptions lowImageOptions;//fresco支持多请求配置
    private int[] imageRoundRadii = null;//圆角度数
    private boolean imageAsCircle = false;//方角还说圆角
    private int imageBorderWidth = 0;//边框宽度
    private int imageBorderColor = 0;//边框颜色
    private boolean isAutoPlayAnimations = true;


    private DisplayOptions(Builder builder) {
        imageResOnLoading = builder.imageResOnLoading;
        imageResOnFail = builder.imageResOnFail;
        imageScaleType = builder.imageScaleType;
        imageRoundRadii = builder.imageRoundRadii;
        imageAsCircle = builder.imageAsCircle;
        imageBorderWidth = builder.imageBorderWidth;
        imageBorderColor = builder.imageBorderColor;
        imageOptions = builder.imageOptions;
        lowImageOptions = builder.lowImageOptions;
        isAutoPlayAnimations = builder.isAutoPlayAnimations;
    }

    public static DisplayOptions createSimple() {
        return new Builder().build();
    }

    public int getImageResOnLoading() {
        return imageResOnLoading;
    }

    public int getImageResOnFail() {
        return imageResOnFail;
    }

    public int[] getImageRoundRadii() {
        return imageRoundRadii;
    }

    public boolean getAsCircle() {
        return imageAsCircle;
    }

    public int getImageBorderWidth() {
        return imageBorderWidth;
    }

    public int getImageBorderColor() {
        return imageBorderColor;
    }

    public ImageOptions getImageOptions() {
        return imageOptions;
    }

    public ImageOptions getLowImageOptions() {
        return lowImageOptions;
    }

    public ScalingUtils.ScaleType getImageScaleType() {
        return imageScaleType;
    }

    public boolean isAutoPlayAnimations() {
        return isAutoPlayAnimations;
    }

    public static class Builder {
        private int imageResOnLoading = 0;
        private int imageResOnFail = 0;
        private ScalingUtils.ScaleType imageScaleType = ScalingUtils.ScaleType.CENTER_CROP;
        private int[] imageRoundRadii = null;
        private boolean imageAsCircle = false;
        private int imageBorderWidth = 0;
        private int imageBorderColor = 0;
        private ImageOptions imageOptions = null;
        private ImageOptions lowImageOptions = null;
        private boolean isAutoPlayAnimations = true;

        public Builder() {

        }

        public Builder(GenericDraweeHierarchyBuilder hierarchyBuilder) {
            if (hierarchyBuilder.getActualImageScaleType() != null) {
                imageScaleType = hierarchyBuilder.getActualImageScaleType();
            }
        }

        public Builder(Builder builder) {
            imageResOnLoading = builder.imageResOnLoading;
            imageResOnFail = builder.imageResOnFail;
            imageScaleType = builder.imageScaleType;
            imageRoundRadii = builder.imageRoundRadii;
            imageAsCircle = builder.imageAsCircle;
            imageBorderWidth = builder.imageBorderWidth;
            imageBorderColor = builder.imageBorderColor;
            imageOptions = builder.imageOptions;
            lowImageOptions = builder.lowImageOptions;
            isAutoPlayAnimations = builder.isAutoPlayAnimations;
        }

        public Builder showImageScaleType(ScalingUtils.ScaleType scaleType) {
            imageScaleType = scaleType;
            return this;
        }

        public Builder showImageOnLoading(int imageRes) {
            imageResOnLoading = imageRes;
            return this;
        }

        public Builder showImageOnFail(int imageRes) {
            imageResOnFail = imageRes;
            return this;
        }

        public Builder showRoundRadii(int[] imageRes) {
            imageRoundRadii = imageRes;
            return this;
        }

        public Builder showCircleRadius(boolean imageRes) {
            imageAsCircle = imageRes;
            return this;
        }

        public Builder showBorderWidth(int imageRes) {
            imageBorderWidth = imageRes;
            return this;
        }

        public Builder showBorderColor(int imageRes) {
            imageBorderColor = imageRes;
            return this;
        }

        public Builder setLowImageOptions(ImageOptions lowImageOptions) {
            this.lowImageOptions = lowImageOptions;
            return this;
        }

        public Builder setAutoPlayAnimations(boolean autoPlayAnimations) {
            this.isAutoPlayAnimations = autoPlayAnimations;
            return this;
        }

        public DisplayOptions build() {
            return new DisplayOptions(this);
        }

        public Builder clone() {
            return new Builder(this);
        }

        public void clone(DisplayOptions displayOptions) {
            if (displayOptions == null) {
                throw new IllegalArgumentException("displayOptions is null ?");
            }
            imageResOnLoading = displayOptions.imageResOnLoading;
            imageResOnFail = displayOptions.imageResOnFail;
            imageScaleType = displayOptions.imageScaleType;
            imageRoundRadii = displayOptions.imageRoundRadii;
            imageAsCircle = displayOptions.imageAsCircle;
            imageBorderWidth = displayOptions.imageBorderWidth;
            imageBorderColor = displayOptions.imageBorderColor;
            imageOptions = displayOptions.imageOptions;
            lowImageOptions = displayOptions.lowImageOptions;
            isAutoPlayAnimations = displayOptions.isAutoPlayAnimations;
        }

        public ImageOptions getImageOptions() {
            return imageOptions;
        }

        public Builder setImageOptions(ImageOptions pOptions) {
            imageOptions = pOptions;
            return this;
        }
    }
}
