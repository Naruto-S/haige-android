package com.music.player.haige.mvp.ui.music.adapter;

import android.graphics.PorterDuff;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.hc.base.utils.ATEUtil;
import com.hc.base.utils.DensityUtil;
import com.hc.base.widget.fastscroller.FastScrollRecyclerView;
import com.hc.core.di.component.AppComponent;
import com.hc.core.http.imageloader.ImageLoader;
import com.hc.core.utils.CoreUtils;
import com.music.player.haige.R;
import com.music.player.haige.app.EventBusTags;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.ui.music.service.MusicServiceConnection;
import com.music.player.haige.mvp.ui.music.viewholder.SongItemViewHolder;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;

import org.simple.eventbus.EventBus;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.schedulers.Schedulers;


public class SongsListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements FastScrollRecyclerView.SectionedAdapter {

    public int currentlyPlayingPosition;
    private List<Song> arrayList;
    private AppCompatActivity mContext;
    private boolean withHeader;
    private float topPlayScore;
    private AppComponent mAppComponent;
    private ImageLoader mImageLoader;//用于加载图片的管理类,默认使用 Glide,使用策略模式,可替换框架
    private OnSongClickListener mOnSongClickListener;

    public SongsListAdapter(AppCompatActivity context, boolean withHeader) {
        this.mContext = context;
        this.withHeader = withHeader;

        //可以在任何可以拿到 Context 的地方,拿到 AppComponent,从而得到用 Dagger 管理的单例对象
        mAppComponent = CoreUtils.obtainAppComponentFromContext(mContext);
        mImageLoader = mAppComponent.imageLoader();
    }

    public void setOnSongClickListener(OnSongClickListener listener) {
        mOnSongClickListener = listener;
    }

    public void removeSong(int position) {
        try {
            arrayList.remove(position);
            int realPosition = withHeader ? position + 1 : position;
            notifyItemRemoved(realPosition);
            notifyItemRangeChanged(realPosition, getItemCount());
        } catch (Exception e) {

        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0 && withHeader) {
            return Type.TYPE_PLAY_SHUFFLE;
        } else {
            return Type.TYPE_SONG;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        switch (viewType) {
            case Type.TYPE_PLAY_SHUFFLE:
                View playShuffle = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.app_item_play_shuffle, viewGroup, false);
                ImageView imageView = playShuffle.findViewById(R.id.app_item_play_shuffle);
                imageView.getDrawable().setColorFilter(ResourceUtils.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
                viewHolder = new PlayShuffleViewHolder(playShuffle);
                break;
            case Type.TYPE_SONG:
                View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.app_item_music, viewGroup, false);
                viewHolder = new ItemHolder(v);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int viewType = getItemViewType(position);
        switch (viewType) {
            case Type.TYPE_PLAY_SHUFFLE:
                break;
            case Type.TYPE_SONG:
                ItemHolder itemHolder = (ItemHolder) holder;
                Song localItem;
                if (withHeader) {
                    localItem = arrayList.get(position - 1);
                    itemHolder.rank.setText(String.valueOf(position));
                } else {
                    localItem = arrayList.get(position);
                    itemHolder.rank.setText(String.valueOf(position + 1));
                }
                itemHolder.title.setText(localItem.musicName);
                itemHolder.artist.setText(localItem.getArtistName(mContext));
                itemHolder.album.setText(localItem.albumName);

                if (MusicServiceConnection.getCurrentAudioId() == localItem.songId) {
                    itemHolder.title.setTextColor(ResourceUtils.getColor(R.color.colorAccent));
                    if (MusicServiceConnection.isPlaying()) {
                        itemHolder.rank.setVisibility(View.GONE);
                        itemHolder.musicVisualizer.setVisibility(View.VISIBLE);
                        itemHolder.musicVisualizer.setColor(ResourceUtils.getColor(R.color.colorAccent));
                    } else {
                        itemHolder.rank.setVisibility(View.VISIBLE);
                        itemHolder.rank.setTextColor(ResourceUtils.getColor(R.color.colorAccent));
                        itemHolder.musicVisualizer.setVisibility(View.GONE);
                    }
                } else {
                    itemHolder.rank.setVisibility(View.VISIBLE);
                    itemHolder.rank.setTextColor(ATEUtil.getThemeTextColorPrimary(mContext));
                    itemHolder.title.setTextColor(ATEUtil.getThemeTextColorPrimary(mContext));
                    itemHolder.musicVisualizer.setVisibility(View.GONE);
                }

                if (topPlayScore != 0) {
                    itemHolder.playScore.setVisibility(View.VISIBLE);
                    RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) itemHolder.playScore.getLayoutParams();
                    int full = DensityUtil.getScreenWidth(mContext);
                    layoutParams.width = (int) (full * (localItem.getPlayCountScore() / topPlayScore));
                }

                setOnPopupMenuListener(itemHolder, position);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return null != arrayList ? (withHeader ? arrayList.size() + 1 : arrayList.size()) : 0;
    }

    private void setOnPopupMenuListener(ItemHolder itemHolder, final int position) {

        final int realSongPosition;
        if (withHeader) {
            realSongPosition = position - 1;
        } else {
            realSongPosition = position;
        }

        itemHolder.popupMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnSongClickListener != null) {
                    mOnSongClickListener.onPopupMenuClick(v, realSongPosition, arrayList.get(realSongPosition));
                }
            }
        });
    }

    public void setSongList(List<Song> arrayList) {
        this.arrayList = arrayList;
        if (arrayList.size() != 0) {
            this.topPlayScore = arrayList.get(0).getPlayCountScore();
        }
        //notifyItemRangeInserted(0, arrayList.size());
        notifyDataSetChanged();
    }

    public void addSongList(List<Song> arrayList) {
        if (this.arrayList == null) {
            this.arrayList = arrayList;
        } else {
            this.arrayList.addAll(arrayList);
        }
        if (arrayList.size() != 0) {
            this.topPlayScore = arrayList.get(0).getPlayCountScore();
        }
        //notifyItemRangeInserted(0, arrayList.size());
        notifyDataSetChanged();
    }

    public int getDataCount() {
        return this.arrayList == null ? 0 : this.arrayList.size();
    }

    @NonNull
    @Override
    public String getSectionName(int position) {
        if (arrayList == null || arrayList.size() == 0 || (withHeader && position == 0))
            return "";

        if (withHeader) {
            position = position - 1;
        }
        Character ch = arrayList.get(position).musicName.charAt(0);
        if (Character.isDigit(ch)) {
            return "#";
        } else
            return Character.toString(ch);
    }

    public static class Type {
        public static final int TYPE_PLAY_SHUFFLE = 0;
        public static final int TYPE_SONG = 1;
    }

    public class ItemHolder extends SongItemViewHolder implements View.OnClickListener {

        public ItemHolder(View view) {
            super(view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Observable.empty()
                    .delay(100, TimeUnit.MILLISECONDS)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnComplete(new Action() {
                        @Override
                        public void run() throws Exception {
                            int position = getAdapterPosition();
                            position = withHeader ? position - 1 : position;
                            if (position >= 0 && position < arrayList.size()) {
                                MusicServiceConnection.playAll(arrayList, position, false);
                                EventBus.getDefault().post(EventBusTags.MAIN.MUSIC_PLAY);
                            }
                        }
                    })
                    .subscribe();
        }
    }

    public class PlayShuffleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public PlayShuffleViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    MusicServiceConnection.playAll(arrayList, -1, true);
                    Handler handler1 = new Handler();
                    handler1.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            notifyItemChanged(currentlyPlayingPosition);
                            notifyItemChanged(getAdapterPosition());
                            currentlyPlayingPosition = getAdapterPosition();
                        }
                    }, 50);
                }
            }, 100);
        }
    }

    public interface OnSongClickListener {
        void onPopupMenuClick(View v, int position, Song song);
    }
}


