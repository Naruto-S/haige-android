/**
 * fresco图片加载封装
 * <p>
 * 使用说明：
 * <p>
 * FitImageView ---- ImageView使用FitImageView
 * 注：FitImageView要设置宽高，fresco的缺陷，不设置宽高加载不出图片
 * <p>
 * BaseImageLoader ---- 加载图片的方法封装在BaseImageLoader，如果满足不了可以自行扩展。
 * 建议：做相应功能模块的时候继承BaseImageLoader在封装一层。如：LocalImageLoader
 * ps：BaseImageLoader.displaySimpleImage(uri, mNetFiv);
 * <p>
 * LocalImageLoader ---- 用来加载本地资源图片。
 * ps：
 * LocalImageLoader.displayResImage(R.mipmap.ic_launcher, mResFiv);
 * LocalImageLoader.displayAssetImage(path, mAssetFiv);
 * LocalImageLoader.displayLocalImage(path, mFileFiv);
 * <p>
 * HaigeImageOptionsUtils ---- 可以自定义showImageOnLoading、showImageOnFail。可参考文件中注释掉的代码。
 */
package com.music.player.haige.app.image;