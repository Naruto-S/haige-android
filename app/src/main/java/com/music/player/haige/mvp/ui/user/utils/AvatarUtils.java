package com.music.player.haige.mvp.ui.user.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;

import com.music.player.haige.app.image.utils.FileStore;
import com.music.player.haige.mvp.ui.utils.FileUtils;

import java.io.File;

/**
 * Created by kince on 16-11-4.
 */

public class AvatarUtils {

    public static final int AVATAR_MAX_SIZE = 1080;

    public static final int REQUEST_GALLERY = 21;
    public static final int REQUEST_CAMERA = 20;

    public static void startGalleryIntent(Activity activity) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        activity.startActivityForResult(intent, REQUEST_GALLERY);
    }

    public static void dispatchTakePictureIntent(Activity activity) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = new File(AvatarUtils.getCameraImageFilePath());
            // Continue only if the File was successfully created
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        FileProvider.getUriForFile(activity, "com.music.player.haige.fileProvider", photoFile));
            } else {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
            }
            activity.startActivityForResult(takePictureIntent, REQUEST_CAMERA);
        }
    }

    /**
     * 查看选择要修改的头像图片是否合法
     *
     * @param context
     * @param uri
     * @return
     */
    public static boolean checkAvatarLegal(Context context, Uri uri) {
        try {
            String path = FileUtils.getPath(context, uri);
            if (TextUtils.isEmpty(path)) {
                return false;
            } else if (path.endsWith(FileStore.SUFFIX_WEBP)) {
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 切割后头像文件存储路径
     *
     * @return
     */
    public static String getCropAvatarImageFilePath() {
        FileUtils.initDataDirectory(FileStore.getSystemImagePath());
        StringBuilder sb = new StringBuilder();
        sb.append(FileStore.getSystemImagePath()).append("IMG_CROP").append(FileStore.SUFFIX_JPG);
        return sb.toString();
    }

    /**
     * 头像文件路径
     *
     * @return
     */
    public static String getAvatarImageFilePath() {
        FileUtils.initDataDirectory(FileStore.getSystemImagePath());
        StringBuilder sb = new StringBuilder();
        sb.append(FileStore.getSystemImagePath())
                .append("IMG_AVATAR")
                .append(FileStore.SUFFIX_JPG);
        return sb.toString();
    }

    /**
     * 拍照后的图片存储路径
     *
     * @return
     */
    public static String getCameraImageFilePath() {
        FileUtils.initDataDirectory(FileStore.getSystemImagePath());
        StringBuilder sb = new StringBuilder();
        sb.append(FileStore.getSystemImagePath())
                .append("IMG_CAMERA")
                .append(FileStore.SUFFIX_JPG);
        return sb.toString();
    }
}
