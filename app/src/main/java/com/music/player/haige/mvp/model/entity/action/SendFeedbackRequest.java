package com.music.player.haige.mvp.model.entity.action;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SendFeedbackRequest implements Serializable {

    @SerializedName("content")
    private String content;

    @SerializedName("contact")
    private String contact;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    @Override
    public String toString() {
        return "SendFeedbackRequest{" +
                "content='" + content + '\'' +
                ", contact='" + contact + '\'' +
                '}';
    }
}
