package com.music.player.haige.mvp.ui.upload.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hc.base.utils.ATEUtil;
import com.music.player.haige.R;
import com.music.player.haige.app.base.recyclerview.BaseQuickAdapter;
import com.music.player.haige.app.base.recyclerview.BaseViewHolder;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.ui.music.service.MusicServiceConnection;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;
import com.music.player.haige.mvp.ui.widget.MusicVisualizer;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UploadStatusListAdapter extends BaseQuickAdapter<Song, BaseViewHolder> {

    public UploadStatusListAdapter(Context context) {
        super(context, 0, null);
    }

    @Override
    public BaseViewHolder onCreateDefViewHolder(ViewGroup parent, int viewType) {
        BaseViewHolder viewHolder;
        viewHolder = new MusicViewHolder(mLayoutInflater.inflate(R.layout.app_item_music_upload_status, parent, false));
        viewHolder.addOnClickListener(R.id.popup_menu);
        return viewHolder;
    }

    @Override
    protected void convert(BaseViewHolder itemHolder, Song song) {
        onBindNormalViewHolder(itemHolder, song);
    }

    private void onBindNormalViewHolder(BaseViewHolder holder, Song song) {
        MusicViewHolder itemHolder = (MusicViewHolder) holder;
        int position = itemHolder.getPosition() + getHeaderLayoutCount();

        itemHolder.rank.setText(String.valueOf(position + 1));
        itemHolder.title.setText(song.musicName);
        itemHolder.artist.setText(song.getArtistName(mContext));
        itemHolder.album.setText(song.albumName);

        if (song.getUploadStatus() == 0) {
            itemHolder.popupMenu.setVisibility(View.GONE);
            itemHolder.uploadStatusTv.setText(song.getUploadStatusDisplay());
            itemHolder.uploadStatusTv.setVisibility(View.VISIBLE);
        } else if (song.getUploadStatus() == 1) {
            itemHolder.popupMenu.setVisibility(View.VISIBLE);
            itemHolder.uploadStatusTv.setText(song.getUploadStatusDisplay());
            itemHolder.uploadStatusTv.setVisibility(View.GONE);
        } else if (song.getUploadStatus() == 2) {
            itemHolder.popupMenu.setVisibility(View.GONE);
            itemHolder.uploadStatusTv.setText(song.getUploadStatusDisplay());
            itemHolder.uploadStatusTv.setVisibility(View.VISIBLE);
        }

        if (MusicServiceConnection.getCurrentAudioId() == song.songId) {
            itemHolder.title.setTextColor(ResourceUtils.getColor(R.color.colorAccent));
            if (MusicServiceConnection.isPlaying()) {
                itemHolder.rank.setVisibility(View.GONE);
                itemHolder.musicVisualizer.setVisibility(View.VISIBLE);
                itemHolder.musicVisualizer.setColor(ResourceUtils.getColor(R.color.colorAccent));
            } else {
                itemHolder.rank.setVisibility(View.VISIBLE);
                itemHolder.rank.setTextColor(ResourceUtils.getColor(R.color.colorAccent));
                itemHolder.musicVisualizer.setVisibility(View.GONE);
            }
        } else {
            itemHolder.rank.setVisibility(View.VISIBLE);
            itemHolder.rank.setTextColor(ATEUtil.getThemeTextColorPrimary(mContext));
            itemHolder.title.setTextColor(ATEUtil.getThemeTextColorPrimary(mContext));
            itemHolder.musicVisualizer.setVisibility(View.GONE);
        }
    }

    class MusicViewHolder extends BaseViewHolder {

        @BindView(R.id.app_item_rank)
        TextView rank;
        @BindView(R.id.text_item_title)
        TextView title;
        @BindView(R.id.text_item_subtitle)
        TextView artist;
        @BindView(R.id.text_item_subtitle_2)
        TextView album;
        @BindView(R.id.tv_upload_status)
        TextView uploadStatusTv;
        @BindView(R.id.popup_menu)
        ImageView popupMenu;
        @BindView(R.id.app_item_play_score)
        View playScore;
        @BindView(R.id.app_item_music_visualizer)
        MusicVisualizer musicVisualizer;

        public MusicViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
