package com.music.player.haige.di.component;

import com.hc.core.di.component.AppComponent;
import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.di.module.DownloadModule;
import com.music.player.haige.di.module.DownloadingModule;
import com.music.player.haige.mvp.ui.setting.fragment.DownloadFragment;
import com.music.player.haige.mvp.ui.setting.fragment.DownloadingFragment;

import dagger.Component;

/**
 * ================================================
 * Created by huangcong on 2018/3/20.
 * ================================================
 */
@FragmentScope
@Component(modules = {DownloadingModule.class}, dependencies = AppComponent.class)
public interface DownloadingComponent {

    void inject(DownloadingFragment fragment);
}
