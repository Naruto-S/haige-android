package com.music.player.haige.mvp.usecase;

import com.hc.core.mvp.UseCase;
import com.music.player.haige.app.Constants;
import com.music.player.haige.mvp.model.entity.music.Album;
import com.music.player.haige.mvp.model.respository.interfaces.Repository;

import java.util.List;

import io.reactivex.Observable;


public class GetAlbums extends UseCase<GetAlbums.RequestValues,GetAlbums.ResponseValue> {

    private final Repository mRepository;

    public GetAlbums(Repository repository){
        mRepository = repository;
    }

    @Override
    public ResponseValue execute(RequestValues requestValues) {
        String action = requestValues.getAction();
        switch (action) {
            case Constants.NAVIGATE_ALLSONG:
                return new ResponseValue(mRepository.getAllAlbums());
            case Constants.NAVIGATE_PLAYLIST_RECENTADD:
                return new ResponseValue(mRepository.getRecentlyAddedAlbums());
            case Constants.NAVIGATE_PLAYLIST_RECENTPLAY:
                return new ResponseValue(mRepository.getRecentlyPlayedAlbums());
            case Constants.NAVIGATE_PLAYLIST_FAVORITE:
                return new ResponseValue(mRepository.getFavoriteAlbums());
            default:
                throw new RuntimeException("wrong action type");
        }
    }

    public static final class RequestValues implements UseCase.RequestValues{

        private final String action;

        public RequestValues(String action) {
            this.action = action;
        }

        public String getAction() {
            return action;
        }
    }

    public static final class ResponseValue implements UseCase.ResponseValue {

        private final Observable<List<Album>> mListObservable;

        public ResponseValue(Observable<List<Album>> listObservable) {
            mListObservable = listObservable;
        }

        public Observable<List<Album>> getAlbumList(){
            return mListObservable;
        }
    }
}
