package com.music.player.haige.app.base.recyclerview.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.music.player.haige.R;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;
import com.music.player.haige.mvp.ui.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by kince on 2016/3/15.
 */
public class EmptyView extends FrameLayout {

    public static final int EMPTY_NO_MUSIC = 0;
    public static final int EMPTY_NO_FOLLOW = 1;

    @BindView(R.id.app_layout_empty_image)
    ImageView mEmptyIv;

    @BindView(R.id.app_layout_empty_text)
    TextView mEmptyTv;

    @BindView(R.id.app_layout_empty_btn)
    TextView mEmptyBtn;

    private OnEmptyListener mListener;

    public EmptyView(Context context) {
        super(context);
    }

    public EmptyView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public EmptyView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        LayoutInflater.from(context).inflate(getLayout(), this, true);
        ButterKnife.bind(this, this);

        mEmptyIv.setOnClickListener(v -> {
            if (Utils.isNotNull(mListener)) {
                mListener.OnEmptyClick();
            }
        });
    }

    public void setEmptyImage(int resId) {
        try {
            mEmptyIv.setImageResource(resId);
        } catch (OutOfMemoryError e) {

        }
    }

    public void setEmptyType(int emptyType) {
        int resImg = R.drawable.ic_empty;
        int resStr = R.string.empty_no_body;
        switch (emptyType) {
            case EMPTY_NO_MUSIC:
                resImg = R.drawable.ic_empty;
                resStr = R.string.empty_no_music;
                mEmptyBtn.setVisibility(VISIBLE);
                break;
            case EMPTY_NO_FOLLOW:
                resImg = R.drawable.app_ic_empty_view;
                resStr = R.string.empty_no_body;
                mEmptyBtn.setVisibility(GONE);
                break;
            default:
                resImg = R.drawable.ic_empty;
                resStr = R.string.empty_no_music;
                mEmptyBtn.setVisibility(VISIBLE);
                break;
        }
        setEmptyImage(resImg);
        mEmptyTv.setText(ResourceUtils.resourceString(resStr));
    }

    public int getLayout() {
        return R.layout.app_layout_empty;
    }

    public void setOnEmptyListener(OnEmptyListener listener) {
        mListener = listener;
    }

    public interface OnEmptyListener {
        void OnEmptyClick();
    }
}
