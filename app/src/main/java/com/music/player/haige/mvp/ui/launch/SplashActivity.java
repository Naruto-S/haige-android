package com.music.player.haige.mvp.ui.launch;

import android.os.Bundle;

import com.hc.core.di.component.AppComponent;
import com.music.player.haige.R;
import com.music.player.haige.app.SystemUIConfig;
import com.music.player.haige.app.base.AppBaseActivity;
import com.music.player.haige.app.base.pref.DevicePrefHelper;
import com.music.player.haige.app.constants.IntentConstants;
import com.music.player.haige.app.constants.TimeConstants;
import com.music.player.haige.app.utils.NotificationUtils;
import com.music.player.haige.app.utils.launcher.ActivityLauncherStart;
import com.music.player.haige.di.component.DaggerSplashComponent;
import com.music.player.haige.di.module.SplashModule;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.presenter.SplashPresenter;
import com.music.player.haige.mvp.ui.utils.Utils;
import com.tbruyelle.rxpermissions2.RxPermissions;

/**
 * ================================================
 * Created by huangcong on 2018/3/10.
 * ================================================
 */

public class SplashActivity extends AppBaseActivity<SplashPresenter> implements MVContract.SplashView, SystemUIConfig {

    @Override
    public void setupActivityComponent(AppComponent appComponent) {
        DaggerSplashComponent
                .builder()
                .appComponent(appComponent)
                .splashModule(new SplashModule(this))
                .build()
                .inject(this);
    }

    @Override
    public int initView(Bundle savedInstanceState) {
        return R.layout.app_activity_splash;
    }

    @Override
    public void initData(Bundle savedInstanceState) {
        if (Utils.isNotNull(getIntent())) {
            int notifyType = getIntent().getIntExtra(IntentConstants.EXTRA_CLICK_NOTIFY, 0);
            Song song = getIntent().getParcelableExtra(IntentConstants.EXTRA_NOTIFY_MUSIC);
            if (notifyType == NotificationUtils.NOTIFY_TYPE_MUSIC) {
                ActivityLauncherStart.startMain(this, song, notifyType);
                finish();
                overridePendingTransition(R.anim.activity_fade_in, R.anim.activity_fade_out);
            }
        }

        if (DevicePrefHelper.getNextShowRateTime() == 0) {
            DevicePrefHelper.putNextShowRateTime(System.currentTimeMillis() + TimeConstants.HOUR_24);
        }

        mPresenter.getMusicWebpBg("gif");
        mPresenter.requestPermission();
    }

    @Override
    public RxPermissions getRxPermissions() {
        return new RxPermissions(this);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        mPresenter.requestPermission();
    }

    @Override
    public boolean translucentStatusBar() {
        return true;
    }

    @Override
    public int setStatusBarColor() {
        return getResources().getColor(R.color.color_00000000);
    }

    @Override
    public boolean lightStatusBar() {
        return false;
    }

    @Override
    public boolean fitSystemWindows() {
        return true;
    }

    @Override
    public boolean fullScreen() {
        return true;
    }
}
