package com.music.player.haige.mvp.model.entity.config;

import com.music.player.haige.mvp.model.entity.BaseResponse;

import java.io.Serializable;

public class AppConfigResponse extends BaseResponse<AppConfigModel> implements Serializable {
}
