package com.music.player.haige.di.component;

import com.hc.core.di.component.AppComponent;
import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.di.module.CommentDialogModule;
import com.music.player.haige.mvp.ui.comment.CommentDialogFragment;

import dagger.Component;

/**
 * Created by Naruto on 2018/5/5.
 */

@FragmentScope
@Component(modules = {CommentDialogModule.class}, dependencies = AppComponent.class)
public interface CommentDialogComponent {

    void inject(CommentDialogFragment fragment);
}
