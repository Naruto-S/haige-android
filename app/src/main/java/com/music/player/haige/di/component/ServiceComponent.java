package com.music.player.haige.di.component;

import com.hc.core.di.component.AppComponent;
import com.hc.core.di.scope.ServiceScope;
import com.music.player.haige.app.service.LocalService;
import com.music.player.haige.di.module.ServiceModule;

import dagger.Component;

/**
 * Created by Kince on 2016/5/12.
 */
@ServiceScope
@Component(modules = {ServiceModule.class}, dependencies = AppComponent.class)
public interface ServiceComponent {

    void inject(LocalService service);
}
