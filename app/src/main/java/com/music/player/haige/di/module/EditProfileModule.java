package com.music.player.haige.di.module;

import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.EditProfileModel;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Naruto on 2018/5/13.
 */
@Module
public class EditProfileModule {
    private MVContract.EditProfileView view;

    public EditProfileModule(MVContract.EditProfileView view){
        this.view = view;
    }

    @FragmentScope
    @Provides
    MVContract.EditProfileView provideView(){
        return view;
    }

    @FragmentScope
    @Provides
    MVContract.EditProfileModel provideModel(EditProfileModel model){
        return model;
    }
}
