/*
 * Copyright 2017 JessYan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.music.player.haige.mvp.model.api.cache;

import com.music.player.haige.mvp.model.entity.BaseResponse;
import com.music.player.haige.mvp.model.entity.rank.RankResponse;
import com.music.player.haige.mvp.model.entity.recommend.RecommendResponse;
import com.music.player.haige.mvp.model.entity.search.SearchHotWordResponse;
import com.music.player.haige.mvp.model.entity.search.SearchKeyWordResponse;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.rx_cache2.DynamicKey;
import io.rx_cache2.EvictProvider;
import io.rx_cache2.LifeCache;
import io.rx_cache2.ProviderKey;
import io.rx_cache2.internal.RxCache;

/**
 * ================================================
 * 展示 {@link RxCache#using(Class)} 中需要传入的 Providers 的使用方式
 * <p>
 * ================================================
 */
public interface ApiCache {

    @ProviderKey("music_recommend")
    @LifeCache(duration = 5, timeUnit = TimeUnit.MINUTES)
    Observable<RecommendResponse> getMusicRecommend(Observable<RecommendResponse> recommends, DynamicKey idLastUserQueried, EvictProvider evictProvider);

    @ProviderKey("music_rank_hot")
    @LifeCache(duration = 2, timeUnit = TimeUnit.HOURS)
    Observable<RankResponse> getMusicHotRank(Observable<RankResponse> ranks, DynamicKey idLastUserQueried, EvictProvider evictProvider);

    @ProviderKey("music_rank_new")
    @LifeCache(duration = 2, timeUnit = TimeUnit.HOURS)
    Observable<RankResponse> getMusicNewRank(Observable<RankResponse> ranks, DynamicKey idLastUserQueried, EvictProvider evictProvider);

    @ProviderKey("search_hot_word")
    @LifeCache(duration = 2, timeUnit = TimeUnit.HOURS)
    Observable<SearchHotWordResponse> getHotWord(Observable<SearchHotWordResponse> hotWord, DynamicKey idLastUserQueried, EvictProvider evictProvider);

    @ProviderKey("search_key_word")
    @LifeCache(duration = 2, timeUnit = TimeUnit.HOURS)
    Observable<SearchKeyWordResponse> searchKeyWord(Observable<SearchKeyWordResponse> searchResult, DynamicKey idLastUserQueried, EvictProvider evictProvider);

}
