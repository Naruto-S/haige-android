package com.music.player.haige.app;

import com.music.player.haige.mvp.model.entity.BaseResponse;

import me.jessyan.rxerrorhandler.core.RxErrorHandler;
import me.jessyan.rxerrorhandler.handler.ErrorHandleSubscriber;

/**
 * ================================================
 * Created by huangcong on 2018/3/21.
 * ================================================
 */

public class ErrorHandleSubscriberImpl<T extends BaseResponse> extends ErrorHandleSubscriber<T> {

    Object tag;

    public ErrorHandleSubscriberImpl(RxErrorHandler rxErrorHandler, Object tag) {
        super(rxErrorHandler);
        this.tag = tag;
    }

    @Override
    public void onNext(T t) {
        t.setTag(tag);
    }
}
