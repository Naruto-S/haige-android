package com.music.player.haige.mvp.presenter;

import com.music.player.haige.mvp.contract.AppBasePresenter;
import com.music.player.haige.mvp.contract.MVContract;

import javax.inject.Inject;

/**
 * ================================================
 * Created by huangcong on 2018/3/20.
 * ================================================
 */

public class DownloadFPresenter extends AppBasePresenter<MVContract.DownloadModel, MVContract.DownloadView> {

    @Inject
    public DownloadFPresenter(MVContract.DownloadModel model, MVContract.DownloadView view) {
        super(model, view);
    }

}
