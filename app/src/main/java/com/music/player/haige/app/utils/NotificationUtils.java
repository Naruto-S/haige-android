package com.music.player.haige.app.utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.widget.RemoteViews;

import com.music.player.haige.R;
import com.music.player.haige.app.utils.launcher.ActivityLauncherStart;
import com.music.player.haige.mvp.model.entity.music.Song;

import java.util.Calendar;

public class NotificationUtils {

    public static final int NOTIFY_TYPE_COMMON = 0;//基础类型
    public static final int NOTIFY_TYPE_MUSIC = 1;//音乐

    public static final int NOTIFY_ID_NORMAL = 0x111;
    public static final int NOTIFY_ID_MUISC = 0x112;

    public static final int NOTIFY_ID_UPDATE = 0x113;

    /**
     * 显示一个普通的通知
     *
     * @param context 上下文
     */
    public static void showNotification(Context context, String title, String text, int notifyType) {
        String channelId = "111";
        Notification notification = new NotificationCompat.Builder(context, channelId)
                /**设置通知左边的大图标**/
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
                /**设置通知右边的小图标**/
                .setSmallIcon(R.drawable.ic_notify_statusbar)
                /**通知首次出现在通知栏，带上升动画效果的**/
                .setTicker(title)
                /**设置通知的标题**/
                .setContentTitle(title)
                /**设置通知的内容**/
                .setContentText(text)
                /**通知产生的时间，会在通知信息里显示**/
                .setWhen(System.currentTimeMillis())
                /**设置该通知优先级**/
                .setPriority(Notification.PRIORITY_DEFAULT)
                /**设置这个标志当用户单击面板就可以让通知将自动取消**/
                .setAutoCancel(true)
                /**设置他为一个正在进行的通知。他们通常是用来表示一个后台任务,用户积极参与(如播放音乐)或以某种方式正在等待,因此占用设备(如一个文件下载,同步操作,主动网络连接)**/
                .setOngoing(false)
                /**向通知添加声音、闪灯和振动效果的最简单、最一致的方式是使用当前的用户默认设置，使用defaults属性，可以组合：**/
                .setDefaults(Notification.DEFAULT_VIBRATE | Notification.DEFAULT_SOUND)
                .setContentIntent(PendingIntent.getActivity(context,
                        1,
                        ActivityLauncherStart.getSplashIntent(context, notifyType),
                        PendingIntent.FLAG_CANCEL_CURRENT))
                .build();
        startNotification(context, channelId, NOTIFY_ID_NORMAL, notification);
    }

    /**
     * 显示一个普通的通知
     *
     * @param context 上下文
     */
    public static void showNotification(Context context, String text, int notifyType) {
        PendingIntent pendingIntent = PendingIntent.getActivity(context,
                1,
                ActivityLauncherStart.getSplashIntent(context, notifyType),
                PendingIntent.FLAG_CANCEL_CURRENT);
        showNotification(context, text, pendingIntent, 0);
    }

    /**
     * 显示一个普通的通知
     *
     * @param context 上下文
     */
    public static void showNotification(Context context, String text, int notifyType, int notifyId) {
        PendingIntent pendingIntent = PendingIntent.getActivity(context,
                1,
                ActivityLauncherStart.getSplashIntent(context, notifyType),
                PendingIntent.FLAG_CANCEL_CURRENT);
        showNotification(context, text, pendingIntent, notifyId);
    }

    /**
     * 显示一个普通的通知
     *
     * @param context 上下文
     */
    public static void showNotification(Context context, String text, PendingIntent pendingIntent, int id) {
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.common_simple_notification);
        remoteViews.setTextViewText(R.id.notify_title, text);
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        String currentTime = getCurrentTime(hour, minute);
        remoteViews.setTextViewText(R.id.notify_time, currentTime);
        String channelId = "111";
        Notification notification = new NotificationCompat.Builder(context, channelId)
                .setContentIntent(pendingIntent)
                .setOngoing(true)
                .setContent(remoteViews)
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_VIBRATE | Notification.DEFAULT_SOUND | Notification.FLAG_AUTO_CANCEL)
                .setSmallIcon(R.drawable.ic_notify_statusbar)
                .build();
        startNotification(context, channelId, id, notification);
    }

    /**
     * 悬挂式，支持6.0以上系统
     *
     * @param context
     */
    public static void showFullScreen(Context context, int notifyType) {
        String channelId = "111";
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, channelId);
        PendingIntent pendingIntent = PendingIntent.getActivity(context,
                0,
                ActivityLauncherStart.getSplashIntent(context, notifyType),
                0);
        builder.setContentIntent(pendingIntent);
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher));
        builder.setAutoCancel(true);
        builder.setContentTitle("悬挂式通知");
        //如果描述的PendingIntent已经存在，则在产生新的Intent之前会先取消掉当前的
        PendingIntent hangPendingIntent = PendingIntent.getActivity(context,
                0,
                ActivityLauncherStart.getSplashIntent(context, notifyType),
                PendingIntent.FLAG_CANCEL_CURRENT);
        builder.setFullScreenIntent(hangPendingIntent, true);
        Notification notification = builder.build();
        startNotification(context, channelId, 3, notification);
    }

    /**
     * 自定义通用Notification
     *
     * @param context       context
     * @param pendingIntent pendingIntent
     * @param title         title
     * @param text          text
     */
    public static void showCommonNotification(Context context, PendingIntent pendingIntent, String title, String text) {
        showCommonNotification(context, pendingIntent, title, text, NOTIFY_TYPE_COMMON);
    }

    /**
     * 自定义通用Notification
     *
     * @param context context
     * @param title   title
     * @param text    text
     */
    public static void showCommonNotification(Context context, String title, String text, int notifyType) {
        PendingIntent pendingIntent = PendingIntent.getActivity(context,
                1,
                ActivityLauncherStart.getSplashIntent(context, notifyType),
                PendingIntent.FLAG_CANCEL_CURRENT);
        showCommonNotification(context, pendingIntent, title, text, NOTIFY_TYPE_COMMON);
    }

    /**
     * 自定义通用Notification
     *
     * @param context       context
     * @param pendingIntent pendingIntent
     * @param title         title
     * @param text          text
     * @param type          {@link #NOTIFY_TYPE_COMMON}通用notify
     */
    public static void showCommonNotification(Context context, PendingIntent pendingIntent, String title, String text, int type) {
        showCommonNotification(context, pendingIntent, title, text, type, 0);
    }

    public static void showCommonNotification(Context context, PendingIntent pendingIntent, String title, String text, int type, int id) {
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.common_notification);
        remoteViews.setTextViewText(R.id.notify_title, title);
        remoteViews.setTextViewText(R.id.notify_text, text);
        String channelId = "111";
        Notification notification = new NotificationCompat.Builder(context, channelId)
                .setContentIntent(pendingIntent)
                .setOngoing(true)
                .setContent(remoteViews)
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_VIBRATE | Notification.DEFAULT_SOUND | Notification.FLAG_AUTO_CANCEL)
                .setSmallIcon(R.drawable.ic_notify_statusbar)
                .build();
        startNotification(context, channelId, id, notification);
    }

    private static void startNotification(Context context, String channelId, int id, Notification notification) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            /**发起通知**/
            if (notificationManager != null) {
                // Since android Oreo notification channel is needed.
                NotificationChannel channel = new NotificationChannel(channelId,
                        "Channel human readable title",
                        NotificationManager.IMPORTANCE_DEFAULT);
                notificationManager.createNotificationChannel(channel);
                notificationManager.notify(id, notification);
            }
        } else {
            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
            notificationManager.notify(id, notification);
        }
    }

    public static void showNotification(Context context, String title, String text, Song song, int notifyType) {
        String channelId = "111";
        Notification notification = new NotificationCompat.Builder(context, channelId)
                /**设置通知左边的大图标**/
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
                /**设置通知右边的小图标**/
                .setSmallIcon(R.drawable.ic_notify_statusbar)
                /**通知首次出现在通知栏，带上升动画效果的**/
                .setTicker(title)
                /**设置通知的标题**/
                .setContentTitle(title)
                /**设置通知的内容**/
                .setContentText(text)
                /**通知产生的时间，会在通知信息里显示**/
                .setWhen(System.currentTimeMillis())
                /**设置该通知优先级**/
                .setPriority(Notification.PRIORITY_DEFAULT)
                /**设置这个标志当用户单击面板就可以让通知将自动取消**/
                .setAutoCancel(true)
                /**设置他为一个正在进行的通知。他们通常是用来表示一个后台任务,用户积极参与(如播放音乐)或以某种方式正在等待,因此占用设备(如一个文件下载,同步操作,主动网络连接)**/
                .setOngoing(false)
                /**向通知添加声音、闪灯和振动效果的最简单、最一致的方式是使用当前的用户默认设置，使用defaults属性，可以组合：**/
                .setDefaults(Notification.DEFAULT_VIBRATE | Notification.DEFAULT_SOUND)
                .setContentIntent(PendingIntent.getActivity(context,
                        1,
                        ActivityLauncherStart.getSplashIntent(context, song, notifyType),
                        PendingIntent.FLAG_CANCEL_CURRENT))
                .build();
        startNotification(context, channelId, NOTIFY_ID_NORMAL, notification);
    }

    /**
     * 取消通知
     */
    public static void cancelNotification(Context context, int id) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            if (notificationManager != null) {
                notificationManager.cancel(id);
            }
        } else {
            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
            notificationManager.cancel(id);
        }
    }

    public static void cancelNotification(Context context) {
        cancelNotification(context, 0);
    }

    private static String getCurrentTime(int hour, int minute) {
        String hourContent = String.valueOf(hour);
        String minuteContent = String.valueOf(minute);
        if (hourContent.length() < 2) {
            hourContent = "0".concat(hourContent);
        }
        if (minuteContent.length() < 2) {
            minuteContent = "0".concat(minuteContent);
        }

        return hourContent.concat(":").concat(minuteContent);
    }
}
