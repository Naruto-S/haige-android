package com.music.player.haige.app.image.loader.listener;

import android.graphics.drawable.Animatable;
import android.view.View;

import com.facebook.imagepipeline.image.ImageInfo;

/**
 * 加载监听
 * <p>
 * Created by ZaKi on 2016/9/2.
 */
public abstract class OnImageLoaderListener {

    //回调加载完成
    public abstract void onImageLoadComplete(String uri, ImageInfo imageInfo, boolean isGif, Animatable animatable, View tagVview);

    //回调加载失败
    public abstract void onImageLoadFail(String uri, Throwable throwable, View tagVview);
}
