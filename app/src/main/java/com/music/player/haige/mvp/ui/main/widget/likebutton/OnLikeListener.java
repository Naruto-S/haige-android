package com.music.player.haige.mvp.ui.main.widget.likebutton;

public interface OnLikeListener {
    void liked(LikeButton likeButton);
    void unLiked(LikeButton likeButton);
    void onShowLoginDialog();
}
