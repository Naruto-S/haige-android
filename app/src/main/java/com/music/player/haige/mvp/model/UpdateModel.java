package com.music.player.haige.mvp.model;

import com.hc.base.mvp.BaseModel;
import com.hc.core.integration.IRepositoryManager;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.service.ApiService;
import com.music.player.haige.mvp.model.entity.BaseResponse;
import com.music.player.haige.mvp.model.entity.comment.CommentRequest;
import com.music.player.haige.mvp.model.entity.comment.SendCommentResponse;
import com.music.player.haige.mvp.model.entity.music.UploadMusicRequest;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * ================================================
 * Created by huangcong on 2018/3/3.
 * ================================================
 */

public class UpdateModel extends BaseModel implements MVContract.UpdateModel {

    @Inject
    public UpdateModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public Observable<BaseResponse> uploadMusic(UploadMusicRequest request) {
        return Observable.just(mRepositoryManager.obtainRetrofitService(ApiService.class).uploadMusic(request))
                .flatMap(observable -> observable);
    }
}
