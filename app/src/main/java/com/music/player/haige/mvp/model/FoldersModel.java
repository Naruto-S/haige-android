package com.music.player.haige.mvp.model;

import com.hc.base.mvp.BaseModel;
import com.hc.core.integration.IRepositoryManager;
import com.music.player.haige.app.GlobalConfiguration;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.usecase.GetFolders;

import javax.inject.Inject;

/**
 * ================================================
 * Created by huangcong on 2018/3/2.
 * ================================================
 */

public class FoldersModel extends BaseModel implements MVContract.FoldersModel {

    private GetFolders mGetFoldersCase;

    @Inject
    public FoldersModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
        mGetFoldersCase = new GetFolders(GlobalConfiguration.sRepository);
    }

    @Override
    public GetFolders.ResponseValue loadFolders() {
        return mGetFoldersCase.execute(new GetFolders.RequestValues());
    }
}
