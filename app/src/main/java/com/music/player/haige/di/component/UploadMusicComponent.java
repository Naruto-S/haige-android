package com.music.player.haige.di.component;

import com.hc.core.di.component.AppComponent;
import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.di.module.UploadMusicModule;
import com.music.player.haige.mvp.ui.music.fragment.UploadMusicFragment;
import com.music.player.haige.mvp.ui.user.MeUploadActivity;

import dagger.Component;

/**
 * ================================================
 * Created by huangcong on 2018/2/27.
 * ================================================
 */

@FragmentScope
@Component(modules = {UploadMusicModule.class}, dependencies = AppComponent.class)
public interface UploadMusicComponent {

    void inject(MeUploadActivity activity);

    void inject(UploadMusicFragment fragment);
}
