package com.music.player.haige.di.component;

import com.hc.core.di.component.AppComponent;
import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.di.module.UpdateModule;
import com.music.player.haige.mvp.ui.upload.UploadActivity;

import dagger.Component;

/**
 * ================================================
 * Created by huangcong on 2018/3/3.
 * ================================================
 */
@FragmentScope
@Component(modules = {UpdateModule.class}, dependencies = AppComponent.class)
public interface UpdateComponent {

    void inject(UploadActivity activity);
}
