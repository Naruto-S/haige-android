package com.music.player.haige.mvp.model;

import com.hc.base.mvp.BaseModel;
import com.hc.core.integration.IRepositoryManager;
import com.music.player.haige.mvp.contract.MVContract;

import javax.inject.Inject;

/**
 * ================================================
 * Created by huangcong on 2018/3/20.
 * ================================================
 */

public class DownloadModel extends BaseModel implements MVContract.DownloadModel {

    @Inject
    public DownloadModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }
}
