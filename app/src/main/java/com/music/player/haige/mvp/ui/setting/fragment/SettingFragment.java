package com.music.player.haige.mvp.ui.setting.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.hc.core.di.component.AppComponent;
import com.music.player.haige.R;
import com.music.player.haige.app.EventBusTags;
import com.music.player.haige.app.ToolbarConfig;
import com.music.player.haige.app.base.AppBaseFragment;
import com.music.player.haige.app.base.pref.UserPrefHelper;
import com.music.player.haige.di.component.DaggerCommonComponent;
import com.music.player.haige.di.module.CommonModule;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.Api;
import com.music.player.haige.mvp.model.utils.CommonUtils;
import com.music.player.haige.mvp.model.utils.NavigationUtil;
import com.music.player.haige.mvp.model.utils.PreferencesUtility;
import com.music.player.haige.mvp.presenter.SettingFPresenter;
import com.music.player.haige.mvp.ui.music.service.cache.StorageUtils;
import com.music.player.haige.mvp.ui.music.service.proxy.utils.ProxyConstants;
import com.music.player.haige.mvp.ui.share.DownloadShareDialog;
import com.music.player.haige.mvp.ui.utils.DeleteFileUtils;
import com.music.player.haige.mvp.ui.utils.FileSizeUtils;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;
import com.music.player.haige.mvp.ui.utils.Utils;
import com.music.player.haige.mvp.ui.utils.ViewVisibleUtils;
import com.music.player.haige.mvp.ui.widget.dialog.SystemDialog;

import org.simple.eventbus.EventBus;

import butterknife.BindView;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

/**
 * ================================================
 * Created by huangcong on 2018/3/2.
 * <p>
 * 设置
 * ================================================
 */

public class SettingFragment extends AppBaseFragment<SettingFPresenter> implements MVContract.CommonView, ToolbarConfig {

    @BindView(R.id.app_layout_toolbar)
    Toolbar mToolbar;

    @BindView(R.id.app_fragment_setting_4g_switch)
    Switch m4GAutoPlaySwitch;

    @BindView(R.id.app_fragment_setting_equalizer_layout)
    View mEqualizerLayout;

    @BindView(R.id.layout_settings_logout)
    View mLogoutView;

    @BindView(R.id.tv_cache_size)
    TextView mCacheSizeTv;

    public static SettingFragment newInstance() {
        SettingFragment fragment = new SettingFragment();
        return fragment;
    }

    @Override
    public Toolbar getToolbar() {
        return mToolbar;
    }

    @Override
    public String getTitle() {
        return getString(R.string.app_mine_setting);
    }

    @Override
    public boolean displayHomeAsUpEnabled() {
        return true;
    }

    @Override
    public boolean displayShowTitleEnabled() {
        return true;
    }

    @Override
    public boolean showOptionsMenu() {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                EventBus.getDefault().post(EventBusTags.MAIN.BACK);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void setupFragmentComponent(AppComponent appComponent) {
        DaggerCommonComponent
                .builder()
                .appComponent(appComponent)
                .commonModule(new CommonModule(this))
                .build()
                .inject(this);
    }

    @Override
    public View initView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.app_fragment_setting, container, false);
    }

    @Override
    public void initData(Bundle savedInstanceState) {
        // 系统是否自带音乐均衡器
        // if (HaigeUtil.hasEffectsPanel(getActivity())) {
        //     mEqualizerLayout.setVisibility(View.VISIBLE);
        // }
        m4GAutoPlaySwitch.setChecked(PreferencesUtility.getInstance(getContext()).is4GAutoPlay());
        ViewVisibleUtils.setVisibleGone(mLogoutView, UserPrefHelper.isLogin());
        mCacheSizeTv.setText(getAppCacheSize());
    }

    @Override
    public void onSuccess(Api.Action action, Object o) {
        super.onSuccess(action, o);

        switch (action) {
            case LOGIN_LOGOUT:
                Toast.makeText(getActivity(), R.string.common_logout_success, Toast.LENGTH_SHORT).show();
                EventBus.getDefault().post(EventBusTags.UI.LOGIN_LOGOUT);
                EventBus.getDefault().post(EventBusTags.MAIN.BACK);
                break;
        }
    }

    @Override
    public void onError(Api.Action action, String code, String message) {
        super.onError(action, code, message);

        switch (action) {
            case LOGIN_LOGOUT:
                Toast.makeText(getActivity(), R.string.common_logout_error, Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @OnClick({R.id.app_fragment_setting_equalizer_layout,
            R.id.app_fragment_setting_share_layout,
            R.id.app_fragment_setting_feedback_layout,
            R.id.app_fragment_setting_about_layout,
            R.id.layout_settings_logout,
            R.id.app_fragment_setting_clean_cache_layout})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.app_fragment_setting_share_layout:
                DownloadShareDialog.newInstance().show(getChildFragmentManager(), DownloadShareDialog.class.getName());
                break;
            case R.id.app_fragment_setting_feedback_layout:
                EventBus.getDefault().post(EventBusTags.MAIN.SETTING_FEEDBACK);
                break;
            case R.id.app_fragment_setting_equalizer_layout:
                NavigationUtil.navigateToEqualizer(getActivity());
                break;
            case R.id.app_fragment_setting_about_layout:
                EventBus.getDefault().post(EventBusTags.MAIN.SETTING_ABOUT);
                break;
            case R.id.layout_settings_logout:
                showSignoutDialog();
                break;
            case R.id.app_fragment_setting_clean_cache_layout:
                new SystemDialog().create(getChildFragmentManager())
                        .setContent(ResourceUtils.resourceString(R.string.dialog_clear_cache_content))
                        .setOk(ResourceUtils.resourceString(R.string.app_confirm))
                        .setCancle(ResourceUtils.resourceString(R.string.app_cancel))
                        .setOnOkListener(() -> {
                            if (DeleteFileUtils.deleteAllFile(ProxyConstants.MUSIC_PATH) && DeleteFileUtils.deleteAllFile(getMusicCachePath())) {
                                mCacheSizeTv.setText(getAppCacheSize());
                                Toast.makeText(getContext(), R.string.common_clear_cache_success, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getContext(), R.string.common_clear_cache_fail, Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setOnCancelListener(() -> {})
                        .show();
                break;
        }
    }

    @OnCheckedChanged(R.id.app_fragment_setting_4g_switch)
    public void onCheckedChanged(CompoundButton button, boolean checked) {
        switch (button.getId()) {
            case R.id.app_fragment_setting_4g_switch:
                PreferencesUtility.getInstance(getContext()).set4GAutoPlay(checked);
                break;
        }
    }

    /**
     * 退出确定Dialog
     */
    private void showSignoutDialog() {
        View view = getLayoutInflater().inflate(R.layout.dialog_bottom_signout, null);
        TextView logout = view.findViewById(R.id.tv_dialog_logout);
        TextView cancel = view.findViewById(R.id.tv_dialog_cancel);

        final Dialog mBottomSheetDialog = new Dialog(getActivity(), R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.show();

        logout.setOnClickListener(v -> {
            if (Utils.isNotNull(mPresenter)) {
                mPresenter.signout(getActivity());
            }
            if (Utils.isNotNull(mBottomSheetDialog)) {
                mBottomSheetDialog.dismiss();
            }
        });

        cancel.setOnClickListener(v -> mBottomSheetDialog.dismiss());
    }

    private String getMusicCachePath() {
        return StorageUtils.getIndividualCacheDirectory(getContext()).getPath();
    }

    private String getAppCacheSize() {
        long size = FileSizeUtils.getAutoFilesSize(ProxyConstants.MUSIC_PATH) + FileSizeUtils.getAutoFilesSize(getMusicCachePath());

        return FileSizeUtils.FormetFileSize(size);
    }
}
