package com.music.player.haige.app;

/**
 * ================================================
 * Created by huangcong on 2018/3/3.
 * <p>
 * 系统UI配置：全屏、状态栏颜色等
 * ================================================
 */

public interface SystemUIConfig {

    /**
     * @return
     */
    boolean translucentStatusBar();

    /**
     * @return
     */
    int setStatusBarColor();

    /**
     * @return
     */
    boolean lightStatusBar();

    /**
     * @return
     */
    boolean fitSystemWindows();

    /**
     * 全屏
     * @return
     */
    boolean fullScreen();
}
