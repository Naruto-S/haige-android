package com.music.player.haige.mvp.ui.main.widget;

import android.app.Activity;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.hc.core.utils.DeviceUtils;
import com.music.player.haige.R;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;

public class PlayMorePopWindow extends PopupWindow {

    private OnPlayMoreClickListener mListener;
    private Activity mActivity;
    private TextView mCloseBarrageTv;
    private TextView mCloseLyricTv;
    private View mRootView;
    private boolean isOpenBarrage = true;
    private boolean isOpenLyric = true;

    public interface OnPlayMoreClickListener {

        void onCloseBarrage(boolean isOpenBarrage);

        void onCloseLyric(boolean isOpenLyric);

    }

    public PlayMorePopWindow(Context context) {
        super(context);
        mActivity = (Activity) context;
        LayoutInflater inflater = LayoutInflater.from(context);
        mRootView = inflater.inflate(R.layout.layout_play_more_pop_window, null);
        mCloseBarrageTv = mRootView.findViewById(R.id.tv_close_barrage);
        mCloseLyricTv = mRootView.findViewById(R.id.tv_close_lyric);
        this.setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setTouchable(true);
        this.setFocusable(true);
        this.setOutsideTouchable(true);
        this.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_me_profile_more_popwindow));
        this.setContentView(mRootView);

        mCloseBarrageTv.setOnClickListener(v -> {
            if (mListener != null) {
                if (isOpenBarrage) {
                    isOpenBarrage = false;
                    mCloseBarrageTv.setText(ResourceUtils.resourceString(R.string.barrage_comment_open));
                } else {
                    isOpenBarrage = true;
                    mCloseBarrageTv.setText(ResourceUtils.resourceString(R.string.barrage_comment_close));
                }
                mListener.onCloseBarrage(isOpenBarrage);
            }
            this.dismiss();
        });
        mCloseLyricTv.setOnClickListener(v -> {
            if (mListener != null) {
                if (isOpenLyric) {
                    isOpenLyric = false;
                    mCloseLyricTv.setText(ResourceUtils.resourceString(R.string.lyric_open));
                } else {
                    isOpenLyric = true;
                    mCloseLyricTv.setText(ResourceUtils.resourceString(R.string.lyric_close));
                }
                mListener.onCloseLyric(isOpenLyric);
            }
            this.dismiss();
        });
    }

    @Override
    public void dismiss() {
        super.dismiss();
        setBackgroundAlpha(1f);
    }

    public void showPopWindow(View targetView) {
        if (isOpenBarrage) {
            mCloseBarrageTv.setText(ResourceUtils.resourceString(R.string.barrage_comment_close));
        } else {
            mCloseBarrageTv.setText(ResourceUtils.resourceString(R.string.barrage_comment_open));
        }

        if (isOpenLyric) {
            mCloseLyricTv.setText(ResourceUtils.resourceString(R.string.lyric_close));
        } else {
            mCloseLyricTv.setText(ResourceUtils.resourceString(R.string.lyric_open));
        }

        if (!this.isShowing()) {
            setBackgroundAlpha(0.5f);

            int windowPos[] = calculatePopWindowPos(targetView, mRootView);
            int xOff = 20;  // 可以自己调整偏移
            windowPos[0] -= xOff;
            this.showAtLocation(targetView, Gravity.TOP | Gravity.START, windowPos[0], windowPos[1]);
        } else {
            this.dismiss();
        }
    }

    public void setPlayMoreClickListener(OnPlayMoreClickListener listener) {
        mListener = listener;
    }

    private void setBackgroundAlpha(float alpha) {
        if (mActivity != null) {
            WindowManager.LayoutParams params = mActivity.getWindow().getAttributes();
            params.alpha = alpha;
            mActivity.getWindow().setAttributes(params);
        }
    }

    public void showCloseLyricTv() {
        mCloseLyricTv.setVisibility(View.VISIBLE);
    }

    public void hideCloseLyricTv() {
        mCloseLyricTv.setVisibility(View.GONE);
    }

    public boolean isOpenBarrage() {
        return isOpenBarrage;
    }

    public void setOpenBarrage(boolean openBarrage) {
        isOpenBarrage = openBarrage;
    }

    public boolean isOpenLyric() {
        return isOpenLyric;
    }

    public void setOpenLyric(boolean openLyric) {
        isOpenLyric = openLyric;
    }

    /**
     * 计算出来的位置，y方向就在anchorView的上面和下面对齐显示，x方向就是与屏幕右边对齐显示
     * 如果anchorView的位置有变化，就可以适当自己额外加入偏移来修正
     * @param anchorView  呼出window的view
     * @param contentView   window的内容布局
     * @return window显示的左上角的xOff,yOff坐标
     */
    private int[] calculatePopWindowPos(final View anchorView, final View contentView) {
        final int windowPos[] = new int[2];
        final int anchorLoc[] = new int[2];
        // 获取锚点View在屏幕上的左上角坐标位置
        anchorView.getLocationOnScreen(anchorLoc);
        final int anchorHeight = anchorView.getHeight();
        // 获取屏幕的高宽
        final int screenHeight = (int) DeviceUtils.getScreenHeight(anchorView.getContext());
        final int screenWidth = (int) DeviceUtils.getScreenWidth(anchorView.getContext());
        contentView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        // 计算contentView的高宽
        final int windowHeight = contentView.getMeasuredHeight();
        final int windowWidth = contentView.getMeasuredWidth();
        // 判断需要向上弹出还是向下弹出显示
        final boolean isNeedShowUp = (screenHeight - anchorLoc[1] - anchorHeight < windowHeight);
        if (isNeedShowUp) {
            windowPos[0] = screenWidth - windowWidth;
            windowPos[1] = anchorLoc[1] - windowHeight;
        } else {
            windowPos[0] = screenWidth - windowWidth;
            windowPos[1] = anchorLoc[1] + anchorHeight;
        }
        return windowPos;
    }
}
