package com.music.player.haige.mvp.presenter;

import com.music.player.haige.mvp.contract.AppBasePresenter;
import com.music.player.haige.mvp.contract.MVContract;

import javax.inject.Inject;

import me.jessyan.rxerrorhandler.core.RxErrorHandler;

public class MusicSharePresenter extends AppBasePresenter<MVContract.CommonModel, MVContract.CommonView> {

    public static final String TAG = CommentFPresenter.class.getSimpleName();

    @Inject
    RxErrorHandler mErrorHandler;


    @Inject
    public MusicSharePresenter(MVContract.CommonModel model, MVContract.CommonView view) {
        super(model, view);
    }
}
