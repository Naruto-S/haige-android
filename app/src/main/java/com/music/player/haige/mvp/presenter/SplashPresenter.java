package com.music.player.haige.mvp.presenter;

import android.Manifest;

import com.hc.core.utils.PermissionUtil;
import com.hc.core.utils.RxLifecycleUtils;
import com.music.player.haige.R;
import com.music.player.haige.app.base.pref.DevicePrefHelper;
import com.music.player.haige.mvp.contract.AppBasePresenter;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.entity.music.WebpBgResponse;
import com.music.player.haige.mvp.model.utils.NavigationUtil;
import com.music.player.haige.mvp.ui.utils.Utils;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.schedulers.Schedulers;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;
import me.jessyan.rxerrorhandler.handler.ErrorHandleSubscriber;

/**
 * ================================================
 * Created by huangcong on 2018/3/10.
 * ================================================
 */

public class SplashPresenter extends AppBasePresenter<MVContract.SplashModel, MVContract.SplashView> implements PermissionUtil.RequestPermission {

    @Inject
    RxErrorHandler mErrorHandler;

    @Inject
    public SplashPresenter(MVContract.SplashModel model, MVContract.SplashView view) {
        super(model, view);
    }

    @Override
    public void onRequestPermissionFailureWithAskNeverAgain(List<String> permissions) {
        // 打开设置
        PermissionUtil.showRequestPermissionDialog(mRootView.getActivity());
    }

    @Override
    public void onRequestPermissionSuccess() {
        Observable.empty()
                .delay(3000, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(Objects.requireNonNull(RxLifecycleUtils.bindToLifecycle(mRootView)))
                .doOnComplete(() -> {
                    NavigationUtil.navigateToMain(mRootView.getActivity());
                    mRootView.getActivity().finish();
                    mRootView.getActivity().overridePendingTransition(R.anim.activity_fade_in, R.anim.activity_fade_out);
                }).subscribe();
    }

    @Override
    public void onRequestPermissionFailure(List<String> permissions) {
        // 再次请求权限
        requestPermission();
    }

    public void requestPermission() {
        if (Utils.isNotNull(mRootView)) {
            PermissionUtil.requestPermission(this,
                    mRootView.getRxPermissions(), mErrorHandler,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.INTERNET);
        }
    }

    /**
     * 获取动态背景图
     */
    public void getMusicWebpBg(String type) {
        buildObservable(mModel.getMusicWebpBg(type))
                .subscribe(new ErrorHandleSubscriber<WebpBgResponse>(mErrorHandler) {
                    @Override
                    public void onNext(WebpBgResponse response) {
                        if (response != null) {
                            DevicePrefHelper.putMusicBg(response.getData());
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                    }
                });
    }
}
