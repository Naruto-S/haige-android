package com.music.player.haige.mvp.model.respository;

import android.content.Context;

import com.music.player.haige.mvp.model.entity.music.Album;
import com.music.player.haige.mvp.model.entity.music.Artist;
import com.music.player.haige.mvp.model.entity.music.FolderInfo;
import com.music.player.haige.mvp.model.entity.music.Playlist;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.model.respository.dataloader.AlbumLoader;
import com.music.player.haige.mvp.model.respository.dataloader.AlbumSongLoader;
import com.music.player.haige.mvp.model.respository.dataloader.ArtistAlbumLoader;
import com.music.player.haige.mvp.model.respository.dataloader.ArtistLoader;
import com.music.player.haige.mvp.model.respository.dataloader.ArtistSongLoader;
import com.music.player.haige.mvp.model.respository.dataloader.FolderLoader;
import com.music.player.haige.mvp.model.respository.dataloader.LastAddedLoader;
import com.music.player.haige.mvp.model.respository.dataloader.PlaylistLoader;
import com.music.player.haige.mvp.model.respository.dataloader.PlaylistSongLoader;
import com.music.player.haige.mvp.model.respository.dataloader.QueueLoader;
import com.music.player.haige.mvp.model.respository.dataloader.SongLoader;
import com.music.player.haige.mvp.model.respository.dataloader.TopTracksLoader;
import com.music.player.haige.mvp.model.respository.interfaces.Repository;
import com.music.player.haige.R;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 *
 */

public class RepositoryImpl implements Repository {

    private Context mContext;

    public RepositoryImpl(Context context) {
        mContext = context;
    }

    @Override
    public Observable<List<Album>> getAllAlbums() {
        return AlbumLoader.getAllAlbums(mContext);
    }

    @Override
    public Observable<Album> getAlbum(long id) {
        return AlbumLoader.getAlbum(mContext, id);
    }

    @Override
    public Observable<List<Album>> getAlbums(String paramString) {
        return AlbumLoader.getAlbums(mContext, paramString);
    }

    @Override
    public Observable<List<Song>> getSongsForAlbum(long albumID) {
        return AlbumSongLoader.getSongsForAlbum(mContext, albumID);
    }

    @Override
    public Observable<List<Album>> getAlbumsForArtist(long artistID) {
        return ArtistAlbumLoader.getAlbumsForArtist(mContext, artistID);
    }

    @Override
    public Observable<List<Artist>> getAllArtists() {
        return ArtistLoader.getAllArtists(mContext);
    }

    @Override
    public Observable<Artist> getArtist(long artistID) {
        return ArtistLoader.getArtist(mContext, artistID);
    }

    @Override
    public Observable<List<Artist>> getArtists(String paramString) {
        return ArtistLoader.getArtists(mContext, paramString);
    }

    @Override
    public Observable<List<Song>> getSongsForArtist(long artistID) {
        return ArtistSongLoader.getSongsForArtist(mContext, artistID);
    }

    @Override
    public Observable<List<Song>> getRecentlyAddedSongs() {
        return LastAddedLoader.getLastAddedSongs(mContext);
    }

    @Override
    public Observable<List<Album>> getRecentlyAddedAlbums() {
        return LastAddedLoader.getLastAddedAlbums(mContext);
    }

    @Override
    public Observable<List<Artist>> getRecentlyAddedArtists() {
        return LastAddedLoader.getLastAddedArtist(mContext);
    }

    @Override
    public Observable<List<Song>> getRecentlyPlayedSongs() {
        return TopTracksLoader.getTopRecentSongs(mContext);
    }

    @Override
    public Observable<List<Album>> getRecentlyPlayedAlbums() {
        return AlbumLoader.getRecentlyPlayedAlbums(mContext);
    }

    @Override
    public Observable<List<Artist>> getRecentlyPlayedArtist() {
        return ArtistLoader.getRecentlyPlayedArtist(mContext);
    }

    @Override
    public Observable<List<Playlist>> getPlaylist(boolean defaultIncluded) {
        return PlaylistLoader.getPlaylists(mContext, defaultIncluded);
    }

    @Override
    public Observable<List<Song>> getSongsInPlaylist(long playlistID) {
        return PlaylistSongLoader.getSongsInPlaylist(mContext, playlistID);
    }

    @Override
    public Observable<List<Song>> getQueueSongs() {
        return QueueLoader.getQueueSongs(mContext);
    }

    @Override
    public Observable<List<Song>> getFavoriteSongs() {
        return SongLoader.getFavoriteSong(mContext);
    }

    @Override
    public Observable<List<Album>> getFavoriteAlbums() {
        return AlbumLoader.getFavoriteAlbums(mContext);
    }

    @Override
    public Observable<List<Artist>> getFavoriteArtist() {
        return ArtistLoader.getFavouriteArtists(mContext);
    }

    @Override
    public Observable<List<Song>> getAllSongs() {
        return SongLoader.getAllSongs(mContext);
    }

    @Override
    public Observable<List<Song>> searchSongs(String searchString) {
        return SongLoader.searchSongs(mContext, searchString);
    }

    @Override
    public Observable<List<Song>> getTopPlaySongs() {
        return TopTracksLoader.getTopPlaySongs(mContext);
    }

    @Override
    public Observable<List<FolderInfo>> getFoldersWithSong() {
        return FolderLoader.getFoldersWithSong(mContext);
    }

    @Override
    public Observable<List<Song>> getSongsInFolder(String path) {
        return SongLoader.getSongListInFolder(mContext, path);
    }

    @Override
    public Observable<List<Object>> getSearchResult(String queryString) {
        Observable<Song> songList = SongLoader.searchSongs(mContext, queryString)
                .flatMap(new Function<List<Song>, Observable<Song>>() {
                    @Override
                    public Observable<Song> apply(List<Song> songs) throws Exception {
                        return Observable.fromIterable(songs);
                    }
                });

        Observable<Album> albumList = AlbumLoader.getAlbums(mContext, queryString)
                .flatMap(new Function<List<Album>, Observable<Album>>() {
                    @Override
                    public Observable<Album> apply(List<Album> albums) throws Exception {
                        return Observable.fromIterable(albums);
                    }
                });

        Observable<Artist> artistList = ArtistLoader.getArtists(mContext, queryString)
                .flatMap(new Function<List<Artist>, Observable<Artist>>() {
                    @Override
                    public Observable<Artist> apply(List<Artist> artists) throws Exception {
                        return Observable.fromIterable(artists);
                    }
                });

        Observable<String> songHeader = Observable.just(mContext.getString(R.string.app_music_songs));
        Observable<String> albumHeader = Observable.just(mContext.getString(R.string.app_music_album));
        Observable<String> artistHeader = Observable.just(mContext.getString(R.string.app_music_artist));

        return Observable.mergeArray(songHeader, songList, albumHeader, albumList, artistHeader, artistList).toList().toObservable();
    }
}
