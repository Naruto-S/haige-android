package com.music.player.haige.app;

import android.support.v7.widget.Toolbar;

/**
 * ================================================
 * Created by huangcong on 2018/2/28.
 * ================================================
 */

public interface ToolbarConfig {

    Toolbar getToolbar();

    /**
     * @return
     */
    String getTitle();

    /**
     * @return
     */
    boolean displayHomeAsUpEnabled();

    /**
     * @return
     */
    boolean displayShowTitleEnabled();

    /**
     * @return
     */
    boolean showOptionsMenu();
}
