package com.music.player.haige.di.module;

import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.AlbumModel;

import dagger.Module;
import dagger.Provides;

/**
 * ================================================
 * Created by huangcong on 2018/3/2.
 * ================================================
 */
@Module
public class AlbumModule {
    private MVContract.AlbumView view;

    public AlbumModule(MVContract.AlbumView view) {
        this.view = view;
    }

    @FragmentScope
    @Provides
    MVContract.AlbumView provideView() {
        return view;
    }

    @FragmentScope
    @Provides
    MVContract.AlbumModel provideModel(AlbumModel model) {
        return model;
    }

}
