package com.music.player.haige.mvp.ui.user;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.hc.core.di.component.AppComponent;
import com.music.player.haige.R;
import com.music.player.haige.app.EventBusTags;
import com.music.player.haige.app.SystemUIConfig;
import com.music.player.haige.app.base.BaseListActivity;
import com.music.player.haige.app.base.pref.UserPrefHelper;
import com.music.player.haige.app.base.recyclerview.BaseQuickAdapter;
import com.music.player.haige.app.base.recyclerview.listener.OnItemClickListener;
import com.music.player.haige.app.base.recyclerview.pullrefresh.HaigeRefreshHeader;
import com.music.player.haige.app.base.recyclerview.view.EmptyView;
import com.music.player.haige.app.constants.IntentConstants;
import com.music.player.haige.app.utils.launcher.ActivityLauncherStart;
import com.music.player.haige.di.component.DaggerFansListComponent;
import com.music.player.haige.di.module.FansListModule;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.entity.action.FollowActionRequest;
import com.music.player.haige.mvp.model.entity.user.UserBean;
import com.music.player.haige.mvp.presenter.FansListPresenter;
import com.music.player.haige.mvp.ui.user.adapter.FansListAdapter;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;
import com.music.player.haige.mvp.ui.utils.Utils;

import org.simple.eventbus.Subscriber;

import butterknife.BindView;
import butterknife.OnClick;

import static com.music.player.haige.mvp.model.entity.user.UserBean.USER_FOLLOW_STAUS_FOLLOW_ME;
import static com.music.player.haige.mvp.model.entity.user.UserBean.USER_FOLLOW_STAUS_FOLLOW_OTHER;
import static com.music.player.haige.mvp.model.entity.user.UserBean.USER_FOLLOW_STAUS_MUTUAL;
import static com.music.player.haige.mvp.model.entity.user.UserBean.USER_FOLLOW_STAUS_UNFOLLOW;

public class FansListActivity extends BaseListActivity<FansListPresenter> implements MVContract.FansListView,
        SystemUIConfig {

    public static final int TYPE_FOLLOWING = 0; //关注
    public static final int TYPE_FOLLOWER = 1;  //粉丝

    @BindView(R.id.app_fragment_title_tv)
    TextView mTitleTv;

    private UserBean mOtherUser;
    private int mFollowType = 0;

    @Override
    public void setupActivityComponent(AppComponent appComponent) {
        DaggerFansListComponent
                .builder()
                .appComponent(appComponent)
                .fansListModule(new FansListModule(this))
                .build()
                .inject(this);
    }

    @Override
    public int initView(Bundle savedInstanceState) {
        return R.layout.activity_fans_list;
    }

    @Override
    protected void setupView() {
        super.setupView();

        if (Utils.isNotNull(getIntent())) {
            mOtherUser = getIntent().getParcelableExtra(IntentConstants.EXTRA_OTHER_USER);
            mFollowType = getIntent().getIntExtra(IntentConstants.EXTRA_FOLLOW_TYPE, 0);
        }

        if (mFollowType == TYPE_FOLLOWING) {
            mTitleTv.setText(ResourceUtils.resourceString(R.string.common_follow));
        } else if (mFollowType == TYPE_FOLLOWER) {
            mTitleTv.setText(ResourceUtils.resourceString(R.string.common_fans));
        }
    }

    @Override
    protected void sendListReq(int pageNum, int pageSize, boolean refresh) {
        if (Utils.isNotNull(mOtherUser)) {
            if (mFollowType == TYPE_FOLLOWING) {
                mPresenter.getFollowing(mOtherUser.getUserId(), pageNum, pageSize, refresh);
            } else if (mFollowType == TYPE_FOLLOWER) {
                mPresenter.getFollower(mOtherUser.getUserId(), pageNum, pageSize, refresh);
            }
        }
    }

    @Override
    protected void reLoadData() {
        fetchPusherList(mAdapter, true);
    }

    @Override
    protected void loadMoreData() {
        fetchPusherList(mAdapter, false);
    }

    @Override
    protected RecyclerView.LayoutManager createLayoutManager() {
        return new LinearLayoutManager(this);
    }

    @Override
    protected OnItemClickListener createItemClickListener() {
        return new OnItemClickListener(false) {
            @Override
            public void SimpleOnItemClick(BaseQuickAdapter adapter, View view, int position) {
                if (Utils.isFastClick()) {
                    return;
                }

                UserBean user = (UserBean) adapter.getItem(position);
                ActivityLauncherStart.startOtherUser(FansListActivity.this, user);
            }

            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                if (Utils.isFastClick()) {
                    return;
                }

                switch (view.getId()) {
                    case R.id.tv_follow:
                        if (Utils.isEmptyCollection(adapter.getData()) || Utils.isNull(adapter.getItem(position))) return;

                        UserBean user = (UserBean) adapter.getItem(position);
                        FollowActionRequest request = new FollowActionRequest();
                        request.setUserId(UserPrefHelper.getUserId());
                        request.setBeFollowedId(user.getUserId());

                        switch (user.getFollowStatus()) {
                            case USER_FOLLOW_STAUS_UNFOLLOW:
                                user.setFollowStatus(USER_FOLLOW_STAUS_FOLLOW_OTHER);
                                mPresenter.follow(request);
                                break;
                            case USER_FOLLOW_STAUS_FOLLOW_ME:
                                user.setFollowStatus(USER_FOLLOW_STAUS_MUTUAL);
                                mPresenter.follow(request);
                                break;
                            case USER_FOLLOW_STAUS_FOLLOW_OTHER:
                                user.setFollowStatus(USER_FOLLOW_STAUS_UNFOLLOW);
                                mPresenter.unfollow(request);
                                break;
                            case USER_FOLLOW_STAUS_MUTUAL:
                                user.setFollowStatus(USER_FOLLOW_STAUS_FOLLOW_ME);
                                mPresenter.unfollow(request);
                                break;
                        }
                        adapter.setData(position, user);
                        break;

                    default:
                        break;
                }
            }
        };
    }

    @Override
    protected RecyclerView.OnScrollListener addRecyclerViewScrollListener() {
        return null;
    }

    @Override
    protected RecyclerView.ItemDecoration createDecoration() {
        return null;
    }

    @Override
    protected RecyclerView.ItemAnimator createItemAnimator() {
        return null;
    }

    @Override
    protected View createRefreshHeader() {
        return new HaigeRefreshHeader(this);
    }

    @Override
    protected Context createContext() {
        return this;
    }

    @Override
    protected BaseQuickAdapter createAdapter() {
        return new FansListAdapter(this, mFollowType);
    }

    @Override
    public int getEmptyType() {
        return EmptyView.EMPTY_NO_FOLLOW;
    }

    @Override
    public boolean translucentStatusBar() {
        return false;
    }

    @Override
    public int setStatusBarColor() {
        return Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP
                ? getResources().getColor(R.color.color_33000000)
                : getResources().getColor(R.color.color_00000000);
    }

    @Override
    public boolean lightStatusBar() {
        return false;
    }

    @Override
    public boolean fitSystemWindows() {
        return true;
    }

    @Override
    public boolean fullScreen() {
        return false;
    }

    @Subscriber
    public void onSubscriber(EventBusTags.FollowItemStatusEvent event) {
        reLoadData();
    }

    @OnClick({R.id.app_fragment_back_iv})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.app_fragment_back_iv:
                finish();
                break;
        }
    }
}
