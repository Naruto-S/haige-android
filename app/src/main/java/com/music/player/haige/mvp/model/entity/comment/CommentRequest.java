package com.music.player.haige.mvp.model.entity.comment;

import com.google.gson.annotations.SerializedName;
import com.music.player.haige.mvp.model.entity.user.UserBean;

import java.io.Serializable;

/**
 * Created by Naruto on 2018/5/12.
 */

public class CommentRequest implements Serializable {

    @SerializedName("music_id")
    private long musicId;

    @SerializedName("context")
    private String context;

    @SerializedName("post_at")
    private long postAt;

    @SerializedName("comment_id")
    private String commentId;

    @SerializedName("sub")
    private boolean sub;    //是否是子评论； 字符串: true | | false

    //本地使用
    private UserBean user;
    private int position;

    public long getMusicId() {
        return musicId;
    }

    public void setMusicId(long musicId) {
        this.musicId = musicId;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public long getPostAt() {
        return postAt;
    }

    public void setPostAt(long postAt) {
        this.postAt = postAt;
    }

    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    public boolean isSub() {
        return sub;
    }

    public void setSub(boolean sub) {
        this.sub = sub;
    }

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "CommentRequest{" +
                "musicId=" + musicId +
                ", context='" + context + '\'' +
                ", postAt=" + postAt +
                ", commentId=" + commentId +
                ", sub=" + sub +
                '}';
    }
}
