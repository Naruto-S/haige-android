package com.music.player.haige.di.module;

import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.RecommendModel;

import dagger.Module;
import dagger.Provides;

/**
 * ================================================
 * Created by huangcong on 2018/2/27.
 * ================================================
 */
@Module
public class RecommendModule {
    private MVContract.RecommendView view;

    public RecommendModule(MVContract.RecommendView view) {
        this.view = view;
    }

    @FragmentScope
    @Provides
    MVContract.RecommendView provideView() {
        return this.view;
    }

    @FragmentScope
    @Provides
    MVContract.RecommendModel provideModel(RecommendModel model) {
        return model;
    }

}
