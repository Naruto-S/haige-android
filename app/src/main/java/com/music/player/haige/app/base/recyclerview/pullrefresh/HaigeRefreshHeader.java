package com.music.player.haige.app.base.recyclerview.pullrefresh;

import android.content.Context;
import android.graphics.drawable.Animatable;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.image.ImageInfo;
import com.music.player.haige.BuildConfig;
import com.music.player.haige.R;

/**
 * Created by Naruto on 2016/9/13.
 * Refresh View
 */
public class HaigeRefreshHeader extends LinearLayout implements IRefreshStatus {

    private LinearLayout mContainer;
    private SimpleDraweeView mLoadingSDV;
    private Animatable mAnimatable;

    public HaigeRefreshHeader(Context context) {
        super(context);
        initView();
    }

    public HaigeRefreshHeader(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public HaigeRefreshHeader(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        mContainer = (LinearLayout) LayoutInflater.from(getContext()).inflate(
                R.layout.layout_haige_loading_header, null);

        addView(mContainer);

        ControllerListener controllerListener = new BaseControllerListener<ImageInfo>() {
            @Override
            public void onFinalImageSet(String id, @Nullable ImageInfo imageInfo, @Nullable Animatable anim) {
                if (anim != null) {
                    mAnimatable = anim;
                }
            }
        };
        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setUri(Uri.parse("res://" + BuildConfig.APPLICATION_ID + "/" + R.drawable.ic_webp_loading))
                .setControllerListener(controllerListener)
                .build();
        mLoadingSDV = (SimpleDraweeView) findViewById(R.id.sdv_loading);
        mLoadingSDV.setController(controller);
    }

    public void startAnim() {
        if (mAnimatable != null && !mAnimatable.isRunning()) {
            mAnimatable.start();
        }
    }

    public void stopAnim() {
        if (mAnimatable != null && mAnimatable.isRunning()) {
            mAnimatable.stop();
        }
    }

    @Override
    public void reset() {
        stopAnim();
    }

    @Override
    public void refreshing() {
        startAnim();
    }

    @Override
    public void pullToRefresh() {
    }

    @Override
    public void releaseToRefresh() {
    }

    @Override
    public void pullProgress(float pullDistance, float pullProgress) {
    }

}
