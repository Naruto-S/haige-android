package com.music.player.haige.di.component;

import com.hc.core.di.component.AppComponent;
import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.di.module.SearchModule;
import com.music.player.haige.mvp.ui.main.fragment.SearchFragment;

import dagger.Component;

/**
 * ================================================
 * Created by huangcong on 2018/3/3.
 * ================================================
 */
@FragmentScope
@Component(modules = {SearchModule.class}, dependencies = AppComponent.class)
public interface SearchComponent {

    void inject(SearchFragment fragment);
}
