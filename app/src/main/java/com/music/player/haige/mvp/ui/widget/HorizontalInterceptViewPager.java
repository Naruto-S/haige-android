package com.music.player.haige.mvp.ui.widget;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;

import com.hc.core.utils.CoreUtils;

import timber.log.Timber;

/**
 * ================================================
 * Created by huangcong on 2018/3/22.
 * <p>
 * 横向滑动拦截
 * ================================================
 */

public class HorizontalInterceptViewPager extends ViewPager {

    private float mDownX;
    private float mDownY;
    private float mTouchSlop;

    public HorizontalInterceptViewPager(Context context) {
        this(context, null);
    }

    public HorizontalInterceptViewPager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        mTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    public boolean onInterceptHoverEvent(MotionEvent event) {
        //Timber.e("===>>> onInterceptHoverEvent " + event.getY());
        return super.onInterceptHoverEvent(event);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        //Timber.e("===>>> dispatchTouchEvent " + event.getY());
        return super.dispatchTouchEvent(event);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        //Timber.e("===>>> onInterceptTouchEvent " + "x:" + event.getX() + ", y:" + event.getY());
        float x = event.getX();
        float y = event.getY();
        boolean intercept = super.onInterceptTouchEvent(event);
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mDownX = x;
                mDownY = y;
                break;
            case MotionEvent.ACTION_MOVE:
                float dx = Math.abs(x - mDownX);
                float dy = Math.abs(y - mDownY);
                //Timber.e("onInterceptTouchEvent ACTION_MOVE");
                if (!intercept && dx > mTouchSlop && dx * 0.5f > dy) {
                    intercept = true;
                }
                break;
            default:
                break;
        }
        return intercept;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        boolean touch = super.onTouchEvent(event);
        //Timber.e("===>>> onTouchEvent " + "x:" + event.getX() + ", y:" + event.getY() + ", touch:" + touch);
        return touch;
    }

    @Override
    protected boolean canScroll(View v, boolean checkV, int dx, int x, int y) {
        boolean canScroll = super.canScroll(v, checkV, dx, x, y);
        //Timber.e("===>>> canScroll  " + canScroll);
        return canScroll;
    }
}
