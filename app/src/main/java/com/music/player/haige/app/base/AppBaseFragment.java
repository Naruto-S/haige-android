package com.music.player.haige.app.base;

import com.hc.base.base.BaseFragment;
import com.hc.core.mvp.IPresenter;
import com.music.player.haige.mvp.model.api.Api;

/**
 * ================================================
 * Created by huangcong on 2018/3/19.
 * ================================================
 */

public abstract class AppBaseFragment<P extends IPresenter> extends BaseFragment<P> {

    public void onSuccess(Api.Action action, Object o) {

    }

    public void onError(Api.Action action, String code, String message) {

    }

}
