package com.music.player.haige.mvp.contract;

import android.app.Activity;

import com.hc.core.mvp.IModel;
import com.music.player.haige.mvp.model.entity.BaseResponse;
import com.music.player.haige.mvp.model.entity.action.ActionRequest;
import com.music.player.haige.mvp.model.entity.action.CommentLikeRequest;
import com.music.player.haige.mvp.model.entity.action.FollowActionRequest;
import com.music.player.haige.mvp.model.entity.action.SendFeedbackRequest;
import com.music.player.haige.mvp.model.entity.comment.CommentRequest;
import com.music.player.haige.mvp.model.entity.comment.CommentResponse;
import com.music.player.haige.mvp.model.entity.comment.SendCommentResponse;
import com.music.player.haige.mvp.model.entity.music.Album;
import com.music.player.haige.mvp.model.entity.music.Artist;
import com.music.player.haige.mvp.model.entity.music.FolderInfo;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.model.entity.music.UploadMusicRequest;
import com.music.player.haige.mvp.model.entity.music.UploadMusicResponse;
import com.music.player.haige.mvp.model.entity.music.WebpBgResponse;
import com.music.player.haige.mvp.model.entity.rank.NewRankTagsResponse;
import com.music.player.haige.mvp.model.entity.rank.RankResponse;
import com.music.player.haige.mvp.model.entity.recommend.RecommendResponse;
import com.music.player.haige.mvp.model.entity.search.SearchHotWordResponse;
import com.music.player.haige.mvp.model.entity.search.SearchKeyWordResponse;
import com.music.player.haige.mvp.model.entity.user.FollowUserResponse;
import com.music.player.haige.mvp.model.entity.user.LoginRequest;
import com.music.player.haige.mvp.model.entity.user.ModifyUserRequest;
import com.music.player.haige.mvp.model.entity.user.UserResponse;
import com.music.player.haige.mvp.usecase.GetAlbums;
import com.music.player.haige.mvp.usecase.GetArtists;
import com.music.player.haige.mvp.usecase.GetFolders;
import com.music.player.haige.mvp.usecase.GetSongs;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import okhttp3.MultipartBody;


/**
 * ================================================
 * Created by huangcong on 2018/2/22.
 * <p>
 * ================================================
 */

public interface MVContract {
    //对于经常使用的关于UI的方法可以定义到IView中,如显示隐藏进度条,和显示文字消息
    interface CommonView extends AppIView {

        Activity getActivity();

        //申请权限
        RxPermissions getRxPermissions();
    }

    //Model层定义接口,外部只需关心Model返回的数据,无需关心内部细节,如是否使用缓存
    interface CommonModel extends IModel {

    }

    interface CommonListView extends AppIView {

        void loadRequestStarted();

        void showErrorNetwork();

        void showLoadingError();

        void showNoMoreView();

        void hideLoadingView();

        void loadRequestCompleted();

        void loadEnded();

        void showRefresh(ArrayList arrayList);

        void showLoadMore(ArrayList arrayList);

    }

    interface CommonListModel extends IModel {

    }

    interface SplashView extends CommonView {

    }

    interface SplashModel extends CommonModel {

        Observable<WebpBgResponse> getMusicWebpBg(String type);

    }

    interface FeedbackView extends CommonView {

    }

    interface FeedbackModel extends CommonModel {

        Observable<BaseResponse> sendFeedback(SendFeedbackRequest request);

    }

    // "推荐"
    interface RecommendView extends CommonListView {

        void onUpateMusicTime();

    }

    interface RecommendModel extends CommonListModel {
        Observable<ArrayList<Song>> loadCurrentMusic();

        Observable<ArrayList<Song>> loadRecommendMusic(int pageNum, int pageSize);

        Observable<File> getLyricFile(String title, String artist, long duration);

        Observable<BaseResponse> countLike(long musicId);

        Observable<BaseResponse> countShare(long musicId);

        Observable<BaseResponse> countDownload(long musicId);

        Observable<WebpBgResponse> getMusicWebpBg(String type);

        Observable<SendCommentResponse> sendMusicComment(CommentRequest request);

        Observable<CommentResponse> getMusicComment(long musicId, int pageSize, String lastId);

        Observable<CommentResponse> getCommentSub(String commentId, int pageSize, int pageNo);

        Observable<BaseResponse> deleteComment(CommentRequest request);

        Observable<BaseResponse> commentLike(CommentLikeRequest request);
    }

    interface NowPlayView extends AppIView {

        void onUpateMusicTime();

    }

    interface NowPlayModel extends IModel {

        Observable<ArrayList<Song>> loadCurrentMusic();

        Observable<WebpBgResponse> getMusicWebpBg(String type);

        Observable<SendCommentResponse> sendMusicComment(CommentRequest request);

        Observable<CommentResponse> getMusicComment(long musicId, int pageSize, String lastId);

        Observable<CommentResponse> getCommentSub(String commentId, int pageSize, int pageNo);

        Observable<File> getLyricFile(String title, String artist, long duration);

        Observable<BaseResponse> countLike(long musicId);

        Observable<BaseResponse> countShare(long musicId);

        Observable<BaseResponse> countDownload(long musicId);

        Observable<BaseResponse> deleteComment(CommentRequest request);

        Observable<BaseResponse> commentLike(CommentLikeRequest request);

    }

    // 本地歌曲
    interface SongsView extends AppIView {

    }

    interface SongsModel extends IModel {

        GetSongs.ResponseValue loadSongs(String action);
    }

    interface ArtistView extends AppIView {

        void showArtists(List<Artist> artists);

        void showEmptyView();
    }

    interface ArtistModel extends IModel {
        GetArtists.ResponseValue loadArtists(String action);
    }

    interface AlbumView extends AppIView {

        void showAlbum(List<Album> artists);

        void showEmptyView();
    }

    interface AlbumModel extends IModel {
        GetAlbums.ResponseValue loadAlbum(String action);
    }

    interface FoldersView extends AppIView {

        void showFolders(List<FolderInfo> folders);

        void showEmptyView();
    }

    interface FoldersModel extends IModel {
        GetFolders.ResponseValue loadFolders();
    }

    interface PlayQueueView extends AppIView {

        void onUpateMusicTime();

    }

    interface PlayQueueModel extends IModel {
        Observable<ArrayList<Song>> loadCurrentMusic();
    }

    interface RankingView extends AppIView {
    }

    interface RankingModel extends IModel {

        Observable<NewRankTagsResponse> getRankingTags();

    }

    interface RankView extends CommonListView {
    }

    interface RankModel extends CommonListModel {

        Observable<RankResponse> loadMusicRank(String tag, int pageNum, int pageSize);
    }

    interface SearchView extends AppIView {
    }

    interface SearchModel extends IModel {
        Observable<SearchHotWordResponse> loadHotWord(int size);
    }

    interface SearchResultView extends CommonListView {

    }

    interface SearchResultModel extends CommonListModel {
        /**
         * @param tagId      如果是通过热词搜索需要传入此字段，否则传0
         * @param pageNum
         * @param searchWord
         * @return
         */
        Observable<SearchKeyWordResponse> searchKeyWord(int tagId, int pageNum, int pageSize, String searchWord);

        /**
         * 搜索本地音乐
         *
         * @param searchWord
         * @return
         */
        Observable<List<Song>> searchLocalKeyWord(String searchWord);

        /**
         * 搜索本地音乐
         *
         * @param searchWord
         * @return
         */
        Observable<List<Song>> searchRecentKeyWord(String searchWord);
    }

    /**
     * 音乐下载
     */
    interface DownloadView extends AppIView {

    }

    interface DownloadModel extends IModel {

    }

    // 已下载
    interface DownloadFinishView extends AppIView {

    }

    interface DownloadFinishModel extends IModel {

    }

    // 正在下载
    interface DownloadingView extends AppIView {

    }

    interface DownloadingModel extends IModel {

    }

    interface LoginView extends AppIView {
        void showProgressBar();

        void hideProgressBar();
    }

    interface LoginModel extends IModel {

        Observable<UserResponse> userLoginOrRegistrer(LoginRequest request);

        Observable<UserResponse> getUserProfile();
    }

    interface CommentView extends AppIView {

    }

    interface CommentModel extends IModel {

    }

    interface MeProfileView extends AppIView {

        void setAllSongsCount(int count);

        void setFavoriteSongsCount(int count);

        void setRecentPlaySongsCount(int count);

        void setDownloadSongsCount(int count);

    }

    interface MeProfileModel extends IModel {

        Observable<UserResponse> getUserProfile();

        GetSongs.ResponseValue loadAllSongs(String action);

    }

    interface EditProfileView extends AppIView {

    }

    interface EditProfileModel extends IModel {

        Observable<BaseResponse> modifyUserProfile(ModifyUserRequest request);

    }

    interface CropView extends AppIView {

        void showProgress();

        void hideProgress();

    }

    interface CropModel extends IModel {

        Observable<BaseResponse> uploadAvatar(List<MultipartBody.Part> partList);

    }

    interface OtherProfileView extends AppIView {

    }

    interface OtherProfileModel extends IModel {

        Observable<UserResponse> getOtherUser(String userId);

        Observable<BaseResponse> follow(FollowActionRequest request);

        Observable<BaseResponse> unfollow(FollowActionRequest request);

    }

    interface FansListView extends CommonListView {

    }

    interface FansListModel extends CommonListModel {
        Observable<FollowUserResponse> getFollowing(String userId, int pageNo, int pageSize);

        Observable<FollowUserResponse> getFollower(String userId, int pageNo, int pageSize);

        Observable<BaseResponse> follow(FollowActionRequest request);

        Observable<BaseResponse> unfollow(FollowActionRequest request);
    }

    interface UploadMusicView extends CommonListView {

    }

    interface UploadMusicModel extends CommonListModel {

        Observable<RecommendResponse> getUploadMusic(String userId, int pageNo, int pageSize);

        Observable<UploadMusicResponse> getUploadMusicStatus(int pageNo, int pageSize);

        Observable<BaseResponse> deleteMusic(ActionRequest request);

    }

    interface LikeMusicView extends CommonListView {

    }

    interface LikeMusicModel extends CommonListModel {

        Observable<RecommendResponse> getLikedList(String userId, int pageNo, int pageSize);

    }

    interface UpdateView extends AppIView {

    }

    interface UpdateModel extends IModel {

        Observable<BaseResponse> uploadMusic(UploadMusicRequest request);

    }
}
