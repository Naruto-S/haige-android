package com.music.player.haige.mvp.ui.music.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.music.player.haige.R;
import com.music.player.haige.mvp.ui.widget.MusicVisualizer;

/**
 * ================================================
 * Created by huangcong on 2018/3/10.
 * ================================================
 */

public class SongItemViewHolder extends RecyclerView.ViewHolder {

    public TextView rank;
    public TextView title;
    public TextView artist;
    public TextView album;
    public ImageView popupMenu;
    public View playScore;
    public MusicVisualizer musicVisualizer;

    public SongItemViewHolder(View view) {
        super(view);
        this.rank = view.findViewById(R.id.app_item_rank);
        this.title = view.findViewById(R.id.text_item_title);
        this.artist = view.findViewById(R.id.text_item_subtitle);
        this.album = view.findViewById(R.id.text_item_subtitle_2);
        this.popupMenu = view.findViewById(R.id.popup_menu);
        this.playScore = view.findViewById(R.id.app_item_play_score);
        this.musicVisualizer = view.findViewById(R.id.app_item_music_visualizer);
    }
}
