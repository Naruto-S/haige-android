package com.music.player.haige.app.utils.statistics;


import android.content.Context;

import com.hc.core.utils.DeviceUtils;
import com.music.player.haige.app.HaigeApplication;

import java.util.HashMap;
import java.util.Map;

public class AppsflyerUtils extends CommonStatistics {

    private static AppsflyerUtils mAppsflyerUtils = null;
    private String mDevice;

    private AppsflyerUtils() {
        mDevice = DeviceUtils.getDeviceUUID(HaigeApplication.getInstance());
    }

    public static AppsflyerUtils getInstance() {
        if (mAppsflyerUtils == null) {
            synchronized (AppsflyerUtils.class) {
                if (mAppsflyerUtils == null) {
                    mAppsflyerUtils = new AppsflyerUtils();
                }
            }
        }
        return mAppsflyerUtils;
    }

    @Override
    public void onResume(Context context) {

    }

    @Override
    public void onPause(Context context) {

    }

    @Override
    public void onPageStart(String value) {

    }

    @Override
    public void onPageEnd(String value) {

    }

    @Override
    public void onEvent(String eventName) {
        Map<String, Object> eventValue = new HashMap<>();
        eventValue.put(eventName, eventName);
        eventValue.put(StatisticsConstant.USER_DEVICE_ID, mDevice);
//        AppsFlyerLib.getInstance().trackEvent(HaigeApplication.getInstance(), eventName, eventValue);
    }

    @Override
    public void onEvent(String eventName, String value) {
        Map<String, Object> eventValue = new HashMap<>();
        eventValue.put(eventName, value);
        eventValue.put(StatisticsConstant.USER_DEVICE_ID, mDevice);
//        AppsFlyerLib.getInstance().trackEvent(HaigeApplication.getInstance(), eventName, eventValue);
    }

    @Override
    public void onEvent(String eventName, Map<String, Object> value) {
        value.put(StatisticsConstant.USER_DEVICE_ID, mDevice);
//        AppsFlyerLib.getInstance().trackEvent(HaigeApplication.getInstance(), eventName, value);
    }
}
