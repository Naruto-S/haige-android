package com.music.player.haige.mvp.ui.music.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hc.base.utils.ATEUtil;
import com.hc.core.utils.DeviceUtils;
import com.music.player.haige.R;
import com.music.player.haige.app.base.recyclerview.BaseQuickAdapter;
import com.music.player.haige.app.base.recyclerview.BaseViewHolder;
import com.music.player.haige.app.image.loader.BaseImageLoader;
import com.music.player.haige.app.image.loader.LocalImageLoader;
import com.music.player.haige.app.image.release.DisplayOptions;
import com.music.player.haige.app.image.widget.HaigeImageView;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.model.utils.StringUtils;
import com.music.player.haige.mvp.ui.music.service.MusicServiceConnection;
import com.music.player.haige.mvp.ui.utils.DebugLogger;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;
import com.music.player.haige.mvp.ui.widget.MusicVisualizer;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MusicListAdapter extends BaseQuickAdapter<Song, BaseViewHolder> {

    private static final int ITEM_TOP = 1;
    private static final int ITEM_NORMAL = 2;

    private boolean isShowTopMusic;

    public MusicListAdapter(Context context, boolean isShowTopMusic) {
        super(context, 0, null);
        this.isShowTopMusic = isShowTopMusic;
    }

    @Override
    public BaseViewHolder onCreateDefViewHolder(ViewGroup parent, int viewType) {
        BaseViewHolder viewHolder;
        if (viewType == ITEM_TOP) {
            viewHolder = new TopMusicViewHolder(mLayoutInflater.inflate(R.layout.app_item_top_music, parent, false));
        } else {
            viewHolder = new MusicViewHolder(mLayoutInflater.inflate(R.layout.app_item_music, parent, false));
        }
        viewHolder.addOnClickListener(R.id.popup_menu);
        return viewHolder;
    }

    @Override
    protected void convert(BaseViewHolder itemHolder, Song song) {
        if (itemHolder.getItemViewType() == ITEM_TOP) {
            onBindTopViewHolder(itemHolder, song);
        } else {
            onBindNormalViewHolder(itemHolder, song);
        }
    }

    private void onBindTopViewHolder(BaseViewHolder holder, Song song) {
        TopMusicViewHolder itemHolder = (TopMusicViewHolder) holder;
        int position = itemHolder.getPosition() - getHeaderLayoutCount();
        if (position == 0) {
            itemHolder.goldMedalIv.setImageResource(R.drawable.app_ic_rank_gold);
        } else if (position == 1) {
            itemHolder.goldMedalIv.setImageResource(R.drawable.app_ic_rank_silver);
        } else if (position == 2) {
            itemHolder.goldMedalIv.setImageResource(R.drawable.app_ic_rank_copper);
        }

        if (StringUtils.isEmpty(song.musicCover)) {
            LocalImageLoader.displayResImage(ATEUtil.getDefaultAlbumResourceId(mContext), itemHolder.goldCoverIv);
        } else {
            BaseImageLoader.displaySimpleImage(
                    song.musicCover,
                    new DisplayOptions.Builder()
                            .showImageOnLoading(R.drawable.app_ic_album_default)
                            .showImageOnFail(R.drawable.app_ic_album_default),
                    itemHolder.goldCoverIv);
        }
        itemHolder.goldNameTv.setText(song.musicName);
        itemHolder.goldArtistTv.setText(song.getArtistName(mContext));
        if (MusicServiceConnection.getCurrentAudioId() == song.songId) {
            itemHolder.goldNameTv.setTextColor(ResourceUtils.getColor(R.color.colorAccent));
            if (MusicServiceConnection.isPlaying()) {
                itemHolder.goldMedalIv.setVisibility(View.GONE);
                itemHolder.goldMusicVis.setColor(ResourceUtils.getColor(R.color.colorAccent));
                itemHolder.goldMusicVis.setVisibility(View.VISIBLE);
            } else {
                itemHolder.goldMedalIv.setVisibility(View.VISIBLE);
                itemHolder.goldMusicVis.setVisibility(View.GONE);
            }
        } else {
            itemHolder.goldMedalIv.setVisibility(View.VISIBLE);
            itemHolder.goldNameTv.setTextColor(ATEUtil.getThemeTextColorPrimary(mContext));
            itemHolder.goldMusicVis.setVisibility(View.GONE);
        }
    }


    private void onBindNormalViewHolder(BaseViewHolder holder, Song song) {
        MusicViewHolder itemHolder = (MusicViewHolder) holder;
        int position = itemHolder.getPosition() - getHeaderLayoutCount();

        itemHolder.rank.setText(String.valueOf(position + 1));
        itemHolder.title.setText(song.musicName);
        itemHolder.artist.setText(song.getArtistName(mContext));
        itemHolder.album.setText(song.albumName);

        if (MusicServiceConnection.getCurrentAudioId() == song.songId) {
            itemHolder.title.setTextColor(ResourceUtils.getColor(R.color.colorAccent));
            if (MusicServiceConnection.isPlaying()) {
                itemHolder.rank.setVisibility(View.GONE);
                itemHolder.musicVisualizer.setVisibility(View.VISIBLE);
                itemHolder.musicVisualizer.setColor(ResourceUtils.getColor(R.color.colorAccent));
            } else {
                itemHolder.rank.setVisibility(View.VISIBLE);
                itemHolder.rank.setTextColor(ResourceUtils.getColor(R.color.colorAccent));
                itemHolder.musicVisualizer.setVisibility(View.GONE);
            }
        } else {
            itemHolder.rank.setVisibility(View.VISIBLE);
            itemHolder.rank.setTextColor(ATEUtil.getThemeTextColorPrimary(mContext));
            itemHolder.title.setTextColor(ATEUtil.getThemeTextColorPrimary(mContext));
            itemHolder.musicVisualizer.setVisibility(View.GONE);
        }
    }

    @Override
    protected int getDefItemViewType(int position) {
        if (isShowTopMusic && (position == 0 || position == 1 || position == 2)) {
            return ITEM_TOP;
        }
        return ITEM_NORMAL;
    }

    class TopMusicViewHolder extends BaseViewHolder {

        @BindView(R.id.app_item_rank_top_gold_medal_iv)
        ImageView goldMedalIv;
        @BindView(R.id.app_item_rank_top_gold_cover_iv)
        HaigeImageView goldCoverIv;
        @BindView(R.id.app_item_rank_top_gold_visualizer)
        MusicVisualizer goldMusicVis;
        @BindView(R.id.app_item_rank_top_gold_name_tv)
        TextView goldNameTv;
        @BindView(R.id.app_item_rank_top_gold_artist_tv)
        TextView goldArtistTv;

        public TopMusicViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class MusicViewHolder extends BaseViewHolder {

        @BindView(R.id.app_item_rank)
        TextView rank;
        @BindView(R.id.text_item_title)
        TextView title;
        @BindView(R.id.text_item_subtitle)
        TextView artist;
        @BindView(R.id.text_item_subtitle_2)
        TextView album;
        @BindView(R.id.popup_menu)
        ImageView popupMenu;
        @BindView(R.id.app_item_play_score)
        View playScore;
        @BindView(R.id.app_item_music_visualizer)
        MusicVisualizer musicVisualizer;

        public MusicViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
