package com.music.player.haige.mvp.ui.main.fragment;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hc.base.base.BaseFragment;
import com.hc.base.widget.SlidingTabLayout;
import com.hc.core.di.component.AppComponent;
import com.music.player.haige.R;
import com.music.player.haige.app.Constants;
import com.music.player.haige.app.EventBusTags;
import com.music.player.haige.app.base.AppBaseFragment;
import com.music.player.haige.di.component.DaggerCommonComponent;
import com.music.player.haige.di.module.CommonModule;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.presenter.PlayMainFPresenter;
import com.music.player.haige.mvp.ui.widget.adapter.TitlePagerAdapter;

import org.simple.eventbus.Subscriber;

import butterknife.BindView;

public class PlayMainFragment extends AppBaseFragment<PlayMainFPresenter> implements MVContract.CommonView {

    public static final int PLAY_MAIN_CURRENT_LIST = 0; //播放列表
    public static final int PLAY_MAIN_CURRENT_PLAY = 1; //播放页

    @BindView(R.id.app_fragment_find_tl)
    SlidingTabLayout mTabLayout;

    @BindView(R.id.app_fragment_find_vp)
    ViewPager mViewPager;

    private int mCurrentItem = 0;

    public static PlayMainFragment newInstance(int item) {
        PlayMainFragment fragment = new PlayMainFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.PLAY_MAIN_CURRENT_ITEM, item);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void setupFragmentComponent(AppComponent appComponent) {
        DaggerCommonComponent
                .builder()
                .appComponent(appComponent)
                .commonModule(new CommonModule(this))
                .build()
                .inject(this);
    }

    @Override
    public View initView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.app_fragment_play_main, container, false);
    }

    @Override
    public void initData(Bundle savedInstanceState) {
        mCurrentItem = getArguments().getInt(Constants.PLAY_MAIN_CURRENT_ITEM);

        String[] titles = getResources().getStringArray(R.array.app_main_play_main);
        BaseFragment[] fragments = new BaseFragment[titles.length];
        fragments[0] = PlayQueueFragment.newInstance();
        fragments[1] = NowPlayFragment.newInstance();
        TitlePagerAdapter adapter = new TitlePagerAdapter(getChildFragmentManager(), titles, fragments);
        mViewPager.setAdapter(adapter);
        mViewPager.setOffscreenPageLimit(adapter.getCount() - 1);
        mTabLayout.setViewPager(mViewPager);
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mViewPager.setCurrentItem(mCurrentItem);
    }

    @Subscriber
    public void onSubscriber(EventBusTags.PlayMain tags) {
        switch (tags) {
            case CURRENT_LIST:
                mViewPager.setCurrentItem(PLAY_MAIN_CURRENT_LIST);
                break;
            case CURRENT_PLAY:
                mViewPager.setCurrentItem(PLAY_MAIN_CURRENT_PLAY);
                break;
        }
    }
}
