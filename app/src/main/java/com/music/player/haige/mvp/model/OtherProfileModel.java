package com.music.player.haige.mvp.model;

import com.hc.base.mvp.BaseModel;
import com.hc.core.integration.IRepositoryManager;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.service.ApiService;
import com.music.player.haige.mvp.model.entity.BaseResponse;
import com.music.player.haige.mvp.model.entity.action.FollowActionRequest;
import com.music.player.haige.mvp.model.entity.user.UserResponse;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by Naruto on 2018/5/13.
 */

public class OtherProfileModel extends BaseModel implements MVContract.OtherProfileModel {

    @Inject
    public OtherProfileModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public Observable<UserResponse> getOtherUser(String userId) {
        return Observable.just(mRepositoryManager.obtainRetrofitService(ApiService.class).getOtherUser(userId))
                .flatMap(observable -> observable);
    }

    @Override
    public Observable<BaseResponse> follow(FollowActionRequest request) {
        return Observable.just(mRepositoryManager.obtainRetrofitService(ApiService.class).follow(request))
                .flatMap(observable -> observable);
    }

    @Override
    public Observable<BaseResponse> unfollow(FollowActionRequest request) {
        return Observable.just(mRepositoryManager.obtainRetrofitService(ApiService.class).unfollow(request))
                .flatMap(observable -> observable);
    }
}
