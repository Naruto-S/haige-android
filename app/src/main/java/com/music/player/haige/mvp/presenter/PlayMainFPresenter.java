package com.music.player.haige.mvp.presenter;

import com.hc.base.mvp.BasePresenter;
import com.music.player.haige.mvp.contract.MVContract;

import javax.inject.Inject;

public class PlayMainFPresenter extends BasePresenter<MVContract.CommonModel, MVContract.CommonView> {

    @Inject
    public PlayMainFPresenter(MVContract.CommonModel model, MVContract.CommonView view) {
        super(model, view);
    }
}
