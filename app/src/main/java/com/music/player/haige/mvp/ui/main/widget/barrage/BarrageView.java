package com.music.player.haige.mvp.ui.main.widget.barrage;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.animation.LinearInterpolator;
import android.widget.RelativeLayout;

import com.hc.core.utils.DeviceUtils;
import com.music.player.haige.mvp.model.entity.comment.CommentBean;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

/**
 * Created by Naruto on 2016/9/1.
 */
public class BarrageView extends RelativeLayout {

    protected static final int MSG_ADD_BARRAGE = 0x1;
    private static final int BARRAGE_MAX_LINE = 4;//弹幕的总行数

    private BarrageHandler mHandler = new BarrageHandler(this);
    private boolean isFirstLineCanNext = true;
    private boolean isSecondLineCanNext = true;
    private boolean isThirdLineCanNext = true;
    private boolean isFourthLineCanNext = true;
    private boolean isBarragePause = false;
    private int mMinTextWidth;
    private int mAvatarWidth;
    private LinkedList<CommentBean> mBarrageModels = new LinkedList<>();
    private HashMap<BarrageItemView, ObjectAnimator> mBarrageAnimMap = new HashMap<>();

    public BarrageView(Context context) {
        this(context, null);
    }

    public BarrageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BarrageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /**
     * 添加一条弹幕数据
     */
    public void enqueueBarrage(CommentBean commentBean) {
        mBarrageModels.add(commentBean);
        mHandler.sendEmptyMessage(MSG_ADD_BARRAGE);
    }

    private void dequeueBarrage() {
        if (mBarrageModels != null) {
            if (mBarrageModels.size() == 0 || isBarragePause) {
                return;
            }

            if (isFirstLineCanNext) {
                createBarrageItem(mBarrageModels.removeFirst(), 4);
                isFirstLineCanNext = false;
            } else if (isSecondLineCanNext) {
                createBarrageItem(mBarrageModels.removeFirst(), 3);
                isSecondLineCanNext = false;
            } else if (isThirdLineCanNext) {
                createBarrageItem(mBarrageModels.removeFirst(), 2);
                isThirdLineCanNext = false;
            } else if (isFourthLineCanNext) {
                createBarrageItem(mBarrageModels.removeFirst(), 1);
                isFourthLineCanNext = false;
            }
        }
    }

    /**
     * 创建一条弹幕
     */
    private void createBarrageItem(CommentBean commentBean, int barrageLine) {
        BarrageItem item = new BarrageItem();
        item.mItemView = new BarrageItemView(getContext(), commentBean);
        item.mItemView.setVisibility(GONE);
        item.mItemView.onFinishInflate();
        item.textMeasuredWidth = (int) getTextWidth(item, commentBean.getContext());
        item.moveSpeed = calculateSpeed((float) item.textMeasuredWidth);
        item.verticalPos = (BARRAGE_MAX_LINE - barrageLine) * ResourceUtils.dpToPX(44);
        item.barrageLine = barrageLine;
        item.moveValue = 0;
        item.isNoticeNext = false;
        item.mItemView.setTextWidth(item.textMeasuredWidth);
        showBarrageItem(item);
    }

    /**
     * 显示弹幕
     *
     * @param item
     */
    private void showBarrageItem(final BarrageItem item) {

        final int leftMargin = this.getRight() - this.getLeft() - this.getPaddingLeft();

        LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        params.topMargin = item.verticalPos;
        this.addView(item.mItemView, params);
        ObjectAnimator animator = createTranslateAnim(item, leftMargin);
        animator.addListener(new AnimatorListenerAdapter() {

            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                item.mItemView.setVisibility(VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                item.mItemView.clearAnimation();
                BarrageView.this.removeView(item.mItemView);
                mBarrageAnimMap.remove(item.mItemView);
            }
        });
        animator.addUpdateListener(animation -> {
            float value = (float) animation.getAnimatedValue();
            item.moveValue = value;
            if ((leftMargin - value) > item.textMeasuredWidth && !item.isNoticeNext) {
                if (item.barrageLine == 4) {
                    isFirstLineCanNext = true;
                } else if (item.barrageLine == 3) {
                    isSecondLineCanNext = true;
                } else if (item.barrageLine == 2) {
                    isThirdLineCanNext = true;
                } else if (item.barrageLine == 1) {
                    isFourthLineCanNext = true;
                }

                item.isNoticeNext = true;
                mHandler.sendEmptyMessageDelayed(MSG_ADD_BARRAGE, 500);
            }
        });
        mBarrageAnimMap.put(item.mItemView, animator);
        animator.start();
    }

    /**
     * 弹幕移动进入动画
     *
     * @param item
     * @param leftMargin
     * @return
     */
    private ObjectAnimator createTranslateAnim(BarrageItem item, int leftMargin) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(item.mItemView, "translationX", leftMargin, -item.textMeasuredWidth);
        animator.setDuration(item.moveSpeed);
        animator.setInterpolator(new LinearInterpolator());
        return animator;
    }

    /**
     * 计算TextView中字符串的长度
     *
     * @param text 要计算的字符串
     * @return TextView中字符串的长度
     */
    private float getTextWidth(BarrageItem item, String text) {
        mMinTextWidth = ResourceUtils.dpToPX(42);
        mAvatarWidth =  ResourceUtils.dpToPX(56);

        try {
            Rect bounds = new Rect();
            TextPaint paint;
            paint = item.mItemView.getContentTv().getPaint();
            paint.getTextBounds(text, 0, text.length(), bounds);
            if (bounds.width() < mMinTextWidth) {
                return mMinTextWidth + mAvatarWidth;
            }
            return bounds.width() + mAvatarWidth;
        } catch (Exception e) {
            return mMinTextWidth;
        }
    }

    /**
     * 根据弹幕宽度计算速度
     *
     * @param textMeasuredWidth
     * @return
     */
    private int calculateSpeed(float textMeasuredWidth) {
        return (int) (DeviceUtils.getScreenWidth(getContext()) + textMeasuredWidth) * 4;
    }

    public void restartBarrage() {
        isBarragePause = false;
        Iterator<Map.Entry<BarrageItemView, ObjectAnimator>> it = mBarrageAnimMap.entrySet().iterator();
        while(it.hasNext()){
            Map.Entry<BarrageItemView, ObjectAnimator> entry = it.next();
            ObjectAnimator value = entry.getValue();
            try {
                if (value.isPaused()) {
                    value.resume();
                }
            } catch (Exception e) {

            }
        }
    }

    public void pauseBarrage() {
        isBarragePause = true;
        Iterator<Map.Entry<BarrageItemView, ObjectAnimator>> it = mBarrageAnimMap.entrySet().iterator();
        while(it.hasNext()){
            Map.Entry<BarrageItemView, ObjectAnimator> entry = it.next();
            ObjectAnimator value = entry.getValue();
            try {
                if (value.isRunning()) {
                    value.pause();
                }
            } catch (Exception e) {

            }
        }
    }

    public void clearBarrage() {
        if (mBarrageModels != null) {
            mBarrageModels.clear();
        }

        mBarrageAnimMap.clear();;

        this.removeAllViews();
    }

    private static class BarrageHandler extends Handler {
        WeakReference<BarrageView> mViewReference;

        BarrageHandler(BarrageView barrageView) {
            mViewReference = new WeakReference<>(barrageView);
        }

        @Override
        public void handleMessage(Message msg) {
            final BarrageView barrageView = mViewReference.get();
            if (barrageView == null) {
                return;
            }

            switch (msg.what) {
                case MSG_ADD_BARRAGE:
                    barrageView.dequeueBarrage();
                    break;
            }
        }
    }

    public class BarrageItem {
        public BarrageItemView mItemView;
        public int moveSpeed;//移动速度
        public int verticalPos;//垂直方向显示的位置
        public int textMeasuredWidth;//字体显示占据的宽度
        public int barrageLine;//弹幕所在行
        public float moveValue;//当前移动到的位置
        public boolean isNoticeNext;//是否通知下一个
    }
}
