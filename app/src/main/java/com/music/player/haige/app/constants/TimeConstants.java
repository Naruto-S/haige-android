package com.music.player.haige.app.constants;

/**
 * Created by liumingkong on 15/5/27.
 */
public class TimeConstants {

    public static final long DAY_3 = 259200000L;
    public static final long HOUR_24 = 86400000L;
    public static final long HOUR_3 = 10800000L;
    public static final long HOUR_2 = 7200000L;
    public static final long HOUR_1 = 3600000L;
    public static final long HOUR_36 = 129600000L;
    public static final long MIN_30 = 1800000L;
    public static final long MIN_20 = 1200000L;
    public static final long MIN_15 = 900000L;
    public static final long MIN_10 = 600000L;
    public static final long MIN_5 = 300000L;
    public static final long MIN_4 = 240000L;
    public static final long MIN_1 = 60000L;
    public static final long SEC_45 = 45000L;
    public static final long SEC_40 = 40000L;
    public static final long SEC_30 = 30000L;
    public static final long SEC_25 = 25000L;
    public static final long SEC_15 = 15000L;
    public static final long SEC_10 = 10000L;
    public static final long SEC_5 = 5000L;
    public static final long MIN_2 = 120000L;
    public static final long SEC_2 = 2000L;
    public static final long SEC_1 = 1000L;
    public static final long HOUR_48 = 172800000L;
    public static final long SEC_3 = 3000L;
    public static final long SEC_4 = 4000L;

    public static final long SEC_ZERO = 0L;

    public static final long DAY7 = 7 * 24 * 60 * 60 * 1000;
}
