package com.music.player.haige.di.module;

import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.RankingModel;

import dagger.Module;
import dagger.Provides;

/**
 * ================================================
 * Created by huangcong on 2018/3/3.
 * ================================================
 */
@Module
public class RankingModule {
    private MVContract.RankingView view;

    public RankingModule(MVContract.RankingView view){
        this.view = view;
    }

    @FragmentScope
    @Provides
    MVContract.RankingView provideView(){
        return view;
    }

    @FragmentScope
    @Provides
    MVContract.RankingModel provideModel(RankingModel model){
        return model;
    }

}
