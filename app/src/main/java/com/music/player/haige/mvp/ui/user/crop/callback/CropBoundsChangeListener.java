package com.music.player.haige.mvp.ui.user.crop.callback;

/**
 * Interface for crop bound change notifying.
 */
public interface CropBoundsChangeListener {

    void onCropAspectRatioChanged(float cropRatio);

}