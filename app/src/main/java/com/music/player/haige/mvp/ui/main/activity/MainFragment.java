package com.music.player.haige.mvp.ui.main.activity;

import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.hc.base.base.BaseFragment;
import com.hc.base.widget.MineTabLayout;
import com.hc.core.di.component.AppComponent;
import com.music.player.haige.R;
import com.music.player.haige.app.EventBusTags;
import com.music.player.haige.app.base.AppBaseFragment;
import com.music.player.haige.app.base.pref.UserPrefHelper;
import com.music.player.haige.app.utils.statistics.CommonStatistics;
import com.music.player.haige.app.utils.statistics.StatisticsConstant;
import com.music.player.haige.di.component.DaggerCommonComponent;
import com.music.player.haige.di.module.CommonModule;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.presenter.MainFPresenter;
import com.music.player.haige.mvp.ui.login.LoginDialogFragment;
import com.music.player.haige.mvp.ui.main.fragment.FindFragment;
import com.music.player.haige.mvp.ui.music.service.MusicServiceConnection;
import com.music.player.haige.mvp.ui.user.MeProfileFragment;
import com.music.player.haige.mvp.ui.utils.Utils;
import com.music.player.haige.mvp.ui.widget.NoScrollViewPager;
import com.music.player.haige.mvp.ui.widget.adapter.TitlePagerAdapter;

import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * ================================================
 * Created by huangcong on 2018/2/27.
 * ================================================
 */

public class MainFragment extends AppBaseFragment<MainFPresenter> implements MVContract.CommonView {

    @BindView(R.id.app_fragment_main_tl)
    MineTabLayout mTabLayout;

    @BindView(R.id.app_fragment_main_vp)
    NoScrollViewPager mViewPager;

    @BindView(R.id.app_fragment_main_music)
    ImageView musicVisualizer;

    private boolean isFirstTimeCreate = true;

    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();
        return fragment;
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        Animation anim;
        if(!isFirstTimeCreate) {
            if (enter) {
                anim = AnimationUtils.loadAnimation(getActivity(),
                        R.anim.slide_in_from_left);
            } else {
                anim = AnimationUtils.loadAnimation(getActivity(),
                        R.anim.slide_out_to_left);
            }
            return anim;
        }
        isFirstTimeCreate = false;
        return null;
    }

    @Override
    public void setupFragmentComponent(AppComponent appComponent) {
        DaggerCommonComponent
                .builder()
                .appComponent(appComponent)
                .commonModule(new CommonModule(this))
                .build()
                .inject(this);
    }

    @Override
    public View initView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.app_fragment_main, container, false);
    }

    @Override
    public void initData(Bundle savedInstanceState) {
        String[] titles = getResources().getStringArray(R.array.app_main);
        BaseFragment[] fragments = new BaseFragment[titles.length];
        fragments[0] = FindFragment.newInstance();
        fragments[1] = MeProfileFragment.newInstance();
        TitlePagerAdapter adapter = new TitlePagerAdapter(getChildFragmentManager(), titles, fragments);
        mViewPager.setAdapter(adapter);
        mViewPager.setOffscreenPageLimit(adapter.getCount() - 1);
        mViewPager.setScroll(false);
        mTabLayout.setViewPager(mViewPager);
        mViewPager.setCurrentItem(0);
        mTabLayout.setTabClickListener(position -> {
            if (position == 0) {
                return true;
            } else if (position == 1) {
                CommonStatistics.sendEvent(StatisticsConstant.TABLE_MY_CLICK);
                if (UserPrefHelper.isLogin()) {
                    return true;
                } else {
                    LoginDialogFragment loginFragment = LoginDialogFragment.newInstance();
                    loginFragment.setLoginListener(() -> {
                        if (UserPrefHelper.isLogin()) {
                            if (Utils.isNotNull(mViewPager)) {
                                mViewPager.setCurrentItem(1);
                            }
                        } else {
                            if (Utils.isNotNull(mViewPager)) {
                                mViewPager.setCurrentItem(0);
                            }
                        }
                    });
                    loginFragment.show(getChildFragmentManager(), LoginDialogFragment.class.getName());
                    return false;
                }
            } else {
                return true;
            }
        });
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void setData(Object data) {

    }

    @OnClick({R.id.app_fragment_main_music})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.app_fragment_main_music:
                CommonStatistics.sendEvent(StatisticsConstant.TABLE_PLAY_CLICK);
                if (MusicServiceConnection.isPlaying()) {
                    EventBus.getDefault().post(EventBusTags.MAIN.MUSIC_PLAY_LIST);
                }
                break;
        }
    }

    @Subscriber
    public void onSubscriber(EventBusTags.MetaChangedEvent event) {
        if (MusicServiceConnection.isPlaying()) {
            AnimationDrawable animation = (AnimationDrawable)
                    ContextCompat.getDrawable(getContext(), R.drawable.ic_equalizer_white_36dp);
            animation.start();
            musicVisualizer.setImageDrawable(animation);
        } else {
            Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.ic_equalizer1_white_36dp);
            musicVisualizer.setImageDrawable(drawable);
        }
    }

    @Subscriber
    public void onSubscriberUI(EventBusTags.UI tags) {
        switch (tags) {
            case GO_TO_RECOMMEND:
                if (Utils.isNotNull(mViewPager)) {
                    mViewPager.setCurrentItem(0);
                }
                break;
            case LOGIN_LOGOUT:
                if (Utils.isNotNull(mViewPager)) {
                    mViewPager.setCurrentItem(0);
                }
                break;
        }
    }
}
