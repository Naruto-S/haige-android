package com.music.player.haige.mvp.presenter;

import com.liulishuo.filedownloader.BaseDownloadTask;
import com.liulishuo.filedownloader.FileDownloadList;
import com.liulishuo.filedownloader.FileDownloadListener;
import com.liulishuo.filedownloader.FileDownloadSampleListener;
import com.liulishuo.filedownloader.FileDownloader;
import com.liulishuo.filedownloader.model.FileDownloadStatus;
import com.liulishuo.filedownloader.util.FileDownloadUtils;
import com.music.player.haige.R;
import com.music.player.haige.app.EventBusTags;
import com.music.player.haige.app.GlobalConfiguration;
import com.music.player.haige.mvp.contract.AppBasePresenter;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.Api;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.model.entity.music.SongDao;
import com.music.player.haige.mvp.ui.music.utils.MusicUtils;
import com.music.player.haige.mvp.ui.setting.adapter.DownloadingListAdapter;
import com.music.player.haige.mvp.ui.utils.Utils;

import org.greenrobot.greendao.query.QueryBuilder;
import org.simple.eventbus.EventBus;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import me.jessyan.rxerrorhandler.handler.ErrorHandleSubscriber;
import timber.log.Timber;

/**
 * ================================================
 * Created by huangcong on 2018/3/20.
 * ================================================
 */

public class DownloadingFPresenter extends AppBasePresenter<MVContract.DownloadingModel, MVContract.DownloadingView> {

    private SongDao mDownloadSongDao;              // 音乐下载记录数据库
    private QueryBuilder<Song> mDownloadSongsQuery;

    @Inject
    public DownloadingFPresenter(MVContract.DownloadingModel model, MVContract.DownloadingView view) {
        super(model, view);
        mDownloadSongDao = GlobalConfiguration.sDownloadDaoSession.getSongDao();
        mDownloadSongsQuery = GlobalConfiguration.sDownloadDaoSession.getSongDao().queryBuilder().orderDesc(SongDao.Properties.DownloadDate);
        if (!FileDownloader.getImpl().isServiceConnected()) {
            // 开启下载服务
            FileDownloader.getImpl().bindService();
        }
    }

    /**
     * 重新设置下载监听
     */
    public void loadDownloadingMusic() {
        buildObservable(Observable.just(mDownloadSongsQuery.list()))
                .subscribe(new ErrorHandleSubscriber<List<Song>>(mErrorHandler) {
                    @Override
                    public void onNext(List<Song> songs) {
                        ArrayList<Song> downloadingSongs = new ArrayList<>();
                        for (Song song : songs) {
                            String url = song.musicPath;
                            String path = MusicUtils.getDownloadPath(song);
                            int id = FileDownloadUtils.generateId(url, path);
                            int status = FileDownloader.getImpl().getStatus(id, path);
                            if (status != FileDownloadStatus.completed
                                    && !new File(path).exists()) {
                                downloadingSongs.add(song);
                            }
                        }

                        if (Utils.isNotNull(mRootView)) {
                            if (downloadingSongs.size() > 0) {
                                mRootView.onSuccess(Api.Action.DOWNLOAD_ING, downloadingSongs);
                            } else {
                                mRootView.onError(Api.Action.DOWNLOAD_ING, Api.State.NORMAL.getCode(), Api.State.NORMAL.getMsg());
                            }
                        }
                    }
                });
    }

    public void replaceDownloadListener(DownloadingListAdapter.ItemHolder holder, Song song) {
        String path = MusicUtils.getDownloadPath(song);
        int id = FileDownloadUtils.generateId(song.musicPath, path);
        holder.downloadId = id;
        holder.song = song;

        holder.mDownloadBtn.setEnabled(true);

        if (FileDownloader.getImpl().isServiceConnected()) {
            final int status = FileDownloader.getImpl().getStatus(id, path);
            if (status == FileDownloadStatus.pending || status == FileDownloadStatus.started ||
                    status == FileDownloadStatus.connected) {
                // start task, but file not created yet
                holder.updateDownloading(status, FileDownloader.getImpl().getSoFar(id), FileDownloader.getImpl().getTotal(id));
            } else if (!new File(path).exists() &&
                    !new File(FileDownloadUtils.getTempPath(path)).exists()) {
                // not exist file
                holder.updateNotDownloaded(status, 0, 0);
            } else if (status == FileDownloadStatus.completed) {
                // already downloaded and exist
                holder.updateDownloaded();
            } else if (status == FileDownloadStatus.progress) {
                // downloading
                holder.updateDownloading(status, FileDownloader.getImpl().getSoFar(id), FileDownloader.getImpl().getTotal(id));
            } else {
                // not start
                holder.updateNotDownloaded(status, FileDownloader.getImpl().getSoFar(id), FileDownloader.getImpl().getTotal(id));
            }
        } else {
            //holder.taskStatusTv.setText(R.string.tasks_manager_demo_status_loading);
            holder.mDownloadBtn.setEnabled(false);
        }
        BaseDownloadTask.IRunningTask task = FileDownloadList.getImpl().get(id);
        if (task != null) {
            BaseDownloadTask downloadTask = task.getOrigin();
            downloadTask.setTag(holder);
            FileDownloader.getImpl().replaceListener(id, mTaskDownloadListener);
        }
    }

    public void onDownloadClick(DownloadingListAdapter.ItemHolder holder, Song song) {
        String path = MusicUtils.getDownloadPath(song);
        int id = FileDownloadUtils.generateId(song.musicPath, path);
        CharSequence action = holder.mDownloadBtn.getText();
        if (action.equals(holder.mDownloadBtn.getResources().getString(R.string.app_pause))) {
            // to pause
            FileDownloader.getImpl().pause(id);
        } else if (action.equals(holder.mDownloadBtn.getResources().getString(R.string.app_start))) {
            FileDownloader.getImpl().create(song.musicPath)
                    .setPath(path)
                    .setCallbackProgressTimes(100)
                    .setListener(mTaskDownloadListener)
                    .setTag(holder)
                    .start();
        }
    }

    private FileDownloadListener mTaskDownloadListener = new FileDownloadSampleListener() {

        private DownloadingListAdapter.ItemHolder checkCurrentHolder(final BaseDownloadTask task) {
            final DownloadingListAdapter.ItemHolder tag = (DownloadingListAdapter.ItemHolder) task.getTag();
            if (tag.downloadId != task.getId()) {
                return null;
            }

            return tag;
        }

        @Override
        protected void pending(BaseDownloadTask task, int soFarBytes, int totalBytes) {
            super.pending(task, soFarBytes, totalBytes);
            final DownloadingListAdapter.ItemHolder tag = checkCurrentHolder(task);
            if (tag == null) {
                return;
            }

            tag.updateDownloading(FileDownloadStatus.pending, soFarBytes, totalBytes);
        }

        @Override
        protected void started(BaseDownloadTask task) {
            super.started(task);
            final DownloadingListAdapter.ItemHolder tag = checkCurrentHolder(task);
            if (tag == null) {
                return;
            }

            //tag.taskStatusTv.setText(R.string.tasks_manager_demo_status_started);
        }

        @Override
        protected void connected(BaseDownloadTask task, String etag, boolean isContinue, int soFarBytes, int totalBytes) {
            super.connected(task, etag, isContinue, soFarBytes, totalBytes);
            final DownloadingListAdapter.ItemHolder tag = checkCurrentHolder(task);
            if (tag == null) {
                return;
            }

            tag.updateDownloading(FileDownloadStatus.connected, soFarBytes
                    , totalBytes);
            //tag.taskStatusTv.setText(R.string.tasks_manager_demo_status_connected);
        }

        @Override
        protected void progress(BaseDownloadTask task, int soFarBytes, int totalBytes) {
            super.progress(task, soFarBytes, totalBytes);
            final DownloadingListAdapter.ItemHolder tag = checkCurrentHolder(task);
            if (tag == null) {
                return;
            }

            tag.updateDownloading(FileDownloadStatus.progress, soFarBytes, totalBytes);
        }

        @Override
        protected void error(BaseDownloadTask task, Throwable e) {
            super.error(task, e);
            final DownloadingListAdapter.ItemHolder tag = checkCurrentHolder(task);
            if (tag == null) {
                return;
            }

            tag.updateNotDownloaded(FileDownloadStatus.error, task.getLargeFileSoFarBytes(), task.getLargeFileTotalBytes());
        }

        @Override
        protected void paused(BaseDownloadTask task, int soFarBytes, int totalBytes) {
            super.paused(task, soFarBytes, totalBytes);
            final DownloadingListAdapter.ItemHolder tag = checkCurrentHolder(task);
            if (tag == null) {
                return;
            }

            tag.updateNotDownloaded(FileDownloadStatus.paused, soFarBytes, totalBytes);
        }

        @Override
        protected void completed(BaseDownloadTask task) {
            super.completed(task);
            final DownloadingListAdapter.ItemHolder tag = checkCurrentHolder(task);
            if (tag == null) {
                return;
            }
            tag.updateDownloaded();
            loadDownloadingMusic();
            int id = FileDownloadUtils.generateId(task.getUrl(), task.getPath());
            int status = FileDownloader.getImpl().getStatus(id, task.getPath());
            Timber.e("===>>> completed id : " + id + ", status : " + status);
            EventBus.getDefault().post(EventBusTags.DOWNLOAD.FINISH);
        }
    };

}
