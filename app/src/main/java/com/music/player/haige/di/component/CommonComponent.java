package com.music.player.haige.di.component;

import com.hc.core.di.component.AppComponent;
import com.hc.core.di.scope.ActivityScope;
import com.music.player.haige.di.module.CommonModule;
import com.music.player.haige.mvp.ui.launch.LauncherActivity;
import com.music.player.haige.mvp.ui.main.activity.MainActivity;
import com.music.player.haige.mvp.ui.main.fragment.FindFragment;
import com.music.player.haige.mvp.ui.main.activity.MainFragment;
import com.music.player.haige.mvp.ui.main.fragment.PlayMainFragment;
import com.music.player.haige.mvp.ui.music.fragment.MusicFragment;
import com.music.player.haige.mvp.ui.setting.fragment.AboutFragment;
import com.music.player.haige.mvp.ui.setting.fragment.CopyrightFragment;
import com.music.player.haige.mvp.ui.setting.fragment.PolicyFragment;
import com.music.player.haige.mvp.ui.setting.fragment.SettingFragment;
import com.music.player.haige.mvp.ui.share.DownloadShareDialog;
import com.music.player.haige.mvp.ui.share.MusicShareDialog;
import com.music.player.haige.mvp.ui.user.EditFragment;

import dagger.Component;

/**
 * ================================================
 * Created by huangcong on 2018/2/23.
 * ================================================
 */

@ActivityScope
@Component(modules = {CommonModule.class}, dependencies = AppComponent.class)
public interface CommonComponent {

    void inject(LauncherActivity activity);

    void inject(MainActivity activity);

    void inject(MainFragment fragment);

    void inject(FindFragment fragment);

    void inject(MusicFragment fragment);

    void inject(SettingFragment fragment);

    void inject(PolicyFragment fragment);

    void inject(CopyrightFragment fragment);

    void inject(AboutFragment fragment);

    void inject(EditFragment fragment);

    void inject(DownloadShareDialog fragment);

    void inject(MusicShareDialog fragment);

    void inject(PlayMainFragment fragment);
}
