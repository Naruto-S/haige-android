package com.music.player.haige.mvp.ui.utils;

import android.animation.Keyframe;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.view.View;

/**
 * 属性动画工具类
 */
public class AnimatorUtils {
    public static final int TYPE_VIEW_ANIM_MOVE_UP = 0; //向上移动显隐
    public static final int TYPE_VIEW_ANIM_MOVE_DOWN = 1; //向下移动显隐
    public static final int TYPE_VIEW_ANIM_ALPHA = 2; //透明度渐变显隐

    /**
     * 上下抖动。就是对view的x轴和y轴进行0.9倍到1.1倍的缩放，同时对view进行一定角度的上下旋转
     *
     * @param view
     * @return
     */
    public static ObjectAnimator sway(View view) {
        return sway(view, 1f);
    }

    public static ObjectAnimator sway(View view, float shakeFactor) {

        PropertyValuesHolder pvhScaleX = PropertyValuesHolder.ofKeyframe(View.SCALE_X,
                Keyframe.ofFloat(0f, 1f),
                Keyframe.ofFloat(.1f, .9f),
                Keyframe.ofFloat(.2f, .9f),
                Keyframe.ofFloat(.3f, 1.1f),
                Keyframe.ofFloat(.4f, 1.1f),
                Keyframe.ofFloat(.5f, 1.1f),
                Keyframe.ofFloat(.6f, 1.1f),
                Keyframe.ofFloat(.7f, 1.1f),
                Keyframe.ofFloat(.8f, 1.1f),
                Keyframe.ofFloat(.9f, 1.1f),
                Keyframe.ofFloat(1f, 1f)
        );

        PropertyValuesHolder pvhScaleY = PropertyValuesHolder.ofKeyframe(View.SCALE_Y,
                Keyframe.ofFloat(0f, 1f),
                Keyframe.ofFloat(.1f, .9f),
                Keyframe.ofFloat(.2f, .9f),
                Keyframe.ofFloat(.3f, 1.1f),
                Keyframe.ofFloat(.4f, 1.1f),
                Keyframe.ofFloat(.5f, 1.1f),
                Keyframe.ofFloat(.6f, 1.1f),
                Keyframe.ofFloat(.7f, 1.1f),
                Keyframe.ofFloat(.8f, 1.1f),
                Keyframe.ofFloat(.9f, 1.1f),
                Keyframe.ofFloat(1f, 1f)
        );

        PropertyValuesHolder pvhRotate = PropertyValuesHolder.ofKeyframe(View.ROTATION,
                Keyframe.ofFloat(0f, 0f),
                Keyframe.ofFloat(.1f, -3f * shakeFactor),
                Keyframe.ofFloat(.2f, -3f * shakeFactor),
                Keyframe.ofFloat(.3f, -3f * shakeFactor),
                Keyframe.ofFloat(.4f, 3f * shakeFactor),
                Keyframe.ofFloat(.5f, -3f * shakeFactor),
                Keyframe.ofFloat(.6f, 3f * shakeFactor),
                Keyframe.ofFloat(.7f, -3f * shakeFactor),
                Keyframe.ofFloat(.8f, 3f * shakeFactor),
                Keyframe.ofFloat(.9f, -3f * shakeFactor),
                Keyframe.ofFloat(1f, 0)
        );

        return ObjectAnimator.ofPropertyValuesHolder(view, pvhScaleX, pvhScaleY, pvhRotate).
                setDuration(1000);
    }

    /**
     * 左右抖动。左右抖动的原理就是对view进行x轴的平移。
     *
     * @param view
     * @return
     */
    public static ObjectAnimator shake(View view) {
        int delta = 8;
        PropertyValuesHolder pvhTranslateX = PropertyValuesHolder.ofKeyframe(View.TRANSLATION_X,
                Keyframe.ofFloat(0f, 0),
                Keyframe.ofFloat(.10f, -delta),
                Keyframe.ofFloat(.26f, delta),
                Keyframe.ofFloat(.42f, -delta),
                Keyframe.ofFloat(.58f, delta),
                Keyframe.ofFloat(.74f, -delta),
                Keyframe.ofFloat(.90f, delta),
                Keyframe.ofFloat(1f, 0f)
        );

        return ObjectAnimator.ofPropertyValuesHolder(view, pvhTranslateX).
                setDuration(450);
    }

    /**
     * 左右抖动(旋转)。左右抖动的原理就是对view进行围绕中心点进行移动角度的晃动。
     *
     * @param view
     * @return
     */
    public static ObjectAnimator rock(View view, int duration) {
        int delta = 30;
        PropertyValuesHolder pvhTranslateX = PropertyValuesHolder.ofKeyframe(View.ROTATION,
                Keyframe.ofFloat(0f, 0),
                Keyframe.ofFloat(.10f, -delta),
                Keyframe.ofFloat(.26f, delta),
                Keyframe.ofFloat(.42f, -delta),
                Keyframe.ofFloat(.58f, delta),
                Keyframe.ofFloat(.74f, -delta),
                Keyframe.ofFloat(.90f, delta),
                Keyframe.ofFloat(1f, 0f)
        );

        return ObjectAnimator.ofPropertyValuesHolder(view, pvhTranslateX).
                setDuration(duration);
    }
}