package com.music.player.haige.app.base;

import io.reactivex.functions.Consumer;

/**
 * Created by john on 2018/4/26.
 */

public class BaseConsumerNext<T> implements Consumer<T> {

    @Override
    public void accept(T t) throws Exception {

    }

}
