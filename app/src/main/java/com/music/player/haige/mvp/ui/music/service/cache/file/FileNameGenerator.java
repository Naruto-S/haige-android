package com.music.player.haige.mvp.ui.music.service.cache.file;

/**
 * Generator for files to be used for caching.
 *
 * @author Alexey Danilov (danikula@gmail.com).
 */
public interface FileNameGenerator {

    void putIsShort(boolean aShort);

    String generate(String url);

}
