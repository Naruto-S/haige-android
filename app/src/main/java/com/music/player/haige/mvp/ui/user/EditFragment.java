package com.music.player.haige.mvp.ui.user;

import android.animation.ObjectAnimator;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.Selection;
import android.text.Spannable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.hc.core.di.component.AppComponent;
import com.music.player.haige.R;
import com.music.player.haige.app.EventBusTags;
import com.music.player.haige.app.base.AppBaseFragment;
import com.music.player.haige.app.base.pref.UserPrefHelper;
import com.music.player.haige.app.constants.IntentConstants;
import com.music.player.haige.app.event.UpdateEditInfoEvent;
import com.music.player.haige.di.component.DaggerCommonComponent;
import com.music.player.haige.di.module.CommonModule;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.entity.user.UserBean;
import com.music.player.haige.mvp.presenter.EditPresenter;
import com.music.player.haige.mvp.ui.utils.AnimatorUtils;
import com.music.player.haige.mvp.ui.utils.KeyBoardUtils;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;
import com.music.player.haige.mvp.ui.utils.Utils;

import org.simple.eventbus.EventBus;

import butterknife.BindView;
import butterknife.OnClick;

public class EditFragment extends AppBaseFragment<EditPresenter> implements MVContract.CommonView {

    public static final int TYPE_EDIT_NAME = 0;
    public static final int TYPE_EDIT_DESC = 1;

    private static final int EDIT_INFO_TYPE_NAME_TEXT_LIMIT = 30;
    private static final int EDIT_INFO_TYPE_NAME_DES_LIMIT = 80;

    @BindView(R.id.edit_info_nickname)
    EditText mUpdateInfoEdt;

    @BindView(R.id.tv_edit_info_num)
    TextView mTextNumLimit;

    @BindView(R.id.app_fragment_title_tv)
    TextView mTitleTv;

    @BindView(R.id.app_fragment_save_tv)
    TextView mSaveTv;

    private int mType = 0;
    private int mMaxLen;
    private UserBean mUserBean;

    public static EditFragment newInstance(int type) {
        EditFragment fragment = new EditFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(IntentConstants.EXTRA_EDIT_INFO_TYPE, type);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void setupFragmentComponent(AppComponent component) {
        DaggerCommonComponent.builder()
                .appComponent(component)
                .commonModule(new CommonModule(this))
                .build()
                .inject(this);
    }

    @Override
    public View initView(LayoutInflater inflater, ViewGroup group, Bundle bundle) {
        return inflater.inflate(R.layout.app_fragment_edit, group, false);
    }

    @Override
    public void initData(Bundle bundle) {
        mUserBean = UserPrefHelper.getMeUser();

        if (Utils.isNotNull(getArguments())) {
            mType = getArguments().getInt(IntentConstants.EXTRA_EDIT_INFO_TYPE, 0);
        }

        switch (mType) {
            case TYPE_EDIT_NAME:
                mTitleTv.setText(ResourceUtils.resourceString(R.string.user_edit_name));
                mMaxLen = EDIT_INFO_TYPE_NAME_TEXT_LIMIT;

                mUpdateInfoEdt.setInputType(InputType.TYPE_CLASS_TEXT
                        | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
                String nickName = Utils.isNull(mUserBean) ? "" : mUserBean.getNickName();
                int nameSize = nickName.length();
                String limitName;
                if (nameSize > EDIT_INFO_TYPE_NAME_TEXT_LIMIT) {
                    limitName = nickName.substring(0, EDIT_INFO_TYPE_NAME_TEXT_LIMIT);
                    mUpdateInfoEdt.setText(limitName);
                } else {
                    mUpdateInfoEdt.setText(nickName);
                }
                mUpdateInfoEdt.setGravity(Gravity.CENTER_VERTICAL);

                if (mUpdateInfoEdt.getText().length() == EDIT_INFO_TYPE_NAME_TEXT_LIMIT) {
                    mTextNumLimit.setTextColor(getResources().getColor(R.color.colorAccent));
                }
                int limitNum = mUpdateInfoEdt.getText().length();
                String limitContent = getString(R.string.user_edit_profile_content_limit, limitNum, mMaxLen);
                mTextNumLimit.setText(limitContent);
                break;
            case TYPE_EDIT_DESC:
                mTitleTv.setText(ResourceUtils.resourceString(R.string.user_edit_desc));
                mMaxLen = EDIT_INFO_TYPE_NAME_DES_LIMIT;

                mUpdateInfoEdt.setInputType(InputType.TYPE_CLASS_TEXT
                        | InputType.TYPE_TEXT_FLAG_MULTI_LINE
                        | InputType.TYPE_TEXT_FLAG_AUTO_COMPLETE
                        | InputType.TYPE_TEXT_FLAG_AUTO_CORRECT
                        | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
                String des = Utils.isNull(mUserBean) ? "" : mUserBean.getDesc();
                String limitDes;
                if (des != null) {
                    int desSize = des.length();
                    if (desSize > EDIT_INFO_TYPE_NAME_DES_LIMIT) {
                        limitDes = des.substring(0, EDIT_INFO_TYPE_NAME_DES_LIMIT);
                        mUpdateInfoEdt.setText(limitDes);
                    } else {
                        mUpdateInfoEdt.setText(des);
                    }
                }
                mUpdateInfoEdt.setMinLines(1);
                mTextNumLimit.setText(String.valueOf(EDIT_INFO_TYPE_NAME_DES_LIMIT));
                if (mUpdateInfoEdt.getText().length() == EDIT_INFO_TYPE_NAME_DES_LIMIT) {
                    mTextNumLimit.setTextColor(getResources().getColor(R.color.colorAccent));
                }
                int limitNumDes = mUpdateInfoEdt.getText().length();
                String limitContentDes = getString(R.string.user_edit_profile_content_limit, limitNumDes, mMaxLen);
                mTextNumLimit.setText(limitContentDes);
                break;
        }

        CharSequence text = mUpdateInfoEdt.getText();
        if (text instanceof Spannable) {
            Spannable spanText = (Spannable) text;
            Selection.setSelection(spanText, text.length());
        }
        mUpdateInfoEdt.addTextChangedListener(editTextWatcher);

        //软键盘完成键捕捉
        mUpdateInfoEdt.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_UNSPECIFIED) {
                KeyBoardUtils.closeKeybord(mUpdateInfoEdt, getActivity());
                return true;
            }
            return false;
        });
    }

    @OnClick({R.id.app_fragment_back_iv, R.id.app_fragment_save_tv})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.app_fragment_back_iv:
                EventBus.getDefault().post(EventBusTags.MAIN.BACK);
                break;
            case R.id.app_fragment_save_tv:
                UpdateEditInfoEvent event = new UpdateEditInfoEvent();
                event.setType(mType);
                event.setCotent(mUpdateInfoEdt.getText().toString());
                EventBus.getDefault().post(event);
                EventBus.getDefault().post(EventBusTags.MAIN.BACK);
                break;
        }
    }

    private final TextWatcher editTextWatcher = new TextWatcher() {

        private int selectionStart;
        private int selectionEnd;

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        public void afterTextChanged(Editable s) {
            if (Utils.isNull(mUpdateInfoEdt)) return;

            selectionStart = mUpdateInfoEdt.getSelectionStart();
            selectionEnd = mUpdateInfoEdt.getSelectionEnd();

            int length = s.toString().length();
            if (length > mMaxLen) {
                mUpdateInfoEdt.removeTextChangedListener(editTextWatcher);
            }
            while (s.toString().length() > mMaxLen) {
                s.delete(selectionStart - 1, selectionEnd);
                selectionStart--;
                selectionEnd--;
            }
            if (length > mMaxLen) {
                mUpdateInfoEdt.setSelection(selectionEnd);
                mUpdateInfoEdt.addTextChangedListener(editTextWatcher);
            }
            int leftNum = s.toString().length();
            String limitContent = String.format(getString(R.string.user_edit_profile_content_limit), leftNum, mMaxLen);
            mTextNumLimit.setText(limitContent);
            if (leftNum == 0) {
                mTextNumLimit.setTextColor(getResources().getColor(R.color.colorAccent));
                ObjectAnimator nopeAnimator = AnimatorUtils.shake(mTextNumLimit);
                nopeAnimator.start();
                if (mSaveTv != null) {
                    mSaveTv.setEnabled(false);
                }
            } else if (leftNum == mMaxLen) {
                Toast.makeText(getContext(), getString(R.string.user_edit_info_max_text_warning), Toast.LENGTH_SHORT).show();
                mTextNumLimit.setTextColor(getResources().getColor(R.color.colorAccent));
                ObjectAnimator nopeAnimator = AnimatorUtils.shake(mTextNumLimit);
                nopeAnimator.start();
                if (mSaveTv != null) {
                    mSaveTv.setEnabled(true);
                }
            } else {
                mTextNumLimit.setTextColor(Color.WHITE);
                if (mSaveTv != null) {
                    mSaveTv.setEnabled(true);
                }
            }
        }
    };
}
