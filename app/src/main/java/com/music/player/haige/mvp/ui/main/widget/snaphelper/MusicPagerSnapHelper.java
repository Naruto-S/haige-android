package com.music.player.haige.mvp.ui.main.widget.snaphelper;

import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.music.player.haige.mvp.ui.utils.Utils;

/**
 * Created by Naruto on 2017/8/26.
 */

public class MusicPagerSnapHelper extends android.support.v7.widget.PagerSnapHelper {

    private onPageChangeListener mOnPageChangeListener;

    @Nullable
    @Override
    public View findSnapView(RecyclerView.LayoutManager layoutManager) {

        if (layoutManager instanceof LinearLayoutManager) {
            int firstChild = ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition();

            //回调监听
            if (Utils.isNotNull(mOnPageChangeListener) && firstChild >= 0) {
                mOnPageChangeListener.onPageChange(firstChild);
            }
        }

        return super.findSnapView(layoutManager);
    }

    public void setOnPageChangeListener(onPageChangeListener listener) {
        mOnPageChangeListener = listener;
    }

    public interface onPageChangeListener {
        void onPageChange(int position);
    }
}
