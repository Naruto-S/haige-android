package com.music.player.haige.di.module;

import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.LikeMusicModel;

import dagger.Module;
import dagger.Provides;

/**
 * ================================================
 * Created by huangcong on 2018/2/27.
 * ================================================
 */
@Module
public class LikeMusicModule {
    private MVContract.LikeMusicView view;

    public LikeMusicModule(MVContract.LikeMusicView view) {
        this.view = view;
    }

    @FragmentScope
    @Provides
    MVContract.LikeMusicView provideView() {
        return this.view;
    }

    @FragmentScope
    @Provides
    MVContract.LikeMusicModel provideModel(LikeMusicModel model) {
        return model;
    }

}
