package com.music.player.haige.mvp.ui.music.service.cache.file;

import com.music.player.haige.mvp.ui.utils.Utils;

/**
 * Created by barry on 2017/4/27.
 */

public class FidFileNameGenerator implements FileNameGenerator {

    @Override
    public void putIsShort(boolean aShort) {

    }

    @Override
    public String generate(String url) {
        String name = "";
        if(!Utils.isEmptyString(url)){
            try{
                String[] strs = url.split("/");
                if(!Utils.isEmptyArray(strs)){
                    name = strs[strs.length -1];
                }
            }catch(Throwable e){

            }
        }else{
            name = "tmp.acc";
        }
        return name;
    }
}
