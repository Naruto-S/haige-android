package com.music.player.haige.mvp.ui.music.fragment;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.hc.core.di.component.AppComponent;
import com.music.player.haige.R;
import com.music.player.haige.app.Constants;
import com.music.player.haige.app.EventBusTags;
import com.music.player.haige.app.base.AppBaseLazyFragment;
import com.music.player.haige.di.component.DaggerArtistComponent;
import com.music.player.haige.di.module.ArtistModule;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.entity.music.Artist;
import com.music.player.haige.mvp.presenter.ArtistPresenter;
import com.music.player.haige.mvp.ui.music.adapter.ArtistAdapter;
import com.music.player.haige.mvp.ui.widget.SpacesItemDecoration;
import com.music.player.haige.mvp.ui.widget.StatusLayout;

import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;

import java.util.List;

import butterknife.BindView;

/**
 * ================================================
 * Created by huangcong on 2018/3/1.
 * ================================================
 */

public class ArtistFragment extends AppBaseLazyFragment<ArtistPresenter> implements MVContract.ArtistView {

    @BindView(R.id.app_layout_sl)
    StatusLayout mStatusLayout;

    @BindView(R.id.app_layout_rv)
    RecyclerView mArtistRecyclerView;

    @BindView(R.id.app_layout_progress)
    ProgressBar mProgressBar;

    private String mAction;
    private ArtistAdapter mArtistAdapter;

    public static ArtistFragment newInstance(String action) {
        Bundle args = new Bundle();
        switch (action) {
            case Constants.NAVIGATE_ALLSONG:
                args.putString(Constants.PLAYLIST_TYPE, action);
                break;
            case Constants.NAVIGATE_PLAYLIST_RECENTADD:
                args.putString(Constants.PLAYLIST_TYPE, action);
                break;
            case Constants.NAVIGATE_PLAYLIST_RECENTPLAY:
                args.putString(Constants.PLAYLIST_TYPE, action);
                break;
            case Constants.NAVIGATE_PLAYLIST_FAVORITE:
                args.putString(Constants.PLAYLIST_TYPE, action);
                break;
            default:
                throw new RuntimeException("wrong action type");
        }
        ArtistFragment fragment = new ArtistFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void setupFragmentComponent(AppComponent appComponent) {
        DaggerArtistComponent
                .builder()
                .appComponent(appComponent)
                .artistModule(new ArtistModule(this))
                .build()
                .inject(this);
    }

    @Override
    public View initView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.app_layout_recyclerview, container, false);
    }

    @Override
    public void lazyInitData(Bundle savedInstanceState) {
        mAction = getArguments().getString(Constants.PLAYLIST_TYPE);

        mStatusLayout.setOnStatusClickListener(new StatusLayout.OnStatusClickListener() {
            @Override
            public void onEmptyClick() {
                EventBus.getDefault().post(EventBusTags.UI.GO_TO_RECOMMEND);
                EventBus.getDefault().post(EventBusTags.MAIN.BACK);
            }

            @Override
            public void onReloadClick() {

            }
        });

        mArtistAdapter = new ArtistAdapter(getActivity(), mAction);
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
        int spacingInPixels = getActivity().getResources().getDimensionPixelSize(R.dimen.dp_4);
        SpacesItemDecoration itemDecoration = new SpacesItemDecoration(spacingInPixels);
        mArtistRecyclerView.setLayoutManager(layoutManager);
        mArtistRecyclerView.setHasFixedSize(true);
        mArtistRecyclerView.addItemDecoration(itemDecoration);
        mArtistRecyclerView.setAdapter(mArtistAdapter);

        mPresenter.loadArtists(mAction);
    }

    @Override
    public void showLoading() {
        super.showLoading();
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void showArtists(List<Artist> artists) {
        mStatusLayout.showStatusView(StatusLayout.STATUS.OK);
        mArtistRecyclerView.setVisibility(View.VISIBLE);
        mArtistAdapter.setArtistList(artists);
    }

    @Override
    public void showEmptyView() {
        mStatusLayout.showStatusView(StatusLayout.STATUS.EMPTY);
        mArtistRecyclerView.setVisibility(View.INVISIBLE);
    }

    @Subscriber
    public void onSubscriber(EventBusTags.FavoriteSongEvent event) {
        if (!Constants.NAVIGATE_PLAYLIST_FAVORITE.equals(mAction)) {
            return;
        }
        mPresenter.loadArtists(mAction);
    }

    @Subscriber
    public void onSubscriber(EventBusTags.RecentlyPlayEvent event) {
        if (!Constants.NAVIGATE_PLAYLIST_RECENTPLAY.equals(mAction)) {
            return;
        }
        mPresenter.loadArtists(mAction);
    }

    @Subscriber
    public void onSubscriber(EventBusTags.MediaUpdateEvent event) {
        if (Constants.NAVIGATE_PLAYLIST_RECENTPLAY.equals(mAction)
                || Constants.NAVIGATE_PLAYLIST_RECENTPLAY.equals(mAction)) {
            return;
        }
        mPresenter.loadArtists(mAction);
    }

}
