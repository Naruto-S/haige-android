package com.music.player.haige.di.module;

import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.mvp.model.CropModel;
import com.music.player.haige.mvp.contract.MVContract;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Naruto on 2018/5/13.
 */
@Module
public class CropModule {
    private MVContract.CropView view;

    public CropModule(MVContract.CropView view){
        this.view = view;
    }

    @FragmentScope
    @Provides
    MVContract.CropView provideView(){
        return view;
    }

    @FragmentScope
    @Provides
    MVContract.CropModel provideModel(CropModel model){
        return model;
    }
}
