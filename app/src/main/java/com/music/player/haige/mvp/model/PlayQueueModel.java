package com.music.player.haige.mvp.model;

import com.hc.base.mvp.BaseModel;
import com.hc.core.integration.IRepositoryManager;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.ui.music.service.MusicServiceConnection;
import com.music.player.haige.mvp.ui.utils.Utils;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * ================================================
 * Created by huangcong on 2018/3/3.
 * ================================================
 */

public class PlayQueueModel extends BaseModel implements MVContract.PlayQueueModel {

    @Inject
    public PlayQueueModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }

    @Override
    public Observable<ArrayList<Song>> loadCurrentMusic() {
        if (Utils.isNotNull(MusicServiceConnection.getPlayinfos())) {
            return Observable.just(MusicServiceConnection.getPlayinfos())
                    .flatMap(longSongHashMap -> {
                        long[] songIds = MusicServiceConnection.getQueue();
                        ArrayList<Song> songs = new ArrayList<>();
                        for (long songId : songIds) {
                            songs.add(longSongHashMap.get(songId));
                        }
                        return Observable.just(songs);
                    });
        } else {
            return null;
        }
    }
}
