package com.music.player.haige.di.component;

import com.hc.core.di.component.AppComponent;
import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.di.module.DownloadFinishModule;
import com.music.player.haige.di.module.DownloadModule;
import com.music.player.haige.mvp.ui.setting.fragment.DownloadFinishFragment;
import com.music.player.haige.mvp.ui.setting.fragment.DownloadFragment;

import dagger.Component;

/**
 * ================================================
 * Created by huangcong on 2018/3/20.
 * ================================================
 */
@FragmentScope
@Component(modules = {DownloadFinishModule.class}, dependencies = AppComponent.class)
public interface DownloadFinishComponent {

    void inject(DownloadFinishFragment fragment);
}
