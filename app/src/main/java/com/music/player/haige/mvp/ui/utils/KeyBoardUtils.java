package com.music.player.haige.mvp.ui.utils;

import android.app.Activity;
import android.content.Context;
import android.util.TypedValue;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.hc.core.utils.DeviceUtils;

/**
 * 打开或关闭软键盘
 */
public class KeyBoardUtils {

    public static final int DEFAULT_SOFT_KEYBOARD_HEIGHT = 266;
    private static final String TAG = KeyBoardUtils.class.getSimpleName();

    /**
     * 打开软键盘
     *
     * @param mEditText 输入框
     * @param mContext  上下文
     */
    public static void openKeybord(EditText mEditText, Context mContext) {
        InputMethodManager imm = (InputMethodManager) mContext
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(mEditText, InputMethodManager.RESULT_SHOWN);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,
                InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    /**
     * 关闭软键盘
     *
     * @param mEditText 输入框
     * @param mContext  上下文
     */
    public static void closeKeybord(EditText mEditText, Context mContext) {
        InputMethodManager imm = (InputMethodManager) mContext
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        imm.hideSoftInputFromWindow(mEditText.getWindowToken(), 0);
    }

    /**
     * 计算软键盘的高度
     *
     * @param paramActivity
     * @return
     */
    public static int getKeyboardHeight(Activity paramActivity) {
        int height = (int) DeviceUtils.getScreenHeight(paramActivity) - DeviceUtils.getStatusBarHeight(paramActivity)
                - DeviceUtils.getVisibleFrameHeight(paramActivity);
        if (height == 0) {
            return getDipDimension(paramActivity, DEFAULT_SOFT_KEYBOARD_HEIGHT);
        }
        return height;
    }

    public static int getDipDimension(Context context, float size) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, size, context.getResources().getDisplayMetrics());
    }

    /**
     * 判断键盘是否打开
     *
     * @param paramActivity
     * @return
     */
    public static boolean isKeyboardOpen(Activity paramActivity) {
        int height = (int) DeviceUtils.getScreenHeight(paramActivity) - DeviceUtils.getStatusBarHeight(paramActivity)
                - DeviceUtils.getVisibleFrameHeight(paramActivity);
        if (height <= 0) {
            return false;
        } else {
            return true;
        }
    }

}  