package com.music.player.haige.mvp.presenter;

import com.music.player.haige.mvp.contract.AppBasePresenter;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.Api;
import com.music.player.haige.mvp.model.entity.BaseResponse;
import com.music.player.haige.mvp.model.entity.user.ModifyUserRequest;

import javax.inject.Inject;

import me.jessyan.rxerrorhandler.handler.ErrorHandleSubscriber;

/**
 * Created by Naruto on 2018/5/13.
 */

public class EditProfilePresenter extends AppBasePresenter<MVContract.EditProfileModel, MVContract.EditProfileView> {

    public static final String TAG = EditProfilePresenter.class.getSimpleName();

    @Inject
    public EditProfilePresenter(MVContract.EditProfileModel model, MVContract.EditProfileView view) {
        super(model, view);
    }

    public void modifyUserProfile(ModifyUserRequest request) {
            buildObservable(mModel.modifyUserProfile(request))
                    .subscribe(new ErrorHandleSubscriber<BaseResponse>(mErrorHandler) {
                        @Override
                        public void onNext(BaseResponse response) {
                            if (response != null) {
                                mRootView.onSuccess(Api.Action.MODIFY_USER_PROFILE, response);
                            } else {
                                mRootView.onError(Api.Action.MODIFY_USER_PROFILE, Api.State.NORMAL.getCode(), Api.State.NORMAL.getMsg());
                            }
                        }

                        @Override
                        public void onError(Throwable t) {
                            super.onError(t);
                            mRootView.onError(Api.Action.USER_PROFILE, "-1", t.getMessage());
                        }
                    });
        }
}
