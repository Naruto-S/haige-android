package com.music.player.haige.mvp.ui.utils;

import android.util.Log;

import com.music.player.haige.BuildConfig;

/**
 * 调试日志工具类。
 * Created by bianque on 6/24/16.
 */
public final class DebugLogger {

    private static final String TAG = DebugLogger.class.getSimpleName();
    private static boolean sDebugMode = BuildConfig.LOG_DEBUG;

    public static void v(String tag, String msg) {
        if (sDebugMode) Log.v("" + tag, "" + msg);
    }

    public static void v(String tag, String msg, Throwable tr) {
        if (sDebugMode) Log.v("" + tag, "" + msg, tr);
    }

    public static void v(String tag, Throwable tr) {
        if (sDebugMode) Log.v("" + tag, "", tr);
    }

    public static void v(String tag, Object obj) {
        if (sDebugMode) Log.v("" + tag, "" + obj);
    }

    public static void d(String msg) {
        if (sDebugMode) Log.d(TAG, "" + msg);
    }

    public static void d(Throwable tr) {
        if (sDebugMode) Log.d(TAG, "" + tr);
    }

    public static void d(Object obj) {
        if (sDebugMode) Log.d(TAG, "" + obj);
    }

    public static void d(String tag, String msg) {
        if (sDebugMode) Log.d("" + tag, "" + msg);
    }

    public static void d(String tag, String msg, Throwable tr) {
        if (sDebugMode) Log.d("" + tag, "" + msg, tr);
    }

    public static void d(String tag, Throwable tr) {
        if (sDebugMode) Log.d("" + tag, "", tr);
    }

    public static void d(String tag, Object obj) {
        if (sDebugMode) Log.d("" + tag, "" + obj);
    }

    public static void i(String tag, String msg) {
        if (sDebugMode) Log.i("" + tag, "" + msg);
    }

    public static void i(String tag, String msg, Throwable tr) {
        if (sDebugMode) Log.i("" + tag, "" + msg, tr);
    }

    public static void i(String tag, Throwable tr) {
        if (sDebugMode) Log.i("" + tag, "", tr);
    }

    public static void i(String tag, Object obj) {
        if (sDebugMode) Log.i("" + tag, "" + obj);
    }

    public static void w(String tag, String msg) {
        if (sDebugMode) Log.w("" + tag, "" + msg);
    }

    public static void w(String tag, String msg, Throwable tr) {
        if (sDebugMode) Log.w("" + tag, "" + msg, tr);
    }

    public static void w(String tag, Throwable tr) {
        if (sDebugMode) Log.w("" + tag, tr);
    }

    public static void w(String tag, Object obj) {
        if (sDebugMode) Log.w("" + tag, "" + obj);
    }

    public static void e(String msg) {
        if (sDebugMode) Log.e(TAG, "" + msg);
    }

    public static void e(Throwable tr) {
        if (sDebugMode) Log.e(TAG, "" + tr);
    }

    public static void e(Object obj) {
        if (sDebugMode) Log.e(TAG, "" + obj);
    }

    public static void e(String tag, String msg) {
        if (sDebugMode) Log.e("" + tag, "" + msg);
    }

    public static void e(String tag, String msg, Throwable tr) {
        if (sDebugMode) Log.e("" + tag, "" + msg, tr);
    }

    public static void e(String tag, Throwable tr) {
        if (sDebugMode) Log.e("" + tag, "", tr);
    }

    public static void e(String tag, Object obj) {
        if (sDebugMode) Log.e("" + tag, "" + obj);
    }

    public static void e(String tag, String msg, Exception e) {
        if (sDebugMode) {
            Log.e(tag, msg, e);
        }
    }
}