package com.music.player.haige.app.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.hc.base.base.BaseService;
import com.hc.base.utils.DateUtils;
import com.hc.core.integration.IRepositoryManager;
import com.hc.core.utils.CoreUtils;
import com.music.player.haige.app.EventBusTags;
import com.music.player.haige.app.HaigeApplication;
import com.music.player.haige.app.base.BaseConsumerError;
import com.music.player.haige.app.base.BaseConsumerNext;
import com.music.player.haige.app.base.pref.DevicePrefHelper;
import com.music.player.haige.app.constants.TimeConstants;
import com.music.player.haige.app.receiver.TimeTickReceiver;
import com.music.player.haige.app.utils.NotificationUtils;
import com.music.player.haige.app.utils.statistics.CommonStatistics;
import com.music.player.haige.app.utils.statistics.StatisticsConstant;
import com.music.player.haige.di.component.DaggerServiceComponent;
import com.music.player.haige.mvp.model.api.service.ApiService;
import com.music.player.haige.mvp.model.entity.config.AppConfigResponse;
import com.music.player.haige.mvp.model.entity.notify.NotifyBean;
import com.music.player.haige.mvp.model.entity.notify.NotifyResponse;
import com.music.player.haige.mvp.model.entity.update.UpdateResponse;
import com.music.player.haige.mvp.ui.utils.DebugLogger;
import com.music.player.haige.mvp.ui.utils.RxJavaUtils;
import com.music.player.haige.mvp.ui.utils.Utils;

import org.simple.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;

import static com.hc.base.utils.DateUtils.HH;

/**
 * Created by john on 2018/1/19.
 */

public class LocalService extends BaseService implements Observer {

    @Inject
    IRepositoryManager mRepositoryManager;

    @Inject
    protected RxErrorHandler mErrorHandler;

    private Disposable mTimerDisposable;
    private Disposable mAppConfigDisposable;
    private Disposable mUpdateDisposable;
    private Disposable mGetNotifyDisposable;

    private HashMap<Integer, Integer> mPushHourMap = new HashMap<>();

    public static void start(Context context) {
        try {
            Intent intent = new Intent(context, LocalService.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(intent);
            }
            context.startService(intent);
        } catch (Exception e) {
            //BugFix:java.lang.IllegalStateException: Not allowed to start service Intent
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            AlarmJobService.doService();
        }
        return Service.START_STICKY;
    }

    @Override
    public void init() {
        DaggerServiceComponent
                .builder()
                .appComponent(CoreUtils.obtainAppComponentFromContext(HaigeApplication.getInstance()))
                .build()
                .inject(this);

        TimeTickReceiver.getInstance(this).addObserver(this);
        startTimer();
    }

    private void startTimer() {
        mTimerDisposable = io.reactivex.Observable.interval(0, 1, TimeUnit.HOURS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aLong -> {
                    updateConfig();
                    getPushNotify();
                });
        addDispose(mTimerDisposable);
    }

    private void updateConfig() {
        mAppConfigDisposable = io.reactivex.Observable
                .just(mRepositoryManager.obtainRetrofitService(ApiService.class).getAppConfig())
                .flatMap(observable -> observable)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseConsumerNext<AppConfigResponse>() {
                    @Override
                    public void accept(AppConfigResponse response) throws Exception {
                        super.accept(response);
                        if (Utils.isNotNull(response.getData())) {
                            DebugLogger.e(TAG, response.getData());
                            DevicePrefHelper.putUserConfig(response.getData());
                        }
                    }
                }, new BaseConsumerError() {
                    @Override
                    public void accept(Throwable throwable) {
                        super.accept(throwable);
                        RxJavaUtils.dispose(mAppConfigDisposable);
                    }
                });
        addDispose(mAppConfigDisposable);

        mUpdateDisposable = io.reactivex.Observable
                .just(mRepositoryManager.obtainRetrofitService(ApiService.class).getUpdateConfig())
                .flatMap(observable -> observable)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseConsumerNext<UpdateResponse>() {
                    @Override
                    public void accept(UpdateResponse response) throws Exception {
                        super.accept(response);
                        if (Utils.isNotNull(response.getData())) {
                            DebugLogger.e(TAG, response.getData());
                            DevicePrefHelper.putUpdateConfig(response.getData());
                            EventBus.getDefault().post(EventBusTags.MAIN.UPDATE_DIALOG);
                        }
                    }
                }, new BaseConsumerError() {
                    @Override
                    public void accept(Throwable throwable) {
                        super.accept(throwable);
                        RxJavaUtils.dispose(mAppConfigDisposable);
                    }
                });
        addDispose(mUpdateDisposable);
    }

    private void getPushNotify() {
        mGetNotifyDisposable = io.reactivex.Observable
                .just(mRepositoryManager.obtainRetrofitService(ApiService.class).getPushNotify())
                .flatMap(observable -> observable)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseConsumerNext<NotifyResponse>() {
                    @Override
                    public void accept(NotifyResponse response) throws Exception {
                        super.accept(response);
                        DebugLogger.e(TAG, response.getData());
                        DevicePrefHelper.putNotifyList(response.getData());
                    }
                }, new BaseConsumerError() {
                    @Override
                    public void accept(Throwable throwable) {
                        super.accept(throwable);
                        RxJavaUtils.dispose(mGetNotifyDisposable);
                    }
                });
        addDispose(mGetNotifyDisposable);
    }

    private void updateNotify() {
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        ArrayList<NotifyBean> notifyLists = DevicePrefHelper.getNotifyList();
        if (Utils.isNotEmptyCollection(notifyLists)) {
            for (NotifyBean notify : notifyLists) {
                if (notify.getStartTime() == hour && !mPushHourMap.containsKey(notify.getStartTime())) {
                    if (notify.getShowType() == 1) {
                        if ((System.currentTimeMillis() - notify.getCreateTime()) < TimeConstants.HOUR_24) {
                            showNotification(notify);
                        }
                    } else if (notify.getShowType() == 2) {
                        showNotification(notify);
                    }
                }
            }
        }
    }

    private void showNotification(NotifyBean notify) {
        mPushHourMap.put(notify.getStartTime(), notify.getStartTime());

        if (notify.getNotifyType() == NotificationUtils.NOTIFY_TYPE_COMMON) {
            NotificationUtils.showNotification(LocalService.this,
                    notify.getTitle(),
                    notify.getContent(),
                    NotificationUtils.NOTIFY_TYPE_COMMON);
        } else if (notify.getNotifyType() == NotificationUtils.NOTIFY_TYPE_MUSIC) {
            NotificationUtils.showNotification(LocalService.this,
                    notify.getTitle(),
                    notify.getContent(),
                    notify.getSong(),
                    NotificationUtils.NOTIFY_TYPE_MUSIC);
        }

        CommonStatistics.sendEvent(StatisticsConstant.NOTICE_ARRIVE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        TimeTickReceiver.getInstance(this).deleteObserver(this);
    }

    @Override
    public void update(Observable observable, Object data) {
        if (observable instanceof TimeTickReceiver) {
            resetTime();
            updateNotify();
        }
    }

    private void resetTime() {
        String currTime = DateUtils.format(System.currentTimeMillis(), HH);
        if (currTime.equals("00:00")) {
            mPushHourMap.clear();
        }
    }
}
