package com.music.player.haige.di.component;

import com.hc.core.di.component.AppComponent;
import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.di.module.NowPlayModule;
import com.music.player.haige.mvp.ui.main.activity.MusicCompleteActivity;
import com.music.player.haige.mvp.ui.main.fragment.NowPlayFragment;

import dagger.Component;

/**
 * ================================================
 * Created by huangcong on 2018/2/25.
 * ================================================
 */

@FragmentScope
@Component(modules = {NowPlayModule.class}, dependencies = AppComponent.class)
public interface NowPlayComponent {

    void inject(NowPlayFragment fragment);

    void inject(MusicCompleteActivity fragment);

}
