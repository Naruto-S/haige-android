package com.music.player.haige.mvp.ui.music.adapter;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hc.base.utils.ATEUtil;
import com.music.player.haige.R;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.ui.music.service.MusicServiceConnection;
import com.music.player.haige.mvp.ui.music.viewholder.SongItemViewHolder;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;
import com.music.player.haige.mvp.ui.utils.Utils;
import com.music.player.haige.mvp.ui.widget.NoDoubleClickListener;

import java.util.ArrayList;


public class PlayQueueSongsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Song> mSongs = new ArrayList<>();
    private AppCompatActivity mContext;
    private ItemClickListener mItemClickListener;

    public PlayQueueSongsAdapter(AppCompatActivity context) {
        this.mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View song = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.app_item_music, viewGroup, false);
        return new ItemHolder(song);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ItemHolder itemHolder = (ItemHolder) holder;
        Song localItem = mSongs.get(position);

        if (localItem == null) return;

        itemHolder.rank.setText(String.valueOf(position + 1));
        itemHolder.title.setText(localItem.musicName);
        itemHolder.artist.setText(localItem.getArtistName(mContext));
        itemHolder.album.setText(localItem.albumName);

        if (MusicServiceConnection.getCurrentAudioId() == localItem.songId) {
            itemHolder.title.setTextColor(ResourceUtils.getColor(R.color.colorAccent));
            if (MusicServiceConnection.isPlaying()) {
                itemHolder.rank.setVisibility(View.GONE);
                itemHolder.musicVisualizer.setVisibility(View.VISIBLE);
                itemHolder.musicVisualizer.setColor(ResourceUtils.getColor(R.color.colorAccent));
            } else {
                itemHolder.rank.setVisibility(View.VISIBLE);
                itemHolder.rank.setTextColor(ResourceUtils.getColor(R.color.colorAccent));
                itemHolder.musicVisualizer.setVisibility(View.GONE);
            }
        } else {
            itemHolder.rank.setVisibility(View.VISIBLE);
            itemHolder.rank.setTextColor(ATEUtil.getThemeTextColorPrimary(mContext));
            itemHolder.title.setTextColor(ATEUtil.getThemeTextColorPrimary(mContext));
            itemHolder.musicVisualizer.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return mSongs == null ? 0 : mSongs.size();
    }

    public void setSongList(ArrayList<Song> arrayList) {
        mSongs.clear();
        mSongs.addAll(arrayList);
        notifyDataSetChanged();
    }

    public ArrayList<Song> getSongs() {
        return mSongs;
    }

    public void clear() {
        mSongs.clear();
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(ItemClickListener listener) {
        this.mItemClickListener = listener;
    }

    public interface ItemClickListener {
        void onItemClick(int position, View view);
    }

    public class ItemHolder extends SongItemViewHolder {

        public ItemHolder(View view) {
            super(view);
            popupMenu.setImageDrawable(mContext.getResources().getDrawable(R.drawable.app_ic_close));
            popupMenu.setOnClickListener(new NoDoubleClickListener() {
                @Override
                public void onNoDoubleClick(View v) {
                    int position = getAdapterPosition();
                    if (position >= 0 && position < mSongs.size()) {
                        MusicServiceConnection.removeFromQueue(getAdapterPosition());
                        mSongs.remove(getAdapterPosition());
                        notifyItemRemoved(getAdapterPosition());
                        // 更新rank号
                        notifyItemRangeChanged(getAdapterPosition(), mSongs.size());
                    }
                }
            });
            view.setOnClickListener(v -> {
                if (Utils.isNotNull(mItemClickListener)) {
                    int position = getAdapterPosition();
                    mItemClickListener.onItemClick(position, v);
                }
            });
        }
    }
}
