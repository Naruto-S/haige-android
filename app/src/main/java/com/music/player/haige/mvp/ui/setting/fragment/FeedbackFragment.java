package com.music.player.haige.mvp.ui.setting.fragment;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.hc.core.di.component.AppComponent;
import com.music.player.haige.R;
import com.music.player.haige.app.EventBusTags;
import com.music.player.haige.app.base.AppBaseFragment;
import com.music.player.haige.di.component.DaggerFeedbackComponent;
import com.music.player.haige.di.module.FeedbackModule;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.model.api.Api;
import com.music.player.haige.mvp.model.entity.action.SendFeedbackRequest;
import com.music.player.haige.mvp.presenter.FeedbackFPresenter;

import org.simple.eventbus.EventBus;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * ================================================
 * Created by huangcong on 2018/3/31.
 * ================================================
 */

public class FeedbackFragment extends AppBaseFragment<FeedbackFPresenter> implements MVContract.FeedbackView {

    @BindView(R.id.edit_feedback_text)
    EditText mFeedBackEdit;
    @BindView(R.id.edit_contact_text)
    EditText mContactEdit;
    @BindView(R.id.tv_submit)
    TextView mSubmitTv;

    private boolean isNullFeedBack = true;
    private boolean isNullContact = true;

    public static FeedbackFragment newInstance(){
        FeedbackFragment fragment = new FeedbackFragment();
        return fragment;
    }

    @Override
    public void setupFragmentComponent(AppComponent appComponent) {
        DaggerFeedbackComponent.builder()
                .appComponent(appComponent)
                .feedbackModule(new FeedbackModule(this))
                .build()
                .inject(this);
    }

    @Override
    public View initView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.app_fragment_feedback, container, false);
    }

    @Override
    public void initData(Bundle savedInstanceState) {
        mSubmitTv.setEnabled(false);

        mFeedBackEdit.addTextChangedListener(feedBackEditWatcher);
        mContactEdit.addTextChangedListener(contactEditWatcher);
    }

    @Override
    public void onSuccess(Api.Action action, Object o) {
        switch (action) {
            case FEEDBACK:
                mFeedBackEdit.setText("");
                mContactEdit.setText("");
                Toast.makeText(getContext(), R.string.common_send_success, Toast.LENGTH_SHORT).show();
                break;
        }
        super.onSuccess(action, o);
    }

    @Override
    public void onError(Api.Action action, String code, String message) {
        switch (action) {
            case FEEDBACK:
                Toast.makeText(getContext(), R.string.common_send_fail, Toast.LENGTH_SHORT).show();
                break;
        }
        super.onError(action, code, message);
    }

    @OnClick({R.id.app_fragment_feedback_back_iv, R.id.tv_submit})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.app_fragment_feedback_back_iv:
                EventBus.getDefault().post(EventBusTags.MAIN.BACK);
                break;
            case R.id.tv_submit:
                String feedback = mFeedBackEdit.getText().toString();
                String contact = mContactEdit.getText().toString();

                SendFeedbackRequest request = new SendFeedbackRequest();
                request.setContent(feedback);
                request.setContact(contact);
                mPresenter.sendFeedback(request);
                break;
        }
    }

    private final TextWatcher feedBackEditWatcher = new TextWatcher() {

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        public void afterTextChanged(Editable s) {
            int length = s.toString().length();
            if (length == 0) {
                isNullFeedBack = true;
            } else {
                isNullFeedBack = false;
            }

            if (!isNullFeedBack && !isNullContact) {
                mSubmitTv.setEnabled(true);
            } else {
                mSubmitTv.setEnabled(false);
            }
        }
    };

    private final TextWatcher contactEditWatcher = new TextWatcher() {

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        public void afterTextChanged(Editable s) {
            int length = s.toString().length();
            if (length == 0) {
                isNullContact = true;
            } else {
                isNullContact = false;
            }

            if (!isNullFeedBack && !isNullContact) {
                mSubmitTv.setEnabled(true);
            } else {
                mSubmitTv.setEnabled(false);
            }
        }
    };
}
