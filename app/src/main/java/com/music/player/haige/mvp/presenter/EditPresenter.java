package com.music.player.haige.mvp.presenter;

import com.music.player.haige.mvp.contract.AppBasePresenter;
import com.music.player.haige.mvp.contract.MVContract;

import javax.inject.Inject;

public class EditPresenter extends AppBasePresenter<MVContract.CommonModel, MVContract.CommonView> {

    public static final String TAG = EditPresenter.class.getSimpleName();

    @Inject
    public EditPresenter(MVContract.CommonModel model, MVContract.CommonView view) {
        super(model, view);
    }
}
