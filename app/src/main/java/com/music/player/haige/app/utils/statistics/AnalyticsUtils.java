package com.music.player.haige.app.utils.statistics;

import android.content.Context;

import com.hc.core.utils.DeviceUtils;
import com.music.player.haige.app.HaigeApplication;
import com.umeng.analytics.MobclickAgent;
import com.umeng.analytics.dplus.UMADplus;

import java.util.HashMap;
import java.util.Map;

public class AnalyticsUtils extends CommonStatistics {

    private static AnalyticsUtils mAnalyticsUtils = null;
    private String mDevice;

    private AnalyticsUtils() {
        mDevice = DeviceUtils.getDeviceUUID(HaigeApplication.getInstance());
    }

    public static AnalyticsUtils getInstance() {
        if (mAnalyticsUtils == null) {
            synchronized (AnalyticsUtils.class) {
                if (mAnalyticsUtils == null) {
                    mAnalyticsUtils = new AnalyticsUtils();
                }
            }
        }
        return mAnalyticsUtils;
    }

    @Override
    public void onPageStart(String value) {
        MobclickAgent.onPageStart(value);
    }

    @Override
    public void onPageEnd(String value) {
        MobclickAgent.onPageEnd(value);
    }

    @Override
    public void onResume(Context context) {
        MobclickAgent.onResume(context);
    }

    @Override
    public void onPause(Context context) {
        MobclickAgent.onPause(context);
    }

    @Override
    public void onEvent(String eventName) {
        Map<String, Object> eventValue = new HashMap<>();
        eventValue.put(eventName, eventName);
        eventValue.put(StatisticsConstant.USER_DEVICE_ID, mDevice);
        UMADplus.track(HaigeApplication.getInstance(), eventName, eventValue);

        MobclickAgent.onEvent(HaigeApplication.getInstance(), eventName);
    }

    @Override
    public void onEvent(String eventName, String value) {
        Map<String, Object> eventValue = new HashMap<>();
        eventValue.put(eventName, value);
        eventValue.put(StatisticsConstant.USER_DEVICE_ID, mDevice);
        UMADplus.track(HaigeApplication.getInstance(), eventName, eventValue);

        MobclickAgent.onEvent(HaigeApplication.getInstance(), eventName);
    }

    @Override
    public void onEvent(String eventName, Map<String, Object> value) {
        value.put(StatisticsConstant.USER_DEVICE_ID, mDevice);
        UMADplus.track(HaigeApplication.getInstance(), eventName, value);

        MobclickAgent.onEvent(HaigeApplication.getInstance(), eventName);
    }
}
