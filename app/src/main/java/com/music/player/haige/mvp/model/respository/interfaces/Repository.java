package com.music.player.haige.mvp.model.respository.interfaces;

import com.music.player.haige.mvp.model.entity.music.Album;
import com.music.player.haige.mvp.model.entity.music.Artist;
import com.music.player.haige.mvp.model.entity.music.FolderInfo;
import com.music.player.haige.mvp.model.entity.music.Playlist;
import com.music.player.haige.mvp.model.entity.music.Song;

import java.util.List;

import io.reactivex.Observable;

public interface Repository {

    Observable<List<Album>> getAllAlbums();

    Observable<Album> getAlbum(long id);

    Observable<List<Album>> getAlbums(String paramString);

    Observable<List<Song>> getSongsForAlbum(long albumID);

    Observable<List<Album>> getAlbumsForArtist(long artistID);

    Observable<List<Artist>> getAllArtists();

    Observable<Artist> getArtist(long artistID);

    Observable<List<Artist>> getArtists(String paramString);

    Observable<List<Song>> getSongsForArtist(long artistID);

    Observable<List<Song>> getRecentlyAddedSongs();

    Observable<List<Album>> getRecentlyAddedAlbums();

    Observable<List<Artist>> getRecentlyAddedArtists();

    Observable<List<Song>> getRecentlyPlayedSongs();

    Observable<List<Album>> getRecentlyPlayedAlbums();

    Observable<List<Artist>> getRecentlyPlayedArtist();

    Observable<List<Playlist>> getPlaylist(boolean defaultIncluded);

    Observable<List<Song>> getSongsInPlaylist(long playlistID);

    Observable<List<Song>> getQueueSongs();

    Observable<List<Song>> getFavoriteSongs();

    Observable<List<Album>> getFavoriteAlbums();

    Observable<List<Artist>> getFavoriteArtist();

    Observable<List<Song>> getAllSongs();

    Observable<List<Song>> searchSongs(String searchString);

    Observable<List<Song>> getTopPlaySongs();

    Observable<List<FolderInfo>> getFoldersWithSong();

    Observable<List<Song>> getSongsInFolder(String path);

    Observable<List<Object>> getSearchResult(String queryString);
}
