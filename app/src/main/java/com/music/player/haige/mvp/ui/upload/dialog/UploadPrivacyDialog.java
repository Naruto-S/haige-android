package com.music.player.haige.mvp.ui.upload.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.TextView;

import com.music.player.haige.R;
import com.music.player.haige.app.base.BaseDialogFragment;
import com.music.player.haige.app.base.pref.DevicePrefHelper;
import com.music.player.haige.app.utils.launcher.ActivityLauncherStart;
import com.music.player.haige.app.web.WebViewActivity;
import com.music.player.haige.mvp.model.entity.music.Song;
import com.music.player.haige.mvp.ui.upload.UploadListActivity;

import butterknife.BindView;
import butterknife.OnClick;

import static com.music.player.haige.app.Constants.URL_PRIVACY_UPLOAD;

public class UploadPrivacyDialog extends BaseDialogFragment {

    @BindView(R.id.tv_title)
    TextView mTitleTv;

    @BindView(R.id.tv_content)
    TextView mContentTv;

    @BindView(R.id.tv_no_agree)
    TextView mNoTv;

    @BindView(R.id.tv_agree)
    TextView mOkTv;

    private boolean isRefuseDialog = false;
    private Song song;

    public static UploadPrivacyDialog create(FragmentManager manager) {
        UploadPrivacyDialog dialog = new UploadPrivacyDialog();
        dialog.setFragmentManger(manager);
        return dialog;
    }

    public UploadPrivacyDialog setRefuseDialog(Song song) {
        this.isRefuseDialog = true;
        this.song = song;
        return this;
    }

    public UploadPrivacyDialog show() {
        show(mFragmentManager);
        return this;
    }

    @Override
    public int setRootView() {
        return R.layout.dialog_upload_privacy;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (isRefuseDialog)  {
            StringBuffer buffer = new StringBuffer();
            buffer.append("你上传的歌曲\"");
            buffer.append(song.musicName);
            buffer.append("\"未通过审核。");

            mTitleTv.setText("审核声明");
            mContentTv.setText(buffer.toString());
            mNoTv.setText("我知道了");
            mOkTv.setText("继续上传");
        }
    }

    @OnClick({R.id.tv_agree, R.id.tv_no_agree, R.id.tv_privacy})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_agree:
                if (!isRefuseDialog) {
                    DevicePrefHelper.putAgreeUploadPrivacy(true);
                } else {
                    ActivityLauncherStart.startActivity(getActivity(), UploadListActivity.class);
                }
                dismiss();
                break;
            case R.id.tv_no_agree:
                if (!isRefuseDialog) {
                    DevicePrefHelper.putAgreeUploadPrivacy(false);
                }
                dismiss();
                break;
            case R.id.tv_privacy:
                WebViewActivity.start(getContext(), URL_PRIVACY_UPLOAD);
                break;
        }
    }
}
