package com.music.player.haige.di.component;

import com.hc.core.di.component.AppComponent;
import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.di.module.LoginDialogModule;
import com.music.player.haige.mvp.ui.login.LoginDialogFragment;

import dagger.Component;

/**
 * Created by Naruto on 2018/5/5.
 */

@FragmentScope
@Component(modules = {LoginDialogModule.class}, dependencies = AppComponent.class)
public interface LoginDialogComponent {

    void inject(LoginDialogFragment fragment);
}
