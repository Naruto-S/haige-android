package com.music.player.haige.mvp.ui.comment;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hc.core.utils.DeviceUtils;
import com.music.player.haige.R;
import com.music.player.haige.mvp.model.entity.comment.CommentBean;
import com.music.player.haige.mvp.model.entity.comment.CommentRequest;
import com.music.player.haige.mvp.ui.comment.adapter.CommentAdapter;
import com.music.player.haige.mvp.ui.main.widget.FixHeightBottomSheetDialog;
import com.music.player.haige.mvp.ui.utils.KeyBoardUtils;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;
import com.music.player.haige.mvp.ui.utils.Utils;
import com.music.player.haige.mvp.ui.widget.emoji.EditCommentView;

import java.util.ArrayList;

public class CommentBottomSheetDialog extends FixHeightBottomSheetDialog {

    private Activity mActivity;
    private ConstraintLayout mContainer;
    private RecyclerView mRecyclerView;
    private TextView mCommentCountTv;
    private TextView mNoMoreTv;
    private TextView mEditTv;
    private ImageView mCloseIv;
    private EditCommentView mEditCommentView;
    private ArrayList<CommentBean> mCommentList = new ArrayList<>();
    private CommentAdapter mAdapter;

    public CommentBottomSheetDialog(@NonNull Context context) {
        super(context, R.style.BottomSheetDialogStyle);
        init(context);
    }

    public CommentBottomSheetDialog(@NonNull Context context, int theme) {
        super(context, theme);
        init(context);
    }

    private void init(Context context) {
        mActivity = (Activity) context;

        View rootView = getLayoutInflater().inflate(R.layout.layout_barrage_comment_list, null);
        mContainer = rootView.findViewById(R.id.container);
        mRecyclerView = rootView.findViewById(R.id.recycler_comment);
        mCommentCountTv = rootView.findViewById(R.id.tv_comment_count);
        mEditCommentView = rootView.findViewById(R.id.edit_comment_view);
        mNoMoreTv = rootView.findViewById(R.id.tv_no_more);
        mEditTv = rootView.findViewById(R.id.tv_comment_edit);
        mEditTv.setOnClickListener(v -> {
            CommentRequest request = new CommentRequest();
            request.setSub(false);
            mEditCommentView.setCommentRequest(request);
            mEditCommentView.setInputVisibility(View.VISIBLE);
            KeyBoardUtils.openKeybord(mEditCommentView.getEditText(), mActivity);
            mEditCommentView.getEditText().requestFocus();
        });
        mCloseIv = rootView.findViewById(R.id.iv_close);
        mCloseIv.setOnClickListener(v -> dismiss());

        initRecyclerView();

        this.setContentView(rootView);

        try {
            // hack bg color of the BottomSheetDialog
            ViewGroup parent = (ViewGroup) rootView.getParent();
            parent.setBackgroundResource(android.R.color.transparent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initRecyclerView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(null);
        mAdapter = new CommentAdapter();
        mAdapter.setCommentReplyListener(request -> {
            request.setSub(true);
            mEditCommentView.setCommentRequest(request);
            mEditCommentView.setInputVisibility(View.VISIBLE);
            KeyBoardUtils.openKeybord(mEditCommentView.getEditText(), mActivity);
            mEditCommentView.getEditText().requestFocus();
        });
        mRecyclerView.setAdapter(mAdapter);

        refreshData();
    }

    private void refreshData() {
        if (Utils.isNotNull(mCommentCountTv)) {
            mCommentCountTv.setText(ResourceUtils.resourceString(R.string.barrage_comment_count, mCommentList.size()));
        }

        if (Utils.isNotNull(mAdapter)) {
            mAdapter.setCommentList(mCommentList);
        }

        if (Utils.isNotEmptyCollection(mCommentList)) {
            if (Utils.isNotNull(mRecyclerView) && Utils.isNotNull(mNoMoreTv)) {
                mRecyclerView.setVisibility(View.VISIBLE);
                mNoMoreTv.setVisibility(View.GONE);
            }
        } else {
            if (Utils.isNotNull(mRecyclerView) && Utils.isNotNull(mNoMoreTv)) {
                mRecyclerView.setVisibility(View.GONE);
                mNoMoreTv.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        ViewGroup.LayoutParams params = mContainer.getLayoutParams();
        params.height = (int) (DeviceUtils.getScreenHeight(mActivity) * 0.8f);
        mContainer.setLayoutParams(params);

        mBehavior.setPeekHeight((int) (DeviceUtils.getScreenHeight(mActivity) * 0.8f));
    }

    @Override
    public void dismiss() {
        KeyBoardUtils.closeKeybord(mEditCommentView.getEditText(), mActivity);
        super.dismiss();
    }

    public EditCommentView getEditCommentView() {
        return mEditCommentView;
    }

    public void setCommentList(ArrayList<CommentBean> commentList) {
        mCommentList.clear();
        mCommentList.addAll(commentList);

        refreshData();
    }

    public void setCommentCount() {
        if (Utils.isNotNull(mCommentCountTv)) {
            mCommentList.clear();
            mCommentList.addAll(getCommentList());
            mCommentCountTv.setText(ResourceUtils.resourceString(R.string.barrage_comment_count, mCommentList.size()));
        }
    }

    public ArrayList<CommentBean> getCommentList() {
        return mAdapter.getCommentList();
    }

    public CommentBean getCommentItem(int position) {
        return mAdapter.getCommentItem(position);
    }

    public void setCommentActionListener(CommentAdapter.onCommentActionListener actionListener) {
        if (Utils.isNotNull(mAdapter)) {
            mAdapter.setCommentActionListener(actionListener);
        }
    }
}
