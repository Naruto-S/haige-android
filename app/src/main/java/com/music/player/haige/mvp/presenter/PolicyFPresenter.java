package com.music.player.haige.mvp.presenter;

import com.music.player.haige.mvp.contract.AppBasePresenter;
import com.music.player.haige.mvp.contract.MVContract;

import javax.inject.Inject;

/**
 * ================================================
 * Created by huangcong on 2018/3/31.
 * ================================================
 */

public class PolicyFPresenter extends AppBasePresenter<MVContract.CommonModel, MVContract.CommonView> {

    @Inject
    public PolicyFPresenter(MVContract.CommonModel model, MVContract.CommonView view) {
        super(model, view);
    }
}
