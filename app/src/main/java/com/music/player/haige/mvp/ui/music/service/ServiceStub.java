package com.music.player.haige.mvp.ui.music.service;

import android.os.Parcel;
import android.os.RemoteException;

import com.music.player.haige.MediaAidlInterface;
import com.music.player.haige.mvp.model.entity.music.MusicPlaybackTrack;
import com.music.player.haige.mvp.model.entity.music.Song;

import java.io.File;
import java.io.PrintWriter;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

import timber.log.Timber;

/**
 * ================================================
 * Created by huangcong on 2018/3/13.
 * ================================================
 */

public class ServiceStub extends MediaAidlInterface.Stub {

    private static final String TAG = ServiceStub.class.getName();

    private final WeakReference<MusicService> mService;

    public boolean onTransact(int code, Parcel data, Parcel reply, int flags) {
        try {
            super.onTransact(code, data, reply, flags);
        } catch (final RuntimeException e) {
            Timber.e(TAG + ", onTransact error");
            e.printStackTrace();
            File file = new File(mService.get().getCacheDir().getAbsolutePath() + "/err/");
            if (!file.exists()) {
                file.mkdirs();
            }
            try {
                PrintWriter writer = new PrintWriter(mService.get().getCacheDir().getAbsolutePath() + "/err/" + System.currentTimeMillis() + "_aidl.log");
                e.printStackTrace(writer);
                writer.close();
            } catch (Exception e1) {
                e1.printStackTrace();
            }

            throw e;
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return true;
    }

    public ServiceStub(final MusicService service) {
        mService = new WeakReference<MusicService>(service);
    }

    @Override
    public void openFile(final String path) throws RemoteException {
        mService.get().openFile(path);
    }

    @Override
    public void open(final Map map, final long[] list, final int position)
            throws RemoteException {
        mService.get().open((HashMap<Long, Song>) map, list, position);
    }

    @Override
    public void stop() throws RemoteException {
        mService.get().stop();
    }

    @Override
    public void pause() throws RemoteException {
        mService.get().pause();
    }


    @Override
    public void play() throws RemoteException {
        mService.get().play();
    }

    @Override
    public void prev(boolean forcePrevious) throws RemoteException {
        mService.get().prev(forcePrevious);
    }

    @Override
    public void next() throws RemoteException {
        mService.get().gotoNext(true);
    }

    @Override
    public void enqueue(final long[] list, final Map infos, final int action)
            throws RemoteException {
        mService.get().enqueue(list, (HashMap<Long, Song>) infos, action);
    }

    @Override
    public Map getPlayinfos() throws RemoteException {
        return mService.get().getPlayinfos();
    }

    @Override
    public void putPlayinfos(Map infos) throws RemoteException {
        mService.get().putPlayinfos((HashMap<Long, Song>) infos);
    }

    @Override
    public void moveQueueItem(final int from, final int to) throws RemoteException {
        mService.get().moveQueueItem(from, to);
    }

    @Override
    public void refresh() throws RemoteException {
        mService.get().refresh();
    }

    @Override
    public void playlistChanged() throws RemoteException {
        mService.get().playlistChanged();
    }

    @Override
    public boolean isPlaying() throws RemoteException {
        return mService.get().isPlaying();
    }

    @Override
    public long[] getQueue() throws RemoteException {
        return mService.get().getQueue();
    }

    @Override
    public long getQueueItemAtPosition(int position) throws RemoteException {
        return mService.get().getQueueItemAtPosition(position);
    }

    @Override
    public int getQueueSize() throws RemoteException {
        return mService.get().getQueueSize();
    }

    @Override
    public int getQueueHistoryPosition(int position) throws RemoteException {
        return mService.get().getQueueHistoryPosition(position);
    }

    @Override
    public int getQueueHistorySize() throws RemoteException {
        return mService.get().getQueueHistorySize();
    }

    @Override
    public int[] getQueueHistoryList() throws RemoteException {
        return mService.get().getQueueHistoryList();
    }

    @Override
    public long duration() throws RemoteException {
        return mService.get().duration();
    }

    @Override
    public long position() throws RemoteException {
        return mService.get().position();
    }

    @Override
    public int secondPosition() throws RemoteException {
        return mService.get().getSecondPosition();
    }

    @Override
    public long seek(final long position) throws RemoteException {
        return mService.get().seek(position);
    }

    @Override
    public void seekRelative(final long deltaInMs) throws RemoteException {
        mService.get().seekRelative(deltaInMs);
    }

    @Override
    public long getAudioId() throws RemoteException {
        return mService.get().getAudioId();
    }

    @Override
    public MusicPlaybackTrack getCurrentTrack() throws RemoteException {
        return mService.get().getCurrentTrack();
    }

    @Override
    public MusicPlaybackTrack getTrack(int index) throws RemoteException {
        return mService.get().getTrack(index);
    }

    @Override
    public long getNextAudioId() throws RemoteException {
        return mService.get().getNextAudioId();
    }

    @Override
    public long getPreviousAudioId() throws RemoteException {
        return mService.get().getPreviousAudioId();
    }

    @Override
    public long getArtistId() throws RemoteException {
        return mService.get().getArtistId();
    }

    @Override
    public long getAlbumId() throws RemoteException {
        return mService.get().getAlbumId();
    }

    @Override
    public String getArtistName() throws RemoteException {
        return mService.get().getArtistName();
    }

    @Override
    public String getTrackName() throws RemoteException {
        return mService.get().getTrackName();
    }

    @Override
    public boolean isTrackLocal() throws RemoteException {
        return mService.get().isTrackLocal();
    }

    @Override
    public String getAlbumName() throws RemoteException {
        return mService.get().getAlbumName();
    }

    @Override
    public String getAlbumPath() throws RemoteException {
        return mService.get().getAlbumPath();
    }

    @Override
    public String[] getAlbumPathtAll() throws RemoteException {
        return mService.get().getAlbumPathAll();
    }

    @Override
    public String getPath() throws RemoteException {
        return mService.get().getPath();
    }

    @Override
    public int getQueuePosition() throws RemoteException {
        return mService.get().getQueuePosition();
    }

    @Override
    public void setQueuePosition(final int index) throws RemoteException {
        mService.get().setQueuePosition(index);
    }

    @Override
    public int getShuffleMode() throws RemoteException {
        return mService.get().getShuffleMode();
    }

    @Override
    public void setShuffleMode(final int shuffleMode) throws RemoteException {
        mService.get().setShuffleMode(shuffleMode);
    }

    @Override
    public int getRepeatMode() throws RemoteException {
        return mService.get().getRepeatMode();
    }

    @Override
    public void setRepeatMode(final int repeatmode) throws RemoteException {
        mService.get().setRepeatMode(repeatmode);
    }

    @Override
    public int removeTracks(final int first, final int last) throws RemoteException {
        return mService.get().removeTracks(first, last);
    }


    @Override
    public int removeTrack(final long id) throws RemoteException {
        return mService.get().removeTrack(id);
    }


    @Override
    public boolean removeTrackAtPosition(final long id, final int position)
            throws RemoteException {
        return mService.get().removeTrackAtPosition(id, position);
    }


    @Override
    public int getMediaMountedCount() throws RemoteException {
        return mService.get().getMediaMountedCount();
    }


    @Override
    public int getAudioSessionId() throws RemoteException {
        return mService.get().getAudioSessionId();
    }


    @Override
    public void setLockscreenAlbumArt(boolean enabled) {
        mService.get().setLockscreenAlbumArt(enabled);
    }

    @Override
    public void exit() throws RemoteException {
        mService.get().exit();
    }

    @Override
    public void timing(int time) throws RemoteException {
        mService.get().timing(time);
    }

    @Override
    public void putIsShort(boolean flags) throws RemoteException {
        mService.get().putIsShort(flags);
    }

    @Override
    public boolean isShort() throws RemoteException {
        return mService.get().isShort();
    }

}
