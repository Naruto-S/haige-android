package com.music.player.haige.di.component;

import com.hc.core.di.component.AppComponent;
import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.di.module.OtherProfileModule;
import com.music.player.haige.mvp.ui.user.OtherUserActivity;

import dagger.Component;

/**
 * Created by Naruto on 2018/5/13.
 */
@FragmentScope
@Component(modules = {OtherProfileModule.class}, dependencies = AppComponent.class)
public interface OtherProfileComponent {

    void inject(OtherUserActivity activity);
}
