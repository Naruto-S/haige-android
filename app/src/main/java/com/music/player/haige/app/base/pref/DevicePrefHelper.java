package com.music.player.haige.app.base.pref;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.music.player.haige.BuildConfig;
import com.music.player.haige.mvp.model.entity.config.AppConfigModel;
import com.music.player.haige.mvp.model.entity.notify.NotifyBean;
import com.music.player.haige.mvp.model.entity.update.UpdateBean;
import com.music.player.haige.mvp.ui.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Naruto on 2017/10/25.
 */

public class DevicePrefHelper extends DevicePref {

    private static final String PREF_KEY_MUSIC_WEBP_BG = "PREF_KEY_MUSIC_WEBP_BG";
    private static final String PREF_KEY_USER_CONFIG = "PREF_KEY_USER_CONFIG";
    private static final String PREF_KEY_NOTIFY_LIST = "PREF_KEY_NOTIFY_LIST";
    private static final String PREF_KEY_UPDATE_CONFIG = "PREF_KEY_UPDATE_CONFIG";
    private static final String PREF_KEY_IS_SHOW_GUIDE_SWITCH = "PREF_KEY_IS_SHOW_GUIDE_SWITCH";
    private static final String PREF_KEY_IS_SHOW_GUIDE_LIKE = "PREF_KEY_IS_SHOW_GUIDE_LIKE";

    private static final String PREF_KEY_SHOW_UPDATE_TIME = "PREF_KEY_SHOW_UPDATE_TIME"; //显示升级提示时间

    private static final String PREF_KEY_IS_SHOW_RATE_DIALOG = "PREF_KEY_IS_SHOW_RATE_DIALOG"; //是否可以显示评分弹窗
    private static final String PREF_KEY_NEXT_SHOW_RATE_TIME = "PREF_KEY_NEXT_SHOW_RATE_TIME"; //下次展示评分弹窗的时间

    private static final String PREF_KEY_UPLOAD_PRIVACY_AGREE = "PREF_KEY_UPLOAD_PRIVACY_AGREE";

    public static void putMusicBg(ArrayList<String> list) {
        Gson gson = new GsonBuilder().serializeNulls().create();
        saveString(PREF_KEY_MUSIC_WEBP_BG, gson.toJson(list));
    }

    public static ArrayList<String> getMusicBg() {
        Gson gson = new GsonBuilder().serializeNulls().create();
        return gson.fromJson(getString(PREF_KEY_MUSIC_WEBP_BG, ""), new TypeToken<List<String>>(){}.getType());
    }

    public static void putUserConfig(AppConfigModel config) {
        Gson gson = new GsonBuilder().serializeNulls().create();
        saveString(PREF_KEY_USER_CONFIG, gson.toJson(config));
    }

    public static AppConfigModel getUserConfig() {
        Gson gson = new GsonBuilder().serializeNulls().create();
        String config = getString(PREF_KEY_USER_CONFIG, "");
        if (Utils.isEmptyString(config)) {
            AppConfigModel model = new AppConfigModel();
            model.setIsShowFeedbackBar(true);

            return model;
        }
        return gson.fromJson(config, AppConfigModel.class);
    }

    public static void putNotifyList(ArrayList<NotifyBean> list) {
        Gson gson = new GsonBuilder().serializeNulls().create();
        saveString(PREF_KEY_NOTIFY_LIST, gson.toJson(list));
    }

    public static ArrayList<NotifyBean> getNotifyList() {
        Gson gson = new GsonBuilder().serializeNulls().create();
        return gson.fromJson(getString(PREF_KEY_NOTIFY_LIST, ""), new TypeToken<List<NotifyBean>>(){}.getType());
    }

    public static void putIsShowGuideSwitch(boolean flags) {
        saveBoolean(PREF_KEY_IS_SHOW_GUIDE_SWITCH, flags);
    }

    public static boolean isShowGuideSwitch() {
        return getBoolean(PREF_KEY_IS_SHOW_GUIDE_SWITCH, true);
    }

    public static void putIsShowGuideLike(boolean flags) {
        saveBoolean(PREF_KEY_IS_SHOW_GUIDE_LIKE, flags);
    }

    public static boolean isShowGuideLike() {
        return getBoolean(PREF_KEY_IS_SHOW_GUIDE_LIKE, true);
    }

    public static void putUpdateConfig(UpdateBean config) {
        Gson gson = new GsonBuilder().serializeNulls().create();
        saveString(PREF_KEY_UPDATE_CONFIG, gson.toJson(config));
    }

    public static UpdateBean getUpdateConfig() {
        Gson gson = new GsonBuilder().serializeNulls().create();
        String config = getString(PREF_KEY_UPDATE_CONFIG, "");
        if (Utils.isEmptyString(config)) {
            UpdateBean model = new UpdateBean();
            model.setForceVersion(0);
            model.setLatestVersion(0);
            model.setContent("版本更新\r\n\n1. 问题修复");
            model.setPath("market://details?id=" + BuildConfig.APPLICATION_ID);

            return model;
        }
        return gson.fromJson(config, UpdateBean.class);
    }

    public static void putShowUpdateTime(long flags) {
        saveLong(PREF_KEY_SHOW_UPDATE_TIME, flags);
    }

    public static long getShowUpdateTime() {
        return getLong(PREF_KEY_SHOW_UPDATE_TIME, 0);
    }

    public static void putNextShowRateTime(long flags) {
        saveLong(PREF_KEY_NEXT_SHOW_RATE_TIME, flags);
    }

    public static long getNextShowRateTime() {
        return getLong(PREF_KEY_NEXT_SHOW_RATE_TIME, 0);
    }

    public static void putShowRateDialog(boolean flags) {
        saveBoolean(PREF_KEY_IS_SHOW_RATE_DIALOG, flags);
    }

    public static boolean isShowRateDialog() {
        return getBoolean(PREF_KEY_IS_SHOW_RATE_DIALOG, true);
    }

    public static void putAgreeUploadPrivacy(boolean flags) {
        saveBoolean(PREF_KEY_UPLOAD_PRIVACY_AGREE, flags);
    }

    public static boolean isAgreeUploadPrivacy() {
        return getBoolean(PREF_KEY_UPLOAD_PRIVACY_AGREE, false);
    }
}
