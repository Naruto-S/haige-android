package com.music.player.haige.mvp.ui.main.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.music.player.haige.R;
import com.music.player.haige.mvp.ui.widget.playpause.PlayPauseView;

/**
 * ================================================
 * Created by huangcong on 2018/3/22.
 * <p>
 * 承载音乐封面、播放暂停、下一首、上一首等控件
 * ================================================
 */

public class RecommendPlayView extends FrameLayout {

    private ImageView mInfoPreIv;
    private PlayPauseView mPlayPauseView;
    private ImageView mInfoNextIv;

    public RecommendPlayView(Context context) {
        this(context, null);
    }

    public RecommendPlayView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RecommendPlayView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        LayoutInflater.from(context).inflate(R.layout.app_layout_recommend_play, this, true);
        initView();
    }

    public void play() {
        if (mPlayPauseView != null && !mPlayPauseView.isPlay()) {
            mPlayPauseView.Play();
        }
    }

    public void pause() {
        if (mPlayPauseView != null && mPlayPauseView.isPlay()) {
            mPlayPauseView.Pause();
        }
    }

    protected void initView() {
        mInfoPreIv = findViewById(R.id.app_layout_music_info_previous_view);
        mPlayPauseView = findViewById(R.id.app_layout_music_info_play_pause_view);
        mInfoNextIv = findViewById(R.id.app_layout_music_info_next_view);

        mPlayPauseView.setDrawableColor(getContext().getResources().getColor(R.color.color_FFFFFF));
        mPlayPauseView.setCircleColor(getContext().getResources().getColor(R.color.color_FFFFFF));
        mPlayPauseView.setCircleAlpah(0);
        mPlayPauseView.setEnabled(true);
        mPlayPauseView.Pause();
    }
}
