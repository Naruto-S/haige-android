package com.music.player.haige.app.web;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.webkit.DownloadListener;

import com.music.player.haige.mvp.ui.utils.Utils;

import java.lang.ref.WeakReference;

/**
 * Created by liumingkong on 16/9/12.
 */
public class WebViewDownLoadListener implements DownloadListener {

    private WeakReference<Activity> weakReference;

    public WebViewDownLoadListener(Activity activity) {
        weakReference = new WeakReference<>(activity);
    }

    @Override
    public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype,
                                long contentLength) {
        Activity activity = weakReference.get();
        if (!Utils.isNull(activity)) {
            Uri uri = Uri.parse(url);
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            activity.startActivity(intent);
        }
    }
}
