package com.music.player.haige.mvp.model.entity.search;

import com.music.player.haige.mvp.model.entity.BaseResponse;

import java.util.ArrayList;

/**
 * =============================
 * Created by huangcong on 2018/3/10.
 * =============================
 */

public class SearchHotWordResponse extends BaseResponse<ArrayList<String>> {

}
