package com.music.player.haige.mvp.ui.setting.fragment;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.hc.base.base.BaseFragment;
import com.hc.base.widget.SlidingTabLayout;
import com.hc.core.di.component.AppComponent;
import com.music.player.haige.R;
import com.music.player.haige.app.EventBusTags;
import com.music.player.haige.app.ToolbarConfig;
import com.music.player.haige.app.base.AppBaseFragment;
import com.music.player.haige.mvp.contract.MVContract;
import com.music.player.haige.mvp.presenter.DownloadFPresenter;
import com.music.player.haige.mvp.ui.widget.adapter.TitlePagerAdapter;

import org.simple.eventbus.EventBus;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * ================================================
 * Created by huangcong on 2018/3/20.
 * ================================================
 */

public class DownloadFragment extends AppBaseFragment<DownloadFPresenter> implements MVContract.DownloadView {

    @BindView(R.id.app_fragment_download_sliding_layout)
    SlidingTabLayout mTabLayout;

    @BindView(R.id.app_fragment_download_vp)
    ViewPager mViewPager;

    public static DownloadFragment newInstance() {
        DownloadFragment fragment = new DownloadFragment();
        return fragment;
    }

    @Override
    public void setupFragmentComponent(AppComponent appComponent) {

    }

    @Override
    public View initView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.app_fragment_download, container, false);
    }

    @Override
    public void initData(Bundle savedInstanceState) {
        String[] titles = getResources().getStringArray(R.array.app_download);
        BaseFragment[] fragments = new BaseFragment[titles.length];
        fragments[0] = DownloadFinishFragment.newInstance();
        fragments[1] = DownloadingFragment.newInstance();
        TitlePagerAdapter adapter = new TitlePagerAdapter(getChildFragmentManager(), titles, fragments);
        mViewPager.setAdapter(adapter);
        mViewPager.setOffscreenPageLimit(adapter.getCount() - 1);
        mTabLayout.setViewPager(mViewPager);
    }

    @OnClick({R.id.app_fragment_download_back_iv})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.app_fragment_download_back_iv:
                EventBus.getDefault().post(EventBusTags.MAIN.BACK);
                break;
        }
    }

}
