package com.music.player.haige.di.component;

import com.hc.core.di.component.AppComponent;
import com.hc.core.di.scope.FragmentScope;
import com.music.player.haige.di.module.DownloadModule;
import com.music.player.haige.mvp.ui.setting.fragment.DownloadFragment;

import dagger.Component;

/**
 * ================================================
 * Created by huangcong on 2018/3/20.
 * ================================================
 */
@FragmentScope
@Component(modules = {DownloadModule.class}, dependencies = AppComponent.class)
public interface DownloadComponent {

    void inject(DownloadFragment fragment);
}
