package com.music.player.haige.mvp.ui.utils;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.music.player.haige.R;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.media.UMWeb;

public class ShareUtils {

    private static final String TAG = ShareUtils.class.getSimpleName();

    public static void shareImage(final Activity activity, Bitmap thumbRes, Bitmap imageRes, UMShareListener shareListener, SHARE_MEDIA platform) {
        UMImage image = new UMImage(activity, imageRes);

        if (platform == SHARE_MEDIA.WEIXIN) {
            image.setThumb(new UMImage(activity, thumbRes));
        }

        new ShareAction(activity)
                .setPlatform(platform)
                .withMedia(image)
                .setCallback(shareListener)
                .share();
    }

    public static void shareImage(final Activity activity, int thumbRes, int imageRes, UMShareListener shareListener, SHARE_MEDIA platform) {
        UMImage image = new UMImage(activity, imageRes);

        if (platform == SHARE_MEDIA.WEIXIN) {
            image.setThumb(new UMImage(activity, thumbRes));
        }

        new ShareAction(activity)
                .setPlatform(platform)
                .withMedia(image)
                .setCallback(shareListener)
                .share();
    }

    public static void shareMusic(final Activity activity, String musicUrl, String title, String thumb, String desc, String targetUrl, UMShareListener shareListener, SHARE_MEDIA platform) {
        UMWeb web = new UMWeb(targetUrl);
        web.setTitle(title);//标题
        web.setThumb(new UMImage(activity, thumb));  //缩略图
        web.setDescription(desc);//描述

        DebugLogger.e(TAG, targetUrl);

        new ShareAction(activity)
                .setPlatform(platform)
                .withMedia(web)
                .setCallback(shareListener)
                .share();
    }

    /**
     * 分享链接
     */
    public static void shareWeb(final Activity activity, String WebUrl, String title, String description, String imageUrl, int imageID, SHARE_MEDIA platform) {
        UMWeb web = new UMWeb(WebUrl);//连接地址
        web.setTitle(title);//标题
        web.setDescription(description);//描述
        if (TextUtils.isEmpty(imageUrl)) {
            web.setThumb(new UMImage(activity, imageID));  //本地缩略图
        } else {
            web.setThumb(new UMImage(activity, imageUrl));  //网络缩略图
        }
        new ShareAction(activity)
                .setPlatform(platform)
                .withMedia(web)
                .setCallback(new UMShareListener() {
                    @Override
                    public void onStart(SHARE_MEDIA share_media) {

                    }

                    @Override
                    public void onResult(final SHARE_MEDIA share_media) {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (share_media.name().equals("WEIXIN_FAVORITE")) {
                                    Toast.makeText(activity, share_media + " 收藏成功", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(activity, share_media + " 分享成功", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }

                    @Override
                    public void onError(final SHARE_MEDIA share_media, final Throwable throwable) {
                        if (throwable != null) {
                            Log.d("throw", "throw:" + throwable.getMessage());
                        }
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(activity, share_media + " 分享失败", Toast.LENGTH_SHORT).show();

                            }
                        });
                    }

                    @Override
                    public void onCancel(final SHARE_MEDIA share_media) {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(activity, share_media + " 分享取消", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                })
                .share();

        //新浪微博中图文+链接
        /*new ShareAction(activity)
                .setPlatform(platform)
                .withText(description + " " + WebUrl)
                .withMedia(new UMImage(activity,imageID))
                .share();*/
    }

    public static boolean checkDownloadShare(int downloadCount) {
        if (downloadCount == 0) {
            return true;
        } else if (downloadCount % 10 == 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 文字绘制在图片上，并返回bitmap对象
     */
    public static Bitmap getUserNameToImg(String name) {
        BitmapDrawable icon = (BitmapDrawable) ResourceUtils.getDrawable(R.drawable.img_share);

        Bitmap bitmap = icon.getBitmap().copy(Bitmap.Config.ARGB_8888, true);
        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setDither(true);
        paint.setTextSize(120);
        paint.setTypeface(Typeface.DEFAULT_BOLD);
        paint.setColor(Color.parseColor("#ff2a54"));

        String text;
        if (name.length() > 10) {
            text = "我是 " + name.substring(0, 10) + "...";
        } else {
            text = "我是 " + name;
        }
        int width = bitmap.getWidth() / 4;

        canvas.drawText(text,
                bitmap.getWidth() / 2 - getTextWidth(paint, text) / 2,
                bitmap.getHeight() / 6 + width, paint);
        return bitmap;
    }

    public static Bitmap getAvatarToImg(Bitmap avatar, Bitmap bitmap) {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setDither(true);

        int width = bitmap.getWidth() / 4;
        Bitmap newAvatar = getRoundBitmap(avatar, width);

        Canvas canvas = new Canvas(bitmap);
        Rect rect = new Rect();
        rect.left = bitmap.getWidth() / 2 - width / 2;
        rect.top = bitmap.getHeight() / 12;
        rect.right = bitmap.getWidth() / 2 + width / 2;
        rect.bottom = rect.top + width;

        canvas.drawBitmap(newAvatar, null, rect, paint);

        return bitmap;
    }

    public static Bitmap getRoundBitmap(Bitmap bitmap, int width) {
        // 前面同上，绘制图像分别需要bitmap，canvas，paint对象
        bitmap = Bitmap.createScaledBitmap(bitmap, width, width, true);
        Bitmap bm = Bitmap.createBitmap(width, width, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bm);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        // 这里需要先画出一个圆
        canvas.drawCircle(width / 2, width / 2, width / 2, paint);
        // 圆画好之后将画笔重置一下
        paint.reset();
        // 设置图像合成模式，该模式为只在源图像和目标图像相交的地方绘制源图像
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, 0, 0, paint);
        return bm;
    }

    public static float getTextWidth(Paint paint, String text) {
        Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), bounds);
        return bounds.width();
    }

}
