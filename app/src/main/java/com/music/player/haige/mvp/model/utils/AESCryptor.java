package com.music.player.haige.mvp.model.utils;


import android.util.Base64;

import java.nio.charset.Charset;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import timber.log.Timber;

public class AESCryptor {

    private static final int KEY_BIT_SIZE = 128;

    private static final Charset CHAR_SET = Charset.forName("utf-8");

    private static final String ALGORITHM = "AES/ECB/PKCS7Padding";

    public static String encrypt(String target, String key) {
        try {
            Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, initKey(key));
            byte[] encryptResult = cipher.doFinal(target.getBytes(CHAR_SET));
            String unsafeStr = new String(Base64.encode(encryptResult, Base64.DEFAULT), CHAR_SET);
            return unsafeStr.replace('+', '-').replace('/', '_');
        } catch (Exception e) {
            Timber.e(e, "AES加密异常－encrypt error:");
        }
        return "";
    }

    public static String decrypt(String target, String key) {
        try {
            Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, initKey(key));
            String unsafeStr = target.replace('-', '+').replace('_', '/');
            byte[] decryptResult = cipher.doFinal(Base64.decode(unsafeStr.getBytes(CHAR_SET), Base64.DEFAULT));
            return new String(decryptResult, CHAR_SET);
        } catch (Exception e) {
            Timber.e(e, "AES解密异常－decrypt error:");
        }
        return "";
    }

    /**
     * @param originalKey
     * @return
     */
    private static SecretKeySpec initKey(String originalKey) {
        byte[] keys = originalKey.getBytes(CHAR_SET);

        byte[] bytes = new byte[KEY_BIT_SIZE / 8];
        for (int i = 0; i < bytes.length; i++) {
            if (keys.length > i) {
                bytes[i] = keys[i];
            } else {
                bytes[i] = 0;
            }
        }

        return new SecretKeySpec(bytes, "AES");
    }


}
