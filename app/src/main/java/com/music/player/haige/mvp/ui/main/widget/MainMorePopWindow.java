package com.music.player.haige.mvp.ui.main.widget;

import android.app.Activity;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.PopupWindow;

import com.hc.core.utils.DeviceUtils;
import com.music.player.haige.R;
import com.music.player.haige.mvp.ui.utils.Utils;

public class MainMorePopWindow extends PopupWindow {

    private Activity mActivity;
    private View mRootView;
    private OnMainMoreClickListener onMainMoreClickListener;

    public MainMorePopWindow(Context context) {
        super(context);
        mActivity = (Activity) context;
        LayoutInflater inflater = LayoutInflater.from(context);
        mRootView = inflater.inflate(R.layout.layout_main_more_pop_window, null);
        this.setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setTouchable(true);
        this.setFocusable(true);
        this.setOutsideTouchable(true);
        this.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_me_profile_more_popwindow));
        this.setContentView(mRootView);

        mRootView.findViewById(R.id.tv_upload).setOnClickListener(v -> {
            if (Utils.isNotNull(onMainMoreClickListener)) {
                onMainMoreClickListener.onUploadMusic();
                dismiss();
            }
        });
    }

    @Override
    public void dismiss() {
        super.dismiss();
        setBackgroundAlpha(1f);
    }

    public void showPopWindow(View targetView) {
        if (!this.isShowing()) {
            setBackgroundAlpha(0.5f);

            int windowPos[] = calculatePopWindowPos(targetView, mRootView);
            int xOff = 20;  // 可以自己调整偏移
            windowPos[0] -= xOff;
            this.showAtLocation(targetView, Gravity.TOP | Gravity.START, windowPos[0], windowPos[1]);
        } else {
            this.dismiss();
        }
    }

    private void setBackgroundAlpha(float alpha) {
        if (mActivity != null) {
            WindowManager.LayoutParams params = mActivity.getWindow().getAttributes();
            params.alpha = alpha;
            mActivity.getWindow().setAttributes(params);
        }
    }

    /**
     * 计算出来的位置，y方向就在anchorView的上面和下面对齐显示，x方向就是与屏幕右边对齐显示
     * 如果anchorView的位置有变化，就可以适当自己额外加入偏移来修正
     * @param anchorView  呼出window的view
     * @param contentView   window的内容布局
     * @return window显示的左上角的xOff,yOff坐标
     */
    private int[] calculatePopWindowPos(final View anchorView, final View contentView) {
        final int windowPos[] = new int[2];
        final int anchorLoc[] = new int[2];
        // 获取锚点View在屏幕上的左上角坐标位置
        anchorView.getLocationOnScreen(anchorLoc);
        final int anchorHeight = anchorView.getHeight();
        // 获取屏幕的高宽
        final int screenHeight = (int) DeviceUtils.getScreenHeight(anchorView.getContext());
        final int screenWidth = (int) DeviceUtils.getScreenWidth(anchorView.getContext());
        contentView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        // 计算contentView的高宽
        final int windowHeight = contentView.getMeasuredHeight();
        final int windowWidth = contentView.getMeasuredWidth();
        // 判断需要向上弹出还是向下弹出显示
        final boolean isNeedShowUp = (screenHeight - anchorLoc[1] - anchorHeight < windowHeight);
        if (isNeedShowUp) {
            windowPos[0] = screenWidth - windowWidth;
            windowPos[1] = anchorLoc[1] - windowHeight;
        } else {
            windowPos[0] = screenWidth - windowWidth;
            windowPos[1] = anchorLoc[1] + anchorHeight;
        }
        return windowPos;
    }

    public void setOnMainMoreClickListener(OnMainMoreClickListener onMainMoreClickListener) {
        this.onMainMoreClickListener = onMainMoreClickListener;
    }

    public interface OnMainMoreClickListener {
        void onUploadMusic();
    }
}
