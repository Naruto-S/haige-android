package com.music.player.haige.app.service;

import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.os.Build;
import android.support.annotation.RequiresApi;

import com.music.player.haige.app.HaigeApplication;
import com.music.player.haige.app.utils.ServiceUtils;

/**
 * Created by Graceful Sun on 18/2/27.
 * E-mail itzhishuaisun@sina.com
 */

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class AlarmJobService extends JobService {

    public static final int DEFAULT_WAKE_UP_INTERVAL = 6 * 60 * 1000; // 6 minutes
    private static final long MIN_PERIOD_MILLIS = 10 * 60 * 1000L;   // 15 minutes
    private static final long MAX_PERIOD_MILLIS = 30 * 60 * 1000L;   // 30 minutes
    private static final int DEFAULT_JOB_ID = 1;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public static void doService() {
        JobInfo.Builder builder = new JobInfo.Builder(DEFAULT_JOB_ID,
                new ComponentName(HaigeApplication.getInstance(), AlarmJobService.class));  //指定哪个JobService执行操作
        builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY);
        builder.setMinimumLatency(MIN_PERIOD_MILLIS); //执行的最小延迟时间
        builder.setOverrideDeadline(MAX_PERIOD_MILLIS);  //执行的最长延时时间
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            builder.setPeriodic(JobInfo.getMinPeriodMillis(), JobInfo.getMinFlexMillis());
        } else {
            builder.setPeriodic(DEFAULT_WAKE_UP_INTERVAL);
        }*/
        builder.setPersisted(true);

        JobScheduler jobScheduler = (JobScheduler) HaigeApplication.getInstance().getSystemService(JOB_SCHEDULER_SERVICE);
        if (jobScheduler != null) {
            jobScheduler.cancelAll();
            jobScheduler.schedule(builder.build());
        }
    }

    @Override
    public boolean onStartJob(JobParameters params) {
        if (!ServiceUtils.isServiceRunning(this, LocalService.class)) {
            LocalService.start(this);
        }
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return false;
    }
}
