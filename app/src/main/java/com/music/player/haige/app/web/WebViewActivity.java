package com.music.player.haige.app.web;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.webkit.CookieManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.music.player.haige.R;
import com.music.player.haige.mvp.ui.utils.DebugLogger;
import com.music.player.haige.mvp.ui.utils.ResourceUtils;
import com.music.player.haige.mvp.ui.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.music.player.haige.app.Constants.URL_PRIVACY_UPLOAD;
import static com.music.player.haige.app.constants.IntentConstants.EXTRA_WEB_URL;

/**
 * Created by yjtao on 14-5-20.
 */
public class WebViewActivity extends AppCompatActivity {

    @BindView(R.id.load_webview)
    WebView mWebView;

    @BindView(R.id.load_progress_layout)
    RelativeLayout mLoadProgressLayout;

    @BindView(R.id.load_progressbar)
    View mLoadProgressbar;

    @BindView(R.id.full_screen_content)
    FrameLayout mFullScreeContent;

    @BindView(R.id.webView_content)
    FrameLayout mWebViewContent;

    @BindView(R.id.app_fragment_policy_title_tv)
    TextView mTitleTv;

    private View customView;
    private String basicUrl;
    private String failUrl;
    private WebChromeClient.CustomViewCallback customViewCallback;
    private WebChromeClient wvcc;
    private int nWidth = 0;
    private boolean hasMeasured = false;

    public static void start(Context context, String url) {
        Intent intent = new Intent(context, WebViewActivity.class);
        intent.putExtra(EXTRA_WEB_URL, url);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        ButterKnife.bind(this);

        initView();
    }

    protected void initView() {
        if (getIntent() != null) {
            basicUrl = getIntent().getStringExtra(EXTRA_WEB_URL);
        }

        if (Utils.isNotEmptyString(basicUrl)) {
            if (basicUrl.equals(URL_PRIVACY_UPLOAD)) {
                mTitleTv.setText("上传声明");
            } else {
                mTitleTv.setText(ResourceUtils.resourceString(R.string.app_about_text_privacy));
            }
        }

        mLoadProgressLayout.setVisibility(View.VISIBLE);
        ViewTreeObserver viewTreeObserver = mLoadProgressLayout.getViewTreeObserver();
        viewTreeObserver.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                if (!hasMeasured) {
                    nWidth = mLoadProgressLayout.getMeasuredWidth();
                    hasMeasured = true;
                }
                return true;
            }
        });

        // 设置webview
        WebViewManager.initWebviewSettings(mWebView);

        if (!Utils.isEmptyString(basicUrl)) {
            mWebView.loadUrl(basicUrl);
        }

        wvcc = new WebChromeClient() {

            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
            }

            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                if (!Utils.isNull(mLoadProgressbar) && !Utils.isNull(mLoadProgressLayout)) {
                    ViewGroup.LayoutParams params = mLoadProgressbar.getLayoutParams();
                    if (newProgress == 100) {
                        DebugLogger.d("onProgressChanged:" + view.getUrl() + ",finish:100");
                        mLoadProgressLayout.setVisibility(View.GONE);
                    } else {
                        if (nWidth == 0 || newProgress == 0) {
                            params.width = 100;
                        } else {
                            params.width = newProgress * nWidth / 100;
                        }
                        mLoadProgressbar.setLayoutParams(params);
                    }
                }
                DebugLogger.d("onProgressChanged:" + view.getUrl() + ",progress:" + newProgress);
                super.onProgressChanged(view, newProgress);
            }

            @Override
            public void onShowCustomView(View view, CustomViewCallback callback) {
                if (!Utils.isNull(customView)) {
                    callback.onCustomViewHidden();
                } else {
                    mFullScreeContent.addView(view);
                    customView = view;
                    customViewCallback = callback;
                    mWebViewContent.setVisibility(View.GONE);
                    mFullScreeContent.setVisibility(View.VISIBLE);
                    mFullScreeContent.bringToFront();
                }
                super.onShowCustomView(view, callback);
            }

            @Override
            public void onHideCustomView() {
                mWebViewContent.setVisibility(View.VISIBLE);
                if (!Utils.isNull(customView)) {
                    customView.setVisibility(View.GONE);
                    mFullScreeContent.removeView(customView);
                    customView = null;
                    mFullScreeContent.setVisibility(View.GONE);
                    try {
                        if (!Utils.isNull(customViewCallback)) {
                            customViewCallback.onCustomViewHidden();
                        }
                    } catch (Throwable throwable) {
                        DebugLogger.e(throwable);
                    }
                }
            }
        };

        // 设置setWebChromeClient对象
        mWebView.setWebChromeClient(wvcc);

        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                return super.shouldOverrideUrlLoading(view, request);
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, android.net.http
                    .SslError error) { //设置webview处理https请求
                DebugLogger.d("onReceivedSslError:" + error);
                super.onReceivedSslError(view, handler, error);
                handler.cancel();
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String
                    failingUrl) { //加载页面报错时的处理
                DebugLogger.d("onReceivedError:" + errorCode + ",description:" + description + "," +
                        "failingUrl:" + failingUrl);
                failUrl = failingUrl;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                failUrl = "";
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                try {
                    DebugLogger.d("onPageFinished,failUrl:" + failUrl + ",url:" + url);
                    super.onPageFinished(view, url);
                    WebViewManager.onPageFinish(view);
                } catch (Throwable throwable) {
                    DebugLogger.e(throwable);
                }
            }

        });

        mWebView.setDownloadListener(new WebViewDownLoadListener(this));
        CookieManager.getInstance().setAcceptCookie(true);
    }


    /**
     * 按键响应，在WebView中查看网页时，按返回键的时候按浏览历史退回,如果不做此项处理则整个WebView返回退出
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            if (!Utils.isNull(customView)) {
                wvcc.onHideCustomView();
                return true;
            }

            if (!Utils.isNull(mWebView) && mWebView.canGoBack()) {
                mWebView.goBack();
            } else {
                finish();
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        try {
            mWebViewContent.removeView(mWebView);
            mWebView.loadDataWithBaseURL(null, "", "text/html", "utf-8", null);
            mWebView.stopLoading();
            // 退出时调用此方法，移除绑定的服务，否则某些特定系统会报错
            mWebView.getSettings().setJavaScriptEnabled(false);
            mWebView.clearHistory();
            mWebView.clearView();
            mWebView.removeAllViews();
            mWebView.destroy();
        } catch (Throwable e) {
            DebugLogger.e(e);
        }
        mWebView = null;
        mLoadProgressLayout = null;
        mLoadProgressbar = null;
        mWebViewContent = null;
        customView = null;
        customViewCallback = null;
        mFullScreeContent = null;

//        System.gc();
//        System.runFinalization();
        super.onDestroy();
    }

    @OnClick({R.id.app_fragment_policy_back_iv})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.app_fragment_policy_back_iv:
                finish();
                break;
        }
    }
}