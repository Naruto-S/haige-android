package com.hc.core.mvp;

import android.app.Activity;
import android.content.Intent;

/**
 * ================================================
 * 框架要求框架中的每个 View 都需要实现此类,以满足规范
 * <p>
 * ================================================
 */
public interface IView<A> {

    /**
     * 显示加载
     */
    void showLoading();

    /**
     * 隐藏加载
     */
    void hideLoading();

    /**
     * 加载成功
     * @param action
     * @param o
     */
    void onSuccess(A action, Object o);

    /**
     * 加载失败
     * @param action
     * @param code
     * @param message
     */
    void onError(A action, String code, String message);

    /**
     * 显示信息
     */
    void showMessage(String message);

    /**
     * 跳转 {@link Activity}
     */
    void launchActivity(Intent intent);

    /**
     * 杀死自己
     */
    void killMyself();
}
