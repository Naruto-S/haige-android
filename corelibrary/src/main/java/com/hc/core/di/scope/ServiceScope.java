package com.hc.core.di.scope;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;

import javax.inject.Scope;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * ================================================
 * Created by huangcong on 2018/3/3.
 * ================================================
 */
@Scope
@Documented
@Retention(RUNTIME)
public @interface ServiceScope {
}
