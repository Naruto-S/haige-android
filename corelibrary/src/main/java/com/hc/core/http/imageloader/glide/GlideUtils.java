package com.hc.core.http.imageloader.glide;

import android.content.Context;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.engine.cache.DiskLruCacheWrapper;
import com.bumptech.glide.signature.EmptySignature;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;

/**
 * ================================================
 * Created by huangcong on 2018/3/8.
 * ================================================
 */

public class GlideUtils {

    /**
     * 判断glide是否已经缓存了此图片
     *
     * @param context
     * @param url
     * @return
     */
    public static boolean isImageCached(Context context, String url) {
        File file = DiskLruCacheWrapper.get(Glide.getPhotoCacheDir(context), 250 * 1024 * 1024).get(new OriginalKey(url, EmptySignature.obtain()));
        return file != null;
    }

    /**
     * Glide原图缓存Key
     */
    private static class OriginalKey implements Key {

        private final String id;
        private final Key signature;

        private OriginalKey(String id, Key signature) {
            this.id = id;
            this.signature = signature;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            OriginalKey that = (OriginalKey) o;

            return id.equals(that.id) && signature.equals(that.signature);
        }

        @Override
        public int hashCode() {
            int result = id.hashCode();
            result = 31 * result + signature.hashCode();
            return result;
        }

        @Override
        public void updateDiskCacheKey(MessageDigest messageDigest)  {
            try {
                messageDigest.update(id.getBytes(STRING_CHARSET_NAME));
                signature.updateDiskCacheKey(messageDigest);
            }catch (UnsupportedEncodingException exception){
            }catch (Exception e){
            }
        }
    }
}
