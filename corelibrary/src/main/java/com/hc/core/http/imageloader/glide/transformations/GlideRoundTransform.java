package com.hc.core.http.imageloader.glide.transformations;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;

import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;

import java.security.MessageDigest;

import timber.log.Timber;

/**
 * Created by huangcong on 2017/10/25.
 * <p>
 * 支持圆形、圆环白边
 */
public class GlideRoundTransform extends BitmapTransformation {

    private static final int VERSION = 1;
    private static final String ID =
            "com.hc.core.http.imageloader.glide.transformations.GlideRoundTransform." + VERSION;
    private static final byte[] ID_BYTES = ID.getBytes(CHARSET);

    private boolean isCircle;              // 是否为圆形图片
    private float mRadius = 0f;            // 默认圆角大小
    private int mBorderColor = Color.BLACK;// 圆形边缘的默认颜色
    private float mBorderWidth = 0;        // 圆形边缘的默认宽度

    public GlideRoundTransform(Context context) {
        super(context);
        this.isCircle = true;
    }

    public GlideRoundTransform(Context context, int radius) {
        super(context);
        this.isCircle = false;
        this.mRadius = Resources.getSystem().getDisplayMetrics().density * radius;
    }

    public GlideRoundTransform(Context context, int borderColor, int borderWidth) {
        this(context, true, borderColor, borderWidth);
    }

    public GlideRoundTransform(Context context, boolean isCircle, int borderColor, int borderWidth) {
        super(context);
        float density = Resources.getSystem().getDisplayMetrics().density;
        this.isCircle = isCircle;
        this.mBorderColor = borderColor;
        this.mBorderWidth = density * borderWidth;
    }

    public GlideRoundTransform(Context context, int radius, int borderColor, int borderWidth) {
        super(context);
        float density = Resources.getSystem().getDisplayMetrics().density;
        this.isCircle = false;
        this.mRadius = density * radius;
        this.mBorderColor = borderColor;
        this.mBorderWidth = density * borderWidth;
    }

    @Override
    protected Bitmap transform(BitmapPool pool, Bitmap toTransform, int outWidth, int outHeight) {
        return isCircle ? circleCrop(pool, toTransform) : roundCrop(pool, toTransform);
    }

    private Bitmap circleCrop(BitmapPool pool, Bitmap source) {
        if (source == null) return null;

        try {
            int size = Math.min(source.getWidth(), source.getHeight());
            int x = (source.getWidth() - size) / 2;
            int y = (source.getHeight() - size) / 2;
            // TODO this could be acquired from the pool too
            Bitmap squared = Bitmap.createBitmap(source, x, y, size, size);

            Bitmap result = pool.get(size, size, Bitmap.Config.ARGB_8888);
            if (result == null) {
                result = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
            }
            Canvas canvas = new Canvas(result);
            Paint paint = new Paint();
            paint.setShader(new BitmapShader(squared, BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP));
            paint.setAntiAlias(true);
            float r = size / 2f;
            canvas.drawCircle(r, r, r, paint);
            // 绘制外环
            if (mBorderWidth != 0) {
                Paint borderPaint = new Paint();
                // 设置边界画笔样式
                borderPaint.setStyle(Paint.Style.STROKE);//设画笔为空心
                borderPaint.setAntiAlias(true);
                borderPaint.setColor(mBorderColor);    //画笔颜色
                borderPaint.setStrokeWidth(mBorderWidth);//画笔边界宽度
                canvas.drawCircle(r, r, r - mBorderWidth / 2f, borderPaint);
            }
            return result;
        } catch (Throwable throwable) {
            Timber.e(throwable, "Glide Round Transform Error!");
        }
        return null;
    }

    private Bitmap roundCrop(BitmapPool pool, Bitmap source) {
        if (source == null) return null;

        try {
            Bitmap result = pool.get(source.getWidth(), source.getHeight(), Bitmap.Config.ARGB_8888);
            if (result == null) {
                result = Bitmap.createBitmap(source.getWidth(), source.getHeight(), Bitmap.Config.ARGB_8888);
            }

            Canvas canvas = new Canvas(result);
            Paint paint = new Paint();
            paint.setShader(new BitmapShader(source, BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP));
            paint.setAntiAlias(true);
            RectF rectF = new RectF(0f, 0f, source.getWidth(), source.getHeight());
            canvas.drawRoundRect(rectF, mRadius, mRadius, paint);
            // 绘制外环
            if (mBorderWidth != 0) {
                Paint borderPaint = new Paint();
                // 设置边界画笔样式
                borderPaint.setStyle(Paint.Style.STROKE);//设画笔为空心
                borderPaint.setAntiAlias(true);
                borderPaint.setColor(mBorderColor);    //画笔颜色
                borderPaint.setStrokeWidth(mBorderWidth);//画笔边界宽度
                RectF oval = new RectF(mBorderWidth, mBorderWidth, source.getWidth() - mBorderWidth, source.getHeight() - mBorderWidth);// 设置个新的长方形
                canvas.drawRoundRect(oval, mRadius, mRadius, borderPaint);//第二个参数是x半径，第三个参数是y半径
            }
            return result;
        } catch (Throwable throwable) {
            Timber.e(throwable, "Glide Round Transform Error!");
        }
        return null;
    }


    @Override
    public String toString() {
        return "GlideRoundTransform()";
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof GrayscaleTransformation;
    }

    @Override
    public int hashCode() {
        return ID.hashCode();
    }

    @Override
    public void updateDiskCacheKey(MessageDigest messageDigest) {
        messageDigest.update(ID_BYTES);
    }
}
