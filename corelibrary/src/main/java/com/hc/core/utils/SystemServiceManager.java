package com.hc.core.utils;

import com.hc.core.base.CoreApplication;

import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.Map;

import timber.log.Timber;

public class SystemServiceManager {
    private static volatile SystemServiceManager sInstance = null;
    private Map<String, SoftReference<Object>> mSystemService;

    private SystemServiceManager() {
        mSystemService = new HashMap<String, SoftReference<Object>>();
    }

    public static SystemServiceManager getInstance() {
        if (sInstance == null) {
            synchronized (SystemServiceManager.class) {
                if (sInstance == null) {
                    sInstance = new SystemServiceManager();
                }
            }
        }
        return sInstance;
    }

    /**
     * 获取服务
     *
     * @param name 最好使用系统Context 中定义的常量
     * @return
     */
    public synchronized Object getService(String name) {
        Object service = null;
        if (mSystemService.containsKey(name)) {
            SoftReference sr = mSystemService.get(name);
            if (sr != null) {
                service = sr.get();
            }
        }
        if (service != null) {
            return service;
        }
        try {
            service = CoreApplication.getInstance().getSystemService(name);
            if (service != null) {
                SoftReference softReference = new SoftReference(service);
                mSystemService.put(name, softReference);
                Timber.d("put service[name,value]->[" + name + "," + service.hashCode() + "]");
            }
            printCurrentServiceSize();
        } catch (Throwable throwable) {
            // no should occur
        }
        return service; //may null
    }

    /**
     * 打印当前缓存的服务个数
     */
    public void printCurrentServiceSize() {
        Timber.d("SystemServiceManager" + ".getCurrentServiceSize->" + mSystemService.size());
    }
}
