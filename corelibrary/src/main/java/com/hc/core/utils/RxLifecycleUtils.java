package com.hc.core.utils;

import com.hc.core.integration.lifecycle.ActivityLifecycleable;
import com.hc.core.integration.lifecycle.FragmentLifecycleable;
import com.hc.core.integration.lifecycle.Lifecycleable;
import com.hc.core.mvp.IView;
import com.trello.rxlifecycle2.LifecycleTransformer;
import com.trello.rxlifecycle2.RxLifecycle;
import com.trello.rxlifecycle2.android.ActivityEvent;
import com.trello.rxlifecycle2.android.FragmentEvent;
import com.trello.rxlifecycle2.android.RxLifecycleAndroid;

import io.reactivex.annotations.NonNull;
import timber.log.Timber;

/**
 * ================================================
 * 使用此类操作 RxLifecycle 的特性
 * <p>
 * ================================================
 */

public class RxLifecycleUtils {

    private RxLifecycleUtils() {
        throw new IllegalStateException("you can't instantiate me!");
    }

    /**
     * 绑定 Activity 的指定生命周期
     *
     * @param view
     * @param event
     * @param <T>
     * @return
     */
    public static <T> LifecycleTransformer<T> bindUntilEvent(@NonNull final IView view, final ActivityEvent event) {
        try {
            if (view instanceof ActivityLifecycleable) {
                return bindUntilEvent((ActivityLifecycleable) view, event);
            }
        } catch (Throwable t) {
            Timber.e(t);
        }
        return null;
    }

    /**
     * 绑定 Fragment 的指定生命周期
     *
     * @param view
     * @param event
     * @param <T>
     * @return
     */
    public static <T> LifecycleTransformer<T> bindUntilEvent(@NonNull final IView view, final FragmentEvent event) {
        try {
            if (view instanceof FragmentLifecycleable) {
                return bindUntilEvent((FragmentLifecycleable) view, event);
            }
        } catch (Throwable t) {
            Timber.e(t);
        }
        return null;
    }

    public static <T, R> LifecycleTransformer<T> bindUntilEvent(@NonNull final Lifecycleable<R> lifecycleable, final R event) {
        try {
            return RxLifecycle.bindUntilEvent(lifecycleable.provideLifecycleSubject(), event);
        } catch (Throwable t) {
            Timber.e(t);
        }
        return null;
    }


    /**
     * 绑定 Activity/Fragment 的生命周期
     *
     * @param view
     * @param <T>
     * @return
     */
    public static <T> LifecycleTransformer<T> bindToLifecycle(@NonNull IView view) {
        try {
            if (view instanceof Lifecycleable) {
                return bindToLifecycle((Lifecycleable) view);
            }
        } catch (Throwable t) {
            Timber.e(t);
        }
        return null;
    }

    public static <T> LifecycleTransformer<T> bindToLifecycle(@NonNull Lifecycleable lifecycleable) {
        try {
            if (lifecycleable instanceof ActivityLifecycleable) {
                return RxLifecycleAndroid.bindActivity(((ActivityLifecycleable) lifecycleable).provideLifecycleSubject());
            } else if (lifecycleable instanceof FragmentLifecycleable) {
                return RxLifecycleAndroid.bindFragment(((FragmentLifecycleable) lifecycleable).provideLifecycleSubject());
            }
        } catch (Throwable t) {
            Timber.e(t);
        }
        return null;
    }

}
