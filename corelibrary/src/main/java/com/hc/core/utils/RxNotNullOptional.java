package com.hc.core.utils;

import android.support.annotation.Nullable;

import java.util.NoSuchElementException;

/**
 * ================================================
 * Created by huangcong on 2018/4/10.
 * <p>
 * rx2扩展:Observable.just()不能传空值
 * ================================================
 */

public class RxNotNullOptional<M> {
    private final M optional;

    public RxNotNullOptional(@Nullable M optional) {
        this.optional = optional;
    }

    public boolean isEmpty() {
        return this.optional == null;
    }

    public M get() {
        if (optional == null) {
            throw new NoSuchElementException("No value present");
        }
        return optional;
    }
}
